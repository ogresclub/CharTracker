package com.mtths.chartracker.preferences

import android.content.Context
import android.content.SharedPreferences
import com.mtths.chartracker.preferences.PrefsHelper.Companion.MY_SHARED_PREFS
import com.mtths.chartracker.utils.Helper

open class PrefsHelperBase(val context: Context?, val SHARED_PREFS_REF: String = MY_SHARED_PREFS) {

    fun getInstance(): SharedPreferences? {
        val sharedPrefs = context?.getSharedPreferences(SHARED_PREFS_REF, 0)
        if (sharedPrefs == null) {
            Helper.log("PREFS_HELPER", "unable to get PrefsHelper, no context was given")
        }
        return sharedPrefs

    }

    fun putString(key: String?, value: String?) {
        getInstance()?.edit()?.putString(key, value)?.apply()
    }

    fun getString(key: String?, defValue: String?): String? {
        return getInstance()?.getString(key, defValue)
    }

    fun getStringNotNull(key: String?, defValue: String): String {
        return getString(key, defValue) ?: defValue
    }

    fun putInt(key: String?, value: Int) {
        getInstance()?.edit()?.putInt(key, value)?.apply()
    }

    fun getInt(key: String?, defValue: Int): Int? {
        return getInstance()?.getInt(key, defValue)
    }

    fun getIntNotNull(key: String?, defValue: Int): Int {
        return getInstance()?.getInt(key, defValue) ?: defValue
    }

    fun putBoolean(key: String?, value: Boolean) {
        getInstance()?.edit()?.putBoolean(key, value)?.apply()
    }

    fun getBoolean(key: String?, defValue: Boolean): Boolean? {
        return getInstance()?.getBoolean(key, defValue)
    }

    fun getBooleanNotNull(key: String?, defValue: Boolean): Boolean {
        return getInstance()?.getBoolean(key, defValue) ?: defValue
    }
}
