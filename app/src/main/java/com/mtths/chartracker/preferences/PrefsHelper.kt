package com.mtths.chartracker.preferences

import android.content.Context
import android.content.UriPermission
import android.content.res.Configuration
import android.util.Log
import androidx.documentfile.provider.DocumentFile
import com.dropbox.core.json.JsonReadException
import com.dropbox.core.oauth.DbxCredential
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_default
import com.mtths.chartracker.dialogs.DialogDataOptions
import com.mtths.chartracker.utils.FileUtils
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.TranslationHelper



class PrefsHelper(
    context: Context?,
    SHARED_PREFS_REF: String = MY_SHARED_PREFS
):
    PrefsHelperBase(context, SHARED_PREFS_REF) {

    private var extendCPIMap: HashMap<String, HashMap<Int, Boolean>>
        get() = Character.parseExtendMap(
            getStringNotNull(
                PREF_EXPAND_CHAR_PROG_ITEM_DICT,
                DEFAULT_PREF_EXPAND_CHAR_PROG_ITEM_DICT
            )
        )
        set(value) {
            putString(
            PREF_EXPAND_CHAR_PROG_ITEM_DICT,
            Gson().toJson(HashMap(value)))}

    fun getExtend(charProgItemFilterConstraintId: Int, orientation: Int): Boolean {
        val fcEnglish = TranslationHelper(requireNotNull(context)).getEnglishTranslation(charProgItemFilterConstraintId)
        return getExtend(
            key = fcEnglish,
            orientation = orientation
        )
    }
    fun getExtend(key: String, orientation: Int): Boolean {
        return extendCPIMap[key]?.get(orientation) ?:
        CharProgressItem.DEFAULT_EXTEND_CHAR_PROGRESS_ITEMS
    }

    fun toggleExtend(charProgItemFilterConstraintId: Int, orientation: Int) {
        val fcEnglish = TranslationHelper(requireNotNull(context)).getEnglishTranslation(charProgItemFilterConstraintId)
        toggleExtend(
            key = fcEnglish,
            orientation = orientation
        )
    }
    fun toggleExtend(key: String, orientation: Int) {
        val bigMap = extendCPIMap
        val map = bigMap[key] ?: HashMap()
        map[orientation] = !(map[orientation] ?: CharProgressItem.DEFAULT_EXTEND_CHAR_PROGRESS_ITEMS)
        bigMap[key] = map
        extendCPIMap = bigMap
    }

    var language: String
        get() = getStringNotNull(PREF_APP_LANGUAGE, DEFAULT_PREF_APP_LANGUAGE)
        set(value) = putString(PREF_APP_LANGUAGE, value)

    var dropBoxCredential: DbxCredential?
        get() = getString(PREF_DROPBOX_CREDENTIAL, null)?.let {
            try {
                DbxCredential.Reader.readFully(it)
            } catch (e: JsonReadException) {
                return null
            }
        }
        set(value) { putString(PREF_DROPBOX_CREDENTIAL, value.toString()) }

    var lootmanagerToken: String?
        get() = getString(PREF_LOOTMANAGER_TOKEN, null)
        set(value) = putString(PREF_LOOTMANAGER_TOKEN, value)

    val lootmanagerDeviceId: String
        get() = getString(PREF_LOOTMANAGER_DEVICE_ID, null)?: run {
            val ALLOWED_CHARACTERS = "0123456789qwertyuiopasdfghjklzxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM"
            val random = java.security.SecureRandom()
            val size = 256
            val sb = StringBuilder(size)
            for (i in 0 until size)
                sb.append(ALLOWED_CHARACTERS[random.nextInt(ALLOWED_CHARACTERS.length)])
            val id = sb.toString()
            putString(PREF_LOOTMANAGER_DEVICE_ID, id)
            Helper.log("LOOTMANAGER", "Device-Id generated and stored: $id")
            id
        }

    var selectedDamageTypes: HashMap<String, Int>
        get() = Character.parseHashMapStrInt(
            getStringNotNull(PREF_SELECTED_DAMAGE_TYPES, EMPTY_HASH_MAP_STRING)
        )
        set(value) = putString(PREF_SELECTED_DAMAGE_TYPES, Gson().toJson(value))

    var damageMonitorModi: HashMap<String, Boolean>
        get() = getStringNotNull(PREF_DAMAGE_MONITORS_MODI_REPLACE, EMPTY_HASH_MAP_STRING).let { json ->
            Gson().fromJson(json, object : TypeToken<HashMap<String, Boolean>>() {}.type)
        }
        set(value) { putString(PREF_DAMAGE_MONITORS_MODI_REPLACE, Gson().toJson(value))}


    fun toggleDamageMonitorModus(damageType: String) {
        val map = damageMonitorModi
        map[damageType] = !(map[damageType] ?: DEFAULT_PREF_DAMAGE_MONITOR_MODUS_REPLACE)
        damageMonitorModi = map
    }

    var xmlExport: Int
            get() = getIntNotNull(PREF_XML_EXPORT_OPTION, DatabaseHelper.EXPORT_CURRENT_SESSIONS)
            set(value) = putInt(PREF_XML_EXPORT_OPTION, value)

    var xmlImport: Int
        get() = getIntNotNull(PREF_XML_IMPORT_OPTION, DatabaseHelper.IMPORT_KEEP_LOCAL)
        set(value) = putInt(PREF_XML_IMPORT_OPTION, value)

    var xmlImportDeleteDb : Boolean
        get() = getBooleanNotNull(PREF_XML_IMPORT_DELETE_DB, false)
        set(value) = putBoolean(PREF_XML_IMPORT_DELETE_DB, value)

    
    var filterChars: Int
        get() = getIntNotNull(PREF_FILTER_CHARS,  FILTER_CHARS_YES)
        set(value) = putInt(PREF_FILTER_CHARS, value)

    var filterSessions: Boolean
        get() = getBooleanNotNull(PREF_FILTER_SESSIONS,  true)
        set(value) = putBoolean(PREF_FILTER_SESSIONS, value)

    var showCharProgression: Boolean
        get() = getBooleanNotNull(PREF_SHOW_CHAR_PROGRESSION, DEFAULT_SHOW_CHAR_PROGRESSION)
        set(value) = putBoolean(PREF_SHOW_CHAR_PROGRESSION, value)

    var backupDatabaseOnAppStart: Boolean
        get() = getBooleanNotNull(PREF_BACKUP_DB_ON_START, DEFAULT_PREF_BACKUP_DB_ON_START)
        set(value) = putBoolean(PREF_BACKUP_DB_ON_START, value)
    
    var crawlOgresClub: Int
        get() = getIntNotNull(PREF_CRAWL_OGRES_CLUB, DEFAULT_CRAWL_OGRES_CLUB)
        set(value) = putInt(PREF_CRAWL_OGRES_CLUB, value)

    var statusBarOffsetMainActivity: Int
        get() = if (Helper.getOrientation(context) == Configuration.ORIENTATION_LANDSCAPE) {
                getIntNotNull(
                    PREF_STATUS_BAR_OFFSET_MAIN_SCREEN_LAND,
                    DEFAULT_STATUS_BAR_OFFSET_MAIN_SCREEN
                )
            } else {
                getIntNotNull(
                    PREF_STATUS_BAR_OFFSET_MAIN_SCREEN,
                    DEFAULT_STATUS_BAR_OFFSET_MAIN_SCREEN
                )
            }

        set(value) = if (Helper.getOrientation(context) == Configuration.ORIENTATION_LANDSCAPE) {
            putInt(PREF_STATUS_BAR_OFFSET_MAIN_SCREEN_LAND, value)
        } else {
            putInt(PREF_STATUS_BAR_OFFSET_MAIN_SCREEN, value)
        }

    var statusBarOffsetCharDetails: Int
        get() = if (Helper.getOrientation(context) == Configuration.ORIENTATION_LANDSCAPE) {
            getIntNotNull(
                PREF_STATUS_BAR_OFFSET_CHAR_DETAILS_LAND,
                DEFAULT_STATUS_BAR_OFFSET_CHAR_DETAILS
            )
        } else {
            getIntNotNull(
                PREF_STATUS_BAR_OFFSET_CHAR_DETAILS,
                DEFAULT_STATUS_BAR_OFFSET_CHAR_DETAILS
            )

        }
        set(value) = if (Helper.getOrientation(context) == Configuration.ORIENTATION_LANDSCAPE) {
            putInt(PREF_STATUS_BAR_OFFSET_CHAR_DETAILS_LAND, value)
        } else {
            putInt(PREF_STATUS_BAR_OFFSET_CHAR_DETAILS, value)
        }

    var sessionDisplayHeightCorrection: Int
        get() = getIntNotNull(
            if (Helper.getOrientation(context) == Configuration.ORIENTATION_LANDSCAPE) {
                PREF_SESSION_DISPLAY_HEIGHT_CORRECTION_LAND
            } else {
                PREF_SESSION_DISPLAY_HEIGHT_CORRECTION

            },
            DEFAULT_SESSION_DISPLAY_HEIGHT_CORRECTION
        )
        set(value) = putInt(
            if (Helper.getOrientation(context) == Configuration.ORIENTATION_LANDSCAPE) {
                PREF_SESSION_DISPLAY_HEIGHT_CORRECTION_LAND
            } else {
                PREF_SESSION_DISPLAY_HEIGHT_CORRECTION
            },
            value
        )
    
    var maxNumColsMainScreen: Int
    get() {
        val orientation: Int = Helper.getOrientation(context)
        return if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            getIntNotNull(
                PREF_MAX_NUM_COLS_MAIN_SCREEN_LAND,
                DEFAULT_MAX_NUM_COLS_MAIN_SCREEN_LAND
            )
        } else {
            getIntNotNull(PREF_MAX_NUM_COLS_MAIN_SCREEN, DEFAULT_MAX_NUM_COLS_MAIN_SCREEN)

        }
    }
    set(value) {
        val orientation: Int = context?.resources?.configuration?.orientation
            ?: Configuration.ORIENTATION_PORTRAIT
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            putInt(PREF_MAX_NUM_COLS_MAIN_SCREEN_LAND, value)
        } else {
            putInt(PREF_MAX_NUM_COLS_MAIN_SCREEN, value)        }
    }

    var numColsCharView: Int
        get() {
            val orientation: Int = context?.resources?.configuration?.orientation
                    ?: Configuration.ORIENTATION_PORTRAIT
            return if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                getInt(PREF_NUM_COLS_CHAR_VIEW_LAND, DEFAULT_NUM_COLS_CHAR_VIEW_LAND)
                    ?: DEFAULT_NUM_COLS_CHAR_VIEW_LAND
            } else {
                getInt(PREF_NUM_COLS_CHAR_VIEW, DEFAULT_NUM_COLS_CHAR_VIEW)
                    ?: DEFAULT_NUM_COLS_CHAR_VIEW

            }
        }
        set(value) {
            val orientation: Int = context?.resources?.configuration?.orientation
                ?: Configuration.ORIENTATION_PORTRAIT
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                putInt(PREF_NUM_COLS_CHAR_VIEW_LAND, value)
            } else {
                putInt(PREF_NUM_COLS_CHAR_VIEW, value)            }
        }

    var searchContextSize: Int
        get() =  getIntNotNull(PREF_SEARCH_CONTEXT_SIZE, DEFAULT_SEARCH_CONTEXT_SIZE)
        set(value) = putInt(PREF_SEARCH_CONTEXT_SIZE, value)

    var checkFeaturedStats: Boolean
        get() = getBooleanNotNull(PREF_CHECK_FEATURED_STATS,  DEFAULT_PREF_CHECK_FEATURED_STATS)
        set(value) = putBoolean(PREF_CHECK_FEATURED_STATS, value)

    var checkFeaturedLanguages: Boolean
        get() = getBooleanNotNull(
            PREF_CHECK_FEATURED_LANGUAGES,
            DEFAULT_PREF_CHECK_FEATURED_LANGUAGES
        )
        set(value) = putBoolean(PREF_CHECK_FEATURED_LANGUAGES, value)

    var checkExpCalcMistakes: Boolean
        get() = !rulesE5 && getBooleanNotNull(PREF_CHECK_EXP_CALC_MISTAKES,  DEFAULT_CHECK_EXP_CALC_MISTAKES)
        set(value) = putBoolean(PREF_CHECK_EXP_CALC_MISTAKES, value)

    var showPriorities: Boolean
        get() = getBooleanNotNull(
            PREF_SHOW_PRIORITIES_IN_CHAR_VIEW,
            DEFAULT_SHOW_PRIORITIES_IN_CHAR_VIEW
        )
        set(value) = putBoolean(PREF_SHOW_PRIORITIES_IN_CHAR_VIEW, value)

    var showOnlyActiveSessionsCharacters: Boolean
        get() = getBooleanNotNull(
            PREF_SHOW_ONLY_ACTIVE_SESSIONS_CHARACTERS_IN_CHAR_VIEW,
            DEFAULT_SHOW_ONLY_ACTIVE_SESSIONS_CHARACTERS_IN_CHAR_VIEW
        )
        set(value) = putBoolean(PREF_SHOW_ONLY_ACTIVE_SESSIONS_CHARACTERS_IN_CHAR_VIEW, value)

    var showCharsRelatedToSession: Boolean
        get() = getBooleanNotNull(
            PREF_SHOW_CHARS_RELATED_TO_SESSION,
            DEFAULT_SHOW_CHARS_RELATED_TO_SESSION
        )
        set(value) = putBoolean(PREF_SHOW_CHARS_RELATED_TO_SESSION, value)

    var hgExpPercentage: Int
        get() = getIntNotNull(PREF_HG_EXP_PERCENTAGE, DEFAULT_PREF_HG_EXP_PERCENTAGE)
        set(value) = putInt(PREF_HG_EXP_PERCENTAGE, value)

    /**
     * value is stored as integer so divide by 100 to get percentage value
     */
    var fokusPercentage: Int
        get() = getIntNotNull(PREF_FOCUS_PERCENTAGE, DEFAULT_PREF_FOCUS_PERCENTAGE)
        set(value) = putInt(PREF_FOCUS_PERCENTAGE, value)

    var showCharNames: Boolean
        get() = getBooleanNotNull(PREF_SHOW_CHAR_NAMES,  DEFAULT_PREF_SHOW_CHAR_NAMES)
        set(value) = putBoolean(PREF_SHOW_CHAR_NAMES, value)

    var checkFilterConstraintErrors: Boolean
        get() = getBooleanNotNull(PREF_CHECK_FILTER_CONSTRAINT_ERRORS,  DEFAULT_PREF_CHECK_FILTER_CONSTRAINT_ERRORS)
        set(value) = putBoolean(PREF_CHECK_FILTER_CONSTRAINT_ERRORS, value)

    var showSessionStatsMainScreen: Boolean
        get() = getBooleanNotNull(PREF_SHOW_SESSION_STATS_MAIN_SCREEN,  DEFAULT_SHOW_SESSION_STATS)
        set(value) = putBoolean(PREF_SHOW_SESSION_STATS_MAIN_SCREEN, value)

    var showSessionStatsCharDetails: Boolean
        get() = getBooleanNotNull(PREF_SHOW_SESSION_STATS_CHAR_DETAILS,  DEFAULT_SHOW_SESSION_STATS)
        set(value) = putBoolean(PREF_SHOW_SESSION_STATS_CHAR_DETAILS, value)


    var deleteGenesisCharOnUpdate: Boolean
        get() = getBooleanNotNull(PREF_DELETE_GENESIS_ON_UPDATE,  DEFAULT_DELETE_GENESIS_ON_UPDATE)
        set(value) = putBoolean(PREF_DELETE_GENESIS_ON_UPDATE, value)

    var autoUploadCharIcons: Boolean
        get() = getBooleanNotNull(PREF_AUTO_UPLOAD_CHAR_ICONS, DEFAULT_AUTO_UPLOAD_CHAR_ICONS)
        set(value) = putBoolean(PREF_AUTO_UPLOAD_CHAR_ICONS, value)

    var autoImportIcons: Boolean
        get() = getBooleanNotNull(PREF_AUTO_IMPORT_CHAR_ICONS, DEFAULT_AUTO_IMPORT_CHAR_ICONS)
        set(value) = putBoolean(PREF_AUTO_IMPORT_CHAR_ICONS, value)

    var autoDownloadCharItems: Boolean
        get() = getBooleanNotNull(PREF_AUTO_DOWNLOAD_CHAR_ITEMS, DEFAULT_AUTO_DOWNLOAD_CHAR_ITEMS)
        set(value) = putBoolean(PREF_AUTO_DOWNLOAD_CHAR_ITEMS, value)

    var sortQuestsSessionStats: Int
        get() = getIntNotNull(PREF_SESSION_STATS_SORT_QUEST, DEFAULT_PREF_SESSION_STATS_SORT_QUEST)
        set(value) = putInt(PREF_SESSION_STATS_SORT_QUEST, value)

    var sortCharactersSessionStats: Int
        get() = getIntNotNull(PREF_SESSION_STATS_SORT_CHARACTER, DEFAULT_PREF_SESSION_STATS_SORT_CHARACTER)
        set(value) = putInt(PREF_SESSION_STATS_SORT_CHARACTER, value)

    var sortPlacesSessionStats: Int
        get() = getIntNotNull(PREF_SESSION_STATS_SORT_PLACE, DEFAULT_PREF_SESSION_STATS_SORT_PLACE)
        set(value) = putInt(PREF_SESSION_STATS_SORT_PLACE, value)

    var showAllCharsSessionStats: Boolean
        get() = getBooleanNotNull(PREF_SHOW_ALL_CHARS_SESSION_STATS, DEFAULT_PREF_SHOW_ALL_CHARS_SESSION_STATS)
        set(value) = putBoolean(PREF_SHOW_ALL_CHARS_SESSION_STATS, value)

    var showCharactersDescriptionsInSessionStats: Boolean
        get() = getBooleanNotNull(PREF_SHOW_CHARS_SESSION_DESCRIPTION, DEFAULT_PREF_SHOW_CHARS_SESSION_DESCRIPTION)
        set(value) = putBoolean(PREF_SHOW_CHARS_SESSION_DESCRIPTION, value)

    var showAllPlacesSessionStats: Boolean
        get() = getBooleanNotNull(PREF_SHOW_ALL_PLACES_SESSION_STATS, DEFAULT_PREF_SHOW_ALL_PLACES_SESSION_STATS)
        set(value) = putBoolean(PREF_SHOW_ALL_PLACES_SESSION_STATS, value)

    var showPlacesDescriptionsInSessionStats: Boolean
        get() = getBooleanNotNull(PREF_SHOW_PLACES_SESSION_DESCRIPTION, DEFAULT_PREF_SHOW_PLACES_SESSION_DESCRIPTION)
        set(value) = putBoolean(PREF_SHOW_PLACES_SESSION_DESCRIPTION, value)

    var showAllQuestsSessionStats: Boolean
        get() = getBooleanNotNull(PREF_SHOW_ALL_QUESTS_SESSION_STATS, DEFAULT_PREF_SHOW_ALL_QUESTS_SESSION_STATS)
        set(value) = putBoolean(PREF_SHOW_ALL_QUESTS_SESSION_STATS, value)

    // todo remove: I think this is not needed anymore
    var filterSearchResults: Boolean
        get() = getBooleanNotNull(PREF_FILTER_SEARCH_RESULTS, DEFAULT_PREF_FILTER_SEARCH_RESULTS)
        set(value) = putBoolean(PREF_FILTER_SEARCH_RESULTS, value)

    var searchSubMatches: Boolean
        get() = getBooleanNotNull(PREF_SEARCH_SUB_MATCHES, DEFAULT_PREF_SEARCH_SUB_MATCHES)
        set(value) = putBoolean(PREF_SEARCH_SUB_MATCHES, value)

    var colorDateChanges: Boolean
        get() = getBooleanNotNull(PREF_COLOR_DATE_CHANGES, DEFAULT_PREF_COLOR_DATE_CHANGES)
        set(value) = putBoolean(PREF_COLOR_DATE_CHANGES, value)

    var showRuleSystemIcons: Boolean
        get() = getBooleanNotNull(PREF_SHOW_RULE_SYSTEM_ICONS, DEFAULT_PREF_SHOW_RULE_SYSTEM_ICONS)
        set(value) = putBoolean(PREF_SHOW_RULE_SYSTEM_ICONS, value)

    var edgeToEdge: Boolean
        get() = getBooleanNotNull(PREF_EDGE_TO_EDGE, DEFAULT_PREF_EDGE_TO_EDGE)
        set(value) = putBoolean(PREF_EDGE_TO_EDGE, value)

    var searchLogIsExpanded: Boolean
        get() = getBooleanNotNull(PREF_SEARCH_LOGS_IS_EXPANDED, DEFAULT_PREF_SEARCH_LOGS_IS_EXPANDED)
        set(value) =  putBoolean(PREF_SEARCH_LOGS_IS_EXPANDED, value)
    var splittermondHomeBrew: Boolean
        get() = getBooleanNotNull(PREF_SPLITTERMOND_HOME_BREW, DEFAULT_PREF_SPLITTERMOND_HOME_BREW)
        set(value) =  putBoolean(PREF_SPLITTERMOND_HOME_BREW, value)

    var enableSchwarzgeld: Boolean
        get() = getBooleanNotNull(PREF_ENABLE_SCHWARZGELD, DEFAULT_PREF_ENABLE_SCHWARZGEL)
        set(value) =  putBoolean(PREF_ENABLE_SCHWARZGELD, value)

    var useStoredCharTrackerFolder: Boolean
        get() = getBooleanNotNull(
            PREF_USE_CHAR_TRACKER_STORED_FOLDER,
            DEFAULT_PREF_USE_CHAR_TRACKER_STORED_FOLDER
        )
        set(value) = putBoolean(PREF_USE_CHAR_TRACKER_STORED_FOLDER, value)

    val activeRuleSystems: List<RuleSystem>
        get() = DatabaseHelper.getInstance(context)?.activeSessions?.map { it.ruleSystem } ?: ArrayList()

    val ruleSystem: RuleSystem
        get() = activeRuleSystems.firstOrNull() ?: rule_system_default

    val rulesOgresClub: Boolean
        get() = RuleSystem.rule_system_ogres_club in activeRuleSystems

    val rulesE5: Boolean
        get() = RuleSystem.rule_system_Dnd_E5 in activeRuleSystems

    val rulesSR5: Boolean
        get() = RuleSystem.rule_system_sr5 in activeRuleSystems

    val rulesSR6: Boolean
        get() = RuleSystem.rule_system_sr6 in activeRuleSystems
    val rulesSkillfull5E: Boolean
        get() = RuleSystem.rule_system_skillfull_dnd_5E in activeRuleSystems

    val rulesSplittermond: Boolean
        get() = RuleSystem.rule_system_splittermond in activeRuleSystems

    val rulesSR: Boolean
        get() = Helper.isShadowrun(activeRuleSystems)

    val rulesDnD: Boolean
        get() = Helper.isDnd(activeRuleSystems)

    var showTotalExp: Boolean
        get() = DatabaseHelper.getInstance(context)?.activeSessions?.map { it.showTotalExp }?.any { it } ?: true
        set(value) {
            DatabaseHelper.getInstance(context)?.let { dbh ->
                dbh.activeSessions.forEach {
                    it.showTotalExp = value
                    dbh.updateSession(it)
                }
            }
        }

    var showCurrentExp: Boolean
        get() = DatabaseHelper.getInstance(context)?.activeSessions?.map { it.showCurrentExp }?.any { it } ?: true
        set(value) {
            DatabaseHelper.getInstance(context)?.let { dbh ->
                dbh.activeSessions.forEach {
                    it.showCurrentExp = value
                    dbh.updateSession(it)
                }
            }
        }

    var charLogMode: Int
        get() = getIntNotNull(PREF_CHAR_LOG_MODE, DEFAULT_PREF_CHAR_LOG_MODE)
        set(value) = putInt(PREF_CHAR_LOG_MODE, value)

    var tagsSortOrder: Int
        get() = getIntNotNull(PREF_TAGS_SORT_ORDER, DEFAULT_PREF_TAGS_SORT_ORDER)
        set(value) =  putInt(PREF_TAGS_SORT_ORDER, value)

    var activeItemCategories: List<String>?
        get() = getString(PREF_ACTIVE_ITEM_CATEGORIES, null)?.split(", ")
        set(value) = putString(PREF_ACTIVE_ITEM_CATEGORIES, value?.joinToString())

    var dialogShowStashUiState: Int
        get() = getIntNotNull(PREF_DIALOG_SHOW_STASH_UI_STATE, DEFAULT_PREF_DIALOG_SHOW_STASH_UI_STATE)
        set(value) = putInt(PREF_DIALOG_SHOW_STASH_UI_STATE, value)

    private val persistedFolder: UriPermission?
        get() = context?.applicationContext?.contentResolver?.persistedUriPermissions?.firstOrNull()

    val localCharTrackerFolder: DocumentFile?
        get() = context?.let {ct ->
            persistedFolder?.let { uriPermission ->
                if (FileUtils.getName(uriPermission.uri).contentEquals(
                        DialogDataOptions.EXPORT_DIRECTORY_FOLDER_NAME,
                        ignoreCase = true)
                    ) {
                    return DocumentFile.fromTreeUri(context,uriPermission.uri)
                }
                FileUtils.getCharTrackerFolder(uriPermission, ct)
            }
        }

    fun getExpandState(data: CharProgressItemData): Boolean {
        context?.let { c ->
            return getBooleanNotNull(
                "${PREF_PREFIX_EXPAND_CPI}_${data.getExpandId(context)}",
                DEFAULT_PREF_PREFIX_EXPAND_CPI
            )
        }
        Log.w("PREFS_HELPER",
            "no context was provided to prefsHelper. cant load expand state for ${data.name}" +
                    "returning default value $DEFAULT_PREF_PREFIX_EXPAND_CPI"
        )
        return DEFAULT_PREF_PREFIX_EXPAND_CPI
    }
    fun setExpandState(data: CharProgressItemData, value: Boolean) {
        context?.let { c ->
            return putBoolean(
                key = "${PREF_PREFIX_EXPAND_CPI}_${data.getExpandId(context)}",
                value = value)
        }
        Log.w("PREFS_HELPER",
            "no context was provided to prefsHelper. cant set expand state for ${data.name}"
        )
    }






    companion object {


        /*
         * for accessing sharedPreferences
         */
        const val MY_SHARED_PREFS = "my_shared_preferences"
        //access preferences for FragmentLogView
        const val PREF_FILTER_SESSIONS = "filter_sessions"
        const val PREF_FILTER_CHARS = "chars"

        const val PREF_SHOW_CHAR_PROGRESSION = "char_progression"
        const val DEFAULT_SHOW_CHAR_PROGRESSION = false

        const val PREF_BACKUP_DB_ON_START = "export_db_on_start"
        const val DEFAULT_PREF_BACKUP_DB_ON_START = false

        const val PREF_PREFIX_EXPAND_CPI = "expand_cpi"
        const val DEFAULT_PREF_PREFIX_EXPAND_CPI = false

        const val PREF_EDGE_TO_EDGE = "edge_to_edge"
        const val DEFAULT_PREF_EDGE_TO_EDGE = true

        const val PREF_XML_IMPORT_OPTION = "xml_import_option"
        const val PREF_XML_IMPORT_DELETE_DB = "xml_import_delete_db"

        const val PREF_XML_EXPORT_OPTION = "xml_export_option"
        const val PREF_CRAWL_OGRES_CLUB = "crawl_feats_ogres_club"

        const val PREF_CHECK_EXP_CALC_MISTAKES = "check_exp_calc_mistake"
        const val DEFAULT_CHECK_EXP_CALC_MISTAKES = true

        const val PREF_FILTER_SEARCH_RESULTS = "filter_search_results"
        const val DEFAULT_PREF_FILTER_SEARCH_RESULTS = true

        const val PREF_SEARCH_SUB_MATCHES = "search_sub_matches"
        const val DEFAULT_PREF_SEARCH_SUB_MATCHES = true

        const val PREF_USE_SEARCH_CONTEXT_SIZE = "use_search_context_size"
        const val DEFAULT_PREF_USE_SEARCH_CONTEXT_SIZE = true

        const val PREF_COLOR_DATE_CHANGES = "color_date_changes"
        const val DEFAULT_PREF_COLOR_DATE_CHANGES = true

        const val PREF_SHOW_RULE_SYSTEM_ICONS = "show_rule_system_icons"
        const val DEFAULT_PREF_SHOW_RULE_SYSTEM_ICONS = true

        const val PREF_SEARCH_LOGS_IS_EXPANDED = "search_logs_is_expanded"
        const val DEFAULT_PREF_SEARCH_LOGS_IS_EXPANDED = false

        const val PREF_SPLITTERMOND_HOME_BREW = "splittermond_home_brew"
        const val DEFAULT_PREF_SPLITTERMOND_HOME_BREW = true

        const val PREF_ENABLE_SCHWARZGELD = "enable_schwarzgeld"
        const val DEFAULT_PREF_ENABLE_SCHWARZGEL = false

        const val PREF_USE_CHAR_TRACKER_STORED_FOLDER = "use_stored_folder"
        const val DEFAULT_PREF_USE_CHAR_TRACKER_STORED_FOLDER = false

        const val PREF_USE_SCHWARZGELD_FOR_WELATH_CALCULATION = "use_schwarzgeld_for_wealth_calculation"
        const val DEFAULT_PREF_USE_SCHWARZGELD_FOR_WELATH_CALCULATION = false

        const val PREF_STATUS_BAR_OFFSET_MAIN_SCREEN = "status_bar_off_set_main_screen"
        const val PREF_STATUS_BAR_OFFSET_MAIN_SCREEN_LAND = "status_bar_off_set_main_screen_landscape"
        const val DEFAULT_STATUS_BAR_OFFSET_MAIN_SCREEN = 50

        const val PREF_STATUS_BAR_OFFSET_CHAR_DETAILS = "status_bar_off_set_char_details"
        const val PREF_STATUS_BAR_OFFSET_CHAR_DETAILS_LAND = "status_bar_off_set_char_details_landscape"
        const val DEFAULT_STATUS_BAR_OFFSET_CHAR_DETAILS = 0

        const val PREF_SESSION_DISPLAY_HEIGHT_CORRECTION = "session_display_height_correction"
        const val PREF_SESSION_DISPLAY_HEIGHT_CORRECTION_LAND = "session_display_height_correction_landscape"
        const val DEFAULT_SESSION_DISPLAY_HEIGHT_CORRECTION = 0

        const val PREF_MAX_NUM_COLS_MAIN_SCREEN = "num_cols_main_screen"
        const val DEFAULT_MAX_NUM_COLS_MAIN_SCREEN = 6

        const val PREF_MAX_NUM_COLS_MAIN_SCREEN_LAND = "num_cols_main_screen_land"
        const val DEFAULT_MAX_NUM_COLS_MAIN_SCREEN_LAND = 12

        const val PREF_NUM_COLS_CHAR_VIEW = "num_cols_char_view"
        const val DEFAULT_NUM_COLS_CHAR_VIEW = 3

        const val PREF_NUM_COLS_CHAR_VIEW_LAND = "num_cols_char_view_land"
        const val DEFAULT_NUM_COLS_CHAR_VIEW_LAND = 6

        const val PREF_SEARCH_CONTEXT_SIZE = "search_context_size"
        const val DEFAULT_SEARCH_CONTEXT_SIZE = 5

        const val PREF_SHOW_PRIORITIES_IN_CHAR_VIEW = "show_priorities_in_char_view"
        const val DEFAULT_SHOW_PRIORITIES_IN_CHAR_VIEW = false

        const val PREF_SHOW_ONLY_ACTIVE_SESSIONS_CHARACTERS_IN_CHAR_VIEW = "show_only_active_sessions_characters_in_char_view"
        const val DEFAULT_SHOW_ONLY_ACTIVE_SESSIONS_CHARACTERS_IN_CHAR_VIEW = false

        const val PREF_SHOW_CHARS_RELATED_TO_SESSION = "show_chars_related_to_session"
        const val DEFAULT_SHOW_CHARS_RELATED_TO_SESSION = true

        const val PREF_VIEW_PAGER_PARAMS = "view_pager_params"
        const val DEFAULT_VIEW_PAGER_PARAMS = "1, 1, 1, 1"

        const val PREF_HG_EXP_PERCENTAGE = "splittermond_hg_exp_percentage"
        const val DEFAULT_PREF_HG_EXP_PERCENTAGE = 100

        const val PREF_FOCUS_PERCENTAGE = "splittermond_fokus_percentage"
        const val DEFAULT_PREF_FOCUS_PERCENTAGE = 200

        const val PREF_SELECTED_DAMAGE_TYPES = "selected_damage_type"
        const val DEFAULT_PREF_SELECTED_DAMAGE_TYPE = 0
        const val EMPTY_HASH_MAP_STRING = "{}"

        const val PREF_DAMAGE_MONITORS_MODI_REPLACE = "damage_monitor_modi"
        const val DEFAULT_PREF_DAMAGE_MONITOR_MODUS_REPLACE = false

        const val PREF_AUTO_UPLOAD_CHAR_ICONS = "auto_upload_custom_icons"
        const val DEFAULT_AUTO_UPLOAD_CHAR_ICONS = true

        const val PREF_AUTO_IMPORT_CHAR_ICONS = "auto_download_custom_icons"
        const val DEFAULT_AUTO_IMPORT_CHAR_ICONS = true

        const val PREF_AUTO_DOWNLOAD_CHAR_ITEMS = "auto_download_char_items"
        const val DEFAULT_AUTO_DOWNLOAD_CHAR_ITEMS = false

        const val PREF_SESSION_STATS_SORT_QUEST = "session_stats_sort_quests"
        const val PREF_SESSION_STATS_SORT_QUESTS_ALPHABETICAL = 0
        const val PREF_SESSION_STATS_SORT_QUESTS_HISTORY = 1
        const val DEFAULT_PREF_SESSION_STATS_SORT_QUEST = PREF_SESSION_STATS_SORT_QUESTS_ALPHABETICAL

        const val PREF_SESSION_STATS_SORT_CHARACTER = "session_stats_sort_characters"
        const val PREF_SESSION_STATS_SORT_CHARACTERS_ALPHABETICAL = 0
        const val PREF_SESSION_STATS_SORT_CHARACTERS_HISTORY = 1
        const val DEFAULT_PREF_SESSION_STATS_SORT_CHARACTER = PREF_SESSION_STATS_SORT_CHARACTERS_ALPHABETICAL

        const val PREF_SESSION_STATS_SORT_PLACE = "session_stats_sort_places"
        const val PREF_SESSION_STATS_SORT_PLACES_ALPHABETICAL = 0
        const val PREF_SESSION_STATS_SORT_PLACES_HISTORY = 1
        const val DEFAULT_PREF_SESSION_STATS_SORT_PLACE = PREF_SESSION_STATS_SORT_PLACES_ALPHABETICAL

        const val PREF_CHAR_LOG_MODE = "char_log_mode"
        const val PREF_CHAR_LOG_MODE_ALL = 0
        const val PREF_CHAR_LOG_MODE_STORY = 1
        const val PREF_CHAR_LOG_MODE_CHAR_PROGRESS_DATA = 2
        const val DEFAULT_PREF_CHAR_LOG_MODE = PREF_CHAR_LOG_MODE_CHAR_PROGRESS_DATA

        const val PREF_TAGS_SORT_ORDER = "tags_sort_order"
        const val PREF_TAGS_SORT_ORDER_ALPHABETICAL = 0
        const val PREF_TAGS_SORT_ORDER_CHRONOLOGICAL = 1
        const val DEFAULT_PREF_TAGS_SORT_ORDER = PREF_TAGS_SORT_ORDER_ALPHABETICAL

        const val PREF_ACTIVE_ITEM_CATEGORIES = "active_item_categories"

        const val PREF_DIALOG_SHOW_STASH_UI_STATE = "dialog_show_stash_ui_state"
        const val PREF_DIALOG_SHOW_STASH_UI_STATE_HIDDEN = 0
        const val PREF_DIALOG_SHOW_STASH_UI_STATE_SHOW_TOGGLES = 1
        const val PREF_DIALOG_SHOW_STASH_UI_STATE_SHOW_CATEGORIES = 2
        const val DEFAULT_PREF_DIALOG_SHOW_STASH_UI_STATE = PREF_DIALOG_SHOW_STASH_UI_STATE_SHOW_TOGGLES




        /**
         * this is used to check if you have spend all you skill points from class/int/race
         */
        const val PREF_CHECK_FEATURED_STATS = "check_featured_skills"
        const val DEFAULT_PREF_CHECK_FEATURED_STATS = true

        const val PREF_CHECK_FEATURED_LANGUAGES = "check_featured_languages"
        const val DEFAULT_PREF_CHECK_FEATURED_LANGUAGES = true

        const val PREF_SHOW_CHAR_NAMES = "show_char_names"
        const val DEFAULT_PREF_SHOW_CHAR_NAMES = false

        const val PREF_CHECK_FILTER_CONSTRAINT_ERRORS = "check_filter_constraint_errors"
        const val DEFAULT_PREF_CHECK_FILTER_CONSTRAINT_ERRORS = true

        const val PREF_SHOW_SESSION_STATS_MAIN_SCREEN = "show_session_stats_main_screen"
        const val PREF_SHOW_SESSION_STATS_CHAR_DETAILS = "show_session_stats_char_details"
        const val DEFAULT_SHOW_SESSION_STATS = true

        const val PREF_DELETE_GENESIS_ON_UPDATE = "delete_genesis_char_on_update"
        const val DEFAULT_DELETE_GENESIS_ON_UPDATE = false

        const val PREF_SHOW_ALL_CHARS_SESSION_STATS = "show_all_chars_session_stats"
        const val DEFAULT_PREF_SHOW_ALL_CHARS_SESSION_STATS = false

        const val PREF_SHOW_CHARS_SESSION_DESCRIPTION = "show_chars_session_description"
        const val DEFAULT_PREF_SHOW_CHARS_SESSION_DESCRIPTION  = false

        const val PREF_SHOW_ALL_PLACES_SESSION_STATS = "show_all_places_session_stats"
        const val DEFAULT_PREF_SHOW_ALL_PLACES_SESSION_STATS = false

        const val PREF_SHOW_PLACES_SESSION_DESCRIPTION = "show_places_session_description"
        const val DEFAULT_PREF_SHOW_PLACES_SESSION_DESCRIPTION  = false

        const val PREF_SHOW_ALL_QUESTS_SESSION_STATS = "show_all_quests_session_stats"
        const val DEFAULT_PREF_SHOW_ALL_QUESTS_SESSION_STATS = false

        const val LANGUAGE_CODE_DE = "de"
        const val LANGUAGE_CODE_EN = "en"
        const val PREF_APP_LANGUAGE = "app_language"
        const val DEFAULT_PREF_APP_LANGUAGE = LANGUAGE_CODE_EN

        private val PREF_EXPAND_CHAR_PROG_ITEM_DICT = "expand_char_progress_item_dict"
        private val DEFAULT_PREF_EXPAND_CHAR_PROG_ITEM_DICT = "{}"


        val ALL_LANGUAGES = listOf(LANGUAGE_CODE_EN, LANGUAGE_CODE_DE)
        /*
         * this is used for dropbox interaction.
         * AccessToken is stored in shared prefs.
         */

        /**
         * include the access token,
         * the refresh token,
         * when the access token expires and
         * the current time to check later if it has already expired.
         */
        const val PREF_DROPBOX_CREDENTIAL = "dropbox_credentials"
        const val DROPBOX_APP_KEY = "kez6fifieuxclti"

        /**
         * this is the same as in manifest the uri scheme to open chartracker from browser
         */
        const val PREF_LOOTMANAGER_TOKEN = "lootmanager_token"
        const val PREF_LOOTMANAGER_DEVICE_ID= "lootmanager_device_id"


        //the references for FILTER_CHARS
        const val FILTER_CHARS_NO_CHARS = 2
        const val FILTER_CHARS_YES = 1
        const val FILTER_CHARS_SHOW_ALL_CHARS = 0

        //the references for CRAWL_OGRES_CLUB
        const val CRAWL_OGRES_CLUB_NEVER = 0
        const val CRAWL_OGRES_CLUB_ON_EACH_START = 2
        const val DEFAULT_CRAWL_OGRES_CLUB = CRAWL_OGRES_CLUB_NEVER


    }
}