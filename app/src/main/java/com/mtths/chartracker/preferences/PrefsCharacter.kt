package com.mtths.chartracker.preferences

import android.content.Context
import com.mtths.chartracker.dataclasses.Character


class PrefsCharacter(
    context: Context?,
    charName: String
):
    PrefsHelperBase(
        context = context,
        SHARED_PREFS_REF = "${Character.SHARED_PREFS_REF}_$charName"
    ) {

    var featured_skill_points_used: Int
        get() = getIntNotNull(PREF_FEATURED_SKILL_POINTS_USED, 0)
        set(value) = putInt(PREF_FEATURED_SKILL_POINTS_USED, value)

    var featured_language_points_used: Int
        get() = getIntNotNull(PREF_FEATURED_LANGUAGE_POINTS_USED, 0)
        set(value) = putInt(PREF_FEATURED_LANGUAGE_POINTS_USED, value)

    var featured_connection_points_used: Int
        get() = getIntNotNull(PREF_FEATURED_CONNECTION_POINTS_USED, 0)
        set(value) = putInt(PREF_FEATURED_CONNECTION_POINTS_USED, value)

    var updateBuffActivityAccordingToItemEquippedState: Boolean
        get() = getBooleanNotNull(PREF_USED_ITEM_EQUIPPED_STATE_FOR_BUFF_ACTIVITY,
            DEFAULT_PREF_USED_ITEM_EQUIPPED_STATE_FOR_BUFF_ACTIVITY)
        set(value) = putBoolean(PREF_USED_ITEM_EQUIPPED_STATE_FOR_BUFF_ACTIVITY, value)

    var applyDamageReductionInDialogAddDamage: Boolean
        get() = getBooleanNotNull(PREF_APPLY_DAMAGE_REDUCTION_IN_DIALOG_ADD_DAMAGE,
            DEFAULT_PREF_APPLY_DAMAGE_REDUCTION_IN_DIALOG_ADD_DAMAGE)
        set(value) = putBoolean(PREF_APPLY_DAMAGE_REDUCTION_IN_DIALOG_ADD_DAMAGE, value)

    companion object {
        const val PREF_FEATURED_SKILL_POINTS_USED = "featured_skill_points_used"
        const val PREF_FEATURED_LANGUAGE_POINTS_USED = "featured_language_points_used"
        const val PREF_FEATURED_CONNECTION_POINTS_USED = "featured_connection_points_used"

        const val PREF_USED_ITEM_EQUIPPED_STATE_FOR_BUFF_ACTIVITY = "use_item_equipped_state_for_buff_activity"
        const val DEFAULT_PREF_USED_ITEM_EQUIPPED_STATE_FOR_BUFF_ACTIVITY = true

        const val PREF_APPLY_DAMAGE_REDUCTION_IN_DIALOG_ADD_DAMAGE= "apply_damage_reduction_in_dialog_add_damage"
        const val DEFAULT_PREF_APPLY_DAMAGE_REDUCTION_IN_DIALOG_ADD_DAMAGE = false
    }


    }