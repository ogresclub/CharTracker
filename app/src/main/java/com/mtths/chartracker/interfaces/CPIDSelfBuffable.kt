package com.mtths.chartracker.interfaces

import android.content.Context
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.Buff
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.utils.StringUtils

/**
 * add companion.REF_ID_SELF_BUFF to autocomplete String of your CPIA
 */
interface CPIDSelfBuffable {

    var relatedLogEntry: LogEntry?
    var buff: Buff

    val totalBuffValue
        get() = buff.effects.sumOf { it.value }

    val buffCalculationString
        get() = buff.effects.joinToString("") {
            " %s %d (%s)".format(
                if (it.value >= 0) "+" else "-",
                it.value,
                it.stat
            )
        }

    fun parseSelfBuff(context: Context) {
        val effects = StringUtils.getListFromText(
            statId = REF_ID_SELF_BUFFS,
            text = relatedLogEntry?.text ?: "",
            context = context
        )
            .map { Buff.parseBuffEffect(it) }


        if (effects.isNotEmpty()) {
            buff = Buff().apply {
                this.effects.addAll(effects)
                modifiable = false
                active = true
                this.logid = relatedLogEntry?.id
            }
        }
    }

    fun addSelfBuffAutoCompleteString(list: MutableList<String>, context: Context) {
        list.add(context.getString(REF_ID_SELF_BUFFS) + " = [")
    }

    companion object {
        val REF_ID_SELF_BUFFS = R.string.label_self_buff


    }
}