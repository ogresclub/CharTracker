package com.mtths.chartracker.interfaces


import android.content.Context
import android.util.Log
import androidx.fragment.app.FragmentManager
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.Character

import com.mtths.chartracker.dataclasses.DamageMonitorData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogDamageMonitor
import com.mtths.chartracker.utils.TranslationHelper
import kotlin.math.max

interface CPIDDamageable {
    val relatedLogEntry: LogEntry?
    val name: String
    val damageableData: List<DamageableData>

    fun getDamage(character: Character?, damageTypeId: Int? = null, context: Context): IntArray? {
        return character?.damages?.get(
            getDamageRef(
                damageTypeId = damageTypeId,
                context = context
            )
        )
    }

    /**
     * @param damageType is only needed if there are multiple monitors
     */
    class DamageableData(val hp: Int, val damageTypeId: Int? = null)

    fun getSrDamageMalus(
        damageTypes: List<Int>,
        item: CharProgressItem?,
        context: Context) = damageTypes.sumOf {
        getDamage(
            character = item?.character,
            damageTypeId = it,
            context = context)?.get(0)?.let {
            max(it / 3, 0)
        } ?: 0
    }

    fun getDamageRefPrefix(): String  {
        return relatedLogEntry?.created_at ?: name
    }

    fun getDamageRef(damageTypeId: Int?, context: Context) =
        getDamageRefPrefix() +
                (damageTypeId?.let {
                    TranslationHelper(context).getEnglishTranslation(damageTypeId)
                        .substringBefore("Damage").trim()
                } ?: "")

    fun getDamageMonitorsData(context: Context): ArrayList<DamageMonitorData> {
        return arrayListOf<DamageMonitorData>().apply {
            addAll(
                damageableData.map {
                    DamageMonitorData(
                        name = context.getString(it.damageTypeId ?: R.string.damage_label) ,
                        damageRef = getDamageRef(it.damageTypeId, context).also {
                            Log.d("DAMAGE_MONITOR", "CPIDDamageable: name = $name, damage ref = $it")
                        },
                        colCount = 3,
                        hp = it.hp
                    )
                }
            )
        }


    }

    fun showDamageMonitor(fm: FragmentManager, context: Context) {
        Log.d("DAMAGE_MONITOR", "showing dialog damage monitor")

        DialogDamageMonitor.newInstance(
            header = name,
            data = getDamageMonitorsData(context),
        ).show(fm, DialogDamageMonitor.TAG)
    }

    companion object {
        const val DAMAGE_TYPE_SEPARATOR = ""
    }
}