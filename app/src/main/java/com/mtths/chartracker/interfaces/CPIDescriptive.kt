package com.mtths.chartracker.interfaces

import android.content.Context
import com.mtths.chartracker.R
import com.mtths.chartracker.utils.StringUtils

interface CPIDescriptive {

    var description: String

    /**
     * parse a char progress items stat which is given as a text
     * (especially potentially including commata) which must be surrounded by []
     * for example description
     */
    fun parseDescription(
        text: String,
        context: Context,
        refId: Int = R.string.description_label) {
        description = StringUtils.parseCPITextStat(
            text = text,
            context = context,
            refId = refId
        )
    }
}