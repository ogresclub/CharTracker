package com.mtths.chartracker.interfaces

import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.StringUtils.getListFromText

interface Moddable {

    val mods: MutableList<String>
    val _REF_MOD: String?
    val REF_MOD: String
        get() = _REF_MOD ?: DEFAULT_REF_MOD
    val modsExpandDataWithHeader: ExpandData
        get() = ExpandData(
            header = REF_MOD,
            text = mods.joinToString("\n", prefix = "\n") {"\t- $it"}
        )
    val modsExpandData: List<ExpandData>
        get() = mods.map {ExpandData(text = "- $it")}

    fun parseMods(text: String) {
        mods.addAll(getListFromText(REF_MOD, text))
    }

    fun addModsAutoCompleteString(l: MutableList<String>) {
        l.add("$REF_MOD = [")
    }

    companion object {
        const val DEFAULT_REF_MOD = "mods"
    }
}