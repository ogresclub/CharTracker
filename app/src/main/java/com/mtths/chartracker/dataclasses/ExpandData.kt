package com.mtths.chartracker.dataclasses

import android.content.Context
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.View
import android.view.View.OnClickListener
import android.view.View.OnLongClickListener
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.mtths.chartracker.R
import com.mtths.chartracker.utils.Helper

open class ExpandData(
    val headerId: Int? = null,
    val header: String = "",
    val text: String,
    val onClickListener: OnClickListener? =  null,
    val onLongClickListener: OnLongClickListener? = null
) {

    open fun getHeader(context: Context) =
        headerId?.let { context.getString(headerId) } ?: header

    open fun displayExpandData(context: Context): SpannableStringBuilder {
        val header = getHeader(context).let { Helper.capitalizeNewsLetterStyle(it) }
        return Helper.append(
            sb = SpannableStringBuilder(),
            text = header,
            what = StyleSpan(Typeface.BOLD),
            flags = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
            .append(if (header.isEmpty()) "" else ": ")
            .append(text)
    }

    open fun getView(activity: FragmentActivity?): View? {

        return TextView(activity).apply {
            text = displayExpandData(context = context)
            layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                marginStart = context.resources.getDimensionPixelSize(
                    R.dimen.cpi_expand_margin_start
                )
            }
            onClickListener?.let {
                setOnClickListener(it)
            }
            onLongClickListener?.let{
                setOnLongClickListener(it)
            }
        }
    }
}