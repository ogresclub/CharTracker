package com.mtths.chartracker.dataclasses

data class Specialization(
    val name: String,
    var level: Int = 1,
    var description: String = "") {
    val expertise: Boolean
        get() = level > 1

    fun clone(): Specialization {
        return Specialization(name = name, level = level, description = description)
    }

    companion object {
        fun groupByLevel(sList: List<Specialization>): Map<Int, List<Specialization>> {
            val map = HashMap<Int, MutableList<Specialization>>()
            sList.forEach {s ->
               map[s.level]?.apply { add(s) } ?: run { map[s.level] = mutableListOf(s) }
            }
            return map
        }
    }
}