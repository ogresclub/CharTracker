package com.mtths.chartracker.dataclasses

import android.database.Cursor
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

data class DndClass(var id: Long = -1,
                    var name: String = "",
                    var link: String = "",
                    var skills_per_lvl: Int = 2,
                    var hpr: Int = 5,
                    /**
                     * saves and bab are level + progression
                     */
                    var progressions: HashMap<String, Int> = HashMap(),
                    var classSkills: List<String> = ArrayList(),
                    var archetype_name: String = "Archetype",
                    /**
                     * this is not filled when loading a character. But when loading a class, it is
                     */
                    var archetype_names: ArrayList<String> = ArrayList()) {


    internal constructor(name: String, link: String = "", skills_per_lvl: Int = 2): this(name = name, link = link, skills_per_lvl = skills_per_lvl, id = -1) {

    }

    override fun toString(): String {
        return name
    }

    fun parseFromCursor(
        cursor: Cursor,
        KEY_ID: String?,
        KEY_NAME: String?,
        KEY_LINK: String?,
        KEY_SKILLS_PER_LEVEL: String?,
        KEY_HIT_POINT_RATE: String?,
        KEY_CLASS_SKILLS: String?,
        KEY_PROGRESSIONS: String?
    ) {
        name =  cursor.getString(cursor.getColumnIndexOrThrow(KEY_NAME))
        id = cursor.getInt(cursor.getColumnIndexOrThrow(KEY_ID)).toLong()
        cursor.getString(cursor.getColumnIndexOrThrow(KEY_LINK))?.let {
            link = it
        }
        skills_per_lvl =
            cursor.getInt(cursor.getColumnIndexOrThrow(KEY_SKILLS_PER_LEVEL))
        hpr =
            cursor.getInt(cursor.getColumnIndexOrThrow(KEY_HIT_POINT_RATE))
        // don't load anything to character.dndclass.archetypes
        cursor.getString(cursor.getColumnIndexOrThrow(KEY_CLASS_SKILLS))?.let {
            classSkills = it.split(",").map { it.trim() }
        }
        cursor.getString(cursor.getColumnIndexOrThrow(KEY_PROGRESSIONS))?.let {
            if (it.isNotBlank()) {
                progressions = Character.parseHashMapStrInt(it)
            }
        }
    }
}