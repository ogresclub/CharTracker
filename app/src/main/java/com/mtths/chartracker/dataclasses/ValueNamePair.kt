package com.mtths.chartracker.dataclasses

import android.util.Log
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterValueBased.Calculator
import java.lang.Integer.max
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

open class ValueNamePair(name: String = "",
                         var value: Int = 0,
                         var hasError: Boolean = false,
                         var mCalculator: Calculator,
                         var description: String = "",
                         var skill: Skill? = null) : CharProgressItemData(name = name) {


    var noCalcErrors = true
    var totalExpSpent = 0
    var augmentation = 0

    /**
     * indicated if the value is meant as relative change or absolut new value
     * this probably shouldn't be stored here, but its convenient...
     */
    var relative: Boolean = false

    /**
     * tells you if i could be that you have put log entries not in correct order.
     * e.g. first int 20 und dann int 18 obwohl für int 18
     */
    var was_reduced = false
    var was_reduced_and_you_even_spend_exp_for_it = false
    var list_of_values = ArrayList<Int>()
    var specializations = ArrayList<Specialization>()

    override val expandData: List<ExpandData>
        get() = super.expandData.toMutableList().apply {
            if (description.isNotBlank()) add(ExpandData(header = "", text = description))
//            val noDescrSpecs = specializations
//                .filter { it.description.isBlank() }
////            if (noDescrSpecs.isNotEmpty()) {
//                add(
//                    ExpandData(
//                        text = noDescrSpecs.joinToString {
//                            it.name + if (it.level > 1) it.level else ""
//                        }
//                    )
//                )
//            }
            addAll(
                specializations
                    .filter { it.description.isNotBlank() }
                    .map { ExpandData(header = it.name, text = it.description) }
            )
        }



//    /**
//     * This constructor is for log entries which don't contain @Value and thus value will be set exp
//     * @param text
//     * @param exp
//     */
//    constructor(text: String, exp: Int, hasError: Boolean): (name = text, value = exp) {
//        noCalcErrors = !hasError
//    }

    init {
        noCalcErrors = !hasError
    }

    /**
     * add specializations and return number of newly added specializations
     */
    data class AddSpecResult(val addedSpecs: Int = 0, val addedExpert: Int = 0)

    fun clone(): ValueNamePair {
        val clone = ValueNamePair(name = name, mCalculator = mCalculator)
        clone.value = value
        clone.noCalcErrors = noCalcErrors
        clone.was_reduced = was_reduced
        clone.was_reduced_and_you_even_spend_exp_for_it = was_reduced_and_you_even_spend_exp_for_it
        clone.list_of_values = ArrayList(2 * list_of_values.size)
        clone.list_of_values.addAll(list_of_values)
        clone.totalExpSpent = totalExpSpent
        clone.specializations.addAll(specializations.map { it.clone() })
        clone.skill = skill
        clone.description = description
        return clone
    }

    /**
     * adds value to list_of_values and if checks if value was reduced and if you spent even exp
     * for it and also if there are calculation errors.
     * @param new_value new value to be added
     * @param ignoreExpCheck if this is true, all the checks will be ignored
     *
     * @return if there is an error in exp calc or the value decreased
     */
    @JvmOverloads
    fun addValue(
        new_value: Int,
        relative: Boolean,
        exp: Int,
        new_specializations: ArrayList<Specialization>,
        /**
         * always overwrite old description
         */
        new_description: String,
        ignoreExpCheck: Boolean = false,
        ruleSystem: RuleSystem): Boolean {


        fun log(msg: String) {
//            Log.e("VNP_ADD_VALUE", "//////////$msg")
        }

        val previous_value = value
        val nValue = if (relative) {
            previous_value + new_value
        } else {
            new_value
        }
        var this_value_was_reduced_and_you_even_spentexp_for_it = false
        var this_value_has_errors = false
        var exp = exp

        if (!ignoreExpCheck) {
            val value_increased = nValue > previous_value
            val value_decreased = nValue < previous_value
            if (value_decreased) {
                was_reduced = true
                if (exp > 0) {
                    was_reduced_and_you_even_spend_exp_for_it = true
                    this_value_was_reduced_and_you_even_spentexp_for_it = true
                }
            }

            //first remove specializations cost from exp
            exp -= mCalculator.getSpecializationCost(
                newSpecialization = new_specializations,
                oldSpecializations = specializations)

            if (exp > 0 && value_increased) {
                if (exp != mCalculator.getExpCost(previous_value, nValue, list_of_values)) {
                    noCalcErrors = false
                    this_value_has_errors = true
                }
            }
        }

        val res = addSpecializations(
            new_specializations = new_specializations,
            mSpecializations = specializations,
            ruleSystem = ruleSystem)
        val newSpecsAdded = res.addedSpecs
        val newExpertAdded = res.addedExpert
        log("added number of new specializations to skill $name: $newSpecsAdded")
        log("added number of new expertises to skill $name: $newExpertAdded")

        description = new_description
        value = nValue
        list_of_values.add(nValue)
        log( "ValueNamePair.Add: " + name + " old/new value: " + previous_value + "/" + nValue +
                " exp: " + exp + " expected exp: " + mCalculator.getExpCost(previous_value, nValue, list_of_values))


        log("this value has errors = " + this_value_has_errors);
        log("this value was reduced and you even spend exp for it = " + this_value_was_reduced_and_you_even_spentexp_for_it);

        return ignoreExpCheck || !this_value_has_errors && !this_value_was_reduced_and_you_even_spentexp_for_it
    }

    companion object {

        val PAIR_INTEGER_STRING__ATTRIBUTE_COMPARATOR_DND: Comparator<ValueNamePair> = object : Comparator<ValueNamePair> {
            override fun compare(o1: ValueNamePair, o2: ValueNamePair): Int {
                return ReplaceDnDAttributesToSortThemCorrectly(o1.name)
                    .compareTo(
                        ReplaceDnDAttributesToSortThemCorrectly(o2.name)
                    )
            }
        }
        val PAIR_INTEGER_STRING__ATTRIBUTE_COMPARATOR_SR: Comparator<ValueNamePair> = object : Comparator<ValueNamePair> {
            override fun compare(o1: ValueNamePair, o2: ValueNamePair): Int {
                return ReplaceSRAttributesToSortThemCorrectly(o1.name)
                    .compareTo(
                        ReplaceSRAttributesToSortThemCorrectly(o2.name)
                    )
            }
        }

        const val REF_DESCRIPTION = "description"

        fun ReplaceDnDAttributesToSortThemCorrectly(s: String): String {
            return s.replace(Regex("(?i)" + Pattern.quote("Strength")), "A")
                .replace(Regex("(?i)" + Pattern.quote("Dexterity")), "AA")
                .replace(Regex("(?i)" + Pattern.quote("Constitution")), "AAA")
                .replace(Regex("(?i)" + Pattern.quote("Intelligence")), "AAAA")
                .replace(Regex("(?i)" + Pattern.quote("Wisdom")), "AAAAA")
                .replace(Regex("(?i)" + Pattern.quote("Charisma")), "AAAAAA")
        }
        fun ReplaceSRAttributesToSortThemCorrectly(s: String): String {
            return s.replace(Regex("(?i)" + Pattern.quote("Body")), "A")
                .replace(Regex("(?i)" + Pattern.quote("Agility")), "AA")
                .replace(Regex("(?i)" + Pattern.quote("Reaction")), "AAA")
                .replace(Regex("(?i)" + Pattern.quote("Strength")), "AAAA")
                .replace(Regex("(?i)" + Pattern.quote("Willpower")), "AAAAA")
                .replace(Regex("(?i)" + Pattern.quote("Logic")), "AAAAAA")
                .replace(Regex("(?i)" + Pattern.quote("Intuition")), "AAAAAAA")
                .replace(Regex("(?i)" + Pattern.quote("Charisma")), "AAAAAAAA")
                .replace(Regex("(?i)" + Pattern.quote("Magic")), "AAAAAAAAA")
                .replace(Regex("(?i)" + Pattern.quote("Resonance")), "AAAAAAAAAA")
                .replace(Regex("(?i)" + Pattern.quote("Res")), "AAAAAAAAAAA")
                .replace(Regex("(?i)" + Pattern.quote("Edge")), "AAAAAAAAAAAA")
        }

        fun addSpecializations(
            new_specializations: ArrayList<Specialization>,
            mSpecializations: ArrayList<Specialization>,
            ruleSystem: RuleSystem): AddSpecResult {
            var addedSpecs = 0
            var addedExpert = 0
            if (ruleSystem == RuleSystem.rule_system_splittermond) {
                for (s in new_specializations) {
                    mSpecializations.find { it.name.equals(s.name, ignoreCase = true) && it.level == s.level }?.also {
                        // if already exists and no desctiption is available, take descr of new spec
                        if (it.description.isBlank()) {
                            it.description = s.description
                        }
                    }
                        ?: run {
                        mSpecializations.add(s)
                        addedSpecs++
                    }
                    mSpecializations.sortWith (compareBy<Specialization> { it.name }.thenBy { it.level } )
                }
            } else {
                for (s in new_specializations) {
                    mSpecializations.find { it.name.equals(s.name, ignoreCase = true) }
                        ?.let { mSpec ->
                            if (mSpec.level == 1 && mSpec.level < s.level) {
                                addedExpert++
                            }
                            mSpec.level = max(s.level, mSpec.level)
                        } ?: run {
                        mSpecializations.add(s)
                        addedSpecs++
                    }
                    mSpecializations.sortBy { it.name }
                }
            }
            return AddSpecResult(addedSpecs = addedSpecs, addedExpert = addedExpert)
        }
    }
}