package com.mtths.chartracker.dataclasses

import android.content.Context
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterOneLineExpandable
import com.mtths.chartracker.utils.Helper
import java.util.HashMap

open class CharProgressItemData(var name: String = "") {
    var relatedLogEntry: LogEntry? = null
    open val expandData: List<ExpandData> = listOf()
    var expanded: Boolean = false
    var viewHolder: ViewHolder = ViewHolder()

    open fun parseName(logEntryText: String, item: CharProgressItem?) {
        name = Helper.parseName(logEntryText = logEntryText, item = item)
    }

    fun getExpandId(context: Context): Int {
        val idString = name + expandData.joinToString { "%s::%s".format(it.getHeader(context),it.text)}
        return idString.hashCode()
    }

    class ViewHolder(
        var columnsHolder: CharProgressItemAdapterOneLineExpandable.ViewHolder? = null,
        var expandHolder: HashMap<String, View> = HashMap()
    )
}