package com.mtths.chartracker.dataclasses

import android.content.Context
import android.content.res.Resources
import android.util.Log
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.interfaces.CPIDSelfBuffable
import com.mtths.chartracker.interfaces.CPIDescriptive
import com.mtths.chartracker.utils.StringUtils.getCPIData
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getListFromText
import com.mtths.chartracker.utils.StringUtils.parseCPITextStat

class SpellSplittermond(
    name: String = "",
    var level: Int = DEFAULT_INTEGER,
    var school: String = "",
    var focus: String = "",
    var types: List<String> = listOf(),
    var difficulty: String = "",
    var castDuration: String = "",
    var castRange: String = "",
    var spellDuration: String = "",
    var enhancement: String = "",
    var page: String = "",
    var genesisId: String = "",
    override var description: String = "",
    var enhancementDescription: String = "",
    var enhancementOptions: List<String> = listOf(),
    /**
     * I only need this to display the spell school in expand data,
     * when character settings displaySpellSchool is false
     *
     * or in general when some expand behaviour depends on character settings
     */
    var character: Character? = null
    ) :
    CharProgressItemData(name = name),
    CPIDescriptive,
    CPIDSelfBuffable {

    constructor(a: Array<String>) : this(
        name = a[0],
        types = a[1].split(",").map { it.trim() }
    )

    override var buff: Buff = Buff()

    val ref: String
        get() = getSpellRef(name)

    fun getCheck(activity: ProgressBarFrameDropBoxActivity): Int {
        return Skill.getSkillValue(
            skillName = school,
            character = character,
            context = activity
        ) + totalBuffValue + getSpecBonus(activity)
    }

    fun getSpecs(activity: ProgressBarFrameDropBoxActivity): List<Specialization>? {
        val skill = (activity.charProgressItemsMap.get(
            R.string.filter_constraint_skill
        )?.mainItem?.adapter?.data?.find {
            (it as? ValueNamePair?)?.name?.equals(school, ignoreCase = true) ?: false
        } as? ValueNamePair?)

         return skill?.specializations?.filter {
                it.name.lowercase() in types.map { it.lowercase() }
            }
    }

    fun getSpecBonus(activity: ProgressBarFrameDropBoxActivity): Int =
        getSpecs(activity)?.maxOfOrNull { it.level } ?: 0


    val replaceNameDict = HashMap<String, String>().apply {
        put("Geister rufen I", "Geister rufen I (Grundregelwerk)")
        put("Felswesen rufen 1", "Felswesen rufen I (Grundregelwerk)")
        put("Aura der Kontermagie", "Aura der Kontramagie")
        put("Beschwörer stören", "Beschwörer Stören")
        put("Einstellung spüren", "Einstellung erspüren")
        put("Emotionen spüren", "Emotionen erspüren")
        put("Holz verstärken", "Holz Verstärken")
        put("Höherer Feebann", "Höherer Feenbann")
        put("Licht", "Licht (Zauber)")
        put("Prophezeiung", "Prophezeiung (Zauber)")
        put("Rüstung stärken", "Rüstung Stärken")
        put("Schattenwand", "Schattenwand (Zauber)")
        put("Verdunkelung", "Verdunklung")
    }

    fun extractFocusCosts(input: String): Map<String, Int> {
        val focusCosts = mutableMapOf("verzehrt" to 0, "kanalisiert" to 0, "erschöpft" to 0)

        // Regex to match optional total cost, K, and V
        val regex = Regex("(\\d+)?(?:K(\\d+))?(?:V(\\d+))?")

        // Match the input string
        val matchResult = regex.find(input)

        matchResult?.let {
            // Parse total cost, channeled, and consumed values
            val totalCost = it.groupValues[1].toIntOrNull()
            val channeled = it.groupValues[2].toIntOrNull() ?: 0
            val consumed = it.groupValues[3].toIntOrNull() ?: 0

            if (totalCost != null) {
                // If total cost is explicitly provided
                focusCosts["kanalisiert"] = channeled
                focusCosts["verzehrt"] = consumed
                focusCosts["erschöpft"] = totalCost - channeled - consumed
            } else {

                focusCosts["verzehrt"] = consumed
                focusCosts["kanalisiert"] = channeled - consumed
                focusCosts["erschöpft"] = 0 // No exhaustion when total is inferred
            }
        }

        return focusCosts
    }

    fun extractEnhanceFocusCosts(input: String): Map<String, Int> {
        val focusString = input.substringBefore(":")
            .substringAfter("/")
            .substringAfter("+")

        return extractFocusCosts(focusString)
    }

    fun getLink(context: Context): String {
        var name = getSpell(name = name, context = context)?.name
        name = replaceNameDict.getOrDefault(name, defaultValue = name) ?: this.name

        return SPELL_URL_TEMPLATE_SPLITTERMOND +
                name
                    .replace("(Ritus)", "")
                    .replace("(Spruch)", "")
                    .replace("ae", "ä") // Staehlerner Schild
                    .trim()
                    .replace(" ", "_")
                    .replace("-", "_")

    }


    override fun parseName(logEntryText: String, item: CharProgressItem?) {
        super.parseName(logEntryText, item)
        name = name.replace("’", "")
    }

    /**
     * parse the school from the string or check if spell with name exists in spellMap
     * and use its school
     */
    fun parseSchool(s: String, spellMap: Map<String, SpellSplittermond>, context: Context) {
        school = (getCPIData(
            dataNameId = REF_ID_SCHOOL,
            text = s,
            context = context) ?: "").ifEmpty {
            getSpell(name, spellMap)?.school ?: ""
        }
    }

    fun parseTypes(s: String, spellMap: Map<String, SpellSplittermond>, context: Context) {
        types = (getListFromText(
            statId = REF_ID_TYPES,
            text = s,
            context = context)).ifEmpty {
            getSpell(name, spellMap)?.types ?: emptyList()
        }
        Log.d("SPLITTERMOND_TYP", "parsed type for spell $name: $types")
    }

    /**
     * parse the level from the string or check if spell with name exists in resources
     * and use its level
     */
    fun parseLevel(s: String, context: Context) {
        level = getIntegerStatFromText(statId = REF_ID_LEVEL, text = s,context = context).toIntOrNull()
            ?: getSpell(name, context)?.level
                    ?: 0
    }

    fun parseCastDuration(s: String, context: Context) {
        getCPIData(dataNameId = REF_ID_CAST_DURATION, text = s, context = context)?.let {
            castDuration = it
        }
    }
    fun parseDifficulty(s: String, context: Context) {
        getCPIData(dataNameId = REF_ID_DIFFICULTY, text = s, context = context)?.let {
            difficulty = it
        }
    }
    fun parseCastRange(s: String, context: Context) {
        getCPIData(dataNameId = REF_ID_CAST_RANGE, text = s, context = context)?.let {
            castRange = it
        }
    }
    fun parseSpellDuration(s: String, context: Context) {
        getCPIData(dataNameId = REF_ID_SPELL_DURATION, text = s, context = context)?.let {
            spellDuration = it
        }
    }
    fun parseEnhancement(s: String, context: Context) {
        getCPIData(dataNameId = REF_ID_ENHANCEMENT, text = s, context = context)?.let {
            enhancement = it
        }
    }
    fun parsePage(s: String, context: Context) {
        getCPIData(dataNameId = REF_ID_PAGE, text = s, context = context)?.let {
            page = it
        }
    }
    fun parseGenesisId(s: String, context: Context) {
        getCPIData(dataNameId = REF_ID_GENESIS_ID, text = s, context = context)?.let {
            genesisId = it
        }
    }
    fun parseFocusCost(s: String, context: Context) {
        getCPIData(dataNameId = REF_ID_FOCUS_COST, text = s, context = context)?.let {
            focus = it
        }
    }
    fun parseEnhancementDescription(s: String, context: Context) {
        parseCPITextStat(
            refId = REF_ID_ENHANCEMENT_DESCRIPTION,
            text = s,
            context = context).let {
            enhancementDescription = it
        }
    }
    fun parseEnhancementOptions(s: String, context: Context) {
        getListFromText(statId = REF_ID_ENHANCEMENT_OPTIONS, text = s, context = context).let {
            enhancementOptions = it
        }
    }


    /**
     * parse the level from the string or check if spell with name exists in spellmap
     * and use its level
     */
    fun parseLevel(
        s: String,
        spellMap: Map<String, SpellSplittermond>,
        context: Context) {
        level = getIntegerStatFromText(
            statId = REF_ID_LEVEL, text = s, context = context
        ).toIntOrNull()
            ?: getSpell(name, spellMap)?.level
                    ?: 0
    }

    fun getSpecCalculationString(activity: ProgressBarFrameDropBoxActivity): String {
        return " + %d (%s)".format(
            getSpecBonus(activity),
            getSpecs(activity)?.maxByOrNull { it.level }?.name

        )
    }

    override val expandData: List<ExpandData>
        get() = super.expandData.toMutableList().apply {
            if (character?.showSpellSchools != true) {
                add(ExpandData(headerId = REF_ID_SCHOOL, text = school))
            }
            Log.d("SPLITTERMOND_TYP", "spell: $name/$ref; typus = '$types'")
            if (types.isNotEmpty()) add(
                ExpandData(
                    headerId = REF_ID_TYPES,
                    text = types.joinToString()
                )
            )
            if (difficulty != "") add(
                ExpandData(
                    headerId = REF_ID_DIFFICULTY,
                    text = difficulty)
            )
            if (focus != "") add(
                ExpandData(
                    headerId = REF_ID_FOCUS_COST,
                    text = focus)
            )
            if (castDuration != "") add(
                ExpandData(
                    headerId = REF_ID_CAST_DURATION,
                    text = castDuration)
            )
            if (castRange != "") add(
                ExpandData(
                    headerId = REF_ID_CAST_RANGE,
                    text = castRange)
            )
            if (spellDuration != "") add(
                ExpandData(
                    headerId = REF_ID_SPELL_DURATION,
                    text = spellDuration)
            )
            if (description != "") add(
                ExpandData(
                    headerId = REF_ID_DESCRIPTION,
                    text = description)
            )
            var erfolgsgradeText = "\n\t- $enhancement: $enhancementDescription"
            if (enhancementOptions.isNotEmpty()) {
                erfolgsgradeText += "\n\t- ${enhancementOptions.joinToString()}"
            }
            if (enhancement != "") add(
                ExpandData(
                    headerId = HEADER_ID_ERFOLGSGRADE,
                    text = erfolgsgradeText)
            )

            if (page != "") add(
                ExpandData(
                    headerId = REF_ID_PAGE,
                    text = page)
            )
        }

    companion object {
        val REF_ID_LEVEL = R.string.label_spell_level
        val REF_ID_SCHOOL = R.string.label_school
        val REF_ID_TYPES = R.string.cpi_ref_spell_splittermond_type
        val REF_ID_DIFFICULTY = R.string.label_difficulty_level
        val REF_ID_FOCUS_COST = R.string.cpi_ref_spell_splittermond_focus
        val REF_ID_CAST_DURATION = R.string.cpi_ref_spell_splittermond_cast_duration
        val REF_ID_CAST_RANGE = R.string.range_label
        val REF_ID_SPELL_DURATION = R.string.cpi_ref_spell_splittermond_spell_duration
        val REF_ID_ENHANCEMENT = R.string.cpi_ref_spell_splittermond_enhancement
        val REF_ID_PAGE = R.string.label_page
        val REF_ID_DESCRIPTION = R.string.cpi_ref_spell_splittermond_description
        val REF_ID_ENHANCEMENT_DESCRIPTION = R.string.cpi_ref_spell_splittermond_enhancement_description
        val REF_ID_ENHANCEMENT_OPTIONS = R.string.cpi_ref_spell_splittermond_enhancement_options
        val REF_ID_GENESIS_ID = R.string.lebel_genesis_id
        val SPELL_URL_TEMPLATE_SPLITTERMOND = "https://splitterwiki.de/wiki/"
        val DEFAULT_INTEGER = 0
        val HEADER_ID_ERFOLGSGRADE = R.string.cpi_header_spell_splittermond_enhancement

        fun getSpellMap(context: Context): HashMap<String, SpellSplittermond> {
            val map = HashMap<String, SpellSplittermond>()
            val res: Resources = context.resources
            val ta = res.obtainTypedArray(R.array.spells_splittermond)
            val n = ta.length()
            for (i in 0 until n) {
                val id = ta.getResourceId(i, 0)
                if (id > 0) {
                    val spell = SpellSplittermond(res.getStringArray(id))
//                    Log.e("PRASING_SPELLS","}}}}}}}}}}}}}}}}}loaded Spell: ${spell.name} // ${spell.ref}; ${spell.types}")
                    map[spell.ref] = spell
                } else {
                    Log.e("PARSING_SKILLS", "something wrong with spell at position $i")
                }
            }
            ta.recycle() // Important!
            return map
        }

        fun getSpell(name: String, spells: Map<String, SpellSplittermond>): SpellSplittermond? {
            return spells[getSpellRef(name)]
        }

        fun getSpell(name: String, context: Context): SpellSplittermond? {
            return getSpellMap(context)[getSpellRef(name)]
        }

        fun getSpellRef(sName: String): String {
            return sName
                .lowercase()
                .trim()
                .replace(Regex("\\s"), "_")
                .replace("’", "")
                .replace("'", "")
        }
    }
}