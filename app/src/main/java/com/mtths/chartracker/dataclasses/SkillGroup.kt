package com.mtths.chartracker.dataclasses

class SkillGroup(
    name: String = "",
    var skills: List<String> = listOf() ,
    var hasError: Boolean = false
) : CharProgressItemData(name =  name) {


        fun fromArray(data: Array<String>): SkillGroup {
            val sg = SkillGroup()
            sg.name = data[0]
            if (data.size > 1) {
                sg.skills = data.drop(1)
            }
            sg.hasError = false
            return sg
        }

}