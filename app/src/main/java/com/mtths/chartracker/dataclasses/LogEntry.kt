package com.mtths.chartracker.dataclasses

import android.content.Context
import android.util.Log
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.Global
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterGear.Companion.AMOUNT_REF
import com.mtths.chartracker.utils.Helper
import java.text.ParseException
import java.util.*


/**
 * A simple class to Store a Log Entry of a char consisting of
 * the amount of exp gained
 * text associated to it
 * a listOfViews of "affected" char (the Entry was written in their Log)
 * a listOfViews of tags
 */
// todo die ids sollten nicht null sein dürfen
data class LogEntry (
    var id: Long = -1,
    var exp: Int = 0,
    var text: String = "",
    var character_ids: ArrayList<Long>? = ArrayList(),
    var tag_ids: ArrayList<Long>? = ArrayList(),
    var session_ids: ArrayList<Long>? = ArrayList(),
    var created_at // format: "dd-MM-yyyy HH:mm:ss"
    : String = "",
    var last_edit_date // format: "dd-MM-yyyy HH:mm:ss"
    : String = "",

    /**
     * this is used in charDetails to display this entry with red background if an exp error occurred
     */
    var hasError: Boolean = false) {

    //// VARIABLES /////////////////////////////////////////////////////////////////////////////////

    private val SPACE = "  " //the string added between exp and text in the displayed.
    private val TAG_REF = 0
    private val CHARACTER_REF = 1
    private val SESSION_REF = 2


    constructor(e: LogEntry) : this(e.id, e.exp, e.text, e.character_ids, e.tag_ids, e.session_ids)


    /**
     * format: dd-MM-yyyy HH:mm:ss
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param min
     * @param sec
     */
    fun setDateofCreation(year: Int, month: Int, day: Int, hour: Int, min: Int, sec: Int) {
        val new_date = Date(year, month, day, hour, min, sec)
        dateOfCreation = new_date
    }

    var dateOfCreation: Date?
        get() {
            var result: Date? = null
            try {
                result = Global.MY_DATE_FORMAT.parse(created_at)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return result
        }
        set(new_date) {
            new_date?.let { created_at = Global.MY_DATE_FORMAT.format(it) } ?:
            Log.e("SET_CHAR_DATE", "cannot set date, date was null")
        }

    val year: Int
        get() = Integer.valueOf(created_at.substring(6, 10))

    val month: Int
        get() = Integer.valueOf(created_at.substring(3, 5))

    val day: Int
        get() = Integer.valueOf(created_at.substring(0, 2))

    val hour: Int
        get() = Integer.valueOf(created_at.substring(11, 13))

    val minute: Int
        get() = Integer.valueOf(created_at.substring(14, 16))

    val second: Int
        get() = Integer.valueOf(created_at.substring(17, 19))
    ///// TOSTRING METHODS ///////////////////////////////////////////////////////////////////////////
    /**
     * returns the log Entry as it would be written in someones journal.
     * Not affected characters or tags included.
     *
     * @return exp & text
     */
    fun toString_ExpText(): String { //if exp or text field is empty, don't add space when concatenating both strings
        return if (text.length == 0 && exp == 0) {
            ""
        } else if (text.length == 0) {
            exp.toString()
        } else if (exp == 0) {
            text
        } else {
            exp.toString() + SPACE + text
        }
    }

    fun toString_CharsExpText(context: Context?): String {
        val dbHelper = DatabaseHelper.getInstance(context)
        val charNames = toString_chars(dbHelper!!)
        return if (charNames.length == 0) {
            toString_ExpText()
        } else {
            toString_chars(dbHelper) + ": " + toString_ExpText()
        }
    }

    fun toString_DateCharsTagsExpText(context: Context?): String { //        Helper.log("TOTAL TO STRING START:");
        return created_at + " " + toString_CharTagsExpText(context)
    }

    fun toString_SessionCharsExpText(context: Context?): String {
        val dbHelper = DatabaseHelper.getInstance(context)
        var charNames = toString_chars(dbHelper!!)
        var sessions = toString_sessions(dbHelper)
        if (charNames != "") {
            charNames += ": "
        }
        if (!sessions.equals("", ignoreCase = true)) {
            sessions += "; "
        }
        return sessions + charNames + toString_ExpText()
    }

    fun toString_CharTagsExpText(context: Context?): String { //        Helper.log("TOTAL TO STRING NO DATE START:");
        val dbHelper = DatabaseHelper.getInstance(context)
        val charNames = toString_chars(dbHelper!!)
        return if (charNames == "") {
            toString_ExpText()
        } else {
            charNames + ": " + toString_tags(dbHelper, null) + " " + toString_ExpText()
        }
    }

    fun toStringTagsExpText(context: Context?, excludedTags: ArrayList<String>?): String {
        val dbHelper = DatabaseHelper.getInstance(context)
        //        //debugging -----------------------------------
//        if (excludedTags == null) {
//            Helper.log("TOTAL TO STRING WITH TAGS START: Tags:");
//        } else {
//            Helper.log("TOTAL TO STRING WITH TAGS START: (excluded tags " + excludedTags.toString_ExpText() + ")");
//        }
//        //-----------------------------------------------
        return toString_tags(dbHelper!!, excludedTags) + " " + toString_ExpText()
    }

    //////////////////////////////// PRIVATE METHODS //////////////////////////////////////////////
    private fun toString_chars(dbHelper: DatabaseHelper): String { //Create string to display associated charNames
        var charNames = ""
        for (id in character_ids!!) {
            val charName = dbHelper.getCharacterName(id)
            charNames = "$charNames$charName "
        }
        return charNames.trim { it <= ' ' }
    }

    private fun toString_tags(dbHelper: DatabaseHelper, excludedTags: ArrayList<String>?): String { //Create string to display associated tags
        var tags = ""
        for (id in tag_ids!!) {
            val tag = dbHelper.getTagName(id)
            if (!Helper.arrayContainsElementIgnoringCases(excludedTags, tag)) {
                tags = "$tags#$tag "
            }
        }
        return tags.trim { it <= ' ' }
    }

    private fun toString_sessions(dbHelper: DatabaseHelper): String { //Create string to display associated sessions
        var sessions = ""
        for (id in session_ids!!) {
            val session = dbHelper.getSessionName(id)
            sessions = "$sessions$$session "
        }
        return sessions.trim()
    }

    //////////////////// HELP METHODS //////////////////////////////////////////////////////////
    fun addCharacter(id_char: Long) {
        character_ids!!.add(id_char)
    }

    /**
     * get the names of tags related to log entry
     *
     * @return a listOfViews with the names of all tags related to the log Entry
     */
    fun getTagNames(dbHelper: DatabaseHelper?, withHastTag: Boolean): ArrayList<String> {
        return if (withHastTag) {
            val result = ArrayList<String>()
            for (tagName in Helper.getNames(dbHelper!!, tag_ids!!, TAG_REF)) {
                result.add("#$tagName")
            }
            result
        } else {
            Helper.getNames(dbHelper!!, tag_ids!!, TAG_REF)
        }
    }

    /**
     * get the names of chars related to log entry
     *
     * @return a listOfViews with the names of all characters related to the log entry
     */
    fun getCharNames(dbHelper: DatabaseHelper?): ArrayList<String> {
        return Helper.getNames(dbHelper!!, character_ids!!, CHARACTER_REF)
    }

    /**
     * @return the names of all sessions related to to entry
     */
    fun getSessionNames(dbHelper: DatabaseHelper?): ArrayList<String> {
        return if (session_ids != null) {
            Helper.getNames(dbHelper!!, session_ids!!, SESSION_REF)
        } else {
            ArrayList()
        }
    }

    fun getCharacters(dbHelper: DatabaseHelper): ArrayList<Character> {
        val chars = ArrayList<Character>()
        for (id in character_ids!!) {
            chars.add(dbHelper.getCharacter(id))
        }
        return chars
    }

    companion object {
        //////////////////////////////////////// COMPARATORS //////////////////////////////////////
        val CREATION_DATE_COMPARATOR: Comparator<LogEntry> =
            Comparator<LogEntry> { e0, e1 ->
                val nullDate = Date(1900,0,0)

                val date0 = e0.dateOfCreation ?: nullDate
                val date1 = e1.dateOfCreation ?: nullDate
                date1.compareTo(date0)
            }



        fun updateAmountInLogEntry(
            oldAmount: Int,
            newAmount: Int,
            context: Context,
            logEntryWithAmountString: LogEntry?,
            amountRef: String) {
            logEntryWithAmountString?.let { e ->

                val toBeReplaced = Regex("$amountRef\\s*=\\s*$oldAmount")
                val replacer = "$amountRef = $newAmount"
                Log.e("UPDATE_AMOUNT", "log text: '${e.text}'")
                Log.e("UPDATE_AMOUNT", "log text: contains $amountRef = $oldAmount: ${e.text.contains(toBeReplaced)}")

                if (e.text.contains(toBeReplaced)) {
                    e.text = e.text.replace(toBeReplaced, replacer)
                } else {
                    e.text += ", $replacer"
                }
                DatabaseHelper.getInstance(context)?.updateLogEntry(e, keep_last_update_date = false)
                Helper.update(Global.allLogs, e)
                Helper.update(Global.logsFilteredBySession, e)
                Helper.update(Global.logsToShow, e)
            }

        }
    }



    ///// CONSTRUCTORS /////////////////////////////////////////////////////////////////////////////
    init {
        this.text = text.trim()
    }
}

