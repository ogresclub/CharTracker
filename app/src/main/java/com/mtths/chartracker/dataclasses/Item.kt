package com.mtths.chartracker

import android.content.Context
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import androidx.core.content.ContextCompat
import com.mtths.chartracker.utils.Helper.append
import com.mtths.chartracker.dataclasses.ItemCategory.Companion.extendedWalletCategories
import com.mtths.chartracker.dataclasses.ItemCategory.Companion.walletCategories
import com.mtths.chartracker.utils.Helper
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

data class UserWithAmount(var name: String, var amount: Int)

data class Stash(var name: String, var manager: String)

data class Item(
    var id: Int = -1,
    var amount: Int = 0,
    var category: String = "",
    var charges: Int = -1,
    var cursed: Boolean = false,
    var description: String = "",
    var enchantment: String = "",
    var buffs: String = "",
    var enhancement: Int = 0,
    var level: Int = -1,
    var name: String = "",
    var typ: String = "",
    var value: Float = 0f,
    var equipped: Boolean = true,
    var marked: Boolean = false,
    var goneMsg: String = "",
    var onlyLocal: Boolean = false,
    var stashes: MutableList<Stash> = mutableListOf(),
    var users: MutableList<UserWithAmount> = mutableListOf()
) {

    val isAvailable: Boolean
        get() =  numAvailable() > 0

    fun getLevelString(): String {
        return if (level > -1) "grad $level " else ""
    }

    fun getValueString(): String {
        return when {
            value == 0f -> ""
            abs(value) < 1 && abs(value * 10) < 1  -> String.format(Locale.GERMAN,"$%.2f ", value)
            abs(value) < 1 && abs(value * 10) >= 1  -> String.format(Locale.GERMAN,"$%.1f ", value)
            else -> String.format(Locale.GERMAN,"$%,d ", value.toInt())
        }
    }

    fun getCursedString(): String {
        return if (cursed) "(CURSED) " else ""
    }

    fun getAmountsString(showTotalAmount: Boolean = true): String {
        if (showTotalAmount) {
            if (numAvailable() != amount) {
                return String.format(Locale.GERMAN, AMOUNT_TEMPLATE, numAvailable(), amount)
            }
        }
        return if (numAvailable() != 1) String.format(Locale.GERMAN,"%,d ", numAvailable()) else ""

    }

    fun getNameString(): String {
        return if (name.isNotBlank()) "$name " else ""
    }

    fun getChargesString(): String {
        return if (charges > -1) "($charges) " else ""
    }

    fun getTypeString(): String {
        return if (typ.isNotBlank()) "$typ " else ""
    }

    fun getEnhancementString(): String {
        return if (enhancement > 0) "+$enhancement " else ""
    }

    fun getEnchantmentString(): String {
        return if (enchantment.isNotBlank()) "$enchantment " else ""
    }
    fun getBuffString(): String {
        return if (buffs.isNotBlank()) "[$buffs] " else ""
    }

    fun getCategoryString(): String {
        return "$category "
    }

    fun getGoneMsgString(): String {
        return if (isGone()) "$goneMsg " else ""
    }

    fun getDescriptionString(): String {
        return if (description.isNotBlank()) "$description " else ""
    }

    override fun toString(): String {
        var s = ""
        s += getCategoryString()
        s += getLevelString()
        s += getCursedString()
        s += getAmountsString()
        s += getNameString()
        s += getChargesString()
        s += getTypeString()
        s += getEnhancementString()
        s += getEnchantmentString()
        s += getDescriptionString()
        s += getBuffString()
        s += getGoneMsgString()
        s += getValueString()
        s += stashes.joinToString { it.name }
        return s.trim()
    }

    fun changeEquipped() {
        equipped = !equipped
    }

    val buffName: String
        get() = name.ifBlank { typ.ifBlank { "Item" } }

    fun isGone(): Boolean {
        return goneMsg.isNotBlank()
    }

    fun getSpannableString(
        expanded: Boolean,
        context: Context,
        lineHeight: Int,
        showCategory: Boolean = true,
        showAll: Boolean = false): SpannableStringBuilder {
        val  sb =  if (expanded) {
            toStringLong(
                context = context,
                lineHeight = lineHeight,
                showCategory = showCategory,
                showAll = showAll)
        } else {
            toStringShort(
                context = context,
                lineHeight = lineHeight,
                showCategory = showCategory,
                showTotalAmount = showAll)
        }
        if (expanded || category in walletCategories || category.equals("treasure", ignoreCase = true)) {
            append(sb, getValueString(),
                ForegroundColorSpan(context.resources.getColor(R.color.gold_color_dark)),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        return sb
    }

    fun toStringLong(
        context: Context,
        showCategory: Boolean = false,
        lineHeight: Int,
        showAll: Boolean = true): SpannableStringBuilder {
        val sb = toStringShort(
            context = context,
            showCategory = showCategory,
            lineHeight = lineHeight,
            showTotalAmount = showAll)
        val sDB = getDescriptionString()
        if (!sb.toString().equals(sDB, ignoreCase = true)) {
            // this happens, when the item has only a description, nothing else, but is still cursed.
            // in this case of course we don't want to add the description a second time
            sb.append(sDB)
        }
        append(sb,
            getBuffString(),
            mutableListOf(
                StyleSpan(Typeface.ITALIC),
                ForegroundColorSpan(ContextCompat.getColor(context, R.color.text_highlight_color_char_progress))),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        append(sb,
            getGoneMsgString(),
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.item_gone_msg_color)),
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return sb
    }

    fun numAvailable(): Int {
        return amount - users.sumOf { it.amount }
    }

    /**
     * for marked items
     */
    fun getImageSpannables(context: Context, lineHeight: Int): SpannableStringBuilder {
        return if (marked) {
            Helper.generateImageSpannable(context = context, lineHeight = lineHeight, icon = R.drawable.ic_star_gold)
        } else {
            SpannableStringBuilder()
        }
    }

    fun toStringShort(
        context: Context,
        showCategory: Boolean = false,
        lineHeight: Int,
        showTotalAmount: Boolean = true): SpannableStringBuilder {

        var sb = getImageSpannables(context = context, lineHeight = lineHeight)
        if (showCategory) {
            sb = append(
                sb, getCategoryString(),
                ForegroundColorSpan(
                    ContextCompat.getColor(context, R.color.text_highlight_color_char_progress)
                ),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        sb.append(getLevelString())
//        sb = append(sb, getCursedString(),
//            ForegroundColorSpan(ContextCompat.getColor(context, android.R.color.holo_red_light)),
//            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)


        sb.append(getAmountsString(showTotalAmount = showTotalAmount))
        val nameStyles = mutableListOf<Any>(StyleSpan(Typeface.BOLD))
        if (cursed) {
            nameStyles.add(ForegroundColorSpan(context.resources.getColor(R.color.item_cursed_color)))
        }
        sb = append(sb, getNameString(), nameStyles,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        sb.append(getChargesString())
        val typeStyles = mutableListOf<Any>(StyleSpan(Typeface.ITALIC))
        if (cursed && getNameString().isBlank()) {
            typeStyles.add(ForegroundColorSpan(context.resources.getColor(R.color.item_cursed_color)))
        }
        sb = append(sb, getTypeString(),
            typeStyles,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        sb = append(sb, getEnhancementString(),
            StyleSpan(Typeface.ITALIC),
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        sb = append(sb, getEnchantmentString(),
            StyleSpan(Typeface.ITALIC),
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        if (sb.isBlank()) {
            appendDescription(sb, context)
        }
        if (sb.toString().trim() == "0") sb.clear()
        return sb
    }

    private fun appendDescription(sb: SpannableStringBuilder, context: Context) {
        val descrStyles = mutableListOf<Any>()
        if (cursed) {
            descrStyles.add(ForegroundColorSpan(context.resources.getColor(R.color.item_cursed_color)))
        }
        append(sb, getDescriptionString(),
            descrStyles,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    fun getNameAndTypString(): String {
        return "$name $typ"
    }

    companion object {

        const val AMOUNT_TEMPLATE = "%,d/%,d "
        const val SCHWARZGELD_ID = -666

        val SCHWARZGELD
            get() = Item(id = SCHWARZGELD_ID, category = "currency", name = "Schwarzgeld", value = 1f, amount = 0)

        fun calcTotalCurrency(
            items: List<Item>,
            onlyUnclaimed: Boolean = false,
            extendedWallet: Boolean = false): Float {
            var wealth = 0f
            val treasureCats = mutableListOf<String>().apply {
                addAll(walletCategories)
                if (extendedWallet) {
                    addAll(extendedWalletCategories)
                }
            }
            items.filter { it.category.lowercase() in treasureCats }
                .forEach {
                    val q = if (onlyUnclaimed) it.numAvailable() else it.amount
                    wealth += it.value * q
                }
            return wealth
        }

        fun parseItems(json: String, local: Boolean = true): ArrayList<Item> {
            val loot = ArrayList<Item>()
            var jitem: JSONObject? = null
            try {
                val values = JSONArray(json)
                for (i in 0 until values.length()) {
                    jitem = values.getJSONObject(i)
                    val item = Item()
                    item.id = jitem.getInt("id")
                    item.amount = jitem.getInt("amount")
                    item.category = jitem.getString("category")
                    item.charges = jitem.getInt("charges")
                    item.cursed = jitem.getBoolean("cursed")
                    item.description = jitem.getString("description")
                    item.enchantment = jitem.getString("enchantment")
                    item.enhancement = jitem.getInt("enhancement")
                    /*
                    in lootmanager buffs are called chartracker buffs
                    since i want to use the same parsing function for the json coming from
                    lootmanager api and json stored in local db i need to check both strings
                    to make sure i really get all the buffs
                     */
                    val ref =  if (local) "buffs" else "chartracker_buffs"
                    item.buffs = jitem.getString(ref)
                    item.level = jitem.getInt("level")
                    item.name = jitem.getString("name")
                    item.typ = jitem.getString("typ")
                    item.value = jitem.getString("value").toFloat()
                    item.marked = jitem.getBoolean("marked")
                    if (jitem.has("gone_msg")) {
                        item.goneMsg = jitem.getString("gone_msg")
                    }

                    if (jitem.has("users")) {
                        val juserlist = jitem.getJSONArray("users")
                        for (i in 0 until juserlist.length()) {
                            val jUwA = juserlist.getJSONObject(i)
                            item.users.add(
                                UserWithAmount(
                                    name = jUwA.getString("name"),
                                    amount = jUwA.getInt("amount")
                                )
                            )
                        }
                    }

                    if (jitem.has("stashes")) {
                        val jStashList = jitem.getJSONArray("stashes")
                        for (i in 0 until jStashList.length()) {
                            val jsession = jStashList.getJSONObject(i)
                            item.stashes.add(
                                Stash(
                                    name = jsession.getString("name"),
                                    manager = jsession.getString("manager")
                                )
                            )
                        }
                    }

                    item.equipped = true
                    if (jitem.has("equipped")) {
                        item.equipped = jitem.getBoolean("equipped")
                    }
                    if (jitem.has("onlyLocal")) {
                        item.onlyLocal = jitem.getBoolean("onlyLocal")
                    }




                    loot.add(item)
                }
            } catch (e: Exception) {
                Log.e("PARSING_ITEM", "parsing item: $jitem")
                e.printStackTrace()
            }

            return loot
        }
    }
}