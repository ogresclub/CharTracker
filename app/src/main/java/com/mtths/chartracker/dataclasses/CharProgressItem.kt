package com.mtths.chartracker.dataclasses

import android.content.Context
import android.util.Log
import com.mtths.chartracker.Global
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapter
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterAdeptPowers
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterArmorSR
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterArmorSplittermond
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterAttribute
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterBuffs
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterCharacter
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterClasses
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterComplexFormSR
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterConnection
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterFeat
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterGear
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterGearSplittermond
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterGrenade
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterInitiation
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterKnowledge
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterLanguages
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterLifeStyle
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterMeleeWeapons
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterPlaces
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterQuests
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterResources
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSRQuality
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSkillGroups
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSkills
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSkillsSR
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSpellSR
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSpells5e
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSpellsSplittermond
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSpilttermondPowers
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSrIds
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterStd
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterStdBuffable
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterVehicle
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterWare
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterWeaponSR
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterWeaponSplittermond
import com.mtths.chartracker.utils.CharProgressItemMap
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.TranslationHelper
import java.util.HashMap
import kotlin.collections.ArrayList

/**
 * This class is used to display log entries of e.g. learned feats, skills, attributes... in Fragment
 * Char Details
 * a linear layout will be created with a header and a listOfViews of log entries who are going to be inflated
 * in the Linear layout listOfViews.
 */
class CharProgressItem(
    /**
     * the header. E.g. Feat, Skills, Notes ...
     */
    var headerId: Int,
    /**
     * log entries will be filtered by this String
     * it will be translated according to language settings
     * so if used, make sure to check all possible translations
     * e.g. @Feat, @Skill, @Note, ...
     */
    var filterConstraintId: Int,
    /**
     * Death, Note and Character are not really considered only Char progress, because those entries
     * may also contain story relevant information.
     */
    var isStoryRelevant: Boolean,
    var isValueBased: Boolean,
    var usesAutoComplete: Boolean = false,
    /**
     * When inflating inCharDetails this is a reference to the character
     * otherwise it is just unimportant.
     */
    character: Character? = null,
    var positionCharDetailsLandscape: Int = POSITION_LEFT,

    // only used for Characters, Places and Quests
    var positionSessionDataLandscape: Int = POSITION_LEFT,
    val context: Context,


    /**
     * this means that auto competition will not only take place after the filter constraint,
     * but anywhere after the filter constraint.
     */
    var multipleAutoComplete: Boolean = false) {


    private val DEBUG_TAG = "CHAR_PROGRESS_ITEM"
    /**
     * This adapter collects the relevant information from the log entries and provides the views
     * to add in listOfViews when we want to inflate
     */
    var adapter: CharProgressItemAdapter<*>? = null
        set(value) {
            field = value
            adapter?.item = this
        }

    val mCharacter: Character? = character

    /**
     * When inflating inCharDetails this is a reference to the character
     * otherwise it is just unimportant.
     *
     * this is dirty.
     * in ActivityCharDetails, we want the character directly from the activity
     * but in ActivityMain, there is no special character and the character is directly stored in
     * mCharacter
     */
    val character: Character?
        get() = (adapter?.context as? ActivityCharDetails?)?.character ?: mCharacter

    /**
     * if this is true false calculated  entries will be marked red
     */
    var check_mistakes = false

    /**
     * this indicates if a filter contraint is found in the text of a log entries which is under
     * construction
     */
    var isActive = false

    val isSessionData: Boolean
        get() = Global.ITEMS_SESSION_DATA.none {
            it.matches(
                filterConstraint,
                context
            )
        }

    /**
     * a list of all possible translations of the filterconstraint
     */
    val filterConstraintTranslations: List<String> =
        TranslationHelper(context).getAllTranslations(filterConstraintId)


    /**
     * this string is localized, meaning it depends of the language settings of the app
     */
    val filterConstraint: String = context.resources.getString(filterConstraintId)
    
    val header: String = context.resources.getString(headerId)

    companion object {
        const val NO_EXP_TO_PROPOSE = "no exp to propose"
        const val POSITION_LEFT = 0
        const val POSITION_RIGHT = 1
        const val POSITION_MIDDLE = 2
        const val DEFAULT_EXTEND_CHAR_PROGRESS_ITEMS = true

        fun createCharProgressItems(activity: ProgressBarFrameDropBoxActivity?,
                                    character: Character? = null):
                CharProgressItemMap {
            val rulesSR5 = activity?.let { Helper.rulesSr5(character = character, context = it) } ?: false
            val rulesSR = activity?.let { Helper.rulesSR(character = character, context = it) } ?: false
            val rulesSkillfull5e = activity?.let {
                Helper.rulesSkillfullE5(
                    character = character,
                    context = it
                )
            } ?: false
            val activeRuleSystems = character?.ruleSystems?: PrefsHelper(activity).activeRuleSystems
//        log("TUTUTUTUTUTUTUT", "rulesSR5 = $rulesSR5, ${prefs.activeRuleSystems} ------------------------- ")
//        log("TUTUTUTUTUTUTUT", "rulesSkillfull5E = $rulesSkillfull5e, ${PrefsHelper(activity).activeRuleSystems} ------------------------- ")
            val charProgressItemsMap = CharProgressItemMap()

            activity?.let { activity ->

                val quests =
                    createCharProgressItemQuests(activity = activity, character = character)
                charProgressItemsMap[quests.mainItem.filterConstraintId] = quests

                //Buffs
                val buffs = createCharProgressItemBuffs(activity = activity, character = character)
                buffs.relevantItems = ArrayList()
                charProgressItemsMap[buffs.mainItem.filterConstraintId] = buffs

                /*
            ValueBased CharProgressItems
             */
                if (Helper.activeRules(
                        allowedRules = listOf(RuleSystem.rule_system_splittermond),
                        activeRuleSystems = activeRuleSystems
                    )
                ) {

                    val powers =
                        createCharProgressItemPowers(activity = activity, character = character)
                    powers.relevantItems = ArrayList()
                    charProgressItemsMap[powers.mainItem.filterConstraintId] = powers

                    val resources =
                        createCharProgressItemResources(activity = activity, character = character)
                    resources.relevantItems = ArrayList()
                    charProgressItemsMap[resources.mainItem.filterConstraintId] = resources

                }


                val attributes =
                    createCharProgressItemAttributes(activity = activity, character = character)
                attributes.relevantItems = ArrayList()
                charProgressItemsMap[attributes.mainItem.filterConstraintId] = attributes

                // SKILL GROUPS
                if (rulesSR5) {
                    val skillGroups =
                        createCharProgressItemSkillGroups(
                            activity = activity,
                            character = character
                        )
                    charProgressItemsMap[skillGroups.mainItem.filterConstraintId] = skillGroups
                }

                val skills =
                    createCharProgressItemSkills(activity = activity, character = character)
                charProgressItemsMap[skills.mainItem.filterConstraintId] = skills

                //KNOWLEDGE
                if (rulesSR) {
                    val knowledge =
                        createCharProgressItemKnowledge(activity = activity, character = character)
                    charProgressItemsMap[knowledge.mainItem.filterConstraintId] = knowledge
                }

                val languages =
                    createCharProgressItemLanguages(activity = activity, character = character)
                charProgressItemsMap[languages.mainItem.filterConstraintId] = languages

                if (!rulesSR) {
                    val classes =
                        createCharProgressItemClasses(activity = activity, character = character)
                    charProgressItemsMap[classes.mainItem.filterConstraintId] = classes
                }

                /*
            Standard CharProgressItems
             */
                //FEATS
                if (Helper.activeRules(
                        listOf(
                            RuleSystem.rule_system_ogres_club,
                            RuleSystem.rule_system_Dnd_E5,
                            RuleSystem.rule_system_skillfull_dnd_5E
                        ),
                        character?.ruleSystems ?: PrefsHelper(activity).activeRuleSystems
                    )
                ) {
                    val feats = CharProgressItem(
                        headerId = R.string.cpi_header_feats,
                        filterConstraintId = R.string.filter_constraint_feat,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                        context = activity
                    )
                    feats.adapter = CharProgressItemAdapterFeat(
                        activity,
                        CharProgressItemAdapterStd.FEAT_COMPARATOR
                    )
                    charProgressItemsMap[feats.filterConstraintId] =
                        CharProgMetaData(mainItem = feats)
                }

                if (Helper.activeRules(
                        listOf(
                            RuleSystem.rule_system_skillfull_dnd_5E,
                            RuleSystem.rule_system_Dnd_E5
                        ), activeRuleSystems
                    )
                ) {
                    val spells = CharProgressItem(
                        headerId = R.string.cpi_header_spells,
                        filterConstraintId = R.string.filter_constraint_spell,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    spells.adapter =
                        CharProgressItemAdapterSpells5e(
                            activity
                        )
                    charProgressItemsMap[spells.filterConstraintId] =
                        CharProgMetaData(mainItem = spells)


                } else if (Helper.activeRules(
                        listOf(
                            RuleSystem.rule_system_splittermond
                        ), activeRuleSystems
                    )
                ) {
                    val spells = CharProgressItem(
                        headerId = R.string.cpi_header_spells,
                        filterConstraintId = R.string.filter_constraint_spell,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    spells.adapter =
                        CharProgressItemAdapterSpellsSplittermond(
                            activity
                        )
                    charProgressItemsMap[spells.filterConstraintId] =
                        CharProgMetaData(mainItem = spells)
                }

                if (Helper.activeRules(
                        listOf(
                            RuleSystem.rule_system_splittermond
                        ), activeRuleSystems
                    )
                ) {
                    //ARMOR
                    Log.d("CREATE_CPIS", "creating armor cpi")
                    val armor = CharProgressItem(
                        headerId = R.string.cpi_header_armors,
                        filterConstraintId = R.string.filter_constraint_armor,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    armor.adapter = CharProgressItemAdapterArmorSplittermond(activity)
                    charProgressItemsMap[armor.filterConstraintId] =
                        CharProgMetaData(
                            mainItem = armor
                        )
                    for (item in charProgressItemsMap.values.map { it.mainItem }) {
                        Log.d("CREATE_CPIS", "auto complete fc: " + item.filterConstraint)
                    }

                    //WEAPONS
                    val weapons = CharProgressItem(
                        headerId = R.string.cpi_header_weapons,
                        filterConstraintId = R.string.filter_constraint_weapon,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    weapons.adapter =
                        CharProgressItemAdapterWeaponSplittermond(activity)
                    charProgressItemsMap[weapons.filterConstraintId] =
                        CharProgMetaData(mainItem = weapons)

                    //AUSRÜSTUNG
                    val gear = CharProgressItem(
                        headerId = R.string.cpi_header_gear,
                        filterConstraintId = R.string.filter_constraint_gear,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    gear.adapter = CharProgressItemAdapterGearSplittermond(activity)
                    charProgressItemsMap[gear.filterConstraintId] =
                        CharProgMetaData(
                            mainItem = gear
                        )
                }

                if (rulesSR) {
                    //ARMOR
                    val armor = CharProgressItem(
                        headerId = R.string.cpi_header_armors,
                        filterConstraintId = R.string.filter_constraint_armor,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    armor.adapter = CharProgressItemAdapterArmorSR(activity)
                    charProgressItemsMap[armor.filterConstraintId] =
                        CharProgMetaData(
                            mainItem = armor
                        )

                    //GEAR
                    val gear = CharProgressItem(
                        headerId = R.string.cpi_header_gear,
                        filterConstraintId = R.string.filter_constraint_gear,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    gear.adapter = CharProgressItemAdapterGear(activity)
                    charProgressItemsMap[gear.filterConstraintId] =
                        CharProgMetaData(
                            mainItem = gear
                        )

                    //WEAPONS
                    val weapons = CharProgressItem(
                        headerId = R.string.cpi_header_weapons,
                        filterConstraintId = R.string.filter_constraint_weapon,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    weapons.adapter =
                        CharProgressItemAdapterWeaponSR(activity)
                    charProgressItemsMap[weapons.filterConstraintId] =
                        CharProgMetaData(mainItem = weapons)


                    //MELEE WEAPONS
                    val melee_weapons = CharProgressItem(
                        headerId = R.string.cpi_header_melee_weapons,
                        filterConstraintId = R.string.filter_constraint_melee_weapon,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    melee_weapons.adapter =
                        CharProgressItemAdapterMeleeWeapons(activity)
                    charProgressItemsMap[melee_weapons.filterConstraintId] =
                        CharProgMetaData(mainItem = melee_weapons)

                    //GRENADES
                    val grenades = CharProgressItem(
                        headerId = R.string.cpi_header_grenades,
                        filterConstraintId = R.string.filter_constraint_grenade,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    grenades.adapter =
                        CharProgressItemAdapterGrenade(
                            activity
                        )
                    charProgressItemsMap[grenades.filterConstraintId] =
                        CharProgMetaData(mainItem = grenades)


                    //SPELLS
                    val spells = CharProgressItem(
                        headerId = R.string.cpi_header_spells,
                        filterConstraintId = R.string.filter_constraint_spell,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    spells.adapter =
                        CharProgressItemAdapterSpellSR(
                            activity
                        )
                    charProgressItemsMap[spells.filterConstraintId] =
                        CharProgMetaData(mainItem = spells)

                    //ComplexForms
                    val complexForms = CharProgressItem(
                        headerId = R.string.cpi_header_complex_forms,
                        filterConstraintId = R.string.filter_constraint_complex_form,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    complexForms.adapter =
                        CharProgressItemAdapterComplexFormSR(
                            activity
                        )
                    charProgressItemsMap[complexForms.filterConstraintId] =
                        CharProgMetaData(mainItem = complexForms)

                    //ADEPT POWERS

                        val adept_power = CharProgressItem(
                            headerId = R.string.cpi_header_adept_powers,
                            filterConstraintId = R.string.filter_constraint_adept_powers,
                            isStoryRelevant = false,
                            isValueBased = false,
                            usesAutoComplete = true,
                            multipleAutoComplete = true,
                            character = character,
                            context = activity
                        )
                        adept_power.adapter =
                            CharProgressItemAdapterAdeptPowers(activity)
                        charProgressItemsMap[adept_power.filterConstraintId] =
                            CharProgMetaData(
                                mainItem = adept_power
                            )



                    val inititaion = createCharProgressItemInitiation(
                        activity = activity,
                        character = character
                    )
                    charProgressItemsMap[inititaion.mainItem.filterConstraintId] = inititaion

                    //WARE
                    val ware = CharProgressItem(
                        headerId = R.string.cpi_header_ware,
                        filterConstraintId = R.string.filter_constraint_ware,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                        context = activity
                    )
                    ware.adapter = CharProgressItemAdapterWare(activity)
                    charProgressItemsMap[ware.filterConstraintId] =
                        CharProgMetaData(
                            mainItem = ware,
                        )

                    //VEHICLES
                    val vehicles = CharProgressItem(
                        headerId = R.string.cpi_header_vehicles,
                        filterConstraintId = R.string.filter_constraint_vehicle,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    vehicles.adapter =
                        CharProgressItemAdapterVehicle(
                            activity
                        )
                    charProgressItemsMap[vehicles.filterConstraintId] =
                        CharProgMetaData(mainItem = vehicles)

                    //DRONES
                    val drones = CharProgressItem(
                        headerId = R.string.cpi_header_drones,
                        filterConstraintId = R.string.filter_constraint_drone,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    drones.adapter =
                        CharProgressItemAdapterVehicle(
                            activity
                        )
                    charProgressItemsMap[drones.filterConstraintId] =
                        CharProgMetaData(mainItem = drones)

                    //IDS
                    val ids = CharProgressItem(
                        headerId = R.string.cpi_header_ids,
                        filterConstraintId = R.string.filter_constraint_id,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        positionCharDetailsLandscape = POSITION_RIGHT,
                        context = activity
                    )
                    ids.adapter =
                        CharProgressItemAdapterSrIds(activity = activity)
                    charProgressItemsMap[ids.filterConstraintId] = CharProgMetaData(mainItem = ids)

                    //LIFESTYLE
                    val lifeStyle = CharProgressItem(
                        headerId = R.string.cpi_header_life_style,
                        filterConstraintId = R.string.filter_constraint_lifestyle,
                        isStoryRelevant = false,
                        isValueBased = false,
                        character = character,
                        positionCharDetailsLandscape = POSITION_RIGHT,
                        context = activity
                    )
                    lifeStyle.adapter =
                        CharProgressItemAdapterLifeStyle(
                            activity,
                            CharProgressItemAdapterStd.DATE_COMPARATOR
                        )
                    charProgressItemsMap[lifeStyle.filterConstraintId] =
                        CharProgMetaData(mainItem = lifeStyle)


                    //CONNECTIONS
                    val connections = createCharProgressItemConnection(
                        activity = activity,
                        character = character
                    )
                    charProgressItemsMap[connections.mainItem.filterConstraintId] = connections

                    //QUALITIES
                    val qualities = CharProgressItem(
                        headerId = R.string.cpi_header_qualitites_shadowrun,
                        filterConstraintId = R.string.filter_constraint_quality,
                        isStoryRelevant = false,
                        isValueBased = false,
                        usesAutoComplete = true,
                        multipleAutoComplete = true,
                        character = character,
                        context = activity
                    )
                    qualities.adapter =
                        CharProgressItemAdapterSRQuality(
                            activity = activity
                        )
                    charProgressItemsMap[qualities.filterConstraintId] =
                        CharProgMetaData(
                            mainItem = qualities
                        )
                }

                //NOTES
                val notes = CharProgressItem(
                    headerId = R.string.cpi_header_notes,
                    filterConstraintId = R.string.filter_constraint_notes,
                    isStoryRelevant = true,
                    isValueBased = false,
                    usesAutoComplete = true,
                    multipleAutoComplete = true,
                    character = character,
                    context = activity
                )
                notes.adapter = CharProgressItemAdapterStdBuffable(
                    activity,
                    CharProgressItemAdapterStd.DATE_COMPARATOR,
                    hideColumnNames = true
                )
                charProgressItemsMap[notes.filterConstraintId] =
                    CharProgMetaData(
                        mainItem = notes
                    )

                if (!rulesSR) {
                    //DEATHS
                    val deaths = CharProgressItem(
                        headerId = R.string.cpi_header_deaths,
                        filterConstraintId = R.string.filter_constraint_death,
                        isStoryRelevant = true,
                        isValueBased = false,
                        usesAutoComplete = false,
                        character = character,
                        context = activity
                    )
                    deaths.adapter = CharProgressItemAdapterStd(
                        activity = activity,
                        comparator = CharProgressItemAdapterStd.DATE_COMPARATOR,
                        hideColumnNames = true
                    )
                    charProgressItemsMap[deaths.filterConstraintId] =
                        CharProgMetaData(mainItem = deaths)

                    //RESURRECTIONS
                    val resurrections = CharProgressItem(
                        headerId = R.string.cpi_header_resurrections,
                        filterConstraintId = R.string.filter_constraint_resurrection,
                        isStoryRelevant = true,
                        isValueBased = false,
                        usesAutoComplete = false,
                        character = character,
                        context = activity
                    )
                    resurrections.adapter = CharProgressItemAdapterStd(
                        activity = activity,
                        comparator = CharProgressItemAdapterStd.DATE_COMPARATOR,
                        hideColumnNames = true
                    )
                    charProgressItemsMap[resurrections.filterConstraintId] =
                        CharProgMetaData(mainItem = resurrections)
                }

                //PLACES
                val places =
                    createCharProgressItemPlaces(activity = activity, character = character)
                charProgressItemsMap[places.mainItem.filterConstraintId] = places

                //CHARACTERS
                val characters =
                    createCharProgressItemCharacters(activity = activity, character = character)
                charProgressItemsMap[characters.mainItem.filterConstraintId] = characters


                charProgressItemsMap.updateStringData(activity)

            }

            return charProgressItemsMap
        }


        fun createCharProgressItemQuests(activity: ProgressBarFrameDropBoxActivity,
                                         character: Character? = null,
                                         relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            //QUESTS
            val quests = CharProgressItem(
                headerId = R.string.cpi_header_quests,
                filterConstraintId = R.string.filter_constraint_quest,
                isStoryRelevant = true,
                isValueBased = false,
                usesAutoComplete = true,
                character = character,
                positionSessionDataLandscape = CharProgressItem.POSITION_RIGHT,
                context = activity
            )
            quests.adapter = CharProgressItemAdapterQuests(
                activity)
            return CharProgMetaData(mainItem = quests, relevantItems = relevantItems)
        }

        fun createCharProgressItemCharacters(activity: ProgressBarFrameDropBoxActivity,
                                             character: Character? = null,
                                             relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            val characters = CharProgressItem(
                headerId = R.string.label_characters,
                filterConstraintId = R.string.filter_constraint_character,
                isStoryRelevant = true,
                isValueBased = false,
                usesAutoComplete = false,
                character = character,
                positionSessionDataLandscape = CharProgressItem.POSITION_LEFT,
                context = activity
            )
            characters.adapter = CharProgressItemAdapterCharacter(
                activity,
                CharProgressItemAdapterCharacter.NAME_COMPARATOR,
                Global.characterNamesToAutocomplete
            )
            return CharProgMetaData(mainItem = characters, relevantItems = relevantItems)
        }
        fun createCharProgressItemPlaces(activity: ProgressBarFrameDropBoxActivity,
                                         character: Character? = null,
                                         relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            val places = CharProgressItem(
                headerId = R.string.cpi_header_places,
                filterConstraintId = R.string.filter_constraint_place,
                isStoryRelevant = true,
                isValueBased = false,
                usesAutoComplete = false,
                character = character,
                positionSessionDataLandscape = POSITION_MIDDLE,
                context = activity
            )
            places.adapter = CharProgressItemAdapterPlaces(
                activity,
                CharProgressItemAdapterCharacter.NAME_COMPARATOR,
                Global.placesNamesToAutocomplete
            )
            return CharProgMetaData(mainItem = places, relevantItems = relevantItems)
        }
        fun createCharProgressItemConnection(activity: ProgressBarFrameDropBoxActivity,
                                         character: Character? = null,
                                         relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            //CONNECTIONS
            val connections = CharProgressItem(
                headerId = R.string.cpi_header_connections,
                filterConstraintId = R.string.filter_constraint_connection,
                isStoryRelevant = true,
                isValueBased = false,
                usesAutoComplete = true,
                multipleAutoComplete = true,
                character = character,
                positionCharDetailsLandscape = POSITION_RIGHT,
                context = activity
            )
            connections.adapter = CharProgressItemAdapterConnection(activity)
            return CharProgMetaData(mainItem = connections, relevantItems = relevantItems)
        }

        fun createCharProgressItemBuffs(activity: ProgressBarFrameDropBoxActivity,
                                        character: Character? = null,
                                        relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {

            //BUFFS
            val buffs = CharProgressItem(
                headerId = R.string.label_buffs,
                filterConstraintId = R.string.filter_constraint_buff,
                isStoryRelevant = false,
                isValueBased = false,
                usesAutoComplete = true,
                multipleAutoComplete = true,
                character = character,
                positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                context = activity
            )

            buffs.adapter = CharProgressItemAdapterBuffs(
                mActivity = activity)
            return CharProgMetaData(mainItem = buffs, relevantItems = relevantItems)
        }

        fun createCharProgressItemAttributes(activity: ProgressBarFrameDropBoxActivity,
                                             character: Character? = null,
                                             relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            //ATTRIBUTES
            val attributes = CharProgressItem(
                headerId = R.string.cpi_header_attributes,
                filterConstraintId = R.string.filter_constraint_attribute,
                isStoryRelevant = false,
                isValueBased = true,
                usesAutoComplete = true,
                character = character,
                positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                context = activity
            )
            val comparator = if (Helper.rulesSR(character, activity)) {
                ValueNamePair.PAIR_INTEGER_STRING__ATTRIBUTE_COMPARATOR_SR
            } else {
                ValueNamePair.PAIR_INTEGER_STRING__ATTRIBUTE_COMPARATOR_DND
            }
            attributes.adapter = CharProgressItemAdapterAttribute(
                activity = activity,
                comparator = comparator)
            return CharProgMetaData(mainItem = attributes, relevantItems = relevantItems)
        }

        fun createCharProgressItemSkills(activity: ProgressBarFrameDropBoxActivity,
                                         character: Character? = null,
                                         relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {


            //SKILLS
            if(activity.let { Helper.rulesSr5(character = character, context = it) }) {
                val skillsSR = CharProgressItem(
                    headerId = R.string.cpi_header_skills,
                    filterConstraintId = R.string.filter_constraint_skill,
                    isStoryRelevant = false,
                    isValueBased = true,
                    usesAutoComplete = true,
                    character = character,
                    positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                    context = activity
                )
                skillsSR.adapter = CharProgressItemAdapterSkillsSR(activity)
                return CharProgMetaData(mainItem = skillsSR, relevantItems = relevantItems)
            }


            val skillsDefault = CharProgressItem(
                headerId = R.string.cpi_header_skills,
                filterConstraintId = R.string.filter_constraint_skill,
                isStoryRelevant = false,
                isValueBased = true,
                usesAutoComplete = true,
                character = character,
                positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                context = activity
            )
            skillsDefault.adapter = CharProgressItemAdapterSkills(
                activity)

            return CharProgMetaData(mainItem = skillsDefault, relevantItems = relevantItems)

        }

        fun createCharProgressItemSkillGroups(activity: ProgressBarFrameDropBoxActivity,
                                              character: Character? = null,
                                              relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            //SKILL GROUPS
            val skillGroups = CharProgressItem(
                headerId = R.string.cpi_header_skill_groups,
                filterConstraintId = R.string.filter_constraint_skill_group,
                isStoryRelevant = false,
                isValueBased = true,
                usesAutoComplete = true,
                character = character,
                context = activity
            )
            // Parameter for SkillGroup Adapter are actually not needed because nothing is inflated here
            skillGroups.adapter = CharProgressItemAdapterSkillGroups(
                activity
            )
            return CharProgMetaData(mainItem = skillGroups, relevantItems = relevantItems)
        }

        fun createCharProgressItemLanguages(activity: ProgressBarFrameDropBoxActivity,
                                            character: Character? = null,
                                            relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            //LANGUAGES
            val languages = CharProgressItem(
                headerId = R.string.cpi_header_languages,
                filterConstraintId = R.string.filter_constraint_language,
                isStoryRelevant = false,
                isValueBased = true,
                usesAutoComplete = true,
                character = character,
                positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                context = activity
            )
            languages.adapter = CharProgressItemAdapterLanguages(
                activity
            )
            return CharProgMetaData(mainItem = languages, relevantItems = relevantItems)
        }
        fun createCharProgressItemResources(activity: ProgressBarFrameDropBoxActivity,
                                            character: Character? = null,
                                            relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            //RESOURCES
            val resources = CharProgressItem(
                headerId = R.string.cpi_header_resources,
                filterConstraintId = R.string.filter_constraint_resource,
                isStoryRelevant = false,
                isValueBased = true,
                usesAutoComplete = true,
                character = character,
                positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                context = activity
            )
            resources.adapter = CharProgressItemAdapterResources(
                activity = activity
            )
            return CharProgMetaData(mainItem = resources, relevantItems = relevantItems)
        }

        fun createCharProgressItemPowers(activity: ProgressBarFrameDropBoxActivity,
                                            character: Character? = null,
                                            relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            //POWERS
            val power = CharProgressItem(
                headerId = R.string.cpi_header_splittermond_powers,
                filterConstraintId = R.string.filter_constraint_splittermond_power,
                isStoryRelevant = false,
                isValueBased = false,
                usesAutoComplete = true,
                multipleAutoComplete = true,
                character = character,
                context = activity
            )
            power.adapter = CharProgressItemAdapterSpilttermondPowers(activity)
            return CharProgMetaData(mainItem = power, relevantItems = relevantItems)
        }

        fun createCharProgressItemKnowledge(activity: ProgressBarFrameDropBoxActivity,
                                            character: Character? = null,
                                            relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            //KNOWLEDGE
            val knowledge = CharProgressItem(
                headerId = R.string.cpi_header_knowledge,
                filterConstraintId = R.string.filter_constraint_knowledge,
                isStoryRelevant = false,
                isValueBased = true,
                usesAutoComplete = true,
                character = character,
                positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                context = activity
            )
            knowledge.adapter = CharProgressItemAdapterKnowledge(
                activity
            )
            return CharProgMetaData(mainItem = knowledge, relevantItems = relevantItems)
        }

        fun createCharProgressItemClasses(
            activity: ProgressBarFrameDropBoxActivity,
            character: Character? = null,
            relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            val level = CharProgressItem(
                headerId = R.string.cpi_header_classes,
                filterConstraintId = R.string.filter_constraint_class,
                isStoryRelevant = false,
                isValueBased = true,
                usesAutoComplete = true,
                character = character,
                positionCharDetailsLandscape = CharProgressItem.POSITION_RIGHT,
                context = activity
            )
            level.adapter = CharProgressItemAdapterClasses(activity)
            return CharProgMetaData(mainItem = level, relevantItems = relevantItems)

        }

        fun createCharProgressItemInitiation(
            activity: ProgressBarFrameDropBoxActivity,
            character: Character? = null,
            relevantItems: MutableList<CharProgressItem> = ArrayList()
        ): CharProgMetaData {
            //INITIATION
            val initiation = CharProgressItem(
                headerId = R.string.cpi_header_initiation,
                filterConstraintId = R.string.filter_constraint_initiation,
                isStoryRelevant = false,
                isValueBased = true,
                usesAutoComplete = false,
                multipleAutoComplete = false,
                character = character,
                context = activity
            )
            initiation.adapter = CharProgressItemAdapterInitiation(activity)

            return CharProgMetaData(mainItem = initiation, relevantItems = relevantItems)
        }
    }

}

class CharProgMetaData(val mainItem: CharProgressItem, var relevantItems: MutableList<CharProgressItem> = ArrayList())

class ItemHeaderAndCreateFunction(val filterConstraintId: Int,
                                  val createNew: (activity: ProgressBarFrameDropBoxActivity,
                                                  character: Character?,
                                                  relevantItems: MutableList<CharProgressItem>) -> CharProgMetaData,
                                  /**
                                   * tell us with which rule system we want to use them
                                   * If think this is important, because in main screen we recreate
                                   * value based items when changing character
                                   * and because we also have the session items, we dont want to
                                   * recreate only value based items when changing character,
                                   * not also session items
                                   * and the data about which item is relevant for which rulesystem
                                   * ist stored in createCharProgressItem(). So we need this data
                                   * again here when only recreating the value based items
                                   *
                                   * this seems pretty messy to me, but I thought writing this down
                                   * makes sense, because I couldn't really remember it myself
                                   * why this way important here
                                   */
                                  val doUseWithRules: HashMap<RuleSystem, Boolean> =
                                      HashMap<RuleSystem, Boolean>().apply {
                                          RuleSystem.allRuleSystems().forEach {
                                              set(it, true)
                                          }
                                      }
) {
    /**
     * check if any translation of the filterConstraint matches the given filterConstraint
     */
    fun matches(filterConstraint: String, context: Context): Boolean {
        return TranslationHelper(context).getAllTranslations(filterConstraintId).find { 
            it.equals(filterConstraint, ignoreCase = true)
        } != null
    }
    

}
