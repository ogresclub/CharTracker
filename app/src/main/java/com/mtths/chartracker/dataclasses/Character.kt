package com.mtths.chartracker.dataclasses

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.mtths.chartracker.*
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterAttribute.Companion.getAttributeMod
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterClasses
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterFeat
import com.mtths.chartracker.utils.FileUtils
import com.mtths.chartracker.utils.FileUtils.createFileAndAllParentDirs
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.decodeSampledBitmapFromFile
import com.mtths.chartracker.utils.Helper.getCharIconsFolderPath
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.utils.HttpUtils
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_ADEPT_POINTS
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_ARMOR_CHECK_PENALTY
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_ASTRAL_INI_DICE
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_BEHINDERUNG
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_COMPOSURE
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_CONSTITUTION_FOR_HP_CALC
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_DEFENSE_RATING
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_DEFENSE_ROLL
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_DRAIN_RESISTANCE
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_ESSENCE
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_FOKUS
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_GEISTIGER_WIDERSTAND
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_HP
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_HPR
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_INITIATIVE
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_INI_DICE
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_JUDGE_INTENTIONS
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_KOERPERLICHER_WIDERSTAND
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MAGIC_POINTS
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MAGIC_POINTS_MULTIPLIER
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MATRIX_ATTACK_RATING
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MATRIX_DEFENSE_RATING
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MATRIX_DEVICE_RATING
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MATRIX_HP
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MATRIX_INITIATIVE
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MATRIX_INI_DICE
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MAX_OVERFLOW
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MEMORY
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_MINOR_ACTIONS
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_NEGATIVE_HP
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_RESIST_DMG
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_RESIST_STUN_DMG
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_SKILLS_PER_LEVEL
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_STUN_HP
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_SOCIAL_RATING
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_SPEED
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_TIK
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_UNARMED_ATTACK_RATING
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_VERTEIDIGUNG
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_Dnd_E5
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_ogres_club
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_skillfull_dnd_5E
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_splittermond
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_sr5
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_sr6
import org.json.JSONObject
import java.io.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.abs
import kotlin.math.ceil
import kotlin.math.max
import com.mtths.chartracker.dataclasses.Skill.Companion.getSkillRef
import com.mtths.chartracker.preferences.PrefsCharacter
import com.mtths.chartracker.utils.CharProgressItemMap
import com.mtths.chartracker.utils.Helper.capitalize
import com.mtths.chartracker.utils.TranslationHelper

/**
 * This class represents a character.
 *
 * if the character gets deleted after being used,
 * but you want to keep references to him in the logEntries, character.isDeleted will be set true.
 *
 */
data class Character (var id: Long = -1,
                      var name: String = "",
                      var currentExp: Int = 0,
                      var totalExp: Int = 0,
                      var priority: Int = 0,
                      var isActive: Boolean = false,
                      var isRetired: Boolean = false,
                      var isDead: Boolean = false,
                      /**
                       * some name referencing to a resource drawable
                       * this is only used, if no iconPath is given for a custom icon
                       */
                      var iconName: String = "dead_head",
                      var currentCommit: HttpUtils.CommitWithLink =
                          HttpUtils.CommitWithLink(HttpUtils.NO_CURRENT_COMMIT_AVAILABLE_MSG)
) {
    var stats: MutableMap<String, Int> = HashMap()

    /**
     * filter all augment stats, because they are basically stored in  buffs and stack,
     * so when they are already loaded when storing happens, they will be counted twice
     * filter also replacement stats, because otherwise they will not disappear
     * when the buff is deleted (of course this could be changed, but where is the point ;)
     */
    fun exportStats(pretty: Boolean = false): String {
        return GsonBuilder().apply {
            if (pretty) setPrettyPrinting()
        }.create().toJson(stats.filterNot {
            it.key.endsWith(ATTR_AFFIX_AUGMENT) ||
                    it.key.endsWith(ATTR_AFFIX_REPLACEMENT) })
    }

    /**
     * clear all stats, except for size
     */
    fun clearCPIStats() {
        stats = HashMap<String, Int>().apply {
            stats[STAT_SIZE]?.let {
                put(STAT_SIZE, it)
            }
        }
    }

    var isDeleted = false
    var created_at: String = ""
    var retired_at: String = ""
    var last_edit_date: String = ""

    /**
     * deprecated
     * earlier the level was stored directly in db
     * now it is tracked in log entries using @level allowing also multi classing
     * this is now the level stored directly
     */
    var _level: Int = 1

    val level: Int
        get() = (charProgressItemsMap.get("@Level")?.mainItem?.adapter
                as? CharProgressItemAdapterClasses?)?.getTotalLevel() ?: 1



    /**
     * this is actually stored in SharedPrefs so needs context, so we load the value when loading
     * character from db
     */
    var hgExpPercentage: Int
        get() = prefs[PREF_HG_EXP_PERCENTAGE]?.toInt() ?: hgExpPercentageDefault
        set(value) {prefs[PREF_HG_EXP_PERCENTAGE] = value.toString()}
    var hgExpPercentageDefault = 100
    /**
     * this is actually stored in SharedPrefs so needs context, so we load the value when loading
     * character from db
     */
    var fokusPercentage: Int
        get() = prefs[PREF_FOCUS_PERCENTAGE]?.toInt() ?: fokusPercentageDefault
        set(value) {prefs[PREF_FOCUS_PERCENTAGE] = value.toString()}

    var fokusPercentageDefault = 200

    val heldengrad: Int
        get() =
            when {
                totalExp - currentExp < hgExpPercentage -> 1
                totalExp - currentExp < 3*hgExpPercentage -> 2
                totalExp - currentExp < 6*hgExpPercentage -> 3
                else -> 4
            }


    /**
     * items are downloaded from this character in lootmanager
     */
    var lootManagerName: String = ""
    val lootManagerDownloadName: String
        get() = lootManagerName.ifBlank { displayName }
    var useManagerAccess: Boolean = false

    /**
     * loot is stored as json string
     * parsing to Item will only take place when items are displayed (what did I mean here??!)
     */
    var loot: ArrayList<Item> = ArrayList()
        set(value) {
            for (new_item in value) {
                for (item in loot) {
                    if (new_item.id == item.id) {
                        new_item.equipped = item.equipped
                    }
                }
            }
            field = value
        }

    /**
     * buffs are stored as json string
     */
    var buffs: ArrayList<Buff> = ArrayList()

    /**
     * this is used to store automatically generated buffs from CPI while generating the CPI
     *  after they are generated, all non modifiable buffs (which are those auto generated by cpi)
     *  will be deleted and replaced by those from temp buffs
     */
    var tempBuffs: MutableList<Buff> = ArrayList()

    fun exportBuffs(pretty: Boolean = false, includeTemps: Boolean): String {
        return GsonBuilder().apply {
            if (pretty) setPrettyPrinting()
        }.create().toJson(buffs.map { it }.toMutableList().apply {
            //add temp buffs and mark with type suffix
            if (includeTemps) addAll(tempBuffs.map {addTempMarker(it)})
        }
            .map { it.toExportBuff() })
    }

    /**
     * nonmodifiable buffs are built from log entry data,
     * so we want to remove them, when changing a log entry
     * to make sure it will not exist 2x, once with old and one with new nme
     */
    fun clearNonModifiableBuffs() {
        buffs.removeIf { !it.modifiable }
    }

    fun addHotSimBuffIfNecessary() {
        if (hasArchetypes(listOf("Decker", "Technomancer"))) {
            buffs.find { getFullAttributeNameAndNormalize(HOT_SIM_BUFF_NAME) == getFullAttributeNameAndNormalize(it.name) }?.apply {
                if (is_technomancer) active = true
            } ?: run {
                buffs.add(Buff().apply {
                    name = HOT_SIM_BUFF_NAME
                    active = is_technomancer
                    modifiable = true
                    effects = mutableListOf(BuffEffect(
                        stat = BUFF_REF_MATRIX_INI_DICE, value = 1
                    ))
                })
            }
        }
    }


    /**
     * loot is stored as json string
     * parsing to Item will only take place when items are displayed (what did I mean here??!)
     */
    var schwarzloot: ArrayList<Item> = ArrayList()
        set(value) {
            for (new_item in value) {
                new_item.onlyLocal = true
                for (item in schwarzloot) {
                    if (new_item.id == item.id) {
                        new_item.equipped = item.equipped
                    }
                }
            }
            field = value
        }

    /**
     * subraces for the race are not loaded from db when loading character!
     */
    var race: Race = Race(name = UNDEFINED)
    var archetype: DndArchetype = DndArchetype(name = UNDEFINED)

    fun hasArchetype(archetypeName: String): Boolean {
        return hasArchetypes(listOf(archetypeName))
    }

    fun hasArchetypes(archetypeNames: List<String>): Boolean {
        return archetype.name.trim().lowercase().split(",")
            .intersect(archetypeNames.map { it.trim().lowercase() }.toSet()).isNotEmpty()
    }

    fun isClassSkill(skillName: String): Boolean {
        val skName = skillName.lowercase()
        val clsSkills = dndClass.classSkills.map { it.lowercase() }

            return clsSkills.contains(skName) ||
                skName.startsWith("knowledge") && clsSkills.contains("knowledge (all)")
    }

    /**
     * archytypes for the class are not loaded from db when loading character!
     */
    var dndClass: DndClass = DndClass(name = UNDEFINED)
    var subrace: Subrace = Subrace(name = UNDEFINED)
    var size: Int
        get() = getTotalStatValue(STAT_SIZE)
        set(value) {setStat(STAT_SIZE, value)}

    val sizeStealthMod: Int
        get() = when(ruleSystem) {
            rule_system_splittermond -> 5 - size
            else -> 0
        }

    fun getPrefs(context: Context) = PrefsCharacter(context, name)

    var featured_language_points_used: Int = 0
    var icon_log_entry: Drawable? = null
    //stats:
    var intelligence: Int
        get() = getTotalStatValue(ATTR_INTELLIGENCE)
        set(value) { stats[ATTR_INTELLIGENCE] = value}

    var body: Int
        get() = getTotalStatValue(ATTR_BODY)
        set(value) { setStat(ATTR_BODY, value) }
    var agility: Int
        get() = getTotalStatValue(ATTR_AGILITY)
        set(value) { setStat(ATTR_AGILITY, value) }
    var willpower: Int
        get() = getTotalStatValue(ATTR_WILLPOWER)
        set(value) { setStat(ATTR_WILLPOWER, value) }
    var logic: Int
        get() = getTotalStatValue(ATTR_LOGIC)
        set(value) { setStat(ATTR_LOGIC, value) }
    var intuition: Int
        get() = getTotalStatValue(ATTR_INTUITION)
        set(value) { setStat(ATTR_INTUITION, value) }
    var strength: Int
        get() = getTotalStatValue(ATTR_STRENGTH)
        set(value) { setStat(ATTR_STRENGTH, value) }
    var reaction: Int
        get() = getTotalStatValue(ATTR_REACTION)
        set(value) { setStat(ATTR_REACTION, value) }
    var charisma: Int
        get() = getTotalStatValue(ATTR_CHARISMA)
        set(value) { setStat(ATTR_CHARISMA, value) }
    var magic: Int
        get() = getTotalStatValue(ATTR_MAGIC)
        set(value) { setStat(ATTR_MAGIC, value) }
    var edge: Int
        get() = getTotalStatValue(ATTR_EDGE)
        set(value) { setStat(ATTR_EDGE, value) }
    var resonance: Int
        get() = getTotalStatValue(ATTR_RESONANCE)
        set(value) { setStat(ATTR_RESONANCE, value) }
    var constitution: Int
        get() = getTotalStatValue(ATTR_CONSTITUTION)
        set(value) { setStat(ATTR_CONSTITUTION, value) }
    var dexterity: Int
        get() = getTotalStatValue(ATTR_DEXTERITY)
        set(value) { setStat(ATTR_DEXTERITY, value) }
    var wisdom: Int
        get() = getTotalStatValue(ATTR_WISDOM)
        set(value) { setStat(ATTR_WISDOM, value) }

    /*
    Matrix Attributes are considered base value 0 (they are not in statmap)
    and are set with buffs, because they can be swapped easily and this can
    be more conveniently done with buffs
    also they depend only on ware, not usual character attributes, and ware affects
    character stats by buffs
     */
    val data_processing: Int // base value only changed by buffs
        get() {
            val technomancer = if (is_technomancer) logic else 0
            return getTotalStatValue(STAT_DATA_PROCESSING) + technomancer
        }

    val matrix_attack: Int // base value only changed by buffs
        get() {
            val technomancer = if (is_technomancer) charisma else 0
            return getTotalStatValue(STAT_MATRIX_ATTACK) + technomancer
        }

    val firewall: Int // base value only changed by buffs
        get() {
            val technomancer = if (is_technomancer) willpower else 0
            return getTotalStatValue(STAT_FIREWALL) + technomancer
        }

    val sleaze: Int // base value only changed by buffs
        get() {
            val technomancer = if (is_technomancer) intuition else 0
            return getTotalStatValue(STAT_SLEAZE) + technomancer
        }

    var bonus_adept_points
        get() = getStatBuffValueOrZero(BUFF_REF_ADEPT_POINTS)
        set(value) { setStatBuff(BUFF_REF_ADEPT_POINTS, value)}

    var used_adept_power_points = 0f
    var essence_cyber = 0f
    var essence_bio = 0f

    val ini_dice: Int
        get() = 1 + getStatBuffValueOrZero(BUFF_REF_INI_DICE)

    val minorActions: Int
        get() = 1 + ini_dice + getStatBuffValueOrZero(BUFF_REF_MINOR_ACTIONS)

    val astral_ini_dice: Int
        get() = 3 + getStatBuffValueOrZero(BUFF_REF_ASTRAL_INI_DICE)
    val matrix_ini_dice: Int
        get() = 2 + getStatBuffValueOrZero(BUFF_REF_MATRIX_INI_DICE)




    var money: Int = 0
    val armor: Int
        get() = getTotalStatValue(STAT_ARMOR)

    val matrix_damage: Int
        get() = damages[REF_DAMAGE_REF_MATRIX]?.sum() ?: 0
    val stun_damage: Int
        get() = damages[REF_DAMAGE_REF_STUN]?.sum() ?: 0
//    var stun_damages: IntArray = intArrayOf(0)
    val physical_damage: Int
        get() = damages[REF_DAMAGE_REF_PHYSICAL]?.sum() ?: 0

    var damages: HashMap<String, IntArray> = HashMap<String, IntArray>().apply {
        put(REF_DAMAGE_REF_PHYSICAL, intArrayOf(0))
        put(REF_DAMAGE_REF_STUN, intArrayOf(0))
    }
    var currentEdge = 0

    var splittermondHomeBrew: Boolean
        get() = prefs[PREF_SPLITTERMOND_HOMEBREW]?.toBoolean() ?: splittermondHomeBrewDefault
        set(value) {prefs[PREF_SPLITTERMOND_HOMEBREW] = value.toString()}

    var splittermondHomeBrewDefault: Boolean = true
    val splittermondHpMultiplier: Int
        get() = if (splittermondHomeBrew) {
            SPLITTERMOND_HP_MULTIPLIER_HOME_BREW
        } else {
            SPLITTERMOND_HP_MULTIPLIER
        }

    val matrixDamageMalus: Int
        get() = max(matrix_damage / 3, 0)

    val damageMalus: Int
        get() {
            val y = (hp / splittermondHpMultiplier)
            val healthLevel = (if (y != 0) (physical_damage - 1) / y else 0)
            val healthLevelModded = max(
                healthLevel +
                        getStatBuffValueOrZero(BuffEffect.BUFF_REF_HEALTH_LEVEL),
                0)


            return when (ruleSystem) {
                rule_system_splittermond -> {
                    if (splittermondHomeBrew) {
                        when (healthLevelModded) {
                            0 -> 0
                            1 -> 2
                            2 -> 4
                            3 -> 8
                            4 -> 16
                            else -> 0
                        }
                    } else {
                        //default splittermond rules
                        when (healthLevelModded) {
                            0 -> 0
                            1 -> 1
                            2 -> 2
                            3 -> 4
                            4 -> 8
                            else -> 0
                        }
                    }
                }
                rule_system_sr5, rule_system_sr6 -> {
                    Integer.max((stun_damage - getStatBuffValueOrZero(BUFF_REF_RESIST_STUN_DMG)) / 3, 0) +
                            Integer.max((physical_damage - getStatBuffValueOrZero(BUFF_REF_RESIST_DMG)) / 3, 0)
                }
                rule_system_skillfull_dnd_5E -> {
                    if ((physical_damage - getStatBuffValueOrZero(BUFF_REF_RESIST_DMG)) > (hp/2)) {
                        2
                    } else {
                        0
                    }
                }
                else -> 0
            }.also {
//                Log.d("WOUNDS","Wound Modifier / Damage Malus for character $name: $it rule system: $ruleSystem, hp = $hp")
            }
        }

    fun getBaseSaveBonus(saveRef: String) = (dndClass.progressions[saveRef] ?: -1) + level

    val reflexSave: Int
        get() = getBaseSaveBonus(PROGRESSION_REF_REFLEX_SAVES) +
                getAttributeMod(rule_system_ogres_club, dexterity) +
                getStatBuffValueOrZero(PROGRESSION_REF_REFLEX_SAVES)

    val willSave: Int
        get() = getBaseSaveBonus(PROGRESSION_REF_WILL_SAVES) +
                getAttributeMod(rule_system_ogres_club, wisdom) +
                getStatBuffValueOrZero(PROGRESSION_REF_WILL_SAVES)

    val fortitudeSave: Int
        get() = getBaseSaveBonus(PROGRESSION_REF_FORT_SAVES) +
                getAttributeMod(rule_system_ogres_club, constitution) +
                getStatBuffValueOrZero(PROGRESSION_REF_FORT_SAVES)

    fun getSave(saveRef: String) = (getBaseSaveBonus(saveRef) + getStatBuffValueOrZero(saveRef))

    val concentrationMalus: Int
        get() = 2 * max(
            0,
            buffs.count {it.type.equals(BUFF_TYPE_SR_SPELL_CONCENTRATION, ignoreCase = true)}
            - getStatBuffValueOrZero(BuffEffect.BUFF_REF_FREE_CONCENTRATION)
                )

    val maxOverFlow: Int
        get() = when (ruleSystem) {
            rule_system_sr5 -> body
            rule_system_sr6 -> body * 2
            rule_system_splittermond -> hp / splittermondHpMultiplier
            rule_system_ogres_club -> constitution +  getStatBuffValueOrZero(BUFF_REF_NEGATIVE_HP)
            else -> 0
        } + getStatBuffValueOrZero(BUFF_REF_MAX_OVERFLOW)



    var ATTRIBUTE_POINT_EXP_COST = 0

    /**
     * use filterConstraint of CharProgressItems as String
     * second entry is orientation
     */
    var charProgressItemsMap = CharProgressItemMap()
    var extendMap = HashMap<String, HashMap<Int, Boolean>>()

    fun exportExtendMap(pretty: Boolean = false): String = GsonBuilder().apply {
        if (pretty) setPrettyPrinting()
    }.create().toJson(extendMap)

    /**
     * a lit of all the rule systems related to a character
     * value will be loaded in CharacterAdapter
     */
    var ruleSystems: List<RuleSystem>? = null

    /**
     * just make sure to tell the character with rulesystem he is in, because this is not loaded
     * when character is loaded from database
     * This needs to be done MANUALLY!!
     */
    val ruleSystem
        get() = ruleSystems?.firstOrNull()


    /**
     * this is used to get an icon if not empty
     * the path is a local path in the internal app directory
     * Otherwise "icon" attribute is used to get the icon from the list of given icons
     */
    var iconFileName: String = ""

    var displaySchwarzgeldWealth: Boolean = false
    val hasSchwarzgeld: Boolean
        get() = schwarzgeld != null

    val schwarzgeld: Item?
        get() = schwarzloot.find { it.name.trim().equals("Schwarzgeld", ignoreCase = true)}

    val displayName: String
        get() = getDisplayName(this)

    val isManager: Boolean
        get() = loot.asSequence().map { it.stashes }.flatten().map { it.manager }.distinct()
            .find { it.equals(lootManagerDownloadName, ignoreCase = true) } != null

    val stashes
        get() = loot.asSequence().map { it.stashes }.flatten().distinctBy { it.name }



    init {
        if (iconName == "dead_head") {
            this.iconName = randomCharIcon
        }
    }

    constructor(name: String = "",
                currentExp: Int = 0,
                totalExp: Int = 0,
                priority: Int = 0) : this(-1, name, currentExp, totalExp, priority, false, false, false) {
    }

    /**
     * store buff effects of single buff in character
     */
    fun applyBuff(buff: Buff) {
        if (buff.active) {
            buff.effects.forEach { bfx ->
                addStatBuff(statRef = bfx.stat, value = bfx.value, replace = bfx.replace)
            }
        }
    }


    fun addStatBuff(statRef: String, value: Int, replace: Boolean) {
        if (replace) {
            setStatReplacement(statRef, max(value, getStatReplacementValueOrZero(statRef)))
        } else {
            setStatBuff(statRef, getStatBuffValueOrZero(statRef) + value)
        }

    }

    fun isBuffKey(key: String): Boolean {
        return key.endsWith(getStatBuffRef("")) ||
                key.endsWith(getStatChecksBuffRef("")) ||
                key.endsWith(getStatReplacementRef(""))
    }

    fun clearStatChangesFromBuffs() {
        stats = stats.filterNot { isBuffKey(it.key) }.toMutableMap()
    }

    fun addTempMarker(buff: Buff) = buff.apply {
        if (!type.endsWith(BUFF_TYPE_TEMP)) {
            type += BUFF_TYPE_TEMP
        }
    }

    fun removeTempMarker(buff: Buff) = buff.apply {
        type = type.substringBefore(BUFF_TYPE_TEMP)
    }

    /**
     * update buffs generated from CPI (those who are not modifiable)
     * with those store in tempBuffs
     */
    fun updateCPIBuffs() {
        buffs.apply {
            removeIf { !it.modifiable }
            buffs.addAll(tempBuffs.map {
                removeTempMarker(it)
            })
        }
        tempBuffs.clear()
    }

    fun loadBuffBoni() {
        generateLootBuffs()
        addHotSimBuffIfNecessary()
        // apply Boni to stats
        applyBuffs()
    }

    /**
     * first clear all buffs stored in character stats then
     * store buff effects of all buffs in character
     *
     * first clearing them hopefully makes sure, they are not applied multiple times
     * which used to be a bug
     */
    fun applyBuffs() {
        clearStatChangesFromBuffs()
        buffs.forEach {
//            Log.d("CPI_CHARACTER", "apllying buff $it")
            applyBuff(buff = it)
        }
    }
    /**
     * this either overwrites the buff with same name if already exists in character
     * or creates a new buff and adds it to character
     */
    fun addBuffOrReplace(b: Buff, temp: Boolean) {
        val nName = getFullAttributeNameAndNormalize( b.name)
        val buffList = {(if (temp) tempBuffs else buffs)}

        buffList().find { getFullAttributeNameAndNormalize(it.name) == nName }?.also { exBuff ->
            exBuff.effects.clear()
            exBuff.effects.addAll(b.effects)
            exBuff.type = b.type
            exBuff.logid = b.logid
            exBuff.character = this
//                    Log.d("BUFFF", "updated buff $exBuff")

        } ?: run {
            buffList().add(b)
        }
    }

    /**
     * create buffs for all lootmanager items and logEntries (buffs  = name[x,y])
     */
    fun generateLootBuffs() {
        loot.filter { it.buffs.isNotBlank() }.forEach { item ->
            val bName = item.buffName
            addBuffOrReplace(
                b = Buff().apply {
                    name = bName
                    effects =
                        item.buffs.split(",")
                            .map { Buff.parseBuffEffect(it) }.toMutableList()
                    modifiable = false
                    active = true
                    type = item.category
                    character = this@Character
                },
                temp = false
            )

        }
    }


    /**
     * this means buffed stat if no replacement
     */
    fun getTotalStatValue(statRef: String): Int {
        return getStatReplacementValue(statRef) ?: getBuffedStatValue(statRef)
    }

    fun getStatValue(statRef: String): Int? {
        return stats[getFullAttributeNameAndNormalize(statRef)]
    }
    fun getStatValueOrZero(statRef: String): Int {
        return getStatValue(statRef) ?: 0
    }
    fun getStatReplacementValue(statRef: String): Int? {
        return stats[getStatReplacementRef(statRef)]
    }
    fun getStatReplacementValueOrZero(statRef: String): Int {
        return getStatReplacementValue(statRef) ?: 0
    }

    fun setStat(statRef: String, value: Int) {
        stats[getFullAttributeNameAndNormalize(statRef)] = value
    }

    fun setStatBuff(statRef: String, value: Int) {
        stats[getStatBuffRef(statRef)] = value
    }
    fun setStatReplacement(statRef: String, value: Int) {
        stats[getStatReplacementRef(statRef)] = value
    }

    fun getBuffedStatValue(statRef: String): Int {
        return (getStatValue(statRef)?: 0) + (stats[getStatBuffRef(statRef)] ?: 0)
    }

    fun getStatBuffValue(statRef: String): Int? {
        return stats[getStatBuffRef(statRef)]
    }
    fun getStatChecksBuffValue(statRef: String): Int? {
        return stats[getStatChecksBuffRef(statRef)]
    }

    fun getStatBuffValueOrZero(statRef: String): Int {
        return getStatBuffValue(statRef) ?: 0
    }
    fun getStatChecksBuffValueOrZero(statRef: String): Int {
        return getStatChecksBuffValue(statRef) ?: 0
    }

    fun getStatBuffRef(statRef: String): String {
        val fullRef = getFullAttributeNameAndNormalize(statRef)
        return "${fullRef}_${ATTR_AFFIX_AUGMENT}"
    }

    /**
     * the idea is to simply use the stat ref and add 'checks' in the end
     * buff affix has to come later, because when we want to apply it in buffs
     * always the buff affix is added in the end for the buff stat name
     */
    fun getStatChecksBuffRef(statRef: String): String {
        return getStatBuffRef("$statRef $ATTR_AFFIX_CHECKS")
    }

    fun getStatReplacementRef(statRef: String): String {
        val fullRef = getFullAttributeNameAndNormalize(statRef)
        return "${fullRef}_${ATTR_AFFIX_REPLACEMENT}"
    }

    fun getExtend(key: String, context: Context, default: Boolean = CharProgressItem.DEFAULT_EXTEND_CHAR_PROGRESS_ITEMS): Boolean {
        return extendMap[key]?.get(Helper.getOrientation(context)) ?: default
    }
    fun getExtend(charProgItemfilterConstraintId: Int, context: Context): Boolean {
        val fcEnglish =
            TranslationHelper(context).getEnglishTranslation(charProgItemfilterConstraintId)
        return getExtend(
            key = fcEnglish,
            context = context
        )
    }
    fun toggleExtend(key: String, context: Context) {
        val orientation = Helper.getOrientation(context)
        val map = extendMap[key] ?: HashMap()
        map[orientation] = !getExtend(key, context)
        extendMap[key] = map
    }
    fun toggleExtend(charProgItemfilterConstraintId: Int, context: Context) {
        val fcEnglish =
            TranslationHelper(context).getEnglishTranslation(charProgItemfilterConstraintId)
        toggleExtend(
            key = fcEnglish,
            context = context
        )
    }

    fun toggleActivity() {
        isActive = !isActive
    }

    /**
     * @return icon type of "" it no custom icon is set
     */
    fun getIconType(): String {
        return if ((iconFileName.isNotEmpty()) && iconFileName.contains(".")) {
            iconFileName.substringAfterLast(".")
        } else {
            ""
        }
    }

    fun clearExp() {
        totalExp = 0
        currentExp = 0
    }

    fun getSimpleIconResource(): Int {
        return Global.charIconsMapWErrorIcon[iconName] ?: ERROR_ICON
    }





    fun getSmallIconForLogEntry(context: Context, lineHeight: Int): Drawable? {
        return icon_log_entry ?: kotlin.run {
            val drawable = ContextCompat.getDrawable(
                context,
                getSimpleIconResource());
            val factor= 0.8
            drawable?.setBounds(0, 0, (lineHeight *factor).toInt(), (lineHeight *factor).toInt())
            icon_log_entry = drawable
            return drawable }
    }

    val matrix_device_rating: Int
        get() = getTotalStatValue(BUFF_REF_MATRIX_DEVICE_RATING)

    val matrix_hp: Int
        get() = 8 + ceil(matrix_device_rating.toFloat() / 2).toInt() +
                        getStatBuffValueOrZero(BUFF_REF_MATRIX_HP)

    val featAdapter = charProgressItemsMap.get("@Feat")?.mainItem?.adapter as? CharProgressItemAdapterFeat?


    val hp: Int
        get() = when(ruleSystem) {
            rule_system_sr5, rule_system_sr6 -> {
                8 + ceil(body.toFloat()/2).toInt() + getStatBuffValueOrZero(BUFF_REF_HP)
            }
            rule_system_skillfull_dnd_5E -> {
                (4 + getAttributeMod(
                    ruleSystem = rule_system_skillfull_dnd_5E,
                    statValue = constitution)
                        ) * (level + 2) + getStatBuffValueOrZero(BUFF_REF_HP)
            }
            rule_system_splittermond -> {
                (konstitution +
                        getStatBuffValueOrZero(BUFF_REF_HP) + size)* splittermondHpMultiplier
            }
            rule_system_ogres_club -> {
                val effectiveHpr = dndClass.hpr + getStatBuffValueOrZero(BUFF_REF_HPR)
                val effectiveCon = constitution + getStatBuffValueOrZero(BUFF_REF_CONSTITUTION_FOR_HP_CALC)
                8 + (level) * (
                        effectiveHpr + getAttributeMod(rule_system_ogres_club, effectiveCon)
                        ) +
                        getStatBuffValueOrZero(BUFF_REF_HP)
            }
            else -> 0
        }

    val stun_hp: Int
        get() =
            when(ruleSystem) {
                rule_system_sr5, rule_system_sr6 -> {
                    8 + ceil(willpower.toFloat() / 2).toInt() + getStatBuffValueOrZero(
                        BUFF_REF_STUN_HP
                    )
                }
                else -> 0
            }

    val totalSkillsPerLevel: Int
        get() {
            var total_skills_per_level = 0
            total_skills_per_level += (dndClass.skills_per_lvl + getStatBuffValueOrZero(BUFF_REF_SKILLS_PER_LEVEL))
            if (intelligence > 0) {
                total_skills_per_level += getAttributeMod(ruleSystem, intelligence)
            }
            return max(total_skills_per_level, 1)

        }

    val physicalLimit: Int?
        get() {
            if (getStatValue(ATTR_STRENGTH) != null &&
                getStatValue(ATTR_BODY) != null && getStatValue(ATTR_REACTION) != null) {
                val physical_total_score: Int =
                    (strength*2 + body + reaction)
//                log("CALC_LIMITS", "physical total score = $physical_total_score")
                var limit = physical_total_score / 3
                if (physical_total_score % 3 > 0) limit++
                return limit
            }
            return null
        }

    val mentalLimit: Int?
        get() {
            if (getStatValue(ATTR_LOGIC) != null &&
                getStatValue(ATTR_INTUITION) != null && getStatValue(ATTR_WILLPOWER) != null) {
                val mental_total_score: Int =
                    (logic * 2 + intuition + willpower)
                var limit = mental_total_score / 3
                if (mental_total_score % 3 > 0) limit++
                return limit
            }
            return null
        }

    val socialLimit: Int?
        get() {
            if (getStatValue(ATTR_CHARISMA) != null && getStatValue(ATTR_WILLPOWER) != null) {
                val social_total_score: Int =
                    (charisma * 2 + willpower + ceil(totalEssence).toInt())
                var limit = social_total_score / 3
                if (social_total_score % 3 > 0) limit++
                return limit
            }
            return null
        }

    val social_rating: Int
        get() = charisma + getStatBuffValueOrZero(BUFF_REF_SOCIAL_RATING)

    val composure: Int
        get() = willpower + charisma + getStatBuffValueOrZero(BUFF_REF_COMPOSURE)

    val memory: Int
        get() = logic + intuition + getStatBuffValueOrZero(BUFF_REF_MEMORY)

    val lift_carry: Int
        get() = body + willpower

    val max_lift_weight: Int
        get() = strength * strength * 10

    val max_overhead_weight: Int
        get() = strength * strength * 5


    val judge_intention: Int
        get() = willpower + intuition + getStatBuffValueOrZero(BUFF_REF_JUDGE_INTENTIONS)

    val tradition_attribute: Int
        get() = if (CAST_STAT.isNotBlank()) { getBuffedStatValue(CAST_STAT) } else when {
            archetype.name.lowercase().contains("technomancer") -> logic
            archetype.name.lowercase().contains("shaman") -> charisma
            archetype.name.lowercase().contains("hermetian") -> logic
            else -> 0
        }

    val magic_points: Int
        get() = (2 + getStatBuffValueOrZero(BUFF_REF_MAGIC_POINTS_MULTIPLIER)) * level +
                getStatBuffValueOrZero(BUFF_REF_MAGIC_POINTS)

    val fokusPunkte: Int
        get() = 2*(getBuffedStatValue(ATTR_WILLENSKRAFT) + getBuffedStatValue(ATTR_MYSTIK))*fokusPercentage/100 +
                getStatBuffValueOrZero(BUFF_REF_FOKUS)

    val currentFokusPunkte: Int
        get() = fokusPunkte - (damages[REF_DAMAGE_REF_MAGIC]?.sum() ?: 0)

    val speed: Int
        get() = getBuffedStatValue(ATTR_BEWEGLICHKEIT) + size +
                getStatBuffValueOrZero(BUFF_REF_SPEED) -
                getStatBuffValueOrZero(BUFF_REF_BEHINDERUNG) / 2

    val is_awakened_caster: Boolean
        get() = magic > 0 && tradition_attribute > 0

    val is_technomancer: Boolean
        get() = resonance > 0

    val unarmed_attack_rating: Int
        get() = reaction + strength + getStatBuffValueOrZero(BUFF_REF_UNARMED_ATTACK_RATING)
    val matrix_attack_rating: Int
        get() = matrix_attack + sleaze + getStatBuffValueOrZero(BUFF_REF_MATRIX_ATTACK_RATING)
    val magic_attack: Int
        get() = magic + tradition_attribute
    val defense_rating: Int
        get() = body + armor + getStatBuffValueOrZero(BUFF_REF_DEFENSE_RATING)
    val matrix_defense_rating: Int
        get() = data_processing + firewall + getStatBuffValueOrZero(BUFF_REF_MATRIX_DEFENSE_RATING)
    val defense_roll: Int
        get() = reaction + intuition + getStatBuffValueOrZero(BUFF_REF_DEFENSE_ROLL) - damageMalus
    val initiative: Int
        get() = when (ruleSystem) {
                rule_system_splittermond -> 10 - intuition + getStatBuffValueOrZero(BUFF_REF_INITIATIVE)
                rule_system_sr5, rule_system_sr6 -> reaction + intuition + getStatBuffValueOrZero(BUFF_REF_INITIATIVE)
                rule_system_ogres_club, rule_system_skillfull_dnd_5E, rule_system_Dnd_E5 ->
                    getAttributeMod(ruleSystem = ruleSystem, statValue = dexterity) +
                            getStatBuffValueOrZero(BUFF_REF_INITIATIVE)
                else -> getStatBuffValueOrZero(BUFF_REF_INITIATIVE)
            }


    val armorCheckPenalty: Int
        get() = when (ruleSystem) {
            rule_system_splittermond -> getStatBuffValueOrZero(BUFF_REF_BEHINDERUNG)
            else -> getStatBuffValueOrZero(BUFF_REF_ARMOR_CHECK_PENALTY)
        }

    val matrix_initiative: Int
        get() = intuition + data_processing + getStatBuffValueOrZero(BUFF_REF_MATRIX_INITIATIVE)
    val astral_initiative: Int
        get() = logic + intuition
    val totalEssence: Float
        get() = MAXIMUM_ESSENCE + getStatBuffValueOrZero(BUFF_REF_ESSENCE) - essence_bio - essence_cyber
    val drain_resisitance: Int
        get() = willpower + tradition_attribute + getStatBuffValueOrZero(BUFF_REF_DRAIN_RESISTANCE)

    val damageReduction: Int
        get() = when(ruleSystem) {
            rule_system_ogres_club -> level + getTotalStatValue(STAT_DAMAGE_REDUCTION)
            else -> getTotalStatValue(STAT_DAMAGE_REDUCTION)
        }
    val tik_malus: Int
        get() = getStatBuffValueOrZero(BUFF_REF_TIK)

    data class Mondzeichen(var name: String = DEFAULT_NAME, var description: String = "") {

        fun toJson() = Gson().toJson(HashMap<String, String>().apply {
            put(REF_NAME, name)
            put(REF_DESCRIPTION, description)
        })

        companion object {
            const val DEFAULT_NAME = "keins"
            const val REF_NAME = "name"
            const val REF_DESCRIPTION = "description"
        }
    }

    var mondzeichen: Mondzeichen = Mondzeichen()

    var konstitution: Int
        get() = getTotalStatValue(ATTR_KONSTITUTION)
        set(value) = setStat(ATTR_KONSTITUTION, value)
    var stärke: Int
        get() = getTotalStatValue(ATTR_STAERKE)
        set(value) = setStat(ATTR_STAERKE, value)
    var beweglichkeit: Int
        get() = getTotalStatValue(ATTR_BEWEGLICHKEIT)
        set(value) = setStat(ATTR_BEWEGLICHKEIT, value)
    var ausstrahlung: Int
        get() = getTotalStatValue(ATTR_AUSSTRAHLUNG)
        set(value) = setStat(ATTR_AUSSTRAHLUNG, value)
    var mystik: Int
        get() = getTotalStatValue(ATTR_MYSTIK)
        set(value) = setStat(ATTR_MYSTIK, value)
    var verstand: Int
        get() = getTotalStatValue(ATTR_VERSTAND)
        set(value) = setStat(ATTR_VERSTAND, value)
    var willenskraft: Int
        get() = getTotalStatValue(ATTR_WILLENSKRAFT)
        set(value) = setStat(ATTR_WILLENSKRAFT, value)


    val verteidigung: Int
        get() = 10 + beweglichkeit + stärke + 2 * heldengrad +
                (5 - size) * 2 +
                getStatBuffValueOrZero(BUFF_REF_VERTEIDIGUNG)
    val geistigerWiderstand: Int
        get() = 10 + verstand + willenskraft + 2 * heldengrad +
                getStatBuffValueOrZero(BUFF_REF_GEISTIGER_WIDERSTAND)
    val koerperlicherWiderstand: Int
        get() = 10 + konstitution + willenskraft + 2 * heldengrad +
                getStatBuffValueOrZero(BUFF_REF_KOERPERLICHER_WIDERSTAND)
    val splitterpunkte: Int
        get() = 3 + getStatBuffValueOrZero(BuffEffect.BUFF_REF_SPLITTERPUNKTE) +
                if (heldengrad > 1) 1 else 0


    val traditionAttributeDisplay: String
        get() = capitalize(CAST_STAT).ifBlank {
            if (is_awakened_caster) {
                when {
                    archetype.name.lowercase().contains("shaman") -> ATTR_CHARISMA
                    archetype.name.lowercase().contains("hermetian") -> ATTR_LOGIC
                    else -> "none"
                }
            } else {
                "none"
            }.let { capitalize(it) }
        }

    /**
     * if a custom tradition attribute is set, it is stored here
     * also used to store Cast Stat in skillfull5e
     */
    var CAST_STAT: String = ""
        set(value) { field = getFullAttributeNameAndNormalize(value) }


    fun totalCurrency(extendedWallet: Boolean = false): Float {
        return Item.calcTotalCurrency(items = loot, extendedWallet = extendedWallet).let {
            if (displaySchwarzgeldWealth) {
                it + Item.calcTotalCurrency(items = schwarzloot, extendedWallet = extendedWallet)
            } else {
                it
            }
        }
    }


    //        Helper.log("LANGUAGE_POINTS", "Intelligence = " + intelligence + ", (int - 10) / 2 = " + (intelligence -10)/2);
    val totalLanguagePoints: Int
        get() =//        Helper.log("LANGUAGE_POINTS", "Intelligence = " + intelligence + ", (int - 10) / 2 = " + (intelligence -10)/2);
            1 + getAttributeMod(ruleSystem, intelligence)

    val totalConnectionPoints: Int
        get() = getStatValueOrZero(ATTR_CHARISMA).let { it * it }


    fun setDateOfRetirement(new_date: Date) {

        retired_at = Global.MY_DATE_FORMAT.format(new_date)
    }

    fun getDateOfRetirementAsString(): String {
        return if (retired_at.contains(" ")) {
            retired_at.substring(0, retired_at.indexOf(" "))
        } else {
            ""
        }
    }

    fun getDateOfCreationAsString(): String {
        return if (created_at.contains(" ")) {
            created_at.substring(0, created_at.indexOf(" "))
        } else {
            ""
        }
    }

//    fun reCalcExp(context: Context) {
//        CoroutineScope(Dispatchers.IO).launch {
//            val dbHelper = DatabaseHelper.getInstance(context)
//
//            var cExp = 0
//            var tExp = 0
//            dbHelper?.getCharLogs(id_character = id)?.forEach {e ->
//                cExp += e.exp
//                if (e.exp > 0) {
//                    tExp += e.exp
//                }
//            }
//            currentExp = cExp
//            totalExp = tExp
//            dbHelper?.updateCharacter(character = this@Character, keep_last_update_date = true)
//        }
//    }

    /**
     * delete the icon of the character from internal app folder.
     */
    fun deleteCharIcon() {
        if (iconFileName.isNotEmpty()) {
            val f = File(iconFileName)
            try {
                f.delete()
            } catch (e: IOException) {
                log("ICON_STUFF", "$iconFileName cannot be deleted")
                e.printStackTrace()
            }

        }
    }


    var prefs = HashMap<String, String>()
        set(value) {
            value[PREF_CUSTOM_SKILL_ATTRIBUTES]?.let { customSkillAttributes = parseHashMapStrStr(it) }
            value[PREF_BUFF_STATES]?.let { buffStates = Buff.parseBuffStateMap(it) }
            value[BASE_STAT_MONDZEICHEN]?.let {
                mondzeichen = parseHashMapStrStr(it).let {
                Mondzeichen(
                    name = it[Mondzeichen.REF_NAME] ?: Mondzeichen.DEFAULT_NAME,
                    description = it[Mondzeichen.REF_DESCRIPTION] ?: "") }
            }

            field = value
        }
    fun exportPrefs(pretty: Boolean = false): String {
        prefs[PREF_CUSTOM_SKILL_ATTRIBUTES] = Gson().toJson(customSkillAttributes)
        prefs[PREF_BUFF_STATES] = Gson().toJson(buffStates)
        prefs[BASE_STAT_MONDZEICHEN] = mondzeichen.toJson()
        return GsonBuilder().apply {
            if (pretty) setPrettyPrinting()
        }.create().toJson(prefs)
    }

    private var customSkillAttributes = HashMap<String, String>()

    /**
     * this is expanded and active property of buffs created from logs and items
     */
    var buffStates = HashMap<String, BuffState>()

    var useKmhForSpeed: Boolean
        get() = prefs[PREF_USE_KMH_FOR_SPEED]?.toBoolean() ?: true
        set(value) { prefs[PREF_USE_KMH_FOR_SPEED] = value.toString()}

    var showAllSkills: Boolean
        get() = prefs[PREF_SHOW_ALL_SKILLS]?.toBoolean() ?: false
        set(value) { prefs[PREF_SHOW_ALL_SKILLS] = value.toString()}

    fun toggleShowAllSkills() {
        showAllSkills = !showAllSkills
    }

    var showAttributes: Boolean
        get() = prefs[PREF_SHOW_ATTRIBUTES]?.toBoolean() ?: false
        set(value) { prefs[PREF_SHOW_ATTRIBUTES] = value.toString()}

    fun toggleShowAttributes() {
        showAttributes = !showAttributes
    }

    var showSpellSchools: Boolean
        get() = prefs[PREF_SHOW_SPELL_SCHOOLS]?.toBoolean() ?: false
        set(value) { prefs[PREF_SHOW_SPELL_SCHOOLS] = value.toString()}

    fun toggleShowSpellSchools() {
        showSpellSchools = !showSpellSchools
    }

    var showCastStat: Boolean
        get() = prefs[PREF_SHOW_CAST_STAT]?.toBoolean() ?: false
        set(value) { prefs[PREF_SHOW_CAST_STAT] = value.toString()}
    var displayTreasureInWallet: Boolean
        get() = prefs[PREF_SHOW_DISPLAY_TREASURE_IN_WALLET]?.toBoolean() ?: false
        set(value) { prefs[PREF_SHOW_DISPLAY_TREASURE_IN_WALLET] = value.toString()}
    var showHiddenBuffs: Boolean
        get() = prefs[PREF_SHOW_HIDDEN_BUFFS]?.toBoolean() ?: false
        set(value) { prefs[PREF_SHOW_HIDDEN_BUFFS] = value.toString()}
    var showBuffCategories: Boolean
        get() = prefs[PREF_SHOW_BUFF_CATEGORIES]?.toBoolean() ?: false
        set(value) { prefs[PREF_SHOW_BUFF_CATEGORIES] = value.toString()}
    var showSpecsWithDescriptionOnlyExtended: Boolean
        get() = prefs[PREF_SHOW_SPECS_WITH_DESCRIPTION_ONYL_EXTENDED]?.toBoolean() ?: ruleSystem?.skillSpecsAsVerticalList ?: false
        set(value) { prefs[PREF_SHOW_SPECS_WITH_DESCRIPTION_ONYL_EXTENDED] = value.toString()}

    fun toggleShowHiddenBuffs() {
        showHiddenBuffs = !showHiddenBuffs
    }
    fun toggleShowBuffCategories() {
        showBuffCategories = !showBuffCategories
    }

    fun setCustomSkillAttribute(skillName: String, attributeName: String) {
        customSkillAttributes[getSkillRef(skillName)] =
            attributeName
                .split(" ")
                .joinToString(separator = " ") {
                    getFullAttributeNameAndNormalize(it.trim())
                }

    }

    fun getCustomSkillAttribute(skillName: String): String? {
        return customSkillAttributes[getSkillRef(skillName)]
    }

    fun removeCustomAttribute(skillName: String) {
        customSkillAttributes.remove(getSkillRef(skillName))
    }


    /**
     * returns if BuffState for name has been found (true) or false, if BuffState has been created
     */
    fun updateStoredBuffState(name: String, buffState: BuffState) {
        buffStates[name] = buffState
    }

    fun replaceBuffNameInBuffStates(oldName: String, newName: String) {
        buffStates[oldName]?.also {
            buffStates[newName] = it
            buffStates.remove(oldName)
        }
    }
    fun removeBuffInBuffStates(name: String) {
        buffStates.remove(name)
    }

    /**
     * @param contentUri Uri gotten as Result of intent GET_CONTENT
     * @return the filename of the icon in internal app folder
     */
    fun copyIconToInternalAppFolder(context: Context, contentUri: Uri): String{
        val type = FileUtils.getType(contentUri, context) ?: DEFAULT_ICON_FILE_TYPE
        val path = Helper.getIconPath(name, context, type)
        val target = File(path)
        createFileAndAllParentDirs(target)
        var fis: InputStream? = null
        var fos: FileOutputStream? = null
        try {
            fis = context.contentResolver?.openInputStream(contentUri)
            fos = FileOutputStream(target)
            fis?.copyTo(fos)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        finally {
            fis?.close()
            fos?.close()
        }
        return path.substringAfterLast("/")
    }

    fun getCompleteIconPath(context: Context): String {
        return Companion.getCompleteIconPath(context, iconFileName)
    }

    fun decodeCharIcon(
        context: Context,
        thumbNailQuality: Int,
        use_custom_icon_if_available: Boolean = true
    ): Drawable? {
//        log("ICON_STUFF", "START decode icon")
        if (use_custom_icon_if_available && iconFileName.isNotEmpty()) {
            val iconPath = getCompleteIconPath(context)
//            log("ICON_STUFF", "iconpath = $iconPath")
            if (File(iconPath).exists()) {
                // use iconPath for decoding if available
//                log("ICON_STUFF", "$iconPath file exists")

                return BitmapDrawable(context.resources,
                    decodeSampledBitmapFromFile(iconPath, thumbNailQuality, thumbNailQuality))
            } else {
                // only used for debugging
//                log("ICON_STUFF", "$iconPath does not exist")
            }
        }

        //if no filepath is given, use icon from resources
//        log("ICON_STUFF", "${this.name} icon path was empty of file does not exist. Using icons from resources")
//        log("ICON_STUFF", "END decode icon")

        return ContextCompat.getDrawable(
            context,
            getSimpleIconResource())
    }


    val randomCharIcon: String
        get() {
            val listOfIconNames = ArrayList<String>()
            listOfIconNames.addAll(Global.charIconsMap.keys)
            val i = Random().nextInt(listOfIconNames.size - 1)
            return listOfIconNames[i]
        }


    companion object {
        const val UNDEFINED = "undefined"
        const val MAXIMUM_ESSENCE = 6f
        val PRIORITY_COMPARATOR: Comparator<Character?> =
            nullsLast(Comparator { lhs, rhs -> rhs.priority - lhs.priority })
        val DEAD_COMPARATOR: Comparator<Character> = Comparator { lhs, rhs ->
            if (lhs.isDead == rhs.isDead) {
                0
            } else if (lhs.isDead) {
                1
            } else {
                -1
            }
        }
        val RETIRED_COMPARATOR: Comparator<Character> = Comparator { lhs, rhs ->
            if (lhs.isRetired == rhs.isRetired) {
                0
            } else if (lhs.isRetired) {
                1
            } else {
                -1
            }
        }
        val RETIRED_AND_DEAD_COMPARATOR: Comparator<Character> = Comparator { lhs, rhs ->
            if (lhs.isRetired && rhs.isRetired && lhs.isDead && !rhs.isDead) {
                -1
            } else if (lhs.isRetired && rhs.isRetired && !lhs.isDead && rhs.isDead) {
                1
            } else {
                0
            }
        }
        const val DEFAULT_ICON_FILE_TYPE = "png"
        const val ERROR_ICON = R.drawable.ic_char_dead_head

        fun getFullAttributeNameAndNormalize(attributeRef: String): String {
            val ref = attributeRef.lowercase().replace("%", "").replace(Regex("\\s"), "")
            return attrShortCutsMap[ref] ?: ref
        }

        const val PROGRESSION_REF_FORT_SAVES = "fortitude saves"
        const val PROGRESSION_REF_REFLEX_SAVES = "reflex saves"
        const val PROGRESSION_REF_WILL_SAVES = "will saves"
        const val PROGRESSION_REF_BAB = "bab"



        val attrShortCutsMap = HashMap<String, String>().apply {
            put("str", "strength")
            put("dex", "dexterity")
            put("const", "constitution")
            put("int", "intelligence")
            put("intu", "intuition")
            put("wis", "wisdom")
            put("cha", "charisma")
            put("agi", "agility")
            put("rea", "reaction")
            put("log", "logic")
        }

        /**
         * this map lists all the string resources that are pointing
         * to a given statRef for the buffs
         * this is needed to write buffs in different languages
         * and still find the correct stat to buff
         */
        val attributesRefStringResourcesMap: HashMap<List<Int>, String>
            get() = HashMap<List<Int>, String>().apply {
                put(
                    listOf(
                        R.string.attribute_constitution,
                        R.string.attribute_constitution_short
                    ),
                    Character.ATTR_CONSTITUTION)
                put(
                    listOf(
                        R.string.attribute_dexterity,
                        R.string.attribute_dexterity_short
                    ),
                    Character.ATTR_DEXTERITY)
                put(
                    listOf(
                        R.string.attribute_intelligence,
                        R.string.attribute_intelligence_short
                    ),
                    Character.ATTR_INTELLIGENCE
                )
                put(
                    listOf(
                        R.string.attribute_wisdom,
                        R.string.attribute_wisdom_short
                    ),
                    Character.ATTR_WISDOM
                )
                put(
                    listOf(
                        R.string.attribute_strength,
                        R.string.attribute_strength_short
                    ),
                    Character.ATTR_STRENGTH
                )
                put(
                    listOf(
                        R.string.attribute_charisma,
                        R.string.attribute_charisma_short
                    ),
                    Character.ATTR_CHARISMA
                )
                put(
                    listOf(
                        R.string.attribute_body
                    ),
                    Character.ATTR_BODY
                )
                put(
                    listOf(
                        R.string.attribute_willpower,
                        R.string.attribute_willpower_short
                    ),
                    Character.ATTR_WILLPOWER
                )
                put(
                    listOf(
                        R.string.attribute_logic,
                        R.string.attribute_logic_short
                    ),
                    Character.ATTR_LOGIC
                )
                put(
                    listOf(
                        R.string.attribute_intuition,
                        R.string.attribute_intuition_short
                    ),
                    Character.ATTR_INTUITION
                )
                put(
                    listOf(
                        R.string.attribute_reaction,
                        R.string.attribute_reaction_short
                    ),
                    Character.ATTR_REACTION
                )
                put(
                    listOf(
                        R.string.attribute_agility,
                        R.string.attribute_agility_short
                    ),
                    Character.ATTR_AGILITY
                )
                put(
                    listOf(
                        R.string.attribute_magic
                    ),
                    Character.ATTR_MAGIC
                )
                put(
                    listOf(
                        R.string.attribute_resonance,
                        R.string.attribute_resonance_short
                    ),
                    Character.ATTR_RESONANCE
                )
                put(
                    listOf(
                        R.string.attribute_edge),
                    Character.ATTR_EDGE
                )
            }

        /**
         * move char icon to path according to new char name
         */
        fun renameCustomCharIcon(oldIconPath: String, newIconPath: String) {


            val target = File(newIconPath)
            val origin = File(oldIconPath)

            if (!target.exists()) {
                target.parentFile?.mkdirs()
                target.createNewFile()
            }

            var fis: InputStream? = null
            var fos: FileOutputStream? = null
            try {
                fis = FileInputStream(origin)
                fos = FileOutputStream(target)
                fis.copyTo(fos)
                origin.delete()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                fis?.close()
                fos?.close()
            }
        }

        fun getCompleteIconPath(context: Context, iconFileName: String): String {
            return "${getCharIconsFolderPath(context = context)}/$iconFileName"
        }

        fun getDisplayName(character: Character): String {
            return character.name.substringBefore("alias")
                .replace(Regex("(\\(|\\[).*(\\)|])"), "")
                .trim()

        }

        fun parseDamages(json: String): HashMap<String, IntArray> {
//            Log.d("EXPORT_DAMAGES", "parse damages: $json")
            if (!json.startsWith("{")) {
                Log.e("PARSE_DAMAGES", "invalid damage format -> reset all damages. This maybe because of an update and a change of data structure")
                return HashMap()
            }
            val map: HashMap<String, IntArray> = Gson().fromJson(json, object : TypeToken<HashMap<String, IntArray>>() {}.type)
            return map
        }

        fun exportDamages(map: HashMap<String, IntArray>, pretty: Boolean = false): String {
            return GsonBuilder().apply {
                if (pretty) setPrettyPrinting()
            }.create().toJson(map).also {
//                Log.d("EXPORT_DAMAGES", "export damages called: $it")
            }
        }

        fun parseExtendMap(json: String): HashMap<String, HashMap<Int, Boolean>> {
            val map = HashMap<String, HashMap<Int, Boolean>>()
            try {
                val o = JSONObject(json)
                val itr = o.keys()
                while (itr.hasNext()) {
                    val subMap = HashMap<Int, Boolean>()
                    val keyString = itr.next()
                    val obj = o.getJSONObject(keyString)
                    val objItr = obj.keys()
                    while (objItr.hasNext()) {
                        val keyIntAsStr = objItr.next()
                        subMap[keyIntAsStr.toInt()] = obj.getBoolean(keyIntAsStr)
                    }
                    map[keyString] = subMap
                }


            } catch (e: Exception) {
                e.printStackTrace()
            }

            return map
        }

        fun parseHashMapStrInt(json: String): HashMap<String, Int> {
            return Gson().fromJson(json, object : TypeToken<HashMap<String, Int>>() {}.type)
        }

        fun parsePrefs(json: String): HashMap<String, String> {
            return parseHashMapStrStr(json)
        }

        fun parseHashMapStrStr(json: String): HashMap<String,String> =
            Gson().fromJson(json, object : TypeToken<HashMap<String, String>>() {}.type)

        fun getTotalWealthString(totalWealth: Float): String {
            return when {
                totalWealth == 0f -> ""
                abs(totalWealth) < 1 -> String.format(Locale.GERMAN,"%.1f", totalWealth)
                else -> {
                    var sFactor = when {
                        abs(totalWealth) < 1000 -> ""
                        abs(totalWealth) < 1000000 -> "K"
                        abs(totalWealth) < 1000000000 -> "M"
                        abs(totalWealth) < 1000000000000 -> "B"
                        else -> "B"
                    }
                    var factor = when {
                        abs(totalWealth) < 1000 -> 1
                        abs(totalWealth) < 1000000 -> 1000
                        abs(totalWealth) < 1000000000 -> 1000000
                        abs(totalWealth) < 1000000000000 -> 1000000000
                        else -> 1000000000
                    }
                    String.format(Locale.GERMAN,"%,d", totalWealth.toInt()/factor) + sFactor
                }
            }
        }

        const val HOT_SIM_BUFF_NAME = "Hot SIM"

        // ATTRIBUTE REFS
        const val ATTR_STRENGTH = "strength"
        const val ATTR_DEXTERITY = "dexterity"
        const val ATTR_CONSTITUTION = "constitution"
        const val ATTR_WISDOM = "wisdom"
        const val ATTR_INTELLIGENCE = "intelligence"
        const val ATTR_CHARISMA = "charisma"
        const val ATTR_BODY = "body"
        const val ATTR_AGILITY = "agility"
        const val ATTR_REACTION = "reaction"
        const val ATTR_INTUITION = "intuition"
        const val ATTR_WILLPOWER = "willpower"
        const val ATTR_LOGIC = "logic"
        const val ATTR_EDGE = "edge"
        const val ATTR_RESONANCE = "resonance"
        const val ATTR_MAGIC = "magic"

        const val ATTR_STAERKE = "stärke"
        const val ATTR_AUSSTRAHLUNG = "ausstrahlung"
        const val ATTR_BEWEGLICHKEIT = "beweglichkeit"
        const val ATTR_KONSTITUTION = "konstitution"
        const val ATTR_MYSTIK = "mystik"
        const val ATTR_VERSTAND = "verstand"
        const val ATTR_WILLENSKRAFT = "willenskraft"

        // following stats will only modified by buffs
        const val STAT_DATA_PROCESSING = "data processing"
        const val STAT_MATRIX_ATTACK = "matrix attack"
        const val STAT_SLEAZE = "sleaze"
        const val STAT_FIREWALL = "firewall"
        const val STAT_ARMOR = "armor"
        const val STAT_DAMAGE_REDUCTION = "damage reduction"
        const val BASE_STAT_MONDZEICHEN = "mondzeichen"
        //// -------------------------------------------

        const val STAT_SIZE = "size"

        const val ATTR_AFFIX_AUGMENT = "augment"
        const val ATTR_AFFIX_CHECKS = "checks"
        const val ATTR_AFFIX_REPLACEMENT = "replace"


        const val PREF_USE_KMH_FOR_SPEED = "use_kmh_for_speed"
        const val PREF_SHOW_ALL_SKILLS = "show_all_skills"
        const val PREF_SHOW_ATTRIBUTES = "show_attributes"
        const val PREF_SHOW_SPELL_SCHOOLS = "show_spell_schools"
        const val PREF_SHOW_CAST_STAT = "show_cast_stat"
        const val PREF_SHOW_BUFF_CATEGORIES = "show_buff_categories"
        const val PREF_SHOW_HIDDEN_BUFFS = "show_hidden_buffs"
        const val PREF_SHOW_SPECS_WITH_DESCRIPTION_ONYL_EXTENDED = "show_specs_with_description_only_extended"
        const val PREF_COLORED_DAMAGE_TYPES = "colored_damage_types"
        const val DEFAULT_PREF_COLORED_DAMAGE_TYPES = false
        const val PREF_SHOW_DISPLAY_TREASURE_IN_WALLET = "display_treasure_in_wallet"
        const val PREF_HG_EXP_PERCENTAGE = "splittermond_hg_exp_percentage"
        const val PREF_FOCUS_PERCENTAGE = "splittermond_fokus_percentage"
        const val PREF_SPLITTERMOND_HOMEBREW = "splittermond_homebrew"

        const val PREF_BUFF_STATES = "buff_states"
        const val PREF_CUSTOM_SKILL_ATTRIBUTES = "custom_skill_attributes"

        const val BUFF_TYPE_SR_SPELL_CONCENTRATION = "concentration"

        /**
         * only used when storing buffs!
         * this is used as a suffix for buffs that are only temporarily stored during generation of cpi
         * they are stored (in db) with the normal buffs, but loaded into a different list
         * meaning their boni are not allied as long as they are temp, only after they are copied
         * to the normal bufflist
         */
        const val BUFF_TYPE_TEMP = "___temp"

        const val SPLITTERMOND_HP_MULTIPLIER = 5
        const val SPLITTERMOND_HP_MULTIPLIER_HOME_BREW = 4

        const val REF_DAMAGE_REF_PHYSICAL = "physical"
        const val REF_DAMAGE_REF_STUN = "stun"
        const val REF_DAMAGE_REF_MAGIC = "magic"
        const val REF_DAMAGE_REF_MATRIX = "matrix"

        const val SHARED_PREFS_REF = "shared_preferences"

    }



}
