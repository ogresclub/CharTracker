package com.mtths.chartracker.dataclasses

import com.mtths.chartracker.DatabaseHelper
import kotlin.collections.ArrayList

data class Race (var id: Long = -1, var name: String = "", var link: String = "") {

    constructor(name: String) : this(name = name, id = -1)

    /**
     * this is not filled when loading a character. But when laoding a race, it is filled
     *
     */
    var subraces: ArrayList<String> = ArrayList()

    override fun toString(): String {
        return name
    }

    fun loadSubRaceNames(dbHelper: DatabaseHelper): ArrayList<String> {
        return dbHelper.getSubracesRelatedToRace(race_name = name)
    }

}