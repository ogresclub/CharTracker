package com.mtths.chartracker.dataclasses


data class Subrace(var id: Long = -1,
              var name: String = "",
              var race: String = "") {


    constructor(name: String) : this(name = name, id = -1)

    constructor(name: String, race: String) : this(name = name, race = race, id = -1) {
    }

    override fun toString(): String {
        return name
    }
}