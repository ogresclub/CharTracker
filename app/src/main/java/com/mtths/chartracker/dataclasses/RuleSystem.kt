package com.mtths.chartracker.dataclasses

import android.content.Context
import android.content.res.Configuration
import com.mtths.chartracker.Item
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.adapters.DamageMonitorAdapter
import com.mtths.chartracker.dataclasses.Character.Companion.REF_DAMAGE_REF_MAGIC
import com.mtths.chartracker.dataclasses.Character.Companion.REF_DAMAGE_REF_MATRIX
import com.mtths.chartracker.dataclasses.Character.Companion.REF_DAMAGE_REF_PHYSICAL
import com.mtths.chartracker.dataclasses.Character.Companion.REF_DAMAGE_REF_STUN
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.PathfinderSkillDetailsBuilder
import com.mtths.chartracker.utils.SR5SkillDetailsBuilder
import com.mtths.chartracker.utils.SR6SkillDetailsBuilder
import com.mtths.chartracker.utils.SkillDetailsBuilder
import com.mtths.chartracker.utils.TranslationHelper


class RuleSystem(
    var ref: String,
    var name: String = ref,
    var icon: Int = -1,
    var theme: Int = R.style.AppTheme,
    var backgroundsPortrait: Map<Int, Int> = HashMap<Int, Int>().apply {
        put(0, R.drawable.wallpaper_dnd_0)
        put(1, R.drawable.wallpaper_dnd_1)
        put(2, R.drawable.wallpaper_dnd_2)
        put(3, R.drawable.wallpaper_dnd_3)
        put(4, R.drawable.wallpaper_dnd_4)
    },
    var backgroundLandscape: Int = R.drawable.wallpaper_dnd,
    var woundsNameId: Int = R.string.label_wounds,
    var hasWounds: Boolean = true,
    var hasEdge: Boolean = hasWounds,
    var hasLevel: Boolean = true,
    var skillSpecsAsVerticalList: Boolean = false,
    var showWalletInCharDeatils: Boolean = false,
    var defaultItems: List<Item> = listOf(),
    var stealthSkillResourceId: Int = R.string.stealth_skill_name_default,
    var languagesHaveValue: Boolean = true,
    var showFragmentDamageMonitor: Boolean = true,
    var showFragmentCharItems: Boolean = true,
    var attributesListId: Int =  R.array.dnd_attributes,
    var newCharData: NewCharData = NewCharData(),
    var countFeaturedSkills: Boolean = false,
    /**
     * defining the behaviour of damageMonitors in StatusMonitor
     */
    var damageTypes: LinkedHashMap<String, DamageMonitorData> =
        LinkedHashMap<String, DamageMonitorData>().apply {
            put(REF_DAMAGE_REF_STUN,
                DamageMonitorData(
                    nameId = R.string.label_damage_monitor_stun,
                    damageRef = REF_DAMAGE_REF_STUN
                )
            )
            put(REF_DAMAGE_REF_PHYSICAL,
                DamageMonitorData(
                    nameId = R.string.label_damage_monitor_physical,
                    damageRef = REF_DAMAGE_REF_PHYSICAL
                )
            )
        },
    var defaultLanguage: String = PrefsHelper.LANGUAGE_CODE_EN,
    /**
     * some rule system specific modification of skills
     * e.g. shadowrun: skill rank = 0 counts as -1
     */
    val getmodifiedSkillValue: (Character, ValueNamePair, Int?, Int, Context?) -> Int =
        { c: Character, vnp: ValueNamePair, baseValue: Int?, value: Int, context: Context? -> value },
    val getSkillDetailsBuilder: (c: Character,
                                 valueNamePair: ValueNamePair,
                                 context: Context?) -> SkillDetailsBuilder =
        {c, vnp, cxt -> SkillDetailsBuilder(c, vnp, cxt) }
) {


    /**
     * get the wallpaper resource for the given position in the viewpager
     * depending also on the orientation and size of the view pager
     */
    fun getBackgroundResource(context: Context, position: Int): Int {
        return Helper.getOrientation(context).let { o ->
            if (o == Configuration.ORIENTATION_LANDSCAPE) {
                backgroundLandscape
            } else {
                backgroundsPortrait[position] ?: backgroundDefault
            }
        }
    }

    override fun toString(): String {
        return name
    }

    override fun equals(other: Any?): Boolean {
        return if (other is RuleSystem) other.ref.lowercase() == ref.lowercase() else false
    }

    override fun hashCode(): Int {
        return ref.hashCode()
    }

    companion object {

        val backgroundDefault = R.drawable.wallpaper_dnd_1

        val sr6SkillIsMatrixDamageMalusAffected: (Context, ValueNamePair) -> Boolean =
            {context, valueNamePair -> valueNamePair.name.lowercase() in
                    TranslationHelper(context).getAllTranslations(
                        listOf(
                            R.string.shadowrun6_skill_cracking,
                            R.string.shadowrun6_skill_electronics
                        )
                    ).map { it.lowercase() } }

        val sRSkillManipulator: (Character, ValueNamePair, Int?, Int, Context?) -> Int =
            { c: Character, vnp: ValueNamePair, baseValue: Int?, value: Int, context: Context? ->
                var mValue = value
                if (baseValue == 0) mValue--

                /*
                apply matrix damage malus to cracking skill
                do not apply to electronics, because there you can also use the skill
                in ways, where you do not use a matrix device
                 */

                context?.let { ctx ->
                    if (sr6SkillIsMatrixDamageMalusAffected(ctx, vnp)) {
                        mValue -= c.matrixDamageMalus
                    }
                }

                mValue
            }

        /**
         * if class skill, add 3 to skill total value
         */
        val pathfinderSkillManipulator: (Character, ValueNamePair, Int?, Int, Context?) -> Int =
            { c: Character, vnp: ValueNamePair, baseValue: Int?, value: Int, context: Context? ->
                var mValue = value
                if (baseValue != null && baseValue > 0) {
                    if (c.isClassSkill(vnp.name.lowercase())) {
                        mValue += 3
                    } else {
                        mValue += 1
                    }
                }
                mValue
            }

        val srNewCharData = NewCharData(
            showClass = false,
            showNotes = false,
            showLevel = false,
            showSubRace = false
        )


        fun allRuleSystems(): List<RuleSystem> {
            return listOf(
                rule_system_ogres_club,
                rule_system_Dnd_E5,
                rule_system_skillfull_dnd_5E,
                rule_system_splittermond,
                rule_system_sr5,
                rule_system_sr6)
        }

        val rule_system_ogres_club = RuleSystem(
            ref = "ogres_club",
            name = "Ogres Club",
            icon = R.drawable.ic_rule_system_ogres_club,
            countFeaturedSkills = true,
            showFragmentDamageMonitor = true,
            showFragmentCharItems = true,
            hasEdge = false,
            getmodifiedSkillValue = pathfinderSkillManipulator,
            getSkillDetailsBuilder = {c, vnp, ctx ->
                PathfinderSkillDetailsBuilder(c = c, vnp = vnp, ctx = ctx) },
            damageTypes =
        LinkedHashMap<String, DamageMonitorData>().apply {
            put(REF_DAMAGE_REF_PHYSICAL,
                DamageMonitorData(
                    nameId = R.string.label_damage_monitor_physical,
                    damageRef = REF_DAMAGE_REF_PHYSICAL
                )
            )
        },
        )
        val rule_system_Dnd_E5 = RuleSystem(
            ref = "dnd_5e",
            name = "D&D E5",
            icon = R.drawable.ic_rule_system_dnd_5e,
            showFragmentDamageMonitor = false,
            showFragmentCharItems = true,
            languagesHaveValue = false,
            newCharData = NewCharData(
                showSubRace = false
            ))
        val rule_system_sr5 = RuleSystem(
            ref = "sr5",
            name = "Shadworun5",
            attributesListId = R.array.sr_attributes,
            theme = R.style.AppThemeSR5,
            newCharData = srNewCharData,
            icon = R.drawable.ic_rule_system_sr5,
            showFragmentDamageMonitor = true,
            showFragmentCharItems = false,
            backgroundsPortrait = HashMap<Int, Int>().apply {
                put(0, R.drawable.wallpaper_sr_0_char_view)
                put(1, R.drawable.wallpaper_sr_1)
                put(2, R.drawable.wallpaper_sr_2)
                put(3, R.drawable.wallpaper_sr_3)
                put(4, R.drawable.wallpaper_sr_1)
            },
            backgroundLandscape = R.drawable.wallpaper_sr,
            damageTypes = LinkedHashMap<String, DamageMonitorData>().apply {
                put(REF_DAMAGE_REF_STUN,
                    DamageMonitorData(
                        nameId = R.string.label_damage_monitor_stun,
                        damageRef = REF_DAMAGE_REF_STUN,
                        colCount = 3
                    )
                )
                put(REF_DAMAGE_REF_PHYSICAL,
                    DamageMonitorData(
                        nameId = R.string.label_damage_monitor_physical_shadowrun,
                        damageRef = REF_DAMAGE_REF_PHYSICAL,
                        colCount = 3
                    )
                )
            },
            hasLevel = false,
            getmodifiedSkillValue = sRSkillManipulator,
            getSkillDetailsBuilder = {c, vnp, ctx ->
                SR5SkillDetailsBuilder(c = c, vnp = vnp, ctx = ctx) }
        )
        val rule_system_sr6 = RuleSystem(
            ref = "sr6",
            name = "Shadworun6",
            attributesListId = R.array.sr_attributes,
            theme = R.style.AppThemeSR6,
            newCharData = srNewCharData,
            icon = R.drawable.ic_rule_system_sr6,
            showFragmentDamageMonitor = true,
            showFragmentCharItems = false,
            backgroundsPortrait = HashMap<Int, Int>().apply {
                put(0, R.drawable.wallpaper_sr_0_char_view)
                put(1, R.drawable.wallpaper_sr_1)
                put(2, R.drawable.wallpaper_sr_2)
                put(3, R.drawable.wallpaper_sr_3)
                put(4, R.drawable.wallpaper_sr_1)
            },
            backgroundLandscape = R.drawable.wallpaper_sr,
            damageTypes = LinkedHashMap<String, DamageMonitorData>().apply {
                put(
                    REF_DAMAGE_REF_MATRIX,
                    DamageMonitorData(
                        nameId = R.string.label_damage_monitor_matrix,
                        damageRef = REF_DAMAGE_REF_MATRIX,
                        colCount = 3
                    )
                )
                put(REF_DAMAGE_REF_STUN,
                    DamageMonitorData(
                        nameId = R.string.label_damage_monitor_stun,
                        damageRef = REF_DAMAGE_REF_STUN,
                        colCount = 3
                    )
                )
                put(REF_DAMAGE_REF_PHYSICAL,
                    DamageMonitorData(
                        nameId = R.string.label_damage_monitor_physical_shadowrun,
                        damageRef = REF_DAMAGE_REF_PHYSICAL,
                        colCount = 3
                    )
                )
            },
            hasLevel = false,
            getmodifiedSkillValue = sRSkillManipulator,
            getSkillDetailsBuilder = {c, vnp, ctx ->
                SR6SkillDetailsBuilder(c = c, vnp = vnp, ctx = ctx) }
        )
        val rule_system_skillfull_dnd_5E = RuleSystem(
            ref = "skillfull_5e",
            name = "Skillful D&D",
            icon = R.drawable.ic_rule_system_skillfull_5e,
            showFragmentDamageMonitor = true,
            showFragmentCharItems = true,
            damageTypes = LinkedHashMap<String, DamageMonitorData>().apply {
                put(
                    REF_DAMAGE_REF_MAGIC,
                    DamageMonitorData(
                        nameId = R.string.label_damage_monitor_stun_skillfull_dnd,
                        damageRef = REF_DAMAGE_REF_MAGIC
                    )
                )
                put(REF_DAMAGE_REF_PHYSICAL,
                    DamageMonitorData(
                        nameId = R.string.label_damage_monitor_physical,
                        damageRef = REF_DAMAGE_REF_PHYSICAL
                    )
                )
            },
            hasWounds = true,
            hasEdge = false)
        val rule_system_splittermond = RuleSystem(
            ref = "splittermond",
            name = "Splittermond",
            attributesListId = R.array.splittermond_attributes,
            newCharData = NewCharData(
                showClass = false,
                showNotes = false,
                showLevel = false,
                showSubRace = false,
                showArchetypes = false
            ),
            icon = R.drawable.ic_rule_system_splittermond,
            showFragmentDamageMonitor = true,
            showFragmentCharItems = false,
            backgroundsPortrait = HashMap<Int, Int>().apply {
                put(0, R.drawable.wallpaper_splittermond_0)
                put(1, R.drawable.wallpaper_splittermond_1)
                put(2, R.drawable.wallpaper_splittermond_2)
                put(3, R.drawable.wallpaper_splittermond_3)
            },
            backgroundLandscape = R.drawable.wallpaper_splittermond,
            hasWounds = true,
            languagesHaveValue = false,
            skillSpecsAsVerticalList = false,
            showWalletInCharDeatils = true,
            defaultItems = listOf(
                Item(id = 0, category = "currency", name = "Telare", value = 0.01f),
                Item(id = 1, category = "currency", name = "Lunare", value = 1f),
                Item(id = 2, category = "currency", name = "Solare", value = 100f),
            ),
            stealthSkillResourceId = R.string.splittermond_skill_stealth,
            defaultLanguage = PrefsHelper.LANGUAGE_CODE_DE,
            damageTypes = LinkedHashMap<String, DamageMonitorData>().apply {
                put(
                    REF_DAMAGE_REF_MAGIC,
                    DamageMonitorData(
                        nameId = R.string.label_damage_monitor_stun_splittermond,
                        damageRef = REF_DAMAGE_REF_MAGIC,
                        damages = arrayOf(
                            DamageMonitorAdapter.Damage(
                                nameResourceId = R.string.damage_type_verzehrt,
                                imageResource = R.drawable.red_black_border_crossed_x3,
                                index = 0,
                                textColor = R.color.text_color_indicator_damage_monitor_alternate
                            ),
                            DamageMonitorAdapter.Damage(
                                nameResourceId = R.string.damage_type_erschoepft,
                                imageResource = R.drawable.red_black_border_crossed_x2,
                                index = 1,
                                textColor = R.color.text_color_indicator_damage_monitor_alternate
                            ),
                            DamageMonitorAdapter.Damage(
                                nameResourceId = R.string.damage_type_kanalisiert,
                                imageResource = R.drawable.red_black_border_crossed_x1,
                                index = 2
                            )
                        )
                    )
                )
                put(REF_DAMAGE_REF_PHYSICAL,
                    DamageMonitorData(
                        nameId = R.string.label_damage_monitor_physical,
                        damageRef = REF_DAMAGE_REF_PHYSICAL,
                        splittermondHp = true,
                        damages = arrayOf(
                            DamageMonitorAdapter.Damage(
                                nameResourceId = R.string.damage_type_normal,
                                imageResource = R.drawable.red_black_border_crossed_x3,
                                index = 0,
                                textColor = R.color.text_color_indicator_damage_monitor_alternate
                            ),
                            DamageMonitorAdapter.Damage(
                                nameResourceId = R.string.damage_type_stun,
                                imageResource = R.drawable.red_black_border_crossed_x2,
                                index = 1,
                                textColor = R.color.text_color_indicator_damage_monitor_alternate
                            ),
                            DamageMonitorAdapter.Damage(
                                nameResourceId = R.string.damage_type_kanalisiert,
                                imageResource = R.drawable.red_black_border_crossed_x1,
                                index = 2
                            )
                        )
                    )
                )
            }
        )

        val rule_system_default = rule_system_ogres_club
    }

    //todo even don't use those predefined possible attribute,
    // but define name and onclick option as well as auto complete proposals
    // here
    class NewCharData(
        val showNotes: Boolean = true,
        val showArchetypes: Boolean = true,
        val showClass: Boolean = true,
        val showRace: Boolean = true,
        val showSubRace: Boolean = true,
        val showLevel: Boolean = true
    )

}




