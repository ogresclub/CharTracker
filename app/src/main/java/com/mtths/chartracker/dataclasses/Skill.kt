package com.mtths.chartracker.dataclasses

import android.content.Context
import android.content.res.Resources
import android.util.Log
import com.mtths.chartracker.R
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterValueBased
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_Dnd_E5
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_ogres_club
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_skillfull_dnd_5E
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_splittermond
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_sr5
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_sr6
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.capitalizeNewsLetterStyle

class Skill(
    name: String = "",
    /**
     * multiple attribute should be separated by space
     */
    var attributes: String?,
    var type: String = SKILL_DEFAULT_TYPE):
    CharProgressItemData(name = name) {

        val attributesList: List<String>?
            get() = attributes?.split(" ")

    override fun toString(): String {
        return "Skill(name=$name,attr=$attributes,type=$type)"
    }

    fun copy(): Skill = Skill(
        name = name,
        attributes = attributes,
        type = type
    )

    /**
     * if integer is handed use getString to retrieve the string
     */
    constructor(values: Array<String>, context: Context) :
            this(
                name = values[0].let {
                    capitalizeNewsLetterStyle(
                        it.toIntOrNull()?.let {context.resources.getString(it)} ?: it)
                },
                attributes = values.clone().drop(1).dropLast(1)
                    .joinToString(separator = " ") { it.toIntOrNull()?.let {context.resources.getString(it)} ?: it },
                type = values.last().let {
                    it.toIntOrNull()?.let { context.resources.getString(it) } ?: it })

    companion object {
        const val SKILL_DEFAULT_TYPE = "general"

        fun getSkillMap(arrayId: Int, context: Context): HashMap<String, Skill> {
            val map = HashMap<String, Skill>()
            val res: Resources = context.resources
            val ta = res.obtainTypedArray(arrayId)
            val n = ta.length()
            for (i in 0 until n) {
                val id = ta.getResourceId(i, 0)
                if (id > 0) {
                    val skill = Skill(res.getStringArray(id), context)
//                    Log.e("PRASING_SKILLS","}}}}}}}}}}}}}}}}}loaded Skill: $skill")
                    map[skill.name.lowercase()] = skill
                } else {
                    Log.e("PARSING_SKILLS", "something wrong with skill at position $i")
                }
            }
            ta.recycle() // Important!
            return map
        }

        fun getBaseSkillRef(skillName: String): String {
            return getSkillRef(Helper.ignoreBracketsAndDigits(skillName))
        }

        fun getSkillRef(skillName: String): String = skillName.lowercase()

        private fun getCustomAttributeOrFallBack(
            name: String,
            character: Character?,
            attributeFallback: String = "",
            defaultAttributes: String?
        ): String? {
            return character?.getCustomSkillAttribute(name)
                ?: defaultAttributes?.ifBlank { attributeFallback }?.ifBlank { null }
        }

        /**
         * If character has defined a custom attribute, it will be replaced here.
         * @param attributeFallback will be used if attribute of skill is blank
         */
        fun getSkill(
            name: String,
            skillData: HashMap<RuleSystem, HashMap<String, Skill>>,
            character: Character?,
            attributeFallback: String = ""
        ): Skill? {
            val nameRef = getBaseSkillRef(name)
            character?.let { c ->
                c.ruleSystems?.forEach{ rS ->
                    skillData[rS]?.get(nameRef)?.let {

                        return it.copy().apply {
                            // copy makes sure, we don't edt any data in skillData
                            // otherwise we will overwrite attributes with custom attributes (if available)
                            // but we want to replaced attributes only to be stored in our value name pair,
                            // not in skillData
                            attributes = getCustomAttributeOrFallBack(
                                name = name,
                                character = character,
                                attributeFallback = attributeFallback,
                                defaultAttributes = this.attributes
                            )
                        }
                        //skill is not in skill data, so there is no info about it
                        // we have to create new skill
                    } ?: return Skill(
                        name = name,
                        attributes = getCustomAttributeOrFallBack(
                            name = name,
                            character = character,
                            attributeFallback = attributeFallback,
                            defaultAttributes = null
                        )
                    )
                }
            }

            return null
        }

        fun getSkillValue(nameId: Int,
                     character: Character?,
                     context: Context): Int = getSkillValue(
            skillName = context.getString(nameId),
            character = character,
            context = context)

        fun getSkillValue(
            skillName: String,
            character: Character?,
            context: Context): Int {
            return character?.let { c ->
                val name = skillName
                val skill = getSkill(
                    name = name,
                    character = c,
                    skillData = getSkillData(context)
                )
                CharProgressItemAdapterValueBased.calcTotalSkillValue(
                    ValueNamePair(
                        name = name,
                        skill = skill,
                        mCalculator = object : CharProgressItemAdapterValueBased.Calculator {}),
                    character = character,
                    context = context
                ).totalValue
            } ?: 0
        }

        fun getSkillWAttrCastStatFallback(
            name: String,
            skillData: HashMap<RuleSystem, HashMap<String, Skill>>,
            /**
             * attribute fallback will be used if attribute of skill is blank
             */
            character: Character?): Skill? {
            return getSkill(
                name = name,
                skillData = skillData,
                character = character,
                attributeFallback = character?.let {
                    if (it.showCastStat &&
                        character.ruleSystems?.contains(rule_system_skillfull_dnd_5E) == true ) {
                        it.CAST_STAT
                    } else {
                        ""
                    }
                } ?: "")
        }


        /**
         * skill names are all lowercase here!
         */
        fun getSkillData(context: Context): HashMap<RuleSystem, HashMap<String, Skill>> =
            HashMap<RuleSystem, HashMap<String, Skill>>().apply {
                this[rule_system_ogres_club] = getSkillMap(
                    arrayId = R.array.ogres_club_skill_list,
                    context = context
                )
                this[rule_system_Dnd_E5] = getSkillMap(
                    arrayId = R.array.dnd_5e_skill_list,
                    context = context
                )
                this[rule_system_sr5] = getSkillMap(
                    arrayId = R.array.shadowrun5_skill_list,
                    context = context
                )
                this[rule_system_sr6] = getSkillMap(
                    arrayId = R.array.shadowrun6_skill_list,
                    context = context
                )
                this[rule_system_skillfull_dnd_5E] = getSkillMap(
                    arrayId = R.array.skills_skillfull_5e,
                    context = context
                )
                this[rule_system_splittermond] = getSkillMap(
                    arrayId = R.array.skills_splittermond,
                    context = context
                )
            }

    }

}
