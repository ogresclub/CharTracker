package com.mtths.chartracker.dataclasses

import android.content.Context
import android.util.Log
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.Buff.Companion.DEFAULT_ACTIVE
import com.mtths.chartracker.dataclasses.Buff.Companion.DEFAULT_EXPAND
import com.mtths.chartracker.dataclasses.Buff.Companion.DEFAULT_HIDE
import com.mtths.chartracker.dataclasses.Buff.Companion.DEFAULT_MODIFIABLE
import com.mtths.chartracker.dataclasses.Buff.Companion.DEFAULT_NAME
import com.mtths.chartracker.dataclasses.Buff.Companion.DEFAULT_TYPE
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.getStat
import com.mtths.chartracker.dataclasses.Character.Companion.STAT_ARMOR
import com.mtths.chartracker.dataclasses.Character.Companion.STAT_DAMAGE_REDUCTION
import com.mtths.chartracker.dataclasses.Character.Companion.STAT_DATA_PROCESSING
import com.mtths.chartracker.dataclasses.Character.Companion.STAT_FIREWALL
import com.mtths.chartracker.dataclasses.Character.Companion.STAT_MATRIX_ATTACK
import com.mtths.chartracker.dataclasses.Character.Companion.STAT_SLEAZE
import com.mtths.chartracker.utils.CharProgressItemMap
import com.mtths.chartracker.utils.StringUtils
import com.mtths.chartracker.utils.StringUtils.getValue
import org.json.JSONArray
import org.json.JSONObject
import java.util.regex.Pattern

open class BuffState(
    open var active: Boolean = DEFAULT_ACTIVE,
    open var expand: Boolean = DEFAULT_EXPAND,
    open var hide: Boolean = DEFAULT_HIDE) {

    override fun toString(): String {
        return "{active:$active,expand:$expand, hide:$hide}"
    }
}

/**
 * This class contains only the data of the buff we want to export to json
 */
open class ExportBuff(var name: String = DEFAULT_NAME,
                      var effects: MutableList<BuffEffect> = mutableListOf(),
                      var modifiable: Boolean = DEFAULT_MODIFIABLE,
                      var type: String = DEFAULT_TYPE,
                      var description: String = "") : BuffState()

class Buff(
    var character: Character? = null,
    var logid: Long? = null) : ExportBuff() {

    override var expand: Boolean = DEFAULT_EXPAND
        get() = character?.buffStates?.get(name)?.expand ?: field
        set(value) {
            field = value
            character?.updateStoredBuffState(
                name = name, buffState = BuffState(
                    active = this.active, expand = value, hide = this.hide
                )
            )
        }

    override var active: Boolean = DEFAULT_ACTIVE
        get() = character?.buffStates?.get(name)?.active ?: field
        set(value) {
            field = value
            character?.updateStoredBuffState(
                name = name, buffState = BuffState(
                    active = value, expand = this.expand, hide = this.hide
                )
            )
        }

    override var hide: Boolean = DEFAULT_HIDE
        get() = character?.buffStates?.get(name)?.hide ?: field
        set(value) {
            field = value
            character?.updateStoredBuffState(
                name = name, buffState = BuffState(
                    active = this.active, expand = this.expand, hide = value
                )
            )
        }


    fun toExportBuff(): ExportBuff {
        return ExportBuff(
            effects = mutableListOf<BuffEffect>().apply {
                addAll(this@Buff.effects)
            },
            modifiable = this@Buff.modifiable,
            type = this@Buff.type,
            description = this@Buff.description
        ).apply {
            name = this@Buff.name
            expand = this@Buff.expand
            active = this@Buff.active
            hide = this@Buff.hide
        }
    }

    override fun toString(): String {
        return "$name[" + effects.joinToString() + "]"
    }

    /**
     * returns false, if the category the buff is in, is collapsed
     */
    fun show(context: Context, character: Character?): Boolean {
        return character?.getExtend(
            key = extendCategoryKey,
            context = context,
            default = true
        ) ?: true
    }

    val extendCategoryKey: String
        get() = BUFF_CATEGORY_KEY_PREFIX + type


    companion object {
        fun parseBuffs(json: String): ArrayList<Buff> {
            val buffs = ArrayList<Buff>()
            try {
                val values = JSONArray(json)
                for (i in 0 until values.length()) {
                    val jbuff = values.getJSONObject(i)
                    val buff = Buff()
                    var ref = "name"
                    if (jbuff.has(ref)) buff.name = jbuff.getString(ref)
                    ref = "description"
                    if (jbuff.has(ref)) buff.description = jbuff.getString(ref)
                    ref = "active"
                    if (jbuff.has(ref)) buff.active = jbuff.getBoolean(ref)
                    ref = "expand"
                    if (jbuff.has(ref)) buff.expand = jbuff.getBoolean(ref)
                    ref = "hide"
                    if (jbuff.has(ref)) buff.hide = jbuff.getBoolean(ref)
                    ref = "modifiable"
                    if (jbuff.has(ref)) buff.modifiable = jbuff.getBoolean(ref)
                    ref = "relatedLogId"
                    if (jbuff.has(ref)) buff.logid = jbuff.getLong(ref)
                    ref = "type"
                    if (jbuff.has(ref)) buff.type = jbuff.getString(ref)
                    ref = "effects"
                    if (jbuff.has(ref)) jbuff.getJSONArray(ref).let {fxs ->
                        for (j in 0 until fxs.length()) {
                            val jfx = fxs.getJSONObject(j)

                            val fx = BuffEffect()
                            ref = "stat"
                            if (jfx.has(ref)) fx.stat = jfx.getString(ref)
                            ref = "value"
                            if (jfx.has(ref)) fx.value = jfx.getInt(ref)
                            ref = "replace"
                            if (jfx.has(ref)) fx.replace = jfx.getBoolean(ref)
                            ref = "note"
                            if (jfx.has(ref)) fx.note = jfx.getString(ref)

                            buff.effects.add(fx)
                        }
                    }
                    buffs.add(buff)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return buffs
        }

        fun parseBuffStateMap(json: String): HashMap<String, BuffState> {
//            Log.d("BUFFF", "parsing buffstate from json: $json")
            val buffStates = HashMap<String, BuffState>()
            try {
                val o = JSONObject(json)
                val itr = o.keys()
                while (itr.hasNext()) {
                    val keyString = itr.next()
                    val obj = o.getJSONObject(keyString)
                    buffStates[keyString] = parseBuffState(obj)
                }
            } catch (e: Exception) {
                Log.e("PARSE_BUFF_STATES", "Error parsing buff states.")
                e.printStackTrace()
            }

            return buffStates

        }


        fun parseBuffState(jbuffState: JSONObject): BuffState {

            fun loge(ref: String) {
                Log.e("BUFFF", "parse buff stat error. no $ref found")

            }

            val buffState = BuffState()
            var ref = "active"
            if (jbuffState.has(ref)) {
                buffState.active = jbuffState.getBoolean(ref)
            } else {
                loge(ref)
            }
            ref = "expand"
            if (jbuffState.has(ref)) {
                buffState.expand = jbuffState.getBoolean(ref)
            } else {
                loge(ref)
            }
            ref = "hide"
            if (jbuffState.has(ref)) {
                buffState.hide = jbuffState.getBoolean(ref)
            } else {
                loge(ref)
            }

            return buffState
        }



        fun parseBuffEffect(s: String): BuffEffect {
            return BuffEffect(stat = getStat(s)).apply {
                //deal with value
                getValue(s)?.let { value = it }
                replace = s.contains("=")
                note = BuffEffect.getNote(s)
            }
        }

        /**
         * extract Buff from text.
         * Buff should be written as: buffname[buffs1,buffs2,...]
         */
        fun parseBuffFromText(s: String): Buff {
            var name = s.substringBefore("[", )
            val buffEffectStringList = StringUtils.getSurroundedContent(s)
                .split(",")
                .filterNot { it.isBlank() }
            val buffEffects = buffEffectStringList.map { parseBuffEffect(it) }
            if (name.isBlank()) {
                name = buffEffects.joinToString { it.stat }
            }

            return Buff().apply {
                this.name = name
                effects.addAll(buffEffects)
            }

        }

        const val DEFAULT_ACTIVE = false
        const val DEFAULT_EXPAND = false
        const val DEFAULT_HIDE = false
        const val DEFAULT_MODIFIABLE = false
        const val DEFAULT_TYPE = ""
        const val DEFAULT_NAME = ""
        const val BUFF_CATEGORY_KEY_PREFIX = "buff_category:"

    }
}

/**
 * this represents one of maybe multiple effects of a buff
 */
data class BuffEffect(
    var stat: String = "",
    var value: Int = 0,
    var replace: Boolean = false,
    var note: String = "") {

    fun valueToString(): String {
        var s = ""
        if (replace) {
            s += "="
        } else{
            if (value > 0) s += "+"
        }
        s += value
        return s
    }

    fun getNameDisplay(): String {
        return addNoteString(stat)
    }

    private fun addNoteString(s: String): String {
        return s + if (note.isNotBlank()) " [$note]" else ""
    }

    override fun toString(): String {
        return addNoteString("$stat ${valueToString()}")
    }

    companion object {
        fun getStat(s: String): String {
            return s.replace(Regex("[-+\\d=]*"), "")
                .replace(Regex("\\[.*?]"),"")
                .trim()
        }

        fun getNote(s: String): String {
            val m = PATTERN_BUFF_EFFECT_NOTE.matcher(s)
            var note = ""
            if (m.find()) {
                m.group(1)?.trim()?.let { note = it }
            }
            return note
        }

        val PATTERN_BUFF_EFFECT_NOTE = Pattern.compile("\\[(.*)]")

        val INVALID_VALUE_INDICATOR = -666

        const val BUFF_REF_SOCIAL_RATING = "social rating"
        const val BUFF_REF_HP = "hp"
        const val BUFF_REF_NEGATIVE_HP = "negative hp"
        const val BUFF_REF_SKILLS_PER_LEVEL = "skills per level"
        const val BUFF_REF_HPR = "hpr"
        const val BUFF_REF_ARMOR_CHECK_PENALTY = "armor check penalty"
        const val BUFF_REF_CONSTITUTION_FOR_HP_CALC = "constitution for hp calc"
        const val BUFF_REF_RESIST_DMG = "resist dmg impact"
        const val BUFF_REF_RESIST_STUN_DMG = "resist stun dmg impact"
        const val BUFF_REF_STUN_HP = "stun hp"
        /**
         * this value reduces the amount of dmg that is used to calc the malus in checks due to damage
         */
        const val BUFF_REF_MAX_OVERFLOW = "max overflow"
        const val BUFF_REF_ADEPT_POINTS = "adept points"
        const val BUFF_REF_INI_DICE = "ini dice"
        const val BUFF_REF_MINOR_ACTIONS = "minor actions"
        const val BUFF_REF_ESSENCE = "essence"
        const val BUFF_REF_ASTRAL_INI_DICE = "astral ini dice"
        const val BUFF_REF_MATRIX_INI_DICE = "matrix ini dice"
        const val BUFF_REF_DEFENSE_ROLL = "defense roll"
        const val BUFF_REF_MATRIX_INITIATIVE = "matrix ini"
        const val BUFF_REF_INITIATIVE = "ini"
        const val BUFF_REF_UNARMED_ATTACK_RATING = "unarmed attack rating"
        const val BUFF_REF_MATRIX_ATTACK_RATING = "matrix attack rating"
        const val BUFF_REF_DEFENSE_RATING = "defense rating"
        const val BUFF_REF_MATRIX_DEFENSE_RATING = "matrix defense rating"
        const val BUFF_REF_COMPOSURE = "composure"
        const val BUFF_REF_MEMORY = "memory"
        const val BUFF_REF_JUDGE_INTENTIONS = "judge intentions"
        const val BUFF_REF_DRAIN_RESISTANCE = "drain resistance"
        const val BUFF_REF_FREE_CONCENTRATION = "free concentration"
        const val BUFF_REF_MATRIX_DEVICE_RATING = "matrix device rating"
        const val BUFF_REF_MATRIX_HP = "matrix hp"

        const val BUFF_REF_MAGIC_POINTS = "magic points"
        const val BUFF_REF_MAGIC_POINTS_MULTIPLIER = "magic points multiplier"

        const val BUFF_REF_VERTEIDIGUNG = "verteidigung"
        const val BUFF_REF_BEHINDERUNG = "behinderung"
        const val BUFF_REF_GEISTIGER_WIDERSTAND = "geistiger widerstand"
        const val BUFF_REF_KOERPERLICHER_WIDERSTAND = "körperlicher widerstand"
        const val BUFF_REF_FOKUS = "fokus"
        const val BUFF_REF_SPEED = "speed"
        const val BUFF_REF_SIZE = "size"
        const val BUFF_REF_TIK = "tik"
        const val BUFF_REF_SPLITTERPUNKTE = "splitterpunkte"
        const val BUFF_REF_ALL_CHECKS = "all checks"
        const val BUFF_REF_HEALTH_LEVEL = "gesundheitsstufe"


        //    don't forget to add these to autocompletion list (Helper.fillWithStdListToAutoComplete)    }

        fun getBuffableStats(ruleSystems: List<RuleSystem>, cpiMap: CharProgressItemMap): List<String> {
            return mutableListOf<String>().apply {
                add(BUFF_REF_ALL_CHECKS)

                if (RuleSystem.rule_system_sr6 in ruleSystems) {
                    add(BUFF_REF_RESIST_DMG)
                    add(BUFF_REF_RESIST_STUN_DMG)
                    add(BUFF_REF_HP)
                    add(BUFF_REF_STUN_HP)
                    add(BUFF_REF_MAX_OVERFLOW)
                    add(BUFF_REF_ADEPT_POINTS)
                    add(STAT_ARMOR)
                    add(BUFF_REF_INI_DICE)
                    add(BUFF_REF_MINOR_ACTIONS)
                    add(BUFF_REF_ESSENCE)
                    add(BUFF_REF_ASTRAL_INI_DICE)
                    add(BUFF_REF_MATRIX_INI_DICE)
                    add(BUFF_REF_SOCIAL_RATING)
                    add(BUFF_REF_MATRIX_INITIATIVE)
                    add(STAT_FIREWALL)
                    add(STAT_DATA_PROCESSING)
                    add(STAT_SLEAZE)
                    add(STAT_MATRIX_ATTACK)
                    add(BUFF_REF_DEFENSE_RATING)
                    add(BUFF_REF_DEFENSE_ROLL)
                    add(BUFF_REF_INITIATIVE)
                    add(BUFF_REF_UNARMED_ATTACK_RATING)
                    add(BUFF_REF_MATRIX_ATTACK_RATING)
                    add(BUFF_REF_MATRIX_DEFENSE_RATING)
                    add(BUFF_REF_COMPOSURE)
                    add(BUFF_REF_MEMORY)
                    add(BUFF_REF_JUDGE_INTENTIONS)
                    add(BUFF_REF_DRAIN_RESISTANCE)
                    add(BUFF_REF_FREE_CONCENTRATION)
                    add(BUFF_REF_MATRIX_DEVICE_RATING)
                    add(BUFF_REF_MATRIX_HP)
                }
                if (RuleSystem.rule_system_ogres_club in ruleSystems) {
                    add(BUFF_REF_HP)
                    add(BUFF_REF_MAX_OVERFLOW)
                    add(BUFF_REF_HPR)
                    add(BUFF_REF_CONSTITUTION_FOR_HP_CALC)
                    add(BUFF_REF_NEGATIVE_HP)
                    add(Character.PROGRESSION_REF_REFLEX_SAVES)
                    add(Character.PROGRESSION_REF_FORT_SAVES)
                    add(Character.PROGRESSION_REF_WILL_SAVES)
                    add(Character.PROGRESSION_REF_BAB)
                    add(BUFF_REF_SKILLS_PER_LEVEL)
                    add(BUFF_REF_INITIATIVE)
                    add(BUFF_REF_ARMOR_CHECK_PENALTY)
                    add(STAT_DAMAGE_REDUCTION)
                }
                if (RuleSystem.rule_system_skillfull_dnd_5E in ruleSystems) {
                    add(BUFF_REF_MAGIC_POINTS)
                    add(BUFF_REF_MAGIC_POINTS_MULTIPLIER)
                    add(BUFF_REF_HP)
                }
                if (RuleSystem.rule_system_splittermond in ruleSystems) {
                    add(BUFF_REF_VERTEIDIGUNG)
                    add(BUFF_REF_BEHINDERUNG)
                    add(STAT_DAMAGE_REDUCTION)
                    add(BUFF_REF_KOERPERLICHER_WIDERSTAND)
                    add(BUFF_REF_GEISTIGER_WIDERSTAND)
                    add(BUFF_REF_FOKUS)
                    add(BUFF_REF_HP)
                    add(BUFF_REF_SPEED)
                    add(BUFF_REF_SIZE)
                    add(BUFF_REF_TIK)
                    add(BUFF_REF_INITIATIVE)
                    add(BUFF_REF_SPLITTERPUNKTE)
                    add(BUFF_REF_HEALTH_LEVEL)
                }
                listOf(R.string.filter_constraint_attribute, R.string.filter_constraint_skill).forEach { fCId ->
                    cpiMap[fCId]?.mainItem?.adapter?.createAutoCompleteStrings()?.let {
                        addAll(it)
                        if (fCId == R.string.filter_constraint_attribute) {
                            // for all attributes add buff to all checks related to this attribute
                            addAll(it.map {  "$it ${Character.ATTR_AFFIX_CHECKS}" })
                        }
                    }
                }
            }
        }
    }
}