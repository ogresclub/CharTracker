package com.mtths.chartracker.dataclasses

open class ImagedText(val text: String, val icon: Int)
