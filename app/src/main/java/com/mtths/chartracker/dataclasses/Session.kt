package com.mtths.chartracker.dataclasses

import android.content.Context
import android.text.SpannableStringBuilder
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_default

/**
 * Created by mtths on 04.04.17.
 */
data class Session(
    var name: String = "",
    var isActive: Boolean = false,
    var ruleSystem: RuleSystem = rule_system_default,
    var showTotalExp: Boolean = true,
    var showCurrentExp: Boolean = true,
    var language: String = PrefsHelper.DEFAULT_PREF_APP_LANGUAGE
) {

    var id: Long = -1
    var created_at: String = ""

    fun toggleActivity() {
        isActive = !isActive
    }

    override fun toString(): String {
        return "$name ($ruleSystem)"
    }

    fun getDisplaySpannable(context: Context, lineHeight: Int): SpannableStringBuilder {
        return if (PrefsHelper(context).showRuleSystemIcons) {
            Helper.generateImageSpannable(
                context = context, lineHeight = lineHeight, icon = ruleSystem.icon
            )
        } else {
            SpannableStringBuilder()
        }
            .append(name)
    }
}