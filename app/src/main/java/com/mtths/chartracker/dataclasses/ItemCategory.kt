package com.mtths.chartracker.dataclasses

import com.mtths.chartracker.Item
import com.mtths.chartracker.R

data class ItemCategory(
    val slot: String,
    var getIconImageResource: (item: Item) -> Int = { R.drawable.ic_loot },
    /**
     * optional categories will not be displayed as empty slot,
     * when no item is owned by the character in this slot
     *
     * only relevant for categories listed in this.slots
     */
    val optional: Boolean = false) {

    fun useMapping(map: HashMap<Array<String>, Int>, defaultIconResource: Int) {
        getIconImageResource = {item ->
            var res = defaultIconResource
                val searchString = item.getNameAndTypString().lowercase()
                for (e in map.entries) {
                    for (w in e.key) {
                        if (searchString.lowercase().contains(w)) {
                            res = e.value
                            break
                        }
                    }
                }
            res
        }
    }


    companion object {

        val walletCategories = listOf("currency")
        val extendedWalletCategories = listOf("treasure")

        fun getCategories(stash: List<Item>?): List<String> {
            val l = stash?.map { it.category }?.distinct()?.toMutableList() ?: mutableListOf()
            l.add("all")
            return l
        }

        fun createCategoriesMap() : HashMap<String, ItemCategory> {
            return HashMap<String, ItemCategory>().apply {
                var name = "eyes"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_eyes }))
                name = "amulet"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_amulet }))
                name = "armor"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_armor }))
                name = "hand"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_hand }))
                name = "belt"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_belt }))
                name = "boots"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_boots }))
                name = "head"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_head }))
                name = "cloak"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_cloak }))
                name = "chest"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_shirt }))
                name = "arm"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_arm }))
                name = "ring"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_ring }))

                // slots, where empty will not be displayed
                name = "ranged"
                var map = HashMap<Array<String>, Int>().apply {
                    put(arrayOf("crossbow", "armburst"), R.drawable.ic_loot_crossbow)
                    put(arrayOf("arrows", "pfeile", "bolzen", "bolts"), R.drawable.ic_loot_arrows)
                    put(arrayOf("bullet", "kugel"), R.drawable.ic_loot_bullets)
                }
                var default = R.drawable.ic_loot_bow
                put(name, ItemCategory(slot = name, optional = true)
                    .apply { useMapping(map = map, defaultIconResource = default) }
                )

                name = "weapon"
                map = HashMap<Array<String>, Int>().apply {
                    put(arrayOf("sword", "schwert", "blade", "händer"), R.drawable.ic_loot_sword)
                    put(arrayOf("club", "keule"), R.drawable.ic_loot_club)
                    put(arrayOf("spear", "speer"), R.drawable.ic_loot_spear)
                    put(arrayOf("hammer"), R.drawable.ic_loot_hammer)
                    put(arrayOf("mace", "kolben", "morgenstern", "morningstar"),
                        R.drawable.ic_loot_mace
                    )
                    put(arrayOf("flail", "flegel"), R.drawable.ic_loot_flail)
                    put(arrayOf("scythe", "sense"), R.drawable.ic_loot_scythe)
                    put(arrayOf("sling", "schleuder"), R.drawable.ic_loot_sling)
                    put(arrayOf("pick", "spitzhacke"), R.drawable.ic_loot_pick)
                    put(arrayOf("halbard", "hellebarde"), R.drawable.ic_loot_halberd)
                    put(arrayOf("rapier","scimitar"), R.drawable.ic_loot_rapier)
                    put(arrayOf("brassknuckles"), R.drawable.ic_loot_brass_knuckles)
                    put(arrayOf("staff", "stab"), R.drawable.ic_loot_staff)
                    put(arrayOf("dagger", "dolch", "knife", "messer", "kukri", "katar"),
                        R.drawable.ic_loot_dagger
                    )
                }
                default = R.drawable.ic_loot_axe
                put(name,
                    ItemCategory(slot = name, optional = true)
                        .apply { useMapping(map = map, defaultIconResource = default) }
                )

                name = "rod"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_rod },
                    optional = true)
                )

                name = "shield"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_shield },
                    optional = true)
                )

                /*
                 * Categories for backpack
                 */
                name = "potion"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_potion }))

                name = "oil"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_potion }))

                name = "charm"
                map = HashMap<Array<String>, Int>().apply {
                    put(arrayOf("beard", "bart"), R.drawable.ic_loot_beard)
                }
                default = R.drawable.ic_loot_charm
                put(name,
                    ItemCategory(slot = name, optional = true)
                        .apply { useMapping(map = map, defaultIconResource = default) }
                )

                name = "scroll"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_scroll }))

                name = "soulstone"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_soul_stone }))

                name = "spellbook"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_spell_book }))

                name = "treasure"
                map = HashMap<Array<String>, Int>().apply {
                    put(arrayOf("portfolio", "paper", "papier", "document"),
                        R.drawable.ic_loot_documents
                    )
                    put(arrayOf("diamond", "diamant", "gem", "Edelstein"),
                        R.drawable.ic_loot_diamond
                    )
                    put(arrayOf("gems", "edelsteine"), R.drawable.ic_loot_gems)
                }
                default = R.drawable.ic_loot_treasure
                put(name, ItemCategory(slot = name)
                    .apply { useMapping(map = map, defaultIconResource = default) }
                )

                name = "gem"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_diamond }))

                name = "orb"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_unstable_orb }))

                name = "wand"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_wand }))

                name = "staff"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_staff }))

                name = "currency"
                map = HashMap<Array<String>, Int>().apply {
                    put(arrayOf("copper"), R.drawable.ic_loot_copper_coins)
                    put(arrayOf("telare"), R.drawable.ic_loot_copper_coins)
                    put(arrayOf("silver"), R.drawable.ic_loot_silver_coins)
                    put(arrayOf("lunare"), R.drawable.ic_loot_silver_coins)
                    put(arrayOf("gold"), R.drawable.ic_loot_gold_coins)
                    put(arrayOf("solare"), R.drawable.ic_loot_gold_coins)
                    put(arrayOf("platin"), R.drawable.ic_loot_platin_coins)
                    put(arrayOf("schwarzgeld"), R.drawable.ic_loot_schwarzgeld)
                    put(arrayOf("diamond", "diamant", "gem", "Edelstein"),
                        R.drawable.ic_loot_diamond
                    )
                    put(arrayOf("gems", "edelsteine"), R.drawable.ic_loot_gems)

                }
                default = R.drawable.ic_loot_coins
                put(name, ItemCategory(slot = name)
                    .apply { useMapping(map = map, defaultIconResource = default) }
                )

                name = "wonderous"
                map = HashMap<Array<String>, Int>().apply {
                    put(arrayOf("stone", "stein", "pearl", "perle"), R.drawable.ic_loot_pearl)
                    put(arrayOf("manual", "book", "buch"), R.drawable.ic_loot_book)
                    put(arrayOf("page", "seite"), R.drawable.ic_loot_documents)
                    put(arrayOf("beutel", "bag"), R.drawable.ic_loot_bag)
                    put(arrayOf("feather", "feder"), R.drawable.ic_loot_feather)
                    put(arrayOf("drum", "trommel"), R.drawable.ic_char_drum)
                    put(arrayOf("broom", "besen"), R.drawable.ic_loot_broom)
                    put(arrayOf("cauldron", "kessel"), R.drawable.ic_loot_cauldron)
                    put(arrayOf("key", "schlüssel"), R.drawable.ic_loot_key)
                    put(arrayOf("coin", "münze"), R.drawable.ic_loot_coins)
                    put(arrayOf("cauldron", "kessel"), R.drawable.ic_loot_cauldron)
                    put(arrayOf("shell", "muschel"), R.drawable.ic_loot_shell)
                    put(arrayOf("flask", "flasche"), R.drawable.ic_loot_potion)
                    put(arrayOf("fiddle", "geige", "fiedel"), R.drawable.ic_loot_violin)
                    put(arrayOf("orb", "kugel"), R.drawable.ic_loot_unstable_orb)

                }
                default = R.drawable.ic_loot
                put(name, ItemCategory(slot = name)
                    .apply { useMapping(map = map, defaultIconResource = default) }
                )

                name = "ammunition"
                map = HashMap<Array<String>, Int>().apply {
                    put(arrayOf("bullets", "kugeln"), R.drawable.ic_loot_bullets)
                }
                default = R.drawable.ic_loot_arrows
                put(name, ItemCategory(slot = name)
                    .apply { useMapping(map = map, defaultIconResource = default) }
                )

                name = "graft"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_eyeball }))

                name = "document"
                put(name, ItemCategory(slot = name, getIconImageResource = { R.drawable.ic_loot_documents }))
            }
        }
    }

}