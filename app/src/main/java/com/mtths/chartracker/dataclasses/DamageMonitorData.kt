package com.mtths.chartracker.dataclasses

import android.os.Parcel
import android.os.Parcelable
import com.mtths.chartracker.adapters.DamageMonitorAdapter

class DamageMonitorData(
    val nameId: Int? = null,
    /**
     * when no nameId is given, use the name string
     * (this is needed for dynamically created damage monitors, e.g. for cpi (vehicle...)
     * because there names are created by used and can thus have no string resource
     */
    val name: String = "",
    /**
     * different types of damage that sum up to total damage of the given damage ref
     * e.g. Splittermond
     */
    val damages: Array<DamageMonitorAdapter.Damage>? = null,
    /**
     * the ref string under which the damage is stored in damages (physical, stun, matirx...)
     *
     */
    val damageRef: String,
    /**
     * if null rows will fill parent
     */
    val colCount: Int? = null,
    /**
     * in splittermond, you have a given number of line with length = hp
     * this makes sure, that the colcount (rowlength) divides the hp
     */
    val splittermondHp: Boolean = false,
    val hp: Int = 0
): Parcelable {

    constructor(parcel: Parcel) : this(
        nameId = parcel.readValue(Int::class.java.classLoader) as? Int,
        name = parcel.readString() ?: "",
        damages = parcel.createTypedArray(DamageMonitorAdapter.Damage.CREATOR),
        damageRef = parcel.readString() ?: "",
        colCount = parcel.readValue(Int::class.java.classLoader) as? Int,
        splittermondHp = parcel.readByte() != 0.toByte(),
        hp = parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(nameId)
        parcel.writeString(name)
        parcel.writeTypedArray(damages, flags)
        parcel.writeString(damageRef)
        parcel.writeValue(colCount)
        parcel.writeByte(if (splittermondHp) 1 else 0)
        parcel.writeInt(hp)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<DamageMonitorData> {
        override fun createFromParcel(parcel: Parcel): DamageMonitorData {
            return DamageMonitorData(parcel)
        }

        override fun newArray(size: Int): Array<DamageMonitorData?> {
            return arrayOfNulls(size)
        }
    }
}