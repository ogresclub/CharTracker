package com.mtths.chartracker.dataclasses

import java.util.HashMap
import kotlin.collections.ArrayList

class FeatLink(
    var name: String = "",
    var links: ArrayList<String> = ArrayList()
) {
    var id: Long = -1
    var id_string: String = normalize(name)


    constructor(name: String, link: String): this(name = name) {
        links.add(link)
    }

    constructor(values: Array<String>) : this(values[0], values[1]) {
        if (values.size > 2) {
            for (i in 2 until values.size) {
                links.add(values[i])
            }
        }
    }

    fun addLink(link: String) {
        links.add(link)
    }

    fun addAllLinks(featLinks: ArrayList<FeatLink>) {
        for (featLink in featLinks) {
            links.addAll(featLink.links)
        }
    }

    companion object {
        fun normalize(s: String): String {
            return s.toLowerCase().replace("[\\s-]".toRegex(), "_").replace("[^a-zA-Z0-9_]".toRegex(), "")
        }
    }
}