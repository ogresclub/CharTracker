package com.mtths.chartracker.dataclasses

import android.content.Context
import android.content.res.Resources
import android.util.Log
import com.mtths.chartracker.R
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.StringUtils.getCPIData
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getListFromText
import com.mtths.chartracker.utils.TranslationHelper

class SpellDnD(
    name: String = "",
    var level: Int = 0,
    var school: String = "",
    var tags: List<String> = listOf()) : CharProgressItemData(name = name) {

    constructor(a: Array<String>) : this(
        name = a[0],
        level = a[1].toInt(),
        school = a[2],
        tags = a[3].split(",")

    )

    val ref: String
        get() = getSpellRef(name)


    fun getLink(ruleSystem: RuleSystem): String {
        return when (ruleSystem) {
            RuleSystem.rule_system_splittermond -> SPELL_URL_TEMPLATE_SPLITTERMOND + Helper.capitalize(ref).replace("-", "_")
            else -> SPELL_URL_TEMPLATE_DND_5e + ref
        }
    }
    val link: String
        get() = SPELL_URL_TEMPLATE_DND_5e + ref


    override fun parseName(logEntryText: String, item: CharProgressItem?) {
        super.parseName(logEntryText, item)
        name = name.replace("’", "")
    }

    /**
     * parse the tags from the string or check if spell with name exists in spellMap
     * and use its tags
     */
    fun parseTags(s: String, spellMap: Map<String, SpellDnD>) {
        tags = getListFromText(REF_TAGS, s).ifEmpty {
            getSpell(name, spellMap)?.tags ?: listOf()
        }
    }

    /**
     * parse the school from the string or check if spell with name exists in spellMap
     * and use its school
     */
    fun parseSchool(s: String, spellMap: Map<String, SpellDnD>) {
        school = (getCPIData(dataName = REF_SCHOOL, text = s) ?: "").ifEmpty {
            getSpell(name, spellMap)?.school ?: ""
        }
    }

    /**
     * parse the level from the string or check if spell with name exists in resources
     * and use its level
     */
    fun parseLevel(s: String, context: Context?) {
        level = getIntegerStatFromText(REF_LEVEL, s).toIntOrNull()
            ?: context?.let { getSpell(name, context) }?.level
                    ?: 0
    }


    /**
     * parse the level from the string or check if spell with name exists in spellmap
     * and use its level
     */
    fun parseLevel(s: String, spellMap: Map<String, SpellDnD>) {
        level = getIntegerStatFromText(REF_LEVEL, s).toIntOrNull()
            ?: getSpell(name, spellMap)?.level
                    ?: 0
    }

    companion object {
        const val REF_LEVEL = "level"
        const val REF_SCHOOL = "school"
        const val REF_TAGS = "tags"
        const val SPELL_URL_TEMPLATE_DND_5e = "https://a5e.tools/spell/"
        const val SPELL_URL_TEMPLATE_SPLITTERMOND = "https://splitterwiki.de/wiki/"

        fun getSpellMap(context: Context): HashMap<String, SpellDnD> {
            val map = HashMap<String, SpellDnD>()
            val res: Resources = context.resources
            val ta = res.obtainTypedArray(R.array.spells_dnd_5e)
            val n = ta.length()
            for (i in 0 until n) {
                val id = ta.getResourceId(i, 0)
                if (id > 0) {
                    val spell = SpellDnD(res.getStringArray(id))
//                    Log.e("PRASING_SPELLS","}}}}}}}}}}}}}}}}}loaded Spell: $spell")
                    map[spell.ref] = spell
                } else {
                    Log.e("PARSING_SKILLS", "something wrong with spell at position $i")
                }
            }
            ta.recycle() // Important!
            return map
        }

        fun getSpell(name: String, spells: Map<String, SpellDnD>): SpellDnD? {
            return spells[getSpellRef(name)]
        }

        fun getSpell(name: String, context: Context): SpellDnD? {
            val res: Resources = context.resources
            val ta = res.obtainTypedArray(R.array.spells_dnd_5e)
            val n = ta.length()
            for (i in 0 until n) {
                val id = ta.getResourceId(i, 0)
                if (id > 0) {
                    val ref = getSpellRef(res.getStringArray(id)[0])
                    if (ref == getSpellRef(name)) {
                        return SpellDnD(res.getStringArray(id)).also {
                            ta.recycle()
                        }
                    }
                } else {
                    Log.e("PARSING_SKILLS", "something wrong with spell at position $i")
                }
            }
            ta.recycle()
            return null// Important!
        }

        fun getSpellRef(sName: String): String {
            return sName
                .lowercase()
                .replace(Regex("\\s"), "-")
                .replace("’", "")
                .replace("'", "")
        }
    }
}