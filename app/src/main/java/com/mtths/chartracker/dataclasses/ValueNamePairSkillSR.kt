package com.mtths.chartracker.dataclasses

import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSkillsSR
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSkillsSR.Calculator
import com.mtths.chartracker.utils.Helper.getSkillGroup
import com.mtths.chartracker.utils.Helper.getSortedSkills
import com.mtths.chartracker.utils.Helper.log
import java.util.*
import kotlin.collections.ArrayList

class ValueNamePairSkillSR(val name: String,
                           var value: Int = 0,
                           var hasError: Boolean = false,
                           var mCalculator: Calculator,
                           var skill: Skill? = null) {

    /**
     * e.g. augmentation by ware or whatever
     */
    var augmentation = 0
    var noCalcErrors = true

    /**
     * tells you if i could be that you have put log entries not in correct order.
     * e.g. first int 20 und dann int 18 obwohl für int 18
     */
    var was_reduced = false
    var was_reduced_and_you_even_spend_exp_for_it = false
    var list_of_values = ArrayList<Int>()
    var specializations = ArrayList<Specialization>()


    fun addValue(
        new_value: Int,
        exp: Int,
        ignoreExpCheck: Boolean,
        data: ArrayList<ValueNamePairSkillSR>,
        new_specializations: ArrayList<Specialization>,
        useSkillGroups: Boolean,
    ): Boolean {
        val logging = false
        val DEBUG_TAG = CharProgressItemAdapterSkillsSR.DEBUG_TAG + "__ADD_SKILL_TO_VALUE"

        log(DEBUG_TAG, "specs to add: ${new_specializations.joinToString()}", logging)
        var exp = exp
        val sg = if (useSkillGroups) {
            getSkillGroup(name)
        } else {
            null
        }
        var old_values_of_skill_group: ArrayList<ValueNamePairSkillSR> = ArrayList()
        if (sg != null) {
            old_values_of_skill_group = getSortedSkills(sg.skills, data, mCalculator)
        }
        var this_value_was_reduced_and_you_even_spent_exp_for_it = false
        var this_value_has_errors = false
        val previous_value = value


        /*
        Helper.log("CharDetails", "ValueNamePairSkill.Add: " + this.name + " old/new value: " + previous_value + "/" + new_value +
        " karma: " +  karma + " expected karma: " + mCalculator.getExpCost(previous_value, new_value));
        */if (!ignoreExpCheck) {
            val value_increased = new_value > previous_value
            val value_decreased = new_value < previous_value
            if (value_decreased) {
                was_reduced = true
                if (exp > 0) {
                    was_reduced_and_you_even_spend_exp_for_it = true
                    this_value_was_reduced_and_you_even_spent_exp_for_it = true
                }
            }
            //first remove specializations cost from exp
            exp -= mCalculator.getSpecializationCost(
                newSpecialization = new_specializations,
                oldSpecializations = specializations)
            val res = ValueNamePair.addSpecializations(
                new_specializations = new_specializations,
                mSpecializations = specializations,
                ruleSystem = RuleSystem.rule_system_sr5
            )
            val newSpecsAdded = res.addedSpecs
            val newExpertAdded = res.addedExpert
            log(DEBUG_TAG,
                "added number of new specializations to skill $name: $newSpecsAdded", logging)
            log(DEBUG_TAG,
                "added number of new expertises to skill $name: $newExpertAdded", logging)




            if (exp > 0 && value_increased) {
                if (sg != null) {
                    log(DEBUG_TAG,
                        "skill = " + name + ", prev value = " + previous_value + ", new_value = " + new_value + ", -e.exp = " + exp
                                + ", calced exp = " + mCalculator.calcExpWithSkillGroups(
                            old_values_of_skill_group,
                            new_value,
                            name
                        ),
                        logging
                    )
                    if (exp != mCalculator.calcExpWithSkillGroups(
                            old_values_of_skill_group,
                            new_value,
                            name
                        )
                    ) {
                        log(DEBUG_TAG, "FOUND ERROR", logging)
                        noCalcErrors = false
                        this_value_has_errors = true
                    } else {
                        log(DEBUG_TAG, "NO ERROR", logging)
                    }
                } else {
                    if (exp != mCalculator.getExpCost(previous_value, new_value)) {
                        noCalcErrors = false
                        this_value_has_errors = true
                    }
                }
            }
        }
        value = new_value
        list_of_values.add(value)
        return ignoreExpCheck || !this_value_has_errors && !this_value_was_reduced_and_you_even_spent_exp_for_it
    }

    override fun toString(): String {
        return "$name, $value, $list_of_values"
    }

    companion object {
        val PAIR_INTEGER_STRING__STRING_COMPARATOR =
            Comparator<ValueNamePairSkillSR> { o1, o2 -> o1.name.compareTo(o2.name) }
        val PAIR_INTEGER_STRING__VALUE_COMPARATOR__INVERS =
            Comparator<ValueNamePairSkillSR> { o1, o2 -> o2.value - o1.value }
    }
}