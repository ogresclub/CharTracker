package com.mtths.chartracker.dataclasses

import java.util.*

/**
 * A simply Tag for a log Entry
 */
data class Tag(var id: Long = -1, var name: String = "") {
    ///////VARIABLES //////////
    var created_at: String = ""



    companion object {
        //COMPARATOR
        val TAG_COMPARATOR: Comparator<Tag> = object : Comparator<Tag> {
            override fun compare(lhs: Tag, rhs: Tag): Int {
                return lhs.name.compareTo(rhs.name)
            }
        }
    }
}