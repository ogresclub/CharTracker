package com.mtths.chartracker

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.util.Xml
import android.widget.EditText
import android.widget.Toast
import androidx.core.net.toUri
import androidx.documentfile.provider.DocumentFile
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ActivityMain.ImportFromXmlTask
import com.mtths.chartracker.activities.ActivityMain.ProgressPublisher
import com.mtths.chartracker.fragments.FragmentMainScreen.OnLogEntryAddedListener
import com.mtths.chartracker.utils.Helper.getDatabasePath
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.utils.Helper.sortChars
import com.mtths.chartracker.dataclasses.*
import com.mtths.chartracker.dialogs.DialogDataOptions
import com.mtths.chartracker.utils.FileUtils
import com.mtths.chartracker.utils.FileUtils.copyUris
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.HttpUtils
import com.mtths.chartracker.parsers.Parsers.bookExpAndMoney
import com.mtths.chartracker.parsers.Parsers.calcMoneyFromText
import com.mtths.chartracker.preferences.PrefsHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import org.xmlpull.v1.XmlSerializer
import java.io.*
import java.util.*
import kotlin.collections.LinkedHashMap

/**
 * a class to help managing database access.
 */

/*
 *I needed to modify the SqLiteOpenHelper because I got probs with changing some locale and I needed
 * to fi the getWritableDatabase method
 */

class DatabaseHelper (var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    /**
     * checks how many databases are opened at the moment in order to make sure, we only open one at a time.
     */
    private var mOpenCounter: Int = 0
    private var mDatabase: SQLiteDatabase? = null

    @Synchronized
    fun openDatabase(): SQLiteDatabase? {
        mOpenCounter++
        if (mOpenCounter == 1) { // Opening new database
            mDatabase = writableDatabase
        }
//                Helper.log(DEBUG_TAG, "db.version = " + mDatabase?.version);
        return mDatabase
    }

    @Synchronized
    fun closeDatabase() {
        mOpenCounter--
        if (mOpenCounter == 0) { // Closing database
            mDatabase?.close()
        }
    }

    /*
     *******************   XML Tags & Attributes for im- and export
     */

    val TAG_DATA: String = "data"
    //Session Tags
    val TAG_SESSION_DATA: String = "session_data"
    //Character Tags
    val TAG_HERO: String = "hero"
    val TAG_RACE: String = "race"
    val TAG_SUBRACE: String = "subrace"
    val TAG_CLASS: String = "class"
    val TAG_CURRENT_COMMIT_HASH = "current_commit_hash"
    val TAG_ARCHETYPE: String = "archetype"
    val TAG_LEVEL: String = "level"
    val TAG_PRIORITY: String = "priority"
    val TAG_DAMAGES: String = "damages"
    val TAG_IS_DELETED: String = "deleted"
    val TAG_IS_RETIRED: String = "retired"
    val TAG_IS_DEAD: String = "dead"
    val TAG_ICON: String = "icon"
    val TAG_ICON_PATH: String = "icon_path"
    val TAG_LOOT: String = "loot"
    val TAG_BUFFS: String = "buffs"
    val TAG_STATS: String = "stats"
    val TAG_PREFS: String = "prefs"
    val TAG_TRADITION_ATTRIBUTE: String = "tradition_attribute"
    val TAG_DISPLAY_SCHWARZGELD_WEALTH: String = "display_schwarzgeld_wealth"
    val TAG_SCHWARZLOOT: String = "schwarzloot"
    val TAG_LOOTMANAGER_NAME = "lootmanager_name"
    val TAG_EXPAND_CHAR_PROG_ITEM_DICT: String = "expand_char_progress_item_dict"
    val TAG_ATTRIBUTE_POINT_COST: String = "attribute_point_cost"
    val TAG_RULE_SYSTEM = "rule_system"
    val TAG_SHOW_TOTAL_EXP = "show_total_exp"
    val TAG_SHOW_CURRENT_EXP = "show_current_exp"
    val TAG_LANGUAGE = "language"
    val TAG_CURRENT_EDGE: String = "current_edge"
    val ATTR_RETIRED_AT: String = "retired_at"
    //LogEntry Tags
    val TAG_TAG: String = "tag"
    val TAG_EXP: String = "exp"
    val TAG_TEXT: String = "text"
    val TAG_CHARACTER: String = "character"
    val TAG_SESSION: String = "session"
    val TAG_LOG_ENTRY: String = "logEntry"
    //common Attributes
    val ATTR_CREATED_AT: String = "created_at"
    val ATTR_LAST_EDIT_DATE: String = "last_edited"
    val ATTR_NAME: String = "name"

    val CUSTOM_ICON_INDICATOR = "file://"
    val ERROR_ICON = "dead_head"

    override fun onCreate(db: SQLiteDatabase) { // creating required tables
        db.execSQL(CREATE_TABLE_CHARACTER)
        db.execSQL(CREATE_TABLE_TAG)
        db.execSQL(CREATE_TABLE_LOG)
        db.execSQL(CREATE_TABLE_SESSION)
        db.execSQL(CREATE_TABLE_LOG_TAG)
        db.execSQL(CREATE_TABLE_LOG_CHARACTER)
        db.execSQL(CREATE_TABLE_LOG_SESSION)
        db.execSQL(CREATE_TABLE_FEATS)
        db.execSQL(CREATE_TABLE_RACES)
        db.execSQL(CREATE_TABLE_CLASSES)
        db.execSQL(CREATE_TABLE_ARCHETYPES)
        db.execSQL("CREATE UNIQUE INDEX $UNIQUE_INDEX_ARCHETYPES_NAME ON $TABLE_ARCHETYPES ($KEY_NAME)")
        db.execSQL(CREATE_TABLE_CLASSES_ARCHETYPES)
        db.execSQL(CREATE_TABLE_SUBRACES)
        db.execSQL("CREATE UNIQUE INDEX $UNIQUE_INDEX_SUBRACE_NAME ON $TABLE_SUBRACES ($KEY_NAME)")
        db.execSQL(CREATE_TABLE_RACE_SUBRACE)
        //        Helper.log(DEBUG_TAG, "database version after creating tables = " + db.getVersion());
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion <= 1 && newVersion > 1) db.execSQL(CREATE_TABLE_FEATS)
        if (oldVersion <= 2 && newVersion > 2) {
            val createNewColumn = "ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_RETIRED_AT DATETIME DEFAULT ''"
            db.execSQL(createNewColumn)
        }
        if (oldVersion <= 3 && newVersion > 3) {
            db.execSQL(CREATE_TABLE_RACES)
            db.execSQL(CREATE_TABLE_CLASSES)
            db.execSQL(CREATE_TABLE_ARCHETYPES)
            db.execSQL(CREATE_TABLE_CLASSES_ARCHETYPES)
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_RACE TEXT DEFAULT 'Dwarf'")
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_CLASS TEXT DEFAULT 'Sentinel'")
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_ARCHETYPE TEXT DEFAULT 'Bastion'")
        }
        if (oldVersion <= 4 && newVersion > 4) {
            db.execSQL(CREATE_TABLE_SUBRACES)
            db.execSQL(CREATE_TABLE_RACE_SUBRACE)
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_SUBRACE TEXT DEFAULT 'Mountain-Dwarf'")
        }
        if (oldVersion <= 5 && newVersion > 5) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_LEVEL INTEGER DEFAULT 6")
        }
        if (oldVersion <= 6 && newVersion > 6) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_ATTRIBUTE_POINT_COST INTEGER DEFAULT 300")
        }
        if (oldVersion <= 7 && newVersion > 7) {
            db.execSQL("ALTER TABLE $TABLE_LOG ADD COLUMN $KEY_LAST_UPDATED_DATE DATETIME DEFAULT '2000-01-01 10:10:10.100'")
        }
        if (oldVersion <= 8 && newVersion > 8) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_LAST_UPDATED_DATE DATETIME DEFAULT '2000-01-01 10:10:10.100'")
        }
        if (oldVersion <= 9 && newVersion > 9) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_ICON_PATH TEXT DEFAULT ''")
        }
        if (oldVersion <= 10 && newVersion > 10) {
            val hashOfLatestCommit = HttpUtils.getHashOflatestCommit()
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_CURRENT_COMMIT TEXT DEFAULT '${hashOfLatestCommit}'")
        }
        if (oldVersion <= 11 && newVersion > 11) {
            db.execSQL("CREATE UNIQUE INDEX $UNIQUE_INDEX_ARCHETYPES_NAME ON $TABLE_ARCHETYPES ($KEY_NAME)")
        }
        if (oldVersion <= 12 && newVersion > 12) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_LOOT TEXT DEFAULT ''")
        }
        if (oldVersion <= 13 && newVersion > 13) {
            resetRacesAndSubRaces()
            db.execSQL("CREATE UNIQUE INDEX $UNIQUE_INDEX_SUBRACE_NAME ON $TABLE_SUBRACES ($KEY_NAME)")
        }
        if (oldVersion <= 14 && newVersion > 14) {
            db.execSQL("ALTER TABLE $TABLE_SESSION ADD COLUMN $KEY_RULE_SYSTEM TEXT " +
                    "DEFAULT '${RuleSystem.rule_system_ogres_club.ref}'")
        }
        if (oldVersion <= 15 && newVersion > 15) {
            db.execSQL("ALTER TABLE $TABLE_SESSION ADD COLUMN $KEY_SHOW_TOTAL_EXP TEXT DEFAULT 1")
            db.execSQL("ALTER TABLE $TABLE_SESSION ADD COLUMN $KEY_SHOW_CURRENT_EXP TEXT DEFAULT 1")
        }
        if (oldVersion <= 16 && newVersion > 17) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_EXPAND_CHAR_PROG_ITEM_DICT TEXT DEFAULT '{}'")
        }
        if (oldVersion < 18 && newVersion >= 18) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_DAMAGES INTEGER DEFAULT 0")
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_STUN_DAMAGE INTEGER DEFAULT 0")
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_MONEY INTEGER DEFAULT 0")
        }
        if (oldVersion < 19 && newVersion >= 19) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_CHAR_RULE_SYSTEMS TEXT DEFAULT ''")
        }
        if (oldVersion < 20 && newVersion >= 20) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_LOOTMANAGER_NAME TEXT DEFAULT ''")
        }
        if (oldVersion < 21 && newVersion >= 21) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_USE_MANAGER_ACCESS TEXT DEFAULT 0")
        }
        if (oldVersion < 22 && newVersion >= 22) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_DISPLAY_SCHWARZGELD_WEALTH TEXT DEFAULT 0")
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_SCHWARZLOOT TEXT DEFAULT ''")
        }
        if (oldVersion < 23 && newVersion >= 23) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_CURRENT_EDGE TEXT DEFAULT 3")
        }
        if (oldVersion < 24 && newVersion >= 24) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_BUFFS TEXT DEFAULT ''")
        }
        if (oldVersion < 25 && newVersion >= 25) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_STATS TEXT DEFAULT ''")
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_TRADITION_ATTRIBUTE TEXT DEFAULT ''")
        }
        if (oldVersion < 26 && newVersion >= 26) {
            db.execSQL("ALTER TABLE $TABLE_CHARACTER ADD COLUMN $KEY_PREFS TEXT DEFAULT ''")
        }
        if (oldVersion < 27 && newVersion >= 27) {
            db.execSQL("ALTER TABLE $TABLE_SESSION ADD COLUMN $KEY_LANGUAGE TEXT DEFAULT 'en'")
        }
        if (oldVersion < 28 && newVersion >= 28) {
            // change type of columen phsical and stun damage to text because they are now stored as array
            val tablecharacters = "characters"
            val createTableCharacters = "CREATE TABLE characters(" +
                    "id INTEGER PRIMARY KEY, name TEXT, total_exp INTEGER, current_exp INTEGER, " +
                    "priority INTEGER, is_active INTEGER, is_retired INTEGER, is_dead INTEGER, " +
                    "is_deleted INTEGER, created_at DATETIME, retired_at DATETIME, " +
                    "last_updated_date DATETIME, icon_id TEXT, race TEXT, subrace TEXT, " +
                    "class TEXT, archetype TEXT, exp_point_cost INTEGER, level INTEGER, " +
                    "icon_path TEXT, loot TEXT, schwarzloot TEXT, buffs TEXT, stats TEXT, " +
                    "prefs TEXT, tradition_attribute TEXT, current_commit TEXT, " +
                    "expand_char_progress_item_dict TEXT, physical_damage TEXT, stun_dmg TEXT, " +
                    "money INTEGER, character_rule_systems TEXT, loot_manager_name TEXT, " +
                    "use_manager_access TEXT, display_schwarzgeld_wealth TEXT, current_edge INTEGER)"
            val characterColumns = "id,name,total_exp,current_exp,priority,is_active,is_retired,is_dead,is_deleted" +
                    ",created_at,retired_at,last_updated_date,icon_id,race,subrace,class,archetype" +
                    ",exp_point_cost,level,icon_path,loot,schwarzloot,buffs,stats,prefs," +
                    "tradition_attribute,current_commit,expand_char_progress_item_dict," +
                    "physical_damage,stun_dmg,money,character_rule_systems,loot_manager_name," +
                    "use_manager_access,display_schwarzgeld_wealth,current_edge"
            db.beginTransaction()
            db.execSQL(
            "CREATE TEMPORARY TABLE c_backup($characterColumns);" +
            "INSERT INTO c_backup SELECT $characterColumns FROM $tablecharacters;" +
            "DROP TABLE $tablecharacters;" +
            createTableCharacters +
            "INSERT INTO $tablecharacters SELECT $characterColumns FROM c_backup;" +
            "DROP TABLE c_backup;" +
            "COMMIT;"
            )
            db.endTransaction()
        }
        if (oldVersion < 29 && newVersion >= 29) {
            /*
            this is a fix for simons and should otherwise not be needed.
            But for some reason, he is missing column languages...
             */
            try {
                db.execSQL("ALTER TABLE $TABLE_SESSION ADD COLUMN $KEY_LANGUAGE TEXT DEFAULT 'en'")
            } catch (e:Exception) {
                Log.e("SIMONS_FIX", "table $TABLE_SESSION already has column $KEY_LANGUAGE")
            }
        }
        if (oldVersion < 30 && newVersion >= 30) {
            backupDbOnUpgrade()
            db.execSQL("ALTER TABLE $TABLE_CLASSES ADD COLUMN $KEY_HIT_POINT_RATE TEXT DEFAULT 0")
        }
        if (oldVersion < 31 && newVersion >= 31) {
            backupDbOnUpgrade()
            db.execSQL("ALTER TABLE $TABLE_CLASSES ADD COLUMN $KEY_PROGRESSIONS TEXT DEFAULT ''")
            db.execSQL("ALTER TABLE $TABLE_CLASSES ADD COLUMN $KEY_CLASS_SKILLS TEXT DEFAULT ''")
        }
    }

    fun backupDbOnUpgrade() {
        PrefsHelper(context).localCharTrackerFolder?.let {
            //will only work, if auto database has been activated or
            // access to local folder has been granted by exporting local files
            exportDbAndNotifySuccess(ctDocumentFile = it, DEBUG_TAG = "BACKUP_DB_ON_UPGRADE_DB")
        }
    }

    //////////////////////////////////////////////////////////////////////////////////

    ///////////////// HELP METHODS /////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////


    /**
     * return the String as which the list of rule systems is stored in db
     */
    fun getRuleSystemsDbString(rsList: List<RuleSystem>?): String {
        return rsList?.let { it.joinToString{it.ref} }?: ""

    }

    ///////////////////// CREATING A NEW LINE IN A TABLE ////////////////////////////////////////
    /*
        * Creating a Character
        */
    fun createCharacter(character: Character): Long {

        if (character.created_at.isBlank()) {
            character.created_at = Helper.getDateTime()
        }

        if (character.last_edit_date.isBlank()) {
            character.last_edit_date = Helper.getDateTime()
        }


        openDatabase()
        mDatabase?.beginTransaction()
        val values = ContentValues()
        values.put(KEY_NAME, character.name)
        values.put(KEY_TOTAL_EXP, character.totalExp)
        values.put(KEY_CURRENT_EXP, character.currentExp)
        values.put(KEY_PRIORITY, character.priority)
        values.put(KEY_IS_ACTIVE, if (character.isActive) 1 else 0)
        values.put(KEY_IS_RETIRED, if (character.isRetired) 1 else 0)
        values.put(KEY_RETIRED_AT, character.retired_at)
        values.put(KEY_RACE, character.race.name)
        values.put(KEY_SUBRACE, character.subrace.name)
        values.put(KEY_CLASS, character.dndClass.name)
        values.put(KEY_ARCHETYPE, character.archetype.name)
        values.put(KEY_IS_DEAD, if (character.isDead) 1 else 0)
        values.put(KEY_IS_DELETED, 0)
        values.put(KEY_CREATED_AT, character.created_at)
        values.put(KEY_LAST_UPDATED_DATE, character.created_at)
        values.put(KEY_ICON_PATH, character.iconFileName)
        values.put(KEY_CURRENT_COMMIT, character.currentCommit.hash)
        values.put(KEY_RETIRED_AT, character.retired_at)
        values.put(KEY_ICON, character.iconName)
        values.put(KEY_LEVEL, character._level)
        values.put(KEY_ATTRIBUTE_POINT_COST, character.ATTRIBUTE_POINT_EXP_COST)
        values.put(KEY_LOOT, Gson().toJson(ArrayList(character.loot)))
        values.put(KEY_BUFFS, character.exportBuffs(includeTemps = true))
        values.put(KEY_STATS, character.exportStats())
        values.put(KEY_PREFS, character.exportPrefs())
        values.put(KEY_TRADITION_ATTRIBUTE, character.CAST_STAT)
        values.put(KEY_EXPAND_CHAR_PROG_ITEM_DICT, character.exportExtendMap())
        values.put(KEY_DAMAGES, Character.exportDamages(character.damages))
        values.put(KEY_MONEY, character.money)
        values.put(KEY_CURRENT_EDGE, character.currentEdge)
        values.put(KEY_CHAR_RULE_SYSTEMS, getRuleSystemsDbString(character.ruleSystems))
        values.put(KEY_LOOTMANAGER_NAME, character.lootManagerName)
        values.put(KEY_USE_MANAGER_ACCESS, if (character.useManagerAccess) 1 else 0)
        values.put(KEY_DISPLAY_SCHWARZGELD_WEALTH, if (character.displaySchwarzgeldWealth) 1 else 0)
        values.put(KEY_SCHWARZLOOT, Gson().toJson(ArrayList(character.schwarzloot)))
        // insert row
        val character_id: Long = mDatabase?.insert(TABLE_CHARACTER, null, values) ?: -1
        mDatabase?.setTransactionSuccessful()
        mDatabase?.endTransaction()
        closeDatabase()
        println("Character successfully created: " + character.name)
        return character_id
    }

    /*
    * Creating a Tag
    */
    fun createTag(tag: String?): Long {
        openDatabase()
        val values: ContentValues = ContentValues()
        values.put(KEY_NAME, tag)
        values.put(KEY_CREATED_AT, Helper.getDateTime())
        // insert row
        val tag_id: Long = mDatabase?.insert(TABLE_TAG, null, values) ?: -1
        closeDatabase()
        Helper.log("New tag created with ID: $tag_id")
        return tag_id
    }

    /*
     * Creating a Session
     */
    fun createSession(
        sessionName: String,
        isActive: Boolean,
        rulesSystem: RuleSystem = RuleSystem.rule_system_default): Long {
        val s = Session(name = sessionName, isActive =  isActive, ruleSystem = rulesSystem)
        s.created_at = Helper.getDateTime()
        return createSession(s)
    }

    fun createSession(s: Session): Long {
        openDatabase()
        val values: ContentValues = ContentValues()
        values.put(KEY_NAME, s.name)
        values.put(KEY_CREATED_AT, s.created_at)
        values.put(KEY_RULE_SYSTEM, s.ruleSystem.ref)
        values.put(KEY_LANGUAGE, s.language)
        values.put(KEY_SHOW_TOTAL_EXP, if (s.showTotalExp) 1 else 0)
        values.put(KEY_SHOW_CURRENT_EXP, if (s.showCurrentExp) 1 else 0)
        values.put(KEY_IS_ACTIVE, if (s.isActive) 1 else 0)
        //insert row
        val session_id: Long = mDatabase?.insert(TABLE_SESSION, null, values) ?: -1
        closeDatabase()
        Helper.log("New session created with ID: $session_id")
        return session_id
    }

    /*
     * Creating a Feat
     */
    fun createFeat(featLink: FeatLink): Long {
        openDatabase()
        val values: ContentValues = ContentValues()
        values.put(KEY_ID_STRING, featLink.id_string)
        values.put(KEY_NAME, featLink.name)
        //put all links in one string separated by commata
        var links: String = ""
        for (link: String in featLink.links) {
            links += "$link,"
        }
        if (links.length > 0) {
            links = links.substring(0, links.length - 1)
        }
        values.put(KEY_LINKS, links)
        // insert row
        val feat_id: Long = mDatabase?.insert(TABLE_FEAT, null, values) ?: -1
        closeDatabase()
        //        Helper.log("New feat created with ID: " + feat_id);
        return feat_id
    }

    /*
     * Creating a Class
     */
    fun createClass(cl: DndClass): Long {
        openDatabase()
        mDatabase?.beginTransaction()
        val values = ContentValues()
        values.put(KEY_NAME, cl.name)
        values.put(KEY_LINK, cl.link)
        values.put(KEY_SKILLS_PER_LEVEL, cl.skills_per_lvl)
        values.put(KEY_HIT_POINT_RATE, cl.hpr)
        values.put(KEY_PROGRESSIONS, Gson().toJson(cl.progressions))
        values.put(KEY_CLASS_SKILLS, cl.classSkills.joinToString())
        // insert row
        val class_id: Long = mDatabase?.insert(TABLE_CLASSES, null, values) ?: -1
        /*
        create related archetypes
         */
        for (archetype_name: String in cl.archetype_names) {
            addArchetypeToClass(cl.name, archetype_name)
        }
        mDatabase?.setTransactionSuccessful()
        mDatabase?.endTransaction()
        closeDatabase()
        //        Helper.log("New feat created with ID: " + class_id);
        return class_id
    }

    /*
     * Creating an Archetype
     */
    fun createArchetype(a: DndArchetype): Long {
        openDatabase()
        val values: ContentValues = ContentValues()
        values.put(KEY_NAME, a.name)
        // insert row
        val archetype_id: Long = mDatabase?.insert(TABLE_ARCHETYPES, null, values) ?: -1
        closeDatabase()
        return archetype_id
    }

    /*
     * Creating a Race
     */
    fun createRace(race: Race): Long {
        openDatabase()
        val values: ContentValues = ContentValues()
        values.put(KEY_NAME, race.name)
        values.put(KEY_LINK, race.link)
        // insert row
        val race_id: Long = mDatabase?.insert(TABLE_RACES, null, values) ?: -1
        /*
        store related subraces
         */
        //todo store relation in db also on subrace creation and check if relation xists already before doing so.
        for (subrace_name: String in race.subraces) {
            addSubraceToRace(race.name, subrace_name)
        }
        closeDatabase()
        return race_id
    }

    /*
     * Creating an Subrace
     */
    fun createSubrace(subrace: Subrace): Long {
        openDatabase()
        val values: ContentValues = ContentValues()
        values.put(KEY_NAME, subrace.name)
        // insert row
        val subrace_id: Long = mDatabase?.insert(TABLE_SUBRACES, null, values) ?: -1
        closeDatabase()
        //        Helper.log("New subrace created with ID: " + subrace_id);
        return subrace_id
    }
    //
// -------------------------- storing a Log Entry -------------------------------------- //
//
    /*
     * Create complete LogEntry (with characters and tags)
     * tags will be read from text
     */
/////////////////////////////////// CREATE A LOG ENTRY ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * stores the data of given logEntry in database
     * (A new id will be generated. If you want to change an already existing logEntry, use updateLogEntry)
     * if e.created at is empty or null, current datetime will be used
     * tags will be extracted from the text
     * e.id will be created, so doesn't matter here
     *
     * @param e the logEntry to store, e.id, e.txt, e.created_at, e.tag_ids & e.character_ids may be null -- e.getExp() NONNULL
     * @return the id if the stored logEntry in TABLE_LOG
     */
    fun createLogEntry(e: LogEntry): Long { /*
         * read tags text and generate created_at
         * if e.text is not given, put empty String as text
         */
        var tag_names: ArrayList<String> = Helper.getTagsFromString(e.text) //tags will be dealt with later

        //now we have the tags read from e.text in tag_names and maybe also those given in e.tag_ids

        if (e.created_at.isEmpty()) {
            e.created_at = Helper.getDateTime()
        }
        if (e.last_edit_date.isEmpty()) {
            e.last_edit_date = Helper.getDateTime()
        }
        /*
         * write "pure" logEntry to TABLE_LOG
         */openDatabase()
        mDatabase?.beginTransaction()
        val values = ContentValues()
        values.put(KEY_EXP, e.exp)
        values.put(KEY_TEXT, e.text)
        values.put(KEY_CREATED_AT, e.created_at)
        values.put(KEY_LAST_UPDATED_DATE, e.last_edit_date)
        // insert row
        val log_id: Long = mDatabase?.insert(TABLE_LOG, null, values) ?: -1
        //----------------------------------------------------------------------------------
//add tags to TABLE_TAG if the tag doesn't exist yet
//and fill tags_ids for **
        val listOfExistingTags: ArrayList<String> = allTagNames
        /*
         * create new tags and add tag_ids to e.tag_ids (if they are not already contained)
         */
//create empty listOfViews for e.tag_ids if it's null
        if (e.tag_ids == null) {
            e.tag_ids = ArrayList()
        }
        for (tag_name: String in tag_names) {
            if (Helper.arrayContainsElementIgnoringCases(listOfExistingTags, tag_name)) { //tag does exist in db
                val id: Long = getTagId(tag_name)
                //tag not already contained in e.tag_ids
                if (!Helper.arrayContainsElement(e.tag_ids, id)) {
                    e.tag_ids?.add(id)
                }
            } else { //tag doesn't exist in db, so is also not contained in e.tag_id (because doesn't have an id)
                e.tag_ids?.add(createTag(tag_name))
            }
        }
        /*
         * add tags to logEntry (in database)
         */e.tag_ids?.forEach { tag_id ->
            addTagToLogEntry(log_id, tag_id)
        }
        /*
         * add chars to LogEntry (in database)
         */e.character_ids?.forEach { char_id ->
            addCharacterToLogEntry(log_id, char_id)
        }

        /*
         * add session to logEntry
         */
//todo sessions should never be null? maybe remove that so error will warn me when there is a problem
        e.session_ids?.forEach { s_id ->
            addSessionToLogEntry(log_id, s_id)
        }

        mDatabase?.setTransactionSuccessful()
        mDatabase?.endTransaction()
        closeDatabase()
        return log_id
    }

    /**
     * store the data for a new logEntry in database
     * Tags will be read from text automatically and
     * created_at will be the datetime of creation
     *
     * @param exp           exp change associated with logEntry
     * @param text          the text of the logEntry
     * @param character_ids ids of the characters affected by the logEntry
     * @return id of the created logEntry in TABLE_LOG
     */
    fun createLogEntry(exp: Int, text: String, session_ids: ArrayList<Long>?, character_ids: ArrayList<Long>?): Long {
        return createLogEntry(LogEntry(exp=exp, text=text, character_ids = character_ids, session_ids = session_ids))
    }

    /**
     * creates a new logEntry with the data given in the two editTexts and returns its id
     * or -1 if the input is invalid
     *
     *
     * @param context
     * @param experienceToAdd
     * @param textToAdd
     * @return
     */
    fun createLogEntry(context: Context?,
                       experienceToAdd: EditText?,
                       textToAdd: EditText?,
                       activeChars: ArrayList<Character>?,
                       activeSessionIds: ArrayList<Long>?,
                       mOnLogEntryAddedListener: OnLogEntryAddedListener?,
                       cheatMode: Boolean): Long {

        var newLogEntry_id: Long = -1
        val expToAdd: Int = experienceToAdd?.let { Helper.getExpToAdd(it) } ?: 0
        val text2Add: String = (textToAdd?.text ?: "").toString().trim()
        val nothingToAdd: Boolean = text2Add.isEmpty() && (expToAdd == 0)
        val noSessionSelected: Boolean = (activeSessionIds?.size == 0)
        val noSessionExists: Boolean = (getAllSessionNames().size == 0)
        //get all money references and calculate the sum
        val moneyToAdd = calcMoneyFromText(text2Add);
        var youAreNotACheater = true //generally trust user ;)
        if (noSessionExists) {
            Toast.makeText(context, "Please create a session", Toast.LENGTH_SHORT).show()
            println("No session exists") //debugging
        } else if (noSessionSelected) {
            Toast.makeText(context, "Please select a session", Toast.LENGTH_SHORT).show()
            println("No session selected") //debugging
        } else if (nothingToAdd) {
            Toast.makeText(context, "Nothing to add", Toast.LENGTH_SHORT).show()
            println("Nothing to add!") //debugging
        } else { //just do sth if there is sth to add and you didn't just click the add button for fun

            /////////////////// ________  HANDLE EXP ________________///////////////////////////

            if (expToAdd != 0) { //skip if not Exp to add

                // ------------- deal with current Exp ---------------------- //

                activeChars?.forEach {character ->
                    val newCurrentExp: Int = expToAdd + character.currentExp
                    //check if you are trying to cheat!
                    if ((newCurrentExp >= 0) || cheatMode) {
                        /*
                        update also in current char
                        later charDisplayAdapter.notifyDataSetHasChanged will update TextViews
                        */
                        character.currentExp = newCurrentExp
                        //clear Exp
                        experienceToAdd?.setText("")
                    } else {
                        Toast.makeText(context,
                            "Spending more EXP than you have is CHEATING!",
                            Toast.LENGTH_LONG).show()
                        youAreNotACheater = false
                    }
                }
                if (youAreNotACheater) {
                    // -------------------  deal with total Exp   -------------------//
                    if (expToAdd > 0) {
                        /*
                        expToAdd my be smaller than 0, but then only current exp will be updated,
                        because you have bought feats, attributes,...
                        */
                        activeChars?.forEach {character ->
                            /*
                            update total exp for current char
                            later adapter.notifyDataSetHasChanged will update TextViews
                            */
                            character.totalExp = expToAdd + character.totalExp
                        }
                    }
                }
            } else {
                println("No Exp gained") //debugging
            }

            //HANDLE MONEY
            activeChars?.forEach { character ->
                character.money = moneyToAdd + character.money
            }

            // ------ update all active characters in database ------- //

            activeChars?.forEach {character ->
                //delete all "cached" ruleSystem for characters because they might have changed after add / modifying a log entry
                character.ruleSystems = null
                updateCharacter(character, keep_last_update_date = true)
            }

            ////////////// ____________ END OF HANDLE EXP ___________________ ////////////////////


//////////////////////////////////////////////////////////////////////////////////////


            ////////////// _____________ HANDLE LOGS ________________________/////////////////////


            if (youAreNotACheater && ((activeChars?.size ?: 0) > 0)) {
                /*
                ---------------- create new Log Entry.  ----------------------//
                get listOfViews of ids of loadedCharDeatails activeChars
                and a string containing all loadedCharDeatails activeChars
                */
                val activeCharIds: ArrayList<Long> = ArrayList()
                var activeCharNames = ""
                activeChars?.forEach {character ->
                    activeCharIds.add(character.id)
                    activeCharNames = activeCharNames + character.name + " "
                }
                Toast.makeText(context, context?.getString(R.string.msg_successfully_added_to_character, activeCharNames.trim()), Toast.LENGTH_LONG).show()
                //Create new log Entry
                newLogEntry_id = createLogEntry(expToAdd, text2Add, activeSessionIds, activeCharIds)
                Helper.log("newLogEntryCreated", "active session Ids: $activeSessionIds")
                //clear Text
                textToAdd?.setText("")
                mOnLogEntryAddedListener?.onLogEntryAdded(newLogEntry_id)
            }
            if (activeChars?.size == 0) {

                //---------------- create new Log Entry without characters  -------------------//
                /*
                 * save logEntry with exp = 0,
                 * because when no char is active chars, exp is not needed and exp EditText will also not be cleared,
                 * so you can add it to a character next time.
                 */
                newLogEntry_id = createLogEntry(0, text2Add, activeSessionIds, null)
                Helper.log("newLogEntryCreated", "active session Ids: $activeSessionIds")
                Toast.makeText(context, "added to log", Toast.LENGTH_SHORT).show()
                //clear Text
                textToAdd?.setText("")
                mOnLogEntryAddedListener?.onLogEntryAdded(newLogEntry_id)
            }
        }
        return newLogEntry_id
    }
    // ------------------------------------------------------------------------------------------//
////////////////////////// FETCHING THINGS ////////////////////////////////////////////////////
// ------------------------------------------------------------------------------------------//
    /**
     * gets the id of a tag given by name if the tag exists. Else return -1
     *
     * @return tag id or -1 if tag doesn't exist
     */
    fun getTagId(tag_name: String): Long {
        return getId(tag_name, TABLE_TAG, KEY_NAME)
    }

    fun getTagName(tag_id: Long): String {
        return getName(tag_id, TABLE_TAG, KEY_NAME)
    }

    /**
     * get the id of a session given by it's name or -1 if session doesn't exists
     *
     * @return session id or -1 if session doesn't exist
     */
    fun getSessionId(session_name: String): Long {
        return getId(session_name, TABLE_SESSION, KEY_NAME)
    }

    fun getSessionName(session_id: Long): String {
        return getName(session_id, TABLE_SESSION, KEY_NAME)
    }

    /**
     * gets the id of a character given by name if the character exists. Else return -1
     *
     * @return character id or -1 if character name doesn't exist
     */
    fun getCharacterId(character_name: String): Long {
        return getId(character_name, TABLE_CHARACTER, KEY_NAME)
    }

    /**
     * gets the id of an archetype given by name if the character exists. Else return -1
     *
     * @return character id or -1 if character name doesn't exist
     */
    fun getArchetypeId(archetype_name: String): Long {
        return getId(archetype_name, TABLE_ARCHETYPES, KEY_NAME)
    }

    /**
     * gets the id of an archetype given by name if the character exists. Else return -1
     *
     * @return character id or -1 if character name doesn't exist
     */
    fun getClassId(class_name: String): Long {
        return getId(class_name, TABLE_CLASSES, KEY_NAME)
    }

    /**
     * gets the id of an archetype given by name if the character exists. Else return -1
     *
     * @return character id or -1 if character name doesn't exist
     */
    fun getRaceId(race_name: String): Long {
        return getId(race_name, TABLE_RACES, KEY_NAME)
    }

    val activeCharacterIds: ArrayList<Long>
        get() = getIds(("SELECT * FROM " + TABLE_CHARACTER + " WHERE "
                + KEY_IS_ACTIVE + " = " + 1), KEY_ID)

    fun getCharacterName(char_id: Long): String {
        return getName(char_id, TABLE_CHARACTER, KEY_NAME)
    }

    fun getActiveSessionsIds(): ArrayList<Long> {
        return getIds(("SELECT * FROM " + TABLE_SESSION + " WHERE "
                + KEY_IS_ACTIVE + " = " + 1), KEY_ID)
    }

    fun getInActiveSessionsIds(): ArrayList<Long> {
        return getIds(("SELECT * FROM $TABLE_SESSION WHERE "
                + "$KEY_IS_ACTIVE = 0"), KEY_ID)
    }

    fun getActiveSessionNames(): ArrayList<String>{
        return getNames(("SELECT * FROM " + TABLE_SESSION + " WHERE "
                + KEY_IS_ACTIVE + " = " + 1), KEY_NAME)
    }

    fun getAllSessionNames(): ArrayList<String>{
        return getAllNames(TABLE_SESSION)
    }

    /*
    * get single Character by id
    */
    fun getCharacter(character_id: Long): Character {
        val l: ArrayList<Character> = getAllCharacters(
            suffix = "WHERE $TABLE_CHARACTER.$KEY_ID = $character_id")
        return if (l.size > 0) {
            l[0]
        } else {
            val c = Character("ERROR", -1, -1, -0)
            c.iconName = "error"
            c
        }
    }

    /*
    * get single Character by name
    */
    fun getCharacter(charName: String): Character {
        val l: ArrayList<Character> = getAllCharacters(
            suffix = "WHERE $TABLE_CHARACTER.$KEY_NAME = '$charName'")
        return if (l.size > 0) {
            l[0]
        } else {
            val c = Character("ERROR", -1, -1, -0)
            c.iconName = "error"
            c
        }
    }

    /**
     * get single Class by name
     * @return might return null
     */
    fun getClass(class_name: String): DndClass? {
        val l: ArrayList<DndClass> = getClasses(
            ("SELECT  * FROM " + TABLE_CLASSES + " WHERE "
                    + KEY_NAME + " = " + "'" + class_name + "'"))
        if (l.size > 0) {
            return l.get(0)
        } else {
            return null
        }
    }

    /**
     * get single archetype by name
     * @return might return null
     */
    fun getArchetype(archetype_name: String): DndArchetype? {
        val l: ArrayList<DndArchetype> = getArchetypes(
            ("SELECT  * FROM " + TABLE_ARCHETYPES + " WHERE "
                    + KEY_NAME + " = " + "'" + archetype_name + "'"))
        if (l.size > 0) {
            return l.get(0)
        } else {
            return null
        }
    }

    /**
     * get single subrace by name
     * @return might return null
     */
    fun getSubrace(subrace_name: String): Subrace? {
        val l: ArrayList<Subrace> = getSubraces(
            ("SELECT  * FROM " + TABLE_SUBRACES + " WHERE "
                    + KEY_NAME + " = " + "'" + subrace_name + "'"))
        if (l.size > 0) {
            return l.get(0)
        } else {
            return null
        }
    }

    /**
     * get single race by name
     * @return might return null
     */
    fun getRace(race_name: String): Race? {
        val l: ArrayList<Race> = getRaces(
            ("SELECT  * FROM " + TABLE_RACES + " WHERE "
                    + KEY_NAME + " = " + "'" + race_name + "'"))
        if (l.size > 0) {
            return l.get(0)
        } else {
            return null
        }
    }

    /**
     * get a single logEntry
     *
     * @param log_id the id of the logEntry you want to get
     * @return the logEntry with given id or null if no such logEntry exists
     */
    fun getLogEntry(log_id: Long): LogEntry? {
        val list: ArrayList<LogEntry> = getAllLogs(
            where_clause = "$TABLE_LOG.$KEY_ID = $log_id")
        return if (list.size > 0) {
            list[0]
        } else {
            null
        }
    }

    /**
     * get tag with given id
     *
     * @param tag_id id of the tag you want
     * @return null if tag id not found in TABLE_TAG
     */
    fun getTag(tag_id: Long): Tag? {
        val list: ArrayList<Tag> = getTags(
            ("SELECT * FROM " + TABLE_TAG + " WHERE "
                    + KEY_ID + " = " + tag_id),
            true,
            KEY_ID)
        if (list.size == 0) {
            return null
        } else {
            return list.get(0)
        }
    }

    /**
     * @param session_id
     * @return null if no session id found and the first session found if there are (for some reason)
     * more than one sessions found.
     */
    fun getSession(session_id: Long): Session? {
        return getSessions(
            ("SELECT * FROM " + TABLE_SESSION + " WHERE "
                    + KEY_ID + " = " + session_id), KEY_ID).firstOrNull()

    }

    /**
     * @param session_id
     * @return null if no session id found and the first session found if there are (for some reason)
     * more than one sessions found.
     */
    fun getSession(sessionName: String): Session? {
        return getSessions(
            ("SELECT * FROM $TABLE_SESSION WHERE "
                    + "$KEY_NAME = '$sessionName'"), KEY_ID).firstOrNull()

    }

    /**
     * @return List of all sessions
     */
    val allSessions: ArrayList<Session>
        get() {
            return getSessions("SELECT * FROM $TABLE_SESSION ORDER BY $KEY_NAME", KEY_ID)
        }

    /**
     * @return all active sessions
     */
    val activeSessions: ArrayList<Session>
        get() {
            return getSessions(
                ("SELECT * FROM " + TABLE_SESSION + " WHERE "
                        + KEY_IS_ACTIVE + " = " + 1), KEY_ID)
        }

    /**
     * get all Characters that shall be displayed in FragmenCharView
     *
     * @return a list of all characters excluding those who have been deleted and are just kept for
     * reference purposes.
     */
    val allCharacters: ArrayList<Character>
        get() {
            return getAllCharacters(
                suffix = "WHERE $TABLE_CHARACTER.$KEY_IS_DELETED = 0"
            )
        }

    val deletedCharacterNames: ArrayList<String>
        get() {
            return getNames(("SELECT * FROM " + TABLE_CHARACTER
                    + " WHERE " + KEY_IS_DELETED + " = 1"
                    + " ORDER BY " + KEY_PRIORITY + " DESC")
                , KEY_NAME)
        }

    /**
     * get ALL characters, including those with isDeleted = true
     *
     * @return list of all characters
     */
    val allCharactersInclDeleted: ArrayList<Character>
        get() {
            return getAllCharacters(
                suffix = "ORDER BY $TABLE_CHARACTER.$KEY_PRIORITY DESC")
        }

    fun getAllCharactersMainScreen(): ArrayList<Character> {
        return getAllCharacters(
            suffix = "WHERE $TABLE_CHARACTER.$KEY_IS_DELETED = 0 " +
                    "AND $TABLE_CHARACTER.$KEY_PRIORITY >= 0 " +
                    "AND $TABLE_CHARACTER.$KEY_IS_RETIRED = 0 " +
                    "ORDER BY $TABLE_CHARACTER.$KEY_PRIORITY DESC")
            .also { sortChars(it) }
    }

    val featsExist: Boolean = !isTableEmpty(TABLE_FEAT)

    private fun isTableEmpty(tableName: String): Boolean {
        openDatabase()
        var count = 0
        mDatabase?.rawQuery("SELECT COUNT(*) FROM $tableName", null)?.let { c ->
            c.moveToFirst()
            count = c.getInt(0)
            c.close()
            closeDatabase()

        }
        return count == 0
    }

    /**
     * get ALL Feats
     *
     * @return list of all feats
     */
    val allFeats: ArrayList<FeatLink>
        get() {
            return getFeats("SELECT * FROM $TABLE_FEAT ORDER BY $KEY_NAME ASC")
        }

    /**
     * get ALL Classes
     *
     * @return list of all classes
     */
    val allClasses: ArrayList<DndClass>
        get() {
            return getClasses("SELECT * FROM $TABLE_CLASSES ORDER BY $KEY_NAME ASC")
        }

    /**
     * get ALL Archetypes
     *
     * @return list of all archetypes
     */
    val allArchetypes: ArrayList<DndArchetype>
        get() {
            return getArchetypes("SELECT * FROM $TABLE_ARCHETYPES ORDER BY $KEY_NAME ASC")
        }

    /**
     * get ALL Races
     *
     * @return list of all races
     */
    val allRaces: ArrayList<Race>
        get() {
            return getRaces("SELECT * FROM $TABLE_RACES ORDER BY $KEY_NAME ASC")
        }

    /**
     * get ALL Subraces
     *
     * @return list of all subraces
     */
    val allSubraces: ArrayList<Subrace>
        get() {
            return getSubraces("SELECT * FROM $TABLE_SUBRACES ORDER BY $KEY_NAME ASC")
        }

    /**
     * get List of all existing tag names
     */
    val allTagNames: ArrayList<String>
        get() {
            return getNames(("SELECT " + KEY_NAME + " FROM " + TABLE_TAG
                    + " ORDER BY " + KEY_NAME), KEY_NAME)
        }

    /**
     * get List of all existing tag ids
     */
    val allTagIds: ArrayList<Long>
        get() {
            return getIds("SELECT $KEY_ID FROM $TABLE_TAG", KEY_ID)
        }

    /**
     * getting all character names (KnownChars)
     */
    val allCharNames: ArrayList<String>
        get() {
            return getNames(("SELECT " + KEY_NAME + " FROM " + TABLE_CHARACTER
                    + " ORDER BY " + KEY_NAME), KEY_NAME)
        }

    /**
     * getting all character names of all nondeleted characters
     */
    val charNames: ArrayList<String>
        get() {
            return getNames(("SELECT " + KEY_NAME + " FROM " + TABLE_CHARACTER
                    + " WHERE " + KEY_IS_DELETED + " = " + 0
                    + " ORDER BY " + KEY_NAME), KEY_NAME)
        }

    val allCharacterIds: ArrayList<Long>
        get() {
            return getIds("SELECT $KEY_ID FROM $TABLE_CHARACTER", KEY_ID)
        }

    /**
     * Get all LogEntries.
     *
     * @return all LogEntries in ArrayList with newest Entry at position 0
     */
    fun getAllLogs(where_clause: String = "",
                   log_session_join: String = "LEFT",
                   log_character_join: String = "LEFT"): ArrayList<LogEntry> {
        var sql = "SELECT DISTINCT " +
                tableColumns(rename = true,
                    table_short = TABLE_LOG,
                    KEY_ID,
                    KEY_EXP,
                    KEY_TEXT,
                    KEY_CREATED_AT,
                    KEY_LAST_UPDATED_DATE
                ) + ", " +
                tableColumns(
                    rename = true,
                    table_short = TABLE_LOG_TAG,
                    KEY_TAG_ID,
                ) + ", " +
                tableColumns(
                    rename = true,
                    table_short = TABLE_LOG_CHARACTER,
                    KEY_CHARACTER_ID,
                ) + ", " +
                tableColumns(
                    rename = true,
                    table_short = TABLE_LOG_SESSION,
                    KEY_SESSION_ID,
                ) + " " +
                "FROM $TABLE_LOG " +
                "$log_session_join JOIN $TABLE_LOG_SESSION " +
                "ON $TABLE_LOG.$KEY_ID = $TABLE_LOG_SESSION.$KEY_LOG_ID " +
                "$log_character_join JOIN $TABLE_LOG_CHARACTER " +
                "ON $TABLE_LOG.$KEY_ID = $TABLE_LOG_CHARACTER.$KEY_LOG_ID " +
                "LEFT JOIN $TABLE_LOG_TAG " +
                "ON $TABLE_LOG.$KEY_ID = $TABLE_LOG_TAG.$KEY_LOG_ID"

        if (where_clause.isNotEmpty()) {
            sql += " WHERE $where_clause"
        }

        val allLogs: ArrayList<LogEntry> = getLogs(sql)
        Collections.sort(allLogs, LogEntry.CREATION_DATE_COMPARATOR)

        return allLogs
    }

//    /**
//     * get logs of active chars and active Sessions
//     */
//    fun getActiveLogs(activeSessions: ArrayList<Long>, activeChars: ArrayList<Long>): ArrayList<LogEntry> {
//        val where_clause = "(" + or("lc.$KEY_CHARACTER_ID", activeChars) + ")" +
//                 " AND (" + or("ls.$KEY_SESSION_ID", activeSessions) + ")"
//        return getLogsFast(selectQuery, "lg_id", "lg_exp", "lg_text", "lg_created", "lg_last_update")
//    }



    /**
     * get logs of active Sessions
     */
    fun getSessionLogs(sessionName: String): ArrayList<LogEntry> {

        var sessionLogs = ArrayList<LogEntry>()
        val session_id = getSessionId(sessionName)
        if (session_id != (-1).toLong()) {
            sessionLogs = getAllLogs(
                where_clause = "$TABLE_LOG_SESSION.$KEY_SESSION_ID = $session_id",
                log_session_join = "INNER")
        } else {
            log("get_session_logs", "no session found with name = $sessionName")
        }

        sessionLogs.sortWith(LogEntry.CREATION_DATE_COMPARATOR )
        return sessionLogs
    }

    /**
     * getting all Logs associated to a character.
     */
    fun getCharLogs(id_character: Long): ArrayList<LogEntry> {
        val charLogs: ArrayList<LogEntry> = getAllLogs(
            log_character_join = "INNER",
            where_clause = "$TABLE_LOG_CHARACTER.$KEY_CHARACTER_ID = $id_character" +
                    " ORDER BY $TABLE_LOG.$KEY_CREATED_AT"
        )
        Collections.sort(charLogs, LogEntry.CREATION_DATE_COMPARATOR)
        return charLogs
    }

    /**
     * get all ids of logEntries with given tag_id
     *
     * @param tag_id id of tag related to logEntries which get returned
     * @return ArrayList of log_ids, empty if no matching log found (should not happen!)
     */
    fun getLogIdsWithTag(tag_id: Long): ArrayList<Long> {
        return getIds(("SELECT * FROM " + TABLE_LOG_TAG + " WHERE "
                + tag_id + " = " + KEY_TAG_ID), KEY_LOG_ID)
    }

    fun getSessionNamesRelatedToCharacter(char_id: Long): ArrayList<String> {
        return getNames(
            getSessionsRelatedToCharacterQuery(listOf(KEY_NAME), char_id = char_id),
            KEY_NAME)
    }

    fun getSessionIdsRelatedToCharacter(char_id: Long): ArrayList<Long> {
        return getIds(
            getSessionsRelatedToCharacterQuery(listOf(KEY_ID), char_id = char_id),
            KEY_ID)
    }

    fun getSessionsRelatedToCharacter(char_id: Long): ArrayList<Session> {
        return getSessions(
            getSessionsRelatedToCharacterQuery(null, char_id = char_id),
            KEY_ID)
    }

    fun getRuleSystemsRelatedToCharacter(char_id: Long): List<RuleSystem> {
        return getNames(
            getSessionsRelatedToCharacterQuery(listOf(KEY_RULE_SYSTEM), char_id = char_id),
            KEY_RULE_SYSTEM).mapNotNull { rsRef -> RuleSystem.allRuleSystems().find { it.ref == rsRef } }
    }

    fun getAllRuleSystems(): List<RuleSystem> {
        return allSessions.map { it.ruleSystem }.distinct()
    }

    /**
     * @param columnNames null means all columns
     */
    private fun getSessionsRelatedToCharacterQuery(columnNames: List<String>?, char_id: Long): String {
        val colString = columnNames?.joinToString() ?: "*"
        return "SELECT $colString FROM $TABLE_SESSION WHERE " +
                "EXISTS (SELECT * FROM $TABLE_LOG_SESSION " +
                "INNER JOIN $TABLE_LOG_CHARACTER USING ($KEY_LOG_ID) " +
                "WHERE $TABLE_LOG_CHARACTER.$KEY_CHARACTER_ID = $char_id " +
                "AND $TABLE_SESSION.$KEY_ID = $TABLE_LOG_SESSION.$KEY_SESSION_ID)"
    }

    fun getTagNamesRelatedToCharacter(char_id: Long): ArrayList<String> {
        val selectQuery: String = "SELECT $KEY_NAME FROM $TABLE_TAG WHERE " +
                "EXISTS (SELECT * FROM $TABLE_LOG_TAG " +
                "INNER JOIN $TABLE_LOG_CHARACTER USING ($KEY_LOG_ID) " +
                "WHERE $TABLE_LOG_CHARACTER.$KEY_CHARACTER_ID = $char_id " +
                "AND $TABLE_TAG.$KEY_ID = $TABLE_LOG_TAG.$KEY_TAG_ID)"
        return getNames(selectQuery, KEY_NAME)
    }

    /**
     * dead and retired chars and chars with priority < 0 are NOT included in this list
     * unless show_all_chars is true
     */

    fun getCharsRelatedToSessions(sessionIds: ArrayList<Long>,
                                  show_all_chars: Boolean = false): ArrayList<Character> {
        if (sessionIds.size == 0) {
            return ArrayList()
        } else {
//            log("FASTCHARS: Chars related to session:")
            var suffix = "WHERE "
            if (!show_all_chars) {
                suffix += "$TABLE_CHARACTER.$KEY_IS_RETIRED = 0 " +
                        "AND $TABLE_CHARACTER.$KEY_IS_DELETED = 0 " +
                        "AND $TABLE_CHARACTER.$KEY_PRIORITY >= 0 AND "
            }
            suffix += "EXISTS (" +
                    "SELECT * FROM $TABLE_LOG_CHARACTER INNER JOIN $TABLE_LOG_SESSION " +
                    "ON $TABLE_LOG_CHARACTER.$KEY_LOG_ID = $TABLE_LOG_SESSION.$KEY_LOG_ID " +
                    "WHERE $TABLE_LOG_SESSION.$KEY_SESSION_ID IN (${sessionIds.joinToString()}) " +
                    "AND $TABLE_CHARACTER.$KEY_ID = $TABLE_LOG_CHARACTER.$KEY_CHARACTER_ID" +
                    ")"

            val chars = getAllCharacters(suffix = suffix)
            sortChars(chars)
            return chars
        }
    }

    fun getTagNamesRelatedToSessions(session_ids: ArrayList<Long>): ArrayList<String> {
        if (session_ids.isEmpty()) {
            return ArrayList()
        }
        val selectQuery: String = ("SELECT"
                + " DISTINCT tg." + KEY_NAME + " AS tg_name"
                + " FROM " + TABLE_TAG + " AS tg"
                + " INNER JOIN " + TABLE_LOG_TAG + " AS lt"
                + " ON tg." + KEY_ID + " = lt." + KEY_TAG_ID
                + " INNER JOIN " + TABLE_LOG_SESSION + " AS ls"
                + " ON lt." + KEY_LOG_ID + " = ls." + KEY_LOG_ID
                + " WHERE "+ or("ls.$KEY_SESSION_ID", session_ids)
                + " ORDER BY tg.$KEY_CREATED_AT DESC"
                )
//        log("SESSION_DATA: tag names: $selectQuery")
        return getNames(selectQuery, "tg_name")
    }

    fun getArchetypeNamesRelatedToClass(class_name: String): ArrayList<String> {
        val selectQuery: String = ("SELECT"
                + " DISTINCT " + tableColumns("",KEY_ARCHETYPE_NAME, KEY_CLASS_NAME)
                + " FROM "
                + TABLE_CLASSES_ARCHETYPES
                + " WHERE " + KEY_CLASS_NAME + " = " + "'" + class_name + "'"
                + " ORDER BY " + KEY_ARCHETYPE_NAME + " ASC")
        val result: ArrayList<String> = getNames(selectQuery, KEY_ARCHETYPE_NAME)
        Helper.removeDuplicate(result)
        return result
    }

    /**
     * might return null if no subrace is found
     * @param race_name
     * @return
     */
    fun getSubracesRelatedToRace(race_name: String): ArrayList<String> {
        val selectQuery: String = ("SELECT"
                + " DISTINCT " + tableColumns("",KEY_SUBRACE_NAME, KEY_RACE_NAME)
                + " FROM "
                + TABLE_RACE_SUBRACE
                + " WHERE " + KEY_RACE_NAME + " = " + "'" + race_name + "'"
                + " ORDER BY " + KEY_SUBRACE_NAME + " ASC")
        val result: ArrayList<String> = getNames(selectQuery, KEY_SUBRACE_NAME)
        Helper.removeDuplicate(result)
        return result

    }

    private fun getClassNamesRelatedToArchetype(archetype_name: String): ArrayList<String> {
        val selectQuery: String = ("SELECT"
                + " DISTINCT " + tableColumns("",KEY_CLASS_NAME, KEY_ARCHETYPE_NAME)
                + " FROM "
                + TABLE_CLASSES_ARCHETYPES
                + " WHERE " + KEY_ARCHETYPE_NAME + " = " + "'" + archetype_name + "'"
                + " ORDER BY " + KEY_CLASS_NAME + " ASC")
        val result: ArrayList<String> = getNames(selectQuery, KEY_CLASS_NAME)
        Helper.removeDuplicate(result)
        return result
    }

    private fun getRaceNameRelatedToSubrace(subrace_name: String): String? {
        val selectQuery: String = ("SELECT"
                + " DISTINCT " + tableColumns("",KEY_RACE_NAME, KEY_SUBRACE_NAME)
                + " FROM "
                + TABLE_RACE_SUBRACE
                + " WHERE " + KEY_SUBRACE_NAME + " = " + "'" + subrace_name + "'"
                + " ORDER BY " + KEY_RACE_NAME)
        val result: ArrayList<String>? = getNames(selectQuery, KEY_RACE_NAME)
        return if (result != null && result.size > 0) {
            Helper.removeDuplicate(result)
            result[0]
        } else {
            null
        }
    }

    private fun or(KEY: String, values: ArrayList<Long>): String {
        var result: String = ""
        for (s: Long in values) {
            result += "$KEY = $s OR "
        }
        if (values.size > 0) {
            return result.substring(0, result.length - 4)
        } else {
            return ""
        }
    }

    private fun loadAllLogCreationAndLastEditDatesWithIds(): ArrayList<Triple<String, String, Long>> {
        val listOfLogCreationAndLastUpdateDates: ArrayList<Triple<String, String, Long>> = ArrayList()
        val selectQuery = "SELECT $KEY_CREATED_AT, $KEY_LAST_UPDATED_DATE, $KEY_ID FROM $TABLE_LOG"
        openDatabase()
        val c: Cursor? = mDatabase?.rawQuery(selectQuery, null)
        if (c?.moveToFirst() == true) {
            do {
                listOfLogCreationAndLastUpdateDates.add(
                    Triple(
                        c.getString(c.getColumnIndexOrThrow(KEY_CREATED_AT)),
                        c.getString(c.getColumnIndexOrThrow(KEY_LAST_UPDATED_DATE)),
                        c.getInt(c.getColumnIndexOrThrow(KEY_ID)).toLong()))
            } while (c.moveToNext())
        }
        c?.close()
        closeDatabase()
        listOfLogCreationAndLastUpdateDates.sortBy { it.first }
        return listOfLogCreationAndLastUpdateDates
    }

    //////////////////////////// UPDATING ROWS ////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
    fun updateSession(s: Session): Int {
        openDatabase()
        val values  = ContentValues()
        values.put(KEY_NAME, s.name)
        values.put(KEY_IS_ACTIVE, if (s.isActive) 1 else 0)
        values.put(KEY_RULE_SYSTEM, s.ruleSystem.ref)
        values.put(KEY_LANGUAGE, s.language)
        values.put(KEY_SHOW_TOTAL_EXP, if (s.showTotalExp) 1 else 0)
        values.put(KEY_SHOW_CURRENT_EXP, if (s.showCurrentExp) 1 else 0)
        val i: Int = mDatabase?.update(TABLE_SESSION, values, "$KEY_ID = ?", arrayOf(s.id.toString())) ?: -1
        closeDatabase()
        return i
    }

    /**
     * update a character in database. created_at will not be updated
     *
     * @param character the character who hast changed
     * @return the number of rows affected (hopefully only 1!^^)
     */
    fun updateCharacter(character: Character, keep_last_update_date: Boolean): Int {
        openDatabase()
        val values =  ContentValues()
        values.put(KEY_NAME, character.name)
        values.put(KEY_TOTAL_EXP, character.totalExp)
        values.put(KEY_CURRENT_EXP, character.currentExp)
        values.put(KEY_PRIORITY, character.priority)
        values.put(KEY_ICON, character.iconName)
        values.put(KEY_RETIRED_AT, character.retired_at)
        if (!keep_last_update_date) {
            values.put(KEY_LAST_UPDATED_DATE, Helper.getDateTime())
        }
        values.put(KEY_RACE, character.race.name)
        values.put(KEY_SUBRACE, character.subrace.name)
        values.put(KEY_CLASS, character.dndClass.name)
        values.put(KEY_ARCHETYPE, character.archetype.name)
        values.put(KEY_LEVEL, character._level)
        values.put(KEY_ATTRIBUTE_POINT_COST, character.ATTRIBUTE_POINT_EXP_COST)
        val active: Int = if (character.isActive) 1 else 0
//        Helper.log("UpdateCharacter", "isActive = $active")
        val deleted: Int = if (character.isDeleted) 1 else 0
//        Helper.log("UpdateCharacter", "isDeleted = $deleted")
        val retired: Int = if (character.isRetired) 1 else 0
//        Helper.log("UpdateCharacter", "isRetired = $retired")
        val dead: Int = if (character.isDead) 1 else 0
//        Helper.log("UpdateCharacter", "isDead = $dead")
        values.put(KEY_IS_ACTIVE, active)
        values.put(KEY_IS_DELETED, deleted)
        values.put(KEY_IS_DEAD, dead)
        values.put(KEY_IS_RETIRED, retired)
        values.put(KEY_ICON_PATH, character.iconFileName)
        values.put(KEY_CURRENT_COMMIT, character.currentCommit.hash)
        values.put(KEY_LOOT, Gson().toJson(ArrayList(character.loot)))
        values.put(KEY_BUFFS, character.exportBuffs(includeTemps = true))
        values.put(KEY_STATS, character.exportStats())
        values.put(KEY_PREFS, character.exportPrefs())
        values.put(KEY_TRADITION_ATTRIBUTE, character.CAST_STAT)
        values.put(KEY_EXPAND_CHAR_PROG_ITEM_DICT, character.exportExtendMap())
        values.put(KEY_DAMAGES, Character.exportDamages(character.damages))
        values.put(KEY_MONEY, character.money)
        values.put(KEY_CURRENT_EDGE, character.currentEdge)
        values.put(KEY_CHAR_RULE_SYSTEMS, getRuleSystemsDbString(character.ruleSystems))
        values.put(KEY_LOOTMANAGER_NAME, character.lootManagerName)
        values.put(KEY_USE_MANAGER_ACCESS, if (character.useManagerAccess) 1 else 0)
        values.put(KEY_DISPLAY_SCHWARZGELD_WEALTH, if (character.displaySchwarzgeldWealth) 1 else 0)
        values.put(KEY_SCHWARZLOOT, Gson().toJson(ArrayList(character.schwarzloot)))

        val i: Int = mDatabase?.update(TABLE_CHARACTER, values, "$KEY_ID = ?", arrayOf(character.id.toString())) ?: -1
        closeDatabase()
        return i
    }

    /**
     * update a logEntry.
     *
     * ?Beware, that if new tags want to be added, they have to be created in the database first to
     * to get an id to add them to logEntry.?
     *
     * tag_ids. This method will however compare ids in tag_ids
     * to those currently associated with the logEntry and add them or remove them in database
     *
     * @param e A logEntry containing the new Data to be stored in database (to ID = e.id)
     * @return true if the updated logEntry has different tags or characters, false else
     */
    @JvmOverloads fun updateLogEntry(e: LogEntry, keep_last_update_date: Boolean = false): Boolean {
        var result = false
        val e_old: LogEntry? = getLogEntry(e.id)
        openDatabase()
        val values = ContentValues()
        values.put(KEY_EXP, e.exp)
        values.put(KEY_TEXT, e.text)
        values.put(KEY_CREATED_AT, e.created_at)
        if (!keep_last_update_date) {
            values.put(KEY_LAST_UPDATED_DATE, Helper.getDateTime())
        }
        mDatabase?.update(TABLE_LOG, values, "$KEY_ID = ?", arrayOf(e.id.toString()))

        /*
         -- update tags (create new tags, remove deleted tag) -- //
         */
        e_old?.let {e_old ->

            e_old.tag_ids?.let { oldTags ->

                // get list of ids of current tags to check which ones to add and which ones to delete

                //add new tags
                e.tag_ids?.forEach { tid ->
                    if (!Helper.arrayContainsElement(oldTags, tid)) {
                        addTagToLogEntry(e.id, tid)
                        result = true
                    }
                }
                //remove deleted tags
                oldTags.forEach { tid ->
                    if (!Helper.arrayContainsElement(e.tag_ids, tid)) {
                        removeTagFromLogEntry(e.id, tid)
                        result = true
                    }
                }
            }
            //remove tags that are not related to any logEntry anymore
            cleanTags(mDatabase)

            /*
        -- update sessions (add new sessions, remove removed sessions)
         */
//get old sessions ids to check which ones are new and which ones have been removed
            e_old.session_ids?.let {oldSessions ->
                //add new sessions
                e.session_ids?.forEach { sid ->
                    if (!Helper.arrayContainsElement(oldSessions, sid)) {
                        addSessionToLogEntry(e.id, sid)
                        result = true
                    }
                }
                //remove deleted sessions
                oldSessions.forEach { sid ->
                    if (!Helper.arrayContainsElement(e.session_ids, sid)) {
                        removeSessionFromLogEntry(e.id, sid)
                        result = true
                    }
                }
                closeDatabase()
            }
            val oldMOney = calcMoneyFromText(e_old.text)
            val newMoney = calcMoneyFromText(e.text)

            /*
             -- update characters (create new characters, remove deleted characters
             */
            e_old.character_ids?.let { oldChars ->

                /*
                deal with chars related to both log entries
                we only have to update the characters, if the exp changed
                */
                if (e.exp != e_old.exp || newMoney != oldMOney) {
                    e.character_ids?.intersect(oldChars.toSet())?.forEach { cid ->

                        //book exp and money from new log
                        val c: Character = getCharacter(cid)
                        c.currentExp += e.exp - e_old.exp
                        if (e.exp > 0) c.totalExp += e.exp
                        if (e_old.exp > 0) c.totalExp -= e_old.exp
                        c.money +=  newMoney - oldMOney
                        Helper.updateCharacter(
                            character = c,
                            context = context,
                            keep_last_update_date = true
                        )
//                        Helper.log("Blub", "updated character new exp ${c.name}: ${c.currentExp}/${c.totalExp}")

                        result = true
                        if (!Helper.arrayContainsElement(oldChars, cid)) {
                            addCharacterToLogEntry(e.id, cid)
                        }

                        if (!Helper.arrayContainsElement(e.character_ids, cid)) {
                            removeCharacterFromLogEntry(e.id, cid)
                            result = true
                        }
                    }
                }

                e.character_ids?.minus(oldChars)?.forEach { cid ->
                    val c: Character = getCharacter(cid)
                    c.currentExp += e.exp
                    c.money += newMoney
                    if (e.exp > 0) c.totalExp += e.exp
                    Helper.updateCharacter(
                        character = c,
                        context = context,
                        keep_last_update_date = true
                    )
                    addCharacterToLogEntry(e.id, cid)
                }

                e.character_ids?.let { newChars  ->

                    e_old.character_ids?.minus(newChars)?.forEach { cid ->
                        val c: Character = getCharacter(cid)
                        c.currentExp -= e_old.exp
                        c.money -= oldMOney
                        if (e_old.exp > 0) c.totalExp += e_old.exp
                        Helper.updateCharacter(
                            character = c,
                            context = context,
                            keep_last_update_date = true
                        )
                        removeCharacterFromLogEntry(e.id, cid)
                    }

                }
            }
        }
        return result
    }

    ////////////////////////// DELETING  ////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
    fun deleteTable(TABLE_NAME: String) {
        openDatabase()
        mDatabase?.execSQL("delete from $TABLE_NAME")
    }

    fun deleteAllFeats() {
        deleteTable(TABLE_FEAT)
    }

    fun resetClassesAndArchetypes() {
        openDatabase()
        mDatabase?.execSQL("delete from $TABLE_CLASSES")
        mDatabase?.execSQL("delete from $TABLE_ARCHETYPES")
        mDatabase?.execSQL("delete from $TABLE_CLASSES_ARCHETYPES")
    }

    fun resetRacesAndSubRaces() {
        openDatabase()
        mDatabase?.execSQL("delete from $TABLE_RACES")
        mDatabase?.execSQL("delete from $TABLE_SUBRACES")
        mDatabase?.execSQL("delete from $TABLE_RACE_SUBRACE")

    }

    /*
     * delete Log Entry.
     */
    fun deleteLogEntryWithoutRemovingExp(log_id: Long) { //delete LogEntry in TABLE_LOG
        openDatabase()
        mDatabase?.delete(TABLE_LOG, "$KEY_ID = ?", arrayOf(log_id.toString()))
        //delete references to associated tags
        mDatabase?.delete(TABLE_LOG_TAG, "$KEY_LOG_ID = ?", arrayOf(log_id.toString()))
        //delete references to associated characters
        mDatabase?.delete(TABLE_LOG_CHARACTER, "$KEY_LOG_ID = ?", arrayOf(log_id.toString()))
        //remove Tags that are related to no logEntry anymore
        cleanTags(mDatabase)
        closeDatabase()
    }

    fun deleteLogEntry(e: LogEntry) {

        //remove exp from related chars
        val chars = e.character_ids?.mapNotNull { Helper.getChar(it) }
        bookExpAndMoney(e = e, subtract = true, chars = chars)
        chars?.forEach { c ->
            c.clearCPIStats()
            updateCharacter(c, keep_last_update_date = true)
            Helper.update(Global.chars, c)
        }
        deleteLogEntryWithoutRemovingExp(e.id)
    }

    /*
     * deleting a character.
     */
    fun deleteCharacter(character: Character, deleteReferencesInLogEntries: Boolean) {
        if (deleteReferencesInLogEntries) {
            deleteCharacterAndReferences(character.id)
        } else {
            character.isActive = false //no new logs for this char will be written
            character.isDeleted = true //character will not appear in charView in Main Activity anymore
            updateCharacter(character = character, keep_last_update_date = false)
        }
    }

    fun deleteSession(session_id: Long) {
        val parameters: Array<String> = arrayOf(session_id.toString())
        //delete from TABLE_CHARACTER
        openDatabase()
        mDatabase?.delete(TABLE_SESSION, "$KEY_ID = ?", parameters)
        //delete references in LogEntries
        mDatabase?.delete(TABLE_LOG_SESSION, "$KEY_ID = ?", parameters)
        closeDatabase()
    }
// ----------------------------------------------------------------------------------------//
///////////////////////// IMPORT EXPORT /////////////////////////////////////////////////////
// ----------------------------------------------------------------------------------------//
// -------------------------------- DATABASE -----------------------------------------------//
    /**
     * import the database calls "database" to appFolder so App can use it.
     */
    fun importDatabase(originUri: Uri): Boolean { // Close the SQLiteOpenHelper so it will commit the created empty
        val DEBUG_TAG = "IMPORT_DATABASE"
        val logging = true
// database to internal storage.
        close()
        //            String newDbPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/CharTracker/database";
        val appDbPath: String = getDatabasePath(context)
        log(DEBUG_TAG, "Importing database:", logging)
        log(DEBUG_TAG, "New Database uri: $originUri", logging)
        log(DEBUG_TAG, "App Database path: $appDbPath", logging)
        val appDb: File = File(appDbPath)

        log(DEBUG_TAG, "Found new database. Copying.", logging)
        try {
            copyUris(originUri = originUri, targetUri = Uri.fromFile(appDb), context)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        // Access the copied database so SQLiteHelper will cache it and mark
// it as created.
        writableDatabase.close()
        log("Done copying")
        return true
    }

    /**
     * Export the database to externalStorage/CharTracker
     */
    fun exportDatabase(targetUri: Uri): Boolean {
        val appDbPath: String = Helper.getAppDirInternalStorage(context) + "/databases/database"
        val appDbUri = Uri.fromFile(File(appDbPath))
        return copyUris(originUri = appDbUri, targetUri = targetUri, context= context)
    }

    fun exportDbAndNotifySuccess(ctDocumentFile: DocumentFile, DEBUG_TAG: String) {
        CoroutineScope(Dispatchers.Main).launch {

            log(DEBUG_TAG, "exporting database... ")
            var path: String? = null


            val exportSuccessful =
                FileUtils.findOrCreateFile(
                    documentFile = ctDocumentFile,
                    displayName = DialogDataOptions.FILE_NAME_DB_EXPORT,
                    mimeType = "application/database"
                )?.uri?.let { fileUri ->

                    withContext(Dispatchers.IO) {
                        path = FileUtils.getLastPathSegmentNoType(fileUri)
                        exportDatabase(fileUri)
                    }

                } ?: false


            if (exportSuccessful) {
                Toast.makeText(
                    context,
                    context.getString(
                        R.string.msg_database_successfully_exported_to,
                        path
                    ),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    context,
                    R.string.msg_database_export_failed,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }


    // --------------------------------------- logs -------------------------------------------- //
    fun writeCharLogToFileAsText(character: Character, file: File, progressPublisher: ProgressPublisher): Pair<Boolean, String> { //get getCharLogs from Database and write LogStrings in ArrayList
        progressPublisher.display("Loading charLog...")
        val charLogs: ArrayList<LogEntry> = getCharLogs(character.id)
        progressPublisher.doProgress(arrayOf(-1, charLogs.size))
        progressPublisher.display("writing charLog to file")
        val charLogsText: ArrayList<String> = ArrayList()
        var progress: Int = 0
        for (e: LogEntry in charLogs) {
            progress++
            charLogsText.add(e.toString_ExpText())
            progressPublisher.doProgress(arrayOf(progress))
        }
        var result: kotlin.Pair<Boolean, String> = Helper.writeToFile(file,
            charLogsText,
            true)
        if (result.first) {
            result = Pair(true, "Charlog exported successfully to " + file.absolutePath)
        }
        return result
    }

    fun writeCharLogToFileAsXml(character: Character, file: File?, charLogs: ArrayList<LogEntry>, progressPublisher: ProgressPublisher) {
        printStringToFile(
            getXMLString(progressPublisher,
                charLogs,
                ArrayList(setOf(character)),
                ArrayList(
                    getSessionsRelatedToCharacter(character.id)
                )
            ),
            file)
    }

    fun getCurrentLogsXMLString(progressPublisher: ProgressPublisher): String? {
        return getXMLString(progressPublisher,
            Global.logsToShow)
    }

    fun getSessionXMLString(sessionName: String, progressPublisher: ProgressPublisher): String? {
        return getXMLString(
            progressPublisher = progressPublisher,
            logsToExport = getSessionLogs(sessionName),
            charsToExport = getCharsRelatedToSessions(arrayListOf(getSessionId(sessionName)), show_all_chars = true),
            sessionsToExport = getSession(sessionName)?.let { arrayListOf(it) },
            fileName = "$sessionName.xml")
    }

    fun getCompleteXMLString(progressPublisher: ProgressPublisher): GetCompleteXmlStringResult {
        val result = GetCompleteXmlStringResult(
            logs = getAllLogs(),
            characters = allCharactersInclDeleted,
            sessions =  allSessions
        ).apply {
            xmlString = getXMLString(progressPublisher, logs, characters, sessions)
        }
        return result
    }

    data class GetCompleteXmlStringResult(
        val logs: ArrayList<LogEntry>,
        val characters: ArrayList<Character>,
        val sessions: ArrayList<Session>,
        var xmlString: String? = null
    )

    /**
     * fileName is only to display to the user which file is being created
     * all though at this point we're just generating the String to print to the file
     */
    fun getXMLString(progressPublisher: ProgressPublisher,
                     logsToExport: ArrayList<LogEntry>? = null,
                     charsToExport: ArrayList<Character>? = null,
                     sessionsToExport: ArrayList<Session>? = null,
                     fileName: String = ""): String? {
        sessionsToExport?.let { Helper.removeDuplicate(it) }
        progressPublisher.display("loading logs from database")
        progressPublisher.display("Creating XML file $fileName...")
        progressPublisher.doProgress(arrayOf(-1, logsToExport?.size ?: 0 + (charsToExport?.size ?: 0) + (sessionsToExport?.size ?: 0)))
        val serializer: XmlSerializer = Xml.newSerializer()
        val writer = StringWriter()
        writer.flush()
        var progress = 0
        try {
            serializer.setOutput(writer)
            //--------------- create XML-String -----------------
            serializer.startDocument("UTF-8", true)
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true)
            serializer.startTag("", TAG_DATA)
            /*
             * Sessions
             */
            sessionsToExport?.forEach { s: Session? ->
                s?.let {
                    serializer.startTag("", TAG_SESSION_DATA)
                    serializer.attribute("", ATTR_NAME, it.name)
                    serializer.attribute("", ATTR_CREATED_AT, it.created_at)
                    createXmlTagWithText(serializer, TAG_RULE_SYSTEM, it.ruleSystem.ref)
                    createXmlTagWithText(serializer, TAG_SHOW_TOTAL_EXP, it.showTotalExp.toString())
                    createXmlTagWithText(serializer, TAG_SHOW_CURRENT_EXP, it.showCurrentExp.toString())
                    createXmlTagWithText(serializer, TAG_LANGUAGE, it.language)
                    serializer.endTag("", TAG_SESSION_DATA)
                    progress++
                    progressPublisher.doProgress(arrayOf(progress))
                }
            }
            /*
             * Characters
             */
            charsToExport?.forEach { c: Character ->
                serializer.startTag("", TAG_HERO)
                serializer.attribute("", ATTR_NAME, c.name)
                serializer.attribute("", ATTR_CREATED_AT, c.created_at)
                serializer.attribute("", ATTR_LAST_EDIT_DATE, c.last_edit_date)
                if (c.retired_at.isNotEmpty()) {
                    serializer.attribute("", ATTR_RETIRED_AT, c.retired_at)
                }

                createXmlTagWithText(serializer, TAG_RACE, c.race.name)
                createXmlTagWithText(serializer, TAG_SUBRACE, c.subrace.name)
                createXmlTagWithText(serializer, TAG_CLASS, c.dndClass.name)
                createXmlTagWithText(serializer, TAG_ARCHETYPE, c.archetype.name)
                createXmlTagWithText(serializer, TAG_LEVEL, c._level.toString())
                createXmlTagWithText(serializer, TAG_PRIORITY, c.priority.toString())
                createXmlTagWithText(serializer, TAG_IS_RETIRED, c.isRetired.toString())
                createXmlTagWithText(serializer, TAG_IS_DEAD, c.isDead.toString())
                createXmlTagWithText(serializer, TAG_IS_DELETED, c.isDeleted.toString())
                createXmlTagWithText(serializer, TAG_CURRENT_EDGE, c.currentEdge.toString())
                if (c.iconFileName.isNotEmpty()) {
                    //custom icons are used
                    createXmlTagWithText(serializer, TAG_ICON_PATH, c.iconFileName)
                }
                createXmlTagWithText(serializer, TAG_ICON, c.iconName)
                createXmlTagWithText(serializer, TAG_ATTRIBUTE_POINT_COST, c.ATTRIBUTE_POINT_EXP_COST.toString())
                if (c.currentCommit.hash != HttpUtils.NO_CURRENT_COMMIT_AVAILABLE_MSG) {
                    createXmlTagWithText(serializer, TAG_CURRENT_COMMIT_HASH, c.currentCommit.hash)
                }
                if (c.loot.isNotEmpty()) {
                    createXmlTagWithText(serializer, TAG_LOOT, GsonBuilder().setPrettyPrinting().create().toJson(c.loot))
                }
                if (c.schwarzloot.isNotEmpty()) {
                    createXmlTagWithText(serializer, TAG_SCHWARZLOOT, GsonBuilder().setPrettyPrinting().create().toJson(c.schwarzloot))
                }

                if (c.buffs.isNotEmpty()) {
                    createXmlTagWithText(serializer, TAG_BUFFS, c.exportBuffs(pretty = true, includeTemps = false))
                }
                if (c.stats.isNotEmpty()) {
                    createXmlTagWithText(serializer, TAG_STATS, c.exportStats(pretty = true))
                }
                if (c.prefs.isNotEmpty()) {
                    createXmlTagWithText(serializer, TAG_PREFS, c.exportPrefs(pretty = true))
                }
                if (c.CAST_STAT.isNotBlank()) {
                    createXmlTagWithText(serializer, TAG_TRADITION_ATTRIBUTE, c.CAST_STAT)
                }
                createXmlTagWithText(serializer, TAG_DISPLAY_SCHWARZGELD_WEALTH, c.displaySchwarzgeldWealth.toString())
                if (c.lootManagerName.isNotBlank()) {
                    createXmlTagWithText(serializer, TAG_LOOTMANAGER_NAME, c.lootManagerName)
                }
                createXmlTagWithText(serializer, TAG_EXPAND_CHAR_PROG_ITEM_DICT,
                    c.exportExtendMap(pretty = true))

                createXmlTagWithText(serializer, TAG_DAMAGES,
                    Character.exportDamages(map = c.damages, pretty = true))

                serializer.endTag("", TAG_HERO)
                progress++
                progressPublisher.doProgress(arrayOf(progress))
            }
            /*
             * Log Entries
             */
            logsToExport?.forEach { e: LogEntry ->
                serializer.startTag("", TAG_LOG_ENTRY)
                serializer.attribute("", ATTR_CREATED_AT, e.created_at)
                serializer.attribute("", ATTR_LAST_EDIT_DATE, e.last_edit_date)

                serializer.startTag("", TAG_EXP)
                serializer.text(e.exp.toString())
                serializer.endTag("", TAG_EXP)
                serializer.startTag("", TAG_TEXT)
                serializer.text(e.text)
                serializer.endTag("", TAG_TEXT)

                if (e.character_ids?.isNotEmpty() == true) {
                    serializer.startTag("", TAG_CHARACTER)
                    var charNamesAsString = ""
                    e.character_ids?.forEach { char_id ->
                        charNamesAsString += "${getCharacterName(char_id)}, "
                    }
                    serializer.text(charNamesAsString.substring(0, maxOf(charNamesAsString.length - 2, 0)))
                    serializer.endTag("", TAG_CHARACTER)
                }

                e.session_ids?.forEach { session_id ->
                    serializer.startTag("", TAG_SESSION)
                    serializer.text(getSessionName(session_id))
                    serializer.endTag("", TAG_SESSION)
                }
                serializer.endTag("", TAG_LOG_ENTRY)
                progress++
                progressPublisher.doProgress(arrayOf(progress))
            }
            serializer.endTag("", TAG_DATA)
            serializer.endDocument()
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(context, "Error serializing xml: " + e.message, Toast.LENGTH_SHORT).show()
            return null
        }
        return writer.toString()
    }

    @Throws(IOException::class)
    private fun createXmlTagWithText(serializer: XmlSerializer, TAG_NAME: String, text: String?) {
        if (text != null) {
            if (text.isNotEmpty()) {
                serializer.startTag("", TAG_NAME)
                serializer.text(text)
                serializer.endTag("", TAG_NAME)
            }
        }
    }

    fun writeToXML(
        context: Context,
        folder: DocumentFile,
        progressPublisher: ProgressPublisher,
        includeIcons: Boolean): Pair<Boolean, String> {
        val fileName = DialogDataOptions.FILE_NAME_XML_EXPORT
        getCompleteXMLString(progressPublisher).let { getXMLRes ->
            getXMLRes.xmlString?.let { xmlString ->
                FileUtils.findOrCreateFile(
                    folder,
                    DialogDataOptions.FILE_NAME_XML_EXPORT,
                    "application/xml"
                )?.uri?.let { uri ->
                    //----------------------- save to File ----------------------
                    context.contentResolver.openOutputStream(uri)?.apply {
                        write(xmlString.toByteArray())
                        close()
                    }
                    //----------------------- export icons ----------------------

                    if (includeIcons) {
                        FileUtils.findOrCreateDir(folder, Global.ICONS_FOLDER)?.
                        let {iconsFolder ->
                            getXMLRes.characters.forEach { c ->
                                if (c.iconFileName.isNotBlank()) {
                                    val iconPath = c.getCompleteIconPath(context)
                                    val originUri = File(iconPath).toUri()
                                    val type = c.iconFileName.substringAfter(".")
                                    FileUtils.findOrCreateFile(
                                        iconsFolder, c.iconFileName, "image/$type")?.
                                    let { targetDoc ->
                                        copyUris(originUri = originUri, targetUri = targetDoc.uri, context)
                                    }
                                }
                            }
                        }
                    }
                    return Pair(true, fileName)
                }
            }
        }


        return Pair(false, fileName)
    }

    fun printStringToFile(s: String?, f: File?): Boolean {
        var filePrinter: PrintStream? = null
        try {
            filePrinter = PrintStream(FileOutputStream(f))
        } catch (e: FileNotFoundException) {
            e.printStackTrace()

            //Toast.makeText(context, "File not found exception: " + e.message, Toast.LENGTH_SHORT).show()
            return false
        }
        filePrinter.println(s)
        filePrinter.close()

        return true
    }


    private fun dropTablesWithXmlImportData() {
        openDatabase()
        Global.clearCache()
        mDatabase?.execSQL("DROP TABLE IF EXISTS $TABLE_CHARACTER")
        mDatabase?.execSQL("DROP TABLE IF EXISTS $TABLE_LOG")
        mDatabase?.execSQL("DROP TABLE IF EXISTS $TABLE_SESSION")
        mDatabase?.execSQL("DROP TABLE IF EXISTS $TABLE_TAG")
        mDatabase?.execSQL("DROP TABLE IF EXISTS $TABLE_LOG_CHARACTER")
        mDatabase?.execSQL("DROP TABLE IF EXISTS $TABLE_LOG_SESSION")
        mDatabase?.execSQL("DROP TABLE IF EXISTS $TABLE_LOG_TAG")
        closeDatabase()
    }

    private fun createTablesWithXmlImportData() {
        openDatabase()
        mDatabase?.execSQL(CREATE_TABLE_CHARACTER)
        mDatabase?.execSQL(CREATE_TABLE_LOG)
        mDatabase?.execSQL(CREATE_TABLE_SESSION)
        mDatabase?.execSQL(CREATE_TABLE_TAG)
        mDatabase?.execSQL(CREATE_TABLE_LOG_CHARACTER)
        mDatabase?.execSQL(CREATE_TABLE_LOG_SESSION)
        mDatabase?.execSQL(CREATE_TABLE_LOG_TAG)
        closeDatabase()
    }

    fun getTotalNumberOfImports(fileUris: List<Uri>) : Int {
        logWithTagBlub("getting total Number of items to Import")

        var totalNumberOfTags = 0
        fileUris.forEach { uri ->


            var isr: InputStreamReader? = null
            try {
                isr = InputStreamReader(context.contentResolver.openInputStream(uri))
                logWithTagBlub("successfully initiated InputStreamReader to file path")
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }

            //--------------- initiate parsing xml ----------------//

            val factory: XmlPullParserFactory
            var xpp: XmlPullParser
            try {
                factory = XmlPullParserFactory.newInstance()
                xpp = factory.newPullParser()
                logWithTagBlub("XML Parser initiated to $uri")
                xpp.setInput(isr)
                xpp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                /*
                first we want to get the total number of logs we have to parse to see how much percent
                is already done.
                */


                var eventType: Int = xpp.eventType
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.END_TAG) {
                        if (xpp.name == TAG_LOG_ENTRY || xpp.name == TAG_HERO || xpp.name == TAG_SESSION_DATA) {
                            totalNumberOfTags++
                        }

                    }
                    eventType = xpp.next()
                }
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return totalNumberOfTags
    }

    class XmlImportResult(val newCreatedChars: ArrayList<String> = ArrayList(),
                          val newCreatedTags: ArrayList<String> = ArrayList(),
                          val charIconFileNamesToImport: ArrayList<String> = ArrayList(),
                          var logsCreatedCounter: Long = 0,
                          var logsUpdatedCounter: Long = 0,
                          var folder: DocumentFile? = null)


    /**
     * Imports LogEntries from xml
     *
     *
     * Those characters that are not in the database
     * (even not after chars from xml have been imported
     * ----> they should be put in the beginning of the xml file)
     * will be created and their ids returned as listOfViews so you can tell the user about the creation.
     * They can be edited or deleted in charView after that.
     *
     * @param overwrite
     * overwrite = keep_local: LogEntries where there is already a entry with same datetime in the database will be untouched,
     * except for tags and characters, they will be overwritten by the xml-data
     * (because those entries can't be changed in the app anyway)
     * overwrite = overwrite: imported entries have priority in case of conflict
     * (but only if there are not more than one entries with the same dateTime! because then I don't know which one to overwrite!)
     * overwrite = delete_db: database file will be deleted before importing
     *
     * @return a Quartet containing newCreatedChars (which have not been imported from the xml file),
     * newCreatedTags and the number of newly created logEntries and the number of updated logEntries
     */
    fun importFromXml(params: ActivityMain.ImportXmlParam,
                      task: ImportFromXmlTask): XmlImportResult {
        logWithTagBlub("started importLogsFromXml")
        //        String filePath = APP_EXTENAL_FILE_DIRECTORY + "/" + "logs.xml";
        val result = XmlImportResult().apply {
            folder = params.folder
        }
        var progress = 0
        val overwrite = params.import_option
        val fileUris = params.xmlLocalUris ?: listOf()
        /*
        task.toast("Import Option = " + overwrite);
        delete database file if option selected
        */
//        Helper.log("CCC", "delete db = ${PrefsHelper(context).xmlImportDeleteDb}")
        if (PrefsHelper(context).xmlImportDeleteDb) {

            dropTablesWithXmlImportData()
            task.toast("Database has been deleted")
            createTablesWithXmlImportData()
        }
        if (fileUris.isEmpty()) {
            task.toast("No Xml Files to import from found.")
        } else {

            /*
            first we want to get the total number of logs we have to parse to loadedCharDeatails how much percent
            is already done.
            */
            task.doProgress(arrayOf(-1, getTotalNumberOfImports(fileUris)))

            fileUris.forEach { fileUri ->


                var isr: InputStreamReader? = null
                try {
                    isr = InputStreamReader(context.contentResolver.openInputStream(fileUri))
                    logWithTagBlub("successfully initiated InputStreamReader to file path")
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }

                //--------------- initiate parsing xml ----------------//

                val factory: XmlPullParserFactory
                var xpp: XmlPullParser
                try {
                    factory = XmlPullParserFactory.newInstance()
                    xpp = factory.newPullParser()
                    logWithTagBlub("XML Parser initiated to $fileUri")
                    xpp.setInput(isr)
                    xpp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)

                    /*
                    Start Import
                     */
                    try {
                        isr = InputStreamReader(context.contentResolver.openInputStream(fileUri))
                        logWithTagBlub("successfully initiated InputStreamReader to file path")
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                        task.toast(e.message)
                    }
                    xpp = factory.newPullParser()
                    xpp = factory.newPullParser()
                    logWithTagBlub("XML Parser initiated second time")
                    xpp.setInput(isr)

                    task.display("Importing from ${fileUri.lastPathSegment}")

                    xpp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                    openDatabase()
                    mDatabase?.beginTransaction()

                    val listOfAllLogCreationAndLastEditDatesWithIds = loadAllLogCreationAndLastEditDatesWithIds()
                    val existingCharacters = allCharactersInclDeleted
                    existingCharacters.sortBy { it.created_at }
                    val existingSessionNames = getAllSessionNames()
                    val existing_tags = allTagNames
                    /*
                    this is actually not needed anymore, because exp will not be stored anymore in the xml file
                    but only calculated directly from the entries. But I leave it, in case I want to change that again.
                     */
//ArrayList<String> charsNoExpBooking = new ArrayList<>();
                    var e: LogEntry? = null
                    var log_entry_has_been_created = false
                    var import_log_created_at: String
                    var import_log_last_edit_date: String
                    var log_entry_with_same_creation_date_already_exists = false
                    var update_existing_log_entry = false

                    var c: Character? = null
                    var character_has_been_created = false
                    var character_with_same_creation_date_already_exists = false
                    var import_character_name: String
                    var import_character_created_at: String
                    var import_character_last_edit_date: String
                    var update_existing_character = false
                    var s: Session? = null
                    var text: String? = null

                    var eventType = xpp.eventType

                    while (eventType != XmlPullParser.END_DOCUMENT) {

                        logWithTagBlub("############################## NEUE RUNDE #####################################")
                        val tagName: String? = xpp.name
                        logWithTagBlub("eventType = $eventType, tagName = $tagName")

                        when (eventType) {

                            /////////////////////////////////////////
                            /////////////////////////////////////////
                            //////// EVENT TYPE : START TAG /////////
                            /////////////////////////////////////////
                            /////////////////////////////////////////

                            XmlPullParser.START_TAG -> {
                                logWithTagBlub("EVENT TYPE START TAG -----------")
                                /*
                                 * Log Entry (create new LogEntry e) and get attribute created at
                                 * also create empty lists for tags and characters
                                 */
                                when {
                                    tagName.equals(TAG_LOG_ENTRY, ignoreCase = true) -> {

                                        import_log_created_at = xpp.getAttributeValue("", ATTR_CREATED_AT)
                                        import_log_last_edit_date = xpp.getAttributeValue("", ATTR_LAST_EDIT_DATE)
                                            ?: import_log_created_at

                                        /*
                                        only do something with this log entry, if there is a conflicting
                                        log entry and option to overwrite is chosen

                                        if database is deleted there will be no conflicts and
                                        if you want to keep the local data, you can ignore the data
                                        from the xml
                                         */
                                        val indicesOfExistingLogs: ArrayList<Int> = ArrayList()
                                        for ((i, datedateid) in listOfAllLogCreationAndLastEditDatesWithIds.withIndex()) {
                                            if (datedateid.first == import_log_created_at) {
                                                indicesOfExistingLogs.add(i)
                                            }
                                        }
                                        logWithTagBlub(indicesOfExistingLogs.toString())
                                        for (i in indicesOfExistingLogs) {
                                            logWithTagBlub(listOfAllLogCreationAndLastEditDatesWithIds[i].toString())
                                        }
                                        logWithTagBlub("---------")
                                        log_entry_with_same_creation_date_already_exists = indicesOfExistingLogs.isNotEmpty()


                                        if (!log_entry_with_same_creation_date_already_exists) {
                                            logWithTagBlub("Log does NOT exist")

                                            e = LogEntry()
                                            e.last_edit_date = import_log_last_edit_date
                                            e.created_at = import_log_created_at
                                            log_entry_has_been_created = true

                                        } else {
                                            logWithTagBlub("Log(s?) DO exist")
                                            /*
                                            ---- Log Entry with same dateTime DOES already exist ------//
                                                 get all logEntries with same creation date
                                            */

                                            //   only do something if there is only one (should be the usual case)
                                            if (indicesOfExistingLogs.size == 1) {
                                                logWithTagBlub("only ONE log exists")
                                                /*
                                                now check witch one was last edited an use this one

                                                then overwrite in case of same editing date
                                                if option is chosen, else just keep local
                                                 */

                                                val exDateDateId = listOfAllLogCreationAndLastEditDatesWithIds[indicesOfExistingLogs[0]]

                                                val compareDatesResult = Helper.DATE_IN_STRING_FORMAT_COMPARATOR.compare(exDateDateId.second, import_log_last_edit_date)
                                                if (compareDatesResult > 0
                                                    ||
                                                    (compareDatesResult == 0
                                                            && overwrite == IMPORT_OVERWRITE)) {
                                                    logWithTagBlub("Log will be UPDATED")

                                                    update_existing_log_entry = true
                                                    log_entry_has_been_created = true
                                                    e = LogEntry()
                                                    e.id = exDateDateId.third
                                                    e.last_edit_date = import_log_last_edit_date
                                                    e.created_at = import_log_created_at

                                                }

                                            }
                                        }

                                        /*
                                         * Character (create new Character c) and get attributes created at & name
                                         * add Character to charsNoExpBooking ArrayList so xp will no be booked from log entries later
                                         */
                                    }
                                    tagName.equals(TAG_HERO, ignoreCase = true) -> {

                                        import_character_name = xpp.getAttributeValue("", ATTR_NAME)
                                        logImportingCharacters("################## Start Importing Character: $import_character_name ###################")
                                        import_character_created_at = xpp.getAttributeValue("", ATTR_CREATED_AT)
                                        import_character_last_edit_date = xpp.getAttributeValue("", ATTR_LAST_EDIT_DATE)
                                            ?: import_character_created_at
                                        logImportingCharacters("created at: $import_character_created_at, last update: $import_character_last_edit_date")

                                        /*
                                        only do something with this character, if there is a conflicting
                                        character and option to overwrite is chosen

                                        if database is deleted there will be no conflicts and
                                        if you want to keep the local data, you can ignore the data
                                        from the xml
                                         */
                                        val indicesOfExistingCharacters: ArrayList<Int> = ArrayList()
                                        for ((i, character) in existingCharacters.withIndex()) {
                                            if (character.created_at == import_character_created_at) {
                                                indicesOfExistingCharacters.add(i)
                                            }
                                        }
                                        logImportingCharacters("indices of existing characters with same creation date: ${indicesOfExistingCharacters.toString()}")
                                        for (i in indicesOfExistingCharacters) {
                                            logImportingCharacters(allCharactersInclDeleted[i].toString())
                                        }
                                        logImportingCharacters("---------")
                                        character_with_same_creation_date_already_exists = indicesOfExistingCharacters.isNotEmpty()



                                        if (!character_with_same_creation_date_already_exists) {
                                            logImportingCharacters("creating NEW character to collect import data")

                                            c = Character()
                                            c.name = import_character_name
                                            c.created_at = import_character_created_at
                                            c.last_edit_date = import_character_last_edit_date
                                            xpp.getAttributeValue("", ATTR_RETIRED_AT)?.let { c?.retired_at = it }
                                            character_has_been_created = true


                                        } else {
                                            logImportingCharacters("Character(s?) DO exist")
                                            /*
                                            ---- Character with same dateTime DOES already exist ------//
                                                 get all Characters with same creation date
                                            */

                                            //   only do something if there is only one (should be the usual case)
                                            if (indicesOfExistingCharacters.size == 1) {
                                                logImportingCharacters("only ONE Character exists")
                                                /*
                                                now check witch one was last edited and use this one

                                                then overwrite in case of same editing date
                                                if option is chosen, else just keep local
                                                 */

                                                val existingCharacter = existingCharacters[indicesOfExistingCharacters[0]]

                                                val compareDatesResult = Helper.DATE_IN_STRING_FORMAT_COMPARATOR.compare(existingCharacter.last_edit_date, import_character_last_edit_date)
                                                logImportingCharacters("compare dates result = $compareDatesResult for existing char: ${existingCharacter.name} and import character: $import_character_name")
                                                if (compareDatesResult > 0
                                                    ||
                                                    (compareDatesResult == 0
                                                            && overwrite == IMPORT_OVERWRITE)) {
                                                    logImportingCharacters("Character will be UPDATED")

                                                    update_existing_character = true
                                                    character_has_been_created = true

                                                    c = Character()
                                                    c.id = existingCharacter.id
                                                    c.currentExp = existingCharacter.currentExp
                                                    c.totalExp = existingCharacter.totalExp
                                                    c.name = import_character_name
                                                    c.created_at = import_character_created_at
                                                    c.last_edit_date = import_character_last_edit_date
                                                    xpp.getAttributeValue("", ATTR_RETIRED_AT)?.let { c.retired_at = it }


                                                }

                                            }
                                        }
                                        /*
                                         * Session (create new Session s) and get attributes created at & name
                                         */
                                    }
                                    tagName.equals(TAG_SESSION_DATA, ignoreCase = true) -> {
                                        logWithTagBlub("importing session")

                                        s = Session()
                                        s.created_at = xpp.getAttributeValue("", ATTR_CREATED_AT)
                                        s.name = xpp.getAttributeValue("", ATTR_NAME)
                                    }
                                }
                            }

                            /////////////////////////////////////////
                            /////////////////////////////////////////
                            ///////// EVENT TYPE : TEXT /////////////
                            /////////////////////////////////////////
                            /////////////////////////////////////////

                            XmlPullParser.TEXT -> {
                                logWithTagBlub("EVENT TYPE TEXT ----------- BORING: text = ${xpp.text}")
                                text = xpp.text
                            }

                            /////////////////////////////////////////
                            /////////////////////////////////////////
                            ////////// EVENT TYPE : END TAG /////////
                            /////////////////////////////////////////
                            /////////////////////////////////////////

                            XmlPullParser.END_TAG -> {

                                logWithTagBlub("EVENT TYPE END TAG -----------")
                                /*
                                 * Sessions
                                 */
                                when {

                                    tagName.equals(TAG_RULE_SYSTEM, ignoreCase = true) -> {
                                        text?.let {  s?.ruleSystem = RuleSystem(ref = it) }
                                    }
                                    tagName.equals(TAG_LANGUAGE, ignoreCase = true) -> {
                                        text?.let {  s?.language = it }
                                    }
                                    tagName.equals(TAG_SHOW_TOTAL_EXP, ignoreCase = true) -> {
                                        s?.showTotalExp = java.lang.Boolean.parseBoolean(text)
                                    }
                                    tagName.equals(TAG_SHOW_CURRENT_EXP, ignoreCase = true) -> {
                                        s?.showCurrentExp = java.lang.Boolean.parseBoolean(text)
                                    }
                                    tagName.equals(TAG_SESSION_DATA, ignoreCase = true) -> {
                                        /*
                                        * check if session exists already. if not create new session
                                        */
                                        s?.let {
                                            if (!Helper.arrayContainsElementIgnoringCases(existingSessionNames, it.name)) {
                                                createSession(it)
                                            }
                                        }
                                        /*
                                         * Characters
                                         *
                                         * for race/subrace/class/archetype only the name will be stored in db
                                         * at creation of character, so we dont have to try to load the real
                                         * race/subrace etc from db. We don't even know if it exists
                                         */
                                    }
                                    character_has_been_created -> {

                                        when {
                                            tagName.equals(TAG_RACE, ignoreCase = true) -> {
                                                text?.let { c?.race = Race(it) }
                                            }
                                            tagName.equals(TAG_SUBRACE, ignoreCase = true) -> {
                                                text?.let { c?.subrace = Subrace(it) }
                                            }
                                            tagName.equals(TAG_CLASS, ignoreCase = true) -> {
                                                text?.let { c?.dndClass = DndClass(it) }
                                            }
                                            tagName.equals(TAG_ARCHETYPE, ignoreCase = true) -> {
                                                text?.let { c?.archetype = DndArchetype(name = it) }
                                            }
                                            tagName.equals(TAG_LEVEL, ignoreCase = true) -> {
                                                text?.let { c?._level = it.toInt() }
                                            }
                                            tagName.equals(TAG_CURRENT_EDGE, ignoreCase = true) -> {
                                                text?.let { c?.currentEdge = it.toInt() }
                                            }
                                            tagName.equals(TAG_PRIORITY, ignoreCase = true) -> {
                                                text?.let { c?.priority = it.toInt() }
                                            }
                                            tagName.equals(TAG_IS_RETIRED, ignoreCase = true) -> {
                                                c?.isRetired = java.lang.Boolean.parseBoolean(text)
                                            }
                                            tagName.equals(TAG_IS_DEAD, ignoreCase = true) -> {
                                                c?.isDead = java.lang.Boolean.parseBoolean(text)
                                            }
                                            tagName.equals(TAG_IS_DELETED, ignoreCase = true) -> {
                                                c?.isDeleted = java.lang.Boolean.parseBoolean(text)
                                            }
                                            tagName.equals(TAG_ICON, ignoreCase = true) -> {
                                                //this is for backwards compatibility purposes
                                                var iconString = text ?: ERROR_ICON
                                                if (iconString.startsWith(CUSTOM_ICON_INDICATOR)) {
                                                    val iconFileName = iconString.substringAfter(CUSTOM_ICON_INDICATOR)
                                                    c?.iconFileName = iconFileName
                                                    result.charIconFileNamesToImport.add(iconFileName)
                                                    Log.d("IMPORT_XML", "added icon: $iconFileName" )
                                                    c?.iconName = ERROR_ICON
                                                } else {
                                                    c?.iconName = iconString
                                                }
                                            }
                                            tagName.equals(TAG_ICON_PATH, ignoreCase = true) -> {
                                                var iconFileName = text ?: ""
                                                if (iconFileName.isNotBlank()) {
                                                    c?.iconFileName = iconFileName
                                                    result.charIconFileNamesToImport.add(iconFileName)
//                                                    Log.d("IMPORT_XML", "added icon: $iconFileName" )
                                                }
                                            }
                                            tagName.equals(TAG_LOOT, ignoreCase = true) -> {
                                                text?.let {  c?.loot = Item.parseItems(it)}
                                            }
                                            tagName.equals(TAG_TRADITION_ATTRIBUTE, ignoreCase = true) -> {
                                                text?.let {   c?.CAST_STAT = it}
                                            }
                                            tagName.equals(TAG_STATS, ignoreCase = true) -> {
                                                text?.let {   c?.stats = Character.parseHashMapStrInt(it)}
                                            }
                                            tagName.equals(TAG_PREFS, ignoreCase = true) -> {
                                                text?.let {   c?.prefs = Character.parsePrefs(it)}
                                            }
                                            tagName.equals(TAG_BUFFS, ignoreCase = true) -> {
                                                text?.let {  c?.buffs = Buff.parseBuffs(it)}
                                            }
                                            tagName.equals(TAG_SCHWARZLOOT, ignoreCase = true) -> {
                                                text?.let {  c?.schwarzloot = Item.parseItems(it)}
                                            }
                                            tagName.equals(TAG_DISPLAY_SCHWARZGELD_WEALTH, ignoreCase = true) -> {
                                                c?.displaySchwarzgeldWealth = java.lang.Boolean.parseBoolean(text)
                                            }
                                            tagName.equals(TAG_LOOTMANAGER_NAME, ignoreCase = true) -> {
                                                text?.let {  c?.lootManagerName = it}
                                            }
                                            tagName.equals(TAG_DAMAGES, ignoreCase = true) -> {
                                                text?.let {  c?.damages = Character.parseDamages(it)}
                                            }
                                            tagName.equals(TAG_EXPAND_CHAR_PROG_ITEM_DICT, ignoreCase = true) -> {
                                                text?.let {  c?.extendMap = Character.parseExtendMap(it)}
                                            }
                                            tagName.equals(TAG_CURRENT_COMMIT_HASH, ignoreCase = true) -> {
                                                text?.let { c?.currentCommit = HttpUtils.CommitWithLink(it) }
                                            }
                                            tagName.equals(TAG_ATTRIBUTE_POINT_COST, ignoreCase = true) -> {
                                                text?.let { c?.ATTRIBUTE_POINT_EXP_COST = it.toInt() }
                                            }
                                            tagName.equals(TAG_HERO, ignoreCase = true) -> {
                                                /*
                                                 * Create new Character (in Database) if no Character with the same
                                                 * Name does exist yet

                                                 or update existing character with data from xml
                                                 */
                                                if (!character_with_same_creation_date_already_exists) {

                                                    c?.let { character ->

                                                        if (character.ATTRIBUTE_POINT_EXP_COST != 300 && character.ATTRIBUTE_POINT_EXP_COST != 500) {
                                                            character.ATTRIBUTE_POINT_EXP_COST = 300
                                                        }
                                                        character.id = createCharacter(character)
                                                        existingCharacters.add(character)

//                                                        existingCharacters.sortBy { it.created_at }
                                                        logImportingCharacters("character didn't exists -> create new: ${character.name}")
                                                    }


                                                } else {

                                                    if (update_existing_character) {

                                                        c?.let{
                                                            updateCharacter(it, keep_last_update_date = true)

                                                            logImportingCharacters("character  EXISTED but will be overwritten: ${it.name}")
                                                        }


                                                    }
                                                }
                                                /*
                                                reset values for next iteration
                                                 */
                                                character_has_been_created = false
                                                update_existing_character = false

                                            }
                                        }
                                    }
                                    log_entry_has_been_created -> {

                                        logWithTagBlub("Log Entry HAS BEEN CREATED in Start tag")

                                        when {
                                            tagName.equals(TAG_EXP, ignoreCase = true) -> {
                                                e?.exp = text!!.toInt()
                                            }
                                            tagName.equals(TAG_TEXT, ignoreCase = true) -> {
                                                e?.text = text!!
                                                /*
                                     *
                                     * when getting characters/tags/sessions it might happen that the
                                     * character/tag/session is not in the database and does not
                                     * have an id to be referenced with in this case character/tag/session
                                     * will be created and it's name returned.
                                     *
                                     */
                                            }
                                            tagName.equals(TAG_SESSION, ignoreCase = true) -> {
                                                val id: Long = getSessionId(text!!)
                                                if (id >= 0) {
                                                    //session exists
                                                    e?.session_ids?.add(id)
                                                } else {
                                                    val s_id: Long = createSession(text, false)
                                                    e?.session_ids?.add(s_id)
                                                }
                                            }
                                            tagName.equals(TAG_CHARACTER, ignoreCase = true) -> {
                                                for (charName in text!!.split(", ")) {
                                                    val id: Long = getCharacterId(charName)
                                                    if (id >= 0) {
                                                        //character exists!
                                                        e?.character_ids?.add(id)
                                                    } else {
                                                        val newCharacter = Character(charName, 0, 0, 0)
                                                        newCharacter.id = createCharacter(newCharacter)
                                                        existingCharacters.add(newCharacter)
                                                        result.newCreatedChars.add(charName)
                                                        e?.character_ids?.add(newCharacter.id)
                                                    }
                                                }
                                            }
                                            tagName.equals(TAG_LOG_ENTRY, ignoreCase = true) -> {

                                                if (!log_entry_with_same_creation_date_already_exists) {
                                                    e?.let {
                                                        logWithTagBlub("new log entry will be created (counter=${result.logsCreatedCounter}): ${e.toString_ExpText()}")
                                                        val id = createLogEntry(it)
                                                        bookExpAndMoney(e = it, chars = existingCharacters)
                                                        result.logsCreatedCounter++
                                                        listOfAllLogCreationAndLastEditDatesWithIds.add(Triple(e.created_at, e.last_edit_date, id))
                                                    }

                                                } else if (update_existing_log_entry) {
                                                    e?.let {

                                                        updateLogEntry(it, keep_last_update_date = true)
                                                        ++result.logsUpdatedCounter
                                                        logWithTagBlub("log entry was updated (counter=${result.logsUpdatedCounter}): ${e.toString_ExpText()}")
                                                    }
                                                }

                                                /*
                                                reset values for next iteration
                                                 */
                                                log_entry_has_been_created = false
                                                update_existing_log_entry = false


                                            }
                                        }
                                    }
                                }
                                if (tagName == TAG_LOG_ENTRY || tagName == TAG_HERO || tagName == TAG_SESSION_DATA) {
                                    progress++
                                    task.doProgress(arrayOf(progress))
                                    logWithTagBlub("progress $progress")

                                }
                            }
                        }
                        try {
                            logWithTagBlub("next Event plz")
                            eventType = xpp.next()
                        } catch (e1: IOException) {
                            task.toast(e1.message)
                            e1.printStackTrace()
                        }
                    }
                    for (character in existingCharacters) {
                        updateCharacter(character = character, keep_last_update_date = true)
                    }
                    mDatabase?.setTransactionSuccessful()
                    mDatabase?.endTransaction()

                } catch (e: Exception) {
                    log("IMPORT_EXECPTION", "caught import exception")
                    e.printStackTrace()
                    task.toast(e.message)
                }
                logWithTagBlub("finished parsing")
                logWithTagBlub("new Characters created: " + result.newCreatedChars.size)
                logWithTagBlub("XmlImportSummery: " + result.newCreatedChars.size + ", " + result.newCreatedTags.size + ", " + result.logsCreatedCounter + ", " + result.logsUpdatedCounter)
            }
        }
        return result
    }


    private fun logWithTagBlub(s: String) {
//        Helper.log("Blub", s)
    }

    private fun logImportingCharacters(s: String) {
//        Helper.log("CCCharacters", s)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
// ------------------------------ PRIVATE METHODS --------------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////
// ---------- private methods to create a log pure and to add tags and characters
    /*
         * Adding a Tag to a LogEntry
         */
    private fun addTagToLogEntry(log_id: Long, tag_id: Long): Long {
        return addRelation(TABLE_LOG_TAG, log_id, KEY_LOG_ID, tag_id, KEY_TAG_ID)
    }

    /*
     * Adding a character to a LogEntry
     */
    private fun addCharacterToLogEntry(log_id: Long, character_id: Long): Long {
        return addRelation(TABLE_LOG_CHARACTER, log_id, KEY_LOG_ID, character_id, KEY_CHARACTER_ID)
    }

    /*
     * Adding a session to a logEntry
     */
    private fun addSessionToLogEntry(log_id: Long, session_id: Long): Long {
        return addRelation(TABLE_LOG_SESSION, log_id, KEY_LOG_ID, session_id, KEY_SESSION_ID)
    }

    /*
    Adding an archetype to a class
     */
    private fun addArchetypeToClass(class_name: String, archetype_name: String): Long {
        openDatabase()
        val values: ContentValues = ContentValues()
        values.put(KEY_CLASS_NAME, class_name)
        values.put(KEY_ARCHETYPE_NAME, archetype_name)
        //insert row
        val id: Long = mDatabase?.insert(TABLE_CLASSES_ARCHETYPES, null, values) ?: -1
        closeDatabase()
        return id
    }

    /*
    Adding an subrace to a race
     */
    private fun addSubraceToRace(race_name: String, subrace_name: String): Long {
        openDatabase()
        val values: ContentValues = ContentValues()
        values.put(KEY_RACE_NAME, race_name)
        values.put(KEY_SUBRACE_NAME, subrace_name)
        //insert row
        val id: Long = mDatabase?.insert(TABLE_RACE_SUBRACE, null, values) ?: -1
        closeDatabase()
        return id
    }

    // -------------------------------------------------------------------------------------------
    /*
         * delete a tag from a logEntry
         */
    private fun removeTagFromLogEntry(log_id: Long, tag_id: Long): Int {
        openDatabase()
        val i: Int = mDatabase?.delete(TABLE_LOG_TAG, "$KEY_LOG_ID = ? AND $KEY_TAG_ID = ?", arrayOf(log_id.toString(), tag_id.toString())) ?: -1
        closeDatabase()
        return i
    }

    /*
     * delete a character from a logEntry
     */
    private fun removeCharacterFromLogEntry(log_id: Long, character_id: Long): Int {
        openDatabase()
        val i: Int = mDatabase?.delete(TABLE_LOG_CHARACTER, "$KEY_LOG_ID = ? AND $KEY_CHARACTER_ID = ?", arrayOf(log_id.toString(), character_id.toString())) ?: -1
        closeDatabase()
        return i
    }

    /*
     * delete a character from a logEntry
     */
    private fun removeSessionFromLogEntry(log_id: Long, session_id: Long): Int {
        openDatabase()
        val i: Int = mDatabase?.delete(TABLE_LOG_SESSION, "$KEY_LOG_ID = ? AND $KEY_SESSION_ID = ?", arrayOf(log_id.toString(), session_id.toString())) ?: -1
        closeDatabase()
        return i
    }

    /**
     * checks for each tag if is has any relate LogEntries. If not, delete tag in TABLE_TAG
     */
    private fun cleanTags(db: SQLiteDatabase?) {
        val tag_ids: ArrayList<Long> = allTagIds
        for (id: Long in tag_ids) {
            if (getLogIdsWithTag(id).size == 0) {
                mDatabase?.delete(TABLE_TAG, "$KEY_ID = ?", arrayOf(id.toString()))
            }
        }
    }

    private fun deleteCharacterAndReferences(character_id: Long) {
        val parameters: Array<String> = arrayOf(character_id.toString())
        //delete from TABLE_CHARACTER
        openDatabase()
        mDatabase?.delete(TABLE_CHARACTER, "$KEY_ID = ?", parameters)
        //delete references in LogEntries
        mDatabase?.delete(TABLE_LOG_CHARACTER, "$KEY_CHARACTER_ID = ?", parameters)
        for (e: LogEntry in Global.allLogs) {
            if (e.character_ids?.size == 1) {
                if (e.character_ids?.get(0) == character_id) {
                    deleteLogEntry(e)
                }
            }
        }
        closeDatabase()
    }



    private fun tableColumns(table_short: String, vararg column_keys: String): String {
        var s: String = ""
        for (c: String in column_keys) {
            s += "$table_short$c, "
        }
        if (!(s == "")) {
            s = s.substring(0, s.length - 2)
        }
        return s
    }

    /**
     * NO . after table_short!
     */
    private fun tableColumns(rename: Boolean = false, table_short: String, vararg column_keys: String): String {
        var s = ""
        for (c: String in column_keys) {
            s += "$table_short.$c"
            if (rename) {
                s += " AS ${table_short}_$c"
            }
            s += ", "

        }
        if (s.length > 2) {
            s = s.substring(0, s.length - 2)
        }
        return s
    }
// ----------------------------- selectQuery Helpers -----------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////
// -------------------------- GENERAL PRIVATE METHODS ----------------------------------------//
////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * add and Entry to a relational database which is a Database like e.g. TABLE_LOG_TAG
     *
     * @param TABLE_RELATIONAL_DATABASE the name of the database where the entry should be created
     * @param id_1                      id of first item
     * @param KEY_ITEM1_ID              the key of the id if item1 in the database
     * @param id_2                      id of second item
     * @param KEY_ITEM2_ID              the key of the id if item2 in the database
     * @return
     */
    private fun addRelation(TABLE_RELATIONAL_DATABASE: String, id_1: Long, KEY_ITEM1_ID: String, id_2: Long, KEY_ITEM2_ID: String): Long {
        openDatabase()
        val values: ContentValues = ContentValues()
        values.put(KEY_ITEM1_ID, id_1)
        values.put(KEY_ITEM2_ID, id_2)
        //insert row
        val id: Long = mDatabase?.insert(TABLE_RELATIONAL_DATABASE, null, values) ?: -1
        closeDatabase()
        return id
    }

    /**
     * if you are searching for a certain element which you think that it is clearly defined by its
     * name, you use this method
     *
     * @param name       the name of the item whose id you want to get
     * @param TABLE_NAME the table in which to search
     * @param KEY_NAME   the Key referencing to the name in the table
     * @return if no id is found, return -1
     */
    private fun getId(name: String, TABLE_NAME: String, KEY_NAME: String): Long {
        val ids: ArrayList<Long> = getIds(("SELECT " + KEY_ID + "," + KEY_NAME
                + " FROM " + TABLE_NAME
                + " WHERE UPPER(" + KEY_NAME + ") = " + "UPPER('" + name + "')"), KEY_ID)
        if (ids.size == 0) {
            return -1
        } else {
            return ids.get(0)
        }
    }

    /**
     * general method to get ids for some arbitrary searchQuery
     *
     * @param selectQuery the selectQuery for the ids
     * @param KEY_ID      the reference to the id we want to obtain in the table
     * @return list of all ids matching the selectQuery
     */
    private fun getIds(selectQuery: String, KEY_ID: String): ArrayList<Long> {
        val ids: ArrayList<Long> = ArrayList()
        openDatabase()
        val c: Cursor? = mDatabase?.rawQuery(selectQuery, null)
        if (c?.moveToFirst() == true) {
            do {
                ids.add(c.getLong(c.getColumnIndexOrThrow(KEY_ID)))
            } while (c.moveToNext())
        }
        c?.close()
        closeDatabase()
        return ids
    }

    /**
     * general method to get name of item given by id from the database
     *
     * @param KEY_NAME the key referencing to the name column in the table
     * @return the name of the (first found) item with the given id, Null if no item with that id found
     */
    private fun getName(id: Long, TABLE: String, KEY_NAME: String): String {
        val names: ArrayList<String> = getNames(
            ("SELECT " + KEY_ID + "," + KEY_NAME
                    + " FROM " + TABLE + " WHERE "
                    + KEY_ID + " = " + id), KEY_NAME)
        if (names.size > 0) {
            return names.get(0)
        } else {
            return "GET_NAME_ERROR"
        }
    }

    /**
     * get All names of items listed in a Table
     * IMPORTANT: The Key to reference to names in the Table hast to be KEY_NAME
     *
     * @param TABLE the table where your ant to get your names from
     * @return list of all names of items in the table
     */
    private fun getAllNames(TABLE: String): ArrayList<String> {
        return getNames(("SELECT " + KEY_NAME + " FROM " + TABLE
                + " ORDER BY " + KEY_NAME), KEY_NAME)
    }

    /**
     * general method to get name entries from the database
     *
     * @param selectQuery the selectQuery for Database
     * @param KEY_NAME    the key referencing to the name column in the table
     * @return list of names of items matching the selectQuery
     */
    private fun getNames(selectQuery: String, KEY_NAME: String): ArrayList<String> {
        val names: ArrayList<String> = ArrayList()
        openDatabase()
        val c: Cursor? = mDatabase?.rawQuery(selectQuery, null)
        if (c?.moveToFirst() == true) {
            do {
                names.add(c.getString(c.getColumnIndexOrThrow(KEY_NAME)))
            } while (c.moveToNext())
        }
        c?.close()
        closeDatabase()
        return names
    }

    /**
     * general method th get a list of feats from table TABLE_FEATS
     *
     * @return all feats matching the selectQuery
     */
    fun getFeats(selectQuery: String,
                 KEY_ID: String?,
                 KEY_ID_STRING: String?,
                 KEY_NAME: String?,
                 KEY_LINKS: String?): ArrayList<FeatLink> {
        val featLinks: ArrayList<FeatLink> = ArrayList()
        openDatabase()
        mDatabase?.rawQuery(selectQuery, null)?.also { c ->
            // looping through all rows and adding to feat list
            if (c.moveToFirst()) {
                do {
                    val featLink: FeatLink = FeatLink()
                    featLink.id = c.getInt(c.getColumnIndexOrThrow(KEY_ID)).toLong()
                    featLink.id_string = c.getString(c.getColumnIndexOrThrow(KEY_ID_STRING))
                    featLink.name = c.getString(c.getColumnIndexOrThrow(KEY_NAME))
                    val links_as_string: String = c.getString(c.getColumnIndexOrThrow(KEY_LINKS))
                    val links: Array<String> = links_as_string.split(",").toTypedArray()
                    featLink.links = ArrayList<String>(listOf(*links))
                    // adding to character
                    featLinks.add(featLink)
                } while (c.moveToNext())
            }
            c.close()
        }?.close()
        closeDatabase()
        return featLinks
    }

    /**
     * general method th get a list of classes from table TABLE_CLASSES
     *
     * @return all Classes matching the selectQuery
     */
    fun getClasses(selectQuery: String,
                   KEY_ID: String?,
                   KEY_NAME: String?,
                   KEY_LINK: String?,
                   KEY_SKILLS_PER_LEVEL: String?,
                   KEY_HIT_POINT_RATE: String?,
                   KEY_PROGRESSIONS: String?,
                   KEY_CLASS_SKILLS: String?,
                   ): ArrayList<DndClass> {
        val classes: ArrayList<DndClass> = ArrayList()
        openDatabase()
        mDatabase?.rawQuery(selectQuery, null)?.also { c ->
            // looping through all rows and adding to feat list
            if (c.moveToFirst()) {
                do {
                    val cl: DndClass = DndClass().apply {
                        parseFromCursor(
                            cursor = c,
                            KEY_ID = KEY_ID,
                            KEY_NAME = KEY_NAME,
                            KEY_LINK = KEY_LINK,
                            KEY_SKILLS_PER_LEVEL = KEY_SKILLS_PER_LEVEL,
                            KEY_HIT_POINT_RATE = KEY_HIT_POINT_RATE,
                            KEY_CLASS_SKILLS = KEY_CLASS_SKILLS,
                            KEY_PROGRESSIONS = KEY_PROGRESSIONS
                        )
                        archetype_names = getArchetypeNamesRelatedToClass(name)
                    }
                    // adding to character
                    classes.add(cl)
                } while (c.moveToNext())
            }
            c.close()
        }?.close()
        closeDatabase()
        return classes
    }

    /**
     * general method th get a list of archetypes from table TABLE_ARCHETYPE
     *
     * @return all archetypes matching the selectQuery
     */
    fun getArchetypes(selectQuery: String,
                      KEY_ID: String?,
                      KEY_NAME: String?): ArrayList<DndArchetype> {
        val archetypes: ArrayList<DndArchetype> = ArrayList()
        openDatabase()
        mDatabase?.rawQuery(selectQuery, null)?.also{ c ->
            // looping through all rows and adding to feat list
            if (c.moveToFirst()) {
                do {
                    val a: DndArchetype = DndArchetype()
                    a.id = c.getInt(c.getColumnIndexOrThrow(KEY_ID)).toLong()
                    a.name = c.getString(c.getColumnIndexOrThrow(KEY_NAME))
                    a.classes = getClassNamesRelatedToArchetype(a.name)
                    // adding to character
                    archetypes.add(a)
                } while (c.moveToNext())
            }
        }?.close()
        closeDatabase()
        return archetypes
    }

    /**
     * general method th get a list of subraces from table TABLE_SUBRACES
     *
     * @return all subraces matching the selectQuery
     */
    fun getSubraces(selectQuery: String,
                    KEY_ID: String?,
                    KEY_NAME: String?): ArrayList<Subrace> {
        val subraces: ArrayList<Subrace> = ArrayList()
        openDatabase()
        mDatabase?.rawQuery(selectQuery, null)?.also { c ->
            // looping through all rows and adding to feat list
            if (c.moveToFirst()) {
                do {
                    val subrace: Subrace = Subrace()
                    subrace.id = c.getInt(c.getColumnIndexOrThrow(KEY_ID)).toLong()
                    subrace.name = c.getString(c.getColumnIndexOrThrow(KEY_NAME))
                    subrace.race = getRaceNameRelatedToSubrace(subrace.name) ?: "undefined"
                    // adding to character
                    subraces.add(subrace)
                } while (c.moveToNext())
            }
            c.close()
        }?.close()
        closeDatabase()
        return subraces
    }

    /**
     * general method th get a list of races from table TABLE_RACES
     *
     * @return all races matching the selectQuery
     */
    fun getRaces(selectQuery: String,
                 KEY_ID: String?,
                 KEY_NAME: String?,
                 KEY_LINK: String?): ArrayList<Race> {
        val races: ArrayList<Race> = ArrayList()
        openDatabase()
        mDatabase?.rawQuery(selectQuery, null)?.also { c ->
            // looping through all rows and adding to feat list
            if (c.moveToFirst()) {
                do {
                    val race: Race = Race()
                    race.id = c.getInt(c.getColumnIndexOrThrow(KEY_ID)).toLong()
                    race.name = c.getString(c.getColumnIndexOrThrow(KEY_NAME))
                    race.link = c.getString(c.getColumnIndexOrThrow(KEY_LINK))
                    race.subraces = getSubracesRelatedToRace(race.name)
                    // adding to character
                    races.add(race)
                } while (c.moveToNext())
            }
            c.close()
        }?.close()
        closeDatabase()
        return races
    }

    /**
     * get characters
     *
     * use references to keys/columns be table_name.column_name
     *
     * @param from_clause you can specify the table which are being selected.
     * But always left join archetype, classes, races and subraces!
     */
    fun getAllCharactersGeneral(from_clause: String): ArrayList<Character> {
        var sql = "SELECT DISTINCT " +
                tableColumns(
                    rename = true,
                    table_short = TABLE_CHARACTER,
                    *TABLE_CHARACTER_COLUMNS.keys.toTypedArray()
                ) + ", " +
                tableColumns(
                    rename = true,
                    table_short = TABLE_RACES,
                    KEY_ID,
                    KEY_NAME,
                    KEY_LINK
                ) + ", " +
                tableColumns(
                    rename = true,
                    table_short = TABLE_SUBRACES,
                    KEY_ID,
                    KEY_NAME,
                ) + ", " +
                tableColumns(
                    rename = true,
                    table_short = TABLE_CLASSES,
                    KEY_ID,
                    KEY_NAME,
                    KEY_LINK,
                    KEY_SKILLS_PER_LEVEL,
                    KEY_HIT_POINT_RATE,
                    KEY_PROGRESSIONS,
                    KEY_CLASS_SKILLS
                ) + ", " +
                tableColumns(
                    rename = true,
                    table_short = TABLE_ARCHETYPES,
                    KEY_ID,
                    KEY_NAME
                ) + " " +
                "FROM $from_clause"

        return getCharacters(
            selectQuery = sql,
            KEY_ID = "${TABLE_CHARACTER}_$KEY_ID",
            KEY_NAME = "${TABLE_CHARACTER}_$KEY_NAME",
            KEY_CREATED_AT = "${TABLE_CHARACTER}_$KEY_CREATED_AT",
            KEY_LAST_UPDATED_DATE = "${TABLE_CHARACTER}_$KEY_LAST_UPDATED_DATE",
            KEY_LEVEL = "${TABLE_CHARACTER}_$KEY_LEVEL",
            KEY_RACE = "${TABLE_CHARACTER}_$KEY_RACE",
            KEY_SUBRACE = "${TABLE_CHARACTER}_$KEY_SUBRACE",
            KEY_CLASS = "${TABLE_CHARACTER}_$KEY_CLASS",
            KEY_ARCHETYPE = "${TABLE_CHARACTER}_$KEY_ARCHETYPE",
            KEY_TOTAL_EXP = "${TABLE_CHARACTER}_$KEY_TOTAL_EXP",
            KEY_CURRENT_EXP = "${TABLE_CHARACTER}_$KEY_CURRENT_EXP",
            KEY_PRIORITY = "${TABLE_CHARACTER}_$KEY_PRIORITY",
            KEY_IS_ACTIVE = "${TABLE_CHARACTER}_$KEY_IS_ACTIVE",
            KEY_IS_RETIRED = "${TABLE_CHARACTER}_$KEY_IS_RETIRED",
            KEY_RETIRED_AT = "${TABLE_CHARACTER}_$KEY_RETIRED_AT",
            KEY_IS_DEAD = "${TABLE_CHARACTER}_$KEY_IS_DEAD",
            KEY_IS_DELETED = "${TABLE_CHARACTER}_$KEY_IS_DELETED",
            KEY_ICON = "${TABLE_CHARACTER}_$KEY_ICON",
            KEY_ATTRIBUTE_POINT_COST = "${TABLE_CHARACTER}_$KEY_ATTRIBUTE_POINT_COST",
            KEY_ICON_PATH = "${TABLE_CHARACTER}_$KEY_ICON_PATH",
            KEY_CURRENT_COMMIT = "${TABLE_CHARACTER}_$KEY_CURRENT_COMMIT",
            KEY_LOOT = "${TABLE_CHARACTER}_$KEY_LOOT",
            KEY_SCHWARZLOOT = "${TABLE_CHARACTER}_$KEY_SCHWARZLOOT",
            KEY_BUFFS = "${TABLE_CHARACTER}_$KEY_BUFFS",
            KEY_STATS = "${TABLE_CHARACTER}_$KEY_STATS",
            KEY_PREFS = "${TABLE_CHARACTER}_$KEY_PREFS",
            KEY_TRADITION_ATTRIBUTE = "${TABLE_CHARACTER}_$KEY_TRADITION_ATTRIBUTE",
            KEY_EXPAND_CHAR_PROG_ITEM_DICT = "${TABLE_CHARACTER}_$KEY_EXPAND_CHAR_PROG_ITEM_DICT",
            KEY_DAMAGES = "${TABLE_CHARACTER}_$KEY_DAMAGES",
            KEY_MONEY = "${TABLE_CHARACTER}_$KEY_MONEY",
            KEY_CURRENT_EDGE = "${TABLE_CHARACTER}_$KEY_CURRENT_EDGE",
            KEY_CHAR_RULE_SYSTEMS = "${TABLE_CHARACTER}_$KEY_CHAR_RULE_SYSTEMS",
            KEY_LOOTMANAGER_NAME = "${TABLE_CHARACTER}_$KEY_LOOTMANAGER_NAME",
            KEY_USE_MANAGER_ACCESS = "${TABLE_CHARACTER}_$KEY_USE_MANAGER_ACCESS",
            KEY_DISPLAY_SCHWARZGELD_WEALTH = "${TABLE_CHARACTER}_$KEY_DISPLAY_SCHWARZGELD_WEALTH",
            KEY_RACE_ID = "${TABLE_RACES}_$KEY_ID",
            KEY_RACE_NAME = "${TABLE_RACES}_$KEY_NAME",
            KEY_RACE_LINK = "${TABLE_RACES}_$KEY_LINK",
            KEY_SUB_RACE_ID = "${TABLE_SUBRACES}_$KEY_ID",
            KEY_SUB_RACE_NAME = "${TABLE_SUBRACES}_$KEY_NAME",
            KEY_CLASS_ID = "${TABLE_CLASSES}_$KEY_ID",
            KEY_CLASS_NAME = "${TABLE_CLASSES}_$KEY_NAME",
            KEY_CLASS_LINK = "${TABLE_CLASSES}_$KEY_LINK",
            KEY_CLASS_SKILLS_PER_LEVEL = "${TABLE_CLASSES}_$KEY_SKILLS_PER_LEVEL",
            KEY_CLASS_HIT_POINT_RATE = "${TABLE_CLASSES}_$KEY_HIT_POINT_RATE",
            KEY_CLASS_PROGRESSION = "${TABLE_CLASSES}_$KEY_PROGRESSIONS",
            KEY_CLASS_CLASS_SKILLS = "${TABLE_CLASSES}_$KEY_CLASS_SKILLS",
            KEY_ARCHETYPE_ID = "${TABLE_ARCHETYPES}_$KEY_ID",
            KEY_ARCHETYPE_NAME = "${TABLE_ARCHETYPES}_$KEY_NAME"
        )
    }

    /**
     * get all characters
     *
     * use references to keys/columns be table_name.column_name
     *
     * @param suffix a where statement or order by statemanet
     * of anthing that comes after the select statement!
     */
    fun getAllCharacters(suffix: String = ""): ArrayList<Character> {
        var fromClause = "$TABLE_CHARACTER " +
                "LEFT JOIN $TABLE_RACES " +
                "ON $TABLE_CHARACTER.$KEY_RACE = $TABLE_RACES.$KEY_NAME " +
                "LEFT JOIN $TABLE_SUBRACES " +
                "ON $TABLE_CHARACTER.$KEY_SUBRACE = $TABLE_SUBRACES.$KEY_NAME " +
                "LEFT JOIN $TABLE_CLASSES " +
                "ON $TABLE_CHARACTER.$KEY_CLASS = $TABLE_CLASSES.$KEY_NAME " +
                "LEFT JOIN $TABLE_ARCHETYPES " +
                "ON $TABLE_CHARACTER.$KEY_ARCHETYPE = $TABLE_ARCHETYPES.$KEY_NAME"

        if (suffix.isNotEmpty()) {
            fromClause += " $suffix"
        }
        return getAllCharactersGeneral(
            from_clause = fromClause
        )
    }

    /**
     * general method th get a list of characters from table TABLE_CHARACTER
     *
     * @return all Characters matchting the selectQuery
     */
    private fun getCharacters(
        selectQuery: String,
        KEY_ID: String,
        KEY_NAME: String,
        KEY_CREATED_AT: String,
        KEY_LAST_UPDATED_DATE: String,
        KEY_LEVEL: String,
        KEY_RACE: String,
        KEY_SUBRACE: String,
        KEY_CLASS: String,
        KEY_ARCHETYPE: String,
        KEY_TOTAL_EXP: String,
        KEY_CURRENT_EXP: String,
        KEY_PRIORITY: String,
        KEY_IS_ACTIVE: String,
        KEY_IS_RETIRED: String,
        KEY_RETIRED_AT: String,
        KEY_IS_DEAD: String,
        KEY_IS_DELETED: String,
        KEY_ICON: String,
        KEY_ATTRIBUTE_POINT_COST: String,
        KEY_ICON_PATH: String,
        KEY_CURRENT_COMMIT: String,
        KEY_RACE_ID: String,
        KEY_RACE_NAME: String,
        KEY_RACE_LINK: String,
        KEY_SUB_RACE_ID: String,
        KEY_SUB_RACE_NAME: String,
        KEY_CLASS_ID: String,
        KEY_CLASS_NAME: String,
        KEY_CLASS_LINK: String,
        KEY_CLASS_SKILLS_PER_LEVEL: String,
        KEY_CLASS_HIT_POINT_RATE: String,
        KEY_CLASS_PROGRESSION: String,
        KEY_CLASS_CLASS_SKILLS: String,
        KEY_ARCHETYPE_ID: String,
        KEY_ARCHETYPE_NAME: String,
        KEY_LOOT: String,
        KEY_SCHWARZLOOT: String,
        KEY_BUFFS: String,
        KEY_STATS: String,
        KEY_PREFS: String,
        KEY_TRADITION_ATTRIBUTE: String,
        KEY_EXPAND_CHAR_PROG_ITEM_DICT: String,
        KEY_DAMAGES: String,
        KEY_MONEY: String,
        KEY_CURRENT_EDGE: String,
        KEY_CHAR_RULE_SYSTEMS: String,
        KEY_LOOTMANAGER_NAME: String,
        KEY_USE_MANAGER_ACCESS: String,
        KEY_DISPLAY_SCHWARZGELD_WEALTH: String
    ): ArrayList<Character> {

//        log("FASTCHARS: sql = $selectQuery")
        val chars: ArrayList<Character> = ArrayList()
        openDatabase()
        mDatabase?.beginTransaction()
        mDatabase?.rawQuery(selectQuery, null)?.also { c ->
            // looping through all rows and adding to characters
            if (c.moveToFirst()) {
                do {
                    val character = Character()
                    character.id = c.getInt(c.getColumnIndexOrThrow(KEY_ID)).toLong()
                    character.name = c.getString(c.getColumnIndexOrThrow(KEY_NAME))

                    //RACE
                    val raceName = c.getString(c.getColumnIndexOrThrow(KEY_RACE))
                    character.race = Race(name = raceName)
                    if (raceName != Character.UNDEFINED) {
                        character.race.id = c.getInt(c.getColumnIndexOrThrow(KEY_RACE_ID)).toLong()
                        c.getString(c.getColumnIndexOrThrow(KEY_RACE_LINK))?.let {
                            character.race.link = it
                        }
                        //don't load anything into character.race.subraces
                    }

                    //SUBRACE
                    val subraceName = c.getString(c.getColumnIndexOrThrow(KEY_SUBRACE))
                    character.subrace = Subrace(name = subraceName)
                    if (subraceName != Character.UNDEFINED) {
                        character.subrace.id = c.getInt(c.getColumnIndexOrThrow(KEY_SUB_RACE_ID)).toLong()
                        character.subrace.race = raceName // this is not really necessary
                    }

                    //CLASS
                    val className: String = c.getString(c.getColumnIndexOrThrow(KEY_CLASS))
                    character.dndClass = DndClass(name = className)
                    if (className != Character.UNDEFINED) {
                        character.dndClass.parseFromCursor(
                            cursor = c,
                            KEY_ID = KEY_CLASS_ID,
                            KEY_NAME = KEY_CLASS, // not really necessary but like this we can use the same method and there is not harm getting the name again
                            KEY_LINK = KEY_CLASS_LINK,
                            KEY_SKILLS_PER_LEVEL = KEY_CLASS_SKILLS_PER_LEVEL,
                            KEY_HIT_POINT_RATE = KEY_CLASS_HIT_POINT_RATE,
                            KEY_CLASS_SKILLS = KEY_CLASS_CLASS_SKILLS,
                            KEY_PROGRESSIONS = KEY_CLASS_PROGRESSION
                        )

                    }

                    //ARCHETYPE
                    val archName: String = c.getString(c.getColumnIndexOrThrow(KEY_ARCHETYPE))
                    character.archetype = DndArchetype(name = archName)
                    if (archName != Character.UNDEFINED) {
                        character.archetype.id = c.getInt(c.getColumnIndexOrThrow(KEY_ARCHETYPE_ID)).toLong()
                    }


                    character._level = c.getInt(c.getColumnIndexOrThrow(KEY_LEVEL))
                    character.totalExp = c.getInt(c.getColumnIndexOrThrow(KEY_TOTAL_EXP))
                    character.currentExp = c.getInt(c.getColumnIndexOrThrow(KEY_CURRENT_EXP))
                    character.priority = c.getInt(c.getColumnIndexOrThrow(KEY_PRIORITY))
                    character.money = c.getInt(c.getColumnIndexOrThrow(KEY_MONEY))
                    character.damages = Character.parseDamages(c.getString(c.getColumnIndexOrThrow(KEY_DAMAGES)))
                    character.currentEdge = c.getInt(c.getColumnIndexOrThrow(KEY_CURRENT_EDGE))
                    character.isActive = (c.getInt(c.getColumnIndexOrThrow(KEY_IS_ACTIVE)) == 1)
                    character.isRetired = (c.getInt(c.getColumnIndexOrThrow(KEY_IS_RETIRED)) == 1)
                    character.retired_at = c.getString(c.getColumnIndexOrThrow(KEY_RETIRED_AT))
                    character.last_edit_date = c.getString(c.getColumnIndexOrThrow(KEY_LAST_UPDATED_DATE))
                    character.isDead = (c.getInt(c.getColumnIndexOrThrow(KEY_IS_DEAD)) == 1)
                    character.isDeleted = (c.getInt(c.getColumnIndexOrThrow(KEY_IS_DELETED)) == 1)
                    character.created_at = c.getString(c.getColumnIndexOrThrow(KEY_CREATED_AT))
                    character.iconName = c.getString(c.getColumnIndexOrThrow(KEY_ICON))
                    character.CAST_STAT = c.getString(c.getColumnIndexOrThrow(KEY_TRADITION_ATTRIBUTE))
                    character.lootManagerName = c.getString(c.getColumnIndexOrThrow(KEY_LOOTMANAGER_NAME))
                    character.useManagerAccess = (c.getInt(c.getColumnIndexOrThrow(KEY_USE_MANAGER_ACCESS)) == 1)
                    character.displaySchwarzgeldWealth = (c.getInt(c.getColumnIndexOrThrow(KEY_DISPLAY_SCHWARZGELD_WEALTH)) == 1)
                    character.iconFileName = c.getString(c.getColumnIndexOrThrow(KEY_ICON_PATH))
                    character.hgExpPercentageDefault = PrefsHelper(context).hgExpPercentage
                    character.fokusPercentageDefault = PrefsHelper(context).fokusPercentage
                    character.splittermondHomeBrewDefault = PrefsHelper(context).splittermondHomeBrew
                    val rSString = c.getString(c.getColumnIndexOrThrow(KEY_CHAR_RULE_SYSTEMS))
                    character.ruleSystems = if (rSString.isEmpty()) null else {
                        rSString.split(", ").mapNotNull { s -> RuleSystem.allRuleSystems().find { it.ref == s} }
                    }
                    character.currentCommit = HttpUtils.CommitWithLink(
                        c.getString(c.getColumnIndexOrThrow(KEY_CURRENT_COMMIT)))
                    character.ATTRIBUTE_POINT_EXP_COST =
                        c.getInt(c.getColumnIndexOrThrow(KEY_ATTRIBUTE_POINT_COST))
                    c.getString(c.getColumnIndexOrThrow(KEY_LOOT)).let {
//                        log("JSON", it)
                        if (it.isNotBlank()) {
                            character.loot = Item.parseItems(json = it)
                        }
                    }
                    c.getString(c.getColumnIndexOrThrow(KEY_SCHWARZLOOT)).let {
//                        log("JSON", it)
                        if (it.isNotBlank()) {
                            character.schwarzloot = Item.parseItems(json = it)
                        }
                    }
                    c.getString(c.getColumnIndexOrThrow(KEY_BUFFS)).let {
//                        log("JSON", it)
                        if (it.isNotBlank()) {
                            val buffs = Buff.parseBuffs(json = it)
                            character.buffs = ArrayList(
                                buffs.filter { !it.type.endsWith(Character.BUFF_TYPE_TEMP) }
                            )
                            character.tempBuffs = ArrayList(
                                buffs.filter { it.type.endsWith(Character.BUFF_TYPE_TEMP) }.map {
                                    character.removeTempMarker(it)
                                }
                            )
                        }
                    }
                    c.getString(c.getColumnIndexOrThrow(KEY_STATS)).let {
//                        log("JSON", it)
                        if (it.isNotBlank()) {
                            character.stats = Character.parseHashMapStrInt(json = it)
                        }
                    }
                    c.getString(c.getColumnIndexOrThrow(KEY_PREFS)).let {
//                        log("JSON", it)
                        if (it.isNotBlank()) {
                            character.prefs = Character.parsePrefs(json = it)
                        }
                    }
                    c.getString(c.getColumnIndexOrThrow(KEY_EXPAND_CHAR_PROG_ITEM_DICT)).let {
                        if (it.isNotBlank()) {
                            character.extendMap = Character.parseExtendMap(json = it)
                        }
                    }
                    character.loadBuffBoni()

                    // adding to characters
                    chars.add(character)
                } while (c.moveToNext())
            }
            c.close()
        }?.close()
        mDatabase?.setTransactionSuccessful()
        mDatabase?.endTransaction()
        closeDatabase()
        return chars
    }

    fun getFeats(selectQuery: String): ArrayList<FeatLink> {
        return getFeats(selectQuery, KEY_ID, KEY_ID_STRING, KEY_NAME, KEY_LINKS)
    }

    fun getClasses(selectQuery: String): ArrayList<DndClass> {
        return getClasses(
            selectQuery,
            KEY_ID,
            KEY_NAME,
            KEY_LINK,
            KEY_SKILLS_PER_LEVEL,
            KEY_HIT_POINT_RATE,
            KEY_PROGRESSIONS,
            KEY_CLASS_SKILLS
        )
    }

    fun getArchetypes(selectQuery: String): ArrayList<DndArchetype> {
        return getArchetypes(selectQuery, KEY_ID, KEY_NAME)
    }

    fun getRaces(selectQuery: String): ArrayList<Race> {
        return getRaces(selectQuery, KEY_ID, KEY_NAME, KEY_LINK)
    }

    fun getSubraces(selectQuery: String): ArrayList<Subrace> {
        return getSubraces(selectQuery, KEY_ID, KEY_NAME)
    }

    fun getLogs(selectQuery: String): ArrayList<LogEntry> {
        return getLogs(
            selectQuery,
            KEY_ID = "${TABLE_LOG}_$KEY_ID",
            KEY_EXP = "${TABLE_LOG}_$KEY_EXP",
            KEY_TEXT = "${TABLE_LOG}_$KEY_TEXT",
            KEY_CREATED_AT = "${TABLE_LOG}_$KEY_CREATED_AT",
            KEY_LAST_UPDATED_DATE = "${TABLE_LOG}_$KEY_LAST_UPDATED_DATE",
            KEY_TAG_ID = "${TABLE_LOG_TAG}_$KEY_TAG_ID",
            KEY_CHARACTER_ID = "${TABLE_LOG_CHARACTER}_$KEY_CHARACTER_ID",
            KEY_SESSION_ID = "${TABLE_LOG_SESSION}_$KEY_SESSION_ID"
        )
    }

    /**
     * A general method to get a list of log entries from TABLE_LOGS
     * when we have more complex queries (like in getActiveLogs) dealing with many table,
     * we might need to specify to which table KEY_ID / CREATED_AT refers to get the current
     * values. If there are no Problems with misunderstanding here, use getLogs method with only
     * selectQuery as Argument
     *
     * @return all LogEntries matching the selectQuery in an ArrayList with newest Entry at position 0
     */
    fun getLogs(selectQuery: String,
                KEY_ID: String,
                KEY_EXP: String,
                KEY_TEXT: String,
                KEY_CREATED_AT: String,
                KEY_LAST_UPDATED_DATE: String,
                KEY_TAG_ID: String,
                KEY_CHARACTER_ID: String,
                KEY_SESSION_ID: String
    ): ArrayList<LogEntry> {
        val logs = ArrayList<LogEntry>()
        val ids = ArrayList<Long>()
        //        Helper.log("LOAD_INIT_DATA", "start loading logs");
//Log.e(LOG, selectQuery);
        openDatabase()
        var currentId: Long
        var currentLog = LogEntry()
        mDatabase?.beginTransaction()
        mDatabase?.rawQuery(selectQuery, null)?.also { c ->
            // looping through all rows and adding to list
            if (c.moveToFirst()) {
                do {
                    currentId = c.getInt(c.getColumnIndexOrThrow(KEY_ID)).toLong()

                    /*
                    add data to current log as long as the id doesn't change.
                    when id changes, create new log if it hasn't come up before
                    or get log from logs list
                     */

                    if (currentLog.id != currentId) {

                        if (currentId !in ids) {
                            currentLog = LogEntry()
                            logs.add(currentLog)
                            logs.sortBy { it.id }

                            //get from query
                            currentLog.id = currentId
                            currentLog.exp = c.getInt(c.getColumnIndexOrThrow(KEY_EXP))
                            currentLog.text = c.getString(c.getColumnIndexOrThrow(KEY_TEXT))
                            currentLog.created_at = c.getString(c.getColumnIndexOrThrow(KEY_CREATED_AT))
                            currentLog.last_edit_date =
                                c.getString(c.getColumnIndexOrThrow(KEY_LAST_UPDATED_DATE))

                        } else {

                            for (log in logs) {
                                if (log.id == currentId) {
                                    currentLog = log
                                    break;
                                }
                            }
                        }
                    }

                    /*
                    now currentLog should be the log matching LOG_ID in this cursor row
                    so now add data from relational tables
                     */
                    val col_idx_tag_id = c.getColumnIndex(KEY_TAG_ID)
                    val col_idx_chr_id = c.getColumnIndex(KEY_CHARACTER_ID)
                    val col_idx_ses_id = c.getColumnIndex(KEY_SESSION_ID)

                    if (col_idx_tag_id != -1) {
                        val tag_id = c.getInt(col_idx_tag_id).toLong()
//                      when doing left join and no tags are related, than we will have 0 here.
                        if (tag_id > 0) {
                            currentLog.tag_ids?.let {
                                if (tag_id !in it) it.add(tag_id)
                            }
                        }
                    }

                    if (col_idx_chr_id != -1) {
                        val chr_id = c.getInt(col_idx_chr_id).toLong()
                        if (chr_id > 0) {
                            currentLog.character_ids?.let {
                                if (chr_id !in it) it.add(chr_id)
                            }
                        }
                    }

                    if (col_idx_ses_id != -1) {
                        val ses_id = c.getInt(col_idx_ses_id).toLong()
                        if (ses_id > 0) {
                            currentLog.session_ids?.let {
                                if (ses_id !in it) it.add(ses_id)
                            }
                        }
                    }

                } while (c.moveToNext())
            }
            mDatabase?.setTransactionSuccessful()
            mDatabase?.endTransaction()
            c.close()
        }?.close()
        closeDatabase()
        return logs
    }

    /**
     * a general method to get a listOfViews of sessions from TABLE_SESSION
     *
     * @return all sessions matching the selectQuery.
     */
    private fun getSessions(selectQuery: String, KEY_ID: String): ArrayList<Session> {
        val sessions: ArrayList<Session> = ArrayList()
        openDatabase()
        mDatabase?.rawQuery(selectQuery, null)?.also { c ->
            // looping through all rows and adding to listOfViews
            if (c.moveToFirst()) {
                do {
                    val s = Session()
                    s.id = c.getInt(c.getColumnIndexOrThrow(KEY_ID)).toLong()
                    s.name = c.getString(c.getColumnIndexOrThrow(KEY_NAME))
                    s.isActive = (c.getInt(c.getColumnIndexOrThrow(KEY_IS_ACTIVE)) == 1)
                    s.created_at = c.getString(c.getColumnIndexOrThrow(KEY_CREATED_AT))
                    s.language = c.getString(c.getColumnIndexOrThrow(KEY_LANGUAGE))
                    s.ruleSystem = RuleSystem.allRuleSystems().find {
                        it.ref == c.getString(c.getColumnIndexOrThrow(KEY_RULE_SYSTEM))
                    } ?: RuleSystem.rule_system_default
                    s.showTotalExp = c.getInt(c.getColumnIndexOrThrow(KEY_SHOW_TOTAL_EXP)) == 1
                    s.showCurrentExp = c.getInt(c.getColumnIndexOrThrow(KEY_SHOW_CURRENT_EXP)) == 1
                    // adding to character listOfViews
                    sessions.add(s)
                } while (c.moveToNext())
            }
            c.close()
        }?.close()
        closeDatabase()
        return sessions
    }

    /**
     * general method to get Tags from TABLE_TAG
     *
     * @param KEY_ID the reference for the ID column in your query
     * @return all tags matching the query
     */
    fun getTags(selectQuery: String, getDates: Boolean, KEY_ID: String?): ArrayList<Tag> {
        val tags: ArrayList<Tag> = ArrayList()
        openDatabase()
        //Log.e(LOG, selectQuery);
        val c: Cursor? = mDatabase?.rawQuery(selectQuery, null)
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    val t: Tag = Tag()
                    t.id = c.getInt(c.getColumnIndexOrThrow(KEY_ID)).toLong()
                    t.name = c.getString(c.getColumnIndexOrThrow(KEY_NAME))
                    if (getDates) {
                        t.created_at = c.getString(c.getColumnIndexOrThrow(KEY_CREATED_AT))
                    }
                    tags.add(t)
                } while (c.moveToNext())
            }
            c.close()
            closeDatabase()
        }
        Collections.sort(tags, Tag.TAG_COMPARATOR)
        return tags
    } ////////////////////////////////////////////////////////////////////////////////////////////////

    companion object {
        private var mInstance: DatabaseHelper? = null
        @Synchronized
        fun getInstance(context: Context?): DatabaseHelper? {
            /*
            Use the application context, which will ensure that you
            don't accidentally leak an Activity's context.
            See this article for more information: http://bit.ly/6LRzfx
            */
            if (mInstance == null && context != null) {
                mInstance = DatabaseHelper(context.applicationContext)
            }
            return mInstance
        }

        val APP_EXTENAL_FILE_DIRECTORY: String = (Environment.getExternalStorageDirectory().absolutePath
                + "/CharTracker")
        val IMPORT_KEEP_LOCAL: Int = 0
        val IMPORT_OVERWRITE: Int = 1
        val IMPORT_DELETE_DATA: Int = 2

        val EXPORT_ALL_LOGS: Int = 0
        val EXPORT_CURRENT_LOGS: Int = 1
        val EXPORT_CURRENT_SESSIONS: Int = 2


        // ***************************************************************
        // Logcat tag
        private val LOG: String = "DatabaseHelper"
        // Database Version
        private val DATABASE_VERSION: Int = 31
        // Database Name
        private val DATABASE_NAME: String = "database"
        // Table Names
        private val TABLE_CHARACTER: String = "characters"
        private val TABLE_TAG: String = "tags"
        private val TABLE_LOG: String = "logs"
        private val TABLE_SESSION: String = "sessions"
        private val TABLE_LOG_TAG: String = "log_tags"
        private val TABLE_LOG_CHARACTER: String = "log_characters"
        private val TABLE_LOG_SESSION: String = "log_sessions"
        private val TABLE_FEAT: String = "feats"
        private val TABLE_CLASSES: String = "classes"
        private val TABLE_ARCHETYPES: String = "archetypes"
        private val TABLE_RACES: String = "races"
        private val TABLE_CLASSES_ARCHETYPES: String = "classes_archetypes"
        private val TABLE_SUBRACES: String = "subraces"
        private val TABLE_RACE_SUBRACE: String = "race_subrace"
        // Common column names
        private val KEY_ID: String = "id"
        private val KEY_CREATED_AT: String = "created_at"
        private val KEY_LAST_UPDATED_DATE: String = "last_updated_date"

        // CHARACTERS Table - column names
        private val KEY_NAME: String = "name"
        private val KEY_TOTAL_EXP: String = "total_exp"
        private val KEY_CURRENT_EXP: String = "current_exp"
        private val KEY_PRIORITY: String = "priority"
        private val KEY_IS_ACTIVE: String = "is_active"
        private val KEY_IS_RETIRED: String = "is_retired"
        private val KEY_RETIRED_AT: String = "retired_at"
        private val KEY_IS_DEAD: String = "is_dead"
        private val KEY_IS_DELETED: String = "is_deleted"
        private val KEY_ICON: String = "icon_id"
        private val KEY_RACE: String = "race"
        private val KEY_SUBRACE: String = "subrace"
        private val KEY_CLASS: String = "class"
        private val KEY_ARCHETYPE: String = "archetype"
        private val KEY_LEVEL: String = "level"
        private val KEY_ATTRIBUTE_POINT_COST: String = "exp_point_cost"
        private val KEY_ICON_PATH: String = "icon_path"
        private val KEY_LOOT: String = "loot"
        private val KEY_BUFFS: String = "buffs"
        private val KEY_STATS: String = "stats"
        private val KEY_PREFS: String = "prefs"
        private val KEY_TRADITION_ATTRIBUTE: String = "tradition_attribute"
        //the commit of pydnd repo which is valid for the character
        private val KEY_CURRENT_COMMIT: String = "current_commit"
        private val KEY_EXPAND_CHAR_PROG_ITEM_DICT: String = "expand_char_progress_item_dict"

        /*
         * physical and stun damages are not needed anymore. They are currently stored in character prefs
         */
        private val KEY_DAMAGES = "physical_damage" // is now used for damage in general (HashMap)
        private val KEY_STUN_DAMAGE = "stun_dmg" // not used anymore
        private val KEY_MONEY = "money"
        private val KEY_CHAR_RULE_SYSTEMS = "character_rule_systems"
        private val KEY_LOOTMANAGER_NAME = "loot_manager_name"
        private val KEY_USE_MANAGER_ACCESS = "use_manager_access"
        private val KEY_DISPLAY_SCHWARZGELD_WEALTH = "display_schwarzgeld_wealth"
        private val KEY_SCHWARZLOOT = "schwarzloot"
        private val KEY_CURRENT_EDGE = "current_edge"

        // TAGS Table - column names
        //private static final String KEY_NAME = "name";
        // LOGS Table - column names
        private val KEY_EXP: String = "exp"
        private val KEY_TEXT: String = "text"

        // SESSIONS Table - column names
        //private static final String KEY_NAME = "name";
        //private static final String KEY_IS_ACTIVE = "is_active";
        private val KEY_RULE_SYSTEM = "rule_system"
        private val KEY_SHOW_TOTAL_EXP = "show_total_exp"
        private val KEY_SHOW_CURRENT_EXP = "show_current_exp"
        private val KEY_LANGUAGE = "language"


        //FEATs Table - column names
        //private static final String KEY_ID = "id";
        private val KEY_ID_STRING: String = "id_string"
        //private static final String KEY_NAME = "name";
        private val KEY_LINKS: String = "links"

        //CLASSES TABLE - column names
        //private static final String KEY_ID = "id";
        //private static final String KEY_NAME = "name";
        private val KEY_LINK: String = "link"
        private val KEY_SKILLS_PER_LEVEL = "skills_per_level"
        private val KEY_HIT_POINT_RATE = "hit_point_rate"
        private val KEY_PROGRESSIONS = "saves_progression"
        private val KEY_CLASS_SKILLS = "class_skills"

        //ARCHETYPES TABLE - column names
        //private static final String KEY_ID = "id";
        //private static final String KEY_NAME = "name";
        private val UNIQUE_INDEX_ARCHETYPES_NAME = "unique_index_archetypes_name"

        //RACES TABLE - column names
        //private static final String KEY_ID = "id";
        //private static final String KEY_NAME = "name";
        //    private static final String KEY_LINK = "link";

        //SUBRACES TABLE - column names
        //private static final String KEY_ID = "id";
        //private static final String KEY_NAME = "name";
        private val UNIQUE_INDEX_SUBRACE_NAME = "unique_index_subrace_name"

        //////////////////////////////////////////////////////////
        /// RELATIONAL TABLES
        // LOG_TAGS Table - column names
        private val KEY_LOG_ID: String = "log_id"
        private val KEY_TAG_ID: String = "tag_id"
        // LOG_CHARACTERS Table - column names
        //private static final String KEY_LOG_ID = "log_id";
        private val KEY_CHARACTER_ID: String = "char_id"
        //LOG_SESSION Table - column names
        //private static final String KEY_LOG_ID = "log_id";
        private val KEY_SESSION_ID: String = "session_id"
        // CLASSES_ARCHETYPES Table - column names
        private val KEY_CLASS_NAME: String = "class_name"
        private val KEY_ARCHETYPE_NAME: String = "archetype_name"
        // RACE_SUBRACE Table - column names
        private val KEY_RACE_NAME: String = "race_name"
        private val KEY_SUBRACE_NAME: String = "subrace_name"
        ////////// Table Create Statements ///////////////
        // Characters table create statement
        private val TABLE_CHARACTER_COLUMNS = LinkedHashMap<String, String>().apply {
            put(KEY_ID, "INTEGER PRIMARY KEY")
            put(KEY_NAME, "TEXT")
            put(KEY_TOTAL_EXP, "INTEGER")
            put(KEY_CURRENT_EXP, "INTEGER")
            put(KEY_PRIORITY, "INTEGER")
            put(KEY_IS_ACTIVE, "INTEGER")
            put(KEY_IS_RETIRED, "INTEGER")
            put(KEY_IS_DEAD, "INTEGER")
            put(KEY_IS_DELETED, "INTEGER")
            put(KEY_CREATED_AT, "DATETIME")
            put(KEY_RETIRED_AT, "DATETIME")
            put(KEY_LAST_UPDATED_DATE, "DATETIME")
            put(KEY_ICON, "TEXT")
            put(KEY_RACE, "TEXT")
            put(KEY_SUBRACE, "TEXT")
            put(KEY_CLASS, "TEXT")
            put(KEY_ARCHETYPE, "TEXT")
            put(KEY_ATTRIBUTE_POINT_COST, "INTEGER")
            put(KEY_LEVEL, "INTEGER")
            put(KEY_ICON_PATH, "TEXT")
            put(KEY_LOOT, "TEXT")
            put(KEY_SCHWARZLOOT, "TEXT")
            put(KEY_BUFFS, "TEXT")
            put(KEY_STATS, "TEXT")
            put(KEY_PREFS, "TEXT")
            put(KEY_TRADITION_ATTRIBUTE, "TEXT")
            put(KEY_CURRENT_COMMIT, "TEXT")
            put(KEY_EXPAND_CHAR_PROG_ITEM_DICT, "TEXT")
            put(KEY_DAMAGES, "TEXT")
            put(KEY_MONEY, "INTEGER")
            put(KEY_CHAR_RULE_SYSTEMS, "TEXT")
            put(KEY_LOOTMANAGER_NAME, "TEXT")
            put(KEY_USE_MANAGER_ACCESS, "TEXT")
            put(KEY_DISPLAY_SCHWARZGELD_WEALTH, "TEXT")
            put(KEY_CURRENT_EDGE, "INTEGER")
        }
        private val CREATE_TABLE_CHARACTER: String = ("CREATE TABLE $TABLE_CHARACTER"
                + TABLE_CHARACTER_COLUMNS.map { it.key +  " " + it.value}
            .joinToString(prefix = "(", postfix = ")")
                )
        // Tag table create statement
        private val CREATE_TABLE_TAG: String = ("CREATE TABLE "
                + TABLE_TAG + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_CREATED_AT + " DATETIME" + ")")
        // Session table create statement
        private val TABLE_SESSIONS_COLUMNS = LinkedHashMap<String, String>().apply {
            put(KEY_ID , "INTEGER PRIMARY KEY")
            put(KEY_NAME , "TEXT")
            put(KEY_CREATED_AT , "DATETIME")
            put(KEY_RULE_SYSTEM , "TEXT")
            put(KEY_SHOW_TOTAL_EXP , "INTEGER")
            put(KEY_SHOW_CURRENT_EXP , "INTEGER")
            put(KEY_IS_ACTIVE , "INTEGER")
            put(KEY_LANGUAGE , "TEXT")
        }
        private val CREATE_TABLE_SESSION: String = ("CREATE TABLE $TABLE_SESSION"
                + TABLE_SESSIONS_COLUMNS.map { it.key +  " " + it.value}
            .joinToString(prefix = "(", postfix = ")")
                )
        // Log table create statement
        private val CREATE_TABLE_LOG: String = ("CREATE TABLE "
                + TABLE_LOG + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_EXP + " INTEGER,"
                + KEY_TEXT + " TEXT,"
                + KEY_LAST_UPDATED_DATE + " DATETIME,"
                + KEY_CREATED_AT + " DATETIME" + ")")
        // log_tags table create statement
        private val CREATE_TABLE_LOG_TAG: String = ("CREATE TABLE "
                + TABLE_LOG_TAG + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_LOG_ID + " INTEGER,"
                + KEY_TAG_ID + " INTEGER" + ")")
        // log_characters table create statement
        private val CREATE_TABLE_LOG_CHARACTER: String = ("CREATE TABLE "
                + TABLE_LOG_CHARACTER + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_LOG_ID + " INTEGER,"
                + KEY_CHARACTER_ID + " INTEGER" + ")")
        // log_characters table create statement
        private val CREATE_TABLE_LOG_SESSION: String = ("CREATE TABLE "
                + TABLE_LOG_SESSION + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_LOG_ID + " INTEGER,"
                + KEY_SESSION_ID + " INTEGER" + ")")
        // feat table create statement
        private val CREATE_TABLE_FEATS: String = ("CREATE TABLE "
                + TABLE_FEAT + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_ID_STRING + " TEXT,"
                + KEY_NAME + " TEXT,"
                + KEY_LINKS + " TEXT"
                + ")")
        //seperate more than one links with comma.
        // classes table create statement
        val TABLE_CLASSES_COLUMNS = LinkedHashMap<String, String>().apply {
            put(KEY_ID, "INTEGER PRIMARY KEY")
            put(KEY_NAME, "TEXT")
            put(KEY_LINK, "TEXT")
            put(KEY_SKILLS_PER_LEVEL, "INTEGER")
            put(KEY_HIT_POINT_RATE, "INTEGER")
            put(KEY_CLASS_SKILLS, "TEXT")
            put(KEY_PROGRESSIONS, "TEXT")
        }
        private val CREATE_TABLE_CLASSES: String = ("CREATE TABLE $TABLE_CLASSES"
                + TABLE_CLASSES_COLUMNS.map { it.key +  " " + it.value}
            .joinToString(prefix = "(", postfix = ")")
                )
        // archetypes table create statement
        private val CREATE_TABLE_ARCHETYPES: String = ("CREATE TABLE "
                + TABLE_ARCHETYPES + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT"
                + ")")
        // races table create statement
        private val CREATE_TABLE_RACES: String = ("CREATE TABLE "
                + TABLE_RACES + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_LINK + " TEXT"
                + ")")
        // classes_archetypes table create statement
        private val CREATE_TABLE_CLASSES_ARCHETYPES: String = ("CREATE TABLE "
                + TABLE_CLASSES_ARCHETYPES + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_CLASS_NAME + " TEXT,"
                + KEY_ARCHETYPE_NAME + " TEXT" + ")")
        // subraces table create statement
        private val CREATE_TABLE_SUBRACES: String = ("CREATE TABLE "
                + TABLE_SUBRACES + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT"
                + ")")
        // race_subrace table create statement
        private val CREATE_TABLE_RACE_SUBRACE: String = ("CREATE TABLE "
                + TABLE_RACE_SUBRACE + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_RACE_NAME + " TEXT,"
                + KEY_SUBRACE_NAME + " TEXT" + ")")
        val DEBUG_TAG: String = "DATABASE_HELPER"
    }

}