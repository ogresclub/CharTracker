package com.mtths.chartracker.widgets

import android.content.Context
import android.util.AttributeSet

/**
 * filtering has to be done by adapter. I need to prevent to textView from filtering because I
 * wanted to do it by myself
 */
class MyMultiAutoCompleteTextViewNonfiltering : MyMultiAutoCompleteTextView {
    constructor(context: Context) : super(context) {
        initThresholdAndTokenizer()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initThresholdAndTokenizer()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initThresholdAndTokenizer()
    }

    override fun performFiltering(text: CharSequence, keyCode: Int) {}
    override fun enoughToFilter(): Boolean {
        return super.enoughToFilter()
    }
}