package com.mtths.chartracker.widgets

import android.content.Context
import android.graphics.Rect
import androidx.appcompat.widget.AppCompatMultiAutoCompleteTextView
import android.util.AttributeSet
import android.view.View
import com.mtths.chartracker.utils.SpaceTokenizer

open class MyMultiAutoCompleteTextView : AppCompatMultiAutoCompleteTextView {
    var always_show = false

    constructor(context: Context) : super(context) {
        initThresholdAndTokenizer()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initThresholdAndTokenizer()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initThresholdAndTokenizer()
    }

    protected fun initThresholdAndTokenizer() {
        threshold = 1
        setTokenizer(SpaceTokenizer())
    }

    override fun enoughToFilter(): Boolean { //        Helper.log("MyMultiAutoCompleteTextView", "triggered enough to filter: always_show = " + always_show);
        return super.enoughToFilter() || always_show
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        showDropDownIfFocused()
    }

    fun showDropDownIfFocused() {
        if (enoughToFilter() && isFocused && windowVisibility == View.VISIBLE) {
            showDropDown()
        }
    }
}