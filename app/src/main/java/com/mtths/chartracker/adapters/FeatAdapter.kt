package com.mtths.chartracker.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mtths.chartracker.Global
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.FeatLink
import com.mtths.chartracker.dialogs.DialogShowFeatLinks

class FeatAdapter(val context: Context,
                  private val layoutResource: Int,
                  val featLink: FeatLink,
                  var onLinkClicked: ((String) -> Unit)? = null
) : RecyclerView.Adapter<FeatAdapter.FeatViewHolder>() {

    // ViewHolder class
    class FeatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: TextView = itemView as TextView

    }



    val iconMap = HashMap<String, Int>().apply {
        put("dndtools.org", R.drawable.ic_rule_system_dnd_5e)
        put("legacy.aonprd.com", R.drawable.ic_pf)
        put("ogres.club", R.drawable.ic_rule_system_ogres_club)
    }

    private fun getItem(position: Int): String? {
        try {
            return featLink.links[position]
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
        return null
    }

    override fun getItemCount(): Int {
        return featLink.links.size
    }

//    override fun getCount(): Int {
//        return featLink.links.size
//    }

    private fun getIcon(link: String): Int {
        var domain = link
        Global.PATTERN_DOMAIN.matcher(link).let { m ->
            if (m.find()) {
                m.group(1)?.let {
                    domain = it
                }
            }
        }
//        Helper.log("GET_LINK_IC", "domain: $domain, icon: ${iconMap[domain]}")
        return iconMap[domain] ?: R.drawable.ic_char_dead_head

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeatViewHolder {
        // Create a new view, which defines the UI of the list item
        val itemView = LayoutInflater.from(parent.context)
            .inflate(layoutResource, parent, false)
        return FeatViewHolder(itemView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: FeatViewHolder, position: Int) {
        getItem(position)?.let { link ->

            holder.textView.apply {
                val sb = Helper.generateImageSpannable(
                    context = context,
                    lineHeight = lineHeight,
                    icon = getIcon(link)
                ).
                append(link)
                text = sb
                setOnClickListener {
                    onLinkClicked?.invoke(link)
                }
            }


        }
    }

}