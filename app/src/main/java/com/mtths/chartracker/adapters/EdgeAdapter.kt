package com.mtths.chartracker.adapters

import android.content.Context
import android.widget.ArrayAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.mtths.chartracker.R
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.widgets.SquareImageView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class EdgeAdapter(context: Context) :
    ArrayAdapter<Boolean?>(context, R.layout.edge_item) {
    var currentEdge = 0
    var totalEdge = 1
    var defaultEdgeImageResource = R.drawable.ic_edge
    var edgeImageResource: Int? = null
    private val layoutResource = R.layout.edge_item
    var MAX_EDGE = 7


    override fun getCount(): Int {
        return MAX_EDGE
    }

    fun addEdge() {
        currentEdge++
        notifyDataSetChanged()
    }

    fun spendEdge() {
        if (currentEdge > 0) {
            currentEdge--
            notifyDataSetChanged()
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        var h: Holder?
        if (convertView == null) {
            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(layoutResource, parent, false).apply {
                Holder().let {
                    it.iv = this.findViewById(R.id.iv)
                    this.tag = it
                    h = it
                }
            }
        } else {
            h = convertView.tag as Holder
        }
        if (position +1 <= totalEdge) {
            h?.iv?.let { iv ->
                iv.visibility = View.VISIBLE
                edgeImageResource?.also {id ->
                    CoroutineScope(Dispatchers.Main).launch {
                        val d = async(context = Dispatchers.IO) {
                            Helper.decodeSampledBitmapFromResource(
                                res = context.resources,
                                resId = id,
                                reqHeight = 40,
                                reqWidth = 40
                            )
                        }
                        d.await().let {
                            iv.setImageBitmap(it)
                        }
                    }
                } ?: run {
                    iv.setImageResource(defaultEdgeImageResource)
                }
            }
        } else {
            h?.iv?.let {
               it.visibility = View.GONE
            }
        }
        if (position + 1 <= currentEdge) {
            convertView!!.setBackgroundResource(R.drawable.green_black_border)
        } else {
            convertView!!.setBackgroundResource(R.drawable.grey_black_border)
        }
        return convertView
    }
    class Holder() {
        var iv: SquareImageView? = null
    }
}