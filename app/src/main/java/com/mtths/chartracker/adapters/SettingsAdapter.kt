package com.mtths.chartracker.adapters

import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.FragmentActivity
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.dialogs.DialogDataOptions
import com.mtths.chartracker.utils.FileUtils
import com.mtths.chartracker.utils.Helper

class SettingsAdapter(
    val activity: FragmentActivity,
    val switchContainer: LinearLayout,
    val namedEditTextContainer: LinearLayout? = null,
    val character: Character? = null,
    val layoutResourceIdSwitch: Int = R.layout.settings_switch,
    val layoutResourceIdNamedEditText: Int = R.layout.settings_named_edit_text) {

    val prefs = PrefsHelper(activity)
    val dbHelper = DatabaseHelper(activity)

     class EditTextViewHolder(
         val nameId: Int,
         val editText: EditText?
     )

    var editTextViewHolders : MutableList<EditTextViewHolder> = mutableListOf()

    class NamedEditText(
        var editText: EditText? = null,
        var label: TextView? = null,
        var container: LinearLayout? = null)

    private fun buildNamedEditText(data: StringData): NamedEditText {
        val inflater = LayoutInflater.from(activity)
        val namedEditText = NamedEditText()
        namedEditText.container = (inflater.inflate(layoutResourceIdNamedEditText, null, false) as LinearLayout)
            .apply {
                namedEditText.editText = findViewById(R.id.editText)
                namedEditText.label = findViewById(R.id.label)
            }
        namedEditText.label?.apply {
            text = context.getString(data.nameId)
            data.description?.let { descr ->
                setOnClickListener {
                    DialogCustom.showAlterDialog(
                        title = context.getString(data.nameId),
                        msg = descr,
                        fragmentManager = activity.supportFragmentManager
                    )
                }
            }

        }
        namedEditText.editText?.apply {
            setText(data.get())
            inputType = data.inputType
        }
        LinearLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        ).apply {
            topMargin = activity.resources.getDimensionPixelSize(
                if (data.startNewGroup) {
                    R.dimen.setting_distance_between_items_groups
                } else {
                    R.dimen.setting_distance_between_items
                })
            data.marginStartDimenId?.let { marginStart = activity.resources.getDimensionPixelSize(it) }
            namedEditText.container?.layoutParams = this
        }
        return namedEditText
    }


    private fun buildSwitch(data: SwitchData, tasks: HashMap<Int, () -> Unit>): SwitchCompat {
        val inflater = LayoutInflater.from(activity)
        val switch = (inflater.inflate(layoutResourceIdSwitch, null, false) as SwitchCompat)
            .apply {
                text = context.getString(data.nameId, *data.nameIdFormatParams)
                isChecked = data.get()
                setOnCheckedChangeListener { compoundButton, b ->
                    data.set(b)
                    tasks[data.nameId]?.invoke()
                }
            }
        FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        ).apply {
            topMargin = activity.resources.getDimensionPixelSize(
                if (data.startNewGroup) {
                    R.dimen.setting_distance_between_items_groups
                } else {
                    R.dimen.setting_distance_between_items
                })

            data.marginStartDimenId?.let { marginStart = activity.resources.getDimensionPixelSize(it) }
            switch.layoutParams = this
        }
        return switch
    }

    fun refreshSwitches() {
        switchContainer.removeAllViews()
        inflateCharSettingsSwitches()
    }

    /**
     * apply changes in editTexts
     * switches changes are applied instantly, but editTexts only when apply button is pressed
     */
    fun applyEditTextChanges(data: List<StringData>, tasks: HashMap<Int, () -> Unit>) {
        val msgs = mutableListOf<String>()
        val changesNameIds = mutableListOf<Int>()
        editTextViewHolders.forEach { h ->
            data.find { it.nameId == h.nameId }?.let { d ->
                h.editText?.text?.toString()?.let {
                    d.set(it)?.let { newValue ->
                        changesNameIds.add(h.nameId)
                        msgs.add(
                            activity.resources.getString(R.string.msg_settings_value_changed,
                                activity.resources.getString(h.nameId),
                                newValue)
                        ) } }
            }
        }
        if (msgs.isNotEmpty()) {
            Toast.makeText(activity, msgs.joinToString("\n"), Toast.LENGTH_SHORT).show()
        }
        changesNameIds.forEach { tasks[it]?.invoke() }
    }

    fun applySettingsChanges(tasks: HashMap<Int, () -> Unit>) {
        applyEditTextChanges(data = mutableListOf<StringData>().apply {
            addAll(getStringDataSettings())
            addAll(getStringDataCharSettings())
        },
            tasks = tasks)
    }


    fun inflateNamedEditTexts(data: List<StringData>) {
        data.forEach { d ->
            if (d.condition()) {
                namedEditTextContainer?.addView(
                    buildNamedEditText(d).also {
                        editTextViewHolders.add(
                            EditTextViewHolder(nameId = d.nameId, editText = it.editText)
                        )
                    }.container
                )
            }
        }
    }

    fun inflateSwitches(data: HashMap<String,List<SwitchData>>, tasks: HashMap<Int, () -> Unit> = HashMap()) {
        data.keys.forEach {key ->
            data[key]?.forEachIndexed {index, d ->
                if (index == 0) {
                    d.startNewGroup = true
                }
                if (d.condition()) {
                    switchContainer.addView(
                        buildSwitch(data = d, tasks = tasks))
                }
            }
        }
    }

    fun inflateCharSettings() {
        inflateCharSettingsSwitches()
        inflateCharSettingsNamedEditTexts()
    }



    fun inflateCharSettingsSwitches() {
        inflateSwitches(
            getSwitchDataCharSettings()
                .also { if (it.isEmpty())
                    Log.e("CHAR_SETTING_SWITCHES",
                        "cant inflate switches, no character was given!") }
        )
    }
    fun inflateCharSettingsNamedEditTexts() {
        inflateNamedEditTexts(
            getStringDataCharSettings()
                .also { if (it.isEmpty())
                    Log.e("CHAR_SETTING_SWITCHES",
                        "cant inflate switches, no character was given!") }
        )
    }
    fun inflateGenesisImportSettings() {
        inflateSwitches(
            getSwitchDataGenesisImportSettings()
        )
    }

    fun inflateDataOptionsSettings() {
        inflateSwitches(
            getSwitchDataDataOptions()
        )
    }

    fun inflateSettings(
        /**
         * additional tasks when setting data
         * relate data by its nameId
         */
        tasks: HashMap<Int, () -> Unit>) {
        inflateSettingsSwitches(tasks = tasks)
        inflateSettingsNamedEditTexts()
    }

    private fun inflateSettingsNamedEditTexts() {
        inflateNamedEditTexts(getStringDataSettings())
    }
    private fun inflateSettingsSwitches(
        /**
         * additional tasks when setting the switch
         * relate the switch by its nameId
         */
        tasks: HashMap<Int, () -> Unit>) {
        inflateSwitches(
            data = getSwitchDataSettings(),
            tasks = tasks
        )
    }

    open class SwitchData(
        val nameId: Int,
        val nameIdFormatParams: Array<String> = arrayOf(),
        val get: () -> Boolean,
        val set: (Boolean) -> Unit,
        val marginStartDimenId: Int? = null,
        val condition: () -> Boolean = {true}) {
        /**
         * use to put the right top margin for the first item in the group
         * does not need to be defined by user
         */
        var startNewGroup = false
    }
    open class StringData(
        val nameId: Int,
        val get: () -> String,
        val set: (String) -> String?,
        val marginStartDimenId: Int? = null,
        val condition: () -> Boolean = {true},
        val inputType: Int = InputType.TYPE_CLASS_TEXT,
        /**
         * when clicking the label description will be displayed to explain the setting
         */
        val description: String? = null
        ) {
        /**
         * use to put the right top margin for the first item in the group
         * does not need to be defined by user
         */
        var startNewGroup = false
    }


    private fun getStringDataSettings(): List<StringData> {
        return listOf(
            StringData(
                nameId = R.string.setting_label_hg_exp_percentage,
                get = {prefs.hgExpPercentage.toString()},
                set = {it.trim().toIntOrNull()?.let { newValue ->
                    if (newValue != prefs.hgExpPercentage) {
                        prefs.hgExpPercentage = newValue
                        character?.hgExpPercentage = newValue
                        newValue.toString()
                    } else {
                        null
                    }
                }},
                condition = { prefs.rulesSplittermond && activity is ActivityMain },
                inputType = InputType.TYPE_CLASS_NUMBER,
                description = activity.getString(R.string.settings_description_hg_exp_percentage)
                        + "\n\n" + activity.getString(R.string.settings_global_hint)

            ),
            StringData(
                nameId = R.string.setting_label_fokus_percentage,
                get = {prefs.fokusPercentage.toString()},
                set = {it.trim().toIntOrNull()?.let { newValue ->
                    if (newValue != prefs.fokusPercentage) {
                        prefs.fokusPercentage = newValue
                        character?.fokusPercentage = newValue
                        newValue.toString()
                    } else {
                        null
                    }
                }},
                condition = { prefs.rulesSplittermond && activity is ActivityMain },
                inputType = InputType.TYPE_CLASS_NUMBER,
                description = activity.getString(R.string.settings_description_fokus_percentage)
                        + "\n\n" + activity.getString(R.string.settings_global_hint)
            ),
            StringData(
                nameId = R.string.setting_label_status_bar_off_set_main_activity,
                get = {prefs.statusBarOffsetMainActivity.toString()},
                set = {it.trim().toIntOrNull()?.let { newValue ->
                    if (newValue != prefs.statusBarOffsetMainActivity) {
                        prefs.statusBarOffsetMainActivity = newValue
                        newValue.toString()
                    } else {
                        null
                    }
                }},
                condition = { prefs.edgeToEdge && activity is ActivityMain },
                inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED,
                description = activity.getString(R.string.settings_description_status_bar_off_set)
                        + "\n\n" +  activity.getString(
                    R.string.settings_description_status_bar_off_set_addition_main_activity
                        )
            ),
            StringData(
                nameId = R.string.setting_label_status_bar_off_set_char_details,
                get = {prefs.statusBarOffsetCharDetails.toString()},
                set = {it.trim().toIntOrNull()?.let { newValue ->
                    if (newValue != prefs.statusBarOffsetCharDetails) {
                        prefs.statusBarOffsetCharDetails = newValue
                        newValue.toString().also {
                            Log.d("BUIPAGF", "new value for char details off set: $it")
                        }
                    } else {
                        Log.d("BUIPAGF", "NO new value for char details off set")
                        null
                    }
                }},
                condition = { prefs.edgeToEdge && activity is ActivityCharDetails },
                inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED,
                description = activity.getString(R.string.settings_description_status_bar_off_set)
            ),
            StringData(
                nameId = R.string.setting_label_session_display_height_correction,
                get = {prefs.sessionDisplayHeightCorrection.toString()},
                set = {it.trim().toIntOrNull()?.let { newValue ->
                    if (newValue != prefs.sessionDisplayHeightCorrection) {
                        prefs.sessionDisplayHeightCorrection = newValue
                        newValue.toString()
                    } else {
                        null
                    }
                }},
                condition = { activity is ActivityMain },
                inputType = InputType.TYPE_CLASS_NUMBER,
                description = activity.getString(R.string.settings_description_status_bar_off_set)
            )
        )
    }

    fun getSwitchDataSettings(): LinkedHashMap<String, List<SwitchData>> { return LinkedHashMap<String, List<SwitchData>>().apply {

        put("Homebrew", listOf(
            SwitchData(
                nameId = R.string.setting_label_homebrew_splittermond_hp,
                get = { prefs.splittermondHomeBrew },
                set = { prefs.splittermondHomeBrew = it },
                condition = {prefs.rulesSplittermond && activity is ActivityMain},
            )
        ))
        put("Exp", listOf(
            SwitchData(
                nameId = R.string.setting_switch_label_show_total_exp,
                get = { prefs.showTotalExp },
                set = { prefs.showTotalExp = it },
                condition = { activity is ActivityMain}
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_show_current_exp,
                get = { prefs.showCurrentExp },
                set = { prefs.showCurrentExp = it },
                condition = { activity is ActivityMain}
            )

        ))
        put("Calculation Checks", listOf(
            SwitchData(
                nameId = R.string.setting_switch_label_check_exp_mistakes,
                get = { prefs.checkExpCalcMistakes },
                set = { prefs.checkExpCalcMistakes = it }
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_check_featured_stats,
                get = { prefs.checkFeaturedStats },
                set = { prefs.checkFeaturedStats = it }
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_check_languages,
                get = { prefs.checkFeaturedLanguages },
                set = { prefs.checkFeaturedLanguages = it }
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_check_filter_constraint_errors,
                get = { prefs.checkFilterConstraintErrors },
                set = { prefs.checkFilterConstraintErrors = it }
            )
        ))
        put("Display Characters", listOf(
            SwitchData(
                nameId = R.string.setting_switch_label_show_chars_related_to_session,
                get = { prefs.showCharsRelatedToSession },
                set = { prefs.showCharsRelatedToSession = it },
                condition = { activity is ActivityMain}
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_show_only_active_session_chars,
                get = { prefs.showOnlyActiveSessionsCharacters },
                set = { prefs.showOnlyActiveSessionsCharacters = it },
                condition = { activity is ActivityMain}
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_show_priorities,
                get = { prefs.showPriorities },
                set = { prefs.showPriorities = it },
                condition = { activity is ActivityMain}
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_show_char_names,
                get = { prefs.showCharNames },
                set = { prefs.showCharNames = it },
                condition = { activity is ActivityMain}
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_show_rule_system_icons,
                get = { prefs.showRuleSystemIcons },
                set = { prefs.showRuleSystemIcons = it },
                condition = { activity is ActivityMain}
            )
        ))
        put("Dropbox", listOf(
            SwitchData(
                nameId = R.string.setting_switch_label_auto_upload_char_icons,
                get = { prefs.autoUploadCharIcons },
                set = { prefs.autoUploadCharIcons = it }
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_auto_import_char_icons,
                get = { prefs.autoImportIcons },
                set = { prefs.autoImportIcons = it }
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_auto_download_char_items,
                get = { prefs.autoDownloadCharItems },
                set = { prefs.autoDownloadCharItems = it },
                condition = { (character?.ruleSystem ?: prefs.ruleSystem).showFragmentCharItems }
            )
        ))
        put("Other", listOf(
            SwitchData(
                nameId = R.string.setting_switch_label_edge_to_edge_display,
                get = { prefs.edgeToEdge },
                set = { prefs.edgeToEdge = it }
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_color_date_changes_in_logs,
                get = { prefs.colorDateChanges },
                set = { prefs.colorDateChanges = it },
                condition = { activity is ActivityMain}
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_show_session_stats,
                get = { prefs.showSessionStatsMainScreen },
                set = {
                    prefs.showSessionStatsMainScreen = it
                },
                condition = {activity is ActivityMain}
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_show_session_stats,
                get = { prefs.showSessionStatsCharDetails },
                set = {
                    prefs.showSessionStatsCharDetails = it
                },
                condition = {activity is ActivityCharDetails}
            ),
            SwitchData(
                nameId = R.string.setting_switch_label_backup_db_on_app_start,
                nameIdFormatParams = arrayOf(prefs.localCharTrackerFolder?.let {
                    "\n${FileUtils.getLastPathSegmentNoType(it.uri)}/" +
                            DialogDataOptions.FILE_NAME_DB_EXPORT
                } ?: ""),
                get = { prefs.backupDatabaseOnAppStart },
                set = {bool ->
                    if (bool && prefs.localCharTrackerFolder == null) {
                        DialogCustom.showConfirmDialog(
                            msg = activity.getString(R.string.msg_activte_backup_database_on_start),
                            onConfirm = {
                                (activity as? ActivityMain?)?.selectCharTrackerFolderToActivateDBBackupOnStart?.launch(null)
                            },
                            onCancel = {},
                            fragmentManager = activity.supportFragmentManager,
                        )
                    } else {
                        prefs.backupDatabaseOnAppStart = bool
                    }

                }
            )
        ))
    }
    }

    private fun getSwitchDataGenesisImportSettings(): LinkedHashMap<String,List<SwitchData>> {
        return LinkedHashMap<String, List<SwitchData>>().apply {

            put("Genesis", listOf(
                SwitchData(
                    nameId = R.string.setting_switch_label_delete_char_on_genesis_update,
                    get = { prefs.deleteGenesisCharOnUpdate },
                    set = {
                        prefs.deleteGenesisCharOnUpdate = it
                    }
                )
            ))

        }
    }
    private fun getSwitchDataDataOptions(): LinkedHashMap<String,List<SwitchData>> {
        return LinkedHashMap<String, List<SwitchData>>().apply {

            put("Local Xml", listOf(
                SwitchData(
                    nameId = R.string.setting_switch_label_use_stored_ct_folder,
                    nameIdFormatParams = arrayOf(
                        (prefs.localCharTrackerFolder?.uri?.let {
                            FileUtils.getLastPathSegmentNoType(it)
                        } + "/${DialogDataOptions.FILE_NAME_XML_EXPORT}") ?: "None",

                    ),
                    get = { prefs.useStoredCharTrackerFolder },
                    set = {
                        prefs.useStoredCharTrackerFolder = it
                    },
                    condition = { prefs.localCharTrackerFolder != null }
                )
            ))

        }
    }

    private fun getStringDataCharSettings(): List<StringData> {
        return mutableListOf<StringData>().apply {

            character?.let { c ->
                add(
                    StringData(
                        nameId = R.string.label_priority,
                        get = {c.priority.toString()},
                        set = { it.trim().toIntOrNull()?.let { newPriority ->
                            if (c.priority != newPriority) {
                                c.priority = newPriority
                                newPriority.toString()
                            } else {
                                null
                            }
                            }
                        },
                        inputType = InputType.TYPE_NUMBER_FLAG_SIGNED,
                        description = activity.getString(R.string.settings_description_priority)
                    )
                )
                add(
                    StringData(
                        nameId = R.string.label_lootmanager_name,
                        get = {c.lootManagerDownloadName},
                        set = {val newLmName = it.trim()
                            if (c.lootManagerDownloadName != newLmName) {
                                c.lootManagerName = newLmName
                                newLmName
                            } else {
                                null
                            }
                        },
                        condition = {(c.ruleSystem ?: prefs.ruleSystem).showFragmentCharItems},
                        description = activity.getString(R.string.settings_description_lootmanager_name)

                    )
                )
                add(
                    StringData(
                        nameId = R.string.setting_label_hg_exp_percentage,
                        get = {c.hgExpPercentage.toString()},
                        set = {it.trim().toIntOrNull()?.let { newValue ->
                            if (newValue != c.hgExpPercentage) {
                                c.hgExpPercentage = newValue
                                newValue.toString()
                            } else {
                                null
                            }
                        }},
                        condition = { prefs.rulesSplittermond },
                        inputType = InputType.TYPE_CLASS_NUMBER,
                        description = activity.getString(R.string.settings_description_hg_exp_percentage)

                    )
                )
                add(
                    StringData(
                        nameId = R.string.setting_label_fokus_percentage,
                        get = {c.fokusPercentage.toString()},
                        set = {it.trim().toIntOrNull()?.let { newValue ->
                            if (newValue != c.fokusPercentage) {
                                c.fokusPercentage = newValue
                                newValue.toString()
                            } else {
                                null
                            }
                        }},
                        condition = { prefs.rulesSplittermond },
                        inputType = InputType.TYPE_CLASS_NUMBER,
                        description = activity.getString(R.string.settings_description_fokus_percentage)
                    )
                )
            }
        }


    }
    private fun getSwitchDataCharSettings(): LinkedHashMap<String,List<SwitchData>> {
        return LinkedHashMap<String, List<SwitchData>>().apply {

            character?.let { c ->

                put("Homebrew", listOf(
                    SwitchData(
                        nameId = R.string.setting_label_homebrew_splittermond_hp,
                        get = { c.splittermondHomeBrew },
                        set = { c.splittermondHomeBrew = it },
                        condition = {prefs.rulesSplittermond}
                    )
                ))

                put("Manager", listOf(
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_manager_access,
                        get = { c.useManagerAccess },
                        set = {
                            c.useManagerAccess = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        },
                        condition = { (c.ruleSystem ?: prefs.ruleSystem).showFragmentCharItems }
                    )
                ))

                put("Geldwäsche", listOf(
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_schwarzgeld,
                        get = { prefs.enableSchwarzgeld || c.hasSchwarzgeld },
                        set = {
                            prefs.enableSchwarzgeld = it
                            refreshSwitches()
                        },
                        condition = { (c.ruleSystem ?: prefs.ruleSystem).showFragmentCharItems }
                    ),
                    /**
                     * enable to make schwarzgeld be included in calculation of wealth
                     */
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_display_schwarzgeld,
                        get = { c.displaySchwarzgeldWealth },
                        set = {
                            c.displaySchwarzgeldWealth = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        },
                        condition = { (c.ruleSystem ?: prefs.ruleSystem).showFragmentCharItems && (character.hasSchwarzgeld || prefs.enableSchwarzgeld) },
                        marginStartDimenId = R.dimen.setting_text_view__sub_margin_start
                    )
                )
                )


                put("DisplayToggles", listOf(
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_display_treasure_in_wallet,
                        get = { c.displayTreasureInWallet },
                        set = {
                            c.displayTreasureInWallet = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        }),
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_show_buff_categories,
                        get = { c.showBuffCategories },
                        set = {
                            c.showBuffCategories = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        }),
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_show_specs_with_description_only_extended,
                        get = { c.showSpecsWithDescriptionOnlyExtended },
                        set = {
                            c.showSpecsWithDescriptionOnlyExtended = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        }),
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_show_spell_schools,
                        get = { c.showSpellSchools },
                        set = {
                            c.showSpellSchools = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        },
                        condition = { Helper.activeRules(
                            allowedRules = listOf(
                                RuleSystem.rule_system_Dnd_E5,
                                RuleSystem.rule_system_skillfull_dnd_5E,
                                RuleSystem.rule_system_splittermond),
                            activeRuleSystems = character.ruleSystems ?: prefs.activeRuleSystems)
                        }
                    ),
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_show_cast_stat,
                        get = { c.showCastStat },
                        set = {
                            c.showCastStat = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        },
                        condition = {prefs.rulesSkillfull5E}
                        ),
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_show_all_skills,
                        get = { c.showAllSkills},
                        set = {
                            c.showAllSkills = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        }),
                    SwitchData(
                        nameId = R.string.char_setting_switch_label_show_attributes,
                        get = { c.showAttributes },
                        set = {
                            c.showAttributes = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        }),
                )
                )

                put("Utils", listOf(
                    SwitchData(
                        nameId = R.string.setting_switch_label_update_buff_activity_according_to_item_equipped_state,
                        get = { c.getPrefs(activity).updateBuffActivityAccordingToItemEquippedState },
                        set = {
                            c.getPrefs(activity).updateBuffActivityAccordingToItemEquippedState = it
                            dbHelper.updateCharacter(c, keep_last_update_date = true)
                        }),
                ))
            }
        }
    }

    companion object {

        fun getTasks(recreateActivity: () -> Unit): HashMap<Int, () -> Unit> = HashMap<Int, () -> Unit>().apply {
            put(R.string.setting_switch_label_show_session_stats, recreateActivity)
            put(R.string.setting_switch_label_edge_to_edge_display, recreateActivity)
            put(R.string.setting_label_status_bar_off_set_main_activity, recreateActivity)
            put(R.string.setting_label_status_bar_off_set_char_details, recreateActivity)
        }
    }
}