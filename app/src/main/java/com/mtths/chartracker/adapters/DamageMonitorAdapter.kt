package com.mtths.chartracker.adapters

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import android.widget.ArrayAdapter
import com.mtths.chartracker.R
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.widgets.SquareImageView
import kotlin.math.max

class DamageMonitorAdapter(
    context: Context,
    var size: Int,
    /**
     * These damage types are all summed up for total damage
     * values of the map are image resources to determine how this damage type is displayed
     * keys are resourceIds of the string defining the name of the damage type
     */
    var damages: Array<Damage> = arrayOf(
        Damage(
            nameResourceId = R.string.damage_type_default,
            imageResource = R.drawable.red_black_border,
            index = 0)
    )
    ) :
    ArrayAdapter<Boolean?>(context, R.layout.status_monitor_item) {

    data class Damage(
        val nameResourceId: Int,
        val imageResource: Int,
        var amount: Int = 0,
        val index: Int,
        val textColor: Int? = null) : Parcelable {

        fun getName(context: Context) = context.getString(nameResourceId)

        constructor(parcel: Parcel) : this(
            nameResourceId = parcel.readInt(),
            imageResource = parcel.readInt(),
            amount = parcel.readInt(),
            index = parcel.readInt(),
            textColor = parcel.readValue(Int::class.java.classLoader) as? Int
        )

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeInt(nameResourceId)
            parcel.writeInt(imageResource)
            parcel.writeInt(amount)
            parcel.writeInt(index)
            parcel.writeValue(textColor)
        }

        override fun describeContents(): Int = 0

        companion object CREATOR : Parcelable.Creator<Damage> {
            override fun createFromParcel(parcel: Parcel): Damage {
                return Damage(parcel)
            }

            override fun newArray(size: Int): Array<Damage?> {
                return arrayOfNulls(size)
            }
        }
    }


    val prefs = PrefsHelper(context)

    var doReplace: () -> Boolean = {false}


    fun log(msg: String) = Log.d("STATUS_MONITOR_ADAPTER", "#### $msg")

    var selectedDamageType: Int = 0
    private val totalDamage
        get() = damages.sumOf { it.amount }

    var totalHp = 0
    private val layoutResource = R.layout.status_monitor_item

    override fun getCount(): Int {
        return size
    }

    /**
     * set amount of damage of "selectedDamageType"
     * if replace() is true, damage of higher index will be replaced by damage of lower instead
     */
    fun putDamage(value: Int) {
        val type = selectedDamageType
        log("do replace = ${doReplace()}, damages = {${damages.joinToString { "${it.getName(context)}: ${it.amount}" }}}")
        if (doReplace()) {
            val currentValue = damages[type].amount
            val diff = value - currentValue
            var index = type
            log("new value: $value, current value = $currentValue, diff = $diff, index = $index")

            damages[type].amount = value

            if (diff > 0) {
                // convert "higher" damage to damage of current type
                var a = diff
                while (a > 0) {
                    index++
                    if (index >= damages.size) {
                        break
                    }
                    a -= damages[index].amount

                    /*
                     set damage of index to 0 if more is to remove than this damage has value
                     or to the damage left
                     */
                    damages[index].amount =max(0, -a)
                }
            }
        } else {
            damages[type].amount = value
        }

    }

    fun getDamageUntil(index: Int): Int {
        return damages.sumOf { if (it.index < index) it.amount else 0 }

    }

    fun addDamagePoint() {
//        log("add damage point called: type = $type")
        val type = selectedDamageType
        putDamage( damages[type].amount + 1)
        notifyDataSetChanged()
    }

    fun healDamagePoint() {
//        log("heal damage point called: type = $type")
        val type = selectedDamageType
        damages[type].amount.let {
//            log("heal 1 dmg from type: $type. damages = ${damages.keys.joinToString { "$it: ${damages[it]}" }}")
            if (it > 0) {
                putDamage(it - 1)
            }
        }
        notifyDataSetChanged()
    }

    fun clearAllDamage() {
        damages.forEach { it.amount = 0 }
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var convertView = convertView
        var holder: ViewHolder
        if (convertView != null) {
            holder = convertView.tag as ViewHolder
            holder.imageView?.destroyDrawingCache()
        } else {
            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(layoutResource, parent, false)
            holder = ViewHolder()
            holder.textView = convertView.findViewById(R.id.text)
            holder.imageView = convertView.findViewById(R.id.icon)
            convertView.tag = holder

        }

        var damage = 0

        if (position + 1 in (totalDamage + 1)..totalHp) {
            holder.imageView?.setBackgroundResource(R.drawable.green_black_border)
        } else if (position + 1 > totalHp) {
            holder.imageView?.setBackgroundResource(R.drawable.grey_black_border)
        }

        damages.forEach { d ->

            if (position + 1 in (damage + 1..damage + d.amount)) {
                var imageResource = d.imageResource
                if (position + 1 > totalHp) {
                    imageResource = R.drawable.grey_and_red_black_border
                }
                holder.imageView?.setBackgroundResource(imageResource)
                d.textColor?.let {
                    Log.d("DAMAGE_TEXT", "textcolor is: $it")
                    holder.textView?.setTextColor(context.getColor(it)) }
            }

            damage += d.amount
        }

        holder.textView?.text = "%s".format(position + 1)


        return convertView!!
    }

    class ViewHolder(
        var textView: TextView? = null,
        var imageView: SquareImageView? = null
    )
}