package com.mtths.chartracker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.mtths.chartracker.Item
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.ItemCategory

class CategoriesAdapter(val context: Context):
    RecyclerView.Adapter<CategoriesAdapter.ItemCategoryViewHolder>() {

    var data: List<String> = listOf()
    var catMap = ItemCategory.createCategoriesMap()
    var filterItemsByCategory: () -> Unit = {}

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCategoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.item_categorie, parent, false)
        return ItemCategoryViewHolder(v)
    }

    override fun onBindViewHolder(holder: ItemCategoryViewHolder, position: Int) {
        val catName = data[position]
        val cat = catMap[catName] ?: ItemCategory("None")
        holder.name?.text = catName
        if (catName.contentEquals("all")) {
            holder.icon?.visibility = View.GONE
        } else {
            holder.icon?.visibility = View.VISIBLE
            holder.icon?.setImageResource(cat.getIconImageResource(Item()))
        }
        PrefsHelper(context).let { prefs ->
            prefs.activeItemCategories?.let { activeCats ->
                val isActive = catName in activeCats
                displayActivity(isActive = isActive, container = holder.container)
            }
        }

        holder.container?.setOnClickListener {
            PrefsHelper(context).let { prefs ->
                prefs.activeItemCategories?.let { activeCats ->
                    val wasActive = catName in activeCats

                    if (!catName.contentEquals("all")) {

                        //update UI
                        displayActivity(isActive = !wasActive, container = holder.container)

                        //store active state
                        if (wasActive) {
                            prefs.activeItemCategories = activeCats.filterNot {
                                it.contentEquals(
                                    catName,
                                    ignoreCase = true
                                )
                            }
                        } else {
                            val nActiveCats = activeCats.distinct().toMutableList()
                            nActiveCats.add(catName)
                            prefs.activeItemCategories = nActiveCats
                        }
                    } else {
                        /*
                        select all button has been pressed
                        */
                        Toast.makeText(context, "Try long click!", Toast.LENGTH_SHORT).show()
                    }
                }
                filterItemsByCategory()

            }
        }

        holder.container?.setOnLongClickListener {
            if (!catName.contentEquals("all")) {
                PrefsHelper(context).let { prefs ->
                    prefs.activeItemCategories = listOf(catName)
                }
            } else {
                //select all categories button has been pressed
                PrefsHelper(context).activeItemCategories =
                    data.filterNot { it.contentEquals("all") }
            }
            notifyDataSetChanged()
            filterItemsByCategory()
            return@setOnLongClickListener true
        }
    }

    fun displayActivity(container: View?, isActive: Boolean) {

        if (isActive) {
            container?.setBackgroundResource(R.drawable.holo_green_light_some_transparent_round_corners)
        } else {
            container?.setBackgroundResource(R.drawable.light_grey_some_transparent_round_corners)
        }
    }

    class ItemCategoryViewHolder(v: View): RecyclerView.ViewHolder (v) {
        var name: TextView? = null
        var icon: ImageView? = null
        var container : View? = null

        init {
            name = v.findViewById(R.id.name)
            icon = v.findViewById(R.id.icon)
            container = v.findViewById(R.id.container)
        }
    }
}
