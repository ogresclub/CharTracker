package com.mtths.chartracker.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.ImageSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.LogAdapter.SearchConstraint.Companion.EXP_CONSTRAINT_ANY
import com.mtths.chartracker.adapters.LogAdapter.SearchConstraint.Companion.EXP_CONSTRAINT_NEGATIVE
import com.mtths.chartracker.adapters.LogAdapter.SearchConstraint.Companion.EXP_CONSTRAINT_POSITIVE
import com.mtths.chartracker.utils.Helper.getBoldWords
import com.mtths.chartracker.utils.Helper.getChar
import com.mtths.chartracker.utils.Helper.getEmphWords
import com.mtths.chartracker.utils.Helper.hasNoPreOrSuffix
import com.mtths.chartracker.utils.Helper.setColor
import com.mtths.chartracker.utils.Helper.setStyle
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterQuests
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.listeners.ListenerImplOnDoubdleClickShowFeatLinks
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.pedromassango.doubleclick.DoubleClick
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.StringUtils
import java.lang.Exception
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * insert LogEntries in a ListView
 */
class LogAdapter (private val mActivity: ProgressBarFrameDropBoxActivity,
                  private val layout_resource: Int,
                  logs: ArrayList<LogEntry>,
                  private val toStringOption: String,
                  excludedTags: ArrayList<String>? = null) : RecyclerView.Adapter<LogAdapter.ViewHolder>(), Filterable {


    val DEBUG_TAG_LOG_ADAPTER = "LOG_ADAPTER"
    //Instance variables
//    private int layout_resource;
    private val dbHelper: DatabaseHelper?
    private var excludedTags: ArrayList<String>?
    private var highlightText: ArrayList<String>? = null
    private var allLogs: List<ColoredLogEntry> = ArrayList()
    private var filteredLogs: List<ColoredLogEntry>? = ArrayList()
    private val mFilter = LogsFilter()
    private var markErrors = false
    private val context: Context
        get() = mActivity
    //Log Entries containing those word will not be displayed;
    private var excludedWords: ArrayList<String>? = null

    /**
     * when filtering, this number of log Entries will be displaed before and after each match
     */
    var searchContextSize: Int = 0
    var allowFiltering = true


    internal constructor(activity: ProgressBarFrameDropBoxActivity,
                         layout_resource: Int,
                         toStringOption: String) : this(activity, layout_resource, ArrayList<LogEntry>(), toStringOption, null) {
    }

    override fun getItemCount(): Int {
        return filteredLogs!!.size
    }

    suspend fun deleteFilteredLogs(): List<LogEntry>? {
        val logs = filteredLogs?.map { it.logEntry }
        Helper.deleteLogs(context, logs)
        return logs
    }

    fun setLogs(logs: ArrayList<LogEntry>) {
        allLogs = logs.map { ColoredLogEntry(logEntry = it) }
    }

    fun setAndShowAllLogs(logs: ArrayList<LogEntry>) {
        setLogs(logs = logs)
        filter.filter("")
    }

    fun setHighlightText(highlightText: ArrayList<String>?) {
        this.highlightText = highlightText
    }

    fun setExcludedTags(excludedTags: ArrayList<String>?) {
        this.excludedTags = excludedTags
    }

    fun setMarkErrors(markErrors: Boolean) {
        this.markErrors = markErrors
        //        logWithLogAdapterTag("markErrors = " + this.markErrors);
    }

    /**
     * @param excludedWords null means exclude nothing
     */
    fun setExcludedWords(excludedWords: ArrayList<String>?) {
        this.excludedWords = excludedWords
    }

    override fun getFilter(): Filter {
        return mFilter
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(layout_resource, parent, false)
        return ViewHolder(v)
    }

    private fun setLogEntryText(coloredLogEntry: ColoredLogEntry, holder: ViewHolder) {
        var logEntryText = ""
        var showCharIcons = true
        val logEntry = coloredLogEntry.logEntry

        when (toStringOption) {
            EXP_TEXT -> {
                logEntryText = logEntry.toString_ExpText()
                showCharIcons = false
                //        } else if (toStringOption.equals(CHARS_TAGS_EXP_TEXT)) {
                //            logEntryText = logEntry.toString_TagsExpText(context);
            }
            TAGS_EXP_TEXT -> {
                logEntryText = logEntry.toStringTagsExpText(context, excludedTags)
                showCharIcons = false
                //        } else if (toStringOption.equals(DATE_EXP_CHARS_TAGS_TEXT)) {
                //            logEntryText = logEntry.toString_DateTagsExpText(context);
            }
            CHARS_EXP_TEXT -> {
                logEntryText = logEntry.toString_ExpText()
                //        } else if (toStringOption.equals(SESSION_CHARS_EXP_TEXT)) {
                //            logEntryText = logEntry.toString_SessionExpText(context);
            }
            else -> {
                logEntryText = logEntry.toString_ExpText()
                //            Helper.log("Invalid toStringOption for LogAdapter. Used default setting: 'exp_chars_tags_text'");
            }
        }

        holder.logTextView?.apply {
            text = getFormatedText(
                logEntryText = logEntryText,
                showCharIcons = showCharIcons,
                logEntry = logEntry,
                lineHeight = lineHeight,
                noSubmatches = !coloredLogEntry.showSubStringMatches)
        }

    }

    private fun colorLogEntries(
        coloredLogEntry: ColoredLogEntry,
        position: Int,
        holder: ViewHolder
    ) {
        if (PrefsHelper(context = context).colorDateChanges) {

            /*
            color log Entry when date changed
             */
            coloredLogEntry.reset()

            val lastColoredLogEntry = if (position == 0) coloredLogEntry else filteredLogs!![position-1]

            coloredLogEntry.copySettings(cl = lastColoredLogEntry)

            val dateString = lastColoredLogEntry.logEntry.dateOfCreation?.let {
                Global.DATE_FORMAT_NO_TIME.format(it)
            } ?: ""
            val lastDateString = coloredLogEntry.logEntry.dateOfCreation?.let {
                Global.DATE_FORMAT_NO_TIME.format(it)
            } ?: ""
//            log("TIITI", "date: $dateString // $lastDateString")
            if (dateString != lastDateString) {
                coloredLogEntry.switchColor(highlightLastLog = lastColoredLogEntry.doHighlight)
//                log("TIITI", "switched color: hightlight = ${coloredLogEntry.doHighlight}")

            }
        }

        holder.logTextView?.setBackgroundResource(coloredLogEntry.coloredBgResource)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val coloredLogEntry = filteredLogs!![position]
        val logEntry = coloredLogEntry.logEntry

        holder.logTextView?.setOnClickListener(DoubleClick(ListenerImplOnDoubdleClickShowFeatLinks(mActivity, logEntry.text)))
        holder.logTextView?.setOnLongClickListener(ListenerImplOpenEditLogsOnLongClick(mActivity.supportFragmentManager, logEntry))
        holder.logTextView?.movementMethod = LinkMovementMethod.getInstance()

        setLogEntryText(coloredLogEntry = coloredLogEntry, holder = holder)
        colorLogEntries(coloredLogEntry = coloredLogEntry, position = position, holder = holder)


        /*
        mark errors
         */
        logWithLogAdapterTag(logEntry.text + " ERROR = " + logEntry.hasError)
        logWithLogAdapterTag("mark errors = $markErrors")
        if (markErrors && logEntry.hasError) holder.logTextView!!.setBackgroundResource(R.drawable.button_holo_red_light)
        //            else holder.logTextView.setBackgroundResource(R.drawable.button_holo_green_light);

    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v) {
        var logTextView: TextView? = null

        init {
            logTextView = v as TextView
        }
    }

    @Synchronized
    fun getFormatedText(logEntryText: String, showCharIcons: Boolean, logEntry: LogEntry, lineHeight: Int, noSubmatches: Boolean): SpannableStringBuilder {
        val sb: SpannableStringBuilder = if (showCharIcons) { //generate char icons
            generateCharIcons(logEntry, lineHeight)
        } else {
            SpannableStringBuilder()
        }

        /*
        Format String (tags italic and search query blue) and then
        append logEntry text after icons
        */
        sb.append(setFormatingSpans(
            sb = SpannableStringBuilder(logEntryText),
            originalText = logEntryText,
            logEntry = logEntry,
            noSubmatches = noSubmatches))
        return sb
    }

    private data class SearchConstraint(
        /**
         * elements in the inner list are connected with AND
         * and the resulting booleans are than connected with OR
         */
        var query: String = "",
        var words: ArrayList<ArrayList<String>> = ArrayList(),
        var filterExp: Boolean = false,
        var expConstraint: Int? = null,
        var expConstraintType: Int = EXP_CONSTRAINT_ANY,
        var dateFrom: Date? = null,
        var dateUntil: Date? = null,
        var showOnlyFullMatches: Boolean = false) {

        fun getAllWords(): List<String> {
            val result = ArrayList<String>()
            words.forEach {
                result.addAll(it)
            }
            return result.distinct()
                .also{
//                Log.d("CHECK_W_BRACK", "all words = ${it.joinToString { "'$it'" }}")
            }
        }

        companion object {
            const val EXP_CONSTRAINT_ANY = 0
            const val EXP_CONSTRAINT_POSITIVE = 1
            const val EXP_CONSTRAINT_NEGATIVE = 2
        }
    }


    /////////////////////////////////// FILTER /////////////////////////////////////////////////////
    private inner class LogsFilter : Filter() {


        /**
         * split by or and then get all words between (they are connected with and)
         * remove logic brackets before getting words
         */
        private fun getWords(s: String): ArrayList<ArrayList<String>> {
            val result = ArrayList<ArrayList<String>>()
            s.split(OR_SEPARATOR).forEach {
                if (it.isNotEmpty()) {
                    val list = ArrayList<String>()
                    val m = GET_WORDS_PATTERN.matcher(
                        it
                            .replace(OPEN_LOGIC_BRACKET, "")
                            .replace(CLOSE_LOGIC_BRACKET, "")
                    // we use those kinds of brackets only for search logics,
                    // so we don't want them in the qords to check
                    )
                    while (m.find()) {
                        //no Quotations
                        m.group(1)?.let { list.add(it) }
                        //quotes
                        m.group(2)?.let { list.add(it) }
                    }
//                    log("LOGSFILTER", "getWords: $list")
                    result.add(list)
                }
            }
//            log("LOGSFILTER", "getWords: done")
            return result
        }

        private fun getExpConstraint(s: String): SearchConstraint {
            val result = SearchConstraint()
            val m = PATTERN_EXP_CONSTRAINT.matcher(s)
            result.filterExp = m.find()
            if (result.filterExp) {
                val expConstrStr = m.group(1)
                result.expConstraint = if (expConstrStr in listOf<String?>(".", "+", "-")) {
                    null
                } else {
                    expConstrStr?.toInt()
                }
                result.expConstraintType = when (expConstrStr) {
                    "." -> EXP_CONSTRAINT_ANY
                    "+" -> EXP_CONSTRAINT_POSITIVE
                    "-" -> EXP_CONSTRAINT_NEGATIVE
                    else -> EXP_CONSTRAINT_ANY
                }

            }
//            log("TITI", "search results after only getting exp constraint: ${result.toString()}")
            return result
        }

        private fun getDateConstraints(s: String, constraint: SearchConstraint): SearchConstraint {
            var m = PATTERN_DATE_CONSTRAINT_FROM.matcher(s)
            if (m.find()) {
                m.group(1)?.let { constraint.dateFrom = Global.DATE_FORMAT_NO_TIME.parse(it) }
            }

            m = PATTERN_DATE_CONSTRAINT_UNTIL.matcher(s)
            if (m.find()) {
                m.group(1)?.let { constraint.dateUntil = Global.DATE_FORMAT_NO_TIME.parse(it) }
            }
            return constraint
        }

        private fun getOnlyFullMatchesValue(s: String, constraint: SearchConstraint): SearchConstraint {
            var m = PATTERN_ONLY_FULL_MATCHES.matcher(s)
            if (m.find()) {
                m.group(1)?.let { constraint.showOnlyFullMatches = it.toBooleanStrict() }
            }
            return constraint
        }

        private fun getSearchConstraints(constraint: CharSequence): SearchConstraint {
            var rawQuery = constraint.toString().trim()
            var result = getExpConstraint(rawQuery)
            rawQuery = rawQuery.replace(Regex(EXP_CONSTRAINT_REGEX, RegexOption.IGNORE_CASE), "").trim()
            result = getOnlyFullMatchesValue(s = rawQuery, constraint = result)
            rawQuery = rawQuery.replace(Regex("${SHOW_ONLY_FULL_MATCHES_INDICATOR}true", RegexOption.IGNORE_CASE), "").trim()
            result = getDateConstraints(s = rawQuery, constraint = result)
            rawQuery = rawQuery.replace(Regex("($FROM_INDICATOR|$UNTIL_INDICATOR)$DATE_CONSTRAINT_REGEX",
                RegexOption.IGNORE_CASE), "").trim()
            result.query = rawQuery
            result.words = getWords(rawQuery).also {
//                Log.d("CHECK_W_BRACK", "get words from query: '${rawQuery}':: ${it.joinToString { it.joinToString(prefix = "[[", postfix = "]]") }}")
            }
            return result
        }

        fun stringContainsWord(sLowered: String, query: String, noSubmatches: Boolean): Boolean {
            val m = Pattern.compile(".*(${Regex.escape(query)}).*").matcher(sLowered)
            var match = false
            while (m.find()) {
                if (noSubmatches) {
                    val start = m.start(1)
                    if (start != -1) {
                        match = match || hasNoPreOrSuffix(
                            textLowerCase = sLowered,
                            name = query,
                            start = m.start(1)
                        )
                    }
                } else {
                    match = true
                }
            }
            return match
        }

        /**
         * check if the actual search query matches the text
         */
        fun checkQuery(
            query: String,
            origTextLowered: String,
            fullMatches: Boolean
        ): Boolean {
            return getWords(query).any { it.all {
                stringContainsWord(
                origTextLowered,
                it.lowercase(),
                noSubmatches = fullMatches
            ) } }
        }

        override fun performFiltering(constraint: CharSequence): FilterResults {
            val results = FilterResults()
            val resultValues = ArrayList<ColoredLogEntry>()

            Log.d("CHECK_W_BRACK", "perform filtering called: search constraint: '$constraint'")
            val constr = getSearchConstraints(constraint)

            /*
            keep track, which logs have been added already
            when a log matches the search query, all logs with searchContextSize distance of indices
            will be added as well
             */
            val addedLogIndices = HashMap<Int, Boolean>()


            //highlight searched words
            setHighlightText(constr.getAllWords().toCollection(ArrayList()))

            /*
            first filter logs by excluded words:
             */
            //get boolean to check conditions
            var notExcludedLogs = allLogs
            excludedWords?.map { it.lowercase(Locale.getDefault())}?.toSet()?.let { exclWords ->
                notExcludedLogs = allLogs.filter{ cLog ->
                    val otl = cLog.logEntry.text.lowercase(Locale.getDefault())
                    Helper.getFilterConstraints(otl).intersect(exclWords).isEmpty()
                }
            }

            //mark all log initially as not added
            if (allowFiltering) {
                for (idx in notExcludedLogs.indices)
                    addedLogIndices[idx] = false
            }

            /*
            filter logs according to search query
             */
            for ((idx, coloredLogEntry) in notExcludedLogs.withIndex()) {
                val e = coloredLogEntry.logEntry
                val otl = e.text.lowercase(Locale.getDefault())


                /*
                 * if all words entered in the search query (except those marked as tags) are found somewhere in the text
                 * (ignoring cases) of the log Entry logTextMatchQuery will be set to true. Else false.
                 */
                var logTextMatchQuery = constr.words.isEmpty()
                var logExpMatchQuery = true
                var logDateMatchQuery = true
                coloredLogEntry.showSubStringMatches = !constr.showOnlyFullMatches

                if (allowFiltering) {

                    val logFromDateMatchQuery = constr.dateFrom == null ||
                            e.dateOfCreation?.after(constr.dateFrom) == true

                    val logUntilDateMatchQuery = constr.dateUntil == null ||
                            e.dateOfCreation?.before(constr.dateUntil) == true
                    logDateMatchQuery = logFromDateMatchQuery && logUntilDateMatchQuery


                    if (logDateMatchQuery) {

                        if (StringUtils.checkRespectingBrackets(
                            check = { s, q -> checkQuery(q, s, constr.showOnlyFullMatches)},
                            s = otl,
                            q = constr.query
                        )) {
                            logTextMatchQuery = true
                        }
                    }

                    logExpMatchQuery = !constr.filterExp || (
                                    (constr.expConstraint == null && (
                                            (constr.expConstraintType == EXP_CONSTRAINT_ANY && e.exp != 0) ||
                                                    (constr.expConstraintType == EXP_CONSTRAINT_POSITIVE && e.exp > 0) ||
                                                    (constr.expConstraintType == EXP_CONSTRAINT_NEGATIVE && e.exp < 0)
                                            ) ||
                                            e.exp == constr.expConstraint
                                            )
                            )

                    //add fitting entries to resultValues

                    if (logTextMatchQuery &&
                        logExpMatchQuery &&
                        logDateMatchQuery) {
                        for (i in idx - searchContextSize..idx + searchContextSize) {
                            if (i >= 0 && i < notExcludedLogs.size) {
//                            log("index $i, already added: ${addedLogIndices[i]}")
                                if (addedLogIndices[i] != true) {
                                    resultValues.add(notExcludedLogs[i])
                                    addedLogIndices[i] = true
                                }
                            }
                        }
                    }
                } else {
                    resultValues.add(coloredLogEntry)
                }
            }

            results.values = resultValues
            results.count = resultValues.size
            //log("FILTERUNG", "perform filtering completed")
            return results
        }


        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
//            log("FILTERUNG", "publish results called")
            filteredLogs = null

            if (results?.values != null) {
                filteredLogs = results.values as ArrayList<ColoredLogEntry>
            }
            if (filteredLogs == null) {
                filteredLogs = ArrayList()
            }
            notifyDataSetChanged()
//            log("FILTERUNG", "publish results finished")
        }
    }

    //////////////////////////////////// PRIVATE METHODS //////////////////////////////////////////////
    private fun generateCharIcons(e: LogEntry, lineHeight: Int): SpannableStringBuilder {
        val sb = SpannableStringBuilder()
        //we need to create placeholders for the charicons.
        for (id in e.character_ids!!) {
            val charIcon: Spannable = SpannableString("i ")
            getChar(id)?.let {  c ->
                c.getSmallIconForLogEntry(context, lineHeight)?.let { d ->
                    val span = ImageSpan(d, ImageSpan.ALIGN_BASELINE)
                    charIcon.setSpan(span, 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
                }
                sb.append(charIcon)
            }
        }
        return sb
    }

    /**
     * bold_matches are the words that should be displayed as bold
     */
    @Synchronized
    private fun setFormatingSpans(
        sb: SpannableStringBuilder,
        originalText: String,
        logEntry: LogEntry,
        bold_matches: ArrayList<String> = getBoldWords(originalText),
        emph_matches: ArrayList<String> = getEmphWords(originalText),
        noSubmatches: Boolean
    ): Spannable {

        val formatedTextResult = Helper.formatText(
            activity = mActivity,
            sb = sb,
            originalText = originalText,
            bold_matches = bold_matches,
            emph_matches = emph_matches,
            italic_matches = logEntry.getTagNames(dbHelper, true),
        )

        var sb = formatedTextResult.spannableStringBuilder
        val originalText = formatedTextResult.originalTextWithoutStyleMarkers



        /*
        color = yellow if originalText contains a @smth which is used to mark a entry used to
        track char Progression
        (e.g. @Feat, @Skill, @Language or @Attribute)
         */

        var colorLogEntry = false
        for (w in Global.charProgressionWords) {
            colorLogEntry = colorLogEntry || originalText.contains(w)
        }
        if (colorLogEntry) {
            sb = setColor(
                arrayListOf(originalText),
                originalText,
                ContextCompat.getColor(context, R.color.text_highlight_color_char_progress),
                sb)
        }


        /*
        set Links to Quest Names
         */
        try {
            val questAdapter = (mActivity as ActivityMain).charProgressItemsMap[R.string.filter_constraint_quest]?.mainItem?.adapter as CharProgressItemAdapterQuests

            val quests = questAdapter.getAutoCompleteDataOrDataIfNoAutoCompleteDataAvailable()
            for (i in quests.indices) {
                val quest = quests[i]
                val s = "%s ${quest.name}".format(context.getString(R.string.filter_constraint_quest))
                var start = originalText.indexOf(s)
                while (start >= 0) {
                    val clickableSpan: ClickableSpan = object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            val activity_tag = mActivity.tag
                            val isCharDetails = activity_tag.equals(ActivityCharDetails.ACTIVITY_TAG, ignoreCase = true)
                            DialogShowFilteredLogs.newInstance(
                                searchString = "\"$s\"",
                                activity_char_details = isCharDetails,
                                use_session_logs = true).
                            show(mActivity.supportFragmentManager, "showTags")
                        }
                    }
                    sb.setSpan(clickableSpan, start, start + s.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                    sb.setSpan(ForegroundColorSpan(mActivity.resources.getColor(R.color.quest_name_color)),
                        start, start + s.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                    start = originalText.indexOf(s, start + 1)
                }
            }

        } catch (e: Exception) {
//            log(DEBUG_TAG_LOG_ADAPTER, "getting quest adapter from charProgressItems in Activity Main failed")
        }


        /*
        highlight search Query
         */
        sb = setColor(
            textToChange = highlightText,
            originalText = originalText,
            ForegroundColorSpanConstructorArg = Color.BLUE,
            spannable = sb,
            noSubstringMatches = noSubmatches)
        sb = setStyle(
            textToChange = highlightText,
            originalText = originalText,
            styleSpanConstructorArg = Typeface.BOLD,
            spannable = sb,
            noSubstringMatches = noSubmatches)


        /*
        make tags italic
         */
        val tags = logEntry.getTagNames(dbHelper, true)
        sb = setStyle(tags, originalText, Typeface.ITALIC, sb)
        return sb
    }

    companion object {
        //toStringOptions Variables
        const val TAGS_EXP_TEXT = "exp_tags_text"
        const val EXP_TEXT = "exp_text"
        const val CHARS_TAGS_EXP_TEXT = "exp_chars_tags_text"
        const val DATE_EXP_CHARS_TAGS_TEXT = "date_exp_chars_tags_text"
        const val CHARS_EXP_TEXT = "exp_chars_text"
        const val SESSION_CHARS_EXP_TEXT = "session_chars_exp_text"
        const val THUMBNAIL_QUALITY = 80
        val EXP_CONSTRAINT_REGEX = "exp=(-?\\d+|\\.|\\+|-)"
        val PATTERN_EXP_CONSTRAINT = Pattern.compile(EXP_CONSTRAINT_REGEX)
        val DATE_CONSTRAINT_REGEX = "\\d\\d-\\d\\d-\\d\\d\\d\\d"
        val SHOW_ONLY_FULL_MATCHES_INDICATOR = "full="
        val FROM_INDICATOR = "from="
        val UNTIL_INDICATOR = "until="
        val PATTERN_DATE_CONSTRAINT_FROM = Pattern.compile("$FROM_INDICATOR($DATE_CONSTRAINT_REGEX)")
        val PATTERN_ONLY_FULL_MATCHES = Pattern.compile("$SHOW_ONLY_FULL_MATCHES_INDICATOR(true)")
        val PATTERN_DATE_CONSTRAINT_UNTIL = Pattern.compile("$UNTIL_INDICATOR($DATE_CONSTRAINT_REGEX)")
        val GET_WORDS_PATTERN = Pattern.compile("(?:([^\"]\\S*)|\"(.+?)(?:\"|$))\\s*")
        const val OR_SEPARATOR = " OR "
        const val OPEN_LOGIC_BRACKET = "\\("
        const val CLOSE_LOGIC_BRACKET = "\\)"



        const val DEFAULT_BACK_GROUND_DRAWABLE =
            R.drawable.light_grey_half_transparent_round_corners
        const val ALTERNATIVE_BACK_GROUND_DRAWABLE =
            R.drawable.light_grey_yellowish_half_transparent_round_corners

    }

    private fun logWithLogAdapterTag(msg: String) {
//        log(DEBUG_TAG_LOG_ADAPTER, msg)
    }

    data class ColoredLogEntry(var logEntry: LogEntry) {
        var coloredBgResource: Int = DEFAULT_BACK_GROUND_DRAWABLE
        var doHighlight: Boolean = false
        var showSubStringMatches = true

        fun switchColor(highlightLastLog: Boolean) {
            doHighlight = !highlightLastLog
            coloredBgResource = if (doHighlight) ALTERNATIVE_BACK_GROUND_DRAWABLE else DEFAULT_BACK_GROUND_DRAWABLE
        }

        fun reset() {
            coloredBgResource = DEFAULT_BACK_GROUND_DRAWABLE
            doHighlight = false
        }

        fun copySettings(cl: ColoredLogEntry) {
            coloredBgResource = cl.coloredBgResource
            doHighlight = cl.doHighlight
        }
    }

    data class ColoredString(var names: MutableList<String>, val color: Int)

    /**
     * Create a new LogAdapter
     *
     * @param activity        needed to load DataHelper and shoe Feats
     * @param layout_resource layout layout_resource to inflate layout in listrows from
     * @param toStringOption  - "exp_text" or "exp_tags_text" or "exp_chars_tags_text" or "date_exp_chars_tags_text"
     */
    init {
        this.excludedTags = excludedTags
        dbHelper = DatabaseHelper.getInstance(mActivity)
        setLogs(logs)
    }

}