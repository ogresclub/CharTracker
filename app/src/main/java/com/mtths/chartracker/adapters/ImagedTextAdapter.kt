package com.mtths.chartracker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.ImagedText
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.Session
open class ImagedTextAdapter(context: Context,
                        private var layoutResource: Int,
                        textViewResourceId: Int,
                        list: List<ImagedText>):
    ArrayAdapter<ImagedText>(context, layoutResource, textViewResourceId, list) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView != null) {
            holder = convertView.tag as ViewHolder
            holder.imageView?.destroyDrawingCache()
        } else {
            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(layoutResource, parent, false)
            holder = ViewHolder()
            holder.textView = convertView.findViewById(R.id.text)
            holder.imageView = convertView.findViewById(R.id.icon)
            convertView.tag = holder
        }

        getItem(position)?.let {
//            Helper.log("RuleSystems", "get View pos $position: ${rS.name}, ${rS.icon}")
            holder.textView?.text = it.text
            holder.imageView?.setImageResource(it.icon)
        }


        return convertView!!

    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getView(position, convertView, parent)
    }

    class ViewHolder(
        var textView: TextView? = null,
        var imageView: ImageView? = null
    )
}