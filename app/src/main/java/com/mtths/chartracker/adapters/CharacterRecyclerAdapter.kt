package com.mtths.chartracker.adapters
//
//import android.content.Context
//import android.graphics.Typeface
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.LinearLayout
//import android.widget.RelativeLayout
//import android.widget.TextView
//import androidx.core.content.ContextCompat
//import androidx.recyclerview.widget.RecyclerView
//import kotlinx.coroutines.*
//import java.util.*
//import kotlin.math.max
//import kotlin.math.min
//import kotlin.math.sqrt
//
//
///**
// * An Adapter to display an Array of Characters in a RecyclerView
// */
//
//// todo replace all characterGridViews with recyclerviews?!
//
//class CharacterRecyclerAdapter(private val context: Context,
//                               private var layoutResource: Int,
//                               private var chars: ArrayList<Character> = ArrayList(),
//                               private val thumbNailQuality: Int,
//                               private var deactivateDeadAndRetiredChars: Boolean = true,
//                               var numColsGridView: Int)
//    : RecyclerView.Adapter<CharacterRecyclerAdapter.ViewHolder>() {
//
//    var showExp = true
//    var textSizeFactor = 1f
//    var initialTextSizeCharName = 12f
//    var initialTextSizeExp = 14f
//    var initialSizeSessionIcon = 50
//    val minTextSizeExp = 7f
//    val prefs: PrefsHelper = PrefsHelper(context)
//    val dbHelper = DatabaseHelper.getInstance(context)
//
//    fun noExp() {
//        showExp = false
//    }
//
//    fun setChars(chars: ArrayList<Character>) {
//        this.chars = chars
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val inflater = LayoutInflater.from(parent.context)
//        val v  = inflater.inflate(layoutResource, parent, false)
//        return ViewHolder(viewItem = v, showExp = showExp)
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//
//        holder.icon?.destroyDrawingCache()
//        holder.sessionIconsContainer?.removeAllViews()
//        holder.scope?.cancel()
//
//        val char = chars[position]
//        fillData(holder =  holder, char =  char)
//
//        /*
//        either use custom icon and small simple icon or only big simple icon and no small icon
//         */
//        holder.scope = CoroutineScope(Dispatchers.Main)
//        holder.scope?.launch {
//            Helper.log("SCOPEE", "LAUNCHED scope $position")
//
//            //-------------------------- MAIN ICON -------------------------------------//
//            val d = async(context = Dispatchers.IO) {
//                char.decodeCharIcon(context, thumbNailQuality)
//            }
//            d.await()?.let{
//                holder.icon?.setImageDrawable(it)
//            }
//
//            //-------------------------- SESSION ICONS -------------------------------------//
//
//            char.ruleSystems?.also {
//                inflateSessionIcons(ruleSystems = it, holder = holder)
//            } ?: CoroutineScope(Dispatchers.Main).launch {
//                val job = async(context = Dispatchers.IO) {
//                    dbHelper?.getRuleSystemsRelatedToCharacter(char_id = char.id)
//                        ?.distinct()
//                }
//                val rSList = job.await()
//                char.ruleSystems = rSList
//                launch(Dispatchers.IO) {
//                    dbHelper?.updateCharacter(char, keep_last_update_date = true)
//                }
//                fillData(holder =  holder, char =  char)
//                inflateSessionIcons(ruleSystems =  rSList, holder =  holder)
//            }
//
//
//            //-------------------------- SIMPLE ICON -------------------------------------//
//            val showSimpleCharIcons = char.iconFileName.isNotBlank()
//            holder.simpleCharIcon?.setImageResource(
//                if (showSimpleCharIcons) {
//                    Global.charIconsMapWErrorIcon[char.iconName] ?: Character.ERROR_ICON
//                } else {
//                    R.color.transparent
//                }
//            )
//
//
//        }
//    }
//
//    override fun getItemCount(): Int {
//        return chars.size
//    }
//
//    private fun fillData(holder: ViewHolder, char: Character) {
//
//        val (id, name, _, _, _, isActive, isRetired, isDead) = char
//
//
//        var displayString = if (prefs.showCharNames) name else ""
//        displayString += if (PrefsHelper(context).showPriorities) {
//            "\n(${char.priority})"
//        } else {
//            ""
//        }
//
//        holder.charName?.text = displayString
//
//
//        holder.charName?.textSize = initialTextSizeCharName * textSizeFactor
//        var textSizeExp: Float = initialTextSizeExp
//        if (textSizeFactor < 1) {
//            val maxTextSizeExp: Float = initialTextSizeExp * sqrt(textSizeFactor)
//            textSizeExp = maxTextSizeExp * 5 / numColsGridView
//            textSizeExp = min(maxTextSizeExp, textSizeExp)
//            textSizeExp = max(minTextSizeExp, textSizeExp)
//        }
//        if (showExp) {
//            if (prefs.showCurrentExp) {
//                holder.currentExp?.visibility = View.VISIBLE
//                holder.currentExp?.text = getCurrentExp(char)
//                holder.currentExp?.textSize = textSizeExp
//                setTextColor(holder.currentExp, char)
//            } else {
//                holder.currentExp?.visibility = View.GONE
//            }
//
//            if (prefs.showTotalExp) {
//                holder.totalExp?.visibility = View.VISIBLE
//                holder.totalExp?.text = getTotalExp(char)
//                holder.totalExp?.setTypeface(null, Typeface.BOLD)
//                holder.totalExp?.textSize = textSizeExp
//                setTextColor(holder.totalExp, char)
//            } else {
//                holder.totalExp?.visibility = View.GONE
//            }
//        }
//
//        if (isDead && isRetired && deactivateDeadAndRetiredChars) {
//            holder.background?.setBackgroundResource(R.drawable.button_violet_red_gradient_half_transparent_for_character)
//        } else if (isDead && deactivateDeadAndRetiredChars) {
//            holder.background?.setBackgroundResource(R.drawable.button_holo_red_light_for_character)
//        } else if (isRetired && deactivateDeadAndRetiredChars) {
//            holder.background?.setBackgroundResource(R.drawable.button_violet_half_transparent_for_character)
//        } else if (isActive) {
//            holder.background?.setBackgroundResource(R.drawable.button_holo_green_light_for_character)
//        } else {
//            holder.background?.setBackgroundResource(R.drawable.button_light_grey_for_character)
//        }
//    }
//
//    private fun inflateSessionIcons(ruleSystems: List<RuleSystem>?, holder: ViewHolder) {
//        if((dbHelper?.getAllRuleSystems()?.size ?: 0) > 1 ) {
//            ruleSystems?.forEach { rS ->
//                val size = initialSizeSessionIcon
//                val siv = SquareImageView(context)
//                val layoutParams = RelativeLayout.LayoutParams(size, size)
//                siv.layoutParams = layoutParams
//                siv.setImageResource(rS.icon)
//                holder.sessionIconsContainer?.addView(siv)
//            }
//        }
//    }
//
//    fun getCurrentExp(character: Character): String {
//        return when {
//            Helper.rulesSr5(character, context) -> String.format(Locale.GERMANY, "%,d¥", character.money)
//            else -> String.format(Locale.GERMANY, "%,d",character.currentExp)
//        }
//    }
//
//    fun getTotalExp(character: Character): String {
//        return when {
//            Helper.rulesSr5(character, context) -> String.format(Locale.GERMAN, "%d/%d", character.currentExp, character.totalExp)
//            else -> String.format(Locale.GERMANY, "%,d", character.totalExp)
//        }
//
//    }
//
//    fun setTextColor(tv: TextView?, character: Character) {
//        tv?.setTextColor(when {
//            prefs.rulesSR5 -> ContextCompat.getColor(context, android.R.color.holo_green_light)
//            else -> ContextCompat.getColor(context, R.color.text_color_default)
//        })
//    }
//
//
//
//    class ViewHolder(viewItem: View, showExp: Boolean) : RecyclerView.ViewHolder(viewItem) {
//
//        var charName: TextView? = null
//        var currentExp: TextView? = null
//        var totalExp: TextView? = null
//        var icon: SquareImageView? = null
//        var background: RelativeLayout? = null
//        var sessionIconsContainer: LinearLayout? = null
//        var simpleCharIcon: SquareImageView? = null
//        var scope: CoroutineScope? = null
//
//        init {
//            charName = viewItem.findViewById(R.id.charButton)
//            icon = viewItem.findViewById(R.id.icon)
//            sessionIconsContainer = viewItem.findViewById(R.id.character_session_icon_container)
//            background = viewItem.findViewById(R.id.background)
//
//            if (showExp) {
//                totalExp = viewItem.findViewById(R.id.total_exp_label)
//                currentExp = viewItem.findViewById(R.id.current_exp_label)
//            }
//            simpleCharIcon = viewItem.findViewById(R.id.simple_char_icon)
//        }
//
//    }
//}