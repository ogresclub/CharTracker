package com.mtths.chartracker.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.View.OnLongClickListener
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.MultiAutoCompleteTextView
import android.widget.TextView
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.Global
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterAttribute.Companion.getAttributeMod
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterClasses
import com.mtths.chartracker.dataclasses.*
import com.mtths.chartracker.dataclasses.BuffEffect.Companion.BUFF_REF_VERTEIDIGUNG
import com.mtths.chartracker.dataclasses.Character.Companion.STAT_DAMAGE_REDUCTION
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_ogres_club
import com.mtths.chartracker.dialogs.DialogCurrentCommit
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.HttpUtils
import com.mtths.chartracker.utils.SnackBarUtils
import com.mtths.chartracker.utils.TranslationHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.HashMap

/**
 * inflates given stats with value as
 * left|Stat            value|right
 */
class StatsAdapter(
    val mActivity: FragmentActivity?,
    val statContainer: LinearLayout,
    val baseStatContainer: LinearLayout,
    val coordinatorLayout: CoordinatorLayout
) {

    val layoutResourceItemImportant = R.layout.stats_container_important
    val layoutResourceItem = R.layout.stat_container


    fun checkMatchingRuleSystems(character: Character?, rsList: List<RuleSystem>): Boolean {
        val mRsList = character?.ruleSystems ?: PrefsHelper(mActivity).activeRuleSystems
        return rsList.intersect(mRsList.toSet()).isNotEmpty()
    }

    fun clearViews() {
        statContainer.removeAllViews()
        baseStatContainer.removeAllViews()
    }

    fun getView(stat: StatData, layoutResourceId: Int): LinearLayout {
        val inflater = LayoutInflater.from(mActivity)
        val container = inflater.inflate(layoutResourceId, null, false) as LinearLayout
        LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        ).apply {
            container.layoutParams = this
        }
        val nameLabel = container.findViewById<TextView>(R.id.name_label)
        nameLabel.layoutParams =
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                stat.weightName
            )
        val nameDisplay = container.findViewById<TextView>(R.id.name_display)
        nameDisplay.layoutParams =
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                stat.weightValue
            )
        nameLabel.setText(stat.nameId)
        nameDisplay.text = stat.getValue()
        val mOnClickListener = stat.onClick ?: OnClickListener {
            if (stat.hint.isNotBlank()) {

                SnackBarUtils.showSnackBar(
                    view = coordinatorLayout,
                    msg = "${mActivity?.resources?.getString(stat.nameId)}: ${stat.hint}",
                    duration = stat.showTime,
                    context = mActivity)

            }
        }
        container.setOnClickListener(mOnClickListener)
        container.setOnLongClickListener(stat.onLongClick)


        return container
    }

    private fun getStatCategoryExpandRef(catName: String) = "stats:$catName"

    private fun getHeader(name: String, character: Character?): View {
        val inflater = LayoutInflater.from(mActivity)
        val v = inflater.inflate(R.layout.buff_category, null, false) as LinearLayout
        v.apply {
            layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
            ).apply {
                setMargins(0, 10, 0, 0)
            }
            findViewById<TextView>(R.id.text)?.apply {
                text = Helper.capitalizeNewsLetterStyle(name)
                textSize = 17f
            }
            findViewById<ImageView>(R.id.expand_button)?.apply {
                character?.let {c ->
                    mActivity?.let {a ->
                        Helper.implementExpand(
                            expandButton = this,
                            character = c,
                            context = a,
                            key = getStatCategoryExpandRef(name)
                        ) {
                            inflate(character = character)
                        }
                    }
                }
            }

        }
        return v
    }

    fun showHeader(stats: List<StatData>, character: Character): Boolean {
        return getEffectiveCount(stats = stats, character = character) > 1
    }

    /**
     * tell how may stats are actually being shown with given rulesystems
     */
    fun getEffectiveCount(stats: List<StatData>, character: Character): Int {
        var count = 0
        stats.forEach { stat ->
            if (checkMatchingRuleSystems(character, stat.rulesSystemsUsedIn)) {
                if (stat.condition) {
                    count++
                }
            }
        }
        return count
    }

    fun sortSmallToStart(data: HashMap<String, List<StatData>>, c: Character): Map<String, List<StatData>> {
        return data.toList().let { list ->
            list.sortedBy {
                getEffectiveCount(it.second, c).let { count ->
                    if (count <= 1) {
                        -1
                    } else {
                        list.indexOf(it)
                    }
                }
            }
        }.toMap()
    }

    fun inflate(stats: List<StatData>,
                character: Character,
                container: LinearLayout,
                layoutResourceId: Int) {
        stats.forEach { stat ->
            if (checkMatchingRuleSystems(character, stat.rulesSystemsUsedIn)) {
                if (stat.condition) {
                    container.addView(getView(stat, layoutResourceId))
                }
            }
        }
    }

    fun inflateBaseStats(character: Character?) {
        character?.let { c ->
            inflate(
                stats = getBaseStats(c),
                character = c,
                container = baseStatContainer,
                layoutResourceId = layoutResourceItemImportant
            )
        }
    }

    fun inflate(character: Character?) {
        clearViews()
        character?.let { c ->
            inflateBaseStats(c)
            inflate(
                stats = getUngroupedStats(c, PrefsHelper(mActivity)),
                character = c,
                container = statContainer,
                layoutResourceId = layoutResourceItemImportant)

            sortSmallToStart(data = getStatCategories(c), c = c)
                .let { cats ->
                    cats.keys.forEach { cat ->
                        var layoutResourceItem = layoutResourceItem
                        cats[cat]?.also {

                            /*
                            add header if more than 1 stat
                            for single stat use important item layout
                            like for base items
                             */
                            if (showHeader(it, c)) {
                                statContainer.addView(getHeader(
                                    name = cat,
                                    character = character))
                            } else {
                                layoutResourceItem = layoutResourceItemImportant
                            }

                        }?.let {
                            if (c.getExtend(
                                key = getStatCategoryExpandRef(cat),
                                context = requireNotNull(mActivity)
                            )) {
                                inflate(
                                    stats = it,
                                    character = c,
                                    container = statContainer,
                                    layoutResourceId = layoutResourceItem
                                )
                            }
                        }
                    }
                }
        }
    }

    /**
     * like race, class etc
     */
    fun getBaseStats(character: Character): List<StatData> {
        return listOf(
            StatData(
                nameId = R.string.race_label,
                getValue = { character.race.name },
                rulesSystemsUsedIn = RuleSystem.allRuleSystems(),
                weightValue = 1f,
                onLongClick = {
                    showDialogChangeRace(character)
                    true
                },
                onClick = {
                    displayRaceInWebBrowser(character)
                }
            ),
            StatData(
                nameId = R.string.groesse_label,
                getValue = { getSizeDisplay(character) },
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond),
                weightValue = 1f,
                onLongClick = {
                    mActivity?.let { activity ->
                        DialogCustom.showNumberPicker(
                            context = activity,
                            setNewAmount = {character.size = it},
                            onAmountChanged = { updateCharAndDisplays(character) },
                            getOldAmount = {character.getStatValue(Character.STAT_SIZE) ?: 0}
                        )
                    }
                    true
                },
                hint = "erhöht Lebenspunkte und Geschwindigkeit [+ Größe] und AC [+2*(5 - Größe)]"),
            StatData(
                nameId = R.string.base_stat_moonsign_label,
                getValue = { character.mondzeichen.name },
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond),
                weightValue = 1f,
                onLongClick = {
                    mActivity?.let { a ->
                        DialogCustom.promptString(
                            title = a.getString(R.string.dialog_title_choose_moonsign),
                            onConfirm = {s ->
                                character.mondzeichen = Character.Mondzeichen(name = s)
                                (mActivity as? ActivityCharDetails?)?.onCharStatusChanged(character)
                            },
                            fragmentManager = a.supportFragmentManager
                        )
                    }
                    true
                },
                onClick = {
                    if (character.mondzeichen.description.isNotBlank()) {
                        mActivity?.let { a -> DialogCustom.showAlterDialog(
                            fragmentManager = a.supportFragmentManager,
                            title = character.mondzeichen.name,
                            msg = character.mondzeichen.description) }
                    }
                }
            ),
            StatData(
                nameId = R.string.subrace_label,
                getValue = { character.subrace.name },
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_ogres_club),
                weightValue = 1f,
                onLongClick = {
                    showDialogChangeSubRace(character)
                    true
                },
                onClick = {
                    displaySubRaceInWebBrowser(character)
                }),
            StatData(
                nameId = R.string.archetype_label,
                nameLabel = getArchetypeNameFromClass(character),
                getValue = { character.archetype.name.replace(Character.UNDEFINED, "-") },
                weightValue = 1f,
                rulesSystemsUsedIn = listOf(
                    RuleSystem.rule_system_ogres_club,
                    RuleSystem.rule_system_Dnd_E5,
                    RuleSystem.rule_system_sr6,
                    RuleSystem.rule_system_sr5),
                onLongClick = {
                    showDialogChangeArchetype(character)
                    true
                },
                onClick = {
                    displayArchetypeInWebBrowser(character)
                }
            ),
            StatData(
                nameId = when (PrefsHelper(mActivity).ruleSystem) {
                    RuleSystem.rule_system_splittermond -> R.string.heldengrad_label
                    else -> R.string.class_label
                },
                getValue = {
                    getLevelDisplayString(character) ?:
                    mActivity?.resources?.getString(
                        R.string.class_level,
                        character.dndClass.name.replace(Character.UNDEFINED, ""),
                        character.level)
                    ?: "-"
                },
                weightValue = 1f,
                rulesSystemsUsedIn = RuleSystem.allRuleSystems().minus(
                    listOf(
                        RuleSystem.rule_system_sr6,
                        RuleSystem.rule_system_sr5).toSet()
                ),
                onLongClick = {
                    mActivity?.let {
                        DialogShowFilteredLogs.newInstance(
                            TranslationHelper(it).getAllTranslations(
                                R.string.filter_constraint_class).joinToString(" OR "))
                            .show(
                                it.supportFragmentManager,
                                "showFilteredLogsLevel"
                            )
                        true
                    }
                    false
                },
                onClick = {
                    displayClassInWebBrowser(character)
                }
            )
        )
    }

    fun displayBuffedConForHPOgresClub(character: Character): String {
        var s = ""
        val conBuffsForHp = addBuffs(
            buffRef = BuffEffect.BUFF_REF_CONSTITUTION_FOR_HP_CALC,
            character = character
        )
        if (conBuffsForHp.isNotBlank()) {
            s = " [${character.constitution}$conBuffsForHp] "
        }
        return s
    }

    fun getUngroupedStats(character: Character?, prefs: PrefsHelper): List<StatData> {
        return listOf(
            StatData(
                nameId = if (Helper.rulesSR(character, mActivity)) {
                    R.string.label_total_exp_sr
                } else {
                    R.string.label_total_exp
                },
                getValue = { String.format(Locale.GERMANY, "%,d", character?.totalExp) },
                rulesSystemsUsedIn = RuleSystem.allRuleSystems(),
                condition = prefs.showTotalExp
            ),
            StatData(
                nameId = if (Helper.rulesSR(character, mActivity)) {
                    R.string.label_current_exp_sr
                } else {
                    R.string.label_current_exp
                },
                getValue = { String.format(Locale.GERMANY, "%,d", character?.currentExp) },
                rulesSystemsUsedIn = listOf(
                    RuleSystem.rule_system_ogres_club,
                    RuleSystem.rule_system_skillfull_dnd_5E,
                    RuleSystem.rule_system_splittermond,
                    RuleSystem.rule_system_sr5,
                    RuleSystem.rule_system_sr6),
                condition = prefs.showCurrentExp
            ),
            StatData(
                nameId = R.string.label_current_commit,
                getValue = { character?.currentCommit?.hash_short?: HttpUtils.NO_CURRENT_COMMIT_AVAILABLE_MSG },
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_ogres_club),
                onLongClick =  {
                    character?.let { character ->
                        val dialog = DialogCurrentCommit.newInstance(character.id)
                        mActivity?.supportFragmentManager?.let { fm ->
                            dialog.show(fm, "dialogcurrentcommit")
                            true
                        }
                    }
                    false
                },
            ),
            StatData(
                nameId = R.string.stat_money_label,
                getValue = { String.format(Locale.GERMAN, "%,d¥", character?.money) },
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr5, RuleSystem.rule_system_sr6)
            ),

            )
    }


    fun getStatCategories(character: Character) = LinkedHashMap<String, List<StatData>>().also {

        // --------------------------------- Shadowrun 5 --------------------------------------- //
        it["Limits"] = listOf(
            StatData(
                nameId = R.string.stat_physical_limit_label,
                getValue = {character.physicalLimit.toString()},
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr5)),
            StatData(
                nameId = R.string.stat_mental_limit_label,
                getValue =  { character.mentalLimit.toString() },
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr5)),
            StatData(
                nameId = R.string.stat_social_limit_label,
                getValue = { character.socialLimit.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr5))
        )

        // ------------------------------  ogres club -------------------------------------- //
        it[requireNotNull(mActivity).getString(R.string.stat_label_saves)] = listOf(
            StatData(
                nameId = R.string.stat_reflex_saves,
                hint = ("level (%d) + " +
                        "${mActivity.getString(R.string.class_label).lowercase()} (%d) + " +
                        "${mActivity.getString(R.string.attribute_dexterity).lowercase()} (%d)").format(
                    character.level,
                    character.dndClass.progressions[Character.PROGRESSION_REF_REFLEX_SAVES],
                    getAttributeMod(rule_system_ogres_club,character.dexterity)
                ) + addBuffs(buffRef = Character.PROGRESSION_REF_REFLEX_SAVES, character = character),
                getValue = {
                    character.reflexSave.toString()
                },
                rulesSystemsUsedIn = listOf(rule_system_ogres_club)
            ),
            StatData(
                nameId = R.string.stat_will_saves,
                hint = ("level (%d) + " +
                        "${mActivity.getString(R.string.class_label).lowercase()} (%d) + " +
                        "${mActivity.getString(R.string.attribute_wisdom).lowercase()} (%d)").format(
                    character.level,
                    character.dndClass.progressions[Character.PROGRESSION_REF_WILL_SAVES],
                    getAttributeMod(rule_system_ogres_club, character.wisdom)
                ) + addBuffs(buffRef = Character.PROGRESSION_REF_WILL_SAVES, character = character),
                getValue = {
                    character.willSave.toString()
                },
                rulesSystemsUsedIn = listOf(rule_system_ogres_club)
            ),
            StatData(
                nameId = R.string.stat_fortitude_saves,
                hint = ("level (%d) + " +
                        "${mActivity.getString(R.string.class_label).lowercase()} (%d) + " +
                        "${mActivity.getString(R.string.attribute_constitution).lowercase()} (%d)").format(
                    character.level,
                    character.dndClass.progressions[Character.PROGRESSION_REF_FORT_SAVES],
                    getAttributeMod(rule_system_ogres_club, character.constitution)
                ) + addBuffs(buffRef = Character.PROGRESSION_REF_FORT_SAVES, character = character),
                getValue = {
                    character.fortitudeSave.toString()
                },
                rulesSystemsUsedIn = listOf(rule_system_ogres_club)
            ),
        )
        it["Stats"] = listOf(
            // ------------------------------  ogres club -------------------------------------- //
            StatData(
                nameId = R.string.stat_hp_label,
                hint = "8 + ( level {%s} ) * ( hpr {%d%s}  + con mod {%d%s} )".format(
                    character.level,
                    character.dndClass.hpr,
                    addBuffs(buffRef = BuffEffect.BUFF_REF_HPR, character = character),
                    getAttributeMod(rule_system_ogres_club,
                        character.constitution + character.getStatBuffValueOrZero(BuffEffect.BUFF_REF_CONSTITUTION_FOR_HP_CALC)
                    ),
                    displayBuffedConForHPOgresClub(character)
                ) + "\n\t" + addBuffs(buffRef = BuffEffect.BUFF_REF_HP, character = character).trim(),
                getValue = {
                    character.hp.toString()
                },
                rulesSystemsUsedIn = listOf(rule_system_ogres_club)
            ),
            StatData(
                nameId = R.string.stat_negative_hp,
                hint = "${mActivity.getString(R.string.attribute_constitution).lowercase()} (%d)".format(
                    character.constitution
                ) + addBuffs(buffRef = BuffEffect.BUFF_REF_NEGATIVE_HP, character = character),
                getValue = {
                    character.maxOverFlow.toString()
                },
                rulesSystemsUsedIn = listOf(rule_system_ogres_club)
            ),
            StatData(
                nameId = R.string.stat_initiative_label,
                hint = "%s (%d)".format(
                    mActivity.getString(R.string.attribute_dexterity).lowercase(),
                    getAttributeMod(
                        ruleSystem = character.ruleSystem, statValue = character.dexterity
                    )
                ) + addBuffs(buffRef = BuffEffect.BUFF_REF_INITIATIVE, character = character),
                getValue = {
                    character.initiative.toString()
                },
                rulesSystemsUsedIn = listOf(rule_system_ogres_club)
            ),
            StatData(
                nameId = R.string.stat_damage_reduction_label,
                hint = "%s (%d)".format(
                    mActivity.getString(R.string.label_level).lowercase(),
                    character.level
                ) + addBuffs(buffRef = STAT_DAMAGE_REDUCTION, character = character),
                getValue = {
                    character.damageReduction.toString()
                },
                rulesSystemsUsedIn = listOf(rule_system_ogres_club)
            ),

            // ------------------------------  skillful 5e -------------------------------------- //
            StatData(
                nameId = R.string.stat_hp_label,
                hint = "(4 + CON-Mod) x (Level + 2)",
                getValue = { character.ruleSystems?.first()?.
                let { it1 -> character.hp }.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_skillfull_dnd_5E)),
            StatData(
                nameId = R.string.stat_cast_stat_label,
                getValue = { Helper.capitalize(character.CAST_STAT).ifBlank { "None" } } ,
                hint = "some characters can use this stat for their spell casting checks if they have learned the feature",
                condition = character.showCastStat,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_skillfull_dnd_5E),
                onLongClick = OnLongClickListener {
                    showChooseTraditionAttribute(
                        mActivity = mActivity,
                        character = character,
                        msg = "Change Cast Stat")
                    return@OnLongClickListener true
                } ),
            StatData(
                nameId = R.string.stat_magic_points_label,
                hint = "(2 + 1/magic feature) * level",
                getValue = { character.magic_points.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_skillfull_dnd_5E)),
            StatData(
                nameId = R.string.stat_armor_class_label,
                hint = "10 + %d (%s) + %d (%s".format(
                    character.dexterity,
                    mActivity.getString(R.string.attribute_dexterity).lowercase(),
                    character.armor, mActivity.getString(R.string.label_armor)).lowercase(),
                getValue = { character.magic_points.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_skillfull_dnd_5E)),

            // ----------------------------  splittermond --------------------------------------- //
            StatData(
                nameId = R.string.stat_lp_label,
                hint = "${character.size} (${mActivity.getString(R.string.size).lowercase()}) + " +
                        "${character.konstitution} (konstitution)" +
                        addBuffs(buffRef = BuffEffect.BUFF_REF_HP, character = character),
                getValue = { (character.hp / character.splittermondHpMultiplier) .toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond)),
            StatData(
                nameId = R.string.stat_initiative_label,
                getValue = { character.initiative.toString() },
                hint = "10 - ${character.intuition} (intuition)"  +
                        addBuffs(buffRef = BuffEffect.BUFF_REF_INITIATIVE, character = character),
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond)),
            StatData(
                nameId = R.string.stat_geschwindigkeit_label,
                hint = "${character.size} (größe) + ${character.beweglichkeit} (beweglichkeit)" +
                        addBuffs(buffRef = BuffEffect.BUFF_REF_SPEED, character = character),
                getValue = { character.speed.toString() },
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond),
            ),
            StatData(
                nameId = R.string.stat_fokus_label,
                hint = "${2 * PrefsHelper(mActivity).fokusPercentage / 100} x " +
                        "(${character.mystik} (mystik) + ${character.willenskraft} (willenskraft))" +
                        addBuffs(buffRef = BuffEffect.BUFF_REF_FOKUS, character = character),
                getValue = { character.fokusPunkte.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond)),
            StatData(
                nameId = R.string.stat_verteidigung_label,
                hint = "10 + (2 x ${character.heldengrad} (hg)) + ${character.beweglichkeit} (beweglichkeit) " +
                        "+  ${character.stärke} (stärke) + 2 x (5 - ${character.size} (größe))" +
                        addBuffs(buffRef = BUFF_REF_VERTEIDIGUNG, character = character),
                getValue = { character.verteidigung.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond)),
            StatData(
                nameId = R.string.stat_damage_reduction_label,
                hint = "Armor + Buffs",
                getValue = { character.damageReduction.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond)),
            StatData(
                nameId = R.string.stat_tik_malus_label,
                hint = "Armor + Debuffs",
                getValue = { if (character.tik_malus != 0) "-${character.tik_malus}" else "0"} ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond)),
            StatData(
                nameId = R.string.stat_geistiger_widerstand_label,
                hint = "10 + (2 x ${character.heldengrad} (hg)) + ${character.verstand} (verstand) + ${character.willenskraft} (willenskraft)" +
                        addBuffs(buffRef = BuffEffect.BUFF_REF_GEISTIGER_WIDERSTAND, character = character),
                getValue = { character.geistigerWiderstand.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond)),
            StatData(
                nameId = R.string.stat_körperlicher_widerstand_label,
                hint = "10 + (2 x ${character.heldengrad} (hg)) + ${character.konstitution} (konstitution) + ${character.willenskraft} (willenskraft)" +
                        addBuffs(buffRef = BuffEffect.BUFF_REF_KOERPERLICHER_WIDERSTAND, character = character),
                getValue = { character.koerperlicherWiderstand.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_splittermond)),
        )

        // ------------------------------- Shadowrun 6 ------------------------------------------//
        it[requireNotNull(mActivity).getString(R.string.label_magic)] = listOf(
            StatData(
                nameId = R.string.stat_drain_resistance_label,
                getValue = { character.drain_resisitance.toString() } ,
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_willpower),
                    character.willpower,
                    mActivity.getString(R.string.stat_tradition_attribute_label),
                    character.tradition_attribute
                )+  addBuffs(buffRef = BuffEffect.BUFF_REF_DRAIN_RESISTANCE, character = character),
                condition = character.is_awakened_caster || character.is_technomancer,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5)
            ),
            StatData(
                nameId = R.string.stat_tradition_attribute_label,
                getValue = { character.traditionAttributeDisplay } ,
                hint = "Hermitian -> Logic, Shaman -> Charisma, long press -> define custom",
                condition = character.is_awakened_caster,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5),
                onLongClick = OnLongClickListener {
                    showChooseTraditionAttribute(mActivity = mActivity, character = character)
                    return@OnLongClickListener true
                }
            ),
            StatData(
                nameId = R.string.stat_label_concentration_Malus,
                getValue = {
                    val cm = character.concentrationMalus
                    if (cm > 0) "-%d".format(cm) else "0"
                } ,
                hint = mActivity.getString(R.string.stat_hint_concentration_malus) ?: "",
                condition = character.is_awakened_caster || character.is_technomancer,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5)
            )
        )
        it[mActivity.getString(R.string.splittermond_skill_category_combat)] = listOf(
            StatData(
                nameId = R.string.stat_defense_roll_label,
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_intuition),
                    character.intuition,
                    mActivity.getString(R.string.attribute_reaction),
                    character.reaction
                ) +  addBuffs(buffRef = BuffEffect.BUFF_REF_DEFENSE_ROLL, character = character) + addDamageMalusIfNotZero(character.damageMalus),
                getValue = { character.defense_roll.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6)),
            StatData(
                nameId = R.string.stat_initiative_label,
                getValue = { "%d + %dd6".format(character.initiative, character.ini_dice) },
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_intuition),
                    character.intuition,
                    mActivity.getString(R.string.attribute_reaction),
                    character.reaction
                )+  addBuffs(buffRef = BuffEffect.BUFF_REF_INITIATIVE, character = character),
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5)),
            StatData(
                nameId = R.string.stat_minor_actions_label,
                getValue = { character.minorActions.toString() },
                hint = "1 + %s (%d)".format(
                    mActivity.getString(R.string.stat_label_ini_dice),
                    character.ini_dice,
                )+  addBuffs(buffRef = BuffEffect.BUFF_REF_INITIATIVE, character = character),
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5)),
            StatData(
                nameId = R.string.stat_matrix_initiative_label,
                getValue = { "%d + %dd6".format(character.matrix_initiative, character.matrix_ini_dice) },
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_intuition),
                    character.intuition,
                    mActivity.getString(R.string.stat_data_processing_label),
                    character.data_processing
                ) +  addBuffs(buffRef = BuffEffect.BUFF_REF_MATRIX_INITIATIVE, character = character) +  " + [2 (Cold)/ 3 (HOT)"  +
                        addBuffs(buffRef = BuffEffect.BUFF_REF_MATRIX_INI_DICE, character = character) + "]d6",
                condition = character.hasArchetypes(listOf("Decker", "Technomancer")) || character.is_technomancer,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5)),
            StatData(
                nameId = R.string.stat_astral_initiative_label,
                getValue = { "%d + %dd6".format(character.astral_initiative, character.astral_ini_dice) },
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_intuition),
                    character.intuition,
                    mActivity.getString(R.string.attribute_logic),
                    character.logic
                ),
                condition = character.is_awakened_caster,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5))
        )
        it[mActivity.getString(R.string.label_matrix_attributes)] = listOf(
            StatData(
                nameId = R.string.stat_data_processing_label,
                getValue = { "%d".format(character.data_processing) },
                condition = character.data_processing > 0,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5)),
            StatData(
                nameId = R.string.stat_firewall_label,
                getValue = { "%d".format(character.firewall) },
                condition = character.firewall > 0,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5)),
            StatData(
                nameId = R.string.stat_sleaze_label,
                getValue = { "%d".format(character.sleaze) },
                condition = character.sleaze > 0,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5)),
            StatData(
                nameId = R.string.stat_matrix_attack_label,
                getValue = { "%d".format(character.matrix_attack) },
                hint = "Deck needed",
                condition = character.matrix_attack > 0,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6, RuleSystem.rule_system_sr5))
        )
        it[mActivity.getString(R.string.label_edge_ratings)] = listOf(
            StatData(
                nameId = R.string.stat_attack_value_label,
                getValue = { character.unarmed_attack_rating.toString() },
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_reaction),
                    character.reaction,
                    mActivity.getString(R.string.attribute_strength),
                    character.strength
                ) +  addBuffs(buffRef = BuffEffect.BUFF_REF_UNARMED_ATTACK_RATING, character = character),
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6)),
            StatData(
                nameId = R.string.stat_matrix_attack_rating_label,
                getValue = { character.matrix_attack_rating.toString() },
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.stat_matrix_attack_label),
                    character.matrix_attack,
                    mActivity.getString(R.string.stat_sleaze_label),
                    character.sleaze
                ) +  addBuffs(buffRef = BuffEffect.BUFF_REF_MATRIX_ATTACK_RATING, character = character),
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6),
                condition = (character.matrix_attack_rating) > 0),
            StatData(
                nameId = R.string.stat_magic_attack_rating_label,
                getValue = { character.magic_attack.toString() } ,
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_magic),
                    character.magic,
                    mActivity.getString(R.string.stat_tradition_attribute_label),
                    character.tradition_attribute
                ),
                condition =  character.is_awakened_caster,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6)),
            StatData(
                nameId = R.string.stat_defense_rating_label,
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_body),
                    character.body,
                    mActivity.getString(R.string.label_armor),
                    character.armor
                ) +  addBuffs(buffRef = BuffEffect.BUFF_REF_DEFENSE_RATING, character = character),
                getValue = { character.defense_rating.toString() } ,
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6)),
            StatData(
                nameId = R.string.stat_matrix_defense_rating_label,
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.stat_data_processing_label),
                    character.data_processing,
                    mActivity.getString(R.string.stat_firewall_label),
                    character.firewall
                ) +  addBuffs(buffRef = BuffEffect.BUFF_REF_MATRIX_DEFENSE_RATING, character = character),
                getValue = { character.matrix_defense_rating.toString() },
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6),
                condition = character.matrix_defense_rating > 0),
            StatData(
                nameId = R.string.stat_social_rating_label,
                getValue = { character.social_rating.toString() },
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_charisma),
                    character.charisma,
                    mActivity.getString(R.string.stat_label_armor_social_rating),
                    character.getStatBuffValueOrZero(BuffEffect.BUFF_REF_SOCIAL_RATING)
                ),
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6)),
        )
        it[mActivity.getString(R.string.attribute_rolls)] = listOf(
            StatData(
                nameId = R.string.stat_composure_label,
                getValue = { character.composure.toString() },
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_willpower),
                    character.willpower,
                    mActivity.getString(R.string.attribute_charisma),
                    character.charisma
                ) +  addBuffs(buffRef = BuffEffect.BUFF_REF_COMPOSURE, character = character),
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6)),
            StatData(
                nameId = R.string.stat_memory_label,
                getValue = { character.memory.toString() },
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_logic),
                    character.logic,
                    mActivity.getString(R.string.attribute_intuition),
                    character.intuition
                ) +  addBuffs(buffRef = BuffEffect.BUFF_REF_MEMORY, character = character),
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6)),
            StatData(
                nameId = R.string.stat_judge_intentions_label,
                getValue = { character.judge_intention.toString() },
                hint = "%s (%d) + %s (%d)".format(
                    mActivity.getString(R.string.attribute_willpower),
                    character.willpower,
                    mActivity.getString(R.string.attribute_intuition),
                    character.intuition
                ) +  addBuffs(buffRef = BuffEffect.BUFF_REF_JUDGE_INTENTIONS, character = character),
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6)),
            StatData(
                nameId = R.string.stat_lift_carry_label,
                getValue = { "${character.max_lift_weight}/${character.max_overhead_weight}kg (${character.lift_carry})" } ,
                hint = mActivity.getString(R.string.stat_hint_carry_overhead,
                    mActivity.getString(R.string.attribute_body),
                    character.body,
                    mActivity.getString(R.string.attribute_willpower),
                    character.willpower,
                    character.lift_carry
                    ) ?: "",
                rulesSystemsUsedIn = listOf(RuleSystem.rule_system_sr6),
                showTime = Snackbar.LENGTH_INDEFINITE)
        )
    }

    fun addBuffs(buffRef: String, character: Character): String {
        return character.buffs.
        flatMap { b -> b.effects.map { e -> Pair(b, e)  } }.
        filter { it.second.stat.contentEquals(buffRef, ignoreCase = true) }.joinToString(
            separator = " + "
        ) {
            "%s (%d)".format(
                it.first.name,
                it.second.value
            )
        }.let {
            if (it.isNotBlank()) " + $it" else ""
        }
    }

    fun addDamageMalusIfNotZero(value: Int): String {
        return if (value != 0) " - Damage Malus ($value)" else ""
    }

    fun updateCharAndDisplays(character: Character) {
        DatabaseHelper.getInstance(mActivity)?.updateCharacter(character = character, keep_last_update_date = false)
        try {
            (mActivity as ActivityCharDetails).updateDisplays(generateCPI = false)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("CLASS_CAST_ERROR","Activity cant be cast to ActivityCharDetails")
        }
    }

    fun showChooseTraditionAttribute(mActivity: FragmentActivity?, character: Character?, msg: String = "Change Tradition Attribute:") {
        val dialog = DialogCustom<String>()
        dialog.setDefaultInput()
        dialog.setTitle(msg)
        try {
            mActivity as ProgressBarFrameDropBoxActivity?
            dialog.setEditText2Adapter(
                mActivity?.let {
                    ArrayAdapter(
                        it,
                        R.layout.auto_complete_entry,
                        it.charProgressItemsMap[R.string.filter_constraint_attribute]?.mainItem?.adapter?.createAutoCompleteStrings() ?: listOf()
                    )
                },
                alwaysShow = true)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("CLASS_CAST_ERROR","Activity cant be cast to ActivityCharDetails")

        }

        dialog.setPositiveButton("OK"
        ) {
            character?.let {
                it.CAST_STAT = dialog.text2.text.toString().trim()
                updateCharAndDisplays(it)
                dialog.dismiss()

            }

        }
        dialog.setNegativeButton("Delete") {
            character?.let {
                it.CAST_STAT = ""
                updateCharAndDisplays(it)
                dialog.dismiss()
            }
        }
        mActivity?.supportFragmentManager?.let { fm ->
            dialog.show(fm, "custom_dialog_prompt_filename")}
    }

    private fun getArchetypeNameFromClass(character: Character): String? {
        return if (PrefsHelper(mActivity).ruleSystem == RuleSystem.rule_system_Dnd_E5) {
            /*
            we won't get the archetype name here, because in DataBaseHelper.getCharacters
            we don't load data from resources, only from db
            and this is only correct for ogresclub characters
             */
//            log("ARCHETYPE", "dnd classes archetype name of character: ${character?.dndClass?.archetype_name}")

            Global.classesFromResources.find { cls ->
                cls.name.equals(
                    character.dndClass.name,
                    ignoreCase = true
                )
            }?.archetype_name

        } else null
    }

    fun getLevelDisplayString(character: Character): String? {
        //set level  to display
        (character.charProgressItemsMap[R.string.filter_constraint_class]?.mainItem?.adapter as? CharProgressItemAdapterClasses).let { adapter ->

            when (character.ruleSystem ?: PrefsHelper(mActivity).ruleSystem) {
                RuleSystem.rule_system_splittermond -> return character.heldengrad.toString()
                else -> adapter?.getClassesAndLevels()?.let { clsNameNLvls ->
                    if (clsNameNLvls.isNotEmpty()) {
                        CoroutineScope(Dispatchers.IO).launch {
                            updateCharacterClass(character = character, clsNameNLvls = clsNameNLvls)
                        }
                        return adapter.getDisplayString(clsNameNLvls)
                    }
                }
            }


        }
        return null
    }

    /**
     * update character class in db,
     * but only, when there is only one class.
     * if there is multi classing character class is set to undefined
     */
    private suspend fun updateCharacterClass(character: Character, clsNameNLvls: List<ValueNamePair>) {
        val DEBUG_TAG = "UPDATE_CHARACTER_CLASS_FROM_LOGS"
        val dbHelper = DatabaseHelper.getInstance(mActivity)
        withContext(Dispatchers.IO) {

            val single = clsNameNLvls.singleOrNull()
            if (single != null) {
                dbHelper?.getClass(single.name)?.let { newClass ->
                    if (newClass != character.dndClass) {
                        Helper.log(
                            DEBUG_TAG,
                            "setting character class in db to new value found in logs: '$newClass'"
                        )
                        character.dndClass = newClass
                        dbHelper.updateCharacter(character, keep_last_update_date = true)
                        Helper.update(Global.chars, character)
                    }
                }
            }
            if (clsNameNLvls.isNotEmpty() && clsNameNLvls.size > 1) {
                // multi classing
                Helper.log(DEBUG_TAG, "multiclassing: setting character class in db to 'undefined'")
                val newClass = DndClass(name = Character.UNDEFINED)
                if (newClass != character.dndClass) {
                    character.dndClass = newClass
                    dbHelper?.updateCharacter(character, keep_last_update_date = true)
                    Helper.update(Global.chars, character)
                }

            }

        }


    }

    private fun displaySubRaceInWebBrowser(character: Character) {
        character.subrace.name.let { subName ->
            if (subName.isNotBlank()) {
                character.race.link.let { link ->
                    val raceName = character.race.name
                    if (link.isBlank()) {
                        toast(msg = "no link found for subrace '$subName' of race '${raceName}'. Maybe crawl in settings.")
                    } else {
                        val subLink = "$link#$subName"
                        Helper.log(
                            "URL",
                            "start opening link for subrace '$subName' of race '${raceName}' " +
                                    "in browser with url = '$subLink'"
                        )
                        mActivity?.let {
                            HttpUtils.openWebPage(subLink, it)
                        }
                    }
                }
            }
        }
    }

    private fun displayRaceInWebBrowser(character: Character) {
        val prefs = PrefsHelper(mActivity)
        when {
            prefs.rulesOgresClub -> {
                character.race.name.let { raceName ->
                    if (raceName.isNotBlank()) {
                        character.race.link.let { link ->
                            openLink(
                                link = link,
                                what = "race '$raceName'",
                                hint = "Maybe crawl in settings")
                        }
                    }
                }
            }
            prefs.rulesE5 -> {
                Global.racesFromResources.
                find { r -> r.name.equals(character.race.name, ignoreCase = true) }?.let {
                    openLink(
                        link = it.link,
                        what = "race '${it.name}'"
                    )
                }
            }
        }
    }

    private fun displayArchetypeInWebBrowser(character: Character) {
        val cls = character.dndClass
        val prefs = PrefsHelper(mActivity)
        when {
            prefs.rulesOgresClub -> {
                character.archetype.let {

                    openLink(
                        link = "${cls.link}#${it.name}",
                        what = "archetype '${it.name}' of class '${cls.name}'",
                        hint = "Maybe crawl in settings"
                    )
                }
            }
            prefs.rulesE5 -> {
                Global.archetypesFromResources.
                find { a -> a.name.equals(character.archetype.name, ignoreCase = true) }?.
                let {
                    openLink(
                        link = it.link,
                        what = "archetype '${it.name}' of class '${cls}'"
                    )
                }
            }
        }
    }

    private fun displayClassInWebBrowser(character: Character) {
        when(PrefsHelper(mActivity).ruleSystem) {
            RuleSystem.rule_system_ogres_club -> {
                character.dndClass.let {
                    openLink(
                        link = it.link,
                        what = "class '${it.name}'",
                        hint = "Maybe crawl in settings."
                    )
                }
            }
            RuleSystem.rule_system_Dnd_E5 -> {
                Global.classesFromResources.
                find { cls -> cls.name.equals(character?.dndClass?.name, ignoreCase = true) }?.
                let {
                    openLink(
                        link = it.link,
                        what = "class '${it.name}'"
                    )
                }
            }
        }
    }

    private fun showDialogChangeArchetype(character: Character) {
        val dialog = DialogCustom<DndArchetype>()
        dialog.setInput(false, "", true, "enter archetype name")
        dialog.setTitle("   Change Archetype   ")
        val archetypes_to_auto_complete = ArrayList<DndArchetype>()
        character.let { character ->
            Helper.addAllAccordingToRuleSystem(
                ruleSystems = character.ruleSystems ?: PrefsHelper(mActivity).activeRuleSystems,
                l = ArrayList<DndArchetype>(),
                data = HashMap<RuleSystem, List<DndArchetype>>().apply {
                    val srArchetypes = mActivity?.resources?.getStringArray(R.array.archetypes_shadworun)
                        ?.map { DndArchetype(name = it) } ?: listOf()

                    DatabaseHelper.getInstance(mActivity)?.getArchetypeNamesRelatedToClass(character.dndClass.name)
                        ?.map { DndArchetype(name = it) }
                        ?.let { it1 -> put(RuleSystem.rule_system_ogres_club, it1) }
                    put(RuleSystem.rule_system_sr5, srArchetypes)
                    put(RuleSystem.rule_system_sr6, srArchetypes)
                }
            ).forEach { archetype ->
                archetypes_to_auto_complete.add(archetype)
            }
        }
        mActivity?.let {
            dialog.setEditText2Adapter(
                ArrayAdapter<DndArchetype>(
                    it,
                    R.layout.auto_complete_entry,
                    archetypes_to_auto_complete),
                true
            )
        }

        // For SR use comma tokenizer, because you can have multiple archetypes
        if (Helper.rulesSR(character, mActivity)) dialog.setEditText2Tokenizer(
            MultiAutoCompleteTextView.CommaTokenizer())

        //use normal autocomplete for sr because you can have multiple archetypes
        if (Helper.rulesDnD(character = character, context = mActivity)) {
            dialog.setEditText2_OnItemClickListener { parent, view, position, id ->
                val newArchetypeName = parent.getItemAtPosition(position).toString().trim()
                if (newArchetypeName.isEmpty()) {
                    Toast.makeText(mActivity, "This is no name for a archetype!", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    dialog.dismiss()
                    changeArchetype(newArchetypeName = newArchetypeName, character = character)
                }
            }
        }
        dialog.setPositiveButton("Ok") {
            val newArchetypeName = dialog.text2.text.toString().trim().removeSuffix(",")
            if (newArchetypeName.isEmpty()) {
                Toast.makeText(mActivity, "This is no name for a archetype!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                dialog.dismiss()
                changeArchetype(newArchetypeName = newArchetypeName, character = character)
            }
        }
        dialog.setNegativeButton("Cancel") { dialog.dismiss() }
        mActivity?.supportFragmentManager?.let { fragmentManager -> dialog.show(fragmentManager, "DialogChangeArchetype") }
    }

    private fun showDialogChangeSubRace(character: Character) {
        val dialog = DialogCustom<Subrace>()
        dialog.setInput(false, "", true, "enter subrace name")
        dialog.setTitle("   Change Subrace   ")
        //set autocomplete according to race. no autocomplete if no race
        val subraces_to_auto_complete = ArrayList<Subrace>()
        character.let { character ->
            DatabaseHelper.getInstance(mActivity)?.getSubracesRelatedToRace(character.race.name)
                ?.forEach { subrace_name ->
                    subraces_to_auto_complete.add(Subrace(subrace_name))
                }
        }
        mActivity?.let {
            dialog.setEditText2Adapter(
                ArrayAdapter(
                    it,
                    R.layout.auto_complete_entry,
                    subraces_to_auto_complete
                ),
                true)
        }
        dialog.setEditText2_OnItemClickListener { parent, view, position, id ->
            val newSubraceName = parent.getItemAtPosition(position).toString().trim()
            if (newSubraceName.isEmpty()) {
                Toast.makeText(
                    mActivity,
                    "This is no name for a subrace!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                dialog.dismiss()
                changeSubRace(newSubraceName, character)
            }
        }
        dialog.setPositiveButton("Ok", View.OnClickListener {
            val newSubraceName = dialog.text2.text.toString().trim()
            if (newSubraceName.isEmpty()) {
                Toast.makeText(mActivity, "This is no name for a subrace!", Toast.LENGTH_SHORT).show()
            } else { //                            Helper.log("CHANGE_SUBRACE", "new subrace name = " + new_subrace_name);
                dialog.dismiss()
                changeSubRace(newSubraceName, character)
            }
        })
        dialog.setNegativeButton("Cancel", View.OnClickListener { dialog.dismiss() })
        mActivity?.supportFragmentManager?.let { fragmentManager ->  dialog.show(fragmentManager, "DialogChangeSubrace")}
    }

    private fun showDialogChangeRace(character: Character) {
        val dialog: DialogCustom<Race> = DialogCustom()
        dialog.setInput(false, "", true, "enter race name")
        dialog.setTitle("    Change Race    ")
        dialog.setEditText2Adapter(mActivity?.let { it1 -> getRaceAdapter(it1) }, true)
        dialog.setEditText2_OnItemClickListener { parent, view, position, id ->
            val newRaceName = parent.getItemAtPosition(position).toString().trim { it <= ' ' }
            if (newRaceName.isEmpty()) {
                Toast.makeText(mActivity, "This is no name for a race!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                dialog.dismiss()
                changeRace(
                    newRaceName = newRaceName,
                    character = character)

            }
        }
        //                Helper.log("CHANGE RACE", "Autocomplete list = "+ Global.racesFromOgresClub.size() + " :" + Global.racesFromOgresClub.toString());
        dialog.setPositiveButton("Ok") {
            val new_race_name = dialog.text2.text.toString().trim()
            if (new_race_name.isEmpty()) {
                Toast.makeText(
                    mActivity,
                    "This is no name for a race!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                dialog.dismiss()
                changeRace(
                    newRaceName = new_race_name,
                    character = character
                )
            }
        }
        dialog.setNegativeButton("Cancel") { dialog.dismiss() }
        mActivity?.supportFragmentManager?.let { fragmentManager -> dialog.show(fragmentManager, "DialogChangeRace") }
    }

    private fun getRaceAdapter(context: Context): ArrayAdapter<Race> {
        return ArrayAdapter(
            context,
            R.layout.auto_complete_entry,
            Helper.addAllToList(
                context = context,
                l = ArrayList(),
                addForOgreClub = Global.racesFromOgresClub,
                addForE5 = Global.racesFromResources
            )
        )
    }

    private fun changeRace(newRaceName: String, character: Character?) {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                //check if this race name is in database
                val newRace = DatabaseHelper.getInstance(mActivity)?.getRace(newRaceName) ?: Race(newRaceName)
                character?.race = newRace

                withContext(Dispatchers.Main) {
                    character?.let { character ->
                        (mActivity as? ActivityCharDetails?)?.onCharStatusChanged(character)
                    }
                }
            }
        }
    }

    private fun changeSubRace(newSubRaceName: String, character: Character?) {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                //check if this race name is in database
                val newSubrace = DatabaseHelper.getInstance(mActivity)?.getSubrace(newSubRaceName) ?: Subrace(newSubRaceName)
                character?.subrace = newSubrace

                withContext(Dispatchers.Main) {
                    character?.let { character ->
                        (mActivity as? ActivityCharDetails?)?.onCharStatusChanged(character)
                    }

                }

            }
        }
    }

    private fun changeArchetype(newArchetypeName: String, character: Character?) {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                //check if this race name is in database
                val newArchetype = DatabaseHelper.getInstance(mActivity)?.getArchetype(newArchetypeName) ?: DndArchetype(name = newArchetypeName)
                character?.archetype = newArchetype

                withContext(Dispatchers.Main) {
                    character?.let { character ->
                        (mActivity as? ActivityCharDetails?)?.onCharStatusChanged(character)
                    }

                }

            }
        }
    }

    private fun openLink(link: String, what: String, hint: String = "") {
        if (link.isBlank()) {
            var msg = "no link found for $what."
            if (hint.isNotBlank()) msg += " $hint."
            toast(msg =  msg)
        } else {
            Helper.log(
                "URL",
                "start opening link for $what" +
                        "in browser with url = '$link'"
            )
            mActivity?.let {
                HttpUtils.openWebPage(link, it)
            }
        }

    }

    private fun getSizeDisplay(character: Character): String {
        val baseSze = character.getStatValue(Character.STAT_SIZE) ?: 0
        return if (baseSze == character.size) {
           baseSze.toString()
        } else {
            "%s (%s)".format(
                baseSze,
                character.size
            )
        }
    }

    private fun toast(msg: String) {
        Toast.makeText(mActivity, msg, Toast.LENGTH_LONG).show()
    }

    data class StatData(
        val nameId: Int,
        /**
         * use this if name is not given by a string resource
         */
        val nameLabel: String? = null,
        val getValue: () -> String,
        /**
         * will be displayed in click, e.g. to sho how value is calculated
         */
        val hint: String = "",
        val condition: Boolean = true,
        val rulesSystemsUsedIn: List<RuleSystem>,
        val showTime: Int = Snackbar.LENGTH_SHORT,
        val onClick: OnClickListener? = null,
        val onLongClick: OnLongClickListener? = null,
        /**
         * the weight of the Textview
         * use to prevent line break if name or value is especially long
         */
        val weightName: Float = 1f,
        /**
         * the weight of the Textview
         * use to prevent line break if name or value is especially long
         */
        val weightValue: Float = 2f
    )
}