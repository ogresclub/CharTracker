package com.mtths.chartracker.adapters

import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.mtths.chartracker.utils.Helper.setColor
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by mtths on 12.04.17.
 */
class AutoCompleteTextToAddAdapter(context: Context?, layoutResource: Int, data: ArrayList<String>) : ArrayAdapter<String?>(context!!, layoutResource) {
    private var complete_data = ArrayList<String>()
    private var suggestions = ArrayList<String>()
    private val stringsToHighlight: ArrayList<String>? = ArrayList()
    var layout_resource: Int

    constructor(context: Context?, layout_resource: Int) : this(context, layout_resource, ArrayList<String>()) {}

    ////////////////////////////////////// METHODS ////////////////////////////////////////////////
    override fun getItem(position: Int): String? {
        return suggestions[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return suggestions.size
    }

    override fun getFilter(): Filter {
        return mFilter
    }

    override fun clear() {
        complete_data.clear()
        suggestions.clear()
    }

    fun setData(data: ArrayList<String>) {
        complete_data = data
        suggestions = data
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView != null) {
            holder = convertView.tag as ViewHolder
        } else {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(layout_resource, parent, false)
            holder = ViewHolder()
            holder.text = convertView as TextView?
            convertView.tag = holder
        }
        val tag = suggestions[position]
        val sbb = SpannableStringBuilder(tag)
        var sb: Spannable = sbb
        if (stringsToHighlight != null) {
            sb = setColor(stringsToHighlight, tag, Color.BLUE, sbb)
        }
        holder.text!!.text = sb
        return convertView!!
    }

    private class ViewHolder {
        var text: TextView? = null
    }

    var DEBUG_TAG_FILTERING_AUTO_COMPLETE = "FILTER_AUTO_COMPLETE"
    fun setStringsToHighlight(highlights: Array<String>) {
        stringsToHighlight!!.clear()
        for (h in highlights) {
            if (h.trim().isNotEmpty()) {
                stringsToHighlight.add(h.trim())
            }
        }
    }

    var mFilter: Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
//            Helper.log(DEBUG_TAG_FILTERING_AUTO_COMPLETE, "START filtering for constraint: $constraint");
            val filterResults = FilterResults()
            val result_values = ArrayList<String>()

            constraint?.let {
                setStringsToHighlight(constraint.toString().split(" ").toTypedArray())
                //                Pattern pattern = Pattern.compile("(?i)^" + constraint + ".*");
                val constraintStrings = constraint.toString().lowercase(Locale.getDefault()).split(" ").toTypedArray()
                val constraintLength = constraint.length
                for (s in complete_data) {
                    val sLower = s.lowercase(Locale.getDefault())
                    var matchesConstraint = s.length > constraintLength
                    if (matchesConstraint) {
                        for (cs in constraintStrings) {
                            if (!sLower.contains(cs.trim())) {
                                matchesConstraint = false
                                break
                            }
                        }
                    }
                    if ( //                            && pattern.matcher(s).matches()
                            matchesConstraint) {
                        result_values.add(s)
//                        log(DEBUG_TAG_FILTERING_AUTO_COMPLETE, "'" + s + "'" + " MATCHES ");
                    }
                }
            }
            result_values.sortWith(tagsAtTheEndComparator)
            filterResults.values = result_values
            filterResults.count = result_values.size
            //            Helper.log(DEBUG_TAG_FILTERING_AUTO_COMPLETE, "END filtering");
            return filterResults
        }



        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {

//            Helper.log(DEBUG_TAG_FILTERING_AUTO_COMPLETE, "publish results method called")

            results?.let {
                val fullFilteredList = it.values as? ArrayList<String> ?: arrayListOf()
//                Helper.log(DEBUG_TAG_FILTERING_AUTO_COMPLETE, "FILTER RESULTS = " + suggestions.toString());
                val proposalMaxSize = 50
                suggestions = if (fullFilteredList.size > proposalMaxSize) {
                    ArrayList(fullFilteredList.subList(0, proposalMaxSize))
                } else {
                    fullFilteredList
                }
                notifyDataSetChanged()

            }
        }
    }
    var tagsAtTheEndComparator: Comparator<String> = Comparator<String> { o1, o2 -> o1.replace("#".toRegex(), "zzz").compareTo(o2.replace("#".toRegex(), "zzz"), ignoreCase = true) }

    init {
        complete_data = data
        suggestions = data
        layout_resource = layoutResource
    }
}