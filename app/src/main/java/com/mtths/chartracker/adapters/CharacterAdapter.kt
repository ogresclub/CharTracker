package com.mtths.chartracker.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import com.mtths.chartracker.*
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.widgets.SquareImageView
import kotlinx.coroutines.*
import java.util.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt


/**
 * An Adapter to display an Array of Characters in a GridView or ListView
 */
internal class CharacterAdapter(context: Context,
                                private var layoutResource: Int,
                                private var chars: ArrayList<Character> = ArrayList(),
                                private val thumbNailQuality: Int,
                                private var deactivateDeadAndRetiredChars: Boolean = true,
                                var numColsGridView: Int) : ArrayAdapter<Character>(context,
    R.layout.character
) {
    var showExp = true
    var textSizeFactor = 1f
    var initialTextSizeCharName = 12f
    var initialTextSizeExp = 14f
    var initialSizeSessionIcon = 50
    var sessionIconCharIconRatio = 5
    val minTextSizeExp = 7f
    val prefs: PrefsHelper = PrefsHelper(context)
    val dbHelper = DatabaseHelper.getInstance(context)

    /**
     * if id >= 0 this indicates, that we want to use the adapter for mark
     * which character we have currently selected and to change the selection
     */
    var selected_char_id: Long = -1

    fun noExp() {
        showExp = false
    }

    override fun getCount(): Int {
        return chars.size
    }

    fun setChars(chars: ArrayList<Character>) {
        this.chars = chars
    }

    override fun getItem(position: Int): Character? {
        return chars[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView != null) {
            holder = convertView.tag as ViewHolder
            holder.icon?.destroyDrawingCache()
            holder.sessionIconsContainer?.removeAllViews()
            holder.scope?.cancel()
        } else {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(layoutResource, parent, false)
            holder = ViewHolder()
            holder.charName = convertView.findViewById(R.id.charName)
            holder.icon = convertView.findViewById(R.id.icon)
            holder.sessionIconsContainer = convertView.findViewById(R.id.character_session_icon_container)
            holder.background = convertView.findViewById(R.id.background)
            holder.priority = convertView.findViewById(R.id.char_priority)

            if (showExp) {
                holder.totalExp = convertView.findViewById(R.id.total_exp_label)
                holder.currentExp = convertView.findViewById(R.id.current_exp_label)
            }
            holder.simpleCharIcon = convertView.findViewById(R.id.simple_char_icon)

            convertView.tag = holder
        }


        val char = chars[position]

        fillData(holder =  holder, char =  char)

        /////////////////////////////// ICONS //////////////////////////////////////

        /*
        either use custom icon and small simple icon or only big simple icon and no small icon
         */
        holder.scope = CoroutineScope(Dispatchers.Main)
        holder.scope?.launch {

            // ------------------------- RECALC EXP ------------------------------------//

            //-------------------------- MAIN ICON -------------------------------------//
            val d = async(context = Dispatchers.IO) {
                char.decodeCharIcon(context, thumbNailQuality)
            }
            d.await()?.let{
                holder.icon?.setImageDrawable(it)
            }

            //-------------------------- SESSION ICONS -------------------------------------//

            char.ruleSystems?.also {
                inflateSessionIcons(ruleSystems = it, holder = holder)
            } ?: CoroutineScope(Dispatchers.Main).launch {
                val job = async(context = Dispatchers.IO) {
                    dbHelper?.getRuleSystemsRelatedToCharacter(char_id = char.id)
                        ?.distinct()
                }
                val rSList = job.await()
                char.ruleSystems = rSList
                launch(Dispatchers.IO) {
                    dbHelper?.updateCharacter(char, keep_last_update_date = true)
                }
                fillData(holder =  holder, char =  char)
                inflateSessionIcons(ruleSystems =  rSList, holder =  holder)
            }


            //-------------------------- SIMPLE ICON -------------------------------------//
            val showSimpleCharIcons = char.iconFileName.isNotBlank()
            holder.simpleCharIcon?.setImageResource(
                if (showSimpleCharIcons) {
                    Global.charIconsMapWErrorIcon[char.iconName] ?: Character.ERROR_ICON
                } else {
                    R.color.transparent
                }
            )


        }


        return convertView!!
    }

    private fun fillData(holder: ViewHolder, char: Character) {

        val (id, _, _, _, priority, isActive, isRetired, isDead) = char


        var displayString = if (prefs.showCharNames) char.displayName else ""

        holder.charName?.text = displayString
        if (displayString.isEmpty()) {
            holder.charName?.visibility = View.GONE
        } else {
            holder.charName?.visibility = View.VISIBLE
        }

        if (PrefsHelper(context).showPriorities) {
            holder.priority?.visibility = View.VISIBLE
            holder.priority?.text = "$priority"
        } else {
            holder.priority?.visibility = View.GONE

        }


        holder.charName?.textSize = initialTextSizeCharName * textSizeFactor
        var textSizeExp: Float = initialTextSizeExp
        if (textSizeFactor < 1) {
            val maxTextSizeExp: Float = initialTextSizeExp * sqrt(textSizeFactor)
            textSizeExp = maxTextSizeExp * 5 / numColsGridView
            textSizeExp = min(maxTextSizeExp, textSizeExp)
            textSizeExp = max(minTextSizeExp, textSizeExp)
        }
        if (showExp) {
            if (prefs.showCurrentExp) {
                holder.currentExp?.visibility = View.VISIBLE
                holder.currentExp?.text = getCurrentExpString(char)
                holder.currentExp?.textSize = textSizeExp
                setTextColor(holder.currentExp, char)
                holder.currentExp?.text = getCurrentExpString(char)

            } else {
                holder.currentExp?.visibility = View.GONE
            }

            if (prefs.showTotalExp) {
                holder.totalExp?.visibility = View.VISIBLE
                holder.totalExp?.text = getTotalExpString(char)
                holder.totalExp?.setTypeface(null, Typeface.BOLD)
                holder.totalExp?.textSize = textSizeExp
                setTextColor(holder.totalExp, char)
                holder.totalExp?.text = getTotalExpString(char)
            } else {
                holder.totalExp?.visibility = View.GONE
            }
        }

        if (selected_char_id > -1) {
            if (id == selected_char_id) {
                holder.background?.setBackgroundResource(R.drawable.button_holo_green_light_for_character)
            } else {
                holder.background?.setBackgroundResource(R.drawable.button_light_grey_for_character)
            }

        } else {

            if (isDead && isRetired && deactivateDeadAndRetiredChars) {
                holder.background?.setBackgroundResource(R.drawable.button_violet_red_gradient_half_transparent_for_character)
            } else if (isDead && deactivateDeadAndRetiredChars) {
                holder.background?.setBackgroundResource(R.drawable.button_holo_red_light_for_character)
            } else if (isRetired && deactivateDeadAndRetiredChars) {
                holder.background?.setBackgroundResource(R.drawable.button_violet_half_transparent_for_character)
            } else if (isActive) {
                holder.background?.setBackgroundResource(R.drawable.button_holo_green_light_for_character)
            } else {
                holder.background?.setBackgroundResource(R.drawable.button_light_grey_for_character)
            }
        }

    }



    private fun inflateSessionIcons(ruleSystems: List<RuleSystem>?, holder: ViewHolder) {
        if((dbHelper?.getAllRuleSystems()?.size ?: 0) > 1 ) {
            ruleSystems?.forEach { rS ->
                val size = holder.icon?.width?.let { it / sessionIconCharIconRatio } ?: initialSizeSessionIcon
                val siv = SquareImageView(context)
                val layoutParams = RelativeLayout.LayoutParams(size, size)
                siv.layoutParams = layoutParams
                siv.setImageResource(rS.icon)
                holder.sessionIconsContainer?.addView(siv)
            }
        }
    }

    fun getCurrentExpString(character: Character): String {
        return when {
            Helper.rulesSR(character, context) -> String.format(Locale.GERMANY, "%,d¥", character.money)
            else -> String.format(Locale.GERMANY, "%,d",character.currentExp)
        }
    }

    fun getTotalExpString(character: Character): String {
        return when {
            Helper.rulesSR(character, context) -> String.format(Locale.GERMAN, "%d/%d", character.currentExp, character.totalExp)
            else -> String.format(Locale.GERMANY, "%,d", character.totalExp)
        }

    }

    fun setTextColor(tv: TextView?, character: Character) {
        tv?.setTextColor(when {
            prefs.rulesSR -> ContextCompat.getColor(context, android.R.color.holo_green_light)
            else -> ContextCompat.getColor(context, R.color.text_color_default)
        })
    }



    private class ViewHolder {
        var charName: TextView? = null
        var currentExp: TextView? = null
        var totalExp: TextView? = null
        var icon: SquareImageView? = null
        var priority: TextView? = null
        var background: RelativeLayout? = null
        var sessionIconsContainer: LinearLayout? = null
        var simpleCharIcon: SquareImageView? = null
        var scope: CoroutineScope? = null

    }
}