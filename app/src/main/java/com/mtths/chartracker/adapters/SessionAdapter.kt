package com.mtths.chartracker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.Session
import kotlin.collections.ArrayList

class SessionAdapter(c: Context,
                     private val layoutResource: Int,
                     var sessions: ArrayList<Session> = ArrayList()
) : ArrayAdapter<Session>(c, layoutResource) {

    override fun getItem(position: Int): Session? {
        try {
            return sessions[position]
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
        return null

    }

    override fun getCount(): Int {
        return sessions.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView != null) {
            holder = convertView.tag as ViewHolder
        } else {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(layoutResource, parent, false)
            holder = ViewHolder()
            holder.textView = convertView as TextView?
            convertView.tag = holder
        }

        getItem(position)?.let { session ->
            holder.textView?.let { tv ->
                val sb = session.getDisplaySpannable(context, tv.lineHeight)
                tv.text = sb
                if (session.isActive) {
                    tv.setBackgroundResource(R.color.holo_green_light_half_transparent)
                } else {
                    tv.setBackgroundResource(R.color.light_grey_half_transparent)
                }
            }

        }

        return convertView!!
    }

    private inner class ViewHolder {
        var textView: TextView? = null
    }

}