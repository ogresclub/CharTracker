package com.mtths.chartracker.adapters

import android.graphics.Color
import android.graphics.Typeface
import android.media.MediaPlayer
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.dataclasses.ItemCategory.Companion.walletCategories
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.ItemCategory
import com.pedromassango.doubleclick.DoubleClick
import com.pedromassango.doubleclick.DoubleClickListener
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.dialogs.DialogShowItems
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.HttpUtils

/**
 * this class takes a list of items and inflates the slot items displays in fragment_char_items
 * this is useful, because the slot item displays use a layout file
 * and otherwise we have to use many findViewbyId calls
 */
class CharItemAdapter(val slotItemsContainer: LinkedHashMap<Int,LinearLayout>? = null,
                      val backPackItemsContainer: LinearLayout? = null,
                      val fActivity: FragmentActivity,
                      private val doLootManagerStuff: (doNetwork: () -> String) -> Unit)
    : RecyclerView.Adapter<CharItemAdapter.BackPackViewHolder>(){
    /**
     * map LEFT/RIGHT to map which hands over the icon for the slot
     */
    val slots: HashMap<Int, List<String>> = HashMap()
    val allslots: MutableList<String> = mutableListOf()
    val categories: HashMap<String, ItemCategory> = ItemCategory.createCategoriesMap()
    var dbhelper: DatabaseHelper? = null
    val backPackLayout = R.layout.list_view_item_backpack
    private val mFilter = ItemFilter()
    var highlightText = ""
    var showOnlyAvailable = true
    var showOnlyGone = false
    var showOnlyCursed = false
    var showOnlyMarked = false
    var expandAll = false
    val context = fActivity

    /**
     * this specifies which onlick options are visible for a given item.
     * equipping when they are in stash, or removeing / using, when they are on character
     */
    var displayCharacter = false
    var managerAccess = false
    var onItemEquipped: () -> Unit = {}

    /**
     * only items with slots in the slotFilter will be displayed
     */
    var categoryFilter: List<String>? = null

    /**
     * return current value
     */
    fun toggleExpand(): Boolean {
        expandAll = !expandAll
        return expandAll
    }

    init {
        slots[LEFT] = listOf("eyes", "amulet", "armor", "weapon", "rod", "hand", "belt", "boots")
            .also { allslots.addAll(it) }
        slots[RIGHT] = listOf("head", "cloak", "chest", "arm", "ranged", "shield", "ring")
            .also { allslots.addAll(it) }

        dbhelper = DatabaseHelper.getInstance(context = context)

    }

    var character: Character? = null
    var completeStash: ArrayList<Item>? = null
    var filteredStash: ArrayList<Item> = ArrayList()

    /**
     * all item that are displayed as lot items are found here.
     * All other items will be put in backpack
     */
    var slotItemIds: ArrayList<Int> = ArrayList()
    var expandedSlotItemsIds: ArrayList<Int> = ArrayList()
    val slotItemLayoutResource = R.layout.character_slot_item

    /**
     * return all items with the given slot
     */
    fun getSlotItems(slot: String): ArrayList<Item> {
        val result = ArrayList<Item>()
        character?.loot?.forEach { item ->
            if (item.category.equals(slot, ignoreCase = true)) {
                result.add(item)
            }
        }
        if (result.isEmpty()) {
            result.add(Item(category = slot))
            if (slot.equals("ring", ignoreCase = true)) {
                // add second ring slot if both are empty
                result.add(Item(category = slot))
            }
        }
        if (slot.equals("ring", ignoreCase = true)) {
            if (result.size == 1) {
                // add second ring slot if one is equipped
                result.add(Item(category = slot))
            }
            if (result.size <= 2 &&
                result.map { it.name.lowercase() }
                    .find { it == "cockring" || it == "ring of cock"} != null) {
                // add third ring slot in special cases!
                result.add(Item(category = slot))
            }
        }
        return result
    }

    fun inflate() {
        inflateSlots()
        inflateBackPack()
    }

    open inner class ListenerImplUseItem(
        val forManager: Boolean,
        val character: Character,
        val item: Item,
        var preChangeBalanceTask: () -> Unit = {}
    ): View.OnLongClickListener {
        override fun onLongClick(v: View?): Boolean {
            doLootManagerStuff {
                Helper.log("LOOTMANAGER", "use item")
                val prefs = PrefsHelper(context)
                var response = "no Auth-Token Found"
                prefs.lootmanagerToken?.let {
                    val charName = if (forManager) "" else character.lootManagerDownloadName
                    val stashName = if (forManager && item.stashes.size == 1) item.stashes[0].name else ""
                    val itemName = if (item.id == -1) item.name.lowercase() else ""
                    preChangeBalanceTask()
                    response = HttpUtils.changeBalance(
                        characterName = charName,
                        stashName = stashName,
                        amount = -1,
                        item_id = item.id,
                        itemName = itemName,
                        token = it,
                        deviceId = prefs.lootmanagerDeviceId
                    )
                    Helper.log(
                        "LOOTMANAGER",
                        "repose for useItem: $response"
                    )
                }
                response
            }
            return true
        }
    }

    open inner class ListenerImplItem(
        val item: Item,
        val showCategory: Boolean,
        val showOnlyAvailable: Boolean = false) : DoubleClickListener {

        /**
         * only use for backpack
         */
        override fun onSingleClick(view: View?) {
            val expanded = item.id in expandedSlotItemsIds
            if (expanded) {
                expandedSlotItemsIds.remove(item.id)
            } else {
                expandedSlotItemsIds.add(item.id)
            }
            try {
                view?.findViewById<TextView>(R.id.item_text)?.let { tv ->
                    var sb = item.getSpannableString(
                        showCategory = showCategory && !expanded,
                        expanded = !expanded,
                        context = context,
                        lineHeight = tv.lineHeight,
                        showAll = !showOnlyAvailable)
                    sb = highlightSearchQuery(sb = sb,
                        originalText = sb.toString(),noSubMatches = false)
                    tv.text = sb

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        override fun onDoubleClick(view: View?) {
            character?.let { c ->
                val link = "${HttpUtils.LOOT_MANAGER_URL}/item?id=${item.id}&char=${c.lootManagerDownloadName.lowercase()}"
                HttpUtils.openWebPage(url = link, activity = fActivity)
            }
        }
    }

    fun isExpanded(item: Item): Boolean {
        return expandAll || item.id in expandedSlotItemsIds
    }

    fun highlightSearchQuery(sb: SpannableStringBuilder,
                             originalText: String,
                             noSubMatches: Boolean): SpannableStringBuilder {
        /*
        highlight search Query
         */
        var result = Helper.setColor(
            textToChange = arrayListOf(highlightText),
            originalText = originalText,
            ForegroundColorSpanConstructorArg = Color.BLUE,
            spannable = sb,
            noSubstringMatches = noSubMatches
        )
        result = Helper.setStyle(
            textToChange = arrayListOf(highlightText),
            originalText = originalText,
            styleSpanConstructorArg = Typeface.BOLD,
            spannable = result,
            noSubstringMatches = noSubMatches
        )
        return result
    }


    fun fillBackpackData(viewHolder: BackPackViewHolder, item: Item) {
        viewHolder.textView?.let { tv ->

            val expanded = isExpanded(item)
            var sb = item.getSpannableString(
                context = context,
                showCategory = expanded,
                lineHeight = tv.lineHeight,
                expanded = expanded,
                showAll = !showOnlyAvailable)
            val originalText = sb.toString()
            val noSubMatches = false

            /*
            highlight search Query
             */
            sb = highlightSearchQuery(sb, originalText, noSubMatches)
            tv.text = sb
        }

        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        params.setMargins(0, 0, 0, 5)
        viewHolder.container?.layoutParams = params

        /*
        in get-char.items API call no characters are given,
        thus numAvailable is always equal to amount and char items are not marked as used
         */
        if (item.numAvailable() <= 0) {
            viewHolder.container?.setBackgroundResource(R.drawable.character_slot_item_background_unavailable)
        } else {
            viewHolder.container?.setBackgroundResource(R.drawable.character_slot_item_background)
        }

        viewHolder.imageView?.setImageResource(
            categories[item.category.lowercase()]?.getIconImageResource?.let { it(item) }
                ?: R.drawable.ic_loot
        )
    }

    fun isSlotItem(item: Item): Boolean {
        return allslots.find { it.equals(item.category, ignoreCase = true) } != null
    }

    /**
     * only use for local items
     */
    fun updateStashesAndStoreCharacterLoot(character: Character, modifiedLocalItem: Item) {
        Helper.replaceItem(completeStash, modifiedLocalItem, addIfNotFound = false)
        Helper.replaceItem(filteredStash, modifiedLocalItem, addIfNotFound = false)
        Helper.replaceItem(character.schwarzloot, modifiedLocalItem, addIfNotFound = true)
        notifyDataSetChanged()
        Log.d("Schwarzgeld", "updated schwarzloot of character:  " + character.schwarzloot.joinToString())
        Helper.updateCharacter(
            character = character, keep_last_update_date = false, context = context)

    }

    fun implUnequipItem(viewHolder: BackPackViewHolder, item: Item, character: Character) {
        viewHolder.iconUnequip?.setOnLongClickListener {
            if (item.name == Item.SCHWARZGELD.name) {
                /*
                Schwarzgeld should only be available for character items, not for stash items!
                 */
                // this will add the removed schwarzgeld to the stash
                changeBalance(spend = false,
                    item = item.copy(name = "Silver", stashes = character.stashes.toMutableList()),
                    character = character,
                    forManager = true,
                    moveToStash = true,
                    msg = context.getString(R.string.msg_how_much_schwarzgeld_to_stash),
                    localItem = item)
            } else {
                val doRemoveItem = { amount: Int ->
                    doLootManagerStuff {
                        Helper.log("LOOTMANAGER", "remove item from character")
                        val prefs = PrefsHelper(context)
                        var response = "no Auth-Token Found"
                        prefs.lootmanagerToken?.let {
                            response = HttpUtils.removeItemFromCharacter(
                                characterName = character.lootManagerDownloadName,
                                item_id = item.id,
                                amount = amount,
                                token = it,
                                deviceId = prefs.lootmanagerDeviceId
                            )
                            Helper.log(
                                "LOOTMANAGER",
                                "repose for remove-item-from-character: $response"
                            )
                        }
                        response
                    }
                }
                if (item.amount > 2) {
                    DialogCustom.showNumberPrompt(
                        fragmentManager = context.supportFragmentManager,
                        context = context,
                        message = "How many items do you want to put back into the stash?",
                        minValue = 0,
                        initialValue = item.amount,
                        maxValue = item.amount,
                        onConfirm = { amount ->
                            doRemoveItem(amount)
                        })
                } else {
                    doRemoveItem(1)
                }
            }

            return@setOnLongClickListener true
        }
    }

    fun implUseCharges(viewHolder: BackPackViewHolder, item: Item, character: Character,
                       forCharItems: Boolean, forStashItems: Boolean, forStashItemsAsManager: Boolean) {
        setUseChargesVisibility(viewHolder, forStashItems, forStashItemsAsManager, item)
        if (item.charges > 0) {
            viewHolder.iconUseCharge?.setOnLongClickListener {
                doLootManagerStuff {
                    Helper.log("LOOTMANAGER", "use charge")
                    val prefs = PrefsHelper(context)
                    var response = "no Auth-Token Found"
                    prefs.lootmanagerToken?.let {
                        response = HttpUtils.useCharge(
                            item_id = item.id,
                            token = it,
                            deviceId = prefs.lootmanagerDeviceId
                        )
                        Helper.log(
                            "LOOTMANAGER",
                            "repose for use_charge: $response"
                        )
                    }
                    response
                }
                return@setOnLongClickListener true
            }
        } else if (item.category.equals("scroll", ignoreCase = true)) {
            viewHolder.iconUseCharge?.setOnLongClickListener(
                ListenerImplUseItem(
                    character = character,
                    item = item,
                    forManager = forStashItemsAsManager,
                    preChangeBalanceTask = {playSound(R.raw.sfx_use_scroll)}))
        }
    }

    fun playSound(rawId: Int)  = MediaPlayer.create(context, rawId).start()

    fun implDrinkPotion(viewHolder: BackPackViewHolder, item: Item, character: Character,
                        forCharItems: Boolean, forStashItems: Boolean, forStashItemsAsManager: Boolean) {
        setDrinkPotionVisibility(viewHolder, forStashItems, forStashItemsAsManager, item)
        if (item.category.equals("potion", ignoreCase = true)) {
            viewHolder.iconDrinkPotion?.setOnLongClickListener(
                ListenerImplUseItem(
                    forManager = forStashItemsAsManager,
                    character = character,
                    item = item,
                    preChangeBalanceTask = {
                        playSound(R.raw.sfx_drink_potion)
                    }
                )
            )
        }
    }
    fun implReceiveOrSpentCurrency(
        viewHolder: BackPackViewHolder,
        item: Item,
        character: Character,
        forCharItems: Boolean,
        forManager: Boolean,
        forStashItems: Boolean, forStashItemsAsManager: Boolean) {
        setReceivePayMoneyVisibility(viewHolder, forCharItems, forStashItems,
            forStashItemsAsManager, item)
        if (item.category.equals("currency", ignoreCase = true) ||
            item.category.equals("treasure", ignoreCase = true)) {


            viewHolder.iconPayCurrency?.setOnClickListener {
                changeBalance(
                    spend = true,
                    character = character,
                    item = item,
                    forManager = forManager,
                    onlyLocal = item.onlyLocal
                )
            }
            viewHolder.iconReceiveCurrency?.setOnClickListener {
                changeBalance(
                    spend = false,
                    character = character,
                    item = item,
                    forManager = forManager,
                    onlyLocal = item.onlyLocal
                )
            }

        }
    }
    fun implEquipItem(viewHolder: BackPackViewHolder, item: Item, character: Character) {
        viewHolder.iconEquip?.setOnLongClickListener {
            val doEquipItem = { amount: Int ->
                doLootManagerStuff {
                    Helper.log("LOOTMANAGER", "equip item")
                    val prefs = PrefsHelper(context)
                    var response = "no Auth-Token Found"
                    prefs.lootmanagerToken?.let {
                        response = HttpUtils.equipItem(
                            characterName = character.lootManagerDownloadName,
                            item_id = item.id,
                            amount = amount,
                            token = it,
                            deviceId = prefs.lootmanagerDeviceId
                        )
                        Helper.log(
                            "LOOTMANAGER",
                            "repose for equip item: $response"
                        )
                    }
                    response
                }
            }
            if (item.numAvailable() > 2 && !isSlotItem(item)) {
                DialogCustom.showNumberPrompt(
                    fragmentManager = context.supportFragmentManager,
                    message = "How many items do you want to equip?",
                    context = context,
                    minValue = 0,
                    maxValue = item.numAvailable(),
                    initialValue = item.numAvailable(),
                    onConfirm = {amount ->
                        doEquipItem(amount)
                    }
                )
            } else {
                doEquipItem(1)
            }
            return@setOnLongClickListener true
        }
    }

    fun setUnequipVisibility(viewHolder: BackPackViewHolder,
                             forStashItems: Boolean,
                             forStashItemsAsManager: Boolean) {
        if (forStashItems) {
            viewHolder.iconUnequip?.visibility = View.GONE
        }
        if (forStashItemsAsManager) {
            viewHolder.iconUnequip?.visibility = View.GONE
        }
    }

    fun setEquipVisibility(viewHolder: BackPackViewHolder,
                           forCharItems: Boolean,
                           forStashItems: Boolean,
                           forStashItemsAsManager: Boolean,
                           item: Item
    ) {
        if (forCharItems) {
            viewHolder.iconEquip?.visibility = View.GONE
        }
        if (forStashItems) {
            viewHolder.iconEquip?.visibility = if (item.isGone() || !item.isAvailable) View.GONE else View.VISIBLE
        }
        if (forStashItemsAsManager) {
            if (item.category in walletCategories) {
                viewHolder.iconEquip?.visibility =  View.GONE
            } else {
                viewHolder.iconEquip?.visibility = if (item.isGone() || !item.isAvailable) View.GONE else View.VISIBLE
            }
        }
    }

    fun setDrinkPotionVisibility(viewHolder: BackPackViewHolder,
                                 forStashItems: Boolean,
                                 forStashItemsAsManager: Boolean,
                                 item: Item
    ) {
        if (item.category.equals("potion", ignoreCase = true)) {
            if (forStashItems) viewHolder.iconDrinkPotion?.visibility = View.GONE
            if (forStashItemsAsManager) {
                viewHolder.iconDrinkPotion?.visibility =
                    if (item.isAvailable) View.VISIBLE else View.GONE
            }
        } else {
            viewHolder.iconDrinkPotion?.visibility = View.GONE
        }

    }

    fun setReceivePayMoneyVisibility(viewHolder: BackPackViewHolder,
                                     forCharItems: Boolean,
                                     forStashItems: Boolean,
                                     forStashItemsAsManager: Boolean,
                                     item: Item
    ) {
        if (item.category.equals("currency", ignoreCase = true) ||
            item.category.equals("treasure", ignoreCase = true)) {
            if (forCharItems) {
                viewHolder.iconPayCurrency?.visibility =
                    if (!item.isAvailable) View.GONE else View.VISIBLE
            }
            if (forStashItems) {
                viewHolder.iconPayCurrency?.visibility = View.GONE
                viewHolder.iconReceiveCurrency?.visibility = View.GONE
            }
            if (forStashItemsAsManager) {
                viewHolder.iconPayCurrency?.visibility =
                    if (!item.isAvailable) View.GONE else View.VISIBLE
                if (item.category.equals("currency", ignoreCase = true)) {
                    viewHolder.iconReceiveCurrency?.visibility = View.VISIBLE
                }
            }
        } else {
            viewHolder.iconPayCurrency?.visibility = View.GONE
            viewHolder.iconReceiveCurrency?.visibility = View.GONE
        }
    }

    fun setUseChargesVisibility(viewHolder: BackPackViewHolder,
                                forStashItems: Boolean,
                                forStashItemsAsManager: Boolean,
                                item: Item
    ) {
        if (item.charges > 0 || item.category.equals("scroll", ignoreCase = true)) {
            if (forStashItems) viewHolder.iconUseCharge?.visibility = View.GONE
            if (forStashItemsAsManager) {
                viewHolder.iconUseCharge?.visibility =
                    if (item.isAvailable) View.VISIBLE else View.GONE
            }
        } else {
            viewHolder.iconUseCharge?.visibility = View.GONE
        }
    }


    fun setBackpackOnClickListeners(
        viewHolder: BackPackViewHolder,
        item: Item,
        character: Character?,
        forCharItems: Boolean,
        forStashItems: Boolean,
        forStashItemsAsManager: Boolean) {

        /*
        for character and stash:
        open in browser on double click
        expand on click
         */
        viewHolder.textView?.setOnClickListener(
            DoubleClick(
                ListenerImplItem(
                    item = item,
                    showCategory = true,
                    showOnlyAvailable = showOnlyAvailable)
            )
        )

        setUnequipVisibility(viewHolder, forStashItems, forStashItemsAsManager)
        setEquipVisibility(viewHolder, forCharItems, forStashItems, forStashItemsAsManager, item)

        if (forCharItems) {
            viewHolder.textView?.setOnLongClickListener {
                changeEquippedStatus(item = item)
                return@setOnLongClickListener true
            }
        }


        character?.let { c ->
            implEquipItem(viewHolder = viewHolder, item = item, character = c)
            implUnequipItem(viewHolder = viewHolder, item = item, character = c)
            implUseCharges(viewHolder = viewHolder, item = item, character = c,
                forCharItems = forCharItems, forStashItems = forStashItems,
                forStashItemsAsManager = forStashItemsAsManager)
            implDrinkPotion(viewHolder = viewHolder, item = item, character = c,
                forCharItems = forCharItems, forStashItems = forStashItems,
                forStashItemsAsManager = forStashItemsAsManager)
            implReceiveOrSpentCurrency(viewHolder = viewHolder,
                item = item, character = c, forManager = forStashItemsAsManager,
                forCharItems = forCharItems, forStashItems = forStashItems,
                forStashItemsAsManager = forStashItemsAsManager)

        }
    }

    fun updateItemInCompleteStash(item: Item) {
        Log.d("Schwarzgeld", "schwarzgeld in complete stash after updating:  " + completeStash?.filter { it.onlyLocal }?.joinToString())
    }

    /**
     * call Dialog to prompt amount and then add money to character
     * @param spend if true, money will be spent instead of added
     */
    fun changeBalance(spend: Boolean,
                      item: Item,
                      character: Character,
                      forManager: Boolean,
                      /**
                       * this means that the item is locally reduced by prompted amount and this amount is added to the stash
                       */
                      moveToStash: Boolean = false,
                      msg: String? = null,
                      onlyLocal: Boolean = false,
                      localItem: Item = item) {
        var mMsg = context.getString(
            if (spend) R.string.msg_how_much_pay else R.string.msg_how_much_receive,
            item.name
        )
        msg?.let {
            mMsg = it
        }
        Log.d("LOOTMANAGER","item to move to stash: id=${item.id} $item")
        Log.d("LOOTMANAGER","local item: id=${localItem.id} $localItem")
        Log.d("LOOTMANAGER","item stashes ${item.stashes.joinToString()}")
        val max = if (spend) item.numAvailable() else null
        val charName = if (forManager) "" else character.lootManagerDownloadName
        val stashName = if (forManager && item.stashes.size == 1) item.stashes[0].name else ""
        val itemName = if (item.id < 0) item.name.lowercase() else ""
        Log.d("LOOTMANAGER","API params charName=$charName, stashName=$stashName, id=${item.id}, itemName=$itemName")


        DialogCustom.showNumberPrompt(
            fragmentManager = context.supportFragmentManager,
            message = mMsg,
            context = context,
            maxValue = max,
            initialValue = max,
            proposals = listOf(10, 100, 1000, 10000, 100000),
            onConfirm = {mAmount ->
                val amount = if (spend) -mAmount else mAmount
                if (moveToStash) {
                    localItem.amount -= mAmount
                    updateStashesAndStoreCharacterLoot(character, modifiedLocalItem = localItem)
                }
                if(onlyLocal) {
                    localItem.amount += amount
                    updateStashesAndStoreCharacterLoot(character, modifiedLocalItem = localItem)
                } else {
                    doLootManagerStuff {
                        Helper.log("LOOTMANAGER", "change balance amount")
                        val prefs = PrefsHelper(context)
                        var response = "no Auth-Token Found"
                        prefs.lootmanagerToken?.let {
                            response = HttpUtils.changeBalance(
                                characterName = charName,
                                stashName = stashName,
                                item_id = item.id,
                                itemName = itemName,
                                amount = amount,
                                token = it,
                                deviceId = prefs.lootmanagerDeviceId
                            )
                            Helper.log(
                                "LOOTMANAGER",
                                "repose for change balance: $response"
                            )
                        }
                        response
                    }
                }

            }
        )
    }

    /**
     * first call inflate slots to sort out items that are displayed in slots
     */
    fun inflateBackPack() {
        backPackItemsContainer?.removeAllViews()
        character?.loot?.sortBy { it.category }
        var itemsToShow: List<Item>? = character?.loot?.filterNot { it.category in walletCategories }
        itemsToShow?.forEach { item ->
            if (item.id !in slotItemIds) {

                val container = LayoutInflater.from(context).inflate(
                    backPackLayout, null, false) as LinearLayout
                val viewHolder = BackPackViewHolder(container)

                fillBackpackData(
                    viewHolder= viewHolder,
                    item = item)

                character?.let { c ->
                    setBackpackOnClickListeners(
                        viewHolder = viewHolder,
                        item = item,
                        character = c,
                        forCharItems = true,
                        forStashItems = false,
                        forStashItemsAsManager = false)
                }


                Helper.log("items", item.toString())

                backPackItemsContainer?.addView(container)
            }
        }
    }

    fun inflateSlots() {
        slotItemIds.clear()
        //inflate left and right side of icons
        listOf(LEFT, RIGHT).forEach { side ->
            inflateSlots(side)
        }
    }

    fun inflateSlots(side: Int) {
        val inflater = LayoutInflater.from(context)
        slotItemsContainer?.get(side)?.removeAllViews()
        slots[side]?.let { slotsens ->
            for (slot in slotsens) {
                val slotItems = getSlotItems(slot)
                /*
                slotItems[0].id == -1 means there is no item for this slot,
                just a placeholder item, so it gets inflated
                 */
                if (!(slotItems.size <= 2 && slotItems[0].id == -1 && categories[slot]?.optional == true)) {
                    for (item in slotItems) {
                        if (item.equipped) {
                            slotItemIds.add(item.id)
                            val slotView = inflater.inflate(slotItemLayoutResource, null, false)
                            val textView = slotView.findViewById<TextView>(R.id.item_text)
                            val slotText = slotView.findViewById<TextView>(R.id.slot_text)
                            val slotIconView = slotView.findViewById<ImageView>(R.id.slot_icon)

                            slotView.setOnClickListener(
                                DoubleClick(
                                    ListenerImplItem(item = item, showCategory = false)))

                            if (item.id == -1) {
                                slotView.setOnLongClickListener {
                                    character?.let { c ->
                                        DialogShowItems.newInstance(
                                            char_id = c.id,
                                            categories = arrayListOf(slot),
                                            showCharItems = false).apply {
                                            onItemEquipped = this@CharItemAdapter.onItemEquipped
                                        }
                                            .show(context.supportFragmentManager, "showStash")
                                    }
                                    return@setOnLongClickListener true
                                }
                            } else {
                                slotView.setOnLongClickListener {
                                    changeEquippedStatus(item = item)
                                    return@setOnLongClickListener true
                                }
                            }


                            textView.text =
                                item.getSpannableString(
                                    expanded = isExpanded(item),
                                    context = context,
                                    lineHeight = textView.lineHeight,
                                    showCategory = false,
                                    showAll = true)
                            slotText.text = slot
                            categories[slot]?.getIconImageResource?.let { getSlotIconId ->
                                slotIconView.setImageResource(getSlotIconId(item))
                            }

                            slotItemsContainer?.get(side)?.addView(slotView)

                        }
                    }
                }
            }
        }

    }

    private fun changeEquippedStatus(item: Item) {
        item.changeEquipped()
        var updateBuff = false
        character?.let {

            if (item.buffs.isNotBlank() &&
                it.getPrefs(context).updateBuffActivityAccordingToItemEquippedState
                ) {
                //////////// don't know if this is a good idea ///////////////////
                // set item buffs activity according to equipped state //
                updateBuff = true
                it.buffs.find { it.name == item.buffName }?.active = item.equipped
            }


            dbhelper?.updateCharacter(character = it, keep_last_update_date = false)
        }
        inflate()
        if (updateBuff) {
            updateAllDisplays(generateCPI = false)
        }

    }

    private fun updateAllDisplays(generateCPI: Boolean = false) {
        try {
            (fActivity as? ActivityCharDetails?)?.updateDisplays(generateCPI = generateCPI)
        } catch (e: Exception) {
            Helper.log("can't update displays after changing equipped state of item because activity is not activityCharDetails")
            e.printStackTrace()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BackPackViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v  = inflater.inflate(backPackLayout, parent, false)
        return BackPackViewHolder(viewItem = v)
    }

    override fun onBindViewHolder(holder: BackPackViewHolder, position: Int) {
        val item = filteredStash[position]
        fillBackpackData(holder, item)
        setBackpackOnClickListeners(
            viewHolder = holder,
            item = item,
            character = character,
            forCharItems = displayCharacter,
            forStashItems = !(displayCharacter || managerAccess),
            forStashItemsAsManager = managerAccess)

    }

    override fun getItemCount(): Int {
        return filteredStash.size
    }

    fun fill(constraint: CharSequence?) {
        mFilter.filter(constraint)
    }

    fun clear() {
        filteredStash.clear()
    }

    fun correctCategory(item: Item): Boolean {
        categoryFilter?.let {
            return item.category in it
        }
        return true
    }

    private inner class ItemFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {

            val results = FilterResults()
            val resultValues = ArrayList<Item>()

            filteredStash.clear()

            constraint?.let { constr ->
                val lconstr = constr.toString().lowercase()
                completeStash?.forEach {
                    if (correctCategory(it) &&
                        ((showOnlyMarked && it.marked) || !showOnlyMarked) &&
                        ((showOnlyGone && it.isGone()) || (!showOnlyGone && !it.isGone())) &&
                        ((showOnlyCursed && it.cursed) || !showOnlyCursed) &&
                        ((showOnlyAvailable && it.numAvailable() > 0) || !showOnlyAvailable)) {
                        if (lconstr in it.toString().lowercase() ) resultValues.add(it)
                    }
                }
            }

            results.values = resultValues
            results.count = resultValues.size

            return results

        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            try {
                results?.let {
                    filteredStash = results.values as ArrayList<Item>
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            notifyDataSetChanged()

        }

    }

    class BackPackViewHolder(viewItem: View) : RecyclerView.ViewHolder(viewItem) {
        var textView: TextView? = null
        var imageView: ImageView? = null
        var container: LinearLayout? = null

        /**
         * invisible in dialogShowStash (uses onBindViewHolder)
         */
        var iconUnequip: ImageView? = null
        /**
         * invisible in fragmentCharItems (uses getView for backpack)
         */
        var iconEquip: ImageView? = null
        /**
         * invisible in dialogShowStash (uses onBindViewHolder)
         */
        var iconDrinkPotion: ImageView? = null
        /**
         * invisible in dialogShowStash (uses onBindViewHolder)
         */
        var iconUseCharge: ImageView? = null
        var iconPayCurrency: ImageView? = null
        var iconReceiveCurrency: ImageView? = null

        init {
            textView = viewItem.findViewById(R.id.item_text)
            imageView = viewItem.findViewById(R.id.icon)
            container = viewItem as LinearLayout
            iconUnequip = viewItem.findViewById(R.id.icon_unequip)
            iconUseCharge = viewItem.findViewById(R.id.icon_use_charge)
            iconEquip = viewItem.findViewById(R.id.icon_equip)
            iconDrinkPotion = viewItem.findViewById(R.id.icon_drink_potion)
            iconPayCurrency = viewItem.findViewById(R.id.icon_pay_currency)
            iconReceiveCurrency = viewItem.findViewById(R.id.icon_receive_currency)
        }
    }


    companion object {
        const val LEFT = 0
        const val RIGHT = 1
    }
}