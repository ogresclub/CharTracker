package com.mtths.chartracker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.ImagedText
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.Session

class RuleSystemAdapter(context: Context,
                        private var layoutResource: Int,
                        textViewResourceId: Int,
                        list: List<RuleSystem>) :
    ImagedTextAdapter(
        context,
        layoutResource,
        textViewResourceId,
        list.map { ImagedText(text = it.name, icon = it.icon) })