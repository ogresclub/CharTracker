package com.mtths.chartracker.dropbox

import com.dropbox.core.DbxRequestConfig
import com.dropbox.core.http.OkHttp3Requestor


class DbxRequestConfigFactory {

    companion object {

        private var sDbxRequestConfig: DbxRequestConfig? = null

        fun getRequestConfig(): DbxRequestConfig? {
            if (sDbxRequestConfig == null) {
                sDbxRequestConfig = DbxRequestConfig.newBuilder("chartracker")
                    .withHttpRequestor(OkHttp3Requestor(OkHttp3Requestor.defaultOkHttpClient()))
                    .build()
            }
            return sDbxRequestConfig
        }
    }


}