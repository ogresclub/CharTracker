package com.mtths.chartracker.dropbox

import com.dropbox.core.v2.DbxClientV2
import com.mtths.chartracker.dropbox.DbxHelper.Companion.DROPBOX_DEBUG_TAG
import com.mtths.chartracker.utils.Helper

/**
 * Singleton instance of [DbxClientV2] and friends
 */
/**
 * Singleton instance of [DbxClientV2] and friends
 */
object DropboxClientFactory {
    private var sDbxClient: DbxClientV2? = null
    fun init(accessToken: String?) {
        if (sDbxClient == null) {
            Helper.log(
                DROPBOX_DEBUG_TAG,
                "Init Dropbox Client Factory with access token: $accessToken"
            )
            sDbxClient = DbxClientV2(DbxRequestConfigFactory.getRequestConfig(), accessToken)
        }
    }

    val client: DbxClientV2?
        get() {
            checkNotNull(sDbxClient) { "Client not initialized." }
            return sDbxClient
        }
}