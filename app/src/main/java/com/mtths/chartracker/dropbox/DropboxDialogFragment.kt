package com.mtths.chartracker.dropbox

import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment

open class DropboxDialogFragment : DialogFragment() {
    lateinit var dbXHelper: DbxHelper
    private var dismissAfterJob = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dbXHelper = DbxHelper(requireContext())
    }

    protected fun doDropBoxStuff(dismissAfterJob: Boolean = true, doStuff: () -> Unit = {}) {
        dbXHelper.doDropBoxStuff(
            doStuff = doStuff,
            doBeforeStart = {this.dismissAfterJob = dismissAfterJob})
    }

    override fun onResume() {
        super.onResume()
       dbXHelper.onResume {
           if (dismissAfterJob) {
               dismiss()
           }
        }
    }
}