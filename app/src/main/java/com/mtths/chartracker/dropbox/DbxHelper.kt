package com.mtths.chartracker.dropbox

import android.content.Context
import android.content.Intent
import android.os.ResultReceiver
import com.dropbox.core.android.Auth
import com.dropbox.core.oauth.DbxCredential
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DbxHelper(val context: Context) {

    private var stayIdle = true
        set(value) {
            Helper.log(
                DROPBOX_DEBUG_TAG,
                "````````````````````setting stay idle to $value: ${Exception().stackTraceToString()}"
            )
            field = value
        }
    private var doTask: () -> Unit = {}
    var mDropBoxResultReceiver: ResultReceiver? = null


    fun doDropBoxStuff(doStuff: () -> Unit, doBeforeStart: () -> Unit = {}) {
//        fileNameNoEnding.forEach { fileName ->
//            log("AAA", "init Dropbox: fileNames: $fileName")
//        }

        /*
        THIS IS SIMPLY A COPY OF THE CODE FROM DROPBOX DIALOG FRAGMENT,
        BUT I WAS TOO LAZY TO FIND A BETTER SOLUTION
         */

        val prefs = PrefsHelper(context = context)
        val credential = prefs.dropBoxCredential
        doBeforeStart()


        Helper.log(
            DROPBOX_DEBUG_TAG,
            "doDropBoxStuff called: context = $this, credential = $credential"
        )

        if (credential == null) {
            /*
            after that on Resume will be called
            we need to define here, what shall be done in OnResume by setting
            */
            stayIdle = false
            Helper.log(DROPBOX_DEBUG_TAG, "setted stayIdle to $stayIdle#")
            doTask = doStuff
            Helper.log(
                DROPBOX_DEBUG_TAG,
                "credential was null. stayIdle = $stayIdle"
            )
            Helper.log(
                DROPBOX_DEBUG_TAG,
                "Authenticate Dropbox to open Session"
            )

            try {
                Auth.startOAuth2PKCE(
                    context,
                    PrefsHelper.DROPBOX_APP_KEY,
                    DbxRequestConfigFactory.getRequestConfig(),
                    listOf("account_info.read", "files.metadata.read", "files.content.write", "files.content.read")
                )
            } catch (e: Exception) {
                Helper.log(
                    DROPBOX_DEBUG_TAG,
                    "failed oAuth2 for Dropbox: ${e.message}"
                )
            }


        } else {

            /*
            credential exists
            refresh access token if about to expire

            this is done in background, because it is network traffic
             */

            CoroutineScope(Dispatchers.Main).launch {
                refreshTokenIfNeeded(credential, context)
                DropboxClientFactory.init(credential.accessToken)
                Helper.log("Dropbox client initiated after token was refreshed: doing Dropbox Stuff")
                doStuff()
            }
        }
    }

    private suspend fun refreshTokenIfNeeded(credential: DbxCredential, context: Context) {
        withContext(Dispatchers.IO) {
            if (credential.aboutToExpire()) {
                Helper.log("DROPBOX", "refreshing access token because it is about to expire: expired credential: $credential")
                credential.refresh(DbxRequestConfigFactory.getRequestConfig())
                PrefsHelper(context).dropBoxCredential = credential
                Helper.log("DROPBOX", "refreshed credential: $credential")

            }
        }
    }

    /**
     * THIS IS USED FOR LEGACY STUFF.
     * IF YOU JUST WANT TO UPLOAD SOME FILES, USE THE METHOD BELOW
     * WITH FILE ENDINGS
     *
     * Start DropBoxIntendWith Task = Upload/Download and Mode = xml/db
     * @param mode Xml or db
     * @param task Upload or Download
     * @param fileNameNoEnding file names wihtout Ending
     */
    fun startDropboxIntent(task: Int, mode: Int,
                                     vararg fileNameNoEnding: String,
                                     overallProgressIndicator: Int = 0) {
        doDropBoxStuff(doStuff =  {
            //        fileNameNoEnding.forEach { fileName ->
//            log("AAA", "Start Dropbox: fileNames: $fileName")
//        }
            GlobalScope.launch {
                val intent = Intent(context, DropBoxIntent::class.java)
                intent.putExtra(DropBoxIntent.EXTRA_RESULT_RECEIVER, mDropBoxResultReceiver)
                intent.putExtra(DropBoxIntent.EXTRA_TASK, task)
                intent.putExtra(DropBoxIntent.EXTRA_MODE, mode)
                intent.putExtra(
                    DropBoxIntent.EXTRA_REF_OVERALL_PROGRESS_INDICATOR,
                    overallProgressIndicator
                )
                intent.putExtra(DropBoxIntent.EXTRA_FILE_NAMES_NO_ENDING, fileNameNoEnding)
                Helper.log("DropBoxIntent will be started")
                context.startService(intent)
            }
        }
        )
    }

    /**
     * Start DropBoxIntendWith Task = Upload/Download and Mode = xml/db
     * @param mode Xml or db
     * @param task Upload or Download
     * @param fileNameWithEnding
     */
    fun startDropboxIntentWthEnding(task: Int, mode: Int,
                                              vararg fileNameWithEnding: String,
                                              overallProgressIndicator: Int = 0) {
        doDropBoxStuff(doStuff =   {
            //        fileNameNoEnding.forEach { fileName ->
//            log("AAA", "Start Dropbox: fileNames: $fileName")
//        }
            GlobalScope.launch {
                val intent = Intent(context, DropBoxIntent::class.java)
                intent.putExtra(DropBoxIntent.EXTRA_RESULT_RECEIVER, mDropBoxResultReceiver)
                intent.putExtra(DropBoxIntent.EXTRA_TASK, task)
                intent.putExtra(DropBoxIntent.EXTRA_MODE, mode)
                intent.putExtra(
                    DropBoxIntent.EXTRA_REF_OVERALL_PROGRESS_INDICATOR,
                    overallProgressIndicator
                )
                intent.putExtra(DropBoxIntent.EXTRA_FILE_NAMES_WITH_ENDING, fileNameWithEnding)
                Helper.log("DropBoxIntent will be started")
                context.startService(intent)
            }
        })
    }

    fun onResume(onTaskDone: () -> Unit = {}) {

        Helper.log(
            DROPBOX_DEBUG_TAG,
            "onResume triggered for DropboxDialogFragment: stayIdle = $stayIdle"
        )
        if (!stayIdle) {
            Helper.log(
                DROPBOX_DEBUG_TAG,
                "################################# Starting DropboxDialogFragment DropboxTask onResume"
            )
            val prefs = PrefsHelper(context)
            var credential = prefs.dropBoxCredential
            if (credential == null) {
                Helper.log(
                    DROPBOX_DEBUG_TAG,
                    "DbxCredential was null. Getting credential from Auth"
                )
                credential = Auth.getDbxCredential()
                Helper.log(
                    DROPBOX_DEBUG_TAG,
                    "storing Dbx credentials: $credential"
                )
                prefs.dropBoxCredential = credential
            }
            credential?.let {
                DropboxClientFactory.init(credential.accessToken)
                Helper.log( "Doing Dropbox Stuff")
                doTask()
                doTask = {}
                stayIdle = true
                onTaskDone()
            }
            if (credential == null) {
                Helper.log(
                    DROPBOX_DEBUG_TAG,
                    "credentials where null in onResume, so NOT doing Dropbox Stuff"
                )
            }
        } else {
            Helper.log(DROPBOX_DEBUG_TAG, "dropbox staying idle")
        }
    }

    companion object {
        val DROPBOX_DEBUG_TAG = "Dropbox"
    }
}