package com.mtths.chartracker.dropbox

import android.app.IntentService
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import android.widget.Toast
import com.dropbox.core.DbxException
import com.dropbox.core.v2.DbxClientV2
import com.dropbox.core.v2.files.WriteMode
import com.mtths.chartracker.activities.ActivityMain.ProgressPublisher
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.DatabaseHelper.Companion.EXPORT_CURRENT_SESSIONS
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.Global
import com.mtths.chartracker.utils.FileUtils.createFileAndAllParentDirs
import com.mtths.chartracker.utils.Helper.getAppDirInternalStorage
import com.mtths.chartracker.utils.Helper.getCharIconsFolderPath
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import java.io.*

/**
 * Save the Database of the app to dropbox
 */
class DropBoxIntent : IntentService(TAG), ProgressPublisher {
    lateinit var appDbPath: String
    lateinit var appInternalDir: String
    lateinit var dropBoxPath: String
    lateinit var localPath: String
    var sessionName: String? = null
    private lateinit var mDbxClient: DbxClientV2
    var dbHelper: DatabaseHelper? = null
    lateinit var prefs: PrefsHelper
    lateinit var reply: ResultReceiver

    @Deprecated("Deprecated in Java")
    override fun onCreate() {
        super.onCreate()
        appInternalDir = getAppDirInternalStorage(baseContext)
        appDbPath = "$appInternalDir/databases/database"
        dbHelper = getInstance(this)
        prefs = PrefsHelper(this)
        log("Dropbox Session will be initiated")
        /*
         * init Dropbox Session
         */

        mDbxClient = DropboxClientFactory.client!!
    }

    @Deprecated("Deprecated in Java")
    override fun onHandleIntent(intent: Intent?) {
        intent?.let {
            val task = intent.getIntExtra(EXTRA_TASK, -1)
            val mode = intent.getIntExtra(EXTRA_MODE, -1)

            /*
            this will return default fileName when there was more than one filename passed
             */
            var fileNameWithoutEnding = getFileNameWithoutEnding(intent)
            val fileNamesWithEnding = intent.getStringArrayExtra(EXTRA_FILE_NAMES_WITH_ENDING)

            log("AAA", "fileNameNoEnding passed to DropboxIntent: $fileNameWithoutEnding")
            var msg = ""
            fileNamesWithEnding?.forEach {
                msg += "$it, "
            }
            log("AAA", "fileNameWithEnding passed to DropboxIntent: ${msg.dropLast(2)}")




            reply = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER)!!
            when (mode) {
                MODE_XML -> {
                    localPath = "$appInternalDir/databases/tmp"
                    dropBoxPath = "/$fileNameWithoutEnding.xml"

                }
                MODE_DB -> {
                    localPath = appDbPath
                    dropBoxPath = "/database"
                }
                MODE_CHARLOG_AS_TEXT, MODE_CHARLOG_AS_XML -> {
                    val charName = getFileNameWithoutEnding(intent, "some_character")
                    localPath = "$appInternalDir/$charName.txt"
                    dropBoxPath = "/${charName}_log.txt"
                }
                MODE_CHAR_ICON -> {
                    localPath = "${getCharIconsFolderPath(appInternalDir)}/"
                    dropBoxPath = "$DROP_BOX_CHAR_ICONS_PATH/"
                }
            }
            if (task == TASK_UPLOAD) {
                when {
                    mode == MODE_XML && prefs.xmlExport == EXPORT_CURRENT_SESSIONS ->
                        uploadFileToDropbox(
                                mode = mode,
                                name = getFileNameWithoutEnding(intent, default_file_name = ""))
                    mode == MODE_CHARLOG_AS_TEXT || mode == MODE_CHARLOG_AS_XML ->
                        uploadFileToDropbox(
                                mode = mode,
                                name = getFileNameWithoutEnding(intent, default_file_name = "some_character"))
                    mode == MODE_CHAR_ICON ->
                        uploadFilesToDropbox(
                                dropBoxFolderpath = dropBoxPath,
                                localFolderPath = localPath,
                                fileNamesWithEnding = fileNamesWithEnding
                        )
                    else -> uploadFileToDropbox(mode = mode)



                }

            }
            if (task == TASK_DOWNLOAD) {
                when (mode) {
                    MODE_DB -> importDbFromDropbox()
                    MODE_XML -> importXmlFromDropbox(intent.getStringArrayExtra(
                        EXTRA_FILE_NAMES_NO_ENDING
                    ))
                    MODE_CHAR_ICON -> downloadCharIconsFromDropbox(dropBoxFolderpath = dropBoxPath, localFolderPath = localPath, fileNamesWithEnding)
                    else -> downloadFilesFromDropbox(dropBoxFolderpath = dropBoxPath, localFolderPath = localPath, fileNamesWithEnding)

                }


            }
            if (intent.getIntExtra(EXTRA_REF_OVERALL_PROGRESS_INDICATOR, 0) > 0) {

                reply.send(INCREASE_OVERALL_PROGRESS, Bundle())
            }
        }
    }

    /**
     * download files from dropbox.
     * all files are downloaded from dropbox path to local path
     */
    private fun downloadFilesFromDropbox(dropBoxFolderpath: String,
                                         localFolderPath: String,
                                         fileNamesWithEnding: Array<String>? = null) {
        val tag = "downloadFilesFromDropBox"
        val bundleWithTag = Bundle()
        var downloadSuccessFul = true
        bundleWithTag.putString(REF_TAG, tag)
        val sucessfullyDownloadedFilenames = ArrayList<String>()

        if (fileNamesWithEnding == null || fileNamesWithEnding.isEmpty()) {

            toast("No FileNames have been delivered to DropBoxIntent", Toast.LENGTH_LONG)
        } else {
            showProgressBar("downloading ...", tag)

            /*
            for each fileName download corresponding file
             */
            fileNamesWithEnding.forEach { fileNameWithEnding ->


                val dbxPath = "$dropBoxFolderpath$fileNameWithEnding"
                val locPath = "$localFolderPath$fileNameWithEnding"



                val f = File(locPath)
                createFileAndAllParentDirs(f)
                var fos: FileOutputStream? = null
                try {
                    fos = FileOutputStream(f)
                    try {
                        setProgressBarText("downloading $dbxPath...", bundleWithTag)
                        mDbxClient.files().download(dbxPath).download(fos)
                        log("Download of $dbxPath successful")
                        sucessfullyDownloadedFilenames.add(locPath)




                    } catch (e: DbxException) {
                        downloadSuccessFul = false
                        e.printStackTrace()
                        toast(e.message, Toast.LENGTH_LONG)
                    } catch (e: IOException) {
                        downloadSuccessFul = false
                        e.printStackTrace()
                        toast(e.message, Toast.LENGTH_LONG)
                    }
                } catch (e: FileNotFoundException) {
                    downloadSuccessFul = false
                    e.printStackTrace()
                    toast(e.message, Toast.LENGTH_LONG)
                    log("Download failed")
                }
                finally {
                    fos?.close()
                }
            }


            reply.send(HIDE_PROGRESS_BAR, bundleWithTag)

            if (downloadSuccessFul) {
                var msg = "Download successful"
                sucessfullyDownloadedFilenames.forEach{msg += " \n$it"}
                toast(msg, Toast.LENGTH_SHORT)
                bundleWithTag.putStringArrayList("path", sucessfullyDownloadedFilenames)
            }
        }
    }

    private fun downloadCharIconsFromDropbox(dropBoxFolderpath: String,
                                             localFolderPath: String,
                                             fileNamesWithEnding: Array<String>? = null) {
        downloadFilesFromDropbox(dropBoxFolderpath, localFolderPath, fileNamesWithEnding)
        reply.send(LOAD_INITIAL_DATA_AND_REFRESH_DISPLAYS, Bundle())

    }

    private fun importXmlFromDropbox(fileNamesNoEnding: Array<String>? = null) {
        val tag = "importFromDropBox"
        val bundleWithTag = Bundle()
        var downloadSuccessFul = true
        bundleWithTag.putString(REF_TAG, tag)
        val sucessfullyDownloadedFilenames = ArrayList<String>()

        if (fileNamesNoEnding == null || fileNamesNoEnding.isEmpty()) {

            toast("No FileNames have been delivered to DropBoxIntent", Toast.LENGTH_LONG)
        } else {
            showProgressBar("downloading ...", tag)

            /*
            for each fileName download corresponding file
             */
            fileNamesNoEnding.forEach { fileNameNoEnding ->

                val fileName = "${fileNameNoEnding.substringBeforeLast(".")}.xml"
                val dropBoxPath = "/$fileName"
                val localPath = "$appInternalDir/databases/$fileName"



                val f = File(localPath)
                val fos: FileOutputStream?
                try {
                    fos = FileOutputStream(f)
                    try {
                        setProgressBarText("downloading $dropBoxPath...", bundleWithTag)
                        mDbxClient.files().download(dropBoxPath).download(fos)
                        log("Download of $dropBoxPath successful")
                        sucessfullyDownloadedFilenames.add(localPath)




                    } catch (e: DbxException) {
                        downloadSuccessFul = false
                        e.printStackTrace()
                        toast(e.message, Toast.LENGTH_LONG)
                    } catch (e: IOException) {
                        downloadSuccessFul = false
                        e.printStackTrace()
                        toast(e.message, Toast.LENGTH_LONG)
                    }
                } catch (e: FileNotFoundException) {
                    downloadSuccessFul = false
                    e.printStackTrace()
                    toast(e.message, Toast.LENGTH_LONG)
                    log("Download failed")
                }
            }

            reply.send(HIDE_PROGRESS_BAR, bundleWithTag)

            if (downloadSuccessFul) {
                toast("Download successful", Toast.LENGTH_SHORT)
                bundleWithTag.putStringArrayList("path", sucessfullyDownloadedFilenames)
                reply.send(IMPORT_FROM_XML, bundleWithTag)
            }
        }
    }

    private fun importDbFromDropbox() {
        val tag = dropBoxPath
        val bundleWithTag = Bundle()
        bundleWithTag.putString(REF_TAG, tag)
        showProgressBar(getString(R.string.msg_downloading_path, dropBoxPath), tag)
        val f = File(localPath)
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(f)
            try {
                mDbxClient.files().download(dropBoxPath).download(fos)
                log("Download successful")
                reply.send(HIDE_PROGRESS_BAR, bundleWithTag)
                toast(getString(R.string.msg_download_successful), Toast.LENGTH_SHORT)
                reply.send(LOAD_INITIAL_DATA_AND_REFRESH_DISPLAYS, bundleWithTag)

            } catch (e: DbxException) {
                e.printStackTrace()
                toast(e.message, Toast.LENGTH_LONG)
            } catch (e: IOException) {
                e.printStackTrace()
                toast(e.message, Toast.LENGTH_LONG)
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            toast(e.message, Toast.LENGTH_LONG)
            log("Download failed")
        }
        finally {
            fos?.close()
        }
    }


    /**
     * variable sessionName is only needed to deliver the fileName when
     * EXPORT_CURRENT_SESSIONS option is chose as export option
     */
    private fun uploadFileToDropbox(mode: Int, name: String = "") {
        log("BBB", "upload Files to Dropbox: name = $name")
        val uploadSuccessful: Boolean
        var file = File(localPath)
        /*
        the tag is used to identify the corresponding progressbar displaying the upload progress
        as tag we will use dropbox path. here we have only one file so, it doesn't change and we
        only have one progress bar to reference
         */
        val tag = dropBoxPath

        showProgressBar(getString(R.string.msg_loading_logs_from_database), tag)
        when (mode) {
            MODE_XML -> {
                var exportString: String? = null

                when (prefs.xmlExport) {
                    DatabaseHelper.EXPORT_ALL_LOGS ->
                        exportString = dbHelper?.getCompleteXMLString(this)?.xmlString
                    DatabaseHelper.EXPORT_CURRENT_LOGS ->
                        exportString = dbHelper?.getCurrentLogsXMLString(this)
                    DatabaseHelper.EXPORT_CURRENT_SESSIONS ->
                        exportString = dbHelper?.getSessionXMLString(name, this)
                }
                exportString?.let { dbHelper?.printStringToFile(exportString, file) }

            }
            MODE_CHARLOG_AS_TEXT -> dbHelper?.getCharacter(name)?.let {
                dbHelper?.writeCharLogToFileAsText(it, file, this)
            }
            MODE_CHARLOG_AS_XML -> {
                val character = dbHelper?.getCharacter(name)
                character?.let { c ->
                    dbHelper?.getCharLogs(c.id)?.let {charLogs ->
                        dbHelper?.writeCharLogToFileAsXml(c, file, charLogs, this)
                    }
                }
                file = File(localPath)
            }
            else -> file = File(localPath)
        }

        val msg = this.getString(R.string.msg_uploading_file_to_path, dropBoxPath)
        val bundleWithTag = Bundle()
        bundleWithTag.putString(REF_TAG, tag)
        setProgressBarText(msg, bundleWithTag)

        uploadSuccessful = saveFileToDropbox(file, dropBoxPath)
        if (uploadSuccessful) {
            reply.send(UPLOAD_SUCCESSFUL, bundleWithTag)
            log("upload successful")
        } else {
            reply.send(UPLOAD_FAILED, bundleWithTag)
            log("upload failed")
        }
        reply.send(HIDE_PROGRESS_BAR, bundleWithTag)
    }

    /**
     * simply upload some file to Dropbox
     */
    private fun uploadFilesToDropbox(dropBoxFolderpath: String,
                                     localFolderPath: String,
                                     fileNamesWithEnding: Array<String>? = null) {
        val tag = "uploadFilesToDropBox"
        val bundleWithTag = Bundle()
        var uploadSuccessful = true
        bundleWithTag.putString(REF_TAG, tag)
        val sucessfullyUploaded = ArrayList<String>()

        if (fileNamesWithEnding.isNullOrEmpty()) {

            toast("No FileNames have been delivered to DropBoxIntent", Toast.LENGTH_LONG)
        } else {
            showProgressBar(getString(R.string.uploading), tag)

            /*
            for each fileName download corresponding file
             */
            fileNamesWithEnding.forEach { fileNameWithEnding ->


                val dbxPath = "$dropBoxFolderpath$fileNameWithEnding"
                val locPath = "$localFolderPath$fileNameWithEnding"
                log("ICON_STUFF", "uploading to: $dbxPath")

                val msg = this.getString(R.string.msg_uploading_file_to_path, dbxPath)
                setProgressBarText(msg, bundleWithTag)
                val file = File(locPath)

                val thisUploadSuccessfull = saveFileToDropbox(file, dbxPath)
                uploadSuccessful = uploadSuccessful &&  thisUploadSuccessfull
                if ( thisUploadSuccessfull) sucessfullyUploaded.add(fileNameWithEnding)
            }
        }

        if (uploadSuccessful) {
            var msg = ""
            sucessfullyUploaded.forEach { msg += "$it\n" }
            bundleWithTag.putString(REF_MSG, msg.dropLast(1))
            reply.send(UPLOAD_SUCCESSFUL, bundleWithTag)
            log("upload successful")
        } else {
            reply.send(UPLOAD_FAILED, bundleWithTag)
            log("upload failed")
        }
        reply.send(HIDE_PROGRESS_BAR, bundleWithTag)
    }

    /**
     * @param file
     * @param dropboxPath where your file will be stored in the dropbox
     * @return true if successfully uploaded, false if some error occurred;
     */
    private fun saveFileToDropbox(file: File, dropboxPath: String): Boolean {
        var fis: FileInputStream
        try {
            fis = FileInputStream(file)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            toast(e.message, Toast.LENGTH_LONG)
            return false
        }
        try {
            log("trying to put file to dropbox")
            mDbxClient.files().uploadBuilder(dropboxPath).withMode(WriteMode.OVERWRITE).uploadAndFinish(fis)
        } catch (e: DbxException) {
            e.printStackTrace()
            toast(e.message, Toast.LENGTH_LONG)
            return false
        } catch (e: IOException) {
            e.printStackTrace()
            toast(e.message, Toast.LENGTH_LONG)
            return false
        }
        finally {
            fis.close()
        }
        return true
    }

    private fun toast(msg: String?, length: Int) {
        val b = Bundle(1)
        b.putString(REF_MSG, msg)
        b.putInt(REF_TOAST_LENGTH, length)
        reply.send(MAKE_TOAST, b)
    }

    private fun showProgressBar(msg: String, tag: String) {
        val b = Bundle(2)
        b.putString(REF_MSG, msg)
        b.putString(REF_TAG, tag)
        reply.send(SHOW_PROGRESS_BAR, b)
        reply.send(SET_PROGRESSBAR_TEXT, b)
    }

    /**
     * return fileName from Intent or default file name if no fileName was delivered
     */
    private fun getFileNameWithoutEnding(intent: Intent?, default_file_name: String = "db") : String {
        var fileNames = intent?.getStringArrayExtra(EXTRA_FILE_NAMES_NO_ENDING)
        if (fileNames.isNullOrEmpty()) {
            log("fileNames passed to DropboxIntent are null or empty")
        } else {
            return fileNames[0].substringBeforeLast(".")
        }

        return default_file_name
    }

    override fun doProgress(values: Array<Int>) {
        val b = Bundle()
        b.putString(REF_TAG, dropBoxPath)
        values[0].let{b.putInt(REF_PROGRESS, it)}
        if (values.size > 1) {
            values[1].let{ b.putInt(REF_MAX_PROGRESS, it)}
        }
        reply.send(DO_PROGRESS, b)
    }

    fun setProgressBarText(msg: String, bundle: Bundle) {
        bundle.putString(REF_MSG, msg)
        reply.send(SET_PROGRESSBAR_TEXT, bundle)
    }

    override fun display(msg: String) {
        val b = Bundle(2)
        b.putString(REF_MSG, msg)
        b.putString(REF_TAG, dropBoxPath)
        reply.send(SET_PROGRESSBAR_TEXT, b)
    }

    companion object {
        private val TAG = DropBoxIntent::class.java.simpleName
        const val EXTRA_RESULT_RECEIVER = "result_receiver"
        const val EXTRA_TASK = "task"
        const val EXTRA_MODE = "mode"
        const val EXTRA_FILE_NAMES_NO_ENDING = "file_names_no_ending"
        const val EXTRA_FILE_NAMES_WITH_ENDING = "file_names_with_ending"
        const val EXTRA_REF_OVERALL_PROGRESS_INDICATOR = "overall_progress_indicator"

        const val OVERALL_PROGRESS_INDICATOR_TAG = "overall_progress"

        const val DROP_BOX_CHAR_ICONS_PATH = "/${Global.ICONS_FOLDER}"

        const val TASK_UPLOAD = 0
        const val TASK_DOWNLOAD = 1

        const val MODE_XML = 0
        const val MODE_DB = 1
        const val MODE_CHARLOG_AS_TEXT = 2
        const val MODE_CHARLOG_AS_XML = 3

        /**
         * a filename has to be given to the intent.
         * It should be the name of the file of the char Icon
         */
        const val MODE_CHAR_ICON = 4
        //result Codes
        const val UPLOAD_SUCCESSFUL = 0

        const val UPLOAD_FAILED = 1
        const val IMPORT_FROM_XML = 2
        const val MAKE_TOAST = 3
        const val DO_PROGRESS = 4
        const val SHOW_PROGRESS_DIALOG = 5
        const val CREATING_FILE_SUCCESSFUL = 7
        const val LOAD_INITIAL_DATA_AND_REFRESH_DISPLAYS = 8
        const val SHOW_PROGRESS_BAR = 9
        const val HIDE_PROGRESS_BAR = 10
        const val SET_PROGRESSBAR_TEXT = 11
        const val INCREASE_OVERALL_PROGRESS = 12
        const val RESULT_CODE_DISMISS_DATA_OPTIONS = 13


        /**
         * we always use dropBoxPath as tag. I think this should be enought to distignuish
         * different uploads that may happen simultaneously, e.g. when I upload a file
         * for each active session
         */
        const val REF_TAG = "tag"
        const val REF_MSG = "msg"
        const val REF_TOAST_LENGTH = "lenght"
        const val REF_MAX_PROGRESS = "value2"
        const val REF_PROGRESS = "value1"
    }
}