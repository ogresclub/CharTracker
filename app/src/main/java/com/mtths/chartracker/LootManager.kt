package com.mtths.chartracker

import android.content.Context
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.LmAuth
import kotlinx.coroutines.*
import java.lang.Exception

class LootManager(val context: Context?) {

    private var stayIdle = true
    private val defaultDoNetwork = { "doint nothing in network" }
    private val defaultOnFinish = { _: String ->}
    private var mDoNetwork: () -> String = defaultDoNetwork
    private var mOnFinish: (String) -> Unit = defaultOnFinish


    fun doLootManagerStuff(doNetwork: () -> String, onFinish: (String) -> Unit) {
        val prefs = PrefsHelper(context)
        val token = prefs.lootmanagerToken

        Helper.log(
            DEBUG_TAG,
            "doLmStuff called: context = $context, token = $token"
        )

        if (token == null) {
            prepareAuth(doNetwork, onFinish)
            startAuth()
        } else {
            CoroutineScope(Dispatchers.IO).launch {
                val response = doNetwork()
                withContext(Dispatchers.Main) {
                    if(handleInvalidToken(response, doNetwork, onFinish)) {
                        onFinish(response)
                    }
                }
            }
        }
    }
    
    fun onResume() {
        Helper.log(DEBUG_TAG, "onResume triggered for LootmanagerFragment:")
        if (!stayIdle) {
            Helper.log(
                DEBUG_TAG,
                "############### Starting Lootmanager Task onResume"
            )
            val prefs = PrefsHelper(context)
            val token = prefs.lootmanagerToken
            token?.let {
                Helper.log(DEBUG_TAG, "Doing Lootmanager Stuff")

                CoroutineScope(Dispatchers.IO).launch {
                    val response = mDoNetwork()
                    withContext(Dispatchers.Main) {
                        mOnFinish(response)
                        mDoNetwork = defaultDoNetwork
                        mOnFinish = defaultOnFinish
                    }
                }
                stayIdle = true

            }
            if (token == null) {
                Helper.log(
                    DEBUG_TAG,
                    "lootmanager token where null in onResume, so NOT doing Dropbox Stuff")
            }
        }
    }

    private fun authTokenIsInvalid(response: String): Boolean {
        return response.contains("Auth-Token")
    }

    /**
     * start Auth (again) if token was invalid
     * return true if token was valid
     */
    fun handleInvalidToken(response: String, doNetwork: () -> String, onFinish: (String) -> Unit): Boolean {
        return if (authTokenIsInvalid(response)) {
            Helper.log(DEBUG_TAG,"token was invalid, requesting new token")
            prepareAuth(doNetwork, onFinish)
            startAuth()
            false
        } else {
            true
        }
    }

    private fun prepareAuth(doNetwork: () -> String, onFinish: (String) -> Unit) {
        /*
        after that on Resume will be called
        we need to define here, what shall be done in OnResume by setting
        */
        stayIdle = false
        mDoNetwork = doNetwork
        mOnFinish = onFinish
        Helper.log(DEBUG_TAG, "token was null. stayIdle = $stayIdle")
        Helper.log(DEBUG_TAG, "Authenticate Lootmanager")
    }

    private fun startAuth() {
        try {
            context?.let {
                LmAuth.startAuth(it)
            }
        } catch (e: Exception) {
            Helper.log(DEBUG_TAG, "failed starting Auth for Lootmanager: ${e.message}")
        }
    }
    
    companion object {
        const val DEBUG_TAG = "LOOTMANAGER_FRAGMENT"
    }
}