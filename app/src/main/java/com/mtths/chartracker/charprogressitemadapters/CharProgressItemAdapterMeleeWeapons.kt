package com.mtths.chartracker.charprogressitemadapters

import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.Buff

import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.Skill
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.interfaces.CPIDSelfBuffable
import com.mtths.chartracker.interfaces.Moddable
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.SnackBarUtils
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getIntegerStatWithExtraCharsFromText
import com.mtths.chartracker.utils.StringUtils.getIntegerStatWithModificationFromText
import com.mtths.chartracker.utils.StringUtils.getWeaponDamageFromText
import kotlin.collections.ArrayList

class CharProgressItemAdapterMeleeWeapons (activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapterOneLineExpandable<CharProgressItemAdapterMeleeWeapons.MeleeWeapon>(
        activity = activity,
        colHeaderIds = listOf(
            REF_ID_CHECK,
            REF_ID_ATTACK,
            REF_ID_DAMAGE
        ),
        colWeights = listOf(
            1f, 2f, 1f
        )
    ) {


    override fun createAutoCompleteStrings(): ArrayList<String> {

        return ArrayList(
            super.createAutoCompleteStrings().apply {
                addAll(
                    mutableListOf(
                        REF_ID_DAMAGE,
                        REF_ID_ATTACK
                    ).apply {
                        if (rulesSR5) {
                            addAll(
                                listOf(
                                    REF_ID_ACCURACY,
                                    REF_ID_AP,
                                    REF_ID_REACH
                                )
                            )
                        }
                        if (rulesSR6) {
                            addAll(
                                listOf(
                                    REF_ID_ATTACK
                                )
                            )
                        }
                    }.map {
                        "${context.getString(it)} = "
                    }
                )
                val mW = MeleeWeapon()
                mW.addModsAutoCompleteString(this)
                mW.addSelfBuffAutoCompleteString(this, context)


            }.map {
                it.lowercase()
            }
        )
    }



    override fun generate_autocomplete_data(e: LogEntry) {

    }

    override fun add(e: LogEntry): LogEntry {

        val textAfterFilterConstraint = Helper.cutAfterString(e.text, item!!.filterConstraint)
        val meleeWeapon = MeleeWeapon()
        meleeWeapon.relatedLogEntry = e
        meleeWeapon.parseMods(textAfterFilterConstraint)
        meleeWeapon.parseSelfBuff(context)
        meleeWeapon.parseName(logEntryText = e.text, item = item)
        meleeWeapon.dmg = getWeaponDamageFromText(context = context)(REF_ID_DAMAGE, e.text)
        meleeWeapon.acc = getIntegerStatWithModificationFromText(context = context)(
            REF_ID_ACCURACY, e.text)
        meleeWeapon.ap = getIntegerStatFromText(
            statId = REF_ID_AP, text =  e.text, context = context)
        meleeWeapon.reach = getIntegerStatFromText(
            statId = REF_ID_REACH, text =  e.text, context = context)
        meleeWeapon.att = getIntegerStatWithExtraCharsFromText(
            statId = REF_ID_ATTACK, text =  e.text, extra_chars = "/-", context = context)
        data.add(meleeWeapon)
        checkExpand(meleeWeapon)
        return e
    }

    override fun inflate(expand: Boolean): View? {
        return super.inflate(expand).apply {
            if (rulesSR5) {
                val label =  R.string.accuracy_label
                renameStat0ColumnHeader(
                    nameId = label,
                    containerView = this,
                    context = context
                )
            }
        }
    }

    override fun inflateView(index: Int, check_mistakes: Boolean): ViewHolder {
        return super.inflateView(index, check_mistakes).apply {

            val meleeWeapon = data[index]

            nameView?.text = meleeWeapon.name

            dataViews?.get(0)?.apply {
                text = meleeWeapon.check.toString()
                setOnClickListener(getShowSkillCheckCalculationOnClickListener(meleeWeapon))
            }

            dataViews?.get(1)?.apply {
                text = meleeWeapon.att
                if (rulesSR5) {
                    text = meleeWeapon.acc
                }
            }
            dataViews?.get(2)?.apply {
                text = meleeWeapon.dmg
            }
            implementExpand(view = root, data = meleeWeapon)
        }
    }

    private fun getShowSkillCheckCalculationOnClickListener(
        weapon: MeleeWeapon
    ): View.OnClickListener = View.OnClickListener { v ->
        item?.character?.let { c ->
            c.ruleSystem?.getSkillDetailsBuilder?.invoke(
                c,
                ValueNamePair(
                    name = meleeCombatSkillName,
                    skill = Skill.getSkill(
                        name = meleeCombatSkillName,
                        character = c,
                        skillData = Skill.getSkillData(context)
                    ),
                    mCalculator = object : CharProgressItemAdapterValueBased.Calculator {}
                ),
                context
            )?.msgFormatTotal?.format(weapon.check) +
                    weapon.buffCalculationString

        }?.let { msg ->
            SnackBarUtils.showSnackBar(
                view = v,
                msg = msg,
                duration = Snackbar.LENGTH_LONG,
                context = this.mContext
            )
        }

    }

    val meleeCombatSkillName: String
        get() = context.getString(R.string.shadowrun6_skill_melee_combat)

    inner class MeleeWeapon :
        CharProgressItemData(),
        Moddable,
        CPIDSelfBuffable {
        override val mods: MutableList<String> = mutableListOf()
        var dmg: String
        var acc: String
        var ap: String
        var reach: String
        var att: String
        override val _REF_MOD: String? = null // means is used DEFAULT REF_MOD
        override var buff: Buff = Buff()

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                if (rulesSR5) {
                    add(ExpandData(
                        headerId = R.string.ap_label,
                        text = ap))
                    add(ExpandData(
                        headerId = R.string.reach_label,
                        text = reach))

                }
                addAll(modsExpandData)
            }

        val check: Int
            get() = getSkillValue(R.string.shadowrun6_skill_melee_combat) + totalBuffValue


        init {
            name = "My Weapon"
            dmg = "-"
            acc = "-"
            ap = "-"
            reach = "-"
            att = "-"
        }
    }

    companion object {
        val REF_ID_CHECK = R.string.label_check
        val REF_ID_ATTACK = R.string.attack_label
        val REF_ID_DAMAGE = R.string.damage_label
        val REF_ID_AP = R.string.ap_label
        val REF_ID_ACCURACY = R.string.accuracy_label
        val REF_ID_REACH = R.string.reach_label
    }
}