package com.mtths.chartracker.charprogressitemadapters

import android.widget.LinearLayout
import android.view.LayoutInflater
import android.widget.TextView
import com.mtths.chartracker.R
import android.view.Gravity
import android.view.View
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import java.util.Comparator

/**
 * same as Std, but no karma is displayed
 */
open class CharProgressItemAdapterPlain(
    activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<StdEntry>
) : CharProgressItemAdapterStd(activity, comparator) {

    override val layoutResourceEntry = R.layout.char_progress_list_item_std

    override val layoutResourceMain = R.layout.char_progress_item_container_bordered
    override fun getView(index: Int, check_mistakes: Boolean): View? {
        //inflate entry
        val entryData = data[index]
        val entry =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false) as LinearLayout
        val numberView = entry.findViewById<TextView>(R.id.number)
        val textView = entry.findViewById<TextView>(R.id.text)
        textView.gravity = Gravity.CENTER
        numberView.visibility = View.GONE
        textView.text = entryData.right_text
        entry.setOnLongClickListener(
            ListenerImplOpenEditLogsOnLongClick(
                mActivity!!.supportFragmentManager,
                entryData.relatedLogEntry
            )
        )
        return entry
    }
}