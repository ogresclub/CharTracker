package com.mtths.chartracker.charprogressitemadapters

import android.content.Context
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.interfaces.CPIDescriptive
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.StringUtils.getStatUntilEndByCommaFromText
import java.util.*

open class CharProgressItemAdapterSpellSR(
    activity: ProgressBarFrameDropBoxActivity?,
    colHeaderIds: List<Int> = listOf(REF_ID_DRAIN),
    val useComplexForms: Boolean = false
) :
    CharProgressItemAdapterOneLineExpandable<CharProgressItemAdapterSpellSR.SpellSR>(
        activity = activity,
        comparator = compareBy { it.name.lowercase() },
        colHeaderIds = colHeaderIds,
    )  {

    override fun createAutoCompleteStrings(): ArrayList<String> {
        return ArrayList(
            listOf(
                REF_ID_CATEGORY,
                REF_ID_TYPE,
                REF_ID_RANGE,
                REF_ID_DAMAGE,
                REF_ID_DURATION,
                REF_ID_DRAIN).map {
                    "${context.getString(it).lowercase()} = "
            }
        ).apply {
            addAll(
                listOf(REF_ID_DESCRIPTION).map {
                    "${context.getString(it).lowercase()} = ["
                }
            )
        }
    }

    override fun generate_autocomplete_data(e: LogEntry) {
    }

    override fun add(e: LogEntry): LogEntry {
        val spell = SpellSR(
            item = item,
            context = context,
            complexForm = useComplexForms
        )
        spell.relatedLogEntry = e
        spell.parseName(logEntryText = e.text, item = item)
        spell.parseDescription(text = e.text, context = context)
        spell.category = getStatUntilEndByCommaFromText(
            statId = REF_ID_CATEGORY,
            text = e.text,
            context = context)
        spell.type = getStatUntilEndByCommaFromText(
            statId = REF_ID_TYPE,
            text =  e.text,
            context = context)
        spell.range = getStatUntilEndByCommaFromText(
            statId = REF_ID_RANGE,
            text =  e.text,
            context = context)
        spell.damage = getStatUntilEndByCommaFromText(
            statId = REF_ID_DAMAGE,
            text =  e.text,
            context = context)
        spell.duration = getStatUntilEndByCommaFromText(
            statId = REF_ID_DURATION,
            text =  e.text,
            context = context)
        spell.drain = getStatUntilEndByCommaFromText(
            statId = REF_ID_DRAIN,
            text =  e.text,
            context = context)
        data.add(spell)
        checkExpand(spell)
        return e
    }

    open fun inflateAdditionalData(spell: SpellSR, check_mistakes: Boolean, holder: ViewHolder) {

    }

    override fun inflateView(index: Int, check_mistakes: Boolean): ViewHolder {
        return super.inflateView(index, check_mistakes).apply {
            val spell = data[index]

            nameView?.apply {
                text = getDisplayString(spell)
                setOnLongClickListener(
                    ListenerImplOpenEditLogsOnLongClick(
                        mActivity!!.supportFragmentManager,
                        spell.relatedLogEntry
                    )
                )
            }
            dataViews?.get(0)?.apply {
                text = spell.drain
            }
            inflateAdditionalData(spell = spell, holder = this, check_mistakes = check_mistakes)

            implementExpand(view = root, data = spell)

        }
    }

    override fun inflate(expand: Boolean): View? {

        val container = super.inflate(expand)

        if (!useComplexForms) {
            container?.findViewById<TextView>(R.id.name_label)?.setOnClickListener {
                item?.character?.let { c ->
                    c.toggleShowSpellSchools()
                    DatabaseHelper.getInstance(context)?.updateCharacter(
                        character = c, keep_last_update_date = true
                    )
                    updateAllDisplays()
                }
            }
        }
        return container
    }

    fun getDisplayString(spell: SpellSR): SpannableStringBuilder {


        return SpannableStringBuilder(Helper.capitalizeNewsLetterStyle(spell.name.lowercase())).also { sb ->
            if (item?.character?.showSpellSchools == true && !useComplexForms) {

                spell.category?.let {cat ->
                    sb.append(" [")
                    Helper.append(
                        sb = sb,
                        text = Helper.capitalize(cat),
                        what = StyleSpan(Typeface.ITALIC),
                        flags = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    sb.append("]")
                }


            }
        }

    }

    open class SpellSR(
        val item: CharProgressItem?,
        val context: Context,
        val complexForm: Boolean) : CharProgressItemData(), CPIDescriptive {
        var category: String? = null
        var type: String? = null
        var range: String? = null
        var damage: String? = null
        var duration: String? = null
        var drain: String? = null
        override var description: String = ""

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                if (!complexForm) {
                    if (item?.character?.showSpellSchools != true) {
                        add(
                            ExpandData(
                                header = context.getString(REF_ID_CATEGORY),
                                text = category ?: "-"
                            )
                        )
                    }
                    add(ExpandData(header = context.getString(REF_ID_TYPE), text = type ?: "-"))
                    if (damage != null && damage != "-") {
                        add(
                            ExpandData(
                                header = context.getString(REF_ID_DAMAGE),
                                text = damage ?: "-"
                            )
                        )
                    }
                    if (range != null && range != "-") {
                        add(
                            ExpandData(
                                header = context.getString(REF_ID_RANGE),
                                text = range ?: "-"
                            )
                        )
                    }
                    add(
                        ExpandData(
                            header = context.getString(REF_ID_DURATION),
                            text = duration ?: "-"
                        )
                    )
                }
                if (description.isNotBlank()) {
                    add(ExpandData(header = context.getString(REF_ID_DESCRIPTION), text = description))
                }
            }
    }

    companion object {
        val REF_ID_CATEGORY = R.string.category_label
        val REF_ID_TYPE = R.string.type_label
        val REF_ID_RANGE = R.string.range_label
        val REF_ID_DAMAGE = R.string.damage_label
        val REF_ID_DURATION = R.string.duration_label
        val REF_ID_DRAIN = R.string.drain_label
        val REF_ID_DESCRIPTION = R.string.description_label
    }
}