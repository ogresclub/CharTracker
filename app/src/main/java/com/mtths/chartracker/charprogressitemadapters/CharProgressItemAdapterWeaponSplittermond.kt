package com.mtths.chartracker.charprogressitemadapters

import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSkills.Companion.getAttrDisplayStyle
import com.mtths.chartracker.dataclasses.Buff
import com.mtths.chartracker.dataclasses.BuffEffect
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.Skill
import com.mtths.chartracker.dataclasses.SpellDnD
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.interfaces.CPIDSelfBuffable
import com.mtths.chartracker.interfaces.Moddable
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.HttpUtils
import com.mtths.chartracker.utils.SnackBarUtils
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getListFromText
import com.mtths.chartracker.utils.StringUtils.getStatFromText
import com.pedromassango.doubleclick.DoubleClick
import com.pedromassango.doubleclick.DoubleClickListener
import java.util.ArrayList

class CharProgressItemAdapterWeaponSplittermond (activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapterOneLineExpandable<CharProgressItemAdapterWeaponSplittermond.Weapon>(
        colHeaderIds = listOf(
            R.string.damage_label_short,
            R.string.splittermond_attack_speed_label,
            R.string.attack_label),
        colWeights = listOf(1.5f, 1f, 1f),
        activity = activity,
        comparator = compareBy { it.name }
    ),
    CharProgressItemAdapterBuffable {


    override fun createAutoCompleteStrings(): ArrayList<String> {
        return arrayListOf(
            "$REF_ATTRIBUTES = [",
            "$REF_WGS  = ",
            "$REF_SKILL  = ",
            "$REF_DAMAGE  = ",
        ).also {
            addBuffAutoCompleteString(it)
            val w = Weapon()
            w.addModsAutoCompleteString(it)
            w.addSelfBuffAutoCompleteString(it, context)
        }


    }

    override fun generate_autocomplete_data(e: LogEntry) {
    }

    override fun add(e: LogEntry): LogEntry {
//        Log.e("BUFFF_ARMOR", "add armor called for log: ${e.text}")

        //first entry is karma, second is text
        val armor = Weapon()
        armor.relatedLogEntry = e
        armor.parseSelfBuff(context)

        /*
        set name
         */
        armor.parseName(e.text, item)
        armor.parseAttributes(e.text)
        armor.parseWGS(e.text)
        armor.parseRange(e.text)
        armor.parseSkill(e.text)
        armor.parseDamage(e.text)

        data.add(armor)
        armor.parseMods(e.text)
        createBuffs(s = e.text, iData = armor, item = item, context = context)
        checkExpand(armor)
        return e
    }

    override fun inflateView(index: Int, check_mistakes: Boolean): ViewHolder {
        val h = super.inflateView(index = index, check_mistakes = check_mistakes)

        val weapon: Weapon = data[index]

        h.nameView?.text = weapon.name
        h.dataViews?.getOrNull(0)?.text = weapon.damage
        h.dataViews?.getOrNull(1)?.text = weapon.getTotalWGS()?.toString() ?: "-"
        h.dataViews?.getOrNull(2)?.apply {
            setOnClickListener(getShowSkillCheckCalculationOnClickListener(weapon))
            text = weapon.getAttackValue().toString()
        }

        implementExpand(view = h.root, data = weapon)

        h.root?.setOnClickListener(DoubleClick(
            object : DoubleClickListener {
                override fun onDoubleClick(view: View?) {
                    mActivity?.let {
                        HttpUtils.openWebPage(
                            url = weapon.getLink(),
                            activity = it
                        )
                    }
                }

                override fun onSingleClick(view: View?) {}
            }
        ))



        mActivity?.let { a ->
            h.root?.setOnLongClickListener(
                ListenerImplOpenEditLogsOnLongClick(
                    a.supportFragmentManager,
                    weapon.relatedLogEntry
                )
            )
        }

        return h
    }

    private fun getShowSkillCheckCalculationOnClickListener(
        weapon: Weapon
    ): View.OnClickListener = View.OnClickListener { v ->
        item?.character?.let { c ->
            weapon.skill?.let { skill ->
                c.ruleSystem?.getSkillDetailsBuilder?.invoke(
                    c,
                    ValueNamePair(
                        name = skill.name,
                        skill = skill,
                        mCalculator = object : CharProgressItemAdapterValueBased.Calculator {}
                    ),
                    context
                )?.msgFormatTotal?.format(weapon.getAttackValue())+
                        weapon.buffCalculationString
            }


        }?.let { msg ->

            SnackBarUtils.showSnackBar(
                view = v,
                msg = msg,
                duration = Snackbar.LENGTH_LONG,
                context = context
            )
        }

    }

    inner class Weapon :
        CharProgressItemData(),
        Moddable,
        CPIDSelfBuffable {
        override val mods: MutableList<String> = mutableListOf()
        override val _REF_MOD: String? = null // means is used DEFAULT REF_MOD
        override var buff = Buff()

        var attributes: List<String>? = null
        var skill: Skill? = null
        var wgs: Int? = null
        var damage: String? = null
        var range: Int? = null

        override val expandData: List<ExpandData>
            get() = mutableListOf(
                ExpandData(
                    header = REF_ATTRIBUTES,
                    text = getAttrDisplayStyle(attributes)?.
                    joinToString(separator = " ")?: "-"),
                ExpandData(
                    header = REF_SKILL,
                    text = skill?.name ?: "-"),
            ).apply {
                if (range != null) add(
                    ExpandData(
                        header = Helper.capitalizeNewsLetterStyle(REF_RANGE),
                        text = range.toString())
                )
                addAll(mods.map { ExpandData(text = it) })
            }

        fun getLink(): String {
            return SpellDnD.SPELL_URL_TEMPLATE_SPLITTERMOND + Helper.getSplittermondRef(name)
        }


        fun parseWGS(s: String) {
            wgs = getIntegerStatFromText(REF_WGS, s).toIntOrNull()
        }
        fun parseRange(s: String) {
            range = getIntegerStatFromText(REF_RANGE, s).toIntOrNull()
        }
        fun parseAttributes(s: String) {
            attributes = getListFromText(
                stat = REF_ATTRIBUTES,
                text = s)
        }

        fun parseSkill(s: String) {
            skill = getStatFromText(REF_SKILL, s)?.let { sk ->
                Skill.getSkill(
                    name = sk,
                    skillData = Skill.getSkillData(context),
                    character = item?.character
                )
            }
        }

        fun getTotalWGS(): Int? {
            return wgs?.let { wgs ->
                wgs + (item?.character?.getStatBuffValueOrZero(BuffEffect.BUFF_REF_TIK) ?: 0)
            }
        }

        fun getAttackValue(): Int {
            return (skill?.let { sk ->
                attributes?.let {
                    sk.attributes = it.joinToString(separator = " ")
                }
                CharProgressItemAdapterValueBased.calcTotalSkillValue(
                    ValueNamePair(
                        name = sk.name,
                        skill = sk,
                        mCalculator = object : CharProgressItemAdapterValueBased.Calculator {}),
                    character = item?.character,
                    context = context
                )
            }?.totalValue ?: -999) + totalBuffValue
        }

        fun parseDamage(s: String) {
            damage = getStatFromText(REF_DAMAGE, s)
        }
    }

    companion object {
        const val DAMAGE_TEXT_VIEW_WEIGHT = 1.5f
        const val DEBUG_TAG = "CHAR_PROG_ADAPTER_ARMORS_SPM"
        const val REF_ATTRIBUTES = "attributes"
        const val REF_WGS = "wgs"
        const val REF_SKILL = "skill"
        const val REF_RANGE = "range"
        const val REF_DAMAGE = "damage"
    }
}

