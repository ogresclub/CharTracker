package com.mtths.chartracker.charprogressitemadapters

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterQuests.Quest
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterStd.Companion.END_MARKER
import com.mtths.chartracker.utils.Helper.getTextAfterStringAndTrim
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.BulletSpanWithRadius
import com.mtths.chartracker.utils.Helper
import java.lang.Exception
import java.util.*

class CharProgressItemAdapterQuests (
    activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapter<Quest>(
        mActivity = activity,
        comparator = DATE_COMPARATOR
    ) {

    override var layoutResourceMain = R.layout.char_progress_item_container_session_stats
    val layoutResourceEntry = R.layout.list_view_item_char_progress_string


    override fun add(e: LogEntry): LogEntry {

        var textAfterFilterConstraint = updateFilterConstraintToCurrentLanguage(e.text)
        while (textAfterFilterConstraint.contains(item!!.filterConstraint)) {
            textAfterFilterConstraint = getTextAfterStringAndTrim(textAfterFilterConstraint, item?.filterConstraint)
            //Eg when we have @Quest Quest1 @Quest Quest2 we want to find both quests
            var text = Helper.getTextBeforeString(
                text = textAfterFilterConstraint, string = item?.filterConstraint
            )


            val quest = Quest(date = e.dateOfCreation)
            quest.relatedLogs.add(e)
            if (text.contains(QUEST_COMPLETE_INDICATOR)) {
                quest.isComplete = true
                text = text.replace(QUEST_COMPLETE_INDICATOR, "")
                    .trim()
            }

            quest.parseName(text)?.let {
                quest.parseDescription(it)
            } ?: run {
                //no double slashes are found so we use old notation @Quest to mark whole log Entry
                //LEGACY ADDING
                quest.name = text
            }

            //search if quest already exists
            getQuest(name = quest.name)?.let { exQuest ->
                /*
               existing quest was found
                */
                //extend short description
                exQuest.shortDescriptions.addAll(quest.shortDescriptions)
                // add newly found related quest
                exQuest.relatedLogs.add(e)
                exQuest.isComplete = exQuest.isComplete || quest.isComplete
            } ?: run {

                // siehe characters adapter: data.add(quest)
                this.data.add(quest)
                auto_complete_data.add(quest)
            }
        }

        return e
    }

    /**
     * return the quest from date with the given name ignoring cases or null if non found
     */
    fun getQuest(name: String): Quest? {
        var quest: Quest? = null
        for (q in data) {
            if (q.name.equals(name, ignoreCase = true)) {
                quest = q
            }
        }
        return quest
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? { //inflate entry
        val questEntry = data[index]
        var entry: TextView? = null
        if (!questEntry.isComplete || PrefsHelper(context).showAllQuestsSessionStats) {
            entry = LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false) as TextView
            val params =
                LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 0, 0, CharProgressItemAdapterCharacter.MARGIN_BOTTOM)
            entry.layoutParams = params
            entry.setBackgroundResource(R.drawable.light_grey_half_transparent_round_corners)
            var s = questEntry.getDisplaySpannable(showDescription = false)
            s.setSpan(
                BulletSpanWithRadius(context.resources.getInteger(R.integer.bullet_radius_quests), 40),
                0, s.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            if (questEntry.isComplete) {
                s.setSpan(StyleSpan(Typeface.ITALIC), 0, s.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                s = Helper.setColor(
                    arrayListOf(s.toString()),
                    s.toString(),
                    ContextCompat.getColor(context, R.color.text_highlight_color_char_progress),
                    s
                )
            }
            entry.text = s
            entry.textSize = 16f
            entry.setTextColor(ContextCompat.getColor(context, R.color.text_color_logs))
            entry.setOnLongClickListener {
                showDialogFilteredLogs(questEntry)
                true
            }
            entry.setOnClickListener {
                mActivity?.supportFragmentManager?.let { fragmentManager ->
                    DialogCustom.newInstance().apply {
                        setTitle(questEntry.getDisplaySpannable(showDescription = true))
                        setMessageOnClickListener {
                            this.dismiss()
                            showDialogFilteredLogs(questEntry)
                        }
                        setPositiveButton("")
                    }.show(fragmentManager, "showSessionData_${questEntry.name}")
                }
            }
        }
        return entry
    }

    fun showDialogFilteredLogs(quest: Quest) {
        mActivity?.let { mActivity ->
            DialogShowFilteredLogs.newInstance(
                use_session_logs = true,
                activity_char_details = false,
                searchString = item?.filterConstraintTranslations?.joinToString(" OR ") { "\"$it ${quest.name}\"" } ?: ""
            ).show(mActivity.supportFragmentManager, "show_quests")
        }

    }

    override fun generate_autocomplete_data(e: LogEntry) {
        add(e)
    }

    override fun createAutoCompleteStrings(): ArrayList<String> {
        val autoCompleteStrings = ArrayList<String>()
        for (q in auto_complete_data) {
            if (!q.isComplete) {
                autoCompleteStrings.add(q.name)
            }
        }
        return autoCompleteStrings
    }

    override fun createAutoCompleteFeatureMarkers(): ArrayList<String> {
        return ArrayList(listOf(QUEST_COMPLETE_INDICATOR))
    }

    inner class Quest(val date: Date?) {
        var name: String = ""
            set(value) {
                field = value.trim()
            }
        var isComplete = false
        var relatedLogs = ArrayList<LogEntry>()
        var shortDescriptions: ArrayList<String> = ArrayList()
        var shortDescription: String = ""
            get() {
                var result = ""
                shortDescriptions.forEach {
                    result += Helper.capitalize(it) + "\n"
                }
                result.trim()
//                log("SESSION_DATA", "short description: '$result'")
                return result.trim()
            }

        /**
         * return the rest of the string after the first double slash
         * or null if no double slash was found
         * splitting into aliases and shorts
         */
        fun parseName(text: String): String? {
            val indexOfDoubleSlash = text.indexOf(END_MARKER)
            name = text
            var result: String? = null

            if (indexOfDoubleSlash > 0) {
                name = text.substring(0, indexOfDoubleSlash)
                result = text.substring(indexOfDoubleSlash + 2)
            } else if (text.isNotBlank()) {
                result = ""
            }
            return result
        }

        fun parseDescription(textAfterFirstEndMarker: String) {
            val indexOfSecondEndMarker = textAfterFirstEndMarker.indexOf(END_MARKER)

            var descr = if (indexOfSecondEndMarker >= 0) {
//                log(DEBUG_TAG, "found second Double Slash:");
                textAfterFirstEndMarker
                    .substring(0, indexOfSecondEndMarker)
                    .trim()
//                    .also { log(DEBUG_TAG, "short description = $it") }

            } else {
                textAfterFirstEndMarker.substringBefore("@")
//                    .also {log(DEBUG_TAG, "NOT found second Double Slash: '$it'"); }
            }
//            log(DEBUG_TAG, "@Character ${name}: $descr")

            if (descr.isNotBlank()) {
                shortDescriptions.add(descr)
            }

        }

        fun getDisplaySpannable(showDescription: Boolean = false): SpannableStringBuilder {

            var sb = SpannableStringBuilder()
            var styles = mutableListOf<Any>()

            if (!showDescription) {
                if (isComplete) {
                    styles.add(StyleSpan(Typeface.ITALIC))
                }
            } else {
                if (isComplete) {
                    styles.add(StyleSpan(Typeface.BOLD_ITALIC))
                } else {
                    styles.add(StyleSpan(Typeface.BOLD))
                }
            }
            Helper.append(sb, name, styles, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

            if (isComplete) {
                sb = Helper.setColor(
                    arrayListOf(sb.toString()),
                    sb.toString(),
                    ContextCompat.getColor(context, R.color.text_highlight_color_char_progress),
                    sb
                )
            }

            if (showDescription) {
                if (isComplete) {
                    Helper.append(
                        sb,
                        ": ",
                        StyleSpan(Typeface.ITALIC),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    Helper.append(
                        sb,
                        shortDescription,
                        StyleSpan(Typeface.ITALIC),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )


                } else {
                    sb.append(": ")
                    sb.append(shortDescription)

                }
            }
//            Helper.log("SESSION_DATA: get display spannable: '$sb'")
            return sb

        }

    }





    fun refresh() {
        (mActivity as? ActivityMain?)?.getSessionDataDisplay()?.fill()
        (mActivity as? ActivityCharDetails?)?.fragmentSessionData?.fill()
    }

    override fun sort() {
        when (PrefsHelper(context).sortQuestsSessionStats) {
            PrefsHelper.PREF_SESSION_STATS_SORT_QUESTS_ALPHABETICAL -> {
//                Toast.makeText(context, "sort alpha", Toast.LENGTH_LONG).show()
                data.sortWith(compareBy ({ it.isComplete }, { it.name }))
            }
            PrefsHelper.PREF_SESSION_STATS_SORT_QUESTS_HISTORY -> {
                data.sortWith(compareByDescending{ it.date })
//                Toast.makeText(context, "sort date", Toast.LENGTH_LONG).show()
            }
            else -> {
                super.sort()
            }
        }
    }

    override fun inflate(expand: Boolean): View? {
        val v = super.inflate(expand = expand)
        header_view?.setOnLongClickListener {
            PrefsHelper(context).showAllQuestsSessionStats = !PrefsHelper(context).showAllQuestsSessionStats
            refresh()
            true
        }


        val iconSortAlphabetically = v?.findViewById<ImageView>(R.id.icon_sort_alphabetically)
        val iconSortHistory = v?.findViewById<ImageView>(R.id.icon_sort_history)
        iconSortAlphabetically?.setOnClickListener {
            PrefsHelper(context).sortQuestsSessionStats =
                PrefsHelper.PREF_SESSION_STATS_SORT_QUESTS_ALPHABETICAL
            refresh()

        }
        iconSortHistory?.setOnClickListener {
            PrefsHelper(context).sortQuestsSessionStats =
                PrefsHelper.PREF_SESSION_STATS_SORT_QUESTS_HISTORY
            refresh()
        }
        return v
    }

    companion object {
        const val QUEST_COMPLETE_INDICATOR = "-close"
        var DATE_COMPARATOR: Comparator<Quest> = Comparator<Quest> { o1, o2 ->
            if (o1.relatedLogs.size > 0 && o2.relatedLogs.size > 0) {
                LogEntry.CREATION_DATE_COMPARATOR.compare(o1.relatedLogs[0], o2.relatedLogs[0])
            } else {
                o1.name.compareTo(o2.name, ignoreCase = true)
            }
        }
    }
}