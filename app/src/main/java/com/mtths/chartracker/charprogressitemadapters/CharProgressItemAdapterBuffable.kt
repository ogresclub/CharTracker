package com.mtths.chartracker.charprogressitemadapters

import android.content.Context
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterBuffs.Companion.BUFF_REF
import com.mtths.chartracker.dataclasses.Buff
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.StringUtils.getListFromText

interface CharProgressItemAdapterBuffable {

    fun addBuffAutoCompleteString(l: ArrayList<String>): ArrayList<String> {
        l.add("$BUFF_REF = [")
        return l
    }

    fun createBuffs(
        s: String,
        iData: CharProgressItemData,
        item: CharProgressItem?,
        context: Context,
        extraBuffEffects: List<String> = listOf()
    ) {
        val effects = getListFromText(stat = BUFF_REF, text = s)
            .toMutableList().apply { addAll(extraBuffEffects) }
            .map { Buff.parseBuffEffect(it) }


        if (effects.isNotEmpty()) {
            val mBuff = Buff().apply {
                name = iData.name
                this.effects.addAll(effects)
                modifiable = false
                type = item?.filterConstraint?.lowercase()?.replace("@", "") ?: "generic"
                active = true
                this.logid = iData.relatedLogEntry?.id
                this.character = item?.character
//                            Log.d("BUFFF", "created buff $this")
            }
            item?.character?.let { c ->
                c.addBuffOrReplace(b = mBuff, temp = true)
                DatabaseHelper.getInstance(context)?.
                updateCharacter(character = c, keep_last_update_date = false)
            }
        }
    }
}
