package com.mtths.chartracker.charprogressitemadapters

import android.view.View
import android.widget.LinearLayout
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.Buff
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.Skill
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.interfaces.CPIDDamageable
import com.mtths.chartracker.interfaces.CPIDSelfBuffable
import com.mtths.chartracker.interfaces.Moddable
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.SnackBarUtils
import com.mtths.chartracker.utils.StringUtils.getAmmoFromText
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getIntegerStatWithExtraCharsFromText
import com.mtths.chartracker.utils.StringUtils.getIntegerStatWithModificationFromText
import com.mtths.chartracker.utils.StringUtils.getStatUntilEndByCommaFromText
import com.mtths.chartracker.utils.StringUtils.getWeaponDamageFromText
import kotlin.collections.ArrayList
import kotlin.math.ceil

class CharProgressItemAdapterWeaponSR(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapterOneLineExpandable<CharProgressItemAdapterWeaponSR.Weapon>(
        activity = activity,
        colHeaderIds = listOf(
            REF_ID_CHECK,
            REF_ID_ATTACK,
            REF_ID_DAMAGE
        ),
        colWeights = listOf(
            1f,
            2f,
            1f
        )
    ) {



    override fun createAutoCompleteStrings(): ArrayList<String> {
        return ArrayList(
            super.createAutoCompleteStrings().apply {
                addAll(
                    mutableListOf(
                        REF_ID_DAMAGE,
                        REF_ID_ATTACK,
                        REF_ID_MODE,
                        REF_ID_AMMO,
                        REF_ID_DEVICE_RATING
                    ).apply {
                        if (rulesSR5) {
                            addAll(
                                listOf(
                                    REF_ID_ACCURACY,
                                    REF_ID_AP,
                                    REF_ID_RECOIL
                                )
                            )
                        }
                        if (rulesSR6) {
                            addAll(
                                listOf(
                                    REF_ID_ATTACK
                                )
                            )
                        }
                    }.map {
                        "${context.getString(it)} = "
                    }
                )
                val w = Weapon()
                w.addModsAutoCompleteString(this)
                w.addSelfBuffAutoCompleteString(this, context)


            }.map {
                it.lowercase()
            }
        )
    }


    override fun generate_autocomplete_data(e: LogEntry) {
    }

    override fun add(e: LogEntry): LogEntry {

        val textAfterFilterConstraint = Helper.cutAfterString(e.text, item?.filterConstraint)
        val weapon = Weapon()
        weapon.relatedLogEntry = e
        weapon.parseSelfBuff(context)
        weapon.parseMods(textAfterFilterConstraint)
        weapon.parseName(logEntryText = e.text, item = item)
        weapon.damage = getWeaponDamageFromText(context)(REF_ID_DAMAGE, e.text)
        weapon.accurracy = getIntegerStatWithModificationFromText(context)(REF_ID_ACCURACY, e.text)
        weapon.ap = getIntegerStatFromText(
            statId = REF_ID_AP, text = e.text, context = context)
        weapon.mode = getStatUntilEndByCommaFromText(
            statId = REF_ID_MODE, text = e.text, context = context)
        weapon.recoil = getIntegerStatFromText(
            statId = REF_ID_RECOIL, text = e.text, context = context)
        weapon.ammo = getAmmoFromText(context)(REF_ID_AMMO, e.text)
        weapon.attack = getIntegerStatWithExtraCharsFromText(
            statId = REF_ID_ATTACK, text = e.text, extra_chars = "/-", context = context)
        weapon.deviceRating = getIntegerStatFromText(
            statId = REF_ID_DEVICE_RATING, text = e.text, context = context
        ).toIntOrNull()
        data.add(weapon)
        checkExpand(weapon)
        return e
    }

    val sr5AccuracyWeight = 1f

    override fun inflate(expand: Boolean): View? {
        return super.inflate(expand).apply {
            if (rulesSR5) {
                val label =  R.string.accuracy_label
                val attackWeight = sr5AccuracyWeight
                renameStat1ColumnHeader(
                    nameId = label,
                    containerView = this,
                    context = context,
                    weight = attackWeight
                )
                renameStat2ColumnHeader(
                    nameId = R.string.damage_label,
                    containerView = this,
                    context = context
                )
            }
        }
    }

    override fun inflateView(index: Int, check_mistakes: Boolean): ViewHolder {
        return super.inflateView(index, check_mistakes).apply {
            val weapon = data[index]

            nameView?.apply {
                text = weapon.name
                setOnClickListener {
                    mActivity?.supportFragmentManager?.let { fm ->
                        weapon.showDamageMonitor(fm = fm, context = context)
                    }
                }
            }
            dataViews?.get(0)?.apply {
                text = weapon.check.toString()
                setOnClickListener(getShowSkillCheckCalculationOnClickListener(weapon))
            }
            dataViews?.get(1)?.apply {
                text = weapon.attack
                if (rulesSR5) {
                    layoutParams = LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        sr5AccuracyWeight
                    )
                    text = weapon.accurracy
                }
            }
            dataViews?.get(2)?.apply {
                text = weapon.damage
            }
            implementExpand(view = root, data = weapon)
        }
    }

    private fun getShowSkillCheckCalculationOnClickListener(
        weapon: Weapon
    ): View.OnClickListener = View.OnClickListener { v ->
        item?.character?.let { c ->
            c.ruleSystem?.getSkillDetailsBuilder?.invoke(
                c,
                ValueNamePair(
                    name = fireArmsSkillName,
                    skill = Skill.getSkill(
                        name = fireArmsSkillName,
                        character = c,
                        skillData = Skill.getSkillData(context)
                    ),
                    mCalculator = object : CharProgressItemAdapterValueBased.Calculator {}
                ),
                context
            )?.msgFormatTotal?.format(weapon.check) +
                    if (weapon.damageMalus > 0) {
                        " - %d (%s)".format(
                            weapon.damageMalus,
                            context.getString(R.string.damage_label)
                        )
                    } else {
                        ""
                    } +
                    weapon.buffCalculationString

        }?.let { msg ->
            SnackBarUtils.showSnackBar(
                view = v,
                msg = msg,
                duration = Snackbar.LENGTH_LONG,
                context = context
            )
        }

    }

    val fireArmsSkillName: String
        get() = context.getString(R.string.shadowrun6_skill_fire_arms)

    inner class Weapon :
        CharProgressItemData(),
        Moddable,
        CPIDDamageable,
        CPIDSelfBuffable
    {
        override val mods: MutableList<String> = mutableListOf()
        override val _REF_MOD: String? = null // means is used DEFAULT REF_MOD
        override var buff: Buff = Buff()
        override val damageableData: List<CPIDDamageable.DamageableData>
            get() =  listOf(
                CPIDDamageable.DamageableData(
                    hp = 8 + ceil((deviceRating ?: 0).toFloat() / 2).toInt(),
                    damageTypeId = R.string.label_damage_monitor_matrix
                )
            )

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                if (rulesSR5) {
                    add(ExpandData(header = context.getString(R.string.ap_label), text = ap))
                    add(ExpandData(header = context.getString(R.string.recoil_label), text = recoil))
                }
                add(ExpandData(header = context.getString(R.string.mode_label), text = mode))
                add(ExpandData(header = context.getString(R.string.ammo_label), text = ammo))
                add(ExpandData(header = context.getString(R.string.label_device_rating), text = deviceRating?.toString() ?: "-"))
                addAll(modsExpandData)
            }

        val damageMalus: Int
            get() = getSrDamageMalus(
                damageTypes = listOf(R.string.label_damage_monitor_matrix),
                item = item,
                context = context
            )

        var damage: String
        var accurracy: String
        var ap: String
        var mode: String
        var recoil: String
        var ammo: String
        var attack: String
        var deviceRating: Int? = null

        val check: Int
            get() = getSkillValue(R.string.shadowrun6_skill_fire_arms) - damageMalus + totalBuffValue

        init {
            name = "My Weapon"
            damage = "-"
            accurracy = "-"
            ap = "-"
            mode = "-"
            recoil = "-"
            ammo = "-"
            attack = "-"
        }
    }

    companion object {
        val REF_ID_ATTACK = R.string.attack_label
        val REF_ID_DAMAGE = R.string.damage_label
        val REF_ID_AP = R.string.ap_label
        val REF_ID_DEVICE_RATING = R.string.label_device_rating
        val REF_ID_ACCURACY = R.string.accuracy_label
        val REF_ID_MODE = R.string.mode_label
        val REF_ID_RECOIL = R.string.recoil_label
        val REF_ID_AMMO = R.string.ammo_label
        val REF_ID_CHECK = R.string.label_check
    }
}