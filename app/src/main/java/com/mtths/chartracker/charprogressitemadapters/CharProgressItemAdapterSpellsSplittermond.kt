package com.mtths.chartracker.charprogressitemadapters

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.Skill
import com.mtths.chartracker.dataclasses.SpellSplittermond
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_CAST_DURATION
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_CAST_RANGE
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_DIFFICULTY
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_ENHANCEMENT
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_ENHANCEMENT_DESCRIPTION
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_ENHANCEMENT_OPTIONS
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_LEVEL
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_DESCRIPTION
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_FOCUS_COST
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_PAGE
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_SCHOOL
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_SPELL_DURATION
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.REF_ID_TYPES
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.getSpell
import com.mtths.chartracker.dataclasses.SpellSplittermond.Companion.getSpellMap
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.fragments.FragmentDamageMonitor
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.capitalizeNewsLetterStyle
import com.mtths.chartracker.utils.Helper.checkFeatured
import com.mtths.chartracker.utils.Helper.checkIgnoreExp
import com.mtths.chartracker.utils.Helper.getRegexContainsFilterConstraint
import com.mtths.chartracker.utils.HttpUtils
import com.mtths.chartracker.utils.SnackBarUtils
import com.pedromassango.doubleclick.DoubleClick
import com.pedromassango.doubleclick.DoubleClickListener
import java.util.ArrayList

class CharProgressItemAdapterSpellsSplittermond(
    activity: ProgressBarFrameDropBoxActivity?
): CharProgressItemAdapterOneLineExpandable<SpellSplittermond>(
    activity = activity,
    comparator = compareBy<SpellSplittermond> { it.level }.thenBy { it.name },
    colHeaderIds = listOf(
        REF_ID_CAST,
        REF_ID_LEVEL,
        REF_ID_CHECK
    )
),
    CharProgressItemAdapterBuffable {

    override val actionIconResource = R.drawable.ic_use_charge


    val spellMap = getSpellMap(context)
    var featured_spells_used = 0

    override fun createAutoCompleteStrings(): ArrayList<String> {
        return super.createAutoCompleteStrings().also {
            it.addAll(
                listOf(
                    REF_ID_LEVEL,
                    REF_ID_SCHOOL,
                    REF_ID_TYPES,
                    REF_ID_DIFFICULTY,
                    REF_ID_FOCUS_COST,
                    REF_ID_CAST_DURATION,
                    REF_ID_CAST_RANGE,
                    REF_ID_SPELL_DURATION,
                    REF_ID_ENHANCEMENT,
                    REF_ID_PAGE
            ).map {
                "${context.getString(it).lowercase()} = "
                }
            )
            it.addAll(
                listOf(
                    REF_ID_ENHANCEMENT_OPTIONS,
                    REF_ID_DESCRIPTION,
                    REF_ID_ENHANCEMENT_DESCRIPTION
                ).map {
                    "${context.getString(it).lowercase()} = ["
                }
            )
            addBuffAutoCompleteString(it)
            SpellSplittermond().addSelfBuffAutoCompleteString(it, context)
            it.addAll(spellMap.values.map { it.name })
        }
    }

    fun getShowSkillCheckCalculationOnClickListener(
        spell: SpellSplittermond
    ): View.OnClickListener = View.OnClickListener { v ->
        val skillName = spell.school
        mActivity?.let { a ->
            item?.character?.let { c ->
                c.ruleSystem?.getSkillDetailsBuilder?.invoke(
                    c,
                    ValueNamePair(
                        name = skillName,
                        skill = Skill.getSkill(
                            name = skillName,
                            character = c,
                            skillData = Skill.getSkillData(context)
                        ),
                        mCalculator = object : CharProgressItemAdapterValueBased.Calculator {}
                    ),
                    context
                )?.msgFormatTotal?.format(spell.getCheck(a)) +
                        spell.getSpecCalculationString(a) +
                        spell.buffCalculationString

            }?.let { msg ->
                SnackBarUtils.showSnackBar(
                    view = v,
                    msg = msg,
                    duration = Snackbar.LENGTH_LONG,
                    context = a
                )
            }
        }

    }

    override fun generate_autocomplete_data(e: LogEntry) {}

    fun getDisplayString(spell: SpellSplittermond): SpannableStringBuilder {
        val displayName = getSpell(name = spell.name, spells = spellMap)?.name
            ?: capitalizeNewsLetterStyle(spell.name)


        return SpannableStringBuilder(displayName).also {sb ->
            if (item?.character?.showSpellSchools == true) {

                sb.append(" [")
                Helper.append(
                    sb = sb,
                    text = spell.school,
                    what = StyleSpan(Typeface.ITALIC),
                    flags = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                sb.append("]")

            }
        }

    }

    override fun add(e: LogEntry): LogEntry {
        SpellSplittermond().apply {
            relatedLogEntry = e
            character = item?.character
            parseName(e.text, item)
            parseSelfBuff(context)
            parseLevel(s = e.text, spellMap = spellMap, context = context)
            //spell map includes only names
            parseSchool(s = e.text, spellMap = spellMap, context = context)
            parseTypes(s = e.text, spellMap = spellMap, context = context)
            //spell map includes only names
            parseCastDuration(s = e.text, context = context)
            parseCastRange(s = e.text, context = context)
            parseSpellDuration(s = e.text, context = context)
            parseEnhancement(s = e.text, context = context)
            parseEnhancementDescription(s = e.text, context = context)
            parseEnhancementOptions(s = e.text, context = context)
            parsePage(s = e.text, context = context)
            parseGenesisId(s = e.text, context = context)
            parseFocusCost(s = e.text, context = context)
            parseDescription(text = e.text, context = context, refId = REF_ID_DESCRIPTION)
            parseDifficulty(s = e.text, context = context)

            createBuffs(s = e.text, iData = this, item = item, context = context)

            val ignoreExpCheck = checkIgnoreExp(e.text)
            e.hasError = !ignoreExpCheck && (e.exp != -getExpCost(level) || e.hasError)

            if (checkFeatured(e.text)) featured_spells_used++

            checkExpand(this)
            data.add(this)
        }

        return e
    }

    override fun getExpProposal(text: String, nb_of_active_chars: Int, data: ArrayList<*>): Int {
        item?.let {
            return if (text.matches(Regex(getRegexContainsFilterConstraint(it.filterConstraint).toString()))) {
                SpellSplittermond()
                    .apply {
                        parseName(text, item)
                        parseLevel(s = text, context = context)
                    }
                    .let { s ->
                        if (s.name.isNotBlank()) {
                            -getExpCost(s.level)
                        } else {
                            0
                        }
                    }
            } else {
                0
            }
        }
        return 0
    }

    override fun inflateView(index: Int, check_mistakes: Boolean): ViewHolder {
        return super.inflateView(index, check_mistakes).apply {

            val spell = data[index]

            nameView?.apply {
                text = getDisplayString(spell = spell)

                setOnClickListener(
                    DoubleClick(
                        object : DoubleClickListener {
                            override fun onDoubleClick(view: View?) {
                                mActivity?.let {
                                    HttpUtils.openWebPage(
                                        url = spell.getLink(context),
                                        activity = it
                                    )
                                }
                            }

                            override fun onSingleClick(view: View?) {}
                        }
                    )
                )
            }
            dataViews?.get(0)?.apply {
                text = spell.level.toString()
            }
            dataViews?.get(1)?.apply {
                mActivity?.let {a ->
                    text = spell.getCheck(activity = a).toString()

                }
                setOnClickListener(getShowSkillCheckCalculationOnClickListener(spell))
            }
            actionIcon?.setOnClickListener {
                mActivity?.let { a -> showAddDamageDialog(spell, a) } // to apply focus cost
            }

            if (check_mistakes && spell.relatedLogEntry?.hasError == true) {
                root?.setBackgroundResource(R.color.holo_red_light_half_transparent)
            }

            implementExpand(view = root, data = spell)
        }
    }

    fun showAddDamageDialog(spell: SpellSplittermond, activity: ProgressBarFrameDropBoxActivity) {
        val focusMap = spell.extractFocusCosts(spell.focus)
        val enhanceFocusMap = spell.extractEnhanceFocusCosts(spell.enhancement)
        val enhanceCostArray = intArrayOf(
            enhanceFocusMap["verzehrt"] ?: 0,
            enhanceFocusMap["erschöpft"] ?: 0,
            enhanceFocusMap["kanalisiert"] ?: 0
        )

        val ref = RuleSystem.rule_system_splittermond.damageTypes[Character.REF_DAMAGE_REF_MAGIC]!!.damageRef
        (activity as? ActivityCharDetails)?.
        fragmentStatusMonitor?.
        damageMonitors?.get(ref)?.
        showDialogAddDamage(
            headerSuffix = " (%s)".format(spell.name),
//            msg = spell.expandData.find { it.headerId == HEADER_ID_ERFOLGSGRADE }?.text ?: "",
            initValues = intArrayOf(
                focusMap["verzehrt"] ?: 0,
                focusMap["erschöpft"] ?: 0,
                focusMap["kanalisiert"] ?: 0
            ),
            createCustomStartView = { damageEditTextMap ->
                val ctxt = DialogCustom.getMyDialogContext(context)

                val container = LinearLayout(ctxt).apply {
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                    )
                    orientation = LinearLayout.VERTICAL
                }

                container.addView(
                    ExpandData(
                        headerId = R.string.stat_fokus_label,
                        text = "%d/%d".format(
                            item?.character?.currentFokusPunkte?: -1,
                            item?.character?.fokusPunkte?: -1
                        )
                    )
                        .getView(activity)
                )
                container.addView(
                    ExpandData(
                        headerId = REF_ID_CHECK,
                        text = spell.getCheck(activity).toString()
                    )
                        .getView(activity)
                )


                spell.expandData.forEach {
                    container.addView(it.getView(activity))
                }



                (LayoutInflater.from(context).inflate(
                     R.layout.settings_switch_nobg, null, false
                 ) as SwitchCompat).apply {
                    isChecked = false
                    setText(R.string.cpi_ref_spell_splittermond_enhancement)
                    val margin = resources.getDimension(R.dimen.big_text).toInt()
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                    ).apply {
                        setMargins(margin, 10, margin, 0)
                    }

                    textSize =  Helper.pxToDp(resources.getDimension(R.dimen.big_text), context)
                    setOnClickListener {
                        if (isChecked) {
                            FragmentDamageMonitor.modifyDamageEditTextValues(
                                damageEditTextMap = damageEditTextMap,
                                damageValueMap = enhanceCostArray
                            )
                        } else {
                            FragmentDamageMonitor.modifyDamageEditTextValues(
                                damageEditTextMap = damageEditTextMap,
                                damageValueMap = enhanceCostArray,
                                factor = -1
                            )
                        }
                    }
                    container.addView(this)
                }

                container

            }

        )

    }

    fun log(msg: String) {
        Log.e("CPIA_SPELLS", "ssssssssssss: $msg")
    }



    fun getExpCost(level: Int): Int {
       return if (level == 0) 1 else 3*level

    }

    private fun getNumFeaturedSpells(): Int? {
        try {
            // for each two point in s skill, you receive one free spell from this spell school
            return mActivity?.charProgressItemsMap?.get(R.string.filter_constraint_skill)?.mainItem?.adapter?.let {
                it as CharProgressItemAdapterSkills }?.data?.filter {
                it.skill?.type in listOf(context.getString(R.string.splittermond_skill_category_magic))
                }?.sumOf { it.value / 2 }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    override fun inflate(expand: Boolean): View? {


        val container = super.inflate(expand)

        container?.findViewById<TextView>(R.id.name_label)?.setOnClickListener {
            item?.character?.let { c ->
                c.toggleShowSpellSchools()
                DatabaseHelper.getInstance(context)?.updateCharacter(
                    character = c, keep_last_update_date = true
                )
                updateAllDisplays()
            }
        }

        /*
        the following is not really needed, because you will manage your character most likely with
        genesis anyways.
         */
//        if (expand) {
//            // Set Header color for skills to show if you have spend all your skills from int/class/human correctly
//            val check = PrefsHelper(context).checkFeaturedStats && rulesSplittermond
//            log("check = $check")
//            getNumFeaturedSpells()?.let { featuredSpells ->
//                log("featured spells: $featuredSpells")
//                if (check) {
//                    item?.character?.let { c ->
//                        val free_featured_skills = featuredSpells - featured_spells_used
//                        header_view?.text = markHeaderOfSkillsOrLanguages(free_featured_skills)
//                    }
//
//                }
//            }
//        }


        return container
    }

    companion object {
        val REF_ID_CHECK = R.string.label_check
        val REF_ID_CAST = R.string.label_cast

    }


}
