package com.mtths.chartracker.charprogressitemadapters

import android.content.Context
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.Skill
import com.mtths.chartracker.dataclasses.Skill.Companion.getSkill
import com.mtths.chartracker.dataclasses.Skill.Companion.getSkillWAttrCastStatFallback
import com.mtths.chartracker.dataclasses.Specialization
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.SnackBarUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CharProgressItemAdapterSkills(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapterValueBased(
        activity = activity,
        comparator = compareBy { it.name },
    ) {

    override val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header
    val skillData: HashMap<RuleSystem, HashMap<String, Skill>> by lazy { Skill.getSkillData(context = context) }
    override val comparator = compareBy<ValueNamePair> { getSkill(it.name)?.type }.thenBy { it.name }

    val separatorIndices = mutableListOf<Int>()

    fun generateSeparatorIndices() {

        fun log(msg: String) {
            Log.e("TYPE_SEP", "generateSeparatorIndices::::: $msg")
        }

        sort()

        separatorIndices.clear()
        var last: ValueNamePair? = null

        data.forEachIndexed { index, valueNamePair ->
//            log("vnp '${valueNamePair.name}' skill = ${valueNamePair.skill}")

            last?.skill?.let { lskill ->
                valueNamePair.skill?.let { skill ->
//                    log("type: ${skill.type}, last type: ${lskill.type}")

                    if (!skill.type.equals(lskill.type, ignoreCase = true)) {
                        separatorIndices.add(index - 1)
                    }
                }
            }
            last = valueNamePair
        }

        log("separator indices = $separatorIndices")

    }


    private fun getSkill(name: String): Skill? {
        return getSkill(
            name = name,
            character = item?.character,
            skillData = skillData)
    }


    override fun modifyTextViews(textViews: List<TextView>, valueNamePair: ValueNamePair) {
        valueNamePair.skill?.type.let { type ->
            if (type  != Skill.SKILL_DEFAULT_TYPE) {
                textViews.forEach {

                    val colorId = when {
                        type.equals(context.resources.getString(R.string.splittermond_skill_category_magic), ignoreCase = true) -> R.color.skill_color_magic
                        type.equals(context.resources.getString(R.string.splittermond_skill_category_combat), ignoreCase = true) -> R.color.skill_color_combat
                        else -> -1
                    }
                    if (colorId >= 0) {
                        it.setTextColor(ContextCompat.getColor(context, colorId))
                    }
                }
            }

        }

    }

    val levelAdapter: CharProgressItemAdapterClasses?
        get() {
            try {
                return mActivity?.charProgressItemsMap?.get(R.string.filter_constraint_class)?.mainItem?.adapter
                        as? CharProgressItemAdapterClasses
            } catch (e: Exception) {
                e.printStackTrace()
                Helper.log("Skills", "No SkillGroup Adapter found")
            }
            return null
        }


    override fun add(e: LogEntry, ignoreMismatches: Boolean, data: ArrayList<ValueNamePair>, dontModifyData: Boolean): Holder {
        val holder = super.add(e, ignoreMismatches, data, dontModifyData)

        item?.character?.let { c ->
            holder.holderItems.forEach { h ->
                h.pair?.let { p ->
                    /*
                    attach skills
                     */
                    if (p.skill == null) {
                        p.skill = getSkillWAttrCastStatFallback(
                            name =  p.name,
                            skillData = skillData,
                            character = item?.character
                        )
                    }

                    /*
                     * store attributes in character
                     */

                    c.setStat(p.name, p.value)
                }
            }
            DatabaseHelper.getInstance(context)?.updateCharacter(
                character = c, keep_last_update_date = true)
        }


        ////////////////////////////// FEATURED SKILL POINTS STUFF /////////////////////////////////




        //        Helper.log(DEBUG_TAG, "add skill called. holder = value " + h.pair.value + ", name " + h.pair.name + ", index " + h.index_of_changed_pair);
        for (h in holder.holderItems) {
            if (e.text.lowercase().contains(Global.FEATURED_MARKER) && h.index_of_changed_pair >= -1) {
                holder.logEntry?.hasError = false
//                log(DEBUG_TAG, ".feature found. holder = value " + h.pair?.value + ", name " + h.pair?.name + ", index " + h.index_of_changed_pair)
                item?.character?.let { c ->
                    var amount_of_skill_points_added = -1
                    if (h.index_of_changed_pair >= -1) {
                        /*
                        this means that the logText contained @Value
                         */
                        h.pair?.let {
                            amount_of_skill_points_added = it.value -  //new value
                                    h.old_value //old value
                        }
                    }
//                    log(DEBUG_TAG, "amount of skill points to add: $amount_of_skill_points_added")
                    if (amount_of_skill_points_added > 0) {
                        c.getPrefs(context).featured_skill_points_used += amount_of_skill_points_added
                    }
                }
            }
        }

        return holder
    }

    private fun addAllAutocompleteStringsToData() {
        createAutoCompleteStrings().forEach { acS ->
            getValueNamePair(name = acS, data = data) ?: run {
                data.add(ValueNamePair(name = acS, mCalculator = mCalculator)
                    .apply {
                        skill = getSkillWAttrCastStatFallback(
                            name = name, skillData = skillData, character = item?.character)
                    })
            }
        }
    }

    private fun hideSkillWith0RanksIfShowAllSkillsDisabled(vnp: ValueNamePair) {
        item?.character?.let { c ->
            vnp.viewHolder.columnsHolder?.root?.visibility =
                if (!c.showAllSkills && vnp.value == 0) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
        }
    }

    private fun displayOrHideRelatedAttribute(vnp: ValueNamePair) {

        vnp.viewHolder.columnsHolder?.extraColumnsContainer?.apply {
            if (item?.character?.showAttributes == true) {
                visibility = View.VISIBLE

                (getAttrDisplayStyle(
                    vnp.skill?.attributes?.split(" "))?: listOf("---")
                        ).let { attList ->
                        attList.forEach { attr ->
                            val index = attList.indexOf(attr)
                            if (childCount < index + 1) {
                                //inflate new view
                                addView(TextView(context).apply {
                                    layoutParams = LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        0.4f
                                    )
                                    gravity = Gravity.CENTER_HORIZONTAL
                                    text = attr
                                })
                            } else {
                                // update existing views
                                (getChildAt(index) as? TextView?)?.apply {
                                    visibility = View.VISIBLE
                                    text = attr
                                }
                            }
                        }
                        if (childCount > attList.size) {
                            removeViews(attList.size, childCount - attList.size)
                        }
                    }

                setOnClickListener {
                    showDialogChooseCustomSkillAttribute(vnp)
                }
            } else {
                visibility = View.GONE
            }
        }
    }

    override fun fillData(mData: ValueNamePair, check_mistakes: Boolean, holder: ViewHolder) {
        super.fillData(mData, check_mistakes, holder)
        hideSkillWith0RanksIfShowAllSkillsDisabled(vnp = mData)

        /////////////////////////////// STRUCTURE ////////////////////////////////////////////

        displayOrHideRelatedAttribute(mData)



        if (prefs.rulesE5) {
            holder.dataViews?.get(0)?.let {
                it.visibility = View.INVISIBLE
            }
        }

        activity?.lifecycleScope?.launch(Dispatchers.IO) {
            fillTotalValue(
                valueNamePair = mData,
                view = holder.root,
                item = item,
                context = context)?.let {
                    totalTv -> modifyTextViews(listOf(totalTv), mData)
            }
        }


        holder.root?.apply {
            setOnClickListener {
                showSkillDetails(
                    view = this,
                    valueNamePair = mData,
                    character = item?.character,
                    context = context)
            }
        }


    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {

        val v = super.getView(index, check_mistakes)

        if (index - 1 in separatorIndices) {
            v?.findViewById<View>(R.id.line)?.visibility = View.GONE
        }

        return v
    }



    override fun inflate(expand: Boolean): View? {

        generateSeparatorIndices()

        val v = super.inflate(expand = expand)

        fun implementToggleShowAllSkills() {
            colNameHeader?.setOnClickListener {
                Log.d("TOGGLE_SHOW_ALL_SKILLS", "on click: toggle all skills")

                item?.let {
                    it.character?.let {c ->
                        c.toggleShowAllSkills()
                        Log.d("TOGGLE_SHOW_ALL_SKILLS",
                            "on click: showSkills after toggle = ${c.showAllSkills}")

                        data.forEach {
                            hideSkillWith0RanksIfShowAllSkillsDisabled(it)
                        }
                    }
                }
            }
        }

        fun implementToggleShowAttributes() {
            colTotalHeader?.setOnClickListener {
                Log.d("TOGGLE_SHOW_ATTRIBUTES", "on click: toggle show attributes")

                item?.character?.let {c ->
                    c.toggleShowAttributes()
                    Log.d("TOGGLE_SHOW_ATTRIBUTES",
                        "on click: showAttributes after toggle = ${c.showAttributes}")
                    DatabaseHelper.getInstance(context)?.updateCharacter(
                        character = c, keep_last_update_date = true)
                    data.forEach {
                        displayOrHideRelatedAttribute(it)
                    }
                }
            }
        }

        ///////////////////////////////////// START ///////////////////////////////////////////////


        implementToggleShowAllSkills()
        implementToggleShowAttributes()

        colTotalHeader?.setText(R.string.total)
        colBaseHeader?.setText(R.string.base)

        if(expand) {
            // Set Header color for skills to show if you have spend all your skills from int/class/human correctly
            val check_skills = PrefsHelper(context).checkFeaturedStats
            item?.character?.let { c ->
                if (check_skills  && c.intelligence > 0) {
                    c.ruleSystem?.let {
                        if(it.countFeaturedSkills) {
                            val free_featured_skills = c.level * c.totalSkillsPerLevel - c.getPrefs(context).featured_skill_points_used
                            header_view?.text = markHeaderWhenCountingPoints(free_featured_skills)
                        }
                    }

                }
            }

        }
        return v
    }

    override fun onItemAttached(item: CharProgressItem?) {
        super.onItemAttached(item)
        addAllAutocompleteStringsToData()
        item?.character?.getPrefs(context)?.featured_skill_points_used = 0
    }

    override fun modifyValueDisplay(vnp: ValueNamePair): String {
        return fillBaseValue(valueNamePair = vnp, character = item?.character)
    }

    private fun showDialogChooseCustomSkillAttribute(
        vnp: ValueNamePair
    ) {
        val skillName = vnp.name
        item?.character?.also { c ->
            val currentCustomAttr = c.getCustomSkillAttribute(skillName)

            fun updateCharAndCustomAttributeDisplay(vnp: ValueNamePair) {
                (mActivity as? ActivityCharDetails?)?.updateCharacterInDbAndGlobalCharsAndReload(
                    keepLastUpdateDate = false
                )
                vnp.skill = getSkillWAttrCastStatFallback(
                    name = vnp.name,
                    skillData = skillData,
                    character = c
                )
                displayOrHideRelatedAttribute(vnp)
                fillTotalValue(
                    valueNamePair = vnp,
                    item = item,
                    view = vnp.viewHolder.columnsHolder?.root,
                    context = context
                )
            }

            val dialog = DialogCustom<String>()
            dialog.setDefaultInput()
            dialog.setInputText(text2Text = currentCustomAttr, text1Text = null)
            dialog.setTitle(R.string.dialog_title_choose_custom_attribute)
            dialog.setEditText2Adapter(
                mActivity?.let {
                    ArrayAdapter(
                        it,
                        R.layout.auto_complete_entry,
                        it.charProgressItemsMap[R.string.filter_constraint_attribute]?.mainItem?.adapter?.createAutoCompleteStrings()
                            ?: listOf()
                    )
                },
                alwaysShow = true
            )

            dialog.setPositiveButton(
                context.getString(R.string.ok)
            ) {

                c.setCustomSkillAttribute(
                    skillName = skillName,
                    attributeName = dialog.text2.text.toString().trim()
                )
                dialog.dismiss()
                updateCharAndCustomAttributeDisplay(vnp)

            }
            val negButtonText = context.getString(
                if (currentCustomAttr != null) R.string.label_delete else R.string.cancel
            )
            dialog.setNegativeButton(negButtonText) {
                if (currentCustomAttr != null) {
                    Log.d("CUSTOM_ATTRIBUTE", "deleting custom attribute $skillName")
                    c.removeCustomAttribute(skillName)
                    updateCharAndCustomAttributeDisplay(vnp)
                }
                dialog.dismiss()
            }


            mActivity?.supportFragmentManager?.let { fm ->
                dialog.show(fm, "custom_dialog_prompt_filename")
            }
        }
    }

    /**
     * add separator line if we have a separator index
     */
    override fun doAfterGetView(listOfViews: LinearLayout?, index: Int) {

        fun log(msg: String) {
            Log.d("TYPE_SEP", "addLine::::: $msg ")
        }
        // add another line to separate different types
        data[index].let { vnp ->
            if (index in separatorIndices) {
                log("adding line at index $index")
                listOfViews?.addView(
                    LayoutInflater.from(context).inflate(
                        R.layout.line, null, false).apply {
                        layoutParams = ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            Helper.dpToPx(2f, context))
                    }
                )
            }
        }
    }

    val srCalculator = object: Calculator {
        override fun getSpecializationCost(oldSpecializations: List<Specialization>?, newSpecialization: List<Specialization>?): Int {
            val oSpec = oldSpecializations?.map { it.name.lowercase() }
            val nSpec = newSpecialization?.map { it.name.lowercase() }
            var sDiff = nSpec
            oSpec?.forEach { sDiff = sDiff?.minus(it) }
//                Helper.log("SPECIALIZATION_COST", "new specs: $nSpec old specs: " + oSpec)
//                Helper.log("SPECIALIZATION_COST", ("diff: $sDiff"))
            val nExp = newSpecialization?.filter { it.expertise }?.map { it.name.lowercase() }
            val oExp = oldSpecializations?.filter { it.expertise }?.map { it.name.lowercase() }
            var eDiff = nExp
            oExp?.forEach { eDiff = eDiff?.minus(it) }
            val specCost = 5 * (sDiff?.size ?: 0)
            val expertCost = 5 * (eDiff?.size ?: 0)
//                Helper.log("SPECIALIZATION_COST", "new expt: $nExp old expt: " + oExp)
//                Helper.log("SPECIALIZATION_COST", ("diff: $eDiff"))

//                log("SPECIALIZATION_COST", "spec cost: $specCost, expert cost: $expertCost")
            return specCost + expertCost
        }

        override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
            var result = 0
            for (i in old_value + 1..new_value) {
                result += i * 5
            }
            return result
        }
    }
    val defaultCalculator = object : Calculator {
        override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
            return Global.SKILL_POINT_EXP_COST * (new_value - old_value)
        }

        override fun getSpecializationCost(
            oldSpecializations: List<Specialization>?,
            newSpecialization: List<Specialization>?
        ): Int {
            return 0
        }
    }

    val skillfullDnDCalculator = object : Calculator {

        fun log(msg: String) {
//            Log.e("SPECS_SDND", "++++++++$msg")
        }

        fun calcIncreaseCost(currentValue: Int): Int {
            return when {
                currentValue < 0 -> 0
                currentValue < 4 -> 4
                currentValue < 6 -> 6
                currentValue < 8 -> 8
                currentValue < 10 -> 10
                currentValue < 12 -> 12
                else -> 12
            }
        }
        override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
            var result = 0
            for (i in old_value until new_value) {
                result += calcIncreaseCost(i)
            }
            return result
        }

        fun calcSpecCost(oldSpecLevel: Int): Int {
            return 5*(oldSpecLevel + 1)
        }

        override fun getSpecializationCost(
            oldSpecializations: List<Specialization>?,
            newSpecialization: List<Specialization>?
        ): Int {
            var exp = 0
            newSpecialization?.forEach { nSpec ->
                var l = oldSpecializations?.
                find { it.name.equals(nSpec.name, ignoreCase = true) }?.level ?: 0
                while (l < nSpec.level) {
                    exp += calcSpecCost(l)
                    log("old level: $l ----> exp cost: ${calcSpecCost(l)}")
                    l++
                }
                log("total cost for new level ${nSpec.level} : $exp")
            }
            return exp
        }
    }

    val spittermondCalculator = object : Calculator {

        fun log(msg: String) {
//            Log.e("SPECS_SDND", "++++++++$msg")
        }

        fun calcIncreaseCost(currentValue: Int): Int {
            return when {
                currentValue < 0 -> 0
                currentValue < 6 -> 3
                currentValue < 9 -> 5
                currentValue < 12 -> 7
                currentValue < 15 -> 9

                else -> 999
            }
        }
        override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
            var result = 0
            for (i in old_value until new_value) {
                result += calcIncreaseCost(i)
            }
            return result
        }

        override fun getSpecializationCost(
            oldSpecializations: List<Specialization>?,
            newSpecialization: List<Specialization>?
        ): Int {
            var exp = 0
            newSpecialization?.forEach { nSpec ->
                exp += 5 * nSpec.level
            }
            return exp
        }
    }


    override val mCalculator: Calculator
        get() {
            return when {
                rulesSR -> srCalculator
                rulesSkillfullE5 -> skillfullDnDCalculator
                rulesSplittermond -> spittermondCalculator
                else -> defaultCalculator
            }
        }


    companion object {
        const val DEBUG_TAG = "CHAR_PROG_ADAPTER_SKILLS"

        fun fillTotalValue(
            valueNamePair: ValueNamePair,
            item: CharProgressItem?,
            view: View?,
            context: Context?
        )
                : TextView? {
            view?.findViewById<TextView>(R.id.number2)?.let { n2View ->

                calcTotalSkillValue(
                    vnp = valueNamePair,
                    character = item?.character,
                    context = context
                )
                    .let { tValueRes ->
                        n2View.text = "%d%s".format(
                            tValueRes.totalValue,
                            if (!tValueRes.foundRelatedAttribute) "" else ""
                        )
                    }
                return n2View
            }
            return null
        }

        fun showSkillDetails(
            view: View,
            valueNamePair: ValueNamePair,
            character: Character?,
            context: Context?
        ) {
            character?.let { c ->

                c.ruleSystem?.getSkillDetailsBuilder?.invoke(c, valueNamePair, context)?.msg?.let {msg ->

                    SnackBarUtils.showSnackBar(
                        view = view,
                        msg = msg,
                        duration = Snackbar.LENGTH_SHORT,
                        context = context
                    )

                }
            }
        }


        /**
         * @param skillNames skill names should be separated by space
         */
        fun getAttrDisplayStyle(skillNames: List<String>?): List<String>? {
            return skillNames?.map { attr ->
                val displayName = if (attr.length >= 3) attr.substring(0, 3) else attr
                displayName.uppercase()
            }
        }
    }




    init {
        stdAutoCompleteData = HashMap()
        stdAutoCompleteData[RuleSystem.rule_system_ogres_club] =  mActivity?.let {a ->
            Skill.getSkillMap(arrayId = R.array.ogres_club_skill_list, context = a)
                .keys
                .map { Helper.capitalizeNewsLetterStyle(it)}
        } ?: listOf()
        stdAutoCompleteData[RuleSystem.rule_system_Dnd_E5] = mActivity?.let {a ->
            Skill.getSkillMap(arrayId = R.array.dnd_5e_skill_list, context = a)
                .keys
                .map { Helper.capitalizeNewsLetterStyle(it)}
        } ?: listOf()
        stdAutoCompleteData[RuleSystem.rule_system_skillfull_dnd_5E] =
            mActivity?.let {
                Skill.getSkillMap(arrayId = R.array.skills_skillfull_5e, context = it)
                    .filterNot { it.value.type == "spellschool" }.keys
                    .map { Helper.capitalizeNewsLetterStyle(it)}
            } ?: listOf()
        stdAutoCompleteData[RuleSystem.rule_system_splittermond] =
            mActivity?.let {
                Skill.getSkillMap(arrayId = R.array.skills_splittermond, context = it)
                    .keys
                    .map { Helper.capitalizeNewsLetterStyle(it)}
            } ?: listOf()
        stdAutoCompleteData[RuleSystem.rule_system_sr5] =
            mActivity?.let {
                Skill.getSkillMap(arrayId = R.array.shadowrun5_skill_list, context = it)
                    .keys
                    .map { Helper.capitalizeNewsLetterStyle(it)}
            } ?: listOf()

        stdAutoCompleteData[RuleSystem.rule_system_sr6] =
            mActivity?.let {
                Skill.getSkillMap(arrayId = R.array.shadowrun6_skill_list, context = it)
                    .keys
                    .map { Helper.capitalizeNewsLetterStyle(it)}
            } ?: listOf()
    }
}