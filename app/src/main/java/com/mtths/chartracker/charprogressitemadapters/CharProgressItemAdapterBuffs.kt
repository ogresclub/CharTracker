package com.mtths.chartracker.charprogressitemadapters

import android.content.Context
import android.graphics.Typeface
import android.os.Handler
import android.os.Looper
import android.text.InputType
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.view.size
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.Buff
import com.mtths.chartracker.dataclasses.BuffEffect
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.dialogs.DialogEditLogEntry
import com.mtths.chartracker.utils.Helper
import com.pedromassango.doubleclick.DoubleClick
import com.pedromassango.doubleclick.DoubleClickListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.collections.ArrayList

/**
 * this is not really a charProgressItemAdapter since Buffs are not added via LogEntries
 * but are directly stored in character and created via UI.
 */
class CharProgressItemAdapterBuffs(
    mActivity: ProgressBarFrameDropBoxActivity?
) : CharProgressItemAdapter<Buff>(
    comparator = compareBy<Buff> {!it.modifiable}.thenBy { it.type }.thenBy { it.name }, mActivity = mActivity),
    CharProgressItemAdapterBuffable {

    val layoutResourceEntryBuffEffect = R.layout.list_view_item_buff_effect
    val layoutResourceEntryBuff = R.layout.list_view_item_buff
    val layoutResourceDescription = R.layout.buff_description
    override val layoutResourceMain = R.layout.char_progress_item_container_addable_bordered

    override fun createAutoCompleteStrings(): ArrayList<String> {
        return super.createAutoCompleteStrings().also {
            addBuffAutoCompleteString(it)
        }
    }

    /*
   When increase and decrease Buttons gets pressed, only after short delay the display
   should be refreshed to give you time to multiclick the button often without lag
   */
    val reloadAfterValueChangeTimer = 1200
    val refreshAfterValueChangeHandler = Handler(Looper.getMainLooper())
    var refreshDisplays: Runnable = Runnable {
//        Toast.makeText(context, "refreshing displays", Toast.LENGTH_SHORT).show()
        updateDisplay()
    }

    val buffEffectsArrayAdapter: ArrayAdapter<String>
        get() {
            return ArrayAdapter(
                context,
                R.layout.auto_complete_entry,
                mutableListOf<String>().apply {
                    try {
                        mActivity?.charProgressItemsMap?.let { cpiM ->
                            item?.character?.ruleSystems?.let { BuffEffect.getBuffableStats(it, cpiMap =  cpiM) }
                                ?.let { addAll(it) }
                        }
                    } catch (e: Exception) {
                        Helper.log("can't update displays after changing expand option for charProgressItem because activity is not activityCharDetails")
                        e.printStackTrace()
                    }
                }
            )
        }

    fun killRefreshdisplayTimer() {
        refreshAfterValueChangeHandler.removeCallbacks(refreshDisplays)
    }

    fun runRefreshDisplaysDelayed() {
        refreshAfterValueChangeHandler.postDelayed(refreshDisplays, reloadAfterValueChangeTimer.toLong())
    }


    override fun add(e: LogEntry): LogEntry {

        /*
        set name
         */

        val name = Helper.parseName(logEntryText = e.text, item = item)
        createBuffs(
            s = e.text,
            iData = CharProgressItemData(name = name).apply { relatedLogEntry = e },
            item = item,
            context = context)

        return e
    }

    fun setTextColor(view: TextView?, buff: Buff) {
        if (!buff.active) {
            TypedValue().let { tV ->
                context.theme.resolveAttribute(R.attr.textColorBuffInactive, tV, true)
                view?.setTextColor(tV.data)
            }
        } else {
            TypedValue().let { tV ->
                context.theme.resolveAttribute(android.R.attr.textColor, tV, true)
                view?.setTextColor(tV.data)
            }
        }
    }
    fun setText(view: TextView?, buff: Buff) {
        item?.character?.let { c ->
            view?.text = SpannableStringBuilder().apply {
                if (buff.type.isNotBlank() && (c.showBuffCategories || !visibleHeader(buff))) {
                    Helper.append(
                        sb = this,
                        text = "${buff.type.lowercase()} ",
                        what = listOf(
                            StyleSpan(Typeface.ITALIC),
                            ForegroundColorSpan(ContextCompat.getColor(context, R.color.text_color_logs))),
                        flags = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }
                Helper.append(
                    sb = this,
                    text = "${Helper.capitalizeNewsLetterStyle(buff.name)} ",
                    what = StyleSpan(Typeface.BOLD),
                    flags = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }
    }


    private fun updateValueDisplay(value_view: TextView, fx: BuffEffect) {
        value_view.text = fx.valueToString()
    }

    fun callOnCharStatusChangedAndUpdateViews() {
        killRefreshdisplayTimer()
        Log.d("TOGGLE_BUFFS", "${item?.character?.buffs?.joinToString { "${it.name}, ${it.active}, ${it.effects.joinToString()}" }}")
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                item?.character?.let { c ->
                    (mActivity as? ActivityCharDetails)?.onCharStatusChanged(
                        character = c,
                        keepLastUpdateDate = false,
                        updateDisplays = false)
                }
            }
            runRefreshDisplaysDelayed()
        }
    }

    fun getView(bufx: BuffEffect, parentBuff: Buff, holder: ViewHolder): View {
        val bview = LayoutInflater.from(mContext).inflate(layoutResourceEntryBuffEffect, null, false) as LinearLayout
        val value_view = bview.findViewById<TextView>(R.id.number)
        val name_view = bview.findViewById<TextView>(R.id.text)
        val plus_button = bview.findViewById<ImageView>(R.id.plus_button)
        val minus_button = bview.findViewById<ImageView>(R.id.minus_button)

        if (!parentBuff.modifiable) {
            listOf(plus_button, minus_button).forEach {
                it.visibility = View.INVISIBLE
            }
        }

        name_view.text = bufx.getNameDisplay()
        setTextColor(view = name_view, buff = parentBuff)
        updateValueDisplay(value_view, bufx)
        setTextColor(view = value_view, buff = parentBuff)

        name_view.setOnClickListener {
            parentBuff.active = !parentBuff.active
            updateBuffActivityInActivity(parentBuff)
            updateNameViewColors(buff = parentBuff, holder = holder)
            callOnCharStatusChangedAndUpdateViews()
        }

        plus_button.setOnClickListener {
            bufx.value++
            updateBuffEffectValueInActivityChar(buff = parentBuff, bfx = bufx)
            updateValueDisplay(value_view, bufx)
            callOnCharStatusChangedAndUpdateViews()


        }
        minus_button.setOnClickListener {
            bufx.value--
            updateBuffEffectValueInActivityChar(buff = parentBuff, bfx = bufx)
            updateValueDisplay(value_view, bufx)
            callOnCharStatusChangedAndUpdateViews()

        }

        minus_button.setOnLongClickListener {
            val msg = context.getString(R.string.msg_delete_buff_effect_question, bufx)
            mActivity?.supportFragmentManager?.let { it1 ->
                DialogCustom.showConfirmDialog(
                    fragmentManager = it1,
                    msg = msg,
                    onConfirm =  {
                        getActivityCharsBuff(parentBuff)?.effects?.remove(bufx)
                        item?.character?.let { c ->
                            (mActivity as? ActivityCharDetails)?.onCharStatusChanged(c)
                        }
                    },
                    onCancel = {})
            }

            return@setOnLongClickListener true
        }

        if (parentBuff.modifiable) {
            implementEditBuffEffectStat(bufx = bufx, name_view = name_view, parentBuff = parentBuff)
            implementEditBuffEffectValue(bufx, value_view)
        }

        return bview
    }

    class ViewHolder(
        var header: LinearLayout? = null,
        var container: LinearLayout? = null,
        var descriptionView: TextView? = null,
        var showHiddenButton: ImageView? = null,
        var nameView: TextView? = null,
        var effectNameViews: MutableList<TextView> = ArrayList(),
        var addBuffEffectButton: ImageView? = null,
        var expandButton: ImageView? = null,
        var line: View? = null
    )

    private fun fillData(holder: ViewHolder, buff: Buff) {

        holder.line?.visibility = if (data.indexOf(buff) == 0) {
            View.GONE
        } else {
            View.VISIBLE
        }

        implementHideBuff(holder = holder, buff = buff)
        holder.expandButton?.setImageDrawable(ContextCompat.getDrawable(context,
            if (buff.expand) {
                R.drawable.expanded_marker
            } else {
                R.drawable.expand_marker
            }
        ))
        setTextColor(view = holder.nameView, buff = buff)
        setText(view = holder.nameView, buff = buff)
        holder.descriptionView?.apply {
            text = buff.description
            visibility = if (buff.description.isBlank()) View.GONE else View.VISIBLE
        }
        implementToggleExpandOnClick(
            holder = holder,
            buff = buff,)
        implementToggleExpandDoubleClickAndToggleActiveOnClick(
            holder = holder,
            buff = buff)
        if (buff.modifiable) {

            implementEditBuffName(buff, holder.nameView)
            implementAddBuffEffect(view = holder.addBuffEffectButton, parentBuff = buff)

            if (buff.expand) {
                holder.addBuffEffectButton?.visibility = View.VISIBLE
            } else {
                holder.addBuffEffectButton?.visibility = View.INVISIBLE

            }
        } else {
            implementShowRelatedLog(view = holder.nameView, buff = buff)
            holder.addBuffEffectButton?.visibility = View.INVISIBLE
        }

        holder.container?.addView(holder.header)
    }

    private fun inflateBuffEffects(holder: ViewHolder, buff: Buff) {
        if (buff.expand) {
            buff.effects.forEach {effect ->
                val view = getView(effect, buff, holder)
                holder.container?.addView(view)
                holder.effectNameViews.add(view.findViewById(R.id.text))
            }
        }
    }

    private fun inflateDescription(holder: ViewHolder, buff: Buff) {
        if (buff.expand) {
            holder.container?.addView(holder.descriptionView)
        }
    }

    private fun fillDataAndInflateEffects(holder: ViewHolder, buff: Buff) {
        fillData(holder = holder, buff = buff)
        inflateBuffEffects(holder = holder, buff = buff)
        inflateDescription(holder = holder, buff = buff)
    }

    private fun newHeader(index: Int): Boolean {
        val buff = data[index]
        return (index == 0) || !buff.type.equals(data[index - 1].type, ignoreCase = true)
    }

    override fun doAfterGetView(listOfViews: LinearLayout?, index: Int) {
        super.doAfterGetView(listOfViews, index)
        val buff = data[index]
        if (newHeader(index) && !visibleHeader(buff)) {
            (listOfViews?.get(listOfViews.size - 1)?.tag as ViewHolder).line?.apply {
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    2*context.resources.getDimension(R.dimen.line).toInt()
                )
            }
        }
    }

    override fun doBeforeGetView(listOfViews: LinearLayout?, index: Int) {
        super.doBeforeGetView(listOfViews, index)

        val buff = data[index]

        fun addHeader(buff: Buff){
            val headerContainer = LayoutInflater.from(context).inflate(
                R.layout.buff_category,
                listOfViews,
                false
            ) as LinearLayout

            headerContainer.findViewById<TextView>(R.id.text).apply {
                var s = "%s".format(Helper.capitalizeNewsLetterStyle(buff.type))
                s += " (%d/%d):".format(
                        data.filter { buff.type.equals(it.type, ignoreCase = true) && it.active }.size,
                        getCategorySize(buff)
                    )
                text = s

            }

            headerContainer.findViewById<ImageView>(R.id.expand_button).apply {
                item?.character?.let { c ->
                    Helper.implementExpand(
                        key = buff.extendCategoryKey,
                        character = c,
                        expandButton = this,
                        context = context
                    ) {
                        Log.d("EXPAND", "inflating chardetails")
                        fillItems(
                            expand = item?.let {
                                it.character?.getExtend(
                                    charProgItemfilterConstraintId = it.filterConstraintId,
                                    context = context
                                )
                            } ?: true
                        )
                    }
                }
            }
            listOfViews?.addView(headerContainer)

        }

        val putHeader = visibleHeader(buff)


        if (newHeader(index) && putHeader && buff.type.isNotBlank()) {
            addHeader(buff)
        }
    }

    private fun getCategorySize(buff: Buff): Int {
        return data.filter { buff.type.equals(it.type, ignoreCase = true) }.size
    }

    private fun visibleHeader(buff: Buff): Boolean = buff.type.isNotBlank() &&
            getCategorySize(buff) >= MIN_NUM_BUFFS_FOR_HEADER

    override fun getView(index: Int, check_mistakes: Boolean): LinearLayout? {
        val buff = data[index]
        if (!buff.show(context = context, character = item?.character)) {
            return null
        }
        val h = ViewHolder()
        val inflater = LayoutInflater.from(mContext)

        h.container = LinearLayout(context).apply {
            orientation = LinearLayout.VERTICAL
        }
        h.descriptionView = inflater.inflate(layoutResourceDescription, h.container, false) as TextView
        LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        ).apply {
            bottomMargin = Helper.dpToPx(5f, context)
            h.container?.layoutParams = this
        }

        /*
        add header with buff name only if there are multiple effects to this buff or buff is not
         */
        h.header = inflater.inflate(layoutResourceEntryBuff, h.container, false) as LinearLayout
        val padding = Helper.spToPx(context = context, sp = if (visibleHeader(buff)) 10f else 5f)
        h.header?.setPadding(padding, 0, padding, 0)

        h.line = h.header?.findViewById(R.id.line)
        h.showHiddenButton = h.header?.findViewById(R.id.hide_button)
        h.nameView = h.header?.findViewById(R.id.text)
        h.addBuffEffectButton = h.header?.findViewById(R.id.add_button)
        h.expandButton = h.header?.findViewById(R.id.expand_button)


        fillDataAndInflateEffects(holder = h, buff = buff)

        h.container?.tag = h

        return h.container!!
    }

    private fun implementHideBuff(holder: ViewHolder, buff: Buff) {
        holder.showHiddenButton?.apply {
            setShowHiddenBuffsIcon(iv = this, show = !buff.hide)
            visibility = if (buff.expand || buff.hide) View.VISIBLE else View.GONE
            setOnClickListener {
                item?.character?.let { c ->
                    getActivityCharsBuff(buff)?.apply {
                        hide = !hide
                    }
                    item?.filterConstraintId?.let { fCId ->
                        fillItems(
                            expand = c.getExtend(
                                charProgItemfilterConstraintId = fCId,
                                context = context
                            )
                        )
                    }
                    (mActivity as? ActivityCharDetails)?.onCharStatusChanged(
                        character = c,
                        keepLastUpdateDate = true,
                        updateDisplays = false)
                }

            }
        }
    }

    fun toggleExpand(buff: Buff, holder: ViewHolder) {
        buff.expand = !(buff.expand)
        item?.character?.let { c ->
            (mActivity as? ActivityCharDetails)?.onCharStatusChanged(
                character = c,
                keepLastUpdateDate = true,
                updateDisplays = false)
        }
        updateBuffData(holder = holder, buff = buff)
    }

    private fun updateBuffData(buff: Buff, holder: ViewHolder) {
        holder.container?.removeAllViews()
        fillDataAndInflateEffects(holder = holder, buff = buff)
    }



    private fun implementToggleExpandOnClick(holder: ViewHolder, buff: Buff) {
        holder.expandButton?.setOnClickListener {
            toggleExpand(holder = holder, buff = buff)
        }
    }

    private fun updateNameViewColors(buff: Buff, holder: ViewHolder) {
        /*
        set text color of buff name and effects according to active state
         */
        setTextColor(
            view = holder.nameView,
            buff = buff
        )
        holder.effectNameViews.forEach {
            setTextColor(
                view = it,
                buff = buff
            )
        }
    }

    private fun implementToggleExpandDoubleClickAndToggleActiveOnClick(
        holder: ViewHolder,
        buff: Buff) {

        holder.nameView?.setOnClickListener(DoubleClick(object : DoubleClickListener {
            override fun onDoubleClick(view: View?) {
                toggleExpand(buff = buff, holder = holder)
            }

            override fun onSingleClick(view: View?) {
                buff.active = !buff.active
//                Log.d("TOGGLE_BUFFS", "toogled buff ${buff.name} activity to ${buff.active}")
                updateBuffActivityInActivity(buff)
                updateNameViewColors(buff = buff, holder = holder)
                callOnCharStatusChangedAndUpdateViews()
            }
        }))
    }

    fun updateBuffActivityInActivity(buff: Buff) {
        getActivityCharsBuff(buff)?.active = buff.active
    }

    fun updateBuffEffectValueInActivityChar(buff: Buff, bfx: BuffEffect) {
        getActivityCharsBuff(buff)?.effects?.find { it.stat == bfx.stat }?.value = bfx.value
    }
    fun getActivityCharsBuff(buff: Buff): Buff? {
        return item?.character?.buffs?.find { it.name == buff.name }
    }

    fun implementAddBuffEffect(view: View?, parentBuff: Buff) {
        view?.setOnClickListener {

            val dialog = DialogCustom<String>()
            dialog.setDefaultInput()
            dialog.setMessage(R.string.buff_effect_usage_explanation)
            dialog.setTitle(R.string.add_buff_effect)
            dialog.setEditText2Adapter(adapter = buffEffectsArrayAdapter, alwaysShow = true)
            dialog.setPositiveButton(context.getString(R.string.ok)
            ) {
                val input = dialog.text2.text.toString().trim()
                val nBuffEffect = Buff.parseBuffEffect(input)
                item?.character?.let { c ->
                    Log.d("BUFFY", "new buff effect: ${nBuffEffect.stat}:${nBuffEffect.value}")
                    if (nBuffEffect.stat.lowercase() !in parentBuff.effects.map { it.stat.lowercase() }) {
                        getActivityCharsBuff(parentBuff)?.effects?.add(nBuffEffect)
                        (mActivity as? ActivityCharDetails)?.onCharStatusChanged(c)
                        dialog.dismiss()
                    } else {
                        Toast.makeText(context, R.string.msg_stat_already_buffed_in_buff, Toast.LENGTH_SHORT).show()
                    }
                }

            }
            mActivity?.supportFragmentManager?.let { fm ->
                dialog.show(fm, "custom_dialog_prompt_filename")}

        }
    }

    fun implementEditBuffEffectValue(bufx: BuffEffect, value_view: View) {
        value_view.setOnLongClickListener {
            mActivity?.supportFragmentManager?.let { fm ->

                var switch: SwitchCompat?

                DialogCustom.showNumberPrompt(
                    fragmentManager = fm,
                    message = context.getString(R.string.label_new_value),
                    context = context,
                    initialValue = bufx.value,
                    inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED,
                    customView = LayoutInflater.from(context).inflate(
                        R.layout.settings_switch_nobg,
                        null
                    ).also {
                        (it as SwitchCompat).apply {
                            switch = it
                            setText(R.string.replace_label)
                            isChecked = bufx.replace
                        }

                    }
                ) { newValue ->
                    bufx.value = newValue
                    switch?.isChecked?.let { bufx.replace = it }
                    item?.character?.let { c ->
                        (mActivity as? ActivityCharDetails)?.onCharStatusChanged(c)
                    }
                }
            }
            return@setOnLongClickListener true
        }

    }

    fun implementShowRelatedLog(view: View?, buff: Buff) {
        view?.setOnLongClickListener {
            Log.e("BUFFF", "show related log (id = ${buff.logid}) for buff '$buff'")
            buff.logid?.let { id ->
                mActivity?.supportFragmentManager?.let { fm ->
                    DialogEditLogEntry.newInstance(id).show(
                        fm, "buffAdapter_EditLogEntry")
                }
            }
            return@setOnLongClickListener true
        }
    }

    fun toast(msgResId: Int) {
        Toast.makeText(context,
            context.getString(msgResId),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun createEditTextDescription(text: String): EditText =
        EditText(context).apply {
            layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            setHintTextColor(ContextCompat.getColor(context, android.R.color.holo_green_dark))
            setHint(R.string.description_label)
            if (text.isNotBlank()) {
                setText(text)
            }
        }


    private fun implementEditBuffName(buff: Buff, name_view: View?) {

        name_view?.setOnLongClickListener {
            val dialog = DialogCustom<String>()
            dialog.setInput(
                editText1Visible = true,
                hint1StringResId = R.string.category_label,
                hint2StringResId = R.string.label_name)

            dialog.setTextInputType(
                text_1_inputType = InputType.TYPE_CLASS_TEXT,
                text_2_inputType = InputType.TYPE_CLASS_TEXT)
            dialog.setTitle(R.string.header_change_buff_category_and_name)
            dialog.setInputText(
                text1Text = buff.type,
                text2Text = buff.name)
            dialog.setEditText2Adapter(adapter = buffEffectsArrayAdapter, alwaysShow = true)
            val descriptionET = createEditTextDescription(buff.description)
            dialog.setView(descriptionET)
            dialog.setPositiveButton(R.string.ok
            ) {
                val newName = dialog.text2.text.toString().trim()
                val newType = dialog.text1.text.toString().trim()
                val newDescription = descriptionET.text.toString().trim()

                if (newName.isNotBlank()) {
                    item?.character?.let { c ->

                        val nameChanged = !newName.equals(buff.name, ignoreCase = true)
                        val typeChanged = !newType.equals(buff.type, ignoreCase = true)
                        val descriptionChanged = !newDescription.equals(buff.description, ignoreCase = true)

                        // if nothing changed, simply dismiss
                        if (!nameChanged && !typeChanged && !descriptionChanged) {
                            dialog.dismiss()

                        } else {
                            val nameAlreadyExists = newName.lowercase() in c.buffs.map { it.name.lowercase() }

                            //deal with forbidden cases
                            when {
                                (nameChanged && nameAlreadyExists && !typeChanged && !descriptionChanged) -> {
                                    toast(R.string.msg_buff_name_already_exists)
                                }
                                newName.isBlank() -> {
                                    toast(R.string.msg_invalid_buff_name)
                                }
                                else -> {
                                    //legit state
                                    if (nameChanged && !nameAlreadyExists) {
                                        c.replaceBuffNameInBuffStates(oldName = buff.name, newName = newName)
                                        buff.name = newName
                                    }
                                    buff.description = newDescription
                                    if (typeChanged) {
                                        buff.type = newType
                                    }

                                    item?.character?.let { c ->
                                        (mActivity as? ActivityCharDetails)?.onCharStatusChanged(c)
                                    }
                                    dialog.dismiss()
                                }
                            }
                        }
                    }
                }
            }
            dialog.setNegativeButton(text = "DELETE") {
                item?.character?.let { c ->
                    c.removeBuffInBuffStates(buff.name)
                    c.buffs.remove(buff)
                    (mActivity as? ActivityCharDetails)?.onCharStatusChanged(c)
                    dialog.dismiss()
                }
            }
            dialog.setNegativeButtonTextColor(color = ContextCompat.getColor(context,R.color.dark_red))
            mActivity?.supportFragmentManager?.let { fm ->
                dialog.show(fm, "custom_dialog_prompt_filename")}
            return@setOnLongClickListener true
        }
    }

    fun implementEditBuffEffectStat(bufx: BuffEffect, name_view: View, parentBuff: Buff) {
        name_view.setOnLongClickListener {
            val dialog = DialogCustom<String>()
            dialog.setDefaultInput()
            dialog.setTitle(R.string.msg_change_buff_stat_dialog)
            dialog.setInputText(text1Text = "", text2Text = bufx.stat)
            dialog.setEditText2Adapter(adapter = buffEffectsArrayAdapter, alwaysShow = true)
            dialog.setPositiveButton("OK"
            ) {
                val newName = dialog.text2.text.toString().trim()
                if (newName.isNotBlank()) {
                    item?.character?.let { c ->
                        if (newName.lowercase() !in parentBuff.effects.map { it.stat.lowercase() }) {
                            bufx.stat = newName
                            (mActivity as? ActivityCharDetails)?.onCharStatusChanged(c)
                            dialog.dismiss()
                        } else {
                            Toast.makeText(context, R.string.msg_stat_already_exists_in_this_buff, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
            dialog.setNegativeButton(context.getString(R.string.label_delete).uppercase()) {
                item?.character?.let { c ->
                    parentBuff.effects.remove(bufx)
                    if (parentBuff.effects.isEmpty()) {
                        c.buffs.remove(parentBuff)
                    }
                    (mActivity as? ActivityCharDetails)?.onCharStatusChanged(c)
                    dialog.dismiss()
                }
            }
            dialog.setNegativeButtonTextColor(color = ContextCompat.getColor(context,R.color.dark_red))
            mActivity?.supportFragmentManager?.let { fm ->
                dialog.show(fm, "custom_dialog_prompt_filename")}
            return@setOnLongClickListener true
        }
    }

    override fun generate_autocomplete_data(e: LogEntry) {
    }

    fun updateDisplay() {
        updateAllDisplays(generateCPI = false)
    }

    private fun implementCreateBuff() {
        itemsContainer?.findViewById<ImageView>(R.id.add_button)?.setOnClickListener {


            val dialog = DialogCustom<String>()
            dialog.setInput(
                editText1Visible = true,
                editText2Visible = true,
                hint2StringResId = R.string.category_label,
                hint1StringResId = R.string.label_name)
            dialog.setTextInputType(
                text_1_inputType = InputType.TYPE_CLASS_TEXT,
                text_2_inputType = InputType.TYPE_CLASS_TEXT
            )
            val descriptionET = createEditTextDescription("")
            dialog.setView(descriptionET)
            dialog.setTitle(R.string.header_enter_buff_category_and_name)
            dialog.setMessage(R.string.msg_add_buff)
            dialog.setPositiveButton(
                context.getString(R.string.ok)
            ) {
                val input = dialog.text1.text.toString().trim()
                val category = dialog.text2.text.toString().trim().ifBlank { Buff.DEFAULT_TYPE }
                val description = descriptionET.text.toString().trim()
                item?.character?.let { c ->
                    createBuffFromText(
                        s = input,
                        category = category,
                        description = description,
                        character = c,
                        context = context,
                        onBuffCreated = {
                            updateDisplay()
                            dialog.dismiss()
                        }
                    )
                }

            }
            mActivity?.supportFragmentManager?.let { fm ->
                dialog.show(fm, "custom_dialog_prompt_filename")
            }
        }
    }

    private fun implementToggleShowHidden(view: View?) {
        view?.setOnClickListener {
            item?.character?.let { c ->
                c.toggleShowHiddenBuffs()
                (mActivity as? ActivityCharDetails)?.onCharStatusChanged(
                    character = c,
                    keepLastUpdateDate = true)
            }
        }
    }
    private fun implementToggleShowBuffCategories(view: View?) {
        view?.setOnLongClickListener {
            item?.character?.let { c ->
                c.toggleShowBuffCategories()
                (mActivity as? ActivityCharDetails)?.onCharStatusChanged(
                    character = c,
                    keepLastUpdateDate = true)
            }
            return@setOnLongClickListener true
        }
    }

    private fun setShowHiddenBuffsIcon(iv: ImageView?, show: Boolean) {
        iv?.setImageResource(if (show) R.drawable.show else R.drawable.hide)
    }

    override fun fillItems(expand: Boolean) {
        item?.character?.let {c ->
            c.buffs.let {
                if (c.showHiddenBuffs) {
                    it
                } else {
                    it.filterNot { it.hide  }
                }
            }.let {
                data = ArrayList(it)
            }
            val showHiddenButton = itemsContainer?.findViewById<ImageView>(R.id.show_hidden_button)
            val numHiddenBuffsDisplay = itemsContainer?.findViewById<TextView>(R.id.num_hidden_label)
            implementToggleShowHidden(showHiddenButton)
            implementToggleShowBuffCategories(showHiddenButton)
            setShowHiddenBuffsIcon(iv = showHiddenButton, show = c.showHiddenBuffs)
            displayNumBuffsIfCollapsed(expand)
            displayNumHiddenBuffs(textView = numHiddenBuffsDisplay, character = c)
            implementCreateBuff()
        }
        super.fillItems(expand)
        //Buff should be always visible, because they are created here and not via logEntries
        itemsContainer?.visibility = View.VISIBLE
    }


    fun displayNumBuffsIfCollapsed(expand: Boolean) {
        if (!expand) {
            header_view?.text = "%s (%d)".format(item?.header, count)
        }
    }
    fun displayNumHiddenBuffs(textView: TextView?, character: Character) {
        textView?.text = if (character.showHiddenBuffs) {
            ""
        } else {
            character.buffs.count { it.hide }.toString()
        }

    }


    companion object {
        const val BUFF_REF = "buffs"
        const val MIN_NUM_BUFFS_FOR_HEADER = 3

        /**
         * s = buffname[stat1 value1, stat2 value2, ...]
         */
        fun createBuffFromText(
            s: String,
            category: String?,
            description: String = "",
            character: Character,
            context: Context,
            onBuffCreated: () -> Unit,
            onBuffAlreadyExists: () -> Unit = {
                Toast.makeText(context, "Buff with same name already exists", Toast.LENGTH_SHORT).show()
            }) {
            val nBuff = Buff.parseBuffFromText(s).apply {
                active = true
                modifiable = true
                expand = true
                this.description = description
                category?.let { this.type = category }
            }
            if (nBuff.name.lowercase() !in character.buffs.map { it.name.lowercase() }) {
                character.buffs.add(nBuff)
                Log.d("BUFFF", "created Buff: $nBuff")
                DatabaseHelper.getInstance(context)
                    ?.updateCharacter(character, keep_last_update_date = false)
                onBuffCreated()
            } else {
                onBuffAlreadyExists()
            }
        }

    }
}