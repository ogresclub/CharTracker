package com.mtths.chartracker.charprogressitemadapters

import android.graphics.Color
import android.graphics.Typeface
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterCharacter.CharProgCharacter
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterStd.Companion.END_MARKER
import com.mtths.chartracker.utils.Helper.append
import com.mtths.chartracker.utils.Helper.capitalize
import com.mtths.chartracker.utils.Helper.getTextAfterStringAndTrim
import com.mtths.chartracker.utils.Helper.getTextBeforeFirstAt
import com.mtths.chartracker.utils.Helper.getTextBeforeString
import com.mtths.chartracker.adapters.LogAdapter
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.LongClickLinkMovementMethod
import com.mtths.chartracker.utils.LongClickableSpan
import java.util.*
import kotlin.collections.ArrayList


open class CharProgressItemAdapterCharacter (activity: ProgressBarFrameDropBoxActivity?,
                                             comparator: Comparator<CharProgCharacter>,
                                             var namesToAutoComplete: ArrayList<LogAdapter.ColoredString>) :

    CharProgressItemAdapter<CharProgCharacter>(
        mActivity = activity,
        comparator = comparator
    ) {

    open val color = R.color.charachter_name_color

    override var layoutResourceMain = R.layout.char_progress_item_container_session_stats
    val layoutResourceEntry = R.layout.list_view_item_char_progress_string


    val DEBUG_TAG = "CHAR_PROG_CHAR"
    override fun createAutoCompleteFeatureMarkers(): ArrayList<String> {
        return ArrayList(listOf(HIDE_CHARACTER_INDICATOR, DEAD_CHARACTER_INDICATOR))
    }

    override fun add(e: LogEntry): LogEntry {
        add(e = e, addToAutoCompleteData = false)
        return e
    }

    override fun generate_autocomplete_data(e: LogEntry) {
        add(e = e, addToAutoCompleteData = true)
    }

    /**
     * Chars should be tracked like @Character George // Short Description // bla bla bla (the last part is optional)
     * @param e
     * @param addToAutoCompleteData autocomplete data is only added the the global list of stuff
     * to autocomplete in fragmentMainScreen. In fragmentCharDetails we don't need to collect
     * this data again, because we want to autocomplete also names that are not in log entries
     * related to this character
     * @return
     */
    fun add(e: LogEntry, addToAutoCompleteData: Boolean = false): LogEntry {

        var textAfterFilterConstraint = updateFilterConstraintToCurrentLanguage(e.text)

        item?.filterConstraint?.let { filterConstraint ->

            while (textAfterFilterConstraint.contains(filterConstraint)) {
                textAfterFilterConstraint =
                    getTextAfterStringAndTrim(textAfterFilterConstraint, filterConstraint)
                val text = getTextBeforeString(textAfterFilterConstraint, filterConstraint)
//                Helper.log(DEBUG_TAG, "text: '$text'")

                val character = CharProgCharacter()
                character.logEntry = e
                character.parseName(text)?.let {
//                    Helper.log(DEBUG_TAG, "text after name parsing: '$it'")
                    character.parseDescription(textAfterFirstEndMarker = it)

                } ?: run {
                    //            Helper.log(DEBUG_TAG, "found NO Double Slash: -> Old Way");
                    //no double slashes are found so we use old notation: name @Character short description
                    character.name = getTextBeforeFirstAt(e.text)
                    if (character.name.isBlank()) {
                        character.name = "????"
                    }
                    character.shortDescriptions.add(text)
                }

                /*
                try to check item already exists, if not create new one
                 */
                getChar(character.name)?.also { exChar ->
                    /*
                    existing item was found
                     */
                    //extend short description
                    exChar.shortDescriptions.addAll(character.shortDescriptions)

                    // add new aliases
                    exChar.addNames(character.names)
                    exChar.addShorts(character.shorts)
                    exChar.dead = exChar.dead || character.dead
                    exChar.hide = exChar.hide || character.hide
                } ?: run {
                    /*
                    item does not yet exist
                     */
                    data.add(character)
                    auto_complete_data.add(character)


                    ///////////////////////////////////////
                    if (addToAutoCompleteData) {
                        val allRefs = ArrayList<String>().apply {
                            addAll(character.names)
                            addAll(character.shorts)
                        }
                        namesToAutoComplete.add(
                            LogAdapter.ColoredString(names = allRefs, color = color)
                        )

                    }

                }
            }
        }
        return e
    }

    private fun getChar(name: String): CharProgCharacter? {
        return try {
            data.first { it.names.map { it.lowercase() }.contains(name.lowercase()) }
        } catch (e: NoSuchElementException) {
            null
        }
    }

    open var showAllItems: Boolean
        get() = PrefsHelper(context).showAllCharsSessionStats
        set(value) {
            PrefsHelper(context).showAllCharsSessionStats = value
        }

    open var showDescription: Boolean
        get() =  PrefsHelper(context).showCharactersDescriptionsInSessionStats
        set(value) {
            PrefsHelper(context).showCharactersDescriptionsInSessionStats = value
        }


    override fun getView(index: Int, check_mistakes: Boolean): View? {
        // damit auch im mainscreen, wo wo die daten in auto_complete_data gespeichert werden
        // was angzegt wird, soll auto_complete date verwendet werden, wenn data leer ist!

        val character = data[index]
        var entry_view: TextView? = null
        if (!character.hide || showAllItems) {
            entry_view = LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false) as TextView
            val params =
                LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 0, 0, MARGIN_BOTTOM)
            entry_view.layoutParams = params

            entry_view.text = character.getDisplaySpannable()
            entry_view.setTextColor(ContextCompat.getColor(context, R.color.text_color_logs))
            if (character.dead) {
                entry_view.setBackgroundResource(R.drawable.holo_red_half_transparent_round_corners)
            } else {
                entry_view.setBackgroundResource(R.drawable.light_grey_half_transparent_round_corners)
            }
            entry_view.setOnLongClickListener {
                showDialogFilteredLogs(character, mActivity)
                true
            }
        }
        return entry_view
    }




    override fun sort() {
        when (PrefsHelper(context).sortCharactersSessionStats) {
            PrefsHelper.PREF_SESSION_STATS_SORT_CHARACTERS_ALPHABETICAL -> {
//                Toast.makeText(context, "sort alpha", Toast.LENGTH_LONG).show()
                data.sortWith(compareBy ({ it.hide }, { it.name }))
            }
            PrefsHelper.PREF_SESSION_STATS_SORT_CHARACTERS_HISTORY -> {
                data.sortWith(compareByDescending{ it.logEntry?.dateOfCreation })
//                Toast.makeText(context, "sort date", Toast.LENGTH_LONG).show()
            }
            else -> {
                super.sort()
            }
        }
    }

    fun refresh() {
        (mActivity as? ActivityMain?)?.getSessionDataDisplay()?.fill()
        (mActivity as? ActivityCharDetails?)?.fragmentSessionData?.fill()
    }

    open fun setHeaderOnClickListeners(v: View?) {
        header_view?.setOnLongClickListener {
            showAllItems = !showAllItems
//            Toast.makeText(context, "show all chars = ${Global.showAllChars}", Toast.LENGTH_LONG).show()
            refresh()
            true
        }
        header_view?.setOnClickListener {
            showDescription = !showDescription
            refresh()
        }

        val iconSortAlphabetically = v?.findViewById<ImageView>(R.id.icon_sort_alphabetically)
        val iconSortHistory = v?.findViewById<ImageView>(R.id.icon_sort_history)

        iconSortAlphabetically?.setOnClickListener {
            Log.d("SORT_SESSION_STATS", "sort characters alphabetically")

            PrefsHelper(context).sortCharactersSessionStats =
                PrefsHelper.PREF_SESSION_STATS_SORT_CHARACTERS_ALPHABETICAL
            sort()
            refresh()

        }
        iconSortHistory?.setOnClickListener {
            Log.d("SORT_SESSION_STATS", "sort characters history")

            PrefsHelper(context).sortCharactersSessionStats =
                PrefsHelper.PREF_SESSION_STATS_SORT_CHARACTERS_HISTORY
            sort()
            refresh()
        }
    }


    override fun inflate(expand: Boolean): View? {
        val v = super.inflate(expand = expand)

        if (!showDescription) {
            inflateAsTextView()
        }

        if (data.isEmpty()) {
            inflateTextView().apply {
                setText(R.string.none)
            }.also {
                listOfViews?.addView(it)
            }
        }


//        listOfViews?.setBackgroundResource(R.drawable.light_grey_half_transparent_round_corners)

        setHeaderOnClickListeners(v)
        return v

    }

    fun inflateTextView(): TextView {
        return LayoutInflater.from(mContext)
            .inflate(R.layout.list_view_item_session_data, null, false) as TextView
    }

    /**
     * characters are sorted by start letter of their name
     */
    fun inflateAsTextView() {

        listOfViews?.removeAllViews()


        if (data.size != 0) {
            val itemsToDisplay = if (showAllItems) data else data.filter { !it.hide }
            val allNames = itemsToDisplay.map { it.names }.flatten().distinct().sorted()
            val startLetters = allNames.map { it.first() }.distinct()
            for (x in startLetters) {
//                log("SESSION_DATA", "start letter: $x")
                val tv = inflateTextView()
                val params =
                    LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                params.setMargins(0, 0, 0, MARGIN_BOTTOM)
                tv.layoutParams = params
                val itemNamesWithSameStart = allNames.filter { it.first().equals(x, ignoreCase = true) }
                if (itemNamesWithSameStart.isNotEmpty()) {
                    val itemsListAsString = itemNamesWithSameStart.joinToString { it }
//                    log("SESSION_DATA", "items list as string: $itemsListAsString")
                    val spanString = SpannableString(itemsListAsString)
                    for (name in itemNamesWithSameStart) {
                        getChar(name)?.let { item ->
                            val clickableSpan = object : LongClickableSpan() {
                                override fun onClick(widget: View) {
                                    mActivity?.supportFragmentManager?.let { fragmentManager ->
                                        DialogCustom.newInstance().apply {
                                            setTitle(item.getDisplaySpannable(forceShowDescription = true))
                                            setMessageOnClickListener {
                                                this.dismiss()
                                                showDialogFilteredLogs(item = item, activity = mActivity)
                                            }
                                            setPositiveButton("")
                                        }.show(fragmentManager, "showSessionData_${name}")
                                    }
                                }

                                override fun onLongClick(view: View?) {
                                    showDialogFilteredLogs(item = item, activity = mActivity)
                                }
                            }

                            val start = itemsListAsString.indexOf(name)
                            spanString.setSpan(
                                clickableSpan,
                                start,
                                start + name.length,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                            )
                            val color = if (item.hide) {
                                ContextCompat.getColor(
                                    context,
                                    R.color.text_highlight_color_char_progress
                                )
                            } else {
                                Color.BLACK
                            }
                            spanString.setSpan(
                                ForegroundColorSpan(color),
                                start,
                                start + name.length,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                            )
                            if (item.dead) {
                                spanString.setSpan(
                                    BackgroundColorSpan(ContextCompat.getColor(
                                        context,
                                        BG_COLOR_DEAD
                                    )),
                                    start,
                                    start + name.length,
                                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                                )
                            }
                        }
                    }
                    tv.text = spanString
                    tv.movementMethod = LongClickLinkMovementMethod.instance
                    tv.setBackgroundResource(R.drawable.light_grey_half_transparent_round_corners)
                }
                listOfViews?.addView(tv)
            }
        }
    }

    fun showDialogFilteredLogs(item: CharProgCharacter, activity: FragmentActivity?) {
        activity?.supportFragmentManager?.let { fm ->
            var searchString = "full=true ${item.names.joinToString(separator = " OR "){"\"$it\""}}"
            if (item.shorts.isNotEmpty()) searchString += " OR ${item.shorts.joinToString(" OR "){"\"$it\""}}"
            DialogShowFilteredLogs.newInstance(
                searchString = searchString,
                activity_char_details = false,
                use_session_logs = true
            )
                .show(fm, "showCharacter_${item.name}")
        }

    }

    inner class CharProgCharacter {
        var name: String
            get() = if (names.isNotEmpty()) names.first() else ""
            set(value) {
                names = arrayListOf(value)
            }

        var names: MutableList<String> = ArrayList()
        var shorts: MutableList<String> = ArrayList()
        var shortDescriptions: ArrayList<String> = ArrayList()
        var shortDescription: String = ""
            get() {
                var result = ""
                shortDescriptions.forEach {
                    result += capitalize(it) + "\n"
                }
                result.trim()
//                log("SESSION_DATA", "short description: '$result'")
                return result.trim()
            }

        var logEntry: LogEntry? = null
        var hide = false
        var dead = false

        fun addNames(newNames: List<String>) {
            newNames.forEach {
                if (it.lowercase() !in names.map { it.lowercase() }) {
                    names.add(it)
                }
            }
        }
        fun addShorts(newShorts: List<String>) {
            newShorts.forEach {
                if (it.lowercase() !in names.map { it.lowercase() }) {
                    shorts.add(it)
                }
            }
        }

        /**
         * return the rest of the string after the first double slash
         * or null if no double slash was found
         * splitting into aliases and shorts
         */
        fun parseName(text: String): String? {
            val indexOfDoubleSlash = text.indexOf(END_MARKER)
            var namesString = text.trim()
            var result: String? = null

            if (indexOfDoubleSlash > 0) {
                namesString = namesString.substring(0, indexOfDoubleSlash)
                result = text.substring(indexOfDoubleSlash + 2)
            } else if (text.isNotBlank()) {
                result = ""
            }

            if (result != null) {
                val mNames = namesString
                    .split("alias", ignoreCase = true)
                    .map { it.trim() }
                    .toMutableList()
                mNames.forEach { name ->
                    var list = name.split("short", ignoreCase = true).map { it.trim() }
                    names.add(list[0])
                    list = list.drop(1)
                    shorts.addAll(list)
                }

            }
            return result
        }

        fun parseDescription(textAfterFirstEndMarker: String) {
            val indexOfSecondEndMarker = textAfterFirstEndMarker.indexOf(END_MARKER)

            var descr = if (indexOfSecondEndMarker >= 0) {
//                log(DEBUG_TAG, "found second Double Slash:");
                textAfterFirstEndMarker
                    .substring(0, indexOfSecondEndMarker)
                    .trim()
//                    .also { log(DEBUG_TAG, "short description = $it") }

            } else {
                textAfterFirstEndMarker.substringBefore("@")
//                    .also {log(DEBUG_TAG, "NOT found second Double Slash: '$it'"); }
            }
            if (descr.contains(HIDE_CHARACTER_INDICATOR)) {
                hide = true
            }
            if (descr.contains(DEAD_CHARACTER_INDICATOR)) {
                dead = true
            }
//            log(DEBUG_TAG, "@Character ${name}: $descr")

            descr = descr
                .replace(HIDE_CHARACTER_INDICATOR, "")
                .replace(DEAD_CHARACTER_INDICATOR, "")
                .trim()
            if (descr.isNotBlank()) {
                shortDescriptions.add(descr)
            }

        }

        fun getDisplaySpannable(forceShowDescription: Boolean = false): SpannableStringBuilder {

            var sb = SpannableStringBuilder()

            names.forEachIndexed { index, n ->
                var nameStyle: Any = StyleSpan(Typeface.BOLD)
                if (hide) {
                    nameStyle = StyleSpan(Typeface.BOLD_ITALIC)
                }
                val styles: MutableList<Any?> = mutableListOf(nameStyle)
                append(sb, n, styles, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                if (index < names.size - 1) {
                    sb.append(" alias ")                }
            }
            if (showDescription || forceShowDescription) {
                if (hide) {
                    append(sb, ": ", StyleSpan(Typeface.ITALIC), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                    append(
                        sb,
                        shortDescription,
                        StyleSpan(Typeface.ITALIC),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )


                } else {
                    sb.append(": ")
                    sb.append(shortDescription)

                }
            }

            if (hide) {
                sb = Helper.setColor(
                    arrayListOf(sb.toString()),
                    sb.toString(),
                    ContextCompat.getColor(context, R.color.text_highlight_color_char_progress),
                    sb
                )
            }

//            Helper.log("SESSION_DATA: get display spannable: '$sb'")
            return sb

        }
    }

    companion object {
        const val MARGIN_BOTTOM = 3
        const val HIDE_CHARACTER_INDICATOR = "-hide"
        const val DEAD_CHARACTER_INDICATOR = "-dead"
        const val BG_COLOR_DEAD = R.color.holo_red_light_half_transparent
        val NAME_COMPARATOR: Comparator<CharProgCharacter> = object : Comparator<CharProgCharacter> {
            override fun compare(o1: CharProgCharacter, o2: CharProgCharacter): Int {
                return o1.name.compareTo(o2.name, ignoreCase = true)
            }
        }
    }
}