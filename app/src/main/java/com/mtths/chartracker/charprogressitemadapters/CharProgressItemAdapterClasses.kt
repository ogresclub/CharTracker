package com.mtths.chartracker.charprogressitemadapters

import android.view.View
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.ValueNamePair

class CharProgressItemAdapterClasses(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapterValueBased(
        activity = activity,
        comparator = compareBy { it.name },
        mCalculator = object : Calculator {
            override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
                return 0
            }
        }) {

    override fun inflate(expand: Boolean): View? {
        return null
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        return null
    }

    fun getTotalLevel(): Int? {
        when (data.size) {
            0 -> return item?.character?._level ?: 1
            1 -> {
                return data[0].value
            }
            2 -> {
                getValueNamePair(name = "", data = data)?.value?.let {
                    return data.map { it.value }.maxOrNull()
                }

            }
        }
        return data.filter { it.name.isNotEmpty() }.map { it.value }.sum()
    }

    fun getUndefinedLevelDisplayPair(): ValueNamePair? {
        val vnp = getValueNamePair(name = "", data = data)

        vnp?.name = item?.character?.dndClass?.name?.let {
            if (it.isNotBlank()) it else Character.UNDEFINED
        }?: Character.UNDEFINED
        return vnp
    }

    private fun getUndefinedValuePair(): ValueNamePair? {
        return getValueNamePair(name = "", data = data)
    }

    fun getDisplayString(vnps: List<ValueNamePair>): String {
        return vnps.joinToString(separator = "/ ") {
            String.format("%s %d", it.name.replace(Character.UNDEFINED, ""), it.value) }
    }

    fun getClassesAndLevels (): List<ValueNamePair> {
        when (data.size) {
            0 -> return ArrayList()
            1 -> return listOf(getUndefinedLevelDisplayPair()?: data[0])
            2 -> {
                getUndefinedValuePair()?.value?.let {
                    //this means if is actually only one class
                    // and we take max of levels as displayed level
                    val level = data.map { it.value }.maxOrNull() ?: -1
                    val className = data.filter { it.name.isNotEmpty() }[0].name
                    return listOf(ValueNamePair(name = className, value = level, mCalculator = mCalculator))
                }
            }
        }
        return data.filter { it.name.isNotBlank() }
    }

    init {
        stdAutoCompleteData = HashMap()
        stdAutoCompleteData[RuleSystem.rule_system_ogres_club] = DatabaseHelper(context).allClasses.map { it.name }
        stdAutoCompleteData[RuleSystem.rule_system_Dnd_E5] = Global.classesFromResources.map { it.name }
    }


}