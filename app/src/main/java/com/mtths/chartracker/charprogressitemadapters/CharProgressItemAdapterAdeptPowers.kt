package com.mtths.chartracker.charprogressitemadapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.utils.Helper.cutAfterString
import com.mtths.chartracker.utils.Helper.removeUnnecassaryZeros
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.StringUtils.getFloatOrIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import java.util.*

class CharProgressItemAdapterAdeptPowers (activity: ProgressBarFrameDropBoxActivity) :
    CharProgressItemAdapter<CharProgressItemAdapterAdeptPowers.AdeptPower>(
        mActivity = activity,
        comparator = ADEPT_POWER_NAME_COMPARATOR
    ), CharProgressItemAdapterBuffable {

    val layoutResourceEntry: Int
        get() = R.layout.char_progress_list_view_item_2_values

    override val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header


    override fun createAutoCompleteStrings(): ArrayList<String> {
        return addBuffAutoCompleteString(ArrayList(listOf("rating =", "points =")))
    }

    override fun generate_autocomplete_data(e: LogEntry) {
    }

    override fun add(e: LogEntry): LogEntry {

        //first entry is karma, second is text
        val adeptPower = AdeptPower()
        adeptPower.relatedLogEntry = e

        /*
        set name
         */
        adeptPower.parseName(logEntryText = e.text, item = item)
        adeptPower.rating = getIntegerStatFromText("rating", e.text)
        adeptPower.points = getFloatOrIntegerStatFromText("points", e.text)
        data.add(adeptPower)
        item?.character?.let { c ->
            if (!adeptPower.points.equals("-", ignoreCase = true)) {
                adeptPower.points?.toFloatOrNull()?.let { c.used_adept_power_points += it }
            }
        }
        createBuffs(s = e.text, iData = adeptPower, item = item, context = context)

        return e
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        val adeptPower: AdeptPower = data[index]

        //inflate entry
        val adpet_power_view: View =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false)
        val name_display = adpet_power_view.findViewById<TextView>(R.id.text)
        val rating_display = adpet_power_view.findViewById<TextView>(R.id.number)
        val points_display = adpet_power_view.findViewById<TextView>(R.id.number2)
        name_display.text = adeptPower.name
        rating_display.text = adeptPower.rating
        points_display.text = adeptPower.points
        mActivity?.supportFragmentManager?.let { sfm ->
            adpet_power_view.setOnLongClickListener(
                ListenerImplOpenEditLogsOnLongClick(
                    sfm,
                    adeptPower.relatedLogEntry
                )
            )
        }

        return adpet_power_view
    }

    override fun inflate(expand: Boolean): View? {
        val container = super.inflate(expand)
        renameStat1ColumnHeader("Points", container)
        item?.character?.let { c ->
            val header_text: String = removeUnnecassaryZeros("${item?.header} (${c.used_adept_power_points}") +
                    String.format(Locale.GERMAN, "/%d", c.magic + c.bonus_adept_points) + ")"
            header_view?.text = header_text
        }
        return container
    }

    inner class AdeptPower : CharProgressItemData() {
        var rating: String? = null
        var points: String? = null
    }

    companion object {
        private val ADEPT_POWER_NAME_COMPARATOR =
            Comparator<AdeptPower> { o1, o2 -> o1.name.compareTo(o2.name, ignoreCase = true) }
    }
}