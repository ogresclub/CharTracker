package com.mtths.chartracker.charprogressitemadapters

import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import java.util.*

class CharProgressItemAdapterComplexFormSR(
    activity: ProgressBarFrameDropBoxActivity?
) :
    CharProgressItemAdapterSpellSR(
        activity = activity,
        colHeaderIds = listOf(REF_ID_DRAIN, REF_ID_DURATION),
        useComplexForms = true
    ) {

    override fun createAutoCompleteStrings(): ArrayList<String> {
        return ArrayList(
            listOf(
                REF_ID_DURATION,
                REF_ID_DRAIN).map {
                "${context.getString(it).lowercase()} = "
            }
        ).apply {
            addAll(
                listOf(REF_ID_DESCRIPTION).map {
                    "${context.getString(it).lowercase()} = ["
                }
            )
        }
    }

    override fun inflateAdditionalData(
        spell: SpellSR,
        check_mistakes: Boolean,
        holder: ViewHolder
    ) {
        super.inflateAdditionalData(spell, check_mistakes, holder)
        holder.dataViews?.get(1)?.apply {
            text = spell.duration
        }
    }
}