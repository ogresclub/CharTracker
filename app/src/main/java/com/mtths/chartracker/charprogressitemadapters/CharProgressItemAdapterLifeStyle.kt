package com.mtths.chartracker.charprogressitemadapters

import android.app.Activity
import android.content.Context
import android.view.View
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs

class CharProgressItemAdapterLifeStyle(
    activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<CharProgressItemAdapterStd.StdEntry>) :
    CharProgressItemAdapterPlain(activity = activity, comparator = comparator) {

    override val layoutResourceEntry = R.layout.char_progress_list_item_std_no_line
    override val layoutResourceMain = R.layout.char_progress_item_container_bordered

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        return if (index > 0) {
            null
        } else {
            super.getView(index, check_mistakes)
        }
    }

    override fun inflate(expand: Boolean): View? {
        val v = super.inflate(expand)
        header_view?.setOnLongClickListener {
            mActivity?.supportFragmentManager?.let { it1 ->
                DialogShowFilteredLogs.newInstance(
                    searchString = item?.filterConstraintTranslations?.joinToString(" OR ") ?: ""
                ).show(it1, "dialogShowFilteredLogs_LifeStyle")
            }
            return@setOnLongClickListener true
        }
        return v
    }
}