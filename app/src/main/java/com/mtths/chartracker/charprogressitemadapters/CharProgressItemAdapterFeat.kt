package com.mtths.chartracker.charprogressitemadapters

import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterBuffs.Companion.BUFF_REF
import com.mtths.chartracker.dataclasses.BuffEffect
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.listeners.ListenerImplOnDoubdleClickShowFeatLinks
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.listeners.ListenerImplToastOnDoubleClickToggleExtendOnSingleClick
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper.countMatches
import com.pedromassango.doubleclick.DoubleClick
import java.util.*
import kotlin.math.min

/**
 * Specific Feats are build in Helper.createCharProgressItems method
 */
class CharProgressItemAdapterFeat(
    activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<StdEntry>) :
    CharProgressItemAdapterStd(
        activity = activity,
        comparator = comparator,
        cutNameAfterComma = true
    ), CharProgressItemAdapterBuffable {


    private var TOUGHNESS_INDEX_IN_DATA = -1
    var totalExpSpendForThisItem = 0
    var numToughness = 0

    /**
     * use lowercase for keys!!!
     * return the buff effects for a feat as string
     * for feats to automatically create a buff providing the effect
     */
    val featsBuffDict = HashMap<String, () -> List<String>>().apply {
        put("toughness") { listOf("${BuffEffect.BUFF_REF_HP} +3") }
        put("toughness of a dwarf") { listOf("${BuffEffect.BUFF_REF_HP} +6") }
        put("improved toughness") { listOf("${BuffEffect.BUFF_REF_HPR} +1") }
        put("learned resistance (hit point damage)") { listOf("${BuffEffect.BUFF_REF_HPR} +1") }
        put("toughness of a giant") { listOf("${BuffEffect.BUFF_REF_CONSTITUTION_FOR_HP_CALC} +4") }
        put("beefy") { listOf("${BuffEffect.BUFF_REF_HPR} +1") }
        put("lightning reflexes") { listOf("${Character.PROGRESSION_REF_REFLEX_SAVES} +2") }
        put("great fortitude") { listOf("${Character.PROGRESSION_REF_FORT_SAVES} +2") }
        put("iron will") { listOf("${Character.PROGRESSION_REF_WILL_SAVES} +2") }
        put("endurance") { listOf("${BuffEffect.BUFF_REF_NEGATIVE_HP} +4") }
    }

    override fun getAutoCompleteReplacement(autoCompleteString: String): String {
        return featsBuffDict.get(autoCompleteString.lowercase())?.invoke()?.joinToString()?.let {
            "$autoCompleteString, $BUFF_REF = [$it]"
        }
            ?: super.getAutoCompleteReplacement(autoCompleteString)
    }


    override fun getExpProposal(text: String, nb_of_active_chars: Int, data: ArrayList<*>): Int {
        val nbOfFeats = item?.let {countMatches(text, it.filterConstraint)} ?: 0
        return if (PrefsHelper(context).rulesOgresClub) -1000 * nbOfFeats else 0

    }

    override fun getView(index: Int, check_mistakes: Boolean): View { //inflate entry
        val entryData = data[index]
        val entry = LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false) as LinearLayout
        val numberView = entry.findViewById<TextView>(R.id.number)
        val textView = entry.findViewById<TextView>(R.id.text)
        numberView.text = Character.getTotalWealthString(entryData.left_text.toFloatOrNull() ?: 0f)
        textView.text = entryData.right_text

        //make exp smaller
        val param = LinearLayout.LayoutParams(
            0,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            7.0f
        )
        textView.layoutParams = param
        if (entryData.hasError) {
            entry.setBackgroundResource(R.color.holo_red_light_half_transparent)
        }

        if (entryData.isToughnessEntry) {
            mActivity?.let { mActivity ->
                entry.setOnLongClickListener {
                    DialogShowFilteredLogs.newInstance(
                        item?.filterConstraintTranslations?.joinToString(" OR ") {
                            "\"$it ${entryData.right_text}\"" } ?: "",
                        check_mistakes, true
                    )
                        .show(
                            mActivity.supportFragmentManager,
                            ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                        )
                    true
                }
            }
        } else {
            mActivity?.let { a ->
                entry.setOnLongClickListener(
                    ListenerImplOpenEditLogsOnLongClick(
                        fm = a.supportFragmentManager,
                        e = entryData.relatedLogEntry
                    )
                )
                true
            }
        }
        entry.setOnClickListener(DoubleClick(ListenerImplOnDoubdleClickShowFeatLinks(mActivity!!, "@Feat " + entryData.right_text)))
        return entry
    }

    override fun generateAddData(e: LogEntry): StdHolder {
        val holder = super.generateAddData(e)
        val containsMarker = e.text.contains(Global.IGNORE_EXP_CHECK_INDICATOR) ||
                e.text.contains(Global.FEATURED_MARKER)
        /*
        set all exp as STD_EXP_COST_FEAT because that's how much a feat costs
         */
        val numberOfFeatsInThisLogEntry = holder.entries.size
        if (numberOfFeatsInThisLogEntry > 1 && !containsMarker) {
            for (entry in holder.entries) {
                entry.left_text = STD_EXP_COST_FEAT.toString()
            }
        }
        var expIsFine = true
        if (e.exp != STD_EXP_COST_FEAT * numberOfFeatsInThisLogEntry &&
            !containsMarker) {
            holder.logEntry!!.hasError = true
            expIsFine = false
            for (entry in holder.entries) {
                entry.hasError = true
            }
        }
        /*
        Sum together all the exp from buying toughness into one entry
         */
        if (numberOfFeatsInThisLogEntry == 1 || expIsFine) {
            holder.entries.forEach { entry ->
                val exp = if (expIsFine) STD_EXP_COST_FEAT else e.exp
                if (numberOfFeatsInThisLogEntry == 1) holder.logEntry?.hasError = false

                if (entry.right_text.equals("Toughness", ignoreCase = true)
                    && e.exp <= 0) {
                    val toughnessEntry = getToughnessEntry()
                    if (toughnessEntry == null) { //toughnessEntry doesn't exists yet so we take this entry as new toughnessEntry
                        entry.exp = exp
                        this.numToughness = min(exp, -1000)/-1000
                        entry.hasError = false
                        entry.left_text = java.lang.String.valueOf(entry.exp)
                        entry.isToughnessEntry = true
                    } else { //remove the toughness entry from holder.entries to make sure we will only have one entry
                        holder.entries.remove(entry)
                        toughnessEntry.exp += exp
                        this.numToughness += exp/-1000
                        toughnessEntry.left_text = java.lang.String.valueOf(toughnessEntry.exp)


                    }
                    totalExpSpendForThisItem -= exp
                }
            }
        }
        for (entry in holder.entries) {
            entry.left_text?.let { entryText ->
                if (!entry.isToughnessEntry && entryText.isNotEmpty()) {
                    totalExpSpendForThisItem -= entryText.toInt()
                }
            }

        }

        holder.entries.forEach {
            createBuffs(
                s = e.text,
                iData = it,
                item = item,
                context = requireNotNull(mActivity)
            )
        }

        return holder
    }

    private fun getToughnessEntry(): StdEntry? {
        //get toughnessEntry
        var toughnessEntry: StdEntry? = null
        if (TOUGHNESS_INDEX_IN_DATA == -1) {
            data.forEach { runningEntry ->
                if (runningEntry?.isToughnessEntry == true) {
                    TOUGHNESS_INDEX_IN_DATA = data.indexOf(runningEntry)
                    toughnessEntry = runningEntry
                }
            }
        } else {
            toughnessEntry = data[TOUGHNESS_INDEX_IN_DATA]
        }
        return toughnessEntry
    }

    override fun createAutoCompleteStrings(): ArrayList<String> { //        Helper.log(DEBUG_TAG, "START creating auto complete strings for feats:");
        val feats_to_auto_complete = ArrayList<String>()
        for (featLink in Global.featsOnOgresClub) {
            feats_to_auto_complete.add(featLink.name)
        }
        for (featLink in Global.featsFromResources) {
            feats_to_auto_complete.add(featLink.name)
        }
        addBuffAutoCompleteString(feats_to_auto_complete)
        //         Helper.log(DEBUG_TAG, "END creating auto complete strings for feats:");
        return feats_to_auto_complete
    }

    override fun inflate(expand: Boolean): View? {
        val v = super.inflate(expand = expand)
        header_view?.setOnLongClickListener {
            item?.let {item ->
                mActivity?.supportFragmentManager?.let { fm ->
                    DialogShowFilteredLogs.newInstance(
                        searchString = item.filterConstraintTranslations.joinToString(" OR "),
                        markErrors = item.check_mistakes, true)
                        .show(
                            fm,
                            ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                        )
                }
            }
            true
        }

            header_view?.setOnClickListener(DoubleClick(
                ListenerImplToastOnDoubleClickToggleExtendOnSingleClick(
                    activity = mActivity!!,
                    msg = String.format(Locale.GERMANY, "%,d", totalExpSpendForThisItem),
                    filterConstraintId = item?.filterConstraintId,
                    character = item?.character)
            ))


        return v
    }

    companion object {
        private const val DEBUG_TAG = "FEAT_ADAPTER"
        const val STD_EXP_COST_FEAT = -1000
    }
}