package com.mtths.chartracker.charprogressitemadapters

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.SpellDnD
import com.mtths.chartracker.dataclasses.SpellDnD.Companion.REF_LEVEL
import com.mtths.chartracker.dataclasses.SpellDnD.Companion.REF_SCHOOL
import com.mtths.chartracker.dataclasses.SpellDnD.Companion.REF_TAGS
import com.mtths.chartracker.dataclasses.SpellDnD.Companion.getSpellMap
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.capitalize
import com.mtths.chartracker.utils.Helper.capitalizeNewsLetterStyle
import com.mtths.chartracker.utils.Helper.checkFeatured
import com.mtths.chartracker.utils.Helper.checkIgnoreExp
import com.mtths.chartracker.utils.Helper.getRegexContainsFilterConstraint
import com.mtths.chartracker.utils.HttpUtils
import com.pedromassango.doubleclick.DoubleClick
import com.pedromassango.doubleclick.DoubleClickListener
import java.util.ArrayList

class CharProgressItemAdapterSpells5e(
    activity: ProgressBarFrameDropBoxActivity?
): CharProgressItemAdapter<SpellDnD>(
    mActivity = activity,
    comparator = compareBy<SpellDnD> { it.level }.thenBy { it.name }),
    CharProgressItemAdapterBuffable {

    val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values
    override  val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header
    val spellMap = if (!rulesSplittermond) getSpellMap(context) else HashMap()
    var featured_spells_used = 0

    override fun createAutoCompleteStrings(): ArrayList<String> {
        return super.createAutoCompleteStrings().also {
            it.addAll(ArrayList(
                listOf(REF_LEVEL, REF_SCHOOL, REF_TAGS).map { "$it = " }
            ))
            addBuffAutoCompleteString(it)
            it.addAll(spellMap.values.map { it.name })
        }
    }

    override fun generate_autocomplete_data(e: LogEntry) {}

    fun getDisplayString(spell: SpellDnD): SpannableStringBuilder {

        fun getSpellSchools(spell: SpellDnD): List<String> {
            return arrayListOf<String>().apply {
                if (rulesSkillfullE5) {
                    if (item?.character?.showCastStat == true) {
                        add(spell.school)
                    }
                    addAll(spell.tags)
                } else {
                    add(spell.school)
                }
            }
        }

        return SpannableStringBuilder(capitalizeNewsLetterStyle(spell.name.lowercase())).also {sb ->
            if (item?.character?.showSpellSchools == true) {
                getSpellSchools(spell).let { mSchools ->

                    sb.append(" [")
                    Helper.append(
                        sb = sb,
                        text = mSchools.joinToString(),
                        what = StyleSpan(Typeface.ITALIC),
                        flags = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    sb.append("]")
                }
            }
        }

    }

    override fun add(e: LogEntry): LogEntry {
        SpellDnD().apply {
            relatedLogEntry = e
            parseName(e.text, item)
            parseLevel(s = e.text, spellMap = spellMap)
            parseTags(s = e.text, spellMap = spellMap)
            parseSchool(s = e.text, spellMap = spellMap)

            createBuffs(s = e.text, iData = this, item = item, context = context)

            val ignoreExpCheck = checkIgnoreExp(e.text)
            e.hasError = !ignoreExpCheck && (e.exp != -getExpCost(level) || e.hasError)

            if (checkFeatured(e.text)) featured_spells_used++


            data.add(this)
        }

        return e
    }

    override fun getExpProposal(text: String, nb_of_active_chars: Int, data: ArrayList<*>): Int {
        item?.let {
            return if (text.matches(Regex(getRegexContainsFilterConstraint(it.filterConstraint).toString()))) {
                SpellDnD()
                    .apply {
                        parseName(text, item)
                        parseLevel(s = text, context = context)
                    }
                    .let { s ->
                        if (s.name.isNotBlank()) {
                            -getExpCost(s.level)
                        } else {
                            0
                        }
                    }
            } else {
                0
            }
        }
        return 0
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        return data[index].let { spell ->
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false).apply {

                findViewById<TextView>(R.id.text).apply {
                    text = getDisplayString(spell = spell)
                }

                findViewById<TextView>(R.id.number).apply {
                    text = spell.level.toString()
                }


                if (check_mistakes && spell.relatedLogEntry?.hasError == true) {
                    setBackgroundResource(R.color.holo_red_light_half_transparent)
                }

                setOnClickListener(DoubleClick(
                    object : DoubleClickListener {
                        override fun onDoubleClick(view: View?) {
                            mActivity?.let {
                                HttpUtils.openWebPage(
                                    url = spell.getLink(item?.character?.ruleSystem ?: PrefsHelper(context).ruleSystem),
                                    activity = it
                                )
                            }
                        }

                        override fun onSingleClick(view: View?) {}
                    }
                ))

                mActivity?.let { a ->

                    setOnLongClickListener(
                        ListenerImplOpenEditLogsOnLongClick(
                            a.supportFragmentManager,
                            spell.relatedLogEntry
                        )
                    )
                }

//            val amount_display = spellView.findViewById<TextView>(R.id.number2)

            }
        }
    }

    fun log(msg: String) {
        Log.e("CPIA_SPELLS", "ssssssssssss: $msg")
    }



    fun getExpCost(level: Int): Int {
        return when {
            rulesSplittermond -> if (level == 0) 1 else 3*level
            else -> (level + 1 ) * 3
        }
    }

    fun getNumFeaturedSpells(): Int? {
        try {
            return mActivity?.charProgressItemsMap?.get(R.string.filter_constraint_skill)?.mainItem?.adapter?.let {
                it as CharProgressItemAdapterSkills }?.data?.filter {
                    it.skill?.type in listOf("magic", "spellschool") }?.sumOf { it.value / 2 }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    override fun inflate(expand: Boolean): View? {


        val container = super.inflate(expand)

        renameStat0ColumnHeader(capitalize(REF_LEVEL), container)

        container?.findViewById<TextView>(R.id.name_label)?.setOnClickListener {
            item?.character?.let { c ->
                c.toggleShowSpellSchools()
                DatabaseHelper.getInstance(context)?.updateCharacter(
                    character = c, keep_last_update_date = true
                )
                updateAllDisplays()
            }
        }

        if (expand) {
            // Set Header color for skills to show if you have spend all your skills from int/class/human correctly
            val check = PrefsHelper(context).checkFeaturedStats && rulesSkillfullE5
            log("check = $check")
            getNumFeaturedSpells()?.let { featuredSpells ->
                log("featured spells: $featuredSpells")
                if (check) {
                    item?.character?.let { c ->
                        val free_featured_skills = featuredSpells - featured_spells_used
                        header_view?.text = markHeaderWhenCountingPoints(free_featured_skills)
                    }

                }
            }
        }


            return container
        }


    }
