package com.mtths.chartracker.charprogressitemadapters

import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.preferences.PrefsHelper

open class CharProgressItemAdapterLanguages (activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapterValueBased(
        activity = activity,
        comparator = compareBy { it.name }
    ) {

    val splittermondCalc = object : Calculator {
        override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
            return 5
        }
    }

    override val ignoreValues: Boolean
        get() = when {
            rulesSplittermond -> true
            else -> super.ignoreValues
        }


    override val mCalculator: Calculator
        get() =
            when {
                rulesSplittermond -> splittermondCalc
                else -> super.mCalculator
            }



    override var DEBUG_TAG = "CHAR_PROG_ADAPTER_LANGUAGES"
    override fun add(
        e: LogEntry,
        ignoreMismatches: Boolean,
        data: ArrayList<ValueNamePair>,
        dontModifyData: Boolean): Holder {
        var holder: Holder

        // ONLY SR
        if (rulesSR && e.text.contains(Global.MOTHER_TONGUE_LANGUAGE_MARKER)) {
            /*
            this is just to trick around a bit. We don't want the log entry to be marked false or anything,
            so we fix it. but in fact we don't want @Value here because the mother tongue doesn't have a value.
             */
            val fakeValue = " $ORIGIN_LANGUGE_INDICATOR"
            val origText = e.text
            if (extractValue(e.text).value != INVALID_VALUE_INDICATOR) {
                e.text += fakeValue
            }
            holder = super.add(e, ignoreMismatches, data, dontModifyData)
            holder.holderItems.first().pair?.let { pair ->
                pair.value = ORIGIN_LANGUGE_INDICATOR
            }
            holder.logEntry?.let { l ->
                l.text = origText
            }


        } else {
            holder = super.add(e, ignoreMismatches, data, dontModifyData)
        }


        //////////////// FEATURED STUFF //////////////////
        for (h in holder.holderItems) {
            if (e.text.lowercase().contains(Global.FEATURED_MARKER) && h.index_of_changed_pair >= -1) {
//                log(DEBUG_TAG, ".feature found. holder = value " + h.pair?.value + ", old value " + h.old_value + ", name " + h.pair?.name + ", index " + h.index_of_changed_pair)
                holder.logEntry?.hasError = false
                item?.character?.let { c ->
                    var amount_of_language_points_added = -1
                    if (h.index_of_changed_pair >= -1) {
                        /*
                        this means that the logText contained @Value
                         */
                        h.pair?.let { pair ->
                            amount_of_language_points_added = pair.value - h.old_value
                        }
                        /*
                        if origin marker is contained, you get 3 points for free because it is your mother tongue
                         */
                        if (e.text.contains(Global.ORIGIN_LANGUAGE_MARKER)) {
                            amount_of_language_points_added -= 3
                            // if you spent less than 3 points in your mother tongue there is no point so it is marked as error
                            if (amount_of_language_points_added < 0) {
                                holder.logEntry?.hasError = true
                            }
                        }
                    }
                    log(
                        DEBUG_TAG,
//                        "amount of language points to add: $amount_of_language_points_added"
                    )
                    if (amount_of_language_points_added > 0) {
                        c.getPrefs(context).featured_language_points_used += amount_of_language_points_added
                    }
                }
            }
        }
        return holder
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        return super.getView(index, check_mistakes)?.apply {
            if (!(item?.character?.ruleSystem ?: prefs.ruleSystem).languagesHaveValue) {
                findViewById<TextView>(R.id.number).apply {
                    visibility = View.GONE
                }
            }

        }
    }

    override fun modifyValueDisplay(vnp: ValueNamePair): String {
        if (rulesSR) {
            return if (vnp.value != ORIGIN_LANGUGE_INDICATOR) {
                vnp.value.toString()
            } else {
                "N"
            }
        }
        return super.modifyValueDisplay(vnp)
    }

    override fun onItemAttached(item: CharProgressItem?) {
        super.onItemAttached(item)
        item?.character?.getPrefs(context)?.featured_language_points_used = 0
    }

    override fun inflate(expand: Boolean): View? {
        val v = super.inflate(expand = expand)

        if(expand) {
            item?.character?.let { c ->
                if (PrefsHelper(context).checkFeaturedLanguages && (c.intelligence ?: -1) > 0) {
                    val free_featured_languages = c.totalLanguagePoints - c.getPrefs(context).featured_language_points_used
                    header_view?.text = markHeaderWhenCountingPoints(free_featured_languages)
                }
            }

        }
        if (!(item?.character?.ruleSystem ?: prefs.ruleSystem).languagesHaveValue){
            renameStat0ColumnHeader(name = "", containerView = v)
        }
        return v
    }

    companion object {
        const val ORIGIN_LANGUGE_INDICATOR = 1024

    }

    init {
        stdAutoCompleteData = HashMap()
        stdAutoCompleteData[RuleSystem.rule_system_ogres_club] = listOf(
            "Abyssal", "Aquan", "Auran", "Celestial", "Common (Faerun)",
            "Common (Greyhawk)", "Common (Planes)", "Draconic", "Druidic", "Dwarven", "Elven", "Giant",
            "Gnome", "Goblin", "Gnoll", "Halfling", "Ignan", "Infernal", "Orc", "Sylvan", "Terran",
            "Undercommon")
        stdAutoCompleteData[RuleSystem.rule_system_Dnd_E5] = listOf(
            "Common", "Dwarvish", "Elvish", "Giant", "Gnomish", "Goblin", "Halfling", "Orc",
            "Abyssal", "Celestial", "Draconic", "Deep Speech", "Infernal", "Primordial", "Sylvan", "Undercommon"
        )
        stdAutoCompleteData[RuleSystem.rule_system_skillfull_dnd_5E] = stdAutoCompleteData[RuleSystem.rule_system_Dnd_E5]!!
        stdAutoCompleteData[RuleSystem.rule_system_sr5] =
            mActivity?.resources?.getStringArray(R.array.languages_sr5)?.toList() ?: ArrayList()

        stdAutoCompleteData[RuleSystem.rule_system_sr6] =
            mActivity?.resources?.getStringArray(R.array.languages_sr5)?.toList() ?: ArrayList()
    }
}
