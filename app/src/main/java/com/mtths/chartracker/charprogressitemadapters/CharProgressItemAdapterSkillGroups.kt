package com.mtths.chartracker.charprogressitemadapters

import android.util.Log
import android.view.View
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterValueBased.Companion.getNameSpecializationDescriptionAndValue
import com.mtths.chartracker.utils.Helper.getSortedSkills
import com.mtths.chartracker.utils.Helper.getTextAfterStringAndTrim
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.SkillGroup
import com.mtths.chartracker.dataclasses.ValueNamePairSkillSR
import com.mtths.chartracker.utils.Helper
import java.util.*
import kotlin.math.min

class CharProgressItemAdapterSkillGroups(
    mActivity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapter<SkillGroup>(
        mActivity = mActivity,
        comparator = Comparator { o1: SkillGroup, o2: SkillGroup ->  o1.name.compareTo(o2.name)}) {

    val DEBUG_TAG_SKILL_GROUPS = "SKILL_GROUPS"
    val skillAdapter: CharProgressItemAdapterSkillsSR?
        get() {
            try {
                return mActivity?.charProgressItemsMap?.get(R.string.filter_constraint_skill)?.mainItem?.adapter
                        as? CharProgressItemAdapterSkillsSR?

            } catch (e: Exception) {
                e.printStackTrace()
                Helper.log(DEBUG_TAG_SKILL_GROUPS, "No Skill Adapter found")
            }
            return null
        }

    override fun clear() {
        super.clear()
        for (sg in Global.skill_groups) {
            sg.hasError = false
        }
    }

    override fun add(e: LogEntry): LogEntry {
        skillAdapter?.let { return add(e, false, it.data) }
        return e
    }

    override fun generate_autocomplete_data(e: LogEntry) {
        skillAdapter?.let { add(e, false, it.auto_complete_data) }
    }

    private fun calcExpCost(
        skill_group_skills_sorted_max_at_0: ArrayList<ValueNamePairSkillSR>,
        new_value: Int
    ): Int {
        var exp_to_propose = 0
        val cap_to_buy_as_single_skills =
            min(skill_group_skills_sorted_max_at_0.maxOf { it.value }, new_value)
        //        Helper.log(DEBUG_TAG_SKILL_GROUPS, "calc exp cost: cap for single buy = " + cap_to_buy_as_single_skills);
        for (i in skill_group_skills_sorted_max_at_0.indices) {

//            Helper.log(DEBUG_TAG_SKILL_GROUPS, "calc exp cost: i = " + i + ", skill = " + skill_group_skills_sorted_max_at_0.get(i).name + " ------------------------------------------------------------");

            /*
            this will be set to zero if we have at already raised three skills to a common level
            and have another 4th skill in the skill group which will be free to raise to the same rank than
             */
            var nullifying_factor = 1
            var inverse_factor = 1
            if (i > 1) {
                inverse_factor = 2
                if (i > 2) {
                    nullifying_factor = 0
                }
            }

//                    Helper.log(DEBUG_TAG_SKILL_GROUPS, "generate exp_proposal: skill = " + sg.skills[i] + ", rank = " + current_values[i]);
            if (cap_to_buy_as_single_skills > skill_group_skills_sorted_max_at_0[i].value) {
                val karma = skillAdapter!!.mCalculator.getExpCost(
                    skill_group_skills_sorted_max_at_0[i].value, cap_to_buy_as_single_skills
                ) / inverse_factor * nullifying_factor
                exp_to_propose += karma
                //                        Helper.log(DEBUG_TAG_SKILL_GROUPS, "calc exp cost: exp cost single for " + skill_group_skills_sorted_max_at_0.get(i).name + ", " +  karma);
            }
        }

        /*
        raise the whole skill group when all skills have same rank
         */if (cap_to_buy_as_single_skills < new_value) {
            var karma = 0
            for (i in cap_to_buy_as_single_skills + 1..new_value) {
                karma += i
            }
            karma *= 5
            exp_to_propose += karma

//                    Helper.log(DEBUG_TAG_SKILL_GROUPS, "calc exp cost: exp cost for group raising from " + cap_to_buy_as_single_skills +
//                            " to " + new_value + " = " + karma);
        }
        return exp_to_propose
    }

    override fun getAutoCompleteValue(name: String): Int? {
        return getSkillGroup(name)?.let { sg ->
            skillAdapter?.let { skill_adapter ->
                getMin(getRanks(sg = sg, data = skill_adapter.auto_complete_data))
            }
        }
    }

    override fun getExpProposal(
        text: String,
        nb_of_active_chars: Int,
        data: ArrayList<*>
    ): Int {
        var proposal = 0
        skillAdapter?.let { skill_adapter ->
            if (nb_of_active_chars == 1 && text.matches(("(?i).*" + item!!.filterConstraint + "\\s+.*").toRegex())
            ) {
                Helper.log(
                    DEBUG_TAG_SKILL_GROUPS,
                    "start get exp proposal ################################################ for text '$text'"
                )
                val textAfterFilterConstraint = getTextAfterStringAndTrim(text, item?.filterConstraint)
                Helper.log(
                    DEBUG_TAG_SKILL_GROUPS,
                    "text after filterconstraint: '$text'"
                )
                var skill_group_name = textAfterFilterConstraint
                val skill_group_value =
                    CharProgressItemAdapterValueBased.extractValue(textAfterFilterConstraint).value
                Helper.log(DEBUG_TAG_SKILL_GROUPS, "new skill group value = $skill_group_value")
                val m = Global.PATTERN_SKILL
                    .matcher(textAfterFilterConstraint)
                if (m.find()) {
                    m.group(1)?.let { skill_group_name = it.trim() }
                }
                Helper.log(DEBUG_TAG_SKILL_GROUPS, "skill group name = $skill_group_name")
                getSkillGroup(skill_group_name)?.let { sg ->
                    Helper.log(
                        DEBUG_TAG_SKILL_GROUPS,
                        "skill group found = " + sg.name.toString() + " " + sg.skills
                    )
                    if (skill_group_value > 0 && !text.lowercase(Locale.getDefault())
                            .contains(
                                Global.IGNORE_EXP_CHECK_INDICATOR.lowercase(Locale.getDefault())
                            )
                    ) {
                        val exp_to_propose = calcExpCost(
                            getSortedSkills(
                                sg.skills,
                                skill_adapter.auto_complete_data,
                                skill_adapter.mCalculator
                            ), skill_group_value
                        )
                        if (exp_to_propose != 0) {
                            proposal = -exp_to_propose
                        }
                    }
                }


            }
        }
        return proposal
    }

    override fun createAutoCompleteStrings(): ArrayList<String> {
        val auto_complete = ArrayList<String>()
        for (sg in Global.skill_groups) {
            auto_complete.add(sg.name)
        }
        return auto_complete
    }

    ///////////////////////////////////// HELP METHODS ////////////////////////////////////////
    private fun add(
        e: LogEntry,
        ignore_mismatches: Boolean,
        data: ArrayList<ValueNamePairSkillSR>
    ): LogEntry {
        var calced_karma_cost = 0
        skillAdapter?.let { skill_adapter ->

            val xvnp = getNameSpecializationDescriptionAndValue(
                text = updateFilterConstraintToCurrentLanguage(e.text).also { Log.d("SKGRP", "translated fc: $it") },
                item = item,
                context = context
            )
            val skill_group_name = xvnp.name
            val skill_group_value = xvnp.value

            Helper.log(
                DEBUG_TAG_SKILL_GROUPS,
                "add: skill group name = '$skill_group_name', log entry has error: ${e.hasError} §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§"
            )
            Helper.log(DEBUG_TAG_SKILL_GROUPS, "add: skill group value = " + skill_group_value);
            getSkillGroup(skill_group_name)?.also { sg ->

            //only continue if a skill group with this name is found.
                Helper.log(
                    DEBUG_TAG_SKILL_GROUPS,
                    "add: get skill group result = " + sg.name + " " + sg.skills.joinToString() + " has error ${sg.hasError}"
                )
                val skill_group_skills_sorted_max_at_0: ArrayList<ValueNamePairSkillSR> =
                    getSortedSkills(sg.skills, data, skill_adapter.mCalculator)
                calced_karma_cost =
                    calcExpCost(skill_group_skills_sorted_max_at_0, skill_group_value)
                for (i in 0 until sg.skills.size) {
                    val skill: String = sg.skills[i]
                    val current_values = getRanks(sg, data)
                    Helper.log(
                        DEBUG_TAG_SKILL_GROUPS,
                        "add: current values = " + skill + " " + current_values[i]
                    );
                    val value_to_increase_to_as_single_skill =
                        getMax(current_values).coerceAtMost(skill_group_value)
                    Helper.log(
                        DEBUG_TAG_SKILL_GROUPS,
                        "add: cap = " + value_to_increase_to_as_single_skill
                    );


                    /*
                    fake a log entry to use the add method of SkillsAdapter,
                    but only if the value of the skill group is higher then the current value
                    because we don't want skills to go down here but rather to bring a skill group
                    to a certain level even though one skill might still be higher than the minimum of the skill group
                     */if (skill_group_value > 0) {
                        // this means the the value could be converted to integer
                        var skill_value = current_values[i]
                        if (skill_value == -1) skill_value++
                        if (value_to_increase_to_as_single_skill > skill_value) {
                            val trick_entry = LogEntry()
                            trick_entry.text =
                                "%s $skill $value_to_increase_to_as_single_skill".format(
                                    context.getString(R.string.filter_constraint_skill)
                                )
                            //fake correct karma so we won't get it marked as a mistake because of this reason.
                            trick_entry.exp = -skill_adapter.mCalculator.calcExpWithSkillGroups(
                                data,
                                value_to_increase_to_as_single_skill,
                                skill
                            )
                            Helper.log(
                                DEBUG_TAG_SKILL_GROUPS,
                                "add: adding fake entry = " + trick_entry.toString_ExpText()
                            )
                            val h = skill_adapter.add(trick_entry, ignore_mismatches, data)
                            Helper.log(
                                DEBUG_TAG_SKILL_GROUPS,
                                "add: ERROR ???????????????????????????????????????????????????? = " + e.hasError
                            )
                            e.hasError = e.hasError || h.logEntry.hasError
                            Helper.log(
                                DEBUG_TAG_SKILL_GROUPS,
                                "add: ERROR ????????????????????????????? after increasing to cap skill data (" + skill + ") = " + e.hasError
                            )
                            h.holderItems.forEach {h ->
                                if (h.index_of_changed_pair < 0) {
                                    h.pair?.let { data.add(it) }
                                    Helper.log(
                                        DEBUG_TAG_SKILL_GROUPS,
                                        "Entry was new and has been added."
                                    );
                                }
                            }
                        }

                        /*
                        now we increase the whole skill group from the earlier maximum value to the newly assigned value.
                         */if (skill_group_value > value_to_increase_to_as_single_skill) {
                            val trick_entry = LogEntry()
                            trick_entry.text = "%s $skill $skill_group_value".format(
                                context.getString(R.string.filter_constraint_skill)
                            )
                            trick_entry.exp = -skill_adapter.mCalculator.calcExpWithSkillGroups(
                                data,
                                skill_group_value,
                                skill
                            )
                            Helper.log(
                                DEBUG_TAG_SKILL_GROUPS,
                                "add: adding fake entry = " + trick_entry.toString_ExpText()
                            )
                            val holder = skill_adapter.add(trick_entry, ignore_mismatches, data)
                            holder.holderItems.forEach { h ->
                                if (h.index_of_changed_pair < 0) {
//                                Helper.log(DEBUG_TAG_SKILL_GROUPS, "Entry was new and has been added.");
                                    h.pair?.let { data.add(it) }
                                    Helper.log(
                                        DEBUG_TAG_SKILL_GROUPS,
                                        "add: ERROR ??????????????????????????? after increasing group from cap to assigned value = " + e.hasError
                                    )
                                }
                            }
                            e.hasError = holder.logEntry.hasError
                        }

                        //check if exp for skill group was spent correctly
                        Helper.log(
                            DEBUG_TAG_SKILL_GROUPS,
                            "add: ERROR ((THIS SHOULD BE FALSE!!!!!)) ?????????????????????????????? before checking skill group " + sg.name.toString() + "s karma = " + e.hasError
                        )
                        e.hasError = e.hasError || e.exp != -calced_karma_cost
                        Helper.log(
                            DEBUG_TAG_SKILL_GROUPS,
                            "add: ERROR ?????????????????????????????? after checking skill group " + sg.name.toString() + "s karma = " + e.hasError
                                .toString() + "\nlog entry karma = " + e.exp.toString() + ", calced exp cost = "
                                    + calced_karma_cost
                        )
                    }
                }
                sg.hasError = sg.hasError || e.hasError
                Helper.log(
                    DEBUG_TAG_SKILL_GROUPS,
                    "finish adding skill group " + sg.name + ". skill data size = ${data.size}. SkillGroup has error: ${sg.hasError}: log entry has error: ${e.hasError}"
                );

            } ?: run {
                Helper.log(DEBUG_TAG_SKILL_GROUPS, "add: get skill group result = null");
            }

        }
        return e
    }

    /**
     * @param name the name of the skill group we want
     * @return the skill group with the given name or null if none such exists
     */
    fun getSkillGroup(name: String): SkillGroup? {
        return Global.skill_groups.find { it.name.equals(name.trim(), ignoreCase = true) }
    }

    /**
     *
     * @param sg the SkillGroup I want to values for
     * @param data the data from where to search for values
     * @return array of values in order given in skillgroup.skills. -1 if no value found.
     */
    fun getRanks(sg: SkillGroup, data: ArrayList<ValueNamePairSkillSR>): IntArray {
        val ranks = IntArray(sg.skills.size)
        for (i in 0 until sg.skills.size) {
            skillAdapter?.let { skill_adapter ->
                val pair = Helper.getValueNamePairSkill(sg.skills[i], data)
                if (pair != null) {
                    ranks[i] = pair.value
                } else {
                    ranks[i] = -1
                }
            }
            //            Helper.log(DEBUG_TAG_SKILL_GROUPS, "get ranks: " + sg.skills[i] + " " + ranks[i]);
        }
        return ranks
    }

    fun getMin(ranks: IntArray): Int {
        var min = ranks[0]
        for (i in ranks) {
            if (i < min) {
                min = i
            }
        }
        return min
    }

    fun getMax(ranks: IntArray): Int {
        var max = ranks[0]
        for (i in ranks) {
            if (i > max) {
                max = i
            }
        }
        return max
    }

    /*
    ----------------------- not needed methods from adapter ----------------------------------------
     */
    override fun getView(index: Int, check_mistakes: Boolean): View? {
        return null
    }

    fun inflate(): View? {
        return null
    }
}