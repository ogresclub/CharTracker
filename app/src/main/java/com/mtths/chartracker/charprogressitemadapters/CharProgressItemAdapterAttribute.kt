package com.mtths.chartracker.charprogressitemadapters

import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_Dnd_E5
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_ogres_club
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_skillfull_dnd_5E
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.TranslationHelper
import java.lang.Integer.max

class CharProgressItemAdapterAttribute (
    activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<ValueNamePair>) : CharProgressItemAdapterValueBased(
    activity = activity,
    comparator = comparator) {

    override val layoutResourceEntry: Int
        get() = if (dndAttributes)
            R.layout.char_progress_list_view_item_2_values_inverse
        else
            R.layout.char_progress_list_view_item_2_values

    val dndAttributes: Boolean
        get() = prefs.rulesDnD

    override fun getExtraData(text: String) {

        super.getExtraData(text)

        val DEBUG_TAG_WARE_MODS = "WARE_ATTRIBUTES_MODS"

        /*
        get augmentation by items
         */
//        Helper.log(DEBUG_TAG_WARE_MODS, "getExtraData: found ware in '${text}'")
        for (attribute in createAutoCompleteStrings()) {
            Helper.getStatAttributeModification(attribute, text)?.let { mod ->
                Helper.log(DEBUG_TAG_WARE_MODS, "$attribute mod = $mod")
                val attribute_pair = getValueNamePair(attribute, data) ?:
                ValueNamePair(name = attribute, mCalculator = mCalculator).also { data.add(it) }
                if (rulesSR) {
                    attribute_pair.augmentation += mod
                } else {
                    attribute_pair.augmentation = max(attribute_pair.augmentation, mod)
                }
                addAugmentationValuesToCharacter(attribute_pair)
            }


        }

        for (attribute in listOf("Str", "Dex", "Con", "Wis", "Int", "Cha")) {
            Helper.getStatAttributeModification(attribute, text)?.let { mod ->
                Helper.log(DEBUG_TAG_WARE_MODS, "$attribute mod = $mod")
                Character.attrShortCutsMap[attribute]?.let { fullAttr ->
                    val attribute_pair = getValueNamePair(fullAttr, data) ?:
                    ValueNamePair(name = fullAttr, mCalculator = mCalculator).also { data.add(it) }
                    if (rulesSR) {
                        attribute_pair.augmentation += mod
                    } else {
                        attribute_pair.augmentation = max(attribute_pair.augmentation, mod)
                    }
                    addAugmentationValuesToCharacter(attribute_pair)
                }
            }
        }

//        item?.character?.let { c ->
//
//            Helper.getStatAttributeModification(Global.BONUS_HP_MARKER, text)?.let {
//                c.hp_bonus += it
//                Log.d("EXTRA_DATA", "character: '${c.name}: hp Bonus: $it")
//            }
//            Helper.getStatAttributeModification(Global.BONUS_STUN_HP_MARKER, text)?.let {
//                c.stun_hp_bonus += it
//                Log.d("EXTRA_DATA", "character: '${c.name}: stun hp Bonus: $it")
//            }
//            Helper.getStatAttributeModification(Global.BONUS_MAX_OVERFLOW, text)?.let {
//                c.max_overflow_dmg_bonus += it
//                Log.d("EXTRA_DATA", "character: '${c.name}: MaxOverflowBonus: $it")
//            }
//            Helper.getStatAttributeModification(Global.BONUS_RESIST_DMG, text)?.let {
//                c.resist_damage_impact += it
//                Log.d("EXTRA_DATA", "character: '${c.name}: ResistDamageImpact: $it")
//            }
//            Helper.getStatAttributeModification(Global.BONUS_RESIST_STUN_DMG, text)?.let {
//                c.resist_stun_damage_impact += it
//                Log.d("EXTRA_DATA", "character: '${c.name}: ResistStunDamageImpact: $it")
//            }
//            Helper.getStatAttributeModification(Global.INI_DICE_MARKER, text)?.let {
//                c.iniDiceBonus = it
//                Log.d("EXTRA_DATA", "character: '${c.name}: iniDiceBonus: $it")
//            }
//            Helper.getFloatOrIntegerStatFromText(Global.BONUS_ADEPT_POINTS, text).let {
//                try {
//                    c.bonus_adept_points += it.toFloat()
//                    Log.d("EXTRA_DATA", "character: '${c.name}: bonusAdeptPoints: $it")
//                } catch (e:Exception) {
////                    Log.d("EXTRA_DATA", "invalid float: '$it' for bonus adept point")
//                }
//            }
//            Helper.getStatAttributeModification(Global.ARMOR_MARKER, text)?.let {
//                c.armor += it
//                Log.d("EXTRA_DATA", "character: '${c.name}: armor: $it")
//            }
//        }
    }

    override fun add(e: LogEntry,
                     ignoreMismatches: Boolean,
                     data: ArrayList<ValueNamePair>,
                     dontModifyData: Boolean): Holder {

        val holder = super.add(e, ignoreMismatches, data, dontModifyData)

        /*
        store attributes at character
         */

        item?.character?.let { c ->

            holder.holderItems.forEach { h ->
                h.pair?.let { p ->
                    c.setStat(p.name, p.value)
                }
            }
            DatabaseHelper.getInstance(context)?.updateCharacter(
                character = c, keep_last_update_date = true)
        }

        return holder
    }

    fun isAugmented(stat: String): Boolean {
        return item?.character?.let {
            (it.getStatBuffValue(stat) ?: it.getStatReplacementValue(stat)) != null
        } ?: false
    }

    override fun modifyNameDisplay(s: SpannableStringBuilder): SpannableStringBuilder {
        /*
        assuming attributes normally are only the attribute
        and dont have e.g. specializations which are displayed italic
        all formatting of the spannable string is lost this way,
        but I cannot simply modify the text in the spannable string builder,
        so I would have to recreate it, but I don't really think it to be necessary
         */
        return SpannableStringBuilder(super.modifyNameDisplay(s).toString().uppercase())
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {

        val attribute_view = super.getView(index, check_mistakes)

        val attribute_pair = data[index]
        val isAugmented = isAugmented(attribute_pair.name)
        if (isAugmented) {
            val scoreView = attribute_view?.findViewById<TextView>(R.id.number)
            scoreView?.text = buildValueWithAugmentation(attribute_pair)
        }

        if (dndAttributes) {
            //fill mods
            val modView = attribute_view?.findViewById<TextView>(R.id.number2)
            val mod = getAttributeMod(rule_system_ogres_club, attribute_pair.value)
            modView?.text =  mod.toString()
            if (isAugmented) {
                val buffedMod = item?.character?.getTotalStatValue(attribute_pair.name)
                    ?.let { getAttributeMod(rule_system_ogres_club, it) }
                modView?.text =  AUG_VALUE_DISPLAY_TEMPLATE.format(mod, buffedMod)
            }
        }

        return attribute_view
    }

    override fun inflate(expand: Boolean): View? {
        val container = super.inflate(expand)

        /*
        set Column Headers correct for Score and Mod
         */
        if (dndAttributes) {
            val scoreView = container?.findViewById<TextView>(R.id.stat_1_label)
            scoreView?.setText(R.string.score)
            val modTv = container?.findViewById<TextView>(R.id.stat_0_label)
            modTv?.setText(R.string.mod)
        }

        if(expand && rulesSR) {
        /*
        Essence has to be added by hand because the value is not integer,but float
        */
            val essence_view =
                LayoutInflater.from(mActivity).inflate(layoutResourceEntry, null, false)
            val left_text = essence_view.findViewById<TextView>(R.id.number)
            val right_text = essence_view.findViewById<TextView>(R.id.text)

            val essence: Float = item?.character?.totalEssence ?: Character.MAXIMUM_ESSENCE

//                Helper.log("ESSENCE", "total essence as float = " + essence);
            val essence_without_dot_zero = Helper.removeUnnecassaryZeros(String.format("%.2f", essence))
            left_text.text = essence_without_dot_zero
            right_text.text = activity?.resources?.getString(R.string.essence_label)
            listOfViews?.addView(essence_view)
        }

        return container
    }



    private fun addAugmentationValuesToCharacter(p: ValueNamePair) {
        val t = TranslationHelper(context)
        item?.character?.let { c ->

            Character.attributesRefStringResourcesMap.forEach { (stringResList, statRef) ->
                if (stringResList.any {
                    t.equalsAnyIgnoreCase(
                        s = p.name, stringResourceId = it) }
                    ){
                    c.setStatBuff(
                        statRef = statRef,
                        value = p.augmentation
                    )
                }
            }
        }

    }

    override val mCalculator: Calculator
        get() {
            return when {
                rulesSR -> {
                    object : Calculator {
                        override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
                            val oldVal =  if (old_value == 0) 1 else old_value
                            var result = 0
                            for (i in (oldVal + 1)..new_value) {
                                result += i * 5
                            }
                            return result
                        }
                    }
                }
                rulesSkillfullE5 -> object : Calculator {
                    fun incrAttrByOneCost(newValue: Int, initValue: Int): Int {
                        return (newValue - initValue).let { diff ->
                            if (diff > 0) {
                                // the brackets are important, because we use integer multiplication!
                                4 + 4* ((diff + 1)/ 2)
                            } else {
                                0
                            }
                        }
                    }

                    override fun getExpCost(
                        old_value: Int,
                        new_value: Int,
                        valueHistory: List<Int>
                    ): Int {
                        var exp = 0
                        valueHistory.firstOrNull()?.let { initVal ->
                            var x = old_value + 1
                            while (x <= new_value) {
                                exp += incrAttrByOneCost(x, initVal)
                                x++
                            }
                        }
                        return exp
                    }
                }
                rulesSplittermond -> object : Calculator {
                    fun incrAttrByOneCost(newValue: Int, initValue: Int): Int {
                        return (newValue - initValue).let { diff ->
                            if (diff > 0) {
                                5*(diff+1)
                            } else {
                                0
                            }
                        }
                    }

                    override fun getExpCost(
                        old_value: Int,
                        new_value: Int,
                        valueHistory: List<Int>
                    ): Int {
                        var exp = 0
                        valueHistory.firstOrNull()?.let { initVal ->
                            var x = old_value + 1
                            while (x <= new_value) {
                                exp += incrAttrByOneCost(x, initVal)
                                x++
                            }
                        }
                        return exp
                    }
                }
                else -> object : Calculator {
                    override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {

                        var expCost = 0
                        var pointsNeededToIncreasingByOne = 0
                        for (i in old_value until new_value) {
                            val j = i - 10
                            pointsNeededToIncreasingByOne = if (i >= 10) {
                                kotlin.math.max(1, j / 2)
                            } else {
                                -(j / 2) + -j % 2
                            }
                            expCost += pointsNeededToIncreasingByOne * Global.ATTRIBUTE_POINT_EXP_COST
                        }
                        return expCost
                    }
                }
            }
        }

    init {
        stdAutoCompleteData = HashMap()
        stdAutoCompleteData[rule_system_ogres_club] =
            context.resources.getStringArray(R.array.dnd_attributes)
            .map { Helper.capitalizeNewsLetterStyle(it) }
        stdAutoCompleteData[rule_system_Dnd_E5] =
            context.resources.getStringArray(R.array.dnd_attributes)
                .map { Helper.capitalizeNewsLetterStyle(it) }
        stdAutoCompleteData[rule_system_skillfull_dnd_5E] =
            context.resources.getStringArray(R.array.dnd_attributes)
                .map { Helper.capitalizeNewsLetterStyle(it) }
        stdAutoCompleteData[RuleSystem.rule_system_sr5] =
            context.resources.getStringArray(R.array.sr_attributes)
                .map { Helper.capitalizeNewsLetterStyle(it) }
        stdAutoCompleteData[RuleSystem.rule_system_sr6] =
            context.resources.getStringArray(R.array.sr_attributes)
                .map { Helper.capitalizeNewsLetterStyle(it) }
        stdAutoCompleteData[RuleSystem.rule_system_splittermond] =
            context.resources.getStringArray(R.array.splittermond_attributes)
                .map { Helper.capitalizeNewsLetterStyle(it) }

    }

    companion object {
        fun getAttributeMod(ruleSystem: RuleSystem?, statValue: Int): Int {
            return when (ruleSystem) {
                in listOf(rule_system_ogres_club, rule_system_Dnd_E5, rule_system_skillfull_dnd_5E) ->
                    (statValue - 10).let { j ->
                        if (j >= 0) {
                            j / 2
                        } else {
                            (j / 2) + j % 2
                        }
                    }

                else -> statValue
            }
        }
    }

}