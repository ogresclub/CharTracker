package com.mtths.chartracker.charprogressitemadapters

import android.view.View
import android.widget.TextView
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.utils.Helper
import java.util.ArrayList

class CharProgressItemAdapterSRQuality(activity: ProgressBarFrameDropBoxActivity?) :
CharProgressItemAdapterSpilttermondPowers(activity = activity) {

    override fun getExpProposal(text: String, nb_of_active_chars: Int, data: ArrayList<*>): Int {
        return 0
    }

    override val valueColumnName = context.getString(R.string.karma_label)
    override var ignoreExpErrors: Boolean = true

    override fun extendGetView(v: View?, index: Int) {
        val vnp = getAutoCompleteDataOrDataIfNoAutoCompleteDataAvailable()[index]
        v?.findViewById<TextView>(R.id.number)?.apply {
            vnp.relatedLogEntry?.exp?.let {
                if (it != 0) {
                    text = it.toString()
                } else {
                    visibility = View.GONE
                }
            }

        }
        v?.findViewById<TextView>(R.id.text)?.apply {
            text = "%s%s".format(
                Helper.capitalizeNewsLetterStyle(vnp.name),
                if (vnp.value > 1) " ${vnp.value}" else ""
            )
        }
    }
}