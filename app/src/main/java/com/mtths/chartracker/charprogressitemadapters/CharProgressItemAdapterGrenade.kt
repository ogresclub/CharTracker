package com.mtths.chartracker.charprogressitemadapters

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity

import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getIntegerStatWithExtraCharsFromText
import com.mtths.chartracker.utils.StringUtils.getIntegerStatWithModificationFromText
import com.mtths.chartracker.utils.StringUtils.getStatUntilEndByCommaFromText
import com.mtths.chartracker.utils.StringUtils.getWeaponDamageFromText
import java.util.*

class CharProgressItemAdapterGrenade(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapter<CharProgressItemAdapterGrenade.Grenade>(
        mActivity = activity,
        comparator = grenade_name_comparator
    ) {
    override fun createAutoCompleteStrings(): ArrayList<String> {
        return if (rulesSR5) {
            ArrayList(
                listOf(
                    "damage =", "ap =", "amount =", "blast ="
                )
            )
        } else {
            ArrayList(
                listOf(
                    "damage =", "amount =", "blast ="
                )
            )
        }
    }
    val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values
    override val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header



    override fun generate_autocomplete_data(e: LogEntry) {

    }
    override fun add(e: LogEntry): LogEntry {
        val grenade = Grenade()
        grenade.relatedLogEntry = e
        grenade.parseName(logEntryText = e.text, item = item)
        if (rulesSR5) {
            grenade.dmg = getWeaponDamageFromText("damage", e.text)
        } else {
            grenade.dmg = getIntegerStatWithExtraCharsFromText("damage", e.text, "/A-Z")
        }
        grenade.amount = getIntegerStatWithModificationFromText("amount", e.text)
        grenade.ap = getIntegerStatFromText("ap", e.text)
        grenade.blast = getStatUntilEndByCommaFromText("blast", e.text)
        data.add(grenade)
        checkExpand(grenade)
        return e
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {

        //inflate entry
        val grenade_view: View =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false)
        val name_display: TextView = grenade_view.findViewById(R.id.text)
        val damage_display: TextView =
            grenade_view.findViewById<TextView>(R.id.number3).apply {
                visibility = View.VISIBLE
            }
        val blast_display: TextView =
            grenade_view.findViewById(R.id.number2)
        val amount_display: TextView =
            grenade_view.findViewById(R.id.number)


        val grenade: Grenade = data[index]
        name_display.text = grenade.name
        damage_display.text = grenade.dmg
        blast_display.text = grenade.blast
        amount_display.text = grenade.amount
        grenade_view.setOnLongClickListener(
            ListenerImplOpenEditLogsOnLongClick(
                mActivity!!.supportFragmentManager,
                grenade.relatedLogEntry
            )
        )

        amount_display.setOnLongClickListener {
            DialogCustom.showNumberPickerToChangeAmount(
                context = context,
                getOldAmount = { grenade.amount?.toIntOrNull() ?: 0 },
                setNewAmount = { grenade.amount = it.toString() },
                onAmountChanged = { updateAllDisplays() },
                logEntryWithAmountString = grenade.relatedLogEntry,
                amountRef = CharProgressItemAdapterGear.AMOUNT_REF,
            )
            return@setOnLongClickListener true
        }

        implementExpand(view = grenade_view, data = grenade)
        return grenade_view
    }

    override fun inflate(expand: Boolean): View? {
        return super.inflate(expand).apply {

            renameStat2ColumnHeader(
                nameId = R.string.damage_label,
                containerView = this,
                context = context
            )
            renameStat1ColumnHeader(
                nameId = R.string.blast_label,
                containerView = this,
                context = context
            )
            renameStat0ColumnHeader(
                nameId = R.string.amount_label,
                containerView = this,
                context = context
            )
        }
    }

    inner class Grenade : CharProgressItemData() {
        var ap: String? = null
        var amount: String? = null
        var blast: String? = null
        var dmg: String? = null

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                if (rulesSR5) {
                    add(ExpandData(
                        header = context.getString(R.string.ap_label),
                        text = ap.toString()))
                }
            }
    }

    companion object {
        private val grenade_name_comparator =
            Comparator<Grenade> { o1, o2 -> o1.name.compareTo(o2.name, ignoreCase = true) }
    }

}