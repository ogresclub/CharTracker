package com.mtths.chartracker.charprogressitemadapters

import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.utils.Helper.capitalizeNewsLetterStyle
import java.util.*


open class  CharProgressItemAdapterSpilttermondPowers(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapterValueBased(
        activity = activity,
        comparator = compareBy { it.name }
    ), CharProgressItemAdapterBuffable  {

    open val valueColumnName = context.getString(R.string.amount_label)
    override var allowInvalidValues: Boolean = true


    override val mCalculator = object :
        Calculator {
        override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
            return 0
        }
    }

    override fun createAutoCompleteStrings(): ArrayList<String> {
        return addBuffAutoCompleteString(
            arrayListOf("${ValueNamePair.REF_DESCRIPTION} = [")
        )
    }

    override fun inflate(expand: Boolean): View? {
        return super.inflate(expand).also {
            renameStat0ColumnHeader(
                name = capitalizeNewsLetterStyle(valueColumnName),
                containerView = it)
        }
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        return super.getView(index, check_mistakes)?.apply {
            extendGetView(v = this, index = index)
        }
    }

    open fun extendGetView(v: View?, index: Int, ) {
        v?.findViewById<TextView>(R.id.number)?.apply {
            if (text.toString() == "0") {
                visibility = View.GONE
            }
        }
    }



    override fun add(
        e: LogEntry,
        ignoreMismatches: Boolean,
        data: ArrayList<ValueNamePair>,
        dontModifyData: Boolean
    ): Holder {
        return super.add(e, ignoreMismatches, data, dontModifyData).also {
            it.holderItems.forEach {
                it.pair?.let { vnp ->
                    vnp.relatedLogEntry = e
                    createBuffs(s = e.text, iData = vnp, item = item, context = context)
                }
            }
        }
    }






}