package com.mtths.chartracker.charprogressitemadapters

import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterGear.Companion.addRatingAndCapacityAutoCompleteStrings
import com.mtths.chartracker.dataclasses.BuffEffect
import com.mtths.chartracker.dataclasses.Character.Companion.STAT_ARMOR
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import java.util.ArrayList

class CharProgressItemAdapterArmorSR(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapter<CharProgressItemAdapterArmorSR.Armor>(
        mActivity = activity,
        comparator = compareBy { it.name }
    ),
    CharProgressItemAdapterBuffable {
    val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values
    override  val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header


    override fun createAutoCompleteStrings(): ArrayList<String> {
        return arrayListOf(
            "$SOCIALSCORE_REF = ",
        ).also {
            addRatingAndCapacityAutoCompleteStrings(it)
            addBuffAutoCompleteString(it)
            Armor().addModsAutoCompleteString(it)
        }
    }

    override fun generate_autocomplete_data(e: LogEntry) {
    }

    override fun add(e: LogEntry): LogEntry {
//        Log.e("BUFFF_ARMOR", "add armor called for log: ${e.text}")

        //first entry is karma, second is text
        val armor = Armor()
        armor.relatedLogEntry = e

        /*
        set name
         */
        item?.filterConstraint?.let { armor.parseName(e.text, item) }
        armor.parseRating(e.text)
        armor.parseCapacity(e.text)
        armor.socialScore = getIntegerStatFromText(SOCIALSCORE_REF, e.text).toIntOrNull()
        data.add(armor)
        armor.parseMods(e.text)
        val extraBuffs = mutableListOf<String>().apply {
            armor.rating?.let {  add("${STAT_ARMOR} $it") }
            armor.socialScore?.let {  add("${BuffEffect.BUFF_REF_SOCIAL_RATING} $it")}
        }
        createBuffs(s = e.text, iData = armor, item = item, context = context,
            extraBuffEffects = extraBuffs

        )
        checkExpand(armor)
        return e
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        val armor: Armor = data[index]

        //inflate entry
        val entry: View =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false)
        val name_display = entry.findViewById<TextView>(R.id.text)
        val rating_display = entry.findViewById<TextView>(R.id.number)
        val social_dispay = entry.findViewById<TextView>(R.id.number2)
        name_display.text = armor.getNameDisplay()
        rating_display.text = armor.getRatingDisplay()
        social_dispay.text = armor.socialScore?.toString() ?: "-"
        mActivity?.let { a ->
            entry.setOnLongClickListener(
                ListenerImplOpenEditLogsOnLongClick(
                    a.supportFragmentManager,
                    armor.relatedLogEntry
                )
            )
        }
        implementExpand(view = entry, data = armor)
        return entry
    }

    override fun inflate(expand: Boolean): View? {
        val v = super.inflate(expand)
        renameStatHeaders(
            nameIds = listOf(
                R.string.rating_label,
                R.string.label_social
            ),
            containerView = v
        )
        return v

    }

    inner class Armor : CharProgressItemAdapterGear.Gear() {
        var socialScore: Int? = 0
    }

    companion object {
        const val DEBUG_TAG = "CHAR_PROG_ADAPTER_ARMORS"
        const val SOCIALSCORE_REF = "socialrating"
    }
}
