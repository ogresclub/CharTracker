package com.mtths.chartracker.charprogressitemadapters

import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogEditLogEntry
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import java.util.Comparator

/**
 * need to implement add method!
 */
open class CharProgressItemAdapterOneLineExpandable<T>(
    /**
     * from right to left
     */
    val colHeaderIds: List<Int>,
    val colWeights: List<Float> = listOf(1f,1f,1f,1f),
    activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<T> = compareBy { it.name }): CharProgressItemAdapter<T>(
    mActivity = activity,
    comparator = comparator) where T: CharProgressItemData {

    open val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values
    override  val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header
    open val actionIconResource: Int? = null


    /**
     * needs to be implemented!
     */
    override fun add(e: LogEntry): LogEntry {
        return e
    }

    override fun generate_autocomplete_data(e: LogEntry) {
    }

    class ViewHolder(
        var root: View? = null,
        var nameView: TextView? = null,
        /**
         * index 0 is the most right view
         */
        var dataViews: List<TextView?>? = null,
        var actionIcon: ImageView? = null,
        var extraColumnsContainer: LinearLayout? = null
    )

    /**
     * i used to override and extent inflate view function,
     * but i can use fill data as well
     */
    open fun fillData(
        mData: T,
        check_mistakes: Boolean,
        holder: ViewHolder
    ) {
        mData.viewHolder.columnsHolder = holder
    }

    /**
     * return existing view holder or inflate new one
     */
    open fun inflateView(index: Int, check_mistakes: Boolean): ViewHolder {

        return data[index].viewHolder.columnsHolder?.also {
            // remove from parent, because in fillData it is added to listOfViews
            // and app will crash if it already has a parent
            (it.root?.parent as? ViewGroup?)?.removeView(it.root)
//            Log.d("VIEW_HOLDER", "returning view holder for ${item?.filterConstraint}.")
        }?: let {


            val h = ViewHolder()
            //inflate entry
            h.root = LayoutInflater.from(mContext).inflate(
                layoutResourceEntry,
                null,
                false
            )
            h.root?.tag = h
            h.nameView = h.root?.findViewById(R.id.text)

            h.dataViews = listOf(R.id.number, R.id.number2, R.id.number3, R.id.number4).map {
                h.root?.findViewById(it)
            }
            h.actionIcon = h.root?.findViewById(R.id.action_icon)
            actionIconResource?.let {
                h.actionIcon?.apply {
                    setImageResource(it)
                    visibility = View.VISIBLE
                }
            }
            h.extraColumnsContainer = h.root?.findViewById(R.id.attributes_container)

            h.dataViews?.forEachIndexed { index, tv ->

                tv?.gravity = Gravity.CENTER
                tv?.layoutParams = LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    colWeights.getOrElse(index = index, defaultValue = { 1f })
                )

                tv?.visibility = if (index < colHeaderIds.size) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }



            h.root?.setOnLongClickListener {
                mActivity?.supportFragmentManager?.let { fm ->
                    data[index].relatedLogEntry?.id?.let { id ->

                        DialogEditLogEntry.newInstance(id).show(
                            fm,
                            DialogEditLogEntry.TAG
                        )
                    }
                }
                return@setOnLongClickListener true
            }
            return  h
        }
    }

    override fun inflate(expand: Boolean): View? {
        return super.inflate(expand).apply {
            renameStatHeaders(
                nameIds = colHeaderIds,
                weights = colWeights,
                containerView = this,
            )
            item?.let {item ->
                mActivity?.supportFragmentManager?.let { fm ->

                    header_view?.setOnLongClickListener {

                        DialogShowFilteredLogs.newInstance(
                            searchString = item.filterConstraintTranslations.joinToString(" OR "),
                            markErrors = item.check_mistakes, true
                        ).show(
                            fm,
                            DialogShowFilteredLogs.TAG
                        )

                        return@setOnLongClickListener true
                    }
                }

            }
        }
    }

    /**
     * override inflateView and leave this untouched
     */
    override fun getView(index: Int, check_mistakes: Boolean): View? {
        return inflateView(index = index, check_mistakes = check_mistakes).apply {
            fillData(mData = data[index], check_mistakes = check_mistakes, holder = this)
        }.root
    }

}