package com.mtths.chartracker.charprogressitemadapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.Skill
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.TranslationHelper
import java.lang.Integer.max
import java.util.*

/**
 * This class collects all the relevant data for a char progress item and implements how to get the
 * relevant information out of a given log entry as well as
 * how to get to view to add to the CharProgressItems listOfViews
 */
abstract class CharProgressItemAdapter<T>(
    /**
     * for context. But its important to get an activity as context
     */
    protected var mActivity: ProgressBarFrameDropBoxActivity?,
    /**
     * used to order the items after they hav been collected in a listOfViews.
     */
    open val comparator: Comparator<T>) {


    val rulesSR5: Boolean
        get() {
            return Helper.rulesSr5(character = item?.character, context = context)
        }
    val rulesSR6: Boolean
        get() {
            return Helper.rulesSr6(character = item?.character, context = context)

        }

    val rulesSR: Boolean
        get() {
            return Helper.rulesSR(character = item?.character, context = context)
        }

    val rulesSkillfullE5: Boolean
        get() {
            return Helper.rulesSkillfullE5(character = item?.character, context = context)
        }
    val rulesSplittermond: Boolean
        get() {
            return Helper.rulesSplittermond(character = item?.character, context = context)
        }
    /**
     * this layout includes the header and the linear layout for the entries
     */
    open val layoutResourceMain = R.layout.char_progress_item_container_bordered
    /**
     * this is the item related to the adapter;
     */
    var item: CharProgressItem? = null
        set(value) {
            field = value
            onItemAttached(value)
        }
    /**
     * the textView of the header
     */
    protected var header_view: TextView? = null
    /**
     * the icon indicating if the cpi is expanded or not
     */
    protected var expandedIcon: ImageView? = null
    /**
     * when at least one data item has expand data, this is set to true
     * this makes sure that the layout of all items is identical
     * and even items without expand data have invisible arrow to match their indention
     * to those items with actual expand data.
     */
    protected var useExpand = false

    /**
     * container for header_view and expandedIcon
     */
    protected var headerContainer: LinearLayout? = null
    /**
     * the container for the log entries and the header
     */
    protected var itemsContainer: LinearLayout? = null
    /**
     * the logEntries containing the String filterConstraint will be inflated and added here
     */
    protected var listOfViews: LinearLayout? = null
    var dataBodyContainer: LinearLayout? = null

    var  stdAutoCompleteData: HashMap<RuleSystem,List<String>> = HashMap()

    open val expandContainerId: Int = R.id.mod_container





    /**
     * here all the data will be collected
     */
    //todo vielleicht sollte es hier nur data geben und kein extra autocomplete data
    var data = ArrayList<T>()
    var auto_complete_data = ArrayList<T>()

    /**
     * This function is called, when the charProgressItem is attached to the adapter
     */
    open fun onItemAttached(item: CharProgressItem?) {}


    fun updateAllDisplays(generateCPI: Boolean = false) {
        try {
            (mActivity as ActivityCharDetails).updateDisplays(generateCPI = generateCPI)
        } catch (e: Exception) {
            Helper.log("can't update displays after changing expand option for charProgressItem because activity is not activityCharDetails")
            e.printStackTrace()
        }
    }
    fun updateDisplaySessionStats() {
        try {
            (mActivity as ActivityMain).getSessionDataDisplay()?.fill()
        } catch (e: Exception) {
            Helper.log("can't update displays after changing expand option for charProgressItem because activity is not activityCharDetails")
            e.printStackTrace()
        }
    }

    fun inflateCharDetails() {
        try {
            (mActivity as ActivityCharDetails).fragmentCharDetails?.inflateCharDetails()
        } catch (e: Exception) {
            Helper.log("can't update displays after changing expand option for charProgressItem because activity is not activityCharDetails")
            e.printStackTrace()
        }
    }


    fun getAutoCompleteDataOrDataIfNoAutoCompleteDataAvailable(): ArrayList<T> {
        return if (auto_complete_data.size > 0) {
            auto_complete_data
        } else {
            data
        }
    }

    protected var mContext: Context = mActivity!!
    val context: Context
        get() = mActivity!!


    val count: Int
        get() = max(data.size, auto_complete_data.size)

    open fun sort() {
        Collections.sort(data, comparator)
    }

    open fun clear() {
        data.clear()
        auto_complete_data.clear()
    }

    /**
     * std doesn't do anything. Change if needed.
     * @param data the list where current values are checked to determine how much the cost is.
     */
    open fun getExpProposal(text: String, nb_of_active_chars: Int, data: ArrayList<*>): Int {
        return 0
    }

    /**
     * get the data which should be displayed in CharDetails and collect in data
     * @param e
     * @return
     */
    abstract fun add(e: LogEntry): LogEntry

    /**
     * this is called when you want to extract data from other charProgressItems then your own
     * to use, in CharProgMetaData the corresponding charProg Item has to be added as relevantItems
     */
    open fun getExtraData(text: String) {
    }

    /**
     * only need for valueBasedCharProgressItems
     * we need this, because in sr, tah skills are value based, but don't actually inherit CPIValueBased
     * otherwise ignore
     */
    open fun getAutoCompleteValue(name: String): Int? {
        return null
    }


    abstract fun getView(index: Int, check_mistakes: Boolean): View?

    /**
     * Use this to add additional views to container (e.g. seperators)
     * @param listOfViews the container with all the views created with getView
     */
    open fun doAfterGetView(listOfViews: LinearLayout?, index: Int) {}
    /**
     * Use this to add additional views to container (e.g. seperators)
     * @param listOfViews the container with all the views created with getView
     */
    open fun doBeforeGetView(listOfViews: LinearLayout?, index: Int) {}

    /**
     * if the filterConstraint in the log entry is not using the current locale language
     * replace it with the current
     */
    fun updateFilterConstraintToCurrentLanguage(text: String): String {
        return item?.let { item ->
            TranslationHelper(context).getAllTranslations(item.filterConstraintId).let {
                text.replace(Regex("(${it.joinToString(" |")} )")
//                    .also { Log.d("VALUE_BASEDTRANS", it.toString()) }
                    , "${item.filterConstraint} ")
            }
        } ?: text
    }

    /**
     * should be called in add method when using implementExpand
     * necessary for implementExpand to work
     */
    open fun checkExpand(data: CharProgressItemData) {
        if (data.expandData.isNotEmpty()) {
            useExpand = true
        }
    }

    open fun addExpandDataViewsToContainer(
        dataItem: CharProgressItemData,
        container: LinearLayout
    ) {
        dataItem.expandData.forEach {
            it.getView(mActivity)?.let {v ->
                container.addView(v)
                dataItem.viewHolder.expandHolder[it.header] = v
            }
        }
    }

    /**
     * you NEED to call checkExpand at the end of add
     * otherwise this will do nothing,
     * because useExpand will never be set to true
     */
    open fun implementExpand(view: View?, data: T) {

        fun setExpandMarker(iv: ImageView, d: CharProgressItemData) {
            setExpandMarker(iv = iv, expand = d.expanded)
        }

        fun fillExpandData(d: CharProgressItemData, container: LinearLayout) {
//            Log.d("EXPAND_DATA", "${d.name}: expand data: ${d.expandData.joinToString {d.displayExpandData(it)}}")
            container.removeAllViews()
            if (d.expanded) {
                addExpandDataViewsToContainer(
                    dataItem = d,
                    container = container)
            }
        }

        if (useExpand) {

            (data as? CharProgressItemData?)?.let { d ->
                val prefs = PrefsHelper(context)
                d.expanded = prefs.getExpandState(d)

                view?.setPadding(0, 0, 0, 0)

                view?.findViewById<LinearLayout>(expandContainerId)?.let { container ->
                    view.findViewById<ImageView>(R.id.expand_icon)?.let { iv ->

                        iv.visibility = if (d.expandData.isNotEmpty()) {
                            View.VISIBLE
                        } else {
                            View.INVISIBLE
                        }


                        //set icon expanded according to value
                        setExpandMarker(iv = iv, d = d)
                        fillExpandData(d = d, container = container)

                        iv.setOnClickListener {
                            d.expanded = !d.expanded
                            prefs.setExpandState(data = d, value = d.expanded)
                            setExpandMarker(iv = iv, d = d)
                            fillExpandData(d = d, container = container)
                        }
                    }

                }

            }
        }
    }

    fun setExpandMarker(iv: ImageView?, expand: Boolean) {
        iv?.setImageResource(
            if (expand) {
                R.drawable.expanded_marker
            } else {
                R.drawable.expand_marker
            })
    }

    /**
     * called in inflate.
     * fill in data of all the cpi collected
     */
    open fun fillItems(expand: Boolean) {
        sort()
        listOfViews?.removeAllViews()
        if (expand) {
            for (i in 0 until count) {
                doBeforeGetView(
                    listOfViews = listOfViews, index = i
                )
                getView(i, item!!.check_mistakes)?.let {

                    listOfViews?.addView(it)
                }
                doAfterGetView(
                    listOfViews = listOfViews, index = i
                )
            }
        }
        /*
        make empty charProgressItems invisible if listOfViews is empty
        */
        if (count == 0) {
            itemsContainer?.visibility = View.GONE
        }
    }


    /**
     * this will inflate the header and entries, but they still have to be added to the parent view
     * to make them visible
     */
    open fun inflate(expand: Boolean): View? {
        val inflater = LayoutInflater.from(context)
        itemsContainer = inflater.inflate(layoutResourceMain, null, false) as LinearLayout
        //INFLATE HEADER------------------------------------------------------------------
        header_view = itemsContainer?.findViewById(R.id.charProgress_label)
        headerContainer = itemsContainer?.findViewById(R.id.header_container)
        expandedIcon = headerContainer?.findViewById(R.id.char_progress_expand_icon)
        setExpandMarker(iv = expandedIcon, expand = expand)
        header_view?.text = if (expand) item?.header else "%s (%s)".format(item?.header, count)
        expandedIcon?.setOnClickListener {

            item?.let{ itm ->
                val orientation = context.resources.configuration.orientation
                if (mActivity is ActivityMain) {

                    PrefsHelper(context).toggleExtend(
                        charProgItemFilterConstraintId = itm.filterConstraintId,
                        orientation = orientation
                    )
                    updateDisplaySessionStats()

                } else {

                    itm.character?.also { c ->

                        c.toggleExtend(
                            charProgItemfilterConstraintId = itm.filterConstraintId,
                            context = context
                        )
                        DatabaseHelper.getInstance(context)?.updateCharacter(c, keep_last_update_date = false)
                        val newExtend = c.getExtend(
                            charProgItemfilterConstraintId = itm.filterConstraintId,
                            context = context
                        )
                        applyExtendVisibility(newExtend)
//                    Toast.makeText(
//                        context,
//                        "expand ${item?.filterConstraint} = ${item?.let {c.getExtend(it.filterConstraint, orientation)}}\n" +
//                                "${c.extendCharProgItemsMap}",
//                        Toast.LENGTH_SHORT).show()
                    }
                }


            }
        }


        //-----------------------------------------------------------------------------
        //INFLATE ENTRIES
        listOfViews = itemsContainer?.findViewById(R.id.charProgress_list)
        dataBodyContainer = itemsContainer?.findViewById(R.id.data_body_container)
        fillItems(expand = true)


        //-----------------------------------------------------------------------------
        //HIDE DATA IF COLLAPSED
        if (!expand) {
            applyExtendVisibility(false)
        }
        return itemsContainer
    }

    fun applyExtendVisibility(expand: Boolean) {
        val visibility = if (expand) View.VISIBLE else View.GONE
        listOfViews?.visibility = visibility
        dataBodyContainer?.visibility = visibility
        setExpandMarker(expandedIcon, expand = expand)
    }

    /**
     * get the data which should be auto completed from a logEntry and store it in
     * auto_complete_data
     * @param e
     */
    abstract fun generate_autocomplete_data(e: LogEntry)

    /**
     * this is is called when the char progress item is active and uses autocomplete.
     * Then this list is added to the autocomplete adapter
     * @return
     */
    open fun createAutoCompleteStrings(): ArrayList<String> {
        return ArrayList()
    }

    /**
     * use this when the auto complete should be replaced by something else
     * e.g. I use it to automatically add buffs for feats
     */
    open fun getAutoCompleteReplacement(autoCompleteString: String) = autoCompleteString

    /**
     * This should return this like -close for quests oder -hide for characters or what ever
     * feature marker this char progress item will have.
     * @return
     */
    open fun createAutoCompleteFeatureMarkers(): ArrayList<String> {
        return ArrayList()
    }

    fun markHeaderWhenCountingPoints(free_featured_points: Int): String? {
        var header_text: String? = item?.header
        if (free_featured_points != 0) {
            header_text += " ($free_featured_points)"
        }
        //set header color
        if (free_featured_points > 0) {
            header_view?.setBackgroundResource(R.color.holo_green_light_half_transparent)
        } else if (free_featured_points < 0) { //now you have use more skills/language points than you should have
            header_view?.setBackgroundResource(R.color.holo_red_light_half_transparent)
        }
        return header_text
    }

    fun getSkillValue(nameId: Int): Int {
        return Skill.getSkillValue(
            nameId = nameId,
            character = item?.character,
            context = context
        )
    }

    fun getSkillValue(skillName: String): Int {
        return Skill.getSkillValue(
            skillName = skillName,
            character = item?.character,
            context = context
        )
    }

    companion object {

        fun renameStatHeaders(
            nameIds: List<Int>,
            containerView: View?,
            weights: List<Float> = listOf(1f,1f,1f,1f)
        ) {
           listOf(
               R.id.stat_0_label,
               R.id.stat_1_label,
               R.id.stat_2_label,
               R.id.stat_3_label,
               ).forEachIndexed { index, statLabelId ->
                   containerView?.findViewById<TextView>(statLabelId)?.apply {

                       if (index < nameIds.count()) {

                           visibility = View.VISIBLE
                           nameIds.getOrNull(index)?.let {
                               text = Helper.capitalizeNewsLetterStyle(context.getString(it))
                           }
                           layoutParams = LinearLayout.LayoutParams(
                               0,
                               LinearLayout.LayoutParams.WRAP_CONTENT,
                               weights.getOrNull(index) ?: 1f
                           )
                       } else {
                           visibility = View.GONE
                       }
                   }
           }
        }
        /**
         * @return column header view
         */
        fun renameStat2ColumnHeader(
            name: String,
            containerView: View?,
            weight: Float = 1f
        ): TextView? {
            return containerView?.findViewById<TextView>(R.id.stat_2_label)?.let {
                it.visibility = View.VISIBLE
                it.text = name
                it.layoutParams = LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    weight
                )
                it
            }
        }
        /**
         * @return column header view
         */
        fun renameStat2ColumnHeader(
            nameId: Int?,
            containerView: View?,
            weight: Float = 1f,
            context: Context
        ): TextView? = renameStat2ColumnHeader(
            name = Helper.capitalizeNewsLetterStyle(nameId?.let { context.getString(it) } ?: ""),
            containerView = containerView,
            weight = weight
        )
        /**
         * @return column header view
         */
        fun renameStat3ColumnHeader(
            name: String,
            containerView: View?,
            weight: Float = 1f
        ): TextView? {
            return containerView?.findViewById<TextView>(R.id.stat_3_label)?.let {
                it.visibility = View.VISIBLE
                it.text = name
                it.layoutParams = LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    weight
                )
                it
            }
        }
        /**
         * @return column header view
         */
        fun renameStat3ColumnHeader(
            nameId: Int?,
            containerView: View?,
            weight: Float = 1f,
            context: Context
        ): TextView? = renameStat3ColumnHeader(
            name = Helper.capitalizeNewsLetterStyle(nameId?.let { context.getString(it) } ?: ""),
            containerView = containerView,
            weight = weight
        )

        /**
         * @return column header view
         */
        fun renameStat0ColumnHeader(
            name: String,
            containerView: View?,
            weight: Float = 1f
        ): TextView? {
            return containerView?.findViewById<TextView>(R.id.stat_0_label)?.let {
                it.text = name
                it.layoutParams = LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    weight
                )
                it
            }
        }

        /**
         * @return column header view
         */
        fun renameStat0ColumnHeader(
            nameId: Int?,
            containerView: View?,
            weight: Float = 1f,
            context: Context
        ): TextView? = renameStat0ColumnHeader(
            name = Helper.capitalizeNewsLetterStyle(nameId?.let { context.getString(it) } ?: ""),
            containerView = containerView,
            weight = weight
        )

        /**
         * @return column header view
         */
        fun renameStat1ColumnHeader(
            name: String,
            containerView: View?,
            weight: Float = 1f
        ): TextView? {
            return containerView?.findViewById<TextView>(R.id.stat_1_label)?.let {
                it.text = name
                it.layoutParams = LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    weight
                )
                it
            }
        }
        /**
         * @return column header view
         */
        fun renameStat1ColumnHeader(
            nameId: Int?,
            containerView: View?,
            weight: Float = 1f,
            context: Context
        ): TextView? = renameStat1ColumnHeader(
            name = Helper.capitalizeNewsLetterStyle(nameId?.let { context.getString(it) } ?: ""),
            containerView = containerView,
            weight = weight
        )
    }
}