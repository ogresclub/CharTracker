package com.mtths.chartracker.charprogressitemadapters

import androidx.constraintlayout.solver.widgets.Helper
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.Global
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.rule_system_splittermond
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.utils.Helper.capitalizeNewsLetterStyle


class CharProgressItemAdapterResources (activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapterValueBased(
        activity = activity,
        comparator = compareBy { it.name }
    )  {

    override val mCalculator = object :
        Calculator {
        override fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
            return 7 * (new_value - old_value)
        }
    }

    override fun add(
        e: LogEntry,
        ignoreMismatches: Boolean,
        data: ArrayList<ValueNamePair>,
        dontModifyData: Boolean
    ): Holder {
        val holder = super.add(e, ignoreMismatches, data, dontModifyData)

        /*
        store attributes at character
         */

        item?.character?.let { c ->

            holder.holderItems.forEach { h ->
                h.pair?.let { p ->
                    c.setStat(p.name, p.value)
                }
            }
            DatabaseHelper.getInstance(context)?.updateCharacter(
                character = c, keep_last_update_date = true)
        }

        return holder
    }

    init {
        stdAutoCompleteData[rule_system_splittermond] =
            context.resources.getStringArray(R.array.splittermond_recources)
                .map { capitalizeNewsLetterStyle(it) }
    }


}