package com.mtths.chartracker.charprogressitemadapters

import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.LogEntry

open class CharProgressItemAdapterStdBuffable(
    activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<CharProgressItemAdapterStd.StdEntry>,
    cutNameAfterComma: Boolean = false,
    split: Boolean = false,
    hideColumnNames: Boolean = false,
    columnName: String = "Cost") :
    CharProgressItemAdapterStd(
        activity = activity,
        comparator = comparator,
        split = split,
        hideColumnNames = hideColumnNames,
        columnName = columnName,
        cutNameAfterComma = cutNameAfterComma),
    CharProgressItemAdapterBuffable {

    override fun createAutoCompleteStrings(): ArrayList<String> {
        return addBuffAutoCompleteString(super.createAutoCompleteStrings())
    }

    override fun generateAddData(e: LogEntry): StdHolder {
        val h = super.generateAddData(e)
        h.entries.firstOrNull()?.let {
            createBuffs(s = e.text, iData = it, item = item, context = context)
        }
        return h
    }
}