package com.mtths.chartracker.charprogressitemadapters

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.Global.FEATURED_MARKER
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.interfaces.CPIDescriptive
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.max

/**
 * behave pretty much like ware, except for essence being replaced by loyalty. But I didn't want to
 * make a general thing out of the ware adapter, because I though that maybe I want to change the connection later
 * anyway, so I just did this little "fix"
 */
class CharProgressItemAdapterConnection(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapter<CharProgressItemAdapterConnection.Connection>(
        mActivity = activity,
        comparator = connection_name_comparator
    ) {
    val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values
    override val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header

    override fun onItemAttached(item: CharProgressItem?) {
        super.onItemAttached(item)
        item?.character?.getPrefs(context)?.featured_connection_points_used = 0
    }


    override fun createAutoCompleteStrings(): ArrayList<String> {
        return arrayListOf(
            "loyalty =", "rating ="
        ).apply {
            add("${ValueNamePair.REF_DESCRIPTION} = [")
        }

    }

    override fun generate_autocomplete_data(e: LogEntry) {
        add(e = e, data = auto_complete_data)
    }

    fun getExpProposal(
        text: String?,
        proposalString: String?,
        nb_of_active_chars: Int,
        data: ArrayList<Connection?>?
    ): String? {
        var proposalString = proposalString
        val loyalty_as_string = getIntegerStatFromText("loyalty", text!!)
        val rating_as_string = getIntegerStatFromText("rating", text)
        var total_connection_rank = 0
        if (!loyalty_as_string.equals("-", ignoreCase = true)) {
            total_connection_rank += loyalty_as_string.toInt()
        }
        if (!rating_as_string.equals("-", ignoreCase = true)) {
            total_connection_rank += rating_as_string.toInt()
        }
        if (total_connection_rank > 0) {
            proposalString = "-${getExpCost(total_connection_rank)}"
        }
        return proposalString
    }

    fun getConnection(name: String): Connection? = data.find { it.name.equals(name, ignoreCase = true) }

    override fun add(e: LogEntry): LogEntry {
        return add(e = e, data = data)
    }

    fun add(e: LogEntry, data: ArrayList<Connection>): LogEntry {
        //first entry is karma, second is text
        val conn = Connection()
        conn.relatedLogEntry = e

        conn.parseName(e.text, item)
        conn.parseDescription(text = e.text, context = context)
        conn.loyalty = getIntegerStatFromText("loyalty", e.text).toIntOrNull() ?: 0
        conn.rating = getIntegerStatFromText("rating", e.text).toIntOrNull() ?: 0

        getConnection(conn.name)?.let {
            it.loyalty = max(it.loyalty, conn.loyalty)
            it.rating = max(it.rating, conn.rating)
        } ?: run {
            data.add(conn)
        }


        if (rulesSR) {
            if (e.text.lowercase(Locale.getDefault()).contains(FEATURED_MARKER)) {
                item?.character?.let { c ->
                    val amount_of_featured_points_added: Int
                    var total_connection_rank = 0
                    total_connection_rank += conn.loyalty
                    total_connection_rank += conn.rating

                    amount_of_featured_points_added = getExpCost(total_connection_rank)
                    if (total_connection_rank > 0) {
                        c.getPrefs(context).featured_connection_points_used += total_connection_rank
                    }
                }
            }
        }


        checkExpand(conn)
        return e
    }

    override fun inflate(expand: Boolean): View? {
        return super.inflate(expand).apply {
            renameStat1ColumnHeader(
                nameId = R.string.loyalty_label,
                containerView = this,
                context = context
            )
            renameStat0ColumnHeader(
                nameId = R.string.connection_rating_label,
                containerView = this,
                context = context
            )


            if (expand && PrefsHelper(context).checkFeaturedStats) {
                item?.character?.let { c ->
                        val free_featured_connections =
                            c.totalConnectionPoints - c.getPrefs(context).featured_connection_points_used
                        header_view?.text = markHeaderWhenCountingPoints(free_featured_connections)

                }

            }
        }
    }

    override fun getView(index: Int, check_mistakes: Boolean): View {
        val connection = data[index]

        //inflate entry
        val connection_view: View =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false)
        val name_display: TextView = connection_view.findViewById(R.id.text)
        val rating_display: TextView = connection_view.findViewById(R.id.number)
        val loyalty_display: TextView = connection_view.findViewById(R.id.number2)
        name_display.text = connection.name
        rating_display.text = connection.rating.toString()
        loyalty_display.text = connection.loyalty.toString()
        connection_view.setOnLongClickListener(
            ListenerImplOpenEditLogsOnLongClick(
                mActivity!!.supportFragmentManager,
                connection.relatedLogEntry
            )
        )
        implementExpand(view = connection_view, data = connection)
        return connection_view
    }

    inner class Connection : CharProgressItemData(), CPIDescriptive {
        var loyalty: Int = 0
        var rating: Int = 0
        override var description: String = ""

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                if (description.isNotBlank()) {
                    add(ExpandData(header = "", text = description))                }
            }

    }

    companion object {
        fun getExpCost(loyalty_plus_rating: Int): Int {
            return loyalty_plus_rating * (loyalty_plus_rating + 1) / 2
        }

        private val connection_name_comparator =
            Comparator<Connection> { o1, o2 -> o1.name.compareTo(o2.name, ignoreCase = true) }
    }
}