package com.mtths.chartracker.charprogressitemadapters

import android.util.Log
import android.view.View
import android.widget.ImageView
import com.mtths.chartracker.adapters.LogAdapter
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.R
import java.util.ArrayList

class CharProgressItemAdapterPlaces(
    activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<CharProgressItemAdapterCharacter.CharProgCharacter>,
    namesToAutoComplete: ArrayList<LogAdapter.ColoredString>) :

    CharProgressItemAdapterCharacter(
        activity = activity,
        comparator = comparator,
        namesToAutoComplete = namesToAutoComplete
    ) {

    override fun sort() {
        when (PrefsHelper(context).sortPlacesSessionStats) {
            PrefsHelper.PREF_SESSION_STATS_SORT_PLACES_ALPHABETICAL -> {
                data.sortWith(compareBy ({ it.hide }, { it.name }))
            }
            PrefsHelper.PREF_SESSION_STATS_SORT_PLACES_HISTORY -> {
                data.sortWith(compareByDescending{ it.logEntry?.dateOfCreation })
//                Toast.makeText(context, "sort date", Toast.LENGTH_LONG).show()
            }
            else -> {
                super.sort()
            }
        }
    }

    override val color = R.color.places_name_color

    override var showAllItems
        get() = PrefsHelper(context).showAllPlacesSessionStats
        set(value) {
            PrefsHelper(context).showAllPlacesSessionStats = value
        }

    override var showDescription: Boolean
        get() =  PrefsHelper(context).showPlacesDescriptionsInSessionStats
        set(value) {
            PrefsHelper(context).showPlacesDescriptionsInSessionStats = value
        }

    override fun setHeaderOnClickListeners(v: View?) {
        header_view?.setOnLongClickListener {
            PrefsHelper(context).showAllPlacesSessionStats = !PrefsHelper(context).showAllPlacesSessionStats
//            Toast.makeText(context, "show all chars = ${Global.showAllChars}", Toast.LENGTH_LONG).show()
            refresh()
            true
        }
        header_view?.setOnClickListener {
            val prefs = PrefsHelper(context)
            prefs.showPlacesDescriptionsInSessionStats = !prefs.showPlacesDescriptionsInSessionStats
            refresh()
        }


        val iconSortAlphabetically = v?.findViewById<ImageView>(R.id.icon_sort_alphabetically)
        val iconSortHistory = v?.findViewById<ImageView>(R.id.icon_sort_history)
        iconSortAlphabetically?.setOnClickListener {
            Log.d("SORT_SESSION_STATS", "sort places alphabetically")
            PrefsHelper(context).sortPlacesSessionStats =
                PrefsHelper.PREF_SESSION_STATS_SORT_PLACES_ALPHABETICAL
            sort()
            refresh()
        }
        iconSortHistory?.setOnClickListener {
            Log.d("SORT_SESSION_STATS", "sort places history")
            PrefsHelper(context).sortPlacesSessionStats =
                PrefsHelper.PREF_SESSION_STATS_SORT_PLACES_HISTORY
            sort()
            refresh()
        }
    }
}