package com.mtths.chartracker.charprogressitemadapters

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogCustom.Companion.showNumberPickerToChangeAmount
import com.mtths.chartracker.interfaces.Moddable
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import kotlin.collections.ArrayList

open class CharProgressItemAdapterGearSplittermond(
    activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapter<CharProgressItemAdapterGearSplittermond.GearSplittermond>(
        mActivity = activity,
        comparator = compareBy { it.name }
    ),
    CharProgressItemAdapterBuffable {
    val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values
    override  val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header


    override fun createAutoCompleteStrings(): ArrayList<String> {
        return super.createAutoCompleteStrings().also {
            GearSplittermond().addModsAutoCompleteString(it)
            addBuffAutoCompleteString(it)
            addAmountAutocompleteStrings(it)

        }
    }

    override fun generate_autocomplete_data(e: LogEntry) {

    }

    override fun add(e: LogEntry): LogEntry {

        //first entry is karma, second is text
        val gear = GearSplittermond()
        gear.relatedLogEntry = e

        /*
        set name
         */
        item?.filterConstraint?.let { gear.parseName(e.text, item) }
        gear.parseAmount(e.text)

        data.add(gear)
        gear.parseMods(e.text)
        createBuffs(s = e.text, iData = gear, item = item, context = context)
        checkExpand(gear)
        return e
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        val gear: GearSplittermond = data[index]

        //inflate entry
        val gear_view: View =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false)
        val name_display = gear_view.findViewById<TextView>(R.id.text)
        val amount_display = gear_view.findViewById<TextView>(R.id.number)
        name_display.text = gear.getNameDisplay()
        amount_display.text = gear.amount.toString()

        gear_view.findViewById<TextView>(R.id.number2).apply { visibility = View.GONE }

        amount_display.setOnLongClickListener {
            showNumberPickerToChangeAmount(
                context = context,
                getOldAmount = {gear.amount},
                setNewAmount = {gear.amount = it},
                onAmountChanged = {updateAllDisplays()},
                logEntryWithAmountString = gear.relatedLogEntry,
                amountRef = AMOUNT_REF
            )
            true
        }

        mActivity?.let { a ->
            gear_view.setOnLongClickListener(
                ListenerImplOpenEditLogsOnLongClick(
                    a.supportFragmentManager,
                    gear.relatedLogEntry
                )
            )
        }
        implementExpand(view = gear_view, data = gear)
        return gear_view
    }

    override fun inflate(expand: Boolean): View? {
        val container = super.inflate(expand)
        renameStat0ColumnHeader("Anzahl", container)
        return container
    }


    open class GearSplittermond : CharProgressItemData(), Moddable {
        override val mods: MutableList<String> = mutableListOf()
        var amount: Int = 1
        override val _REF_MOD: String? = null // means is used DEFAULT REF_MOD

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                addAll(modsExpandData)
            }


        fun parseAmount(s: String) {
            amount = getIntegerStatFromText(AMOUNT_REF, s).toIntOrNull() ?: 1
        }


        fun getNameDisplay(): String {
            return "%s".format(name)
        }
    }

    companion object {
        const val DEBUG_TAG = "CHAR_PROG_ADAPTER_GEAR_SPLITTERMOND"
        const val INVALID_INTEGER_INDICATOR = -999
        const val AMOUNT_REF = "anzahl"

        fun addAmountAutocompleteStrings(list: ArrayList<String>) {
            list.add("$AMOUNT_REF = ")

        }
    }
}
