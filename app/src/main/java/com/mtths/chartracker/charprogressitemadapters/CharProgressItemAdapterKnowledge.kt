package com.mtths.chartracker.charprogressitemadapters

import android.view.View
import android.widget.TextView
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.Session

class CharProgressItemAdapterKnowledge (
    activity: ProgressBarFrameDropBoxActivity?,
) : CharProgressItemAdapterLanguages(activity) {
    init {
        stdAutoCompleteData = HashMap()

        stdAutoCompleteData[RuleSystem.rule_system_sr5] =
            mActivity?.resources?.getStringArray(R.array.knowledges_sr5)?.toList() ?: ArrayList()

        stdAutoCompleteData[RuleSystem.rule_system_sr6] =
            mActivity?.resources?.getStringArray(R.array.knowledges_sr5)?.toList() ?: ArrayList()
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        val v = super.getView(index, check_mistakes)
        if (rulesSR6) {
            v?.findViewById<TextView>(R.id.number)?.visibility = View.INVISIBLE
        }
        return v
    }

    override fun inflate(expand: Boolean): View? {
        return super.inflate(expand).apply {
            renameStatHeaders(
                nameIds = listOf(),
                weights = listOf(),
                containerView = this
            )
        }
    }
}