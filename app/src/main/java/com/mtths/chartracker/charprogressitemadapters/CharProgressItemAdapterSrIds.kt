package com.mtths.chartracker.charprogressitemadapters

import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.interfaces.CPIDescriptive
import com.mtths.chartracker.interfaces.Moddable
import com.mtths.chartracker.utils.StringUtils
import java.util.ArrayList

class CharProgressItemAdapterSrIds(
    activity: ProgressBarFrameDropBoxActivity?
):
    CharProgressItemAdapterOneLineExpandable<CharProgressItemAdapterSrIds.SrID> (
        activity = activity,
        colHeaderIds = listOf(
            R.string.rating_label
        )
    ) {
    override fun createAutoCompleteStrings(): ArrayList<String> {
        return super.createAutoCompleteStrings().apply {
            addAll(
                listOf(REF_RATING).map {
                    "${context.getString(REF_RATING).lowercase()} = "
                }
            )

            addAll(
                listOf(
                    REF_DESCRIPTION,
                    REF_LICENCES
                ).map {
                    "${context.getString(it).lowercase()} = ["
                }
            )
        }
    }

    override fun add(e: LogEntry): LogEntry {

        data.add(
            SrID().apply {
                relatedLogEntry = e
                parseRating(s = e.text)
                parseMods(text = e.text) //parses licences in this case
                parseName(logEntryText = e.text, item = item)
                parseDescription(text = e.text, context = context)
                checkExpand(this)
            }
        )



        return e
    }

    override fun inflateView(index: Int, check_mistakes: Boolean): ViewHolder {
        return super.inflateView(index, check_mistakes).apply {
            val mId = data[index]

            dataViews?.get(0)?.text = mId.rating.toString()

            nameView?.text = mId.name


            implementExpand(
                view = this.root, data = mId
            )

        }
    }


    val REF_LICENCES = R.string.cpi_ref_licences
    val REF_DESCRIPTION = R.string.description_label
    val REF_RATING = R.string.rating_label

    inner class SrID(
        var rating: Int? = null,
        override var description: String = ""
    ): CharProgressItemData(), CPIDescriptive, Moddable {

        override val mods: MutableList<String> = mutableListOf() //mods are licences
        override val _REF_MOD: String = context.getString(REF_LICENCES)

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                if (mods.isNotEmpty()) add(modsExpandDataWithHeader)
                if (description.isNotBlank()) {
                    add(
                        ExpandData(
                            headerId = REF_DESCRIPTION,
                            text = description
                        )
                    )
                }
            }

        fun parseRating(s: String) {
            rating = StringUtils.getIntegerStatFromText(
                statId = REF_RATING,
                text = s,
                context = context
            ).toIntOrNull()
        }
    }

}