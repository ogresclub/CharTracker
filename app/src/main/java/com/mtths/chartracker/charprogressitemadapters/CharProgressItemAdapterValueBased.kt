package com.mtths.chartracker.charprogressitemadapters

import android.content.Context
import android.graphics.Typeface
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnLongClickListener
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterAttribute.Companion.getAttributeMod
import com.mtths.chartracker.dataclasses.BuffEffect
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.utils.Helper.getRegexContainsFilterConstraint
import com.mtths.chartracker.utils.Helper.getTextAfterStringAndTrim
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.Specialization
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.listeners.ListenerImplToastOnDoubleClickToggleExtendOnSingleClick
import com.pedromassango.doubleclick.DoubleClick
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.capitalizeNewsLetterStyle
import com.mtths.chartracker.utils.Helper.checkIgnoreExp
import com.mtths.chartracker.utils.StringUtils
import java.lang.Integer.max
import java.lang.Integer.min
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.HashMap


open class  CharProgressItemAdapterValueBased(
    val activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<ValueNamePair>,
    open val mCalculator: Calculator = object :
        Calculator {}
    ):
    CharProgressItemAdapterOneLineExpandable<ValueNamePair> (
        //todo col header could be set to ValueBased Parameters and than handed here, but I am too lazy now
        colHeaderIds = listOf(R.string.score, R.string.empty),
        activity = activity,
        comparator = comparator
    ) {


    val point_exp_cost = 200
    val NO_VALID_VALUE_OR_NO_AT_VALUE_IS_FOUND = -2
    val NEW_VALUE_NAME_PAIR_CREATED = -1
    val prefs = PrefsHelper(context)
    var colNameHeader: TextView? = null
    var colTotalHeader: TextView? = null
    var colBaseHeader: TextView? = null

    /**
     * this means that we don't need values but used @filterConstraint Blub
     * and default value = 1 will be set automatically
     * values other than 1 will have not effect
     * I used this for languages, because in some rulesystems they have levels and in some
     * they don't
     *
     * this makes sure, that when value is missing, no INVALID_VALUE_INDICATOR will be produced
     */
    open val ignoreValues: Boolean = false

    /**
     * set to true when not wanting to mark potential invalid entries
     */
    open var allowInvalidValues: Boolean = false
    open var ignoreExpErrors: Boolean = false

    var totalExpSpendForThisItem = 0

    override val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header


    open var DEBUG_TAG = "VALUE_BASED_ADAPTER"


    interface Calculator {

        fun getExpCost(old_value: Int, new_value: Int, valueHistory: List<Int>): Int {
            return Global.SKILL_POINT_EXP_COST * (new_value - old_value)
        }

        fun getSpecializationCost(
            oldSpecializations: List<Specialization>?,
            newSpecialization: List<Specialization>?
        ): Int {
            return 0
        }

    }

    override fun generate_autocomplete_data(e: LogEntry) {
        val holder: Holder = add(e, true, auto_complete_data)
        for (holderItem in holder.holderItems) {
            if (holderItem.index_of_changed_pair == NEW_VALUE_NAME_PAIR_CREATED) {
                holderItem.pair?.let { auto_complete_data.add(it) }
            }
        }
    }

    override fun getAutoCompleteValue(name: String): Int? {
        return getValueNamePair(name, getAutoCompleteDataOrDataIfNoAutoCompleteDataAvailable())?.value
    }

    override fun createAutoCompleteStrings(): ArrayList<String> {
//        Helper.log("AUTO_COMPLETE", "create autocomplete strings for item '${item?.header}': stdAutoCompleteList = $stdAutoCompleteData")
        val l = ArrayList<String>()
        PrefsHelper(context).activeRuleSystems.forEach { ruleSystem ->
            stdAutoCompleteData[ruleSystem]?.let { l.addAll(it) }
        }
        val auto_complete_strings = l.distinct().toMutableList()
        for (pair in auto_complete_data) {
            if (auto_complete_strings.find { it.equals(pair.name, ignoreCase = true) } == null) {
                auto_complete_strings.add(pair.name)
            }
        }
        return auto_complete_strings.toCollection(ArrayList())
    }

    /**
     * add a logEntry to the data.
     * It will be checked if there is already a valueNamePair with the name (text between filterConstraint and @Value)
     * and the value will be updated or a new value name pair will be created
     * @param e
     * @return the LogEntry, but e.hasError is set now
     */
    override fun add(e: LogEntry): LogEntry {
        val holder: Holder =
            add(e, false, getAutoCompleteDataOrDataIfNoAutoCompleteDataAvailable())
        for (h in holder.holderItems) {
            h.pair?.let { pair ->
                h.index_of_changed_pair?.let { idx_of_changed_pair ->
                    if (idx_of_changed_pair < 0) {
                        getAutoCompleteDataOrDataIfNoAutoCompleteDataAvailable().add(pair)
                    }
                }
            }
        }
        return holder.logEntry ?: e


    }

    fun add(e: LogEntry, ignoreMismatches: Boolean, data: ArrayList<ValueNamePair>): Holder {
        return add(e, ignoreMismatches, data, false)
    }

    /**
     * this function is called in add before anything is actually done
     * used for shadowrun to find specializations
     * @param text simply the text of the charProgressItem E.G. Acrobatics \[Jump\] 7
     * there will not come any @smth in this string
     */
    open fun getSpeializations(text: String): ArrayList<String> {
        val specializations = ArrayList<String>()
        val m = Pattern.compile("([^\\[\\@]+)\\s*(\\[([^\\[\\]]*)\\])?")
            .matcher(text)
        if (m.find()) {
            if (m.group(3) != null) {
                Helper.log(
                    CharProgressItemAdapterSkills.DEBUG_TAG,
                    "specializations: " + m.group(3)
                )
                specializations.addAll(Arrays.asList(*m.group(3).split("\\s*,\\s*").toTypedArray()))
            } else {
//                Helper.log(CharProgressItemAdapterSkills.DEBUG_TAG, "no specialization found")
            }
        }
        return specializations
    }


    /**
     * add a logEntry to the data.
     * If will be checked if there is already a valueNamePair with the name (text between filterConstraint and @Value)
     * and the value will be updated or a new value name pair will be created
     * @param e
     * @param ignoreMismatches if true, don't add those names to Data where there is no @Value
     * found in e.text or no valid value can be read.
     * @return holder index is -1 if new valueNamePair is created and -2 if no value is found
     */
    open fun add(
        e: LogEntry,
        ignoreMismatches: Boolean,
        data: ArrayList<ValueNamePair>,
        dontModifyData: Boolean
    ): Holder {

        fun log(msg: String) {
//            Log.d("CPIA_VALUE_BASED_ADD", "****$msg")
        }
//         log(DEBUG_TAG, "START +++++++++++++++++++++++++++++++++ adding Value based CHar Progress Item " + item?.header?.uppercase() + ". text = " + e.text);
        val result = Holder()
        result.logEntry = e
        var currentValue: Int
        var name: String //attribute name or skill name
        var description: String
        var textAfterFilterConstraintUntilNextFilterConstraint: String?
        var textAfterFilterConstraint = updateFilterConstraintToCurrentLanguage(e.text).also {
//            Log.d("VALUE_BASEDTRANS", "translated log text: '$it'")
        }
        var holderItem: HolderItem
        var expectedExpCost = 0
        var totalExpectedExpCost = 0
        if (dontModifyData) {
            result.collectedData = ArrayList()
        }
        /*
                maybe your exp doesn't match the usual pattern because you could by it cheaper e.g.
                than you can ignore this value and still check see if all the rest is correct.
                */
        val ignoreExpCheck = checkIgnoreExp(e.text) || ignoreExpErrors
        if (ignoreExpCheck) {
            /*
             here I assume that this means that you have not payed the expected cost so I add
             the given exp.
             This yields the wrong result if you have something like @Attribute @Skill
             */
            totalExpSpendForThisItem -= e.exp
        }
        var finalRound = false
        var onlyOneRound = false
        var counter = 0

        while (!finalRound) {
            counter++
            holderItem = HolderItem()
            textAfterFilterConstraint =
                getTextAfterStringAndTrim(textAfterFilterConstraint, item?.filterConstraint)
            textAfterFilterConstraintUntilNextFilterConstraint = textAfterFilterConstraint
            if (item != null && textAfterFilterConstraint.contains(item!!.filterConstraint)) {
                textAfterFilterConstraintUntilNextFilterConstraint =
                    textAfterFilterConstraint.substring(
                        0,
                        textAfterFilterConstraint.indexOf(item!!.filterConstraint)
                    )
            } else {
                finalRound = true
            }
            //remove all @Values
            textAfterFilterConstraintUntilNextFilterConstraint =
                Global.PATTERN_AT_VALUE.matcher(textAfterFilterConstraintUntilNextFilterConstraint)
                    .replaceAll("").trim()
            //cut everything after @ if there is some (e.g. other filter constraint)
            textAfterFilterConstraintUntilNextFilterConstraint =
                Global.PATTERN_AFTER_AT.matcher(textAfterFilterConstraintUntilNextFilterConstraint)
                    .replaceAll("").trim()

            val xvnp = getNameSpecializationDescriptionAndValue(
                text = textAfterFilterConstraintUntilNextFilterConstraint,
                item = item,
                context = context,
                ignoreValues = ignoreValues
            )
            val specializations = xvnp.specializations
            val relative = xvnp.relative
            currentValue = xvnp.value

            name = xvnp.name
            description = xvnp.description

            //big letters for Attributes
            if (item?.header.equals(
                    "%s".format(
                    context.getString(R.string.filter_constraint_attribute)
            ),
                    ignoreCase = true)) {
                name = name.uppercase()
            }

            //DEBUGGING
//            Helper.log(DEBUG_TAG, "text after filter constraint = '" + textAfterFilterConstraint + "'");
//            Helper.log(DEBUG_TAG, "text after filter constraint Until next filter constraint = '" + textAfterFilterConstraintUntilNextFilterConstraint + "'");
//            Helper.log(DEBUG_TAG, "value = " + currentValue);

            //                Helper.log(DEBUG_TAG, "name = '" + name + "'");
            //check if there is already such a skill/attribute and if this value is higher.
            var existsAlready = false
            var indexOfCurrentName = 0
            /*
            if there is already a value related to this name get its index in data
            */
            for (valueNamePair in data) {
                if (valueNamePair.name.equals(name, ignoreCase = true)) {
                    existsAlready = true
                    indexOfCurrentName = data.indexOf(valueNamePair)
                    holderItem.index_of_changed_pair = indexOfCurrentName
                }
            }

            var valueNamePair: ValueNamePair
            /*
            get valueNamePair related to name or create new one if none exists
             */
            if (!existsAlready) {
                valueNamePair = ValueNamePair(
                    name = name.trim(),
                    description = description,
                    mCalculator = mCalculator
                ).apply {
                    relatedLogEntry = e
                }
                holderItem.index_of_changed_pair = -1
                holderItem.old_value = 0
            } else {

                //valueNamePair exists already
                valueNamePair = data[indexOfCurrentName]
                log("exists already: ${valueNamePair.name} with value ${valueNamePair.value}, with specs: ${valueNamePair.specializations}")
                if (dontModifyData) {
                    valueNamePair = valueNamePair.clone()
                    log("dont modify data -> cloning vnp. clone = $valueNamePair, with specs: ${valueNamePair.specializations}")
                }
                holderItem.old_value = valueNamePair.value
            }


            var invalidValue = false
            if (currentValue == INVALID_VALUE_INDICATOR) {
                // no Value found

                /*
                thats somewhat dirty...
                 */
                if (!allowInvalidValues &&
                    specializations.isEmpty() &&
                    description.isBlank() &&
                    !textAfterFilterConstraintUntilNextFilterConstraint.contains(
                        CharProgressItemAdapterBuffs.BUFF_REF)
                ){
                    //also no specialization found
                    invalidValue = true
                    log("INvalid value AND no specs, no description and not buffs!:")
                    holderItem = dealWithValueBasedCharProgItemWhereNoValueIsFound(
                        e,
                        ignoreMismatches,
                        textAfterFilterConstraintUntilNextFilterConstraint
                    )
                } else {
                    currentValue = valueNamePair.value
                }
            }

            if (!invalidValue) {
                log("VALID value OR specs:")

                /*
            check if exp calc is correct:
             */
//                Helper.log(DEBUG_TAG, "has error BEFORE adding value = " + e.hasError);
                holderItem.old_value.let { oldValue ->

                    var currentAbsolutValue = currentValue
                    if (relative) currentAbsolutValue += oldValue

                    expectedExpCost = valueNamePair.mCalculator.getExpCost(
                        oldValue, currentAbsolutValue, valueNamePair.list_of_values
                    ) +
                            valueNamePair.mCalculator.getSpecializationCost(
                                oldSpecializations = valueNamePair.specializations,
                                newSpecialization = specializations
                            )

                }

                //                Helper.log(DEBUG_TAG, "expected exp cost = " + expectedExpCost);
                if (finalRound && counter == 1) {
                    /*
                only one filter constraint is in text
                 */
                    result.logEntry?.hasError = !valueNamePair.addValue(
                        new_value = currentValue,
                        relative = relative,
                        exp = -e.exp,
                        ignoreExpCheck = ignoreExpCheck,
                        new_specializations = specializations,
                        new_description = description,
                        ruleSystem = item?.character?.ruleSystem ?: prefs.ruleSystem
                    )
                    totalExpectedExpCost = expectedExpCost
                    totalExpSpendForThisItem -= e.exp
                    valueNamePair.totalExpSpent -= e.exp
                    onlyOneRound = true


                } else {
                    /*
                more than one filter constraints are in text
                 */
                    result.logEntry?.hasError = !valueNamePair.addValue(
                        new_value = currentValue,
                        relative = relative,
                        exp = expectedExpCost,
                        ignoreExpCheck = ignoreExpCheck,
                        new_specializations = specializations,
                        new_description = description,
                        ruleSystem = item?.character?.ruleSystem ?: prefs.ruleSystem
                    ) || (result.logEntry?.hasError ?: false)
                    totalExpectedExpCost += expectedExpCost
                    if (ignoreExpCheck) {
                        totalExpSpendForThisItem -= e.exp
                        valueNamePair.totalExpSpent -= e.exp
                    } else {
                        totalExpSpendForThisItem += expectedExpCost
                        valueNamePair.totalExpSpent += expectedExpCost
                    }
                }
                //                Helper.log(DEBUG_TAG, "has error AFTER adding value = " + e.getHasError());
                holderItem.pair = valueNamePair
                checkExpand(valueNamePair)
            }

            /*
            add holder to holderItems or valueNamePair to collectedData
             */
            if (dontModifyData) {
                log("dont nodify data -> add vnp to collected data")
                holderItem.pair?.let { result.collectedData?.add(it) }
            } else {
                log("add vnp to holder items")
                result.holderItems.add(holderItem)
            }

        }

        /*
            when there are more than on filter constraints it is impossible to tell which one had wrong
            exp calculated so we check in the end it the sum of all expected exp costs is the same as
            the log entries exp.
            It there is a error, all value name pairs in this log entries will be tagged as having error
             */
        if (!onlyOneRound && !ignoreExpCheck) {
            result.logEntry?.hasError = (result.logEntry?.hasError
                ?: false) || -totalExpectedExpCost != result.logEntry?.exp
            if (-totalExpectedExpCost != result.logEntry?.exp) {
                for (holderItem1 in result.holderItems) {
                    holderItem1.pair?.noCalcErrors = false
                }
            }
            //DEBUGGING
//            Helper.log(DEBUG_TAG, "has error: e = " + e.getHasError() + ", holder.logEntry  = " + result.logEntry.getHasError());
//            Helper.log(DEBUG_TAG, "total expected exp = " + totalExpectedExpCost);
//            Helper.log(DEBUG_TAG, "log entry exp = " + e.getExp());
//            Helper.log(DEBUG_TAG, "final has error  = " + e.getHasError() + ", " + result.logEntry.getHasError());
//            Helper.log(DEBUG_TAG, "END ++++++++++++++++++++++++++++++++++++++++++++++");
        }
        result.expectedExp = totalExpectedExpCost
        return result
    }

    open fun modifyValueDisplay(vnp: ValueNamePair): String {
        return vnp.value.toString()
    }

    open fun modifyTextViews(textViews: List<TextView>, valueNamePair: ValueNamePair) {
    }

    fun displaySpecsVertical(entry: View, valueNamePair: ValueNamePair) {
        entry.findViewById<LinearLayout>(R.id.mod_container).let { modContainer ->
            val sMap = HashMap<String, MutableList<Int>>()
            valueNamePair.specializations.forEach {s ->
                val specs = sMap[s.name] ?: mutableListOf<Int>().also { sMap[s.name] = it }
                specs.add(s.level)
            }
            val lMap = HashMap<Int, MutableList<String>>().apply {
                sMap.keys.forEach { k ->
                    sMap[k]?.max()?.let { mlvl ->
                        val sks = get(mlvl) ?: mutableListOf<String>().also { put(mlvl, it) }
                        sks.add(k)
                    }
                }
            }

            fun getSpecName(name: String): String {
                return "%s%s".format(
                    name ,
                    sMap[name]?.let {lvls ->
                        if (lvls.size > 1) {
                            " (${lvls.joinToString(separator = ",")})"
                        } else ""
                    } ?: "")
            }

            lMap.keys.forEach {l ->
                val specTv = LayoutInflater.from(mContext).inflate(
                    R.layout.list_view_item_char_progress_string,null, false) as TextView
                val lvlInd = if (lMap.keys.size > 1) "$l: " else ""
                val specs = "\t\t$lvlInd" + lMap[l]?.joinToString { getSpecName(it) }

                specTv.text = Helper.append(
                    sb = SpannableStringBuilder(),
                    what = StyleSpan(Typeface.ITALIC),
                    text = specs,
                    flags = SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                modContainer.addView(specTv)
            }
        }
    }

    open fun modifyNameDisplay(s :SpannableStringBuilder): SpannableStringBuilder {
        return s
    }

    override fun fillData(
        mData: ValueNamePair,
        check_mistakes: Boolean,
        holder: ViewHolder) {
        super.fillData(mData, check_mistakes, holder)

        //inflate entry
        val entry = holder.root
        val numberView = holder.dataViews?.get(0)
        val nameView = holder.nameView
        numberView?.text = modifyValueDisplay(mData)

        nameView?.let { nameV ->
            numberView?.let { numV ->
                modifyTextViews(listOf(nameV, numV), valueNamePair = mData)
            }
        }


        val verticalSpecs = item?.character?.showSpecsWithDescriptionOnlyExtended ?: false

        nameView?.text = modifyNameDisplay(
            getNameDisplay(
                valueNamePair = mData,
                verticalSpecs = verticalSpecs)
        )

        if (verticalSpecs)  {
            entry?.let { displaySpecsVertical(entry = it, valueNamePair = mData) }
        }

        implementExpand(view = entry, data = mData)


        if (check_mistakes) {
            if (!mData.noCalcErrors || mData.was_reduced_and_you_even_spend_exp_for_it) {
                entry?.setBackgroundResource(R.color.holo_red_light_half_transparent)
            }
            //            else {
//                entry?.setBackgroundResource(R.color.holo_green_light_half_transparent);
//            }
            if (mData.was_reduced) {
                var msg = "In " + item?.header + " " + mData.name + " was reduced"
                if (mData.was_reduced_and_you_even_spend_exp_for_it) {
                    msg += ". And you even spend exp for it! Probably order of logs in incorrect."
                }
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show()
            }
        }
//        log(DEBUG_TAG, "check miskates finalized = $check_mistakes")
        entry?.setOnLongClickListener {
            mActivity?.let {
                val searchString = item?.filterConstraintTranslations?.
                joinToString(" OR ") {
                    "\"$it ${mData.name}\""
                } ?: ""
                Log.d("CPI_VALUE_BASED", "searchString: $searchString")
                DialogShowFilteredLogs.newInstance(
                    searchString = searchString,
                    markErrors = check_mistakes,
                    activity_char_details = true
                )
                    .show(
                        it.supportFragmentManager,
                        ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                    )
            }
            true
        }
        entry?.setOnClickListener(
            DoubleClick(
                ListenerImplToastOnDoubleClickToggleExtendOnSingleClick(
                    activity = mActivity!!,
                    msg = String.format(Locale.GERMANY, "%,d", mData.totalExpSpent),
                    filterConstraintId = item?.filterConstraintId,
                    character = item?.character
                )
            )
        )
    }

    open fun buildValueWithAugmentation(valueNamePair: ValueNamePair): String {
        return String.format(
            Locale.GERMAN,
            AUG_VALUE_DISPLAY_TEMPLATE,
            valueNamePair.value,
            item?.character?.getTotalStatValue(valueNamePair.name)
        )
    }

    override fun inflate(expand: Boolean): View? {
        val v: View? = super.inflate(expand = expand)

        colTotalHeader = v?.findViewById(R.id.stat_1_label)
        colTotalHeader?.text = ""
        colBaseHeader = v?.findViewById(R.id.stat_0_label)
        colBaseHeader?.setText(R.string.score)
        colNameHeader = v?.findViewById(R.id.name_label)

        header_view?.setOnLongClickListener(OnLongClickListener {
            mActivity?.let { activity ->
                item?.let {
                    DialogShowFilteredLogs.newInstance(
                        it.filterConstraintTranslations.joinToString(" OR "),
                        it.check_mistakes
                    ).show(
                        activity.supportFragmentManager,
                        ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                    )
                }
            }
            return@OnLongClickListener true
        })
        header_view?.setOnClickListener(
            DoubleClick(
                ListenerImplToastOnDoubleClickToggleExtendOnSingleClick(
                    activity = mActivity!!,
                    msg = String.format(Locale.GERMANY, "%,d", totalExpSpendForThisItem),
                    filterConstraintId = item?.filterConstraintId,
                    character = item?.character
                )
            )
        )
        return v
    }

    private fun dealWithValueBasedCharProgItemWhereNoValueIsFound(
        e: LogEntry,
        ignoreMismatches: Boolean,
        textAfterFilterConstraintUntilNextFilterConstraint: String
    ): HolderItem {
        var valueNamePair = ValueNamePair("Error", 0, true, mCalculator = mCalculator)
        if (!ignoreMismatches) {
            valueNamePair = ValueNamePair(
                textAfterFilterConstraintUntilNextFilterConstraint,
                e.exp,
                e.hasError,
                mCalculator = mCalculator
            )
            //        Helper.log("LOG_ERROR", e.getText() + " ERROR = " + e.getHasError());
        }
        valueNamePair.relatedLogEntry = e
        return HolderItem(valueNamePair, NO_VALID_VALUE_OR_NO_AT_VALUE_IS_FOUND)
    }

    /**
     * Might return Null if no valueNamePair with fitting name exists for this item
     * @param name
     * @return
     */
    fun getValueNamePair(
        name: String,
        data: ArrayList<ValueNamePair>,
    ): ValueNamePair? {
        return data.find {
            name.equals(it.name, ignoreCase = true)
        }
    }

    /**
     *
     * @param text from this text it should be determined what exp to propose
     *
     * @return
     */
    override fun getExpProposal(text: String, nb_of_active_chars: Int, data: ArrayList<*>): Int {
        var proposal = 0
        if (nb_of_active_chars == 1 &&
            item?.let { text.matches(Regex(getRegexContainsFilterConstraint(it.filterConstraint).toString())) } == true &&
            !PrefsHelper(context).rulesE5
        ) {
            val e = LogEntry()
            e.text = text

            val holder: Holder = add(e, true, data as ArrayList<ValueNamePair>, true)
            proposal = -holder.expectedExp
        }
        return proposal
    }

    /**
     * this is only used if there is no @Value found in logText so there will be no old value
     *
     * @param e
     * @param pair
     * @param index_of_changed_pair
     */
    class Holder(
        var logEntry: LogEntry? = null,
        pair: ValueNamePair? = null,
        index_of_changed_pair: Int? = null
    ) {

        var expectedExp = 0

        /**
         * this is where data is stored when we dont want to modify to data we currently have
         * (data or getAuto_complete_data())
         * We use this to get exp proposal. There we want to access to available data,
         * but don't want to change it
         */
        var collectedData: ArrayList<ValueNamePair>? = null
        var holderItems = ArrayList<HolderItem>()

        init {
            index_of_changed_pair?.let { holderItems.add(HolderItem(pair, index_of_changed_pair)) }

        }


    }

    /**
     * this is only used if there is no @Value found in logText so there will be no old value
     *
     * @param pair
     * @param index_of_changed_pair
     */
    class HolderItem(
        /**
         * contains the new value of the name cut from LogEntry and the name itself
         */
        var pair: ValueNamePair? = null,
        /**
         * this is the index of the changed valueNamePair in data.
         * if index is negative it means that there is no pair with that name yet in the data
         */
        var index_of_changed_pair: Int = -10
    ) {


        /**
         * the value that was there before
         */
        var old_value: Int = -999
    }

    companion object {
        const val INVALID_VALUE_INDICATOR = -666

        class ExtractValueResult(var value: Int, var relative: Boolean)

        /**
         * +x or --x is considered to be relative value
         */
        fun extractValue(s: String, ignoreValues: Boolean = false): ExtractValueResult { //        Helper.log(DEBUG_TAG, "getting value:");
            val list = ArrayList<Int>()
            val m = Pattern.compile("(\\s+?|^)((-*\\+?)\\d+)(\\s+?|$)").matcher(s)
            var relative = false
            while (m.find()) {
                m.group(2)?.replace("\\s".toRegex(), "")?.let {
//                                log(DEBUG_TAG, "value as String = $it")
                    var intAsString = it
                    while (intAsString.startsWith("--")) {
                        intAsString = intAsString.drop(1)
                            }
                    intAsString
                }?.toIntOrNull()?.let {
                    list.add(it)
                }



                m.group(3)?.let {
                    /*
                    if number is of type +x pr --x assume it is meant relative
                     */
                    if ((it.startsWith("-") && it.length > 1) || it.startsWith("+")) {
                        relative = true
                    }

                }
            }

                return ExtractValueResult(
                    value = if (list.size == 1) {
                        list[0]
                    } else {
                        if (ignoreValues) {
                            1
                        } else {
                            INVALID_VALUE_INDICATOR
                        }
                    },
                    relative = relative
                )

        }

        fun getRelatedAttributesBuffedValueSum(
            attributes: List<String>?,
            character: Character?,
        ): Int? {
            return character?.let { c ->
                attributes?.let { relAttrs ->
                    relAttrs.sumOf { relAttr ->
                        getAttributeMod(
                            ruleSystem = c.ruleSystems?.firstOrNull(),
                            statValue = c.getTotalStatValue(
                                relAttr
                            )
                        )
                    }
                }
            }
        }
        fun getRelatedAttributesCheckBuffSum(
            attributes: List<String>?,
            character: Character?,
        ): Int? {
            return character?.let { c ->
                attributes?.let { attrs ->
                    attrs.sumOf { attr ->
                        c.getStatChecksBuffValueOrZero(attr)
                    }
                }
            }
        }


        fun fillBaseValue(valueNamePair: ValueNamePair, character: Character?): String {
            var base = 0
            var total = 0
            character?.let { c ->
                base = c.getStatValueOrZero(valueNamePair.name)
                total = c.getTotalStatValue(valueNamePair.name)
            }
            var s = base.toString()
            if (total != base) {
                s += " (%d)".format(total)
            }
            return s
        }

        fun getStealthMod(valueNamePair: ValueNamePair, context: Context?, character: Character?): Int {
            return context?.resources?.getString(
                character?.ruleSystem?.stealthSkillResourceId
                    ?: R.string.stealth_skill_name_default
            ).let { stealthSk ->
                if (valueNamePair.name.equals(stealthSk, ignoreCase = true)) {
                    character?.sizeStealthMod ?: 0
                } else 0
            }
        }

        // todo move this to rulesystem?
        fun getAmorPenalty(vnp: ValueNamePair, context: Context?, character: Character?): Int {
              fun log(msg: String) {
//                  Log.d("ARMOR_PENALTY", msg)
              }

            return when (character?.ruleSystem ?: PrefsHelper(context).ruleSystem) {
                RuleSystem.rule_system_ogres_club -> {
                    val affectedAttrs = listOf(
                        R.string.attribute_dexterity,
                        R.string.attribute_strength
                    ).mapNotNull {
                        context?.getString(it)
                    }.toSet()

                    vnp.skill?.let { sk ->
                        if (vnp.skill?.attributes?.split(" ")?.
                            intersect(affectedAttrs)?.isNotEmpty() == true
                        ) {
                            character?.armorCheckPenalty ?: 0
                        } else {
                            0
                        }
                    } ?: 0
                }
                RuleSystem.rule_system_splittermond -> {
                    val attrBew = context?.getString(R.string.splittermond_attribute_agility)

                    vnp.skill?.let { sk ->
                        val isGeneralSkill = sk.type == context?.resources?.getString(R.string.splittermond_skill_category_general)

                            if (isGeneralSkill) {
                                vnp.skill?.attributes?.split(" ")?.
                                find { it.trim().equals(attrBew, ignoreCase = true) }?.
                                let {
                                    character?.armorCheckPenalty
                                } ?: run {
                                    0
                                }
                            } else {
                                // not general skill
                                0
                            }

                    } ?: 0

                }
                else -> 0
            }
        }

        fun calcTotalSkillValue(
            vnp: ValueNamePair,
            character: Character?,
            context: Context?
        ): CalcTotalValueResult {
            character?.let { c ->


                var total = c.getTotalStatValue(vnp.name)

                val base = c.getStatValueOrZero(vnp.name)

                c.ruleSystem?.let {
                    total = it.getmodifiedSkillValue(c, vnp, base, total, context)
                }



                //mark skills where no attribute is found
                var foundRelatedAttribute = false

                // add related attribute value
                getRelatedAttributesBuffedValueSum(
                    attributes = vnp.skill?.attributesList,
                    character = c,
                )?.let { attr ->
                    foundRelatedAttribute = true
                    total += attr
                }
                getRelatedAttributesCheckBuffSum(
                    attributes = vnp.skill?.attributesList,
                    character = c,
                )?.let { attrChecks ->
                    total += attrChecks
                }
                total += c.getStatBuffValueOrZero(BuffEffect.BUFF_REF_ALL_CHECKS)
                total -= c.damageMalus
                total -= c.concentrationMalus
                total += getStealthMod(
                    valueNamePair = vnp,
                    context = context,
                    character = c
                )
                total -= getAmorPenalty(
                    vnp = vnp,
                    context = context,
                    character = c)

                return CalcTotalValueResult(
                    totalValue = total,
                    foundRelatedAttribute = foundRelatedAttribute
                )
            }
            return CalcTotalValueResult(totalValue = -999, foundRelatedAttribute = false)
        }

        data class CalcTotalValueResult(val totalValue: Int, val foundRelatedAttribute: Boolean)

        const val AUG_VALUE_DISPLAY_TEMPLATE = "%d (%d)"




        fun getNameSpecializationDescriptionAndValue(
            text: String,
            item: CharProgressItem?,
            context: Context,
            ignoreValues: Boolean = false
        ): ValueNamePair {

            fun log(msg: String) {
//                Log.d("GET_NAME_SPECS_VALUE", "~~~$msg")
            }

            /**
           return either the number at the end of s or the number of [] that where used to surround it
             */

            fun getSpecNameLevelAndDescription(s: String, bracketLevel: Int): Specialization {
                var result = Specialization("")
                val m = Global.PATTERN_SPECIALIZATION.matcher(s)

                if (m.find()) {
                    m.group(1)?.let { result = Specialization(name = it.trim()) }

                    result.level = m.group(2)?.toIntOrNull() ?: bracketLevel

                    m.group(5)?.let { result.description = it }
                }
                return result
            }

            fun dealWithSingleSpec(specStr: String, bracketLevel: Int, specializations: MutableList<Specialization>) {
                val spec = getSpecNameLevelAndDescription(s = specStr, bracketLevel = bracketLevel)
                log("add spec: $spec (parsed from $specStr)")

                specializations.find { it.name.equals(spec.name, ignoreCase = true) }
                    ?.let {
                        it.level = max(spec.level, it.level)
                        log("new level: ${it.level}")
                        it.description = it.description.ifBlank { spec.description }
                    } ?: run {
                    specializations.add(spec)
                    log("new spec added: $spec")
                }  ?: run {
                    log("no specialization found")
                }
            }

            fun hasSpecialization(s: String): Boolean {
                val iComma = s.indexOf(",")
                val iBracket = s.indexOf("[")
                return iBracket >= 0 && (iComma >= 0 && iBracket < iComma || iComma == -1)
            }

//            Exception().printStackTrace()
            //get skill name and maybe specializations and Value
            val fullString: String =
                item?.filterConstraint?.let { text.substringAfter(it) } ?: text
            var name_with_spec_and_expert = fullString
            if (hasSpecialization(name_with_spec_and_expert)) {
                var ind = name_with_spec_and_expert.indexOf("]")
                if (ind > -1) {

                    while (ind < name_with_spec_and_expert.length
                        && name_with_spec_and_expert[ind].toString() == "]"
                    ) {
                        ind++
                    }
                    name_with_spec_and_expert = name_with_spec_and_expert.substring(0, ind)

                }

            } else {
                name_with_spec_and_expert = name_with_spec_and_expert.substringBefore(",")
            }
            log("get name with specializations and expertises from: $name_with_spec_and_expert")
            var name = ""
            val description = StringUtils.parseCPITextStat(
                text = fullString,
                refId = R.string.description_label,
                context = context)
            log("got description: $description")
            val value = extractValue(name_with_spec_and_expert.substringBefore("["), ignoreValues = ignoreValues)
            log("got value = ${value.value}, relative = ${value.relative}")
            val specializations = ArrayList<Specialization>()
            val m = Global.PATTERN_SKILL
                .matcher(name_with_spec_and_expert)
            if (m.find()) {
                m.group(1)?.let { name = it.trim() }
                log("got name: $name")
                m.group(4)?.also { g ->
                    log( "specializations in regex: " + m.group(2))
                    val bracketLevel = min(m.group(3)?.length ?: 0, m.group(5)?.length?: 0).also { log("bracket level = $it") }
                    if (g.contains("//")) {
                        /*
                        the specialization has a description so we assume, there is only one
                        if there is a description and multiple, we run into problems
                        because the description can contain commata and it is also used
                        as separator/delimiter
                         */
                        dealWithSingleSpec(
                            specStr = g,
                            bracketLevel = bracketLevel,
                            specializations = specializations
                        )
                    } else {
                        g.split(",")
                            .map { it.trim() }
                            .forEach { specStr ->
                                dealWithSingleSpec(
                                    specStr = specStr,
                                    bracketLevel = bracketLevel,
                                    specializations = specializations
                                )
                            }
                    }
                } ?: run {
                    log("no specialization found")
                }
            }
            return ValueNamePair(
                name = name,
                value = value.value,
                description = description,
                mCalculator = object : Calculator {})
                .apply {
                    this.specializations = specializations
                    this.relative = value.relative
                }
        }

        fun getNameDisplay(valueNamePair: ValueNamePair, verticalSpecs: Boolean = false): SpannableStringBuilder {
            var text_display = SpannableStringBuilder(capitalizeNewsLetterStyle( valueNamePair.name))
//            Helper.log(
//                "BUILDING_SKILLS",
//                "found skill: " + valueNamePair.name + " with speci: " + valueNamePair.specializations.toString() + "name newsletterStyle: " + capitalizeNewsLetterStyle( valueNamePair.name)
//            )
            var specsToPrint: List<Specialization> = valueNamePair.specializations
            if (verticalSpecs) {
                specsToPrint = specsToPrint.filter { it.description.isBlank() }
            }
            if (specsToPrint.isNotEmpty()) {

                text_display = Helper.append(
                    sb = text_display,
                    what = StyleSpan(Typeface.ITALIC),
                    text = printSpecializations(specsToPrint),
                    flags = SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE)

            }

            return text_display
        }

        fun printSpecializations(specs: List<Specialization>): String {

            specs.distinctBy { it.level }.singleOrNull()?.level?.let { l ->
                if (l < 3) {
                    return " " + specs.sortedBy { it.name }
                        .joinToString(
                            prefix = "[".repeat(l),
                            postfix = "]".repeat(l)) { capitalizeNewsLetterStyle(it.name) }
                }
            }

            return " [" + specs.sortedWith(
                compareBy<Specialization> { it.level }.thenBy { it.name }
            ).joinToString {
                capitalizeNewsLetterStyle(it.name) + if (it.level > 1) " ${it.level}" else ""
            } + "]"
        }
    }
}