package com.mtths.chartracker.charprogressitemadapters


import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterGear.Companion.addRatingAndCapacityAutoCompleteStrings
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.StringUtils.getFloatOrIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getWareType
import java.util.*

class CharProgressItemAdapterWare(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapter<CharProgressItemAdapterWare.Ware>(
        mActivity = activity,
        comparator = ware_name_comparator
    ),
    CharProgressItemAdapterBuffable {

    val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values
    override val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header


    override fun createAutoCompleteStrings(): ArrayList<String> {
        return arrayListOf(
                "essence =", "type ="
            ).also {
            addRatingAndCapacityAutoCompleteStrings(it)
            Ware().addModsAutoCompleteString(it)
            addBuffAutoCompleteString(it)
        }
    }

    override fun generate_autocomplete_data(e: LogEntry) {

    }



    override fun add(e: LogEntry): LogEntry {


        //first entry is karma, second is text
        val ware = Ware()
        ware.relatedLogEntry = e

        ware.parseName(e.text, item)
        ware.parseEssence(e.text)

        ware.type = getWareType(e.text)
        /*
        set rating
         */
        ware.parseRating(e.text)
        ware.parseCapacity(e.text)
        ware.parseMods(e.text)
        createBuffs(s = e.text, iData = ware, item = item, context = context)
        data.add(ware)
        checkExpand(ware)

        return e
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        val ware: Ware = data[index]

        //inflate entry
        val ware_view: View =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false)
        val name_display = ware_view.findViewById<TextView>(R.id.text)
        val rating_display = ware_view.findViewById<TextView>(R.id.number)
        val essence_display = ware_view.findViewById<TextView>(R.id.number2)
        name_display.text = ware.getNameDisplay()
        rating_display.text = ware.getRatingDisplay()
        essence_display.text = ware.essence
        mActivity?.let { a ->
            ware_view.setOnLongClickListener(
                ListenerImplOpenEditLogsOnLongClick(
                    a.supportFragmentManager,
                    ware.relatedLogEntry
                )
            )
        }
        implementExpand(view = ware_view, data = ware)
        return ware_view
    }

    override fun inflate(expand: Boolean): View? {
        val container = super.inflate(expand)
        renameStat1ColumnHeader("Essence", container)
        return container
    }

    inner class Ware : CharProgressItemAdapterGear.Gear() {
        override val mods: MutableList<String> = mutableListOf()
        var essence: String? = null
        var type: String? = null

        /**
         * parse type first
         */
        fun parseEssence(s: String) {

            val essence_as_string = getFloatOrIntegerStatFromText(
                "essence", s
            ).trim()
            //            Helper.log("ESSENCE",e.text + "\n essence as string read = " + essence_as_string);

            if (!essence_as_string.equals(
                    "-",
                    ignoreCase = true
                ) && essence_as_string.matches("-?\\d*\\.?\\d*".toRegex())) {
                val essence = essence_as_string.toFloat()
                //                Helper.log("ESSENCE","essence as float = " + essence);

                item?.character?.let { c ->
                    when {
                        type.equals("bio", ignoreCase = true) -> {
                            if (essence >= 0) {
                                c.essence_bio += essence
                            } else {
                                c.essence_bio -= essence
                            }
                        }

                        type.equals("cyber", ignoreCase = true) -> {
                            if (essence >= 0) {
                                c.essence_cyber += essence
                            } else {
                                c.essence_cyber -= essence
                            }
                        }
                        else -> {
                            if (essence >= 0) {
                                c.essence_cyber += essence
                            } else {
                                c.essence_cyber -= essence
                            }
                        }
                    }
                }

                this.essence = essence_as_string.replace("\\.0+".toRegex(), "")



            }
        }
    }

    companion object {
        private val ware_name_comparator =
            Comparator<Ware> { o1, o2 -> o1.name.compareTo(o2.name, ignoreCase = true) }
    }
}