package com.mtths.chartracker.charprogressitemadapters

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogCustom.Companion.showNumberPickerToChangeAmount
import com.mtths.chartracker.interfaces.Moddable
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getValue
import kotlin.collections.ArrayList

open class CharProgressItemAdapterGear(
    activity: ProgressBarFrameDropBoxActivity?,
    val onlyRating: Boolean = false) :
    CharProgressItemAdapter<CharProgressItemAdapterGear.Gear>(
        mActivity = activity,
        comparator = compareBy { it.name }
    ),
    CharProgressItemAdapterBuffable {
    val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values
    override  val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header


    override fun createAutoCompleteStrings(): ArrayList<String> {
        return super.createAutoCompleteStrings().also {
            addRatingAutoCompleteString(it)
            Gear().addModsAutoCompleteString(it)

            if (!onlyRating) {
                addCapacityAutocompleteStrings(it)
                addBuffAutoCompleteString(it)
                addAmountAutocompleteStrings(it)
            }
        }
    }

    override fun generate_autocomplete_data(e: LogEntry) {

    }

    override fun add(e: LogEntry): LogEntry {

        //first entry is karma, second is text
        val gear = Gear()
        gear.relatedLogEntry = e

        /*
        set name
         */
        item?.filterConstraint?.let { gear.parseName(e.text, item) }
        gear.parseRating(e.text)
        gear.parseCapacity(e.text)
        gear.parseAmount(e.text)

        data.add(gear)
        gear.parseMods(e.text)
        createBuffs(s = e.text, iData = gear, item = item, context = context)
        checkExpand(gear)
        return e
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        val gear: Gear = data[index]

        //inflate entry
        val gear_view: View =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false)
        val name_display = gear_view.findViewById<TextView>(R.id.text)
        val rating_display = gear_view.findViewById<TextView>(R.id.number)
        val amount_display = gear_view.findViewById<TextView>(R.id.number2)
        name_display.text = gear.getNameDisplay()
        rating_display.text = gear.getRatingDisplay()
        amount_display.text = gear.amount.toString()

        amount_display.setOnLongClickListener {
            showNumberPickerToChangeAmount(
                context = context,
                getOldAmount = {gear.amount},
                setNewAmount = {gear.amount = it},
                onAmountChanged = {updateAllDisplays()},
                logEntryWithAmountString = gear.relatedLogEntry,
                amountRef = AMOUNT_REF,
            )
            true
        }

        mActivity?.let { a ->
            gear_view.setOnLongClickListener(
                ListenerImplOpenEditLogsOnLongClick(
                    a.supportFragmentManager,
                    gear.relatedLogEntry
                )
            )
        }
        implementExpand(view = gear_view, data = gear)
        return gear_view
    }

    override fun inflate(expand: Boolean): View? {
        val container = super.inflate(expand)

        renameStatHeaders(
            nameIds = mutableListOf(
                R.string.rating_label,
            ).apply {
                    if(!onlyRating) {
                        add(R.string.amount_label)
                    }
            },
            containerView = container
        )
        return container
    }


    open class Gear : CharProgressItemData(), Moddable {
        override val mods: MutableList<String> = mutableListOf()
        var rating: Int? = null
        var capacity: Int? = null
        var amount: Int = 1
        override val _REF_MOD: String? = null // means is used DEFAULT REF_MOD
        val usedCapacity: Int
            get() = mods.sumOf { getModCapacityValue(it) }

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                addAll(modsExpandData)
            }

        fun getModCapacityValue(mod: String): Int {
            return getValue(s = mod, begin = Regex("\\("), end = Regex("\\)")) ?: getValue(s = mod)
            ?: 0
        }

        fun parseCapacity(s: String) {
            capacity = getIntegerStatFromText(CAPACITY_REF, s).toIntOrNull()
        }

        fun parseAmount(s: String) {
            amount = getIntegerStatFromText(AMOUNT_REF, s).toIntOrNull() ?: 1
        }

        fun parseRating(s: String) {
            rating = getIntegerStatFromText(RATING_REF, s).toIntOrNull()

        }

        fun getNameDisplay(): String {
            capacity?.let {
                return if (usedCapacity > 0) {
                    "%s [%d/%d]".format(name, usedCapacity, capacity)
                } else {
                    "%s [%d]".format(name, capacity)
                }
            }
            return "%s".format(name)
        }

        fun getRatingDisplay(): String {
            return rating?.toString() ?: "-"
        }
    }

    companion object {
        const val DEBUG_TAG = "CHAR_PROG_ADAPTER_GEAR"
        const val INVALID_INTEGER_INDICATOR = -999
        const val RATING_REF = "rating"
        const val CAPACITY_REF = "capacity"
        const val AMOUNT_REF = "amount"

        fun addRatingAndCapacityAutoCompleteStrings(list: ArrayList<String>) {
            addRatingAutoCompleteString(list)
            list.add("$CAPACITY_REF = ")
            list.add("$RATING_REF = ")

        }

        fun addRatingAutoCompleteString(list: ArrayList<String>) {
            list.add("$RATING_REF = ")
        }

        fun addCapacityAutocompleteStrings(list: ArrayList<String>) {
            list.add("$CAPACITY_REF = ")
        }

        fun addAmountAutocompleteStrings(list: ArrayList<String>) {
            list.add("$AMOUNT_REF = ")

        }
    }
}
