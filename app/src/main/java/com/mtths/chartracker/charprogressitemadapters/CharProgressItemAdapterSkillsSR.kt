package com.mtths.chartracker.charprogressitemadapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.mtths.chartracker.*
import com.mtths.chartracker.Global.FEATURED_MARKER
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSkills.Companion.fillTotalValue
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSkills.Companion.showSkillDetails
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterValueBased.Companion.fillBaseValue
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterValueBased.Companion.getNameSpecializationDescriptionAndValue
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterValueBased.Companion.printSpecializations
import com.mtths.chartracker.dataclasses.*
import com.mtths.chartracker.utils.Helper.cutAfterString
import com.mtths.chartracker.utils.Helper.getSkillGroupName
import com.mtths.chartracker.utils.Helper.getValueNamePairSkill
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.TranslationHelper
import java.util.*
import kotlin.collections.HashMap

class CharProgressItemAdapterSkillsSR (
    activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<ValueNamePairSkillSR> = compareBy { it.name }
) : CharProgressItemAdapter<ValueNamePairSkillSR>(activity, comparator) {
    var data_copy: ArrayList<ValueNamePairSkillSR> = ArrayList()
    val skillData: HashMap<RuleSystem, HashMap<String, Skill>> by lazy { Skill.getSkillData(context = context) }


    var mCalculator: Calculator = if (rulesSR5) {
        object : Calculator {
        }
    } else {
        // SR6
        object: Calculator {
            override fun getSpecializationCost(oldSpecializations: List<Specialization>?, newSpecialization: List<Specialization>?): Int {
                val oSpec = oldSpecializations?.map { it.name.lowercase() }
                val nSpec = newSpecialization?.map { it.name.lowercase() }
                var sDiff = nSpec
                oSpec?.forEach { sDiff = sDiff?.minus(it) }
//                Helper.log("SPECIALIZATION_COST", "new specs: $nSpec old specs: " + oSpec)
//                Helper.log("SPECIALIZATION_COST", ("diff: $sDiff"))
                val nExp = newSpecialization?.filter { it.expertise }?.map { it.name.lowercase() }
                val oExp = oldSpecializations?.filter { it.expertise }?.map { it.name.lowercase() }
                var eDiff = nExp
                oExp?.forEach { eDiff = eDiff?.minus(it) }
                val specCost = 5 * (sDiff?.size ?: 0)
                val expertCost = 5 * (eDiff?.size ?: 0)
//                Helper.log("SPECIALIZATION_COST", "new expt: $nExp old expt: " + oExp)
//                Helper.log("SPECIALIZATION_COST", ("diff: $eDiff"))

//                log("SPECIALIZATION_COST", "spec cost: $specCost, expert cost: $expertCost")
                return specCost + expertCost
            }

            override fun getExpCost(old_value: Int, new_value: Int): Int {
                var result = 0
                for (i in old_value + 1..new_value) {
                    result += i * 5
                }
                return result
            }
        }
    }

    override val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header



    interface Calculator {

        fun getExpCost(old_value: Int, new_value: Int): Int {
            var result = 0
            for (i in old_value + 1..new_value) {
                result += i * 2
            }
            return result
        }

        fun calcExpWithSkillGroups(
            data_containing_old_values: ArrayList<ValueNamePairSkillSR>,
            new_value: Int,
            skill: String
        ): Int {
            return data_containing_old_values.
            find { it.name.equals(skill, ignoreCase = true) }?.
            let {
                getExpCost(new_value, it.value)
            } ?: 0

        }

        fun getSpecializationCost(oldSpecializations: List<Specialization>?, newSpecialization: List<Specialization>?): Int {
            return 7 * (newSpecialization?.minus(oldSpecializations?.toSet())?.size ?: 0)
        }
    }

    override fun createAutoCompleteStrings(): ArrayList<String> {
//        Helper.log("AUTO_COMPLETE", "create autocomplete strings for item '${item?.header}': stdAutoCompleteList = $stdAutoCompleteData")
        var l = ArrayList<String>()
        PrefsHelper(context).activeRuleSystems.forEach{ ruleSystem ->
            stdAutoCompleteData[ruleSystem]?.let { l.addAll(it) }
        }
        val auto_complete_strings = l.distinct().toMutableList()
        for (pair in auto_complete_data) {
            if (auto_complete_strings.find { it.equals(pair.name, ignoreCase = true) } == null) {
                auto_complete_strings.add(pair.name)
            }
        }
        return auto_complete_strings.toCollection(ArrayList())
    }

    override fun generate_autocomplete_data(e: LogEntry) {
        val holder = add(e, false, auto_complete_data)
        holder.holderItems.forEach { h ->
            if (h.index_of_changed_pair < 0) h.pair?.let { auto_complete_data.add(it) }
        }

    }

    /**
     * add a logEntry to the data.
     * If will be checked if there is already a valueNamePair with the name (text between filterConstraint and @Value)
     * and the value will be updated or a new value name pair will be created
     * @param e
     * @return the LogEntry, but e.hasError is set now
     */
    override fun add(e: LogEntry): LogEntry {
        val h = add(e, true, data)
        h.holderItems.forEach {holder ->
            if (holder.index_of_changed_pair < 0) holder.pair?.let { data.add(it) }
        }

        return h.logEntry ?: LogEntry()
    }

    fun add(e: LogEntry,
            ignoreMismatches: Boolean,
            data: ArrayList<ValueNamePairSkillSR>,
            dontModifyData: Boolean = false): Holder {

        val logging = true

        fun handleSingleSkillTask(e: LogEntry, ignoreMismatches: Boolean, data: ArrayList<ValueNamePairSkillSR>, dontModifyData: Boolean = false): HolderItem {
            val holder = HolderItem()
//            log(
//                DEBUG_TAG,
//                "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ handle single skill task called. " + e.text + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~",
//            )

//            Helper.log("VALUE_BASED_ADD", "filterConstraint is met. Entry will be added to " + item.header);

            //get skill name and maybe specializations and Value
            val textAfterFilterConstraint = cutAfterString(
                updateFilterConstraintToCurrentLanguage(e.text),
                item?.filterConstraint).trim()
            val xvnp = getNameSpecializationDescriptionAndValue(
                text = textAfterFilterConstraint, item = item, context = context)
            val name = xvnp.name
            var valueOfCurrentLogEntry = xvnp.value
            val specializations = xvnp.specializations
//            log(DEBUG_TAG, "name: $name; value: $valueOfCurrentLogEntry; specializations: $specializations", logging = logging)


            //check if there is already such a skill/attribute and if this value is higher.
            var existsAlready = false
            var indexOfCurrentName = 0

            /*
            if there is already a value related to this name get its index in data
            */for (valueNamePair in data) {
                if (valueNamePair.name.equals(name.trim(), ignoreCase = true)) {
                    existsAlready = true
                    indexOfCurrentName = data.indexOf(valueNamePair)
                    holder.index_of_changed_pair = indexOfCurrentName
//                    log(DEBUG_TAG, "already exists: index = $indexOfCurrentName", logging = logging)
                }
            }
            val valueNamePair: ValueNamePairSkillSR

            /*
            get valueNamePair related to name or create new one if none exists
            */
            if (!existsAlready) {

//                log(DEBUG_TAG, "creating new Value Name Pair", logging = logging)
                valueNamePair = ValueNamePairSkillSR(
                    name = name.trim(),
                    mCalculator = mCalculator
                )
                holder.index_of_changed_pair = -1
                holder.old_value = 0
            } else {

                //valueNamePair exists already
                valueNamePair = data[indexOfCurrentName]
                holder.old_value = valueNamePair.value
            }
            holder.pair = valueNamePair


            /*
            maybe your karma doesn't match the usual pattern because you could by it cheaper e.g.
            than you can ignore this value and still check see if all the rest is correct.
            */
            val ignoreExpCheck: Boolean =
                e.text.lowercase(Locale.getDefault()).contains(Global.IGNORE_EXP_CHECK_INDICATOR)


            ///////////////////////////// BUILD CASES ///////////////////////////////

            if (valueOfCurrentLogEntry == CharProgressItemAdapterValueBased.INVALID_VALUE_INDICATOR) {
                // no Value found
                if (specializations.isEmpty()) {
                    //also no specialization found
                    return dealWithValueBasedCharProgItemWhereNoValueIsFound(e, ignoreExpCheck, item)
                } else {
                    valueOfCurrentLogEntry = valueNamePair.value
                }
            }
            holder.hasError = !valueNamePair.addValue(
                valueOfCurrentLogEntry,
                -e.exp,
                ignoreExpCheck,
                data,
                specializations,
                rulesSR5
            )


//            Helper.log(DEBUG_TAG, "no error way gone :)))))))", logging = logging)

            /*
            now we have made sure that we have either specializations or a value
             */


            ////////////////////////////// FEATURED SKILL POINTS STUFF /////////////////////////////////
            if (e.text.lowercase()
                    .contains(FEATURED_MARKER) && holder.index_of_changed_pair >= -1
            ) {
//                Helper.log(
//                    DEBUG_TAG,
//                    ".feature found. holder = value " + holder.pair?.value + ", name " + holder.pair?.name.toString() + ", index " + holder.index_of_changed_pair,
//                    logging = logging
//                )
                item?.character?.let { c ->
                    var amount_of_skill_points_added = -1
                    if (holder.index_of_changed_pair >= -1) {
                        /*
                    this means that the logText contained @Value
                     */
                        amount_of_skill_points_added = holder.pair!!.value -  //new value
                                holder.old_value //old value
                    }
//                    Helper.log(
//                        DEBUG_TAG,
//                        "amount of skill points to add: $amount_of_skill_points_added",
//                        logging = logging
//                    )
                    if (amount_of_skill_points_added > 0) {
                        c.getPrefs(context).featured_skill_points_used += amount_of_skill_points_added
                    }
                }
            }


            return holder
        }

        /*
        return trivial result if not filter constraint is known
         */
        val holder = Holder(e)
//        log(DEBUG_TAG, "dealing with log entry: //////////////////////// ${e.text} ////////////////////////", logging = logging)
        item?.filterConstraint?.let { filterConstraint ->
            var list = e.text.replace(Global.REGEX_MARKER, "").split(filterConstraint)
            val first = list.first()
            val prefix = first + Helper.getMarkers(e.text)
//            log(DEBUG_TAG, "PREFIXES: $prefix", logging = logging)
            list = list.drop(1)
            list.filter { it.isNotBlank() }
                .forEach { skillTask ->
                    val h = handleSingleSkillTask(
                        e = e.copy(text = "$prefix $filterConstraint $skillTask"),
                        ignoreMismatches = ignoreMismatches,
                        data = data,
                        dontModifyData = dontModifyData)
                    holder.holderItems.add(h)
                    h.pair?.let { holder.collectedData?.add(it) }
                    e.hasError = e.hasError || h.hasError
                }
        }


        holder.holderItems.forEach { h ->
            h.pair?.let { p ->

                //set skill if none
                if (p.skill == null) {
                    p.skill = Skill.getSkillWAttrCastStatFallback(
                        name = p.name,
                        skillData = skillData,
                        character = item?.character
                    )
                }

                /*
                store attributes at character
                */
                item?.character?.let { c ->
                    c.setStat(p.name, p.value)
                }
            }
        }



        return holder
    }

    override fun getView(index: Int, check_mistakes: Boolean): View {
        val valueNamePair: ValueNamePairSkillSR = data.get(index)
//        Helper.log("BUILDING_SKILLS", "-------------- called getView: skill: " + valueNamePair.name)

        //inflate entry
        val entry = LayoutInflater.from(mContext).inflate(
            R.layout.char_progress_list_view_item_2_values,
            null,
            false
        ) as LinearLayout
        val left_text = entry.findViewById<TextView>(R.id.number)
        val right_text = entry.findViewById<TextView>(R.id.text)

        //display how skill value ist calculated
        val vnp = ValueNamePair(
            name = valueNamePair.name,
            value = valueNamePair.value,
            mCalculator = object : CharProgressItemAdapterValueBased.Calculator {})
        entry.setOnClickListener {
            showSkillDetails(
                view = entry,
                valueNamePair = vnp,
                character = item?.character,
                context = context)

        }

        left_text.text = fillBaseValue(valueNamePair = vnp, character = item?.character)
        fillTotalValue(
            item = item,
            view = entry,
            valueNamePair = vnp,
            context = context)

        var text_display: String = java.lang.String.valueOf(valueNamePair.name)
        if (valueNamePair.specializations.size > 0) {
//            Helper.log(
//                "BUILDING_SKILLS",
//                "found skill: " + valueNamePair.name + " with speci: " + valueNamePair.specializations.toString()
//            )
            text_display += printSpecializations(valueNamePair.specializations)
        } else {
//            Helper.log(
//                "BUILDING_SKILLS",
//                "found skill: " + valueNamePair.name + " NO SPECIALIZATION"
//            )
        }
        right_text.text = text_display
        if (check_mistakes) {
            if (!valueNamePair.noCalcErrors || valueNamePair.was_reduced_and_you_even_spend_exp_for_it) {
                entry.setBackgroundResource(R.color.holo_red_light_half_transparent)
            }
            //            else {
//                entry.setBackgroundResource(R.color.holo_green_light_half_transparent);
//            }
            if (valueNamePair.was_reduced) {
                var msg =
                    "In " + item?.header.toString() + " " + valueNamePair.name + " was reduced"
                if (valueNamePair.was_reduced_and_you_even_spend_exp_for_it) {
                    msg += ". And you even spend karma for it! Probably order of logs in incorrect."
                }
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show()
            }
        }
        val skill_group_name: String = getSkillGroupName(valueNamePair.name)
        if (!skill_group_name.equals("", ignoreCase = true)) {
            val skill_group_stuff = TranslationHelper(context).getAllTranslations(
                R.string.filter_constraint_skill_group).joinToString(" OR ") {
                    "\"$it $skill_group_name\"" }
            entry.setOnLongClickListener {
                val searchString = (item?.filterConstraintTranslations?.
                joinToString(" OR ") {
                    "\"$it ${valueNamePair.name}\""
                }
                        + skill_group_stuff
                        )
                DialogShowFilteredLogs.newInstance(
                    searchString,
                    check_mistakes
                ).show(
                    mActivity!!.supportFragmentManager,
                    ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                )
                true
            }
        } else {
            entry.setOnLongClickListener {
                val searchString = (item?.filterConstraintTranslations?.joinToString(" OR ") { "\"$it ${valueNamePair.name}\"" } ?: "")
                DialogShowFilteredLogs.newInstance(
                    searchString,
                    check_mistakes
                ).show(
                    mActivity!!.supportFragmentManager,
                    ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                )
                true
            }
        }
        return entry
    }

    private fun calcExpCost(
        new_value: Int,
        skill_name: String,
        data: ArrayList<ValueNamePairSkillSR>
    ): Int {
        val DEBUG_TAG_CALC_EXP_GROUP_SKILL = "CALC_EXP_GROUP_SKILL"
//        Helper.log(
//            DEBUG_TAG_CALC_EXP_GROUP_SKILL,
//            "*********************************** $skill_name, new value = $new_value"
//        )
        var karma = 0
        Helper.getSkillGroup(skill_name)?.also { sg ->
            var inverse_factor = 1
            var nullifying_factor = 1
            var next_step = 0
            var current_value = 0
            var index = -1

            /*
                    sort skills by the value;
                    */
            val sg_skills_valued: ArrayList<ValueNamePairSkillSR> =
                Helper.getSortedSkills(sg.skills, data, mCalculator)

            //DEBUG
            val s = StringBuilder()
            for (pair in sg_skills_valued) {
                s.append(pair.toString()).append(";;")
            }
//            Helper.log(DEBUG_TAG_CALC_EXP_GROUP_SKILL, "values of skill group = $s")
            for (i in sg_skills_valued.indices) {
                if (sg_skills_valued[i].name.equals(skill_name, ignoreCase = true)) {
                    index = i
                }
            }
            //            Helper.log(DEBUG_TAG, "index = " +  index);
            current_value = sg_skills_valued[index].value
            //            Helper.log(DEBUG_TAG, "current value  = " +  current_value);

            //make sure the index is as high as possible without changing the value;
            while (index > 0 && sg_skills_valued[index - 1].value == current_value) {
                index--
            }
            //            Helper.log(DEBUG_TAG, "index after getting min index with same value = " +  index);
            while (index > 0) {
//                Helper.log(DEBUG_TAG, "starting while loop : WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW index = " +  index);
                nullifying_factor = 1
                inverse_factor = 1
                if (index > 1) {
                    inverse_factor = 2
                    if (index > 2) {
                        nullifying_factor = 0
                    }
                }
                //                Helper.log(DEBUG_TAG, "inverse_factor  = " +  inverse_factor + ", nullifying_factor = " + nullifying_factor);
                current_value = sg_skills_valued[index].value
                //                Helper.log(DEBUG_TAG, "current value  = " +  current_value);
                val index_of_next_step = getMinimumIndexOfNextHighestSkill(sg_skills_valued, index)
                //                Helper.log(DEBUG_TAG, "index of next step = " +  index_of_next_step);
                if (index_of_next_step == -1) {
//                    Helper.log(DEBUG_TAG, "ERROR returning exp = 0 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" +
//                            "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" +
//                            "XXXXXXXXXXXXXXXXXXXXXXXXX");
                    return 0
                }
                next_step = Math.min(new_value, sg_skills_valued[index_of_next_step].value)
                //                Helper.log(DEBUG_TAG, "next step = " +  next_step);
                if (next_step > current_value) {
                    karma += mCalculator.getExpCost(
                        current_value,
                        next_step
                    ) / inverse_factor * nullifying_factor
                    //                    Helper.log(DEBUG_TAG, "exp cost for current value to next step = " + mCalculator.getExpCost(current_value, next_step) / inverse_factor * nullifying_factor);
                } else {
//                    Helper.log(DEBUG_TAG, "next step <= current value so stop:");
                }
                index = index_of_next_step
                current_value = next_step
                //                Helper.log(DEBUG_TAG_CALC_EXP_GROUP_SKILL, "karma = " + karma);
//                Helper.log(DEBUG_TAG, "new index = " +  index);
//                Helper.log(DEBUG_TAG, "new current value  = " +  current_value);
                if (current_value >= new_value) {
//                    Helper.log(DEBUG_TAG, "STOPPING while loop : WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW index = " +  index);
                    break
                }
            }
            if (current_value < new_value) {
                karma += mCalculator.getExpCost(current_value, new_value)
                //                Helper.log(DEBUG_TAG, "reached skill maximum: cost to increase from max (" + current_value  + ") to " + new_value + " = "  + mCalculator.getExpCost(current_value, new_value));
            }
        } ?:run {
            /*
                    -------------- skill is not in a skill group -------------------
                     */
            var current_value = 0

            //if there is already a skill/Language/Attribute with that name we use
            //its current value to calc correct karma cost.
            val valueNamePair = getValueNamePairSkill(skill_name, data) //might return 0 if there is none
            if (valueNamePair != null) {
//                  Helper.log(DEBUG_TAG_AUTO_COMPLETITION, "ValueNamePair: " + valueNamePair.name + " " + valueNamePair.value);
                current_value = valueNamePair.value
            }


//                Helper.log("current_value = " + current_value);
            if (new_value > current_value) {
//                  Helper.log(DEBUG_TAG_AUTO_COMPLETITION, "value > current_value");
                karma += mCalculator.getExpCost(current_value, new_value)
            }
        }
        return karma
    }

    override fun getExpProposal(
        text: String,
        nb_of_active_chars: Int,
        data: ArrayList<*>
    ): Int {
        var proposal = 0
        val mText = updateFilterConstraintToCurrentLanguage(text)
        if (nb_of_active_chars == 1 && mText.matches(("(?i).*" + item?.filterConstraint.toString() + "\\s+.*").toRegex())
        ) {
            val textAfterFilterConstraint =
                Helper.getTextAfterStringAndTrim(mText, item?.filterConstraint)
            val xvnp = getNameSpecializationDescriptionAndValue(
                text = textAfterFilterConstraint, item = item, context = context)
            var name = xvnp.name.trim()
            var value = xvnp.value
            val specializations = xvnp.specializations

//            Helper.log(
//                DEBUG_TAG,
//                "get exp proposal for: $name: $value, specializations $specializations #############################"
//            )
            if (!mText.lowercase(Locale.getDefault())
                    .contains(Global.IGNORE_EXP_CHECK_INDICATOR.lowercase(Locale.getDefault()))
            ) {
//                  
                proposal = -calcExpCost(value, name, data as ArrayList<ValueNamePairSkillSR>)
//                Helper.log(DEBUG_TAG, "exp proposal no specialization: $proposal")
                proposal -=  mCalculator.getSpecializationCost(
                    oldSpecializations = getValueNamePairSkill(name = name, data)?.specializations,
                    newSpecialization = specializations)


//                Helper.log(DEBUG_TAG, "exp proposal WITH specialization: $proposal ------- old specializations: ${
//                    getValueNamePairSkill(
//                        name = name,
//                        data
//                    )?.specializations
//                }")



            }

        }
        return proposal
    }

    private fun getMinimumIndexOfNextHighestSkill(
        valueNamePairList: ArrayList<ValueNamePairSkillSR>,
        index: Int
    ): Int {
        var index = index
        if (index >= 0 && index < valueNamePairList.size) {
            var first_higher_value = -1
            val init_value: Int = valueNamePairList[index].value
            while (index > 0) {
                index--
                first_higher_value = valueNamePairList[index].value
                if (first_higher_value > init_value) {
                    break
                }
            }
            while (index > 0 && valueNamePairList[index - 1].value == first_higher_value) {
                index--
            }
            return index
        }
        return -1
    }

    override fun getAutoCompleteValue(name: String): Int? {
        return getValueNamePairSkill(name, auto_complete_data)?.value
    }

    override fun inflate(expand: Boolean): View? {
        val container = super.inflate(expand = false)
        val inflater = LayoutInflater.from(context)



        //INFLATE HEADER------------------------------------------------------------------
        header_view = container?.findViewById(R.id.charProgress_label)
        header_view?.text = item?.header
        //-----------------------------------------------------------------------------

        //Set Column Headers
        val totalTv = container?.findViewById<TextView>(R.id.stat_1_label)
        totalTv?.setText(R.string.total)
        val baseTv = container?.findViewById<TextView>(R.id.stat_0_label)
        baseTv?.setText(R.string.base)

        if (expand) {
            //INFLATE ENTRIES
            listOfViews = container?.findViewById(R.id.charProgress_list)
            sort()
            listOfViews?.removeAllViews()
            data_copy = ArrayList(data)
            if (expand) {
                skillGroupAdapter?.also { sg_adapter ->
                    for (sg in Global.skill_groups) {
                        listOfViews?.addView(inflateSkillGroup(sg, inflater, sg_adapter))
                        for (skill in sg.skills) {
                            removeIfContained(skill, data)
                        }
                    }
                }


                /*
       inflate views
       */
//        Helper.log("INFLATING_CHAR_PROG_ITEMS", "Data size "+ item.header + " = " + data.size());
                for (i in 0 until count) {
//            Helper.log("INFLATING_CHAR_PROG_ITEMS", item.header + ": " + data.get(i).toString());
                    listOfViews?.addView(getView(i, item?.check_mistakes ?: false))
                }
                data = data_copy


                /*
               make empty charProgressItems invisible if there listOfViews is empty
               */
                if (listOfViews?.childCount == 0) {
                    container?.visibility = View.GONE
                }

            }
        }
        header_view?.setOnLongClickListener {
            val searchString = item?.filterConstraintTranslations?.joinToString(" OR ") +
                    " OR " +
                    TranslationHelper(context).getAllTranslations(
                        R.string.filter_constraint_skill_group).joinToString(" OR ")
            Log.d("SRSKLL", "SR skill header onLongClick searchString = $searchString")
            DialogShowFilteredLogs.newInstance(
                 searchString = searchString,
                markErrors = item?.check_mistakes ?: false
            ).show(
                mActivity!!.supportFragmentManager,
                ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
            )
            true
        }
        return container
    }

    /**
     * removes a value name pair with given name from data if it is contained
     * @param name
     * @param data
     */
    private fun removeIfContained(name: String, data: ArrayList<ValueNamePairSkillSR>) {
        val data_to_remove: ArrayList<ValueNamePairSkillSR> = ArrayList<ValueNamePairSkillSR>()
        for (pair in data) {
            if (pair.name.equals(name, ignoreCase = true)) {
                data_to_remove.add(pair)
            }
        }
        data.removeAll(data_to_remove)
    }

    val skillGroupAdapter: CharProgressItemAdapterSkillGroups?
        get() {
            try {
                return mActivity?.charProgressItemsMap?.get(R.string.filter_constraint_skill_group)?.mainItem?.adapter
                        as? CharProgressItemAdapterSkillGroups?
            } catch (e: Exception) {
                e.printStackTrace()
                Helper.log("Skills", "No SkillGroup Adapter found")
            }
            return null
        }

    private fun inflateSkillGroup(
        sg: SkillGroup,
        inflater: LayoutInflater,
        sg_adapter: CharProgressItemAdapterSkillGroups?
    ): View {
        var contains_relevant_data = false
        val skill_group_container =
            inflater.inflate(R.layout.char_progress_list_item_skill_group, null) as LinearLayout
        val skill_names_container =
            skill_group_container.findViewById<LinearLayout>(R.id.skill_group_name_container)
        val skill_ranks_container =
            skill_group_container.findViewById<LinearLayout>(R.id.skill_group_rank_container)
        val skill_group_name = skill_group_container.findViewById<TextView>(R.id.skill_group_name)
        val skill_group_rank = skill_group_container.findViewById<TextView>(R.id.skill_group_rank)
        skill_group_name.text = sg.name
        val min: Int = sg_adapter?.getMin(sg_adapter.getRanks(sg, data_copy)) ?: -1
        skill_group_rank.text = min.toString()
        if (min == -1) {
            skill_group_rank.visibility = View.INVISIBLE
        }
        if (sg.hasError && item?.check_mistakes == true) {
            skill_group_name.setBackgroundResource(R.color.holo_red_light_half_transparent)
        }
        val check_mistakes_finalized: Boolean = item?.check_mistakes ?: false

        fun getSkillGroupHeaderStreachString(): String  {
            val sgfcList = TranslationHelper(context).getAllTranslations(R.string.filter_constraint_skill_group)
            return ("${sgfcList.joinToString(" OR ") {
                "\"$it ${sg.name}\""}
            } OR " + sg.skills.joinToString(separator = " OR ") {sname ->
                item?.filterConstraintTranslations?.joinToString(" OR ") {fc ->
                    "\"$fc $sname\""
                } ?: ""
            }
                    )
                .also {
                    Log.d("SRSKLL", "skill group name onLongClick searchString: '$it'")
                }
        }
        skill_group_name.setOnLongClickListener {
            mActivity?.supportFragmentManager?.let { sfm ->
                DialogShowFilteredLogs.newInstance(
                    getSkillGroupHeaderStreachString(),
                    check_mistakes_finalized
                ).show(
                    sfm,
                    ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                )
            }
            true
        }
        skill_group_rank.setOnLongClickListener {
            mActivity?.supportFragmentManager?.let { sfm ->
                DialogShowFilteredLogs.newInstance(
                    getSkillGroupHeaderStreachString(),
                    check_mistakes_finalized
                ).show(
                    sfm,
                    ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                )
            }
            true
        }


        /////////////////// SKILLS ////////////////
        for (skill in sg.skills) {
            /*
            inflate skill name view
             */
            val skill_name_view = inflater.inflate(R.layout.skill_indented, null) as TextView
            val skill_rank_view = inflater.inflate(R.layout.skill_rank, null) as TextView
            val skill_pair = getValueNamePairSkill(skill, data)
            var rank = -1
            var name: String = skill
            var rank_display: String? = "-1"
            if (skill_pair != null) {
                rank = skill_pair.value
                rank_display = rank.toString()
                if (skill_pair.specializations.size > 0) {
                    name += " " + skill_pair.specializations.toString()
                }
                if (skill_pair.augmentation != 0) {
                    rank_display = java.lang.String.format(
                        Locale.GERMAN,
                        "%d (%d)",
                        skill_pair.value,
                        skill_pair.value + skill_pair.augmentation
                    )
                }
            } else {
                skill_rank_view.setTextColor(
                    ContextCompat.getColor(context, android.R.color.holo_green_dark)
                )
            }
            skill_rank_view.text = rank_display
            skill_name_view.text = name

            skill_rank_view.setOnClickListener {
                mActivity?.supportFragmentManager?.let { sfm ->
                    val searchString = item?.filterConstraintTranslations?.
                    joinToString(" OR ") { "\"$it $name\""} ?: ""
                    DialogShowFilteredLogs.newInstance(
                        searchString,
                        check_mistakes_finalized
                    ).show(
                        sfm,
                        ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                    )
                }
            }
            skill_names_container.addView(skill_name_view)

            /*
            inflate skill rank view
             */
            skill_ranks_container.addView(skill_rank_view)


            // skill group will not be displayed if no relevant data
            contains_relevant_data = contains_relevant_data || rank != -1

            /*
            checking mistakes only for those where there are acctually enties
             */if (skill_pair != null) {
                if (item?.check_mistakes == true) {
                    if (!skill_pair.noCalcErrors || skill_pair.was_reduced_and_you_even_spend_exp_for_it) {
                        skill_name_view.setBackgroundResource(R.color.holo_red_light_half_transparent)
                        skill_rank_view.setBackgroundResource(R.color.holo_red_light_half_transparent)
                    }
                    //            else {
//                entry.setBackgroundResource(R.color.holo_green_light_half_transparent);
//            }
                    if (skill_pair.was_reduced) {
                        var msg =
                            "In " + item?.header.toString() + " " + skill_pair.name.toString() + " was reduced"
                        if (skill_pair.was_reduced_and_you_even_spend_exp_for_it) {
                            msg += ". And you even spend karma for it! Probably order of logs in incorrect."
                        }
                        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show()
                    }
                }
                fun getSkillHeaderSearchString(): String {
                    return item?.let { item ->
                        item.filterConstraintTranslations.joinToString(" OR ") {
                            "\"$it ${skill_pair.name}\""
                        } +  " OR " +
                                TranslationHelper(context).getAllTranslations(
                                    R.string.filter_constraint_skill_group).
                                joinToString(" OR ") {
                                    "\"%s %s\"".format(
                                        it,
                                        getSkillGroupName(skill_pair.name)
                                    )
                                }
                    } ?: ""
                }

                skill_name_view.setOnLongClickListener {
                    DialogShowFilteredLogs.newInstance(
                        getSkillHeaderSearchString(),
                        check_mistakes_finalized
                    ).show(
                        mActivity!!.supportFragmentManager,
                        ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                    )
                    true
                }
                skill_rank_view.setOnLongClickListener {
                    DialogShowFilteredLogs.newInstance(
                        getSkillHeaderSearchString(),
                        check_mistakes_finalized
                    ).show(
                        mActivity!!.supportFragmentManager,
                        ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                    )
                    true
                }
            }
        }
        if (!contains_relevant_data) skill_group_container.visibility = View.GONE
        return skill_group_container
    }

    /**
     * this is only used if there is no @Value found in logText so there will be no old value
     *
     * @param e
     * @param pair
     * @param index_of_changed_pair
     */
    class Holder(var logEntry: LogEntry, pair: ValueNamePairSkillSR? = null, index_of_changed_pair: Int? = null) {

        /**
         * this is where data is stored when we dont want to modify to data we currently have
         * (data or getAuto_complete_data())
         * We use this to get exp proposal. There we want to access to avalable data,
         * but don't want to change it
         */
        var collectedData: ArrayList<ValueNamePairSkillSR>? = null
        var holderItems = ArrayList<HolderItem>()

        init {
            index_of_changed_pair?.let {
                holderItems.add(
                    HolderItem(
                        pair,
                        index_of_changed_pair
                    )
                )
            }

        }
    }

    class HolderItem
    /**
     * this is only used if there is no @Value found in logText so there will be no old value
     *
     * @param pair
     * @param index_of_changed_pair
     */ (
        /**
         * contains the new value of the name cut from LogEntry and the name itself
         */
        var pair: ValueNamePairSkillSR? = null,
        /**
         * this is the index of the changed valueNamePair in data.
         * if index is negative it means that there is no pair with that name yet in the data
         * -2 means that no @Value was found
         */
        var index_of_changed_pair: Int = -10
    ) {
        /**
         * the value that was there before
         */
        var old_value = -999
        var hasError = false
    }

    companion object {
        const val DEBUG_TAG = "CHAR_PROG_ADAPTER_SKILLS_SR"
        fun dealWithValueBasedCharProgItemWhereNoValueIsFound(
            e: LogEntry,
            ignoreExpCheck: Boolean,
            item: CharProgressItem?
        ): HolderItem {
            e.hasError = !ignoreExpCheck
            val valueNamePair = ValueNamePairSkillSR(
                name = cutAfterString(e.text, item?.filterConstraint).trim(), e.exp, e.hasError,
                mCalculator = object : Calculator {})

//        Helper.log("LOG_ERROR", e.text + " ERROR = " + e.hasError);
            return HolderItem(valueNamePair, -2)
        }

    }

    init {
        stdAutoCompleteData = HashMap()
        stdAutoCompleteData[RuleSystem.rule_system_sr5] =
            mActivity?.let {
                Skill.getSkillMap(arrayId = R.array.shadowrun5_skill_list, context = it)
                    .keys
                    .map { Helper.capitalizeNewsLetterStyle(it)}
            } ?: listOf()
        stdAutoCompleteData[RuleSystem.rule_system_sr6] =
            mActivity?.let {
                Skill.getSkillMap(arrayId = R.array.shadowrun6_skill_list, context = it)
                    .keys
                    .map { Helper.capitalizeNewsLetterStyle(it)}
            } ?: listOf()
    }
}