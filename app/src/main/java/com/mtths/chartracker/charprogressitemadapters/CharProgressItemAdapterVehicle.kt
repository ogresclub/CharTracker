package com.mtths.chartracker.charprogressitemadapters

import android.graphics.drawable.ClipDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View.OnClickListener
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.Buff
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.Skill.Companion.getSkill
import com.mtths.chartracker.dataclasses.Skill.Companion.getSkillData
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.interfaces.CPIDDamageable
import com.mtths.chartracker.interfaces.CPIDSelfBuffable
import com.mtths.chartracker.interfaces.Moddable
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.SnackBarUtils
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import com.mtths.chartracker.utils.StringUtils.getIntegerStatWithExtraCharsFromText
import com.mtths.chartracker.utils.StringUtils.getStatFromText
import java.util.*
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt


class   CharProgressItemAdapterVehicle(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapterOneLineExpandable<CharProgressItemAdapterVehicle.Vehicle>(
        activity = activity,
        colHeaderIds = listOf(
            REF_ID_CHECK,
            REF_ID_CURRENT_SPEED,
            REF_ID_HANDLING
        ),
        colWeights = listOf(
            1f,
            1.2f,
            1.3f
        )
    ){

    override fun createAutoCompleteStrings(): ArrayList<String> {
        return super.createAutoCompleteStrings().apply {
            addAll(
                //add id refs for stats here
                mutableListOf(
                    REF_ID_HANDLING,
                    REF_ID_ACCELLERATION,
                    REF_ID_BODY,
                    REF_ID_ARMOR,
                    REF_ID_PILOT,
                    REF_ID_SENSOR,
                    REF_ID_SEATS,
                    REF_ID_DEVICE_RATING,
                    REF_ID_MAX_SPEED_REF
                ).apply {
                    if (rulesSR6) add(REF_ID_SPEED_INTERVAL)
                }.map {
                    context.getString(it)
                }.map {
                    "$it = "
                }
            )
            Vehicle().addSelfBuffAutoCompleteString(this, context)
            addAll(
                listOf(
                    Vehicle().REF_MOD
                ).map {
                    "$it = ["
                }
            )


        }
    }

    override fun generate_autocomplete_data(e: LogEntry) {
    }

    override fun add(e: LogEntry): LogEntry {
        val textAfterFilterConstraint = Helper.cutAfterString(e.text, item!!.filterConstraint)
        val vehicle = Vehicle()
        vehicle.relatedLogEntry = e
        vehicle.parseSelfBuff(context)
        vehicle.parseMods(textAfterFilterConstraint)
        vehicle.parseName(logEntryText = e.text, item = item)
        vehicle.handling = getStatFromText(
            statId = REF_ID_HANDLING,
            text = e.text,
            context = context) ?: "-"
        vehicle.maxSpeed = if (rulesSR5) {
            getIntegerStatWithExtraCharsFromText(
                statId = REF_ID_MAX_SPEED_REF,
                text = e.text,
                context = context,
                extra_chars = "/").toIntOrNull()
        } else {
            getIntegerStatFromText(
                statId = REF_ID_MAX_SPEED_REF,
                text = e.text,
                context = context).toIntOrNull()
        }
        vehicle.speedInterval = getIntegerStatFromText(
            statId = REF_ID_SPEED_INTERVAL,
            text = e.text,
            context = context).toIntOrNull()
        vehicle.acceleration = getIntegerStatFromText(
            statId = REF_ID_ACCELLERATION,
            text = e.text,
            context = context
        )
        vehicle.body = getIntegerStatFromText(
            statId = REF_ID_BODY,
            text = e.text,
            context = context
        ).toIntOrNull()
        vehicle.deviceRating = getIntegerStatFromText(
            statId = REF_ID_DEVICE_RATING,
            text = e.text,
            context = context
        ).toIntOrNull()
        vehicle.armor = getIntegerStatFromText(
            statId = REF_ID_ARMOR,
            text = e.text,
            context = context
        ).toIntOrNull()
        vehicle.pilot = getIntegerStatFromText(
            statId = REF_ID_PILOT,
            text = e.text,
            context = context
        )
        vehicle.sensor = getIntegerStatFromText(
            statId = REF_ID_SENSOR,
            text = e.text,
            context = context
        )
        vehicle.seats = getIntegerStatFromText(
            statId = REF_ID_SEATS,
            text = e.text,
            context = context
        )
        data.add(vehicle)
        checkExpand(vehicle)
        return e
    }

    fun getSeekbar(vehicle: Vehicle): SeekBar? {

        val color = TypedValue().apply {
            context.theme.resolveAttribute(
                R.attr.char_progress_item_container_border_color,
                this,
                true
            )
        }.data


        fun setProgressDrawable(seekBar: SeekBar) {
            // Set the progress drawable color
            val progressDrawable = ClipDrawable(
                ColorDrawable(color),
                Gravity.START,
                ClipDrawable.HORIZONTAL).apply {
            }
            seekBar.progressDrawable = progressDrawable
        }


        fun setCustomThumb(seekBar: SeekBar, size: Int) {
            val thumb = ShapeDrawable(OvalShape())
            thumb.intrinsicHeight = size
            thumb.intrinsicWidth = size
            thumb.paint.color = color // Set the color you want
            seekBar.thumb = thumb
            Log.d("SEEKBAR", "Custom thumb set with color: $color")
        }


        return vehicle.maxSpeed?.let { maxSpeed ->

            SeekBar(context).apply {
                max = maxSpeed
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                setCustomThumb(this, 20)
                setProgressDrawable(this)

                progress = min(vehicle.currentSpeed, maxSpeed)
                setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
                    override fun onProgressChanged(seekbar: SeekBar?, progress: Int, fromUser: Boolean) {
                        vehicle.currentSpeed = progress
                        updateColumnData(vehicle)
                        Log.d("SEEKBAR", "current speed = $progress")
                    }

                    override fun onStartTrackingTouch(p0: SeekBar?) {
                    }

                    override fun onStopTrackingTouch(p0: SeekBar?) {
                        inflateCharDetails()
                    }
                })
            }
        }
    }

    override fun addExpandDataViewsToContainer(
        dataItem: CharProgressItemData,
        container: LinearLayout
    ) {
        val containerMap = HashMap<Int, LinearLayout>()

        var mContainer = container

        //try to add Seekbar.
        val vehicle = (dataItem as? Vehicle?)
        vehicle?.let {
            getSeekbar(it)?.let {
                container.addView(it)
                container.addView(
                    LinearLayout(context).apply {
                        layoutParams = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                        )
                        orientation = LinearLayout.VERTICAL
                        mContainer = this
                    }
                )
            }
        }



        // add Left and Right Container
        mContainer.apply {
            orientation = LinearLayout.HORIZONTAL


            for (i in 0 until 2) {
                addView(
                    LinearLayout(context).apply {
                        layoutParams = LinearLayout.LayoutParams(
                            0,
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            1f
                        )
                        orientation = LinearLayout.VERTICAL
                        containerMap[i] = this
                    }
                )
            }
        }

        dataItem.expandData.forEachIndexed { index, d ->
            d.getView(mActivity)?.let {
                containerMap[index % 2]?.addView(
                    it
                )
                //store view reference in viewHolder
                vehicle?.viewHolder?.expandHolder?.put(d.header, it)
            }
        }
    }

    fun updateColumnData(vehicle: Vehicle) {
        vehicle.viewHolder?.columnsHolder?.apply {
            dataViews?.get(0)?.apply {
                text = vehicle.check.toString()
                setOnClickListener(getShowSkillCheckCalculationOnClickListener(vehicle))
            }
            dataViews?.get(1)?.apply {
                text = vehicle.getSpeedString(
                    speed = vehicle.currentSpeed,
                    acceleration = false
                )
                setOnClickListener(toggleUnitsOnClickListener)
            }
            dataViews?.get(2)?.text = vehicle.handling
        }

    }

    override fun inflateView(index: Int, check_mistakes: Boolean): ViewHolder {
        return super.inflateView(index, check_mistakes).apply {

            val vehicle: Vehicle = data[index]
            vehicle.viewHolder = CharProgressItemData.ViewHolder()
            vehicle.viewHolder?.columnsHolder = this

            nameView?.apply {
                text = vehicle.name
                setOnClickListener(getShowDamageMonitorOnClickListener(vehicle))
            }
            updateColumnData(vehicle)

            implementExpand(view = this.root, data = vehicle)
        }
    }

    val toggleUnitsOnClickListener: OnClickListener = OnClickListener {
        item?.character?.let { c ->
            c.useKmhForSpeed = !c.useKmhForSpeed
            DatabaseHelper.getInstance(context)?.updateCharacter(
                character = c,
                keep_last_update_date = true
            )
            inflateCharDetails()
        }
    }

    fun getShowDamageMonitorOnClickListener(
        vehicle: Vehicle
    ): OnClickListener = OnClickListener {
        mActivity?.supportFragmentManager?.let { fm ->
            vehicle.showDamageMonitor(fm = fm, context = context)
        }
    }

    fun getShowSkillCheckCalculationOnClickListener(
        vehicle: Vehicle
    ): OnClickListener = OnClickListener {v ->
        item?.character?.let { c ->
            c.ruleSystem?.getSkillDetailsBuilder?.invoke(
                c,
                ValueNamePair(
                    name = pilotingSkillName,
                    skill = getSkill(
                        name = pilotingSkillName,
                        character = c,
                        skillData = getSkillData(context)
                    ),
                    mCalculator = object : CharProgressItemAdapterValueBased.Calculator {}
                ),
                context
            )?.msgFormatTotal?.format(vehicle.check) +
                    if (vehicle.damageMalus > 0) {
                        " - %d (%s)".format(
                            vehicle.damageMalus,
                            context.getString(R.string.damage_label)
                        )
                    } else {
                        ""
                    } +
                    if (vehicle.speedMalus > 0) {
                        " - %d (%s)".format(
                            vehicle.speedMalus,
                            context.getString(R.string.cpi_vehicles_label_current_speed)
                        )
                    } else {
                        ""
                    } + vehicle.buffCalculationString

        }?.let { msg ->
            SnackBarUtils.showSnackBar(
                view = v,
                msg = msg,
                duration = Snackbar.LENGTH_LONG,
                context = context
            )
        }

    }

    val pilotingSkillName: String
        get() = context.getString(R.string.shadowrun6_skill_piloting)



    inner class Vehicle :
        CharProgressItemData(),
        Moddable,
        CPIDDamageable,
        CPIDSelfBuffable {
        override val mods: MutableList<String> = mutableListOf()
        override val _REF_MOD: String? = null
        override var buff: Buff = Buff()
            // means is used DEFAULT REF_MOD
        override val damageableData: List<CPIDDamageable.DamageableData>
            get() = listOf(
                CPIDDamageable.DamageableData(
                    hp = 8 + ceil((deviceRating ?: 0).toFloat() / 2).toInt(),
                    damageTypeId = R.string.label_damage_monitor_matrix
                ),
                CPIDDamageable.DamageableData(
                    hp = (body ?: 0).let { b ->
                        8 + ceil(b.toFloat() / 2).toInt()
                    },
                    damageTypeId = R.string.label_physical_damage

                )
            )


        var currentSpeed: Int = 0
        val damageMalus: Int
            get() = listOf(
                R.string.label_damage_monitor_matrix,
                R.string.label_physical_damage
            ).sumOf {
                getDamage(
                    character = item?.character,
                    damageTypeId = it,
                    context = context)?.get(0)?.let {
                    max(it / 3, 0)
                } ?: 0
            }

        val speedMalus: Int
            get() = speedInterval?.let {
                currentSpeed / (if (it > 0) it else 1)
            } ?: 0


        val check: Int
            get() = getSkillValue(R.string.shadowrun6_skill_piloting) - damageMalus - speedMalus + totalBuffValue
        val kmh: Boolean
            get() = item?.character?.useKmhForSpeed == true
        var handling: String
        var maxSpeed: Int? = null
        var acceleration: String
        var body: Int? = null
        var armor: Int? = null
        var pilot: String
        var sensor: String
        var seats: String
        var speedInterval: Int? = null
        var deviceRating: Int? = null

        val pilotingBase: Int
            get() = item?.character?.getTotalStatValue(
                pilotingSkillName
            ) ?: -1
        val defenseRating: Int
            get() = item?.character?.let {
                max(
                    it.getTotalStatValue(pilotingSkillName),
                    (body ?: 0)
                ) + (armor ?: 0)
            } ?: 0


        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                add(
                    ExpandData(
                        headerId = REF_ID_MAX_SPEED_REF,
                        text = getSpeedString(
                            speed = maxSpeed),
                        onClickListener = toggleUnitsOnClickListener
                    )
                )
                add(
                    ExpandData(
                        headerId = REF_ID_SPEED_INTERVAL,
                        text = getSpeedString(
                            speed = speedInterval),
                        onClickListener = toggleUnitsOnClickListener
                    )
                )
                add(
                    ExpandData(
                        headerId = REF_ID_ACCELLERATION,
                        text = getSpeedString(
                            speed = acceleration.toIntOrNull(),
                            acceleration = true),
                        onClickListener = toggleUnitsOnClickListener
                    )
                )
                add(
                    ExpandData(
                        headerId = REF_ID_BODY,
                        text = body.toString()
                    )
                )
                add(
                    ExpandData(
                        headerId = REF_ID_ARMOR,
                        text = armor.toString()
                    )
                )
                add(
                    ExpandData(
                        headerId = REF_ID_PILOT,
                        text = pilot
                    )
                )
                add(
                    ExpandData(
                        headerId = REF_ID_SENSOR,
                        text = sensor
                    )
                )
                add(
                    ExpandData(
                        headerId = REF_ID_SEATS,
                        text = seats
                    )
                )
                add(
                    ExpandData(
                        headerId = R.string.stat_defense_rating_label,
                        text = defenseRating.toString(),
                        onClickListener =  {
                            Snackbar.make(
                                it,
                                "max( %s (%d), %s (%d) ) + %s (%d)".format(
                                    context.getString(R.string.shadowrun6_skill_piloting),
                                    pilotingBase,
                                    context.getString(REF_ID_BODY),
                                    body ?: 0,
                                    context.getString(REF_ID_ARMOR),
                                    armor ?: 0
                            ),
                                Snackbar.LENGTH_LONG
                            ).show()
                        }
                    )
                )
                if (mods.isNotEmpty()) {
                    add(modsExpandDataWithHeader)
                }
            }

        private fun convertSpeed(speed: Int?): Int? {
            return speed?.let { (it * 1.2).roundToInt()}
        }

        fun getSpeedString(
            speed: Int?,
            includeUnit: Boolean = true,
            acceleration: Boolean = false): String {
            val unit = when {
                kmh && acceleration -> "kmh/r"
                kmh && !acceleration -> "kmh"
                !kmh && acceleration -> "m/rr"
                !kmh && ! acceleration -> "m/r"
                else -> ""
            }
            val mSpeed = if (kmh) convertSpeed(speed) else speed
            return mSpeed?.let {
                it.toString() + if (includeUnit) unit else ""
            } ?: "-"
        }

        init {
            name = "Some Vehicle"
            handling = "-"
            acceleration = "-"
            acceleration = "-"
            pilot = "-"
            sensor = "-"
            seats = "-"
        }
    }

    companion object {
        val REF_ID_MAX_SPEED_REF = R.string.cpi_vehicles_label_max_speed
        val REF_ID_CURRENT_SPEED = R.string.cpi_vehicles_label_current_speed
        val REF_ID_SPEED_INTERVAL = R.string.cpi_vehicles_label_speed_interval
        val REF_ID_HANDLING = R.string.cpi_vehicles_label_handling
        val REF_ID_ACCELLERATION = R.string.cpi_vehicles_label_acceleration
        val REF_ID_BODY = R.string.cpi_vehicles_label_body
        val REF_ID_DEVICE_RATING = R.string.label_device_rating
        val REF_ID_ARMOR = R.string.cpi_vehicles_label_armor
        val REF_ID_PILOT = R.string.cpi_vehicles_label_pilot
        val REF_ID_SENSOR = R.string.cpi_vehicles_label_sensor
        val REF_ID_SEATS = R.string.cpi_vehicles_label_seats
        val REF_ID_CHECK = R.string.label_check
    }
}