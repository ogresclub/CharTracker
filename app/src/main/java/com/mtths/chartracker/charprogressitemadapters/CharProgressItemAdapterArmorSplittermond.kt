package com.mtths.chartracker.charprogressitemadapters

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.BuffEffect
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.SpellDnD
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.capitalize
import com.mtths.chartracker.utils.HttpUtils
import com.mtths.chartracker.utils.StringUtils.getIntegerStatFromText
import com.pedromassango.doubleclick.DoubleClick
import com.pedromassango.doubleclick.DoubleClickListener
import java.util.ArrayList

class CharProgressItemAdapterArmorSplittermond(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapter<CharProgressItemAdapterArmorSplittermond.Armor>(
        mActivity = activity,
        comparator = compareBy { it.name }
    ),
    CharProgressItemAdapterBuffable {
    val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values
    override  val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header



    override fun createAutoCompleteStrings(): ArrayList<String> {
        return arrayListOf(
            "$REF_BEHINDERUNG = ",
            "$REF_VERTEIDIGUNG  = ",
            "$REF_SR  = ",
            "$REF_TIK_MALUS = "
        ).also {
            addBuffAutoCompleteString(it)
            Armor().addModsAutoCompleteString(it)
        }
    }

    override fun generate_autocomplete_data(e: LogEntry) {
    }

    override fun add(e: LogEntry): LogEntry {
//        Log.e("BUFFF_ARMOR", "add armor called for log: ${e.text}")

        //first entry is karma, second is text
        val armor = Armor()
        armor.relatedLogEntry = e

        /*
        set name
         */
        item?.filterConstraint?.let { armor.parseName(e.text, item) }
        armor.parseVerteidigung(e.text)
        armor.parseBehinderung(e.text)
        armor.parseTikMalus(e.text)
        armor.parseSR(e.text)

        data.add(armor)
        armor.parseMods(e.text)
        val extraBuffs = mutableListOf<String>().apply {
            armor.verteidigung?.let {  add("${BuffEffect.BUFF_REF_VERTEIDIGUNG} $it") }
            armor.behinderung?.let {  add("${BuffEffect.BUFF_REF_BEHINDERUNG} $it")}
            armor.tikmalus?.let {  add("${BuffEffect.BUFF_REF_TIK} $it")}
            armor.sr?.let {  add("${Character.STAT_DAMAGE_REDUCTION} $it")}
        }
        createBuffs(s = e.text, iData = armor, item = item, context = context,
            extraBuffEffects = extraBuffs
        )
        checkExpand(armor)
        return e
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? {
        val armor: Armor = data[index]



        //inflate entry
        val entry: View =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false)

        val name_display = entry.findViewById<TextView>(R.id.text)
        val verteidigungs_display = entry.findViewById<TextView>(R.id.number).apply {
            gravity = Gravity.CENTER
        }
        val tik_malus_display = entry.findViewById<TextView>(R.id.number2).apply {
            gravity = Gravity.CENTER
        }
        val behinderungs_display = entry.findViewById<TextView>(R.id.number3).apply {
            gravity = Gravity.CENTER
            visibility = View.VISIBLE
        }
        val sr_display = entry.findViewById<TextView>(R.id.number4).apply {
            gravity = Gravity.CENTER
            visibility = View.VISIBLE
        }
        name_display.text = armor.getNameDisplay()
        verteidigungs_display.text = armor.verteidigung?.toString() ?: "-"
        tik_malus_display.text = armor.tikmalus?.let {"-%d".format(it)} ?: "-"
        behinderungs_display.text = armor.behinderung?.toString() ?: "-"
        sr_display.text = armor.sr?.toString() ?: "-"


        implementExpand(view = entry, data = armor)


        entry.setOnClickListener(DoubleClick(
            object : DoubleClickListener {
                override fun onDoubleClick(view: View?) {
                    mActivity?.let {
                        HttpUtils.openWebPage(
                            url = armor.getLink(),
                            activity = it
                        )
                    }
                }

                override fun onSingleClick(view: View?) {}
            }
        ))



        mActivity?.let { a ->
            entry.setOnLongClickListener(
                ListenerImplOpenEditLogsOnLongClick(
                    a.supportFragmentManager,
                    armor.relatedLogEntry
                )
            )
        }


        return entry
    }

    override fun inflate(expand: Boolean): View? {
        val v = super.inflate(expand)

        renameStat0ColumnHeader(name = capitalize(context.resources.getString(R.string.defense_short)), containerView = v)
        renameStat3ColumnHeader(name = capitalize(context.resources.getString(R.string.dr)), containerView = v)
        renameStat1ColumnHeader(name = capitalize(context.resources.getString(R.string.tik)), containerView = v)
        renameStat2ColumnHeader(name = capitalize(context.resources.getString(R.string.encumbrance_short)), containerView = v)


        return v

    }

    inner class Armor : CharProgressItemAdapterGear.Gear() {
        var behinderung: Int? = 0
        var verteidigung: Int? = 0
        var tikmalus: Int? = 0
        var sr: Int? = 0

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                addAll(mods.map { ExpandData(text = it) })
            }
        fun getLink(): String {
            return SpellDnD.SPELL_URL_TEMPLATE_SPLITTERMOND + Helper.getSplittermondRef(name)
        }

        fun parseVerteidigung(s: String) {
            verteidigung = getIntegerStatFromText(REF_VERTEIDIGUNG, s).toIntOrNull()
        }
        fun parseBehinderung(s: String) {
            behinderung = getIntegerStatFromText(REF_BEHINDERUNG, s).toIntOrNull()
        }
        fun parseTikMalus(s: String) {
            tikmalus = getIntegerStatFromText(REF_TIK_MALUS, s).toIntOrNull()
        }
        fun parseSR(s: String) {
            sr = getIntegerStatFromText(REF_SR, s).toIntOrNull()
        }


    }



    companion object {
        const val DEBUG_TAG = "CHAR_PROG_ADAPTER_ARMORS_SPM"
        const val REF_BEHINDERUNG = "behinderung"
        const val REF_VERTEIDIGUNG = "verteidigung"
        const val REF_TIK_MALUS = "tik malus"
        const val REF_SR = "sr"
    }
}
