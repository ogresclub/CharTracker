package com.mtths.chartracker.charprogressitemadapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.ExpandData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.interfaces.CPIDescriptive
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.StringUtils
import kotlin.collections.ArrayList

class CharProgressItemAdapterInitiation(activity: ProgressBarFrameDropBoxActivity?) :
    CharProgressItemAdapter<CharProgressItemAdapterInitiation.CPIInitiation>(
        mActivity = activity,
        comparator = compareBy { it.relatedLogEntry?.dateOfCreation }),
    CharProgressItemAdapterBuffable
{
    override val layoutResourceMain = R.layout.char_progress_item_container_2_values_2_line_header
    val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values



    override fun inflate(expand: Boolean): View? {
        val v =  super.inflate(expand)
        header_view?.text = String.format("%s (%d)", item?.header, count)
        renameStat0ColumnHeader(
            nameId = R.string.karma_label,
            containerView = v,
            context = context
        )
        return v
    }

    override fun createAutoCompleteStrings(): ArrayList<String> {
        return addBuffAutoCompleteString(super.createAutoCompleteStrings()).apply {
            listOf(
                REF_ID_DESCRIPTION,
                REF_ID_STORY).map {
                "${context.getString(it)} = ["
            }
        }
    }


    override fun getExpProposal(text: String, nb_of_active_chars: Int, data: ArrayList<*>): Int {
        if  (text.matches(Helper.getRegexContainsFilterConstraint(item!!.filterConstraint))) {
            return -(10 + count + 1)
        }
        return super.getExpProposal(text, nb_of_active_chars, data)
    }

    override fun generate_autocomplete_data(e: LogEntry) {
        auto_complete_data.add(doAdd(e))
    }

    override fun getView(index: Int, check_mistakes: Boolean): View {
        val init = data[index]

        //inflate entry
        val init_view: View =
            LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false)
        val name_display = init_view.findViewById<TextView>(R.id.text)
        val karmaDisplay = init_view.findViewById<TextView>(R.id.number)
        init_view.findViewById<TextView>(R.id.number2).visibility = View.GONE
        name_display.text = init.getNameDisplay()
        karmaDisplay.text = init.relatedLogEntry?.exp.toString()

        mActivity?.let { a ->
            init_view.setOnLongClickListener(
                ListenerImplOpenEditLogsOnLongClick(
                    a.supportFragmentManager,
                    init.relatedLogEntry
                )
            )
        }

        implementExpand(view = init_view, data = init)
        return init_view
    }

    fun doAdd(e: LogEntry): CPIInitiation {
        val containsMarker = e.text.contains(Global.IGNORE_EXP_CHECK_INDICATOR) ||
                e.text.contains(Global.FEATURED_MARKER)
        e.hasError =  !containsMarker && e.exp != getExpProposal(e.text, 1, data)

        val initiation = CPIInitiation().apply {
            relatedLogEntry = e
            parseName(logEntryText = e.text, item = item)
            parseDescription(text = e.text, context = context)
            story = getStory(text = e.text)
        }
        Log.d("INITIATION", "${initiation.name} " +
                "log has error = ${e.hasError}: " +
                "description = ${initiation.description};;; " +
                "story = ${initiation.story}")
        createBuffs(
            s = e.text,
            iData = initiation,
            item = item,
            context = context
        )
        checkExpand(initiation)

        return initiation
    }


    override fun add(e: LogEntry): LogEntry {
        doAdd(e).let {
            data.add(it)
        }
        return e
    }


    inner class CPIInitiation(
        override var description: String = "",
        var story: String = ""
    ): CharProgressItemData(), CPIDescriptive {

        override val expandData: List<ExpandData>
            get() = super.expandData.toMutableList().apply {
                add(
                    ExpandData(
                        headerId = REF_ID_STORY,
                        text = story
                    )
                )
                add(
                    ExpandData(
                        headerId = REF_ID_DESCRIPTION,
                        text = description
                    )
                )
            }

        fun getNameDisplay() = Helper.capitalizeNewsLetterStyle(name)

        fun getStory(text: String) = StringUtils.parseCPITextStat(
            text = text,
            refId = R.string.cpi_ref_story,
            context = context
        )

    }

    companion object {
        val REF_ID_DESCRIPTION = R.string.description_label
        val REF_ID_STORY = R.string.cpi_ref_story
    }
}