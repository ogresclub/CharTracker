package com.mtths.chartracker.charprogressitemadapters

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterStd.StdEntry
import com.mtths.chartracker.dataclasses.CharProgressItemData
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.listeners.ListenerImplOpenEditLogsOnLongClick
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.utils.Helper

/**
 * Standard means first entry is exp, second is text after filterconstraint of progressItem
 */
open class CharProgressItemAdapterStd (
    activity: ProgressBarFrameDropBoxActivity?,
    comparator: Comparator<StdEntry>,
    val cutNameAfterComma: Boolean = false,
    var split: Boolean = false,
    /**
     * do not display column header row
     */
    val hideColumnNames: Boolean = false,
    /**
     * the column which is not called name
     */
    val columnName: String = "Cost") :
    CharProgressItemAdapter<StdEntry>(
        mActivity = activity,
        comparator = comparator) {

    open val layoutResourceEntry = R.layout.char_progress_list_view_item_2_values

    override val layoutResourceMain: Int = R.layout.char_progress_item_container_2_values_2_line_header


    open fun generateAddData(e: LogEntry): StdHolder {
        val holder = StdHolder()
        val gravityCenter = e.text.contains(Global.GRAVITY_CENTER_INDICATOR)
        var textAfterFilterConstraint = updateFilterConstraintToCurrentLanguage(
            e.text.replace(Global.GRAVITY_CENTER_INDICATOR, ""))

        while (textAfterFilterConstraint.contains(item!!.filterConstraint)) {
            textAfterFilterConstraint = Helper.getTextAfterStringAndTrim(
                text = textAfterFilterConstraint, string = item?.filterConstraint
            ).trim()
            //Eg when we have @Quest Quest1 @Quest Quest2 we want to find both quests
            val textUntilNextFilterConstraint = Helper.getTextBeforeString(
                text = textAfterFilterConstraint, string = item?.filterConstraint
            ).trim()
            val textUntilEndMarker = Helper.getTextBeforeString(
                text = textUntilNextFilterConstraint, string = END_MARKER
            )

            //first entry is exp, second is text
            val entry = StdEntry()
            entry.relatedLogEntry = e
            /*
        set exp
        */if (e.exp != 0) {
                entry.left_text = e.exp.toString()
            } else {
                entry.left_text = ""
            }
            /*
        set text
         */
            entry.right_text = if (cutNameAfterComma) {
                textUntilEndMarker.substringBefore(",")
            } else {
                textUntilEndMarker
            }
            entry.gravitiyCenter = gravityCenter
            holder.entries.add(entry)
        }
        holder.logEntry = e
        return holder
    }

    override fun add(e: LogEntry): LogEntry {
        if (e.text.matches(Helper.getRegexContainsFilterConstraint(item!!.filterConstraint))) {
            val holder = generateAddData(e)
            data.addAll(holder.entries)
            return holder.logEntry!!
        }
        return e
    }

    override fun getView(index: Int, check_mistakes: Boolean): View? { //inflate entry
        val entryData = data[index]
        val entry = LayoutInflater.from(mContext).inflate(layoutResourceEntry, null, false) as LinearLayout
        val numberView = entry.findViewById<TextView>(R.id.number)
        val number2View = entry.findViewById<TextView>(R.id.number2)
        number2View.visibility = View.GONE // not needed here
        if (hideColumnNames && index == 0) {
           entry.findViewById<View>(R.id.line)?.visibility = View.GONE
        }
        val textView = entry.findViewById<TextView>(R.id.text)

        if (item?.check_mistakes == true && entryData.hasError) {
            entry.setBackgroundResource(R.color.holo_red_light_half_transparent)
        }

        header_view?.setOnLongClickListener(View.OnLongClickListener {
            mActivity?.let { activity ->
                item?.let {
                    DialogShowFilteredLogs.newInstance(
                        it.filterConstraintTranslations.joinToString(" OR "),
                        it.check_mistakes
                    ).show(
                        activity.supportFragmentManager,
                        ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS
                    )
                }
            }
            return@OnLongClickListener true
        })

        var sb = SpannableStringBuilder()

        if (!split) {
            if (entryData.left_text.isNotEmpty()) {
                Helper.append(
                    sb,
                    entryData.left_text,
                    StyleSpan(Typeface.ITALIC),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                sb.append(" ")
            }
            numberView.visibility = View.GONE
        } else {
            //DO SPLIT
            if (entryData.left_text.isNotEmpty()) {
                numberView.text = entryData.left_text
            } else {
                numberView.text = ""
            }
        }

        sb.append(entryData.right_text)

        /*
        make marked text bold and don't display bold indicators *daa*
         */
        mActivity?.let {
            val text = sb.toString()
            sb = Helper.formatText(
                activity = it,
                sb = sb,
                originalText = text,
                italic_matches = Helper.getTagsFromString(text)
            )
                .spannableStringBuilder
        }


        textView.text = sb
        //        Helper.log("STD_CHAR_PROG", "text to inflate = " + sb);
        if (entryData.gravitiyCenter) {
            textView.gravity = Gravity.CENTER
        }

        mActivity?.let { a ->
            entry.setOnLongClickListener(
                ListenerImplOpenEditLogsOnLongClick(
                fm = a.supportFragmentManager,
                e = entryData.relatedLogEntry)
            )
            true
        }
        return entry
    }

    override fun inflate(expand: Boolean): View? {
        val container = super.inflate(expand)
        renameStat0ColumnHeader(columnName, container)
        if (hideColumnNames) {
            container?.findViewById<LinearLayout>(R.id.column_names)?.visibility = View.GONE
        }

        return container
    }

    override fun generate_autocomplete_data(e: LogEntry) {}
    override fun createAutoCompleteStrings(): ArrayList<String> {
        return ArrayList<String>()
    }

    inner class StdEntry: CharProgressItemData() {
        var left_text: String = ""
        var right_text: String = ""
            set(value) {
                name = value
                field = value
            }

        var hasError = false
        var gravitiyCenter = false
        var exp //only used for Toughness to count to complete exp spent into toughness;
                = 0
        var isToughnessEntry = false
    }

    inner class StdHolder {
        var entries = ArrayList<StdEntry>()
        var logEntry: LogEntry? = null
    }

    companion object {
        var FEAT_COMPARATOR: Comparator<StdEntry> = Comparator { o1, o2 -> o1.right_text.compareTo(o2.right_text) }
        var DATE_COMPARATOR: Comparator<StdEntry> = Comparator { o1, o2 -> LogEntry.CREATION_DATE_COMPARATOR.compare(o1?.relatedLogEntry, o2?.relatedLogEntry) }
        const val END_MARKER = "//"
    }
}