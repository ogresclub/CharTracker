package com.mtths.chartracker.activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.ResultReceiver
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.GridView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.activities.ActivityMain.ProgressPublisher
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.Global
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.dialogs.DialogChangeCharacterStatus.OnCharStatusChangedListener
import com.mtths.chartracker.dialogs.DialogCharIcon.OnCharIconChangedListener
import com.mtths.chartracker.dialogs.DialogEditLogEntry.OnLogEntryEditedListener
import com.mtths.chartracker.fragments.FragmentMainScreen.OnLogEntryAddedListener
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.utils.Helper.remove
import com.mtths.chartracker.utils.Helper.rulesSr5
import com.mtths.chartracker.utils.Helper.rulesSr6
import com.mtths.chartracker.utils.Helper.update
import com.mtths.chartracker.utils.Helper.updateAllLogs
import com.mtths.chartracker.adapters.CharacterAdapter
import com.mtths.chartracker.dataclasses.LogEntry.Companion.CREATION_DATE_COMPARATOR
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.fragments.FragmentCharDetails
import com.mtths.chartracker.fragments.FragmentCharItems
import com.mtths.chartracker.fragments.FragmentCharLog
import com.mtths.chartracker.fragments.FragmentStatusMonitor
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.dialogs.FileDialog
import com.mtths.chartracker.dropbox.DropBoxIntent
import com.mtths.chartracker.fragments.FragmentSessionData
import com.mtths.chartracker.utils.Helper
import kotlinx.coroutines.*
import java.io.File
import java.util.*
import kotlin.math.max

/**
 * Created by mtths on 09.04.17.
 */
class ActivityCharDetails : ProgressBarFrameDropBoxActivity(),
    OnLogEntryEditedListener,
    OnCharIconChangedListener,
    OnCharStatusChangedListener,
    OnLogEntryAddedListener,
    FragmentStatusMonitor.OnStatusMonitorChangedListener {
    private lateinit var charDisplay: GridView
    private lateinit var charDisplayAdapter: CharacterAdapter
    var character: Character? = null
        private set
    var char_id: Long = 0
    var charLogs: ArrayList<LogEntry>? = null
    private var dbHelper: DatabaseHelper? = null
    lateinit var prefs: PrefsHelper
    //AsyncTasks
    private var mUpdateDisplaysJob: Job? = null
    private var loadCharLogsJob: Job? = null
    ////////////////////////////////////// METHODS /////////////////////////////////////////////////
    public override fun onCreate(savedInstanceState: Bundle?) {
        prefs = PrefsHelper(this)
        dbHelper = getInstance(this)
        char_id = intent.getLongExtra(PARAM_CHAR_ID, -1)
        log("received id: $char_id")
        character = dbHelper?.getCharacter(char_id)?.apply {
            //todo find another way to make sure a rule system is always available
            if (ruleSystems.isNullOrEmpty()) {
                ruleSystems = this@ActivityCharDetails.prefs.activeRuleSystems
                log("CHAR_DETAILS", "set character rules system to prefs active rule systems: ${character?.ruleSystems?.joinToString()}")
            }
            Global.cachedCharProgressItems[id]?.let {
                charProgressItemsMap = it
            }
        }
        log("CHAR_DETAILS", "character rule systemS: [${character?.ruleSystems?.joinToString()}]")
        fillFragmentOrderList()
        setThemeAccordingToRuleSystem()
        super.onCreate(savedInstanceState)
        tag = ACTIVITY_TAG
        setContentView(R.layout.activity_char_details)
        initInterfaceItems()
        implementCharViewOnClick()
        mDropBoxResultReceiver = DropBoxResultReceiver(Handler(Looper.getMainLooper()))
        //char items will be downloaded in fragmentCharItems in Create if Option is On
        loadCharLogs()
        fillChars()
    }

    private fun setEdgeToEdgeDisplay() {
        if (prefs.edgeToEdge) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            );



            if (Global.charsRelatedToSession.size > 1) {
                charDisplay.apply {

                    layoutParams = RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                    ).apply {
                        setMargins(0, getStatusBarHeight(), 0, 0)
                    }
                }
            }
        }
    }

    private fun getStatusBarHeight() = Helper.getStatusBarHeight(this) + prefs.statusBarOffsetCharDetails

    /**
     * this determines how much space is left at the top for the status bar + the char grid view
     */
    override fun getViewPagerPadding(): Int {
        return super.getViewPagerPadding() + prefs.statusBarOffsetCharDetails  + charDisplay.height + resources.getDimension(R.dimen.tiny_space).toInt()
    }

    /**
     * fill the list for REFs of the fragments from right to left
     * call super method AFTER adding, because it reverses the list
     * because the viewpager starts from left
     * (doesn't make much sense to me right now to inverse the list instead of
     * getting the correct order from the start, but probably I had a good reason for doing it...)
     */
    override fun fillFragmentOrderList() {
        fun log(s: String) = Log.d("CREATE_VIEW_PAGER_DATA", s)

        val rSList = character?.ruleSystems ?: listOf(prefs.ruleSystem)
        log("rule systems: ${rSList.joinToString { it.name }} // character rSList = ${character?.ruleSystems}, session rS = ${prefs.ruleSystem}")
        if ((character?.ruleSystems?.size ?: 0) > 1) {
            Log.e("VIEW PAGER DATA", "Character has more than one rule system: ${character?.ruleSystems?.joinToString { it.name }}")
        }


        fragmentOrderList.add(FragmentCharLog.REF)
        if (prefs.showSessionStatsCharDetails) {
            fragmentOrderList.add(FragmentSessionData.REF)
        }
        fragmentOrderList.add(FragmentCharDetails.REF)
        if (rSList.any { it.showFragmentDamageMonitor }) {
            fragmentOrderList.add(FragmentStatusMonitor.REF)
        }
        if (rSList.any { it.showFragmentCharItems }) {
            fragmentOrderList.add(FragmentCharItems.REF)
        }
        super.fillFragmentOrderList()
    }

    fun implementCharViewOnClick() {
        charDisplay.setOnItemClickListener { parent, view, position, id ->
            val clickedCharacter = parent.getItemAtPosition(position) as Character
            char_id = clickedCharacter.id
            character = clickedCharacter
                .apply {
                    if (ruleSystems.isNullOrEmpty()) {
                        ruleSystems = this@ActivityCharDetails.prefs.activeRuleSystems
                    }
                }
            loadCharLogs(downloadItems = prefs.autoDownloadCharItems)
        }
    }

    fun updateCharacterInDbAndGlobalCharsAndReload(keepLastUpdateDate: Boolean) {
        character?.let { c ->
            update(Global.chars, c)
            dbHelper?.updateCharacter(c, keep_last_update_date = keepLastUpdateDate)
            reloadCharacter(c.id)
//            Log.d("STATUS_MONITOR", "physical dmg set = " + c.physical_damage + ", stored was " + dbHelper?.getCharacter(c.id)?.physical_damage);
//            Log.d("STATUS_MONITOR", "edge set = " + c.edge + ", stored was " + dbHelper?.getCharacter(c.id)?.edge);

        }
    }

    override fun onStatusMonitorChanged() {
        character?.let { c ->
            update(Global.chars, c)
            dbHelper?.updateCharacter(c, keep_last_update_date = false)
//            Log.d("STATUS_MONITOR", "physical dmg set = " + c.physical_damage + ", stored was " + dbHelper?.getCharacter(c.id)?.physical_damage);
//            Log.d("STATUS_MONITOR", "edge set = " + c.edge + ", stored was " + dbHelper?.getCharacter(c.id)?.edge);

        }
        updateDisplays(generateCPI = false)
    }


    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        log("Started onActivityResult. requestCode = " + requestCode + "ResultCode = " + resultCode)
        data?.let {
            when (requestCode) {
                FILE_SELECT_CODE_EXPORT_CHARLOG_LOCAL_TEXT -> if (resultCode == Activity.RESULT_OK) {
                    val path = data.getStringExtra(FileDialog.RESULT_PATH)
                    log("got path. Writing charlog as text now")
                    //export CharLog
                    log("exporting charlog... ")
                    WriteCharLogToFileTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, path, "txt")
                }
                FILE_SELECT_CODE_EXPORT_CHARLOG_LOCAL_XML -> if (resultCode == Activity.RESULT_OK) {
                    val path = data.getStringExtra(FileDialog.RESULT_PATH)
                    log("got path. Writing charlog as xml now")
                    //export CharLog
                    log("exporting charlog... ")
                    WriteCharLogToFileTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, path, "xml")
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * set the background of a view to violet or red if character is dead or retired.
     */
    fun setBackgroundColorAccordingToCharStatus(v: LinearLayout?) {
        v?.let {v ->
            if (character!!.isDead && character!!.isRetired) {
                v.setBackgroundResource(R.drawable.voilet_red_gradient)
            } else if (character!!.isDead) {
                v.setBackgroundResource(R.color.holo_red_light_half_transparent)
            } else if (character!!.isRetired) {
                v.setBackgroundResource(R.color.voilet_half_transparent)
            } else {
                v.setBackgroundResource(android.R.color.transparent)
            }
        }
    }

    val dropBoxResultReceiver: ResultReceiver?
        get() = mDropBoxResultReceiver

    fun reloadCharacter(char_id: Long) {

        //reload character and copy cpiMap and rulesystems to newly loaded character
        val tempC = dbHelper?.getCharacter(char_id)?.apply {
            character?.charProgressItemsMap?.let {
                charProgressItemsMap = it
            }
            ruleSystems = character?.ruleSystems ?: this@ActivityCharDetails.prefs.activeRuleSystems
        }
        character = tempC

    }

    fun updateBackgroundColor() {
        fragmentCharDetails?.updateBackgroundColor()
        fragmentCharLog?.updateBackgroundColor()
        fragmentCharItems?.updateBackgroundColor()
        fragmentStatusMonitor?.updateBackgroundColor()
        fragmentSessionData?.updateBackgroundColor()
    }

    override fun onCharStatusChanged(character: Character,
                                     keepLastUpdateDate: Boolean,
                                     updateDisplays: Boolean) {
        update(Global.chars, character)
        update(Global.charsRelatedToSession, character)
        dbHelper?.updateCharacter(character = character, keep_last_update_date = keepLastUpdateDate)
        reloadCharacter(character.id)

        if (prefs.autoDownloadCharItems) {
            fragmentCharItems?.downloadCharItems()
        }
        if (updateDisplays) {
            updateDisplays(generateCPI = false)
        }
    }

    /**
     * this method gets called, when a character is resurrected and
     * for this reason a log Entry is created
     *
     * @param newLogEntry_id
     */
    override fun onLogEntryAdded(newLogEntry_id: Long) {
        val dbHelper = getInstance(this)
        val e = dbHelper?.getLogEntry(newLogEntry_id)
        log("newLogEntryLoaded", e!!.toString_SessionCharsExpText(this))
        Global.allLogs.add(0, e)
        charLogs?.add(0, e)
        updateDisplays(generateCPI = true)
    }

    override fun onCharIconChanged(newIcon: String, isCustomIcon: Boolean) {
        character?.let { character ->
            if (isCustomIcon) {
                character.iconFileName = newIcon
                log("ICON_STUFF", "icon path set to: ${character.iconFileName}")

                /*
                upload icon to Dropbox if auto upload is active
                 */
                if (PrefsHelper(this).autoUploadCharIcons) {
                    dbXHelper.startDropboxIntentWthEnding(
                            task = DropBoxIntent.TASK_UPLOAD,
                            mode = DropBoxIntent.MODE_CHAR_ICON,
                            fileNameWithEnding = arrayOf(character.iconFileName))
                }
            } else {
                character.iconName = newIcon
            }

            update(Global.chars, character)
            Toast.makeText(this, getString(R.string.icon_updated_notification), Toast.LENGTH_SHORT).show()
            getInstance(this)?.updateCharacter(
                character = character, keep_last_update_date = false)
            fragmentCharDetails?.updateCharIcon()

            /*
            Auto Upload Char Icon to Dropbox
             */

        }
    }

    /**
     * this methods get called, when a log Entry in Fragment charLogs gets updated
     * @param eUpdated
     */
    override fun onLogEntryUpdated(eUpdated: LogEntry) {

        lifecycleScope.launch {
            mOnLogEntryUpdated(eUpdated)
            updateDisplays(generateCPI = true)
        }
    }

    override fun onLogEntryDeleted(e: LogEntry) {
        clearNonModifiableBuffsAndUpdateCharacter()
        remove(Global.allLogs, e)
        charLogs?.let { remove(it, e) }
        updateDisplays(generateCPI = true)
    }

    /**
     * this is called after using LogAdapter.deleteLogEntries()
     */
    fun onLogEntriesDeleted(logs: List<LogEntry>?) {
        clearNonModifiableBuffsAndUpdateCharacter()
        logs?.forEach { e ->
            remove(Global.allLogs, e)
            charLogs?.let { remove(it, e) }
        }
        updateDisplays(generateCPI = true)
    }

    override fun onDestroy() {
        super.onDestroy()
        prefs.enableSchwarzgeld = false
        Global.cachedCharProgressItems.forEach { (_, value) -> value.clearViewHolders() }
    }

    /////////////////////////////////////// PRIVATE METHODS ///////////////////////////////////////
    private fun initInterfaceItems() {
        pageAdapter = MyPageAdapter(supportFragmentManager)
        pager = findViewById(R.id.viewPager)
        pager.adapter = pageAdapter
        pager.currentItem = fragmentOrderList.indexOf(FragmentCharDetails.REF)
        pager.offscreenPageLimit = 4 // this means load all pages
        charDisplay = findViewById(R.id.char_display)
        charDisplayAdapter = CharacterAdapter(
            context = this,
            layoutResource = R.layout.character_no_exp_char_details,
            thumbNailQuality = 300,
            deactivateDeadAndRetiredChars = true,
            numColsGridView = 7)
        charDisplayAdapter.noExp()
        charDisplay.adapter = charDisplayAdapter
        progressBarFrameLoading = findViewById(R.id.progress_bar_frame)
        progressBarLoading = progressBarFrameLoading.findViewById(R.id.progress_bar)
        progressBarLoadingTextView = progressBarFrameLoading.findViewById(R.id.progress_bar_text_view)
        progressBarLoading.progressDrawable = ContextCompat.getDrawable(this,
            R.drawable.my_progress_bar
        )
    }

    val fragmentCharLog: FragmentCharLog?
        get() = getViewPagerFragment(FragmentCharLog.REF) as? FragmentCharLog?

    val fragmentCharDetails: FragmentCharDetails?
        get() = getViewPagerFragment(FragmentCharDetails.REF) as? FragmentCharDetails?


    /**
     * is null when not using ruleSystem With Status Monitor
     */
    val fragmentStatusMonitor: FragmentStatusMonitor?
        get() = getViewPagerFragment(FragmentStatusMonitor.REF) as? FragmentStatusMonitor?


    /**
     * is null when not using rule System with char Items
     */
    val fragmentCharItems: FragmentCharItems?
        get() = getViewPagerFragment(FragmentCharItems.REF) as? FragmentCharItems?

    /**
     * is null when disabled in settings
     */
    val fragmentSessionData: FragmentSessionData?
        get() = getViewPagerFragment(FragmentSessionData.REF) as? FragmentSessionData?


    private fun fillChars() {
        if (Global.charsRelatedToSession.size > 1) {
            log("CHARZ", Global.charsRelatedToSession.joinToString { it.name })
            charDisplayAdapter.selected_char_id = char_id
            charDisplayAdapter.setChars(Global.charsRelatedToSession)
            charDisplay.numColumns = Global.charsRelatedToSession.size
            charDisplayAdapter.notifyDataSetChanged()
        }
        //update view pager paddings of fragments to not overlap with char display
        // if I don't use the delay, sometimes, some views are not yet initialized and the app crashes,
        // so just give it some time
        Handler(Looper.getMainLooper()).postDelayed({
            fragmentCharDetails?.setViewPagerPadding()
            fragmentCharLog?.setViewPagerPadding()
            fragmentCharItems?.setViewPagerPadding()
            fragmentStatusMonitor?.setViewPagerPadding()
            fragmentSessionData?.setViewPagerPadding()
        }, 100)

    }

    private fun loadCharLogs(downloadItems: Boolean = false) {
        loadCharLogsJob?.cancel()
        loadCharLogsJob = lifecycleScope.launch(Dispatchers.Main) {

            //LOAD CHARLOGS
            character?.let { c ->

                withContext(Dispatchers.IO) {
                    dbHelper?.let {
                        charLogs = it.getCharLogs(c.id).also {cLog ->
                            c.totalExp = cLog.sumOf { max(it.exp, 0) }
                            c.currentExp = cLog.sumOf { it.exp }
                        }
                        it.updateCharacter(c, keep_last_update_date = true)
                        update(Global.chars, c)
                        update(Global.charsRelatedToSession, c)
                    }
                    charLogs?.let { Collections.sort(it, CREATION_DATE_COMPARATOR) }

                }

                if (downloadItems) fragmentCharItems?.downloadCharItems()
                updateDisplays(generateCPI = true)
            }
        }
    }

    fun setThemeAccordingToRuleSystem() {
        character?.ruleSystem?.theme?.let {
            setTheme(it)
            activeTheme = it
        }
    }

    fun updateDisplays(generateCPI: Boolean) {

        updateBackgroundColor()

        if (mUpdateDisplaysJob?.isActive == true) {
            mUpdateDisplaysJob?.cancel()
            hideProgressBarLoading()
        }
        fragmentCharDetails?.loadCharDetailsJob?.cancel()

        mUpdateDisplaysJob = lifecycleScope.launch(Dispatchers.Main) {
            showProgressBarLoading()

            val updateCharDetailsJob = async {
                reloadCharacter(char_id)
                fillChars()
                log("CHARACTER_RULE_SYSTEM", "update display. loaded character newly from db and got rulesystem from old char: rs = ${character?.ruleSystems}")
                fragmentCharDetails?._updateDisplays(updateUI = true, generateCPI = generateCPI)
                fragmentCharItems?.updateDisplay()
                fragmentCharLog?.updateDisplay()
                fragmentStatusMonitor?.updateDisplays()
                fragmentSessionData?.fill()
                //update also filtered Logs Dialog if it is available
                val showFilteredLogs = supportFragmentManager.findFragmentByTag(
                    FRAGMENT_TAG_SHOW_FILTERED_LOGS
                ) as? DialogShowFilteredLogs?
                showFilteredLogs?.fillLogAdapter()
            }


            updateCharDetailsJob.await()
            hideProgressBarLoading()
        }
    }


    ////////////////////////////////// PRIVATE CLASSES ////////////////////////////////////////////


    override fun onResume() {
        super.onResume()
        setEdgeToEdgeDisplay()
    }

    /**
     * Handle the Results after DropBox upload or download is completed by DropBoxIntent.
     */
    inner class DropBoxResultReceiver(handler: Handler?) : ResultReceiver(handler) {

        private var progressDialog: ProgressDialog

        override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
            super.onReceiveResult(resultCode, resultData)
            if (resultCode == DropBoxIntent.UPLOAD_SUCCESSFUL) {
                progressDialog.incrementProgressBy(1)
                progressDialog.dismiss()
                Toast.makeText(applicationContext, R.string.msg_upload_successful, Toast.LENGTH_SHORT).show()
            } else if (resultCode == DropBoxIntent.UPLOAD_FAILED) {
                progressDialog.dismiss()
                Toast.makeText(applicationContext, R.string.msg_upload_failed, Toast.LENGTH_SHORT).show()
            } else if (resultCode == DropBoxIntent.IMPORT_FROM_XML) { //this is actually not needed here but I just copied it from activity Main....
            } else if (resultCode == DropBoxIntent.MAKE_TOAST) {
                Toast.makeText(this@ActivityCharDetails, resultData.getString("msg"), resultData.getInt("length")).show()
            } else if (resultCode == DropBoxIntent.CREATING_FILE_SUCCESSFUL) {
                progressDialog.setMessage(getString(R.string.msg_please_wait_while_uploading_file))
                progressDialog.incrementProgressBy(1)
            } else if (resultCode == DropBoxIntent.LOAD_INITIAL_DATA_AND_REFRESH_DISPLAYS) {
                updateDisplays(generateCPI = true)
            } else if (resultCode == DropBoxIntent.DO_PROGRESS) {
                if (resultData.getInt("value1") < 0) {
                    progressBarLoading.max = resultData.getInt("value2")
                } else {
                    progressBarLoading.progress = resultData.getInt("value1")
                }
            } else if (resultCode == DropBoxIntent.SHOW_PROGRESS_BAR) {
                progressBarFrameLoading.visibility = View.VISIBLE
            } else if (resultCode == DropBoxIntent.HIDE_PROGRESS_BAR) {
                progressBarFrameLoading.visibility = View.GONE
                progressBarLoading.progress = 0
            } else if (resultCode == DropBoxIntent.SET_PROGRESSBAR_TEXT) {
                progressBarLoadingTextView.text = resultData.getString("msg")
            }
        }

        init {
            progressDialog = ProgressDialog(ContextThemeWrapper(this@ActivityCharDetails, R.style.myDialog))
        }
    }

    /**
     * you need two arguments: path and data type (xml or txt)
     */
    private inner class WriteCharLogToFileTask : AsyncTask<String, Int?, Pair<Boolean, String>>(), ProgressPublisher {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            progressBarLoading.progress = 0
            progressBarFrameLoading.visibility = View.VISIBLE
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg strings: String): Pair<Boolean, String> {
            var path = strings[0].trim { it <= ' ' }
            val data_type = strings[1].trim()
            //put correct ending to file
            if (!(path.length > data_type.length + 1 && path.endsWith(".$data_type"))) {
                path += ".$data_type"
            }
            return when {
                data_type.equals("txt", ignoreCase = true) -> {
                    character?.let {
                        dbHelper?.writeCharLogToFileAsText(it, File(path), this)
                    } ?: Pair(false, "Error")
                }
                data_type.equals("xml", ignoreCase = true) -> {
                    val file = File(path)
                    charLogs?.let { charLogs ->
                        character?.let { c ->
                            dbHelper?.writeCharLogToFileAsXml(c, file, charLogs, this)
                        }
                    }
                    Pair(true, "Charlog exported successfully to " + file.absolutePath)
                }
                else -> {
                    Pair(true, "Charlog export failed. No Valid Option was chosen.")
                }
            }
        }

        override fun doProgress(values: Array<Int>) {
            publishProgress(*values)
        }

        override fun display(msg: String) {
            runOnUiThread { progressBarLoadingTextView.text = msg }
        }

        fun toast(msg: String?) {
            runOnUiThread { Toast.makeText(this@ActivityCharDetails, msg, Toast.LENGTH_SHORT).show() }
        }

        @Deprecated("Deprecated in Java")
        override fun onProgressUpdate(vararg values: Int?) {
            values[0]?.let {val0 ->
                if (val0 < 0) {
                    progressBarLoading.max = values[1] ?: 0
                } else {
                    progressBarLoading.progress = val0
                }
            }
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(resultPair: Pair<Boolean, String>) {
            progressBarFrameLoading.visibility = View.GONE
            progressBarLoading.progress = 0
            if (resultPair.first) {
                toast(resultPair.second)
            } else {
                toast("Exporting charlog failed")
            }
        }
    }

    fun clearNonModifiableBuffsAndUpdateCharacter() {
        character?.let {
            it.clearNonModifiableBuffs()
            getInstance(this@ActivityCharDetails)?.updateCharacter(it, keep_last_update_date = true)
            reloadCharacter(it.id)
        }
    }

    suspend fun mOnLogEntryUpdated(eUpdated: LogEntry) {

        withContext(Dispatchers.IO) {
            dbHelper?.updateLogEntry(eUpdated)
            updateAllLogs(eUpdated)
            charLogs?.let { update(it, eUpdated) }
            clearNonModifiableBuffsAndUpdateCharacter()

//            character = getInstance(this@ActivityCharDetails)?.getCharacter(char_id)
//            don't know why this should be done here, hope it doesn't break anything removing it
        }


    }


    companion object {
        const val ACTIVITY_TAG = "char_details"
        const val PARAM_CHAR_ID = "char_id"
        //AsyncLoaders Data
        const val LOADER_TASK_ON_LOG_ENTRY_UPDATED = 0
        const val REF_LOG_ID = "log_id"
        const val FILE_SELECT_CODE_EXPORT_CHARLOG_LOCAL_TEXT = 0
        const val FILE_SELECT_CODE_EXPORT_CHARLOG_LOCAL_XML = 1
        const val FRAGMENT_TAG_SHOW_FILTERED_LOGS = "DialogShowFilteredLogs"
    }
}