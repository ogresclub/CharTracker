package com.mtths.chartracker.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.HttpUtils
import okhttp3.internal.and
import java.security.SecureRandom

class ActivityLmAuth : Activity() {



    private var mAuthStateNonce: String? = null
    private var mActivityDispatchHandlerPosted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuthStateNonce = savedInstanceState?.getString(REF_AUTH_STATE_NONCE)


    }

    override fun onResume() {

        super.onResume()
        if (Build.VERSION.SDK_INT < 29) {
            // onTopResumedActivityChanged was introduced in Android 29 so we need to call it
            // manually when Android version is less than 29
            onTopResumedActivityChanged(true /* onTop */)
        }
    }

    override fun onNewIntent(intent: Intent?) {

        super.onNewIntent(intent)

        if (mAuthStateNonce == null) {
            Helper.log(DEBUG_TAG, "state was null, so finished")
            authFinished()
            return
        }

        intent?.let {

            var d = it.data
            if (!d.toString().startsWith("$URI_SCHEME://")) {
                // todo redirect url not hierarchical on server side
                // this worked some time ago, but now the // are missing somehow, even tough the
                // redirection url it lootmanager includes it in the url.
                // that why this dirty hack it needed...
                d = Uri.parse(d.toString().replace("$URI_SCHEME:", "$URI_SCHEME://"))
            }

            d?.let {


                val token = it.getQueryParameter(REF_EXTRA_TOKEN).also {
                    Helper.log(DEBUG_TAG, "token arg: $it")
                }
                val state = it.getQueryParameter(REF_EXTRA_STATE).also {
                    Helper.log(DEBUG_TAG, "state arg: $it")
                }
                val device_id = it.getQueryParameter(REF_DEVICE_ID).also {
                    Helper.log(DEBUG_TAG, "device_id arg: $it")
                }
                val prefs = PrefsHelper(this)

                if (state?.contentEquals(mAuthStateNonce) == true) {
                    if (device_id == prefs.lootmanagerDeviceId) {
                        prefs.lootmanagerToken = token
                            .also { Helper.log(DEBUG_TAG, "token has been stored $it") }
                    } else {
                        Helper.log(
                            DEBUG_TAG,
                            "finished auth without fetching token, because device_id did not match"
                        )
                    }
                } else {
                    Helper.log(
                        DEBUG_TAG,
                        "finished auth without fetching token, because state was false"
                    )
                }
                authFinished()

            }
        }
    }

    /**
     * AuthActivity is launched first time, or user didn't finish oauth/dauth flow but
     * switched back to this activity. (hit back button)
     *
     * If DAuth/Browser Auth succeeded, this flow should finish through onNewIntent()
     * instead of onResume().
     *
     * NOTE: Although Android Studio doesn't think this overrides a method, it actually overrides
     * onTopResumedActivityChanged() introduced in Android level 29.
     *
     * See:
     * https://developer.android.com/reference/android/app/Activity#onTopResumedActivityChanged(boolean)
     */
    override fun onTopResumedActivityChanged(onTop: Boolean) {
        if (isFinishing || !onTop) {
            return
        }
        val authNotFinish = mAuthStateNonce != null
        if (authNotFinish) {
            // We somehow returned to this activity without being forwarded
            // here by the official app.

            // Most commonly caused by user hitting "back" from the auth screen
            // or (if doing browser auth) task switching from auth task back to
            // this one.
            Helper.log(DEBUG_TAG, "auth finished because onTopresumedActvityChanged was called and state was not null, so auth was still ongoing")
            authFinished()
            return
        }
        if (mActivityDispatchHandlerPosted) {
            Helper.log(DEBUG_TAG, "onResume called again before Handler run")
            return
        }

        // Random entropy passed through auth makes sure we don't accept a
        // response which didn't come from our request.  Each random
        // value is only ever used once.
        val state = createStateNonce()
        Helper.log(DEBUG_TAG, "set state: $state")


        /*
         * An Android bug exists where onResume may be called twice in rapid succession.
         * As mAuthNonceState would already be set at start of the second onResume, auth would fail.
         * Empirical research has found that posting the remainder of the auth logic to a handler
         * mitigates the issue by delaying remainder of auth logic to after the
         * previously posted onResume.
         */Handler(Looper.getMainLooper()).post(Runnable {
            Helper.log(DEBUG_TAG, "running startActivity in handler")
            // Save state that indicates we started a request, only after
            // we started one successfully.
            mAuthStateNonce = state
            startAuth(state = state, device_id = PrefsHelper(this).lootmanagerDeviceId)
        })
        mActivityDispatchHandlerPosted = true
    }

    fun startAuth(state: String, device_id: String) {
        Helper.log("LOOTMANAGER", "requesting token with device_id '$device_id'")
        HttpUtils.openWebPage(
            url = "https://ogres.club/lootmanager/request-token?" +
                    "state=$state&" +
                    "device_id=$device_id&" +
                    "callbacklink=$URI_SCHEME",
            activity = this
        )

    }

    fun authFinished() {
        mAuthStateNonce = null
        finish()
    }

    private fun createStateNonce(): String {
        val NONCE_BYTES = 16 // 128 bits of randomness.
        val randomBytes = ByteArray(NONCE_BYTES)
        getSecureRandom()?.nextBytes(randomBytes)
        val sb = StringBuilder()
        sb.append("oauth2:")
        for (i in 0 until NONCE_BYTES) {
            sb.append(String.format("%02x", randomBytes[i] and 0xff))
        }
        return sb.toString()
    }

    /**
     * Sets the SecurityProvider interface to use for all AuthActivity instances.
     * If set to null (or never set at all), default `java.security` providers
     * will be used instead.
     *
     *
     *
     * You shouldn't need to use this method directly in your app.  Instead,
     * simply configure `java.security`'s providers to match your preferences.
     *
     *
     * @param prov the new `SecurityProvider` interface.
     */
    private fun getSecurityProvider(): SecurityProvider {
        synchronized(sSecurityProviderLock) { return sSecurityProvider }
    }

    private fun getSecureRandom(): SecureRandom? {
        val prov = getSecurityProvider()
        return if (null != prov) {
            prov.secureRandom
        } else SecureRandom()
    }

    /**
     * Provider of the local security needs of an AuthActivity.
     *
     *
     *
     * You shouldn't need to use this class directly in your app.  Instead,
     * simply configure `java.security`'s providers to match your preferences.
     *
     */
    interface SecurityProvider {
        /**
         * Gets a SecureRandom implementation for use during authentication.
         */
        val secureRandom: SecureRandom?
    }

    // Class-level state used to replace the default SecureRandom implementation
    // if desired.
    private val sSecurityProvider: SecurityProvider = object : SecurityProvider {
        override val secureRandom: SecureRandom?
            get() = SecureRandom()
    }
    private val sSecurityProviderLock = Any()



    companion object {
        const val URI_SCHEME = "lootmanagerauth"
        const val REF_EXTRA_TOKEN = "token"
        const val REF_EXTRA_STATE = "state"
        const val REF_DEVICE_ID = "device_id"
        const val REF_AUTH_STATE_NONCE = "auth_state_nonce"
        const val DEBUG_TAG = "LOOTMANAGER_AUTH"

        fun makeIntent(context: Context): Intent {
            return Intent(context, ActivityLmAuth::class.java)
        }
    }
}