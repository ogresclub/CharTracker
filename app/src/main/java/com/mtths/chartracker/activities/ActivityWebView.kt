package com.mtths.chartracker.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import com.mtths.chartracker.R

class ActivityWebView : Activity() {

    lateinit private var browser: WebView
    lateinit private var field: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        browser = findViewById(R.id.web_view)
        field = findViewById(R.id.url_field)
        intent.getStringExtra(EXTRA_URL)?.let {
            field.setText(it) }
        browser.webViewClient = MyBrowser()
        open(field.text.toString())
    }

    private inner class MyBrowser : WebViewClient() {

        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            val uri = request?.url
            val scheme = uri?.scheme
            Log.d("OPEN_URL", "uri = $uri, scheme ${uri?.scheme}")
            if (scheme.toString().startsWith("http")) {
                return false
            }

            Log.d("OPEN_URL", "url ist custom scheme starting intent: scheme = $scheme")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            finish()
            this@ActivityWebView.startActivity(intent)
            return true
        }
    }

    fun open(url: String) {
        browser.settings.loadsImagesAutomatically = true
        browser.settings.javaScriptEnabled = true
        browser.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        browser.loadUrl(url)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return true
    }


    companion object {
        const val EXTRA_URL = "extra_url"
    }
}