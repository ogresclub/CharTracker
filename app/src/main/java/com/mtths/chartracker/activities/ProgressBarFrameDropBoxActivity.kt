package com.mtths.chartracker.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.ResultReceiver
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.mtths.chartracker.dropbox.DbxHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.dialogs.DialogNewChar
import com.mtths.chartracker.fragments.FragmentCharDetails
import com.mtths.chartracker.fragments.FragmentCharItems
import com.mtths.chartracker.fragments.FragmentCharLog
import com.mtths.chartracker.fragments.FragmentCharacterView
import com.mtths.chartracker.fragments.FragmentLogView
import com.mtths.chartracker.fragments.FragmentLogViewMainActivity
import com.mtths.chartracker.fragments.FragmentMainScreen
import com.mtths.chartracker.fragments.FragmentSessionData
import com.mtths.chartracker.fragments.FragmentStatusMonitor
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.CharProgressItemMap
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.log


/**
 * don't forget to init the progressbar or otherwise don't use the methods.
 * don't forget to instantiate the Dropbox Result Receiver and
 * also build a custom DropBox Result Receiver for whatever you want to do with the Dropbox
 * also implement setViewPagerPadding to get the correct padding
 *
 * This is a ProgressbarFrameActivity and also a Dropbox Activity
 */
@SuppressLint("Registered")
open class ProgressBarFrameDropBoxActivity : BaseFragmentActivity(), DialogNewChar.OnLoadingListener {
    lateinit var progressBarFrameLoading: FrameLayout
    lateinit var progressBarLoadingTextView: TextView
    lateinit var progressBarLoading: ProgressBar
    lateinit var dbXHelper: DbxHelper

    /**
     * this is stored when theme is set to check if it changed
     */
    var activeTheme: Int = R.style.AppTheme // default theme

    /**
     * don't forget to initialize the viewpager
     * and set the adapter
     */
    lateinit var pager: ViewPager
    var pageAdapter = MyPageAdapter(supportFragmentManager)


    private fun recreateOnThemeChange() {
        val ruleSystem = PrefsHelper(this).ruleSystem
        if (activeTheme != ruleSystem.theme) {
            showProgressBarLoading()
            //update language also to not need to recreate twice
            updateBaseContextLocale(this)
            log("recreating activity because theme changed: $activeTheme -> ${ruleSystem.theme} (${ruleSystem.name})")
            recreate()
        }
    }


    //////////////////////////////  VIEW PAGER ///////////////////////////////////

    val fragmentOrderList: MutableList<String> = mutableListOf()

    fun getViewPagerFragment(fragmentRef: String): Fragment? {
        val fm = supportFragmentManager
        val ind = fragmentOrderList.indexOf(fragmentRef)
        return fm.findFragmentByTag("android:switcher:" + R.id.viewPager.toString() + ":" + ind)
    }

    open fun fillFragmentOrderList() {
        fragmentOrderList.reverse()
        log("fragmentOrderList for view pager: ${fragmentOrderList.joinToString()}")

    }


        /**
     * if you use a view pager and want some sort of header which is the same for alle pages
     * but the background comes from the viewpager and changes for each page
     * then set the padding of the viewpager view o make ui elements show below the header
     */
    open fun getViewPagerPadding(): Int {
        return if (PrefsHelper(this).edgeToEdge) Helper.getStatusBarHeight(this) else 0
    }

    inner class MyPageAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
//            log("VIEW_PAGER", "getItem: position $position, count $count, fragmentOderList = ${fragmentOrderList.joinToString()}")

            return when (fragmentOrderList.getOrElse(position) { FragmentCharDetails.REF }) {
                FragmentCharDetails.REF -> FragmentCharDetails.newInstance(position)
                FragmentCharLog.REF -> FragmentCharLog.newInstance(position)
                FragmentStatusMonitor.REF -> FragmentStatusMonitor.newInstance(position)
                FragmentCharItems.REF -> FragmentCharItems.newInstance(position)
                FragmentSessionData.REF -> FragmentSessionData.newInstance(position)
                FragmentMainScreen.REF -> FragmentMainScreen.newInstance(position)
                FragmentCharacterView.REF -> FragmentCharacterView.newInstance(position)
                FragmentLogViewMainActivity.REF -> FragmentLogViewMainActivity.newInstance(position)
                else -> FragmentCharItems.newInstance(position)
            }
        }

        override fun getCount(): Int {
            return fragmentOrderList.size

        }
    }

    ////////////////////////////// DROPBOX ///////////////////////////////////////

    var mDropBoxResultReceiver: ResultReceiver?
        get() = dbXHelper.mDropBoxResultReceiver
        set(value) {dbXHelper.mDropBoxResultReceiver = value}



    var charProgressItemsMap = CharProgressItemMap()

    //////////////////////////////////////////////////////////////////////

    var numOfLoadings = 0
    var tag //to be defined by child class
            : String? = null
        protected set

    fun showProgressBarLoading(msg: String = resources.getString(R.string.loading_all_caps)) {
        numOfLoadings++
        log("PROGRESS_BAR", "START: num of loadings = $numOfLoadings")
        if (numOfLoadings == 1) { // this means that there is no other loading progess going on which might need the progress
            progressBarLoading.progress = 0
        }
        progressBarFrameLoading.visibility = View.VISIBLE
        progressBarLoadingTextView.text = msg
    }

    fun hideProgressBarLoading() {
        if (numOfLoadings > 0) numOfLoadings--
        log("PROGRESS_BAR", "END: num of loadings = $numOfLoadings")
        if (numOfLoadings == 0) {
            progressBarFrameLoading.visibility = View.GONE
            progressBarLoadingTextView.text = resources.getString(R.string.loading_all_caps)
        }
    }

    override fun onStartLoading() {
        showProgressBarLoading()
    }

    override fun onEndLoading() {
        hideProgressBarLoading()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        dbXHelper = DbxHelper(this)
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        recreateOnThemeChange()
        super.onResume()
        dbXHelper.onResume()
    }




}