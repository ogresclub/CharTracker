package com.mtths.chartracker.activities

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView.OnItemLongClickListener
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.Global
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.dialogs.DialogEditSession.OnSessionEditedListener
import com.mtths.chartracker.dialogs.DialogNewSession.OnNewSessionCreatedListener
import com.mtths.chartracker.utils.Helper.arrayContainsElement
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.adapters.SessionAdapter
import com.mtths.chartracker.dataclasses.Session
import com.mtths.chartracker.dialogs.DialogEditSession
import com.mtths.chartracker.dialogs.DialogNewSession
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class ActivitySessionManager : BaseFragmentActivity(), OnSessionEditedListener, OnNewSessionCreatedListener {
    //////////////////////////////////////////// VARIABLES /////////////////////////////////////////
    private lateinit var sessionList: ListView
    private lateinit var newSessionButton: TextView
    private lateinit var okButton: TextView
    private lateinit var adapter: SessionAdapter
    private var sessions = ArrayList<Session>()
    private var dbHelper: DatabaseHelper? = null
    private lateinit var prefs: PrefsHelper
    private var loadAllSessionsTask: LoadAllSessionsTask? = null

    /////////////////////////////// INTERFACES ////////////////////////////////////////////////////
    override fun onSessionDeleted(s: Session) {
        Toast.makeText(this, "Session deleted", Toast.LENGTH_SHORT).show()

        if (s.isActive) {
            while (Global.activeSessionIds.remove(s.id)) {
                continue
            }
        }
        //        Global.allSessionIds.remove(s.id);
        dbHelper?.getAllLogs()?.let { Global.allLogs = it }
        fillAdapter()
    }

    /**
     * update session name in sessionList
     */
    override fun onSessionEdited(editedSession: Session) {
        fillAdapter()
    }

    override fun onNewSessionCreated(session_id: Long) {
        Global.activeSessionIds.add(session_id)
        fillAdapter()
    }

    //////////////////////////////////// METHODS ///////////////////////////////////////////////////
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session_manager)
        setFinishOnTouchOutside(false)
        //log("IS_SHOWING BEFORE SETTING TO TRUE = $IS_SHOWING")
        IS_SHOWING = true
        dbHelper = getInstance(this)
        prefs = PrefsHelper(this)
        initInterfaceItems()
        implementSessionActivityToggled()
        implementOpenEditSessionDialog()
        implementOpenCreateNewSessionDialog()
        implementOkButton()
        initAdapter()
        fillAdapter()
    }

    override fun onResume() {
        super.onResume()
        fillAdapter()
    }

    override fun onStop() {
        super.onStop()
        IS_SHOWING = false
    }

    //////////////////////////////////// PRIVATE METHODS //////////////////////////////////////////
    private fun initInterfaceItems() {
        sessionList = findViewById<View>(R.id.session_listView) as ListView
        newSessionButton = findViewById<View>(R.id.new_session_button) as TextView
        okButton = findViewById<View>(R.id.ok_button) as TextView
    }

    private fun initAdapter() {
        adapter = SessionAdapter(this, R.layout.list_view_item_session)
        sessionList.adapter = adapter
    }

    private fun implementOkButton() {
        okButton.setOnClickListener {
            if (dbHelper?.getActiveSessionNames()?.size == 0) {
                Toast.makeText(this@ActivitySessionManager, "Please choose at least on session", Toast.LENGTH_SHORT).show()
            } else {
                log("ZZZ", "setcharactivityaccToSession")
                CoroutineScope(Dispatchers.Main).launch {
                    setActivityOfCharsAccordingToActiveSessionsTask(context = this@ActivitySessionManager)
                    this@ActivitySessionManager.finish()
                }
            }
        }
    }

    private fun implementOpenCreateNewSessionDialog() {
        newSessionButton.setOnClickListener { DialogNewSession.newInstance().show(supportFragmentManager, "createNewSession") }
    }

    private fun implementSessionActivityToggled() {
        sessionList.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val s = parent.getItemAtPosition(position) as Session
            s.toggleActivity()
            dbHelper?.updateSession(s)
            log(DEBUG_TAG_SESSION, "old active session ids: " + Global.activeSessionIds)
            if (s.isActive) {
                view.setBackgroundResource(R.color.holo_green_light_half_transparent)
                //add to activeSessionIds if it's not in there anyway (which should not be the case)
                if (!arrayContainsElement(Global.activeSessionIds, s.id)) {
                    Global.activeSessionIds.add(s.id)
                }
            } else {
                view.setBackgroundResource(R.color.light_grey_half_transparent)
                /*
                remove session from loadedCharDetails session
                the loop remove it even if the id was in there many times
                which should not happen, but who knows ;)
                */
                var successfullyRemovedSession = Global.activeSessionIds.remove(s.id)
                while (successfullyRemovedSession) {
                    successfullyRemovedSession = Global.activeSessionIds.remove(s.id)
                }
            }
            log(DEBUG_TAG_SESSION,"changed active session ids: " + Global.activeSessionIds)
        }
    }

    private fun implementOpenEditSessionDialog() {
        sessionList.onItemLongClickListener = OnItemLongClickListener { parent, view, position, _ ->
            val se = parent.getItemAtPosition(position) as Session
            log("Session " + se.name + " transmitted with id: " + se.id)
            DialogEditSession.newInstance(se.id).show(
                supportFragmentManager, "edit_session_dialog")
            true
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        loadAllSessionsTask?.cancel(true)
    }
    private fun fillAdapter() {

        loadAllSessionsTask?.cancel(true)
        loadAllSessionsTask = LoadAllSessionsTask()
        loadAllSessionsTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    private inner class LoadAllSessionsTask : AsyncTask<Void, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            sessions.clear()
            adapter.clear()
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void): Void? {
            sessions = dbHelper!!.allSessions
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            adapter.sessions = sessions
            adapter.notifyDataSetChanged()
        }
    }

    companion object {
        var IS_SHOWING = false
        const val DEBUG_TAG_SESSION = "SESSIONS__"

        suspend fun setActivityOfCharsAccordingToActiveSessionsTask(context: Context) {
            Global.chars.clear()
            Global.charsRelatedToSession.clear()

            withContext(Dispatchers.Default) {
                val dbh = getInstance(context)

                dbh?.let { dbh ->

                    dbh.getCharsRelatedToSessions(dbh.getInActiveSessionsIds(), show_all_chars = true).let { charsRelatedToInactiveSession ->
                        log("ZZZ", "character will be set to inactive because of session: ${charsRelatedToInactiveSession.joinToString {it.name}}")
                        charsRelatedToInactiveSession.forEach { inactiveChar ->
                            inactiveChar.isActive = false
                            dbh.updateCharacter(
                                character = inactiveChar,
                                keep_last_update_date = true)

                        }

                    }

                    dbh.getCharsRelatedToSessions(dbh.getActiveSessionsIds(), show_all_chars = true).let { charsRelatedToActiveSession ->

                        charsRelatedToActiveSession.forEach {
                            if (!(it.isDeleted || it.isRetired || it.isDead || it.priority < 0)) {
                                it.isActive = true
                                log(
                                    "ZZZ",
                                    "Found char supposed to be active: ${it.name} // ${it.isActive}"
                                )
                            } else {
                                it.isActive = false
                                log(
                                    "ZZZ",
                                    "Found char supposed to be inactive because of status: ${it.name} // active: ${it.isActive}, dead: ${it.isDead}, retired: ${it.isRetired}, priority: ${it.priority}"
                                )
                            }

                            dbh.updateCharacter(character = it, keep_last_update_date = true)
                        }

                    }

                    Global.chars = dbh.allCharacters
                }
            }

        }
    }

}