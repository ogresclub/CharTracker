package com.mtths.chartracker.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.fragment.app.FragmentActivity
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.dialogs.DialogDataOptions
import com.mtths.chartracker.utils.FileUtils
import com.mtths.chartracker.utils.Helper

class ActivityRecoverData : FragmentActivity(), DialogDataOptions.OnDataImportedListener {

    lateinit var prefs : PrefsHelper
    lateinit var dbHelper : DatabaseHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_recover_data)
        dbHelper = DatabaseHelper(this)
        prefs = PrefsHelper(this)
        DialogCustom.getAlterDialogBuilder(this).apply {
            setTitle(R.string.label_export_database)
            setMessage(R.string.msg_data_recovery)
            setPositiveButton(R.string.ok)  { _, _ ->
                DialogDataOptions.newInstance(
                    dataRecovery = true
                ).show(
                    supportFragmentManager, DialogDataOptions.FRAGMENT_TAG
                )
            }
        }.show()
    }

    override fun onDataImported() {
        //only need for data DialogDataOptions to work
    }

    private fun takePersistentUriPermission(uri: Uri) {
        FileUtils.takePersistentUriPermission(
            uri = uri,
            applicationContext = applicationContext)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val DEBUG_TAG = "ON_ACTIVITY_RESULT"
        Helper.log(
            DEBUG_TAG, "Started onActivityResult. " +
                    "requestCode = " + requestCode +
                    " ResultCode = " + resultCode +
                    " data = $data"
        )
        data?.let {
            when (requestCode) {
                DialogDataOptions.FILE_SELECT_CODE_DB_EXPORT -> if (resultCode == Activity.RESULT_OK) {
                    data.data?.let { folderUri ->
                        FileUtils.getCharTrackerFolder(
                            folderUri = folderUri,
                            context = this
                        )?.let { documentFile ->
                            dbHelper.exportDbAndNotifySuccess(
                                documentFile, DEBUG_TAG = "RECOVER_DATABASE"
                            )
                        }
                        finish()

                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}