package com.mtths.chartracker.activities

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.ResultReceiver
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.mtths.chartracker.*
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.DatabaseHelper.XmlImportResult
import com.mtths.chartracker.dataclasses.*
import com.mtths.chartracker.dialogs.*
import com.mtths.chartracker.dropbox.DbxHelper.Companion.DROPBOX_DEBUG_TAG
import com.mtths.chartracker.dropbox.DropBoxIntent
import com.mtths.chartracker.dropbox.DropboxClientFactory
import com.mtths.chartracker.fragments.FragmentCharacterView
import com.mtths.chartracker.fragments.FragmentLogView
import com.mtths.chartracker.fragments.FragmentLogViewMainActivity
import com.mtths.chartracker.fragments.FragmentMainScreen
import com.mtths.chartracker.fragments.FragmentSessionData
import com.mtths.chartracker.listeners.ListenerGeneralImplOnLogEntryAdded
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.preferences.PrefsHelper.Companion.CRAWL_OGRES_CLUB_ON_EACH_START
import com.mtths.chartracker.utils.FileUtils
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.add
import com.mtths.chartracker.utils.Helper.createCharProgressWords
import com.mtths.chartracker.utils.Helper.deleteStoredRuleSystemsForCharsRelatedToLog
import com.mtths.chartracker.utils.Helper.filterByActiveSessions
import com.mtths.chartracker.utils.Helper.filterByChars
import com.mtths.chartracker.utils.Helper.getActiveChars
import com.mtths.chartracker.utils.Helper.getSortedTagNamesWithHashTagFromLogEntries
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.utils.Helper.remove
import com.mtths.chartracker.utils.Helper.toastXmlImportSummery
import com.mtths.chartracker.utils.Helper.update
import com.mtths.chartracker.utils.Helper.updateAllLogs
import com.mtths.chartracker.utils.HttpUtils
import com.mtths.chartracker.utils.OgresClubCrawler
import com.mtths.chartracker.utils.OgresClubCrawler.OnFinishedCrawlingOgresClubListener
import kotlinx.coroutines.*
import java.io.File
import java.lang.Runnable
import java.util.Collections


/**
 * This Activity is use to swipe between two Fragments.
 * In this case tha FragmentMainScreen and FragmentLogView.
 */

open class ActivityMain: ProgressBarFrameDropBoxActivity(),
    FragmentMainScreen.OnCharActivityChangedListener,
    FragmentMainScreen.OnLogEntryAddedListener,
    FragmentLogView.OnSessionsChangedListener,
    DialogNewChar.OnNewCharCreatedListener,
    DialogNewSession.OnNewSessionCreatedListener,
    DialogDataOptions.OnDataImportedListener,
    DialogEditLogEntry.OnLogEntryEditedListener,
    DialogCharIcon.OnCharIconChangedListener,
    FragmentMainScreen.OnDemandAllDataListener,
    FragmentMainScreen.OnSessionStatsLoadedListener {

    companion object {

        const val ACTIVITY_TAG = "main"
        const val FRAGMENT_TAG_SHOW_SETTINGS = "show_settings"
        const val DEBUG_CRAWLING_OGRES_CLUB = "crawling_ogres_club"
        const val LOAD_INIT_DATA_DEBUG_TAG = "LOAD_INIT_DATA"

        const val PARAM_VIEW_PAGER_INDEX = "view_page_index"
        const val KEY_PLEASE_CREATE_SESSION_PROMPT_ACTIVE = "please_create_session_prompt_active"

    }


    //////////////////////////////////////// VARIABLES ////////////////////////////////////////////

    lateinit var sessionDisplay: TextView

    /**
     * containing the session display and settings and help...
     */
    lateinit var header: RelativeLayout
    lateinit var dbHelper: DatabaseHelper
    lateinit var prefs: PrefsHelper
    var numOfTasksInitLoadingData = 0
    val listOfProgressBars = ArrayList<ProgressBarData>()


    var mLoadLogsTask: LoadLogsTask? = null
    var mLoadCharsTask: LoadCharsTask? = null
    var mLoadSessionsTask: LoadSessionsTask? = null
    var mOgresClubCrawler: OgresClubCrawler? = null
    var mLoadFeatsFromResourcesTask: LoadFeatsFromResourcesTask? = null
    var mExportToXmlTask: ExportToXmlTask? = null
    var filterLogsAndUpdateLogDisplaysTask: FilterLogsAndUpdateGlobalLogDisplaysTask? = null


    lateinit var progressBarFrameSpecial: FrameLayout
    lateinit var progressBarSpecialTextView: TextView
    lateinit var progressBarSpecial: ProgressBar
    lateinit var settings: ImageView
    lateinit var help: ImageView
    lateinit var progressBars: LinearLayout
    lateinit var button_data_options: TextView

    var PLEASE_CREATE_SESSION_PROMPT_ACTIVE = false

    val selectCharTrackerFolderToActivateDBBackupOnStart =
        registerForActivityResult(ActivityResultContracts.OpenDocumentTree()) { uri: Uri? ->
            if (uri != null) {
                takePersistentUriPermission(uri)
            }
            prefs.backupDatabaseOnAppStart = true
        }


    lateinit var progressDialog: MyProgressDialog


    /*
    When Char Button gets pressed, only after 1 second the display should be refreshed to give you
    time to toggle a whole bunch of characters without having to wait to load the log entries
    each time (it take long ...)
    */
    val reloadAfterCharToggeledTimer = 1000
    val refreshAfterCharToggeledHandler = Handler(Looper.getMainLooper())
    var refreshGlobalDisplaysRunnable: Runnable = Runnable {
        updateGlobalLogDisplays(filterSessions = false, loadSessionDetails = false)
    }


    /////////////////////////////////// METHODS ////////////////////////////////////////////////////


    override fun fillFragmentOrderList() {
        fragmentOrderList.add(FragmentLogViewMainActivity.REF)
        fragmentOrderList.add(FragmentMainScreen.REF)
        if (prefs.showSessionStatsMainScreen) {
            fragmentOrderList.add(FragmentSessionData.REF)
        }
        fragmentOrderList.add(FragmentCharacterView.REF)
        super.fillFragmentOrderList()
    }

    /////////////////////////////////// METHODS ////////////////////////////////////////////////////

    override fun fillSessionStats() {
        getSessionDataDisplay()?.fill()
    }


    override fun onCharIconChanged(newIconName: String, isCustomIcon: Boolean) {
        val fragment = supportFragmentManager.findFragmentByTag("newChar")
        if (fragment is DialogNewChar) {
            fragment.setIcon(newIconName)
        }
    }

    fun launchJobClearNonModifiableBuffsAndUpdateCharacter() {
        lifecycleScope.launch(Dispatchers.IO) {
            Global.chars.forEach {
                it.clearNonModifiableBuffs()
                dbHelper.updateCharacter(it, keep_last_update_date = true)
            }
        }
    }

    override fun onLogEntryUpdated(eUpdated: LogEntry) {
        lifecycleScope.launch {
            mObLogEntryUpdated(eUpdated)
            updateGlobalLogDisplays(filterSessions = false, filterChars = false)
        }
        launchJobClearNonModifiableBuffsAndUpdateCharacter()

    }

    suspend fun mObLogEntryUpdated(eUpdated: LogEntry) {
        withContext(Dispatchers.IO) {
            deleteStoredRuleSystemsForCharsRelatedToLog(eUpdated, dbHelper)
            dbHelper.updateLogEntry(eUpdated)
            updateAllLogs(eUpdated)
        }
    }


    override fun onLogEntryDeleted(e: LogEntry) {
        launchJobClearNonModifiableBuffsAndUpdateCharacter()
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                remove(Global.allLogs, e)
                remove(Global.logsFilteredBySession, e)
                remove(Global.logsToShow, e)
                deleteStoredRuleSystemsForCharsRelatedToLog(e, dbHelper)
            }
            updateGlobalLogDisplays(filterSessions = false, filterChars = false)
        }
    }

    override fun onDataImported() {
        updateGlobalLogDisplays(filterSessions = true)
    }

    override fun onNewSessionCreated(session_id: Long) {
        Global.activeSessionIds.add(session_id)
        updateSessionDisplay()
        setBackgroundsAccordingToRuleSystem()
    }

    override fun onNewCharCreated(newChar: Character, initialLogs: ArrayList<LogEntry>) {

        add(Global.chars, newChar)
        Global.allLogs.addAll(initialLogs)
        Global.logsFilteredBySession.addAll(initialLogs)
        Global.logsToShow.addAll(initialLogs)
        updateGlobalLogDisplays(filterSessions = false, filterChars = false)
        /*
       upload icon to Dropbox if auto upload is active
        */
        if (prefs.autoUploadCharIcons && newChar.iconFileName.isNotEmpty()) {
            dbXHelper.startDropboxIntentWthEnding(
                task = DropBoxIntent.TASK_UPLOAD,
                mode = DropBoxIntent.MODE_CHAR_ICON,
                fileNameWithEnding = arrayOf(newChar.iconFileName))
        }
    }

    override fun onLogEntryAdded(newLogEntry_id: Long) {
        ListenerGeneralImplOnLogEntryAdded(this).generalOnLogEntryAdded(newLogEntry_id)
        updateGlobalLogDisplays(filterSessions = false, filterChars = false)
    }


    override fun onCharStatsChanged(changedChar: Character, loadSessionDetails: Boolean) {
        killRefreshDisplayTimer()
        update(Global.chars, changedChar)
        refreshAfterCharToggeledHandler.postDelayed(
            refreshGlobalDisplaysRunnable,
            reloadAfterCharToggeledTimer
                .toLong())
    }

    override fun onCharFilterOptionsToggled() {
        updateGlobalLogDisplays(filterSessions = false, loadSessionDetails = false)
        updateSessionDisplay()
    }

    override fun onActiveSessionsChanged() {
        updateGlobalLogDisplays(filterSessions = true)
        updateSessionDisplay()
    }

    override fun onShowAllSessionsToggled() {
        updateGlobalLogDisplays(filterSessions = false)
        updateSessionDisplay()
    }

    override fun onDemandAllData() {
        getMainScreen()?.fillHistoryAdapter()
        getLogView()?.fragmentLogView?.completeFillLogAdapter()
    }

    fun setThemeAccordingToRuleSystem() {
        prefs.activeRuleSystems.firstOrNull()?.let {rs ->
            activeTheme = rs.theme
            setTheme(rs.theme.also {
                log("RULE_SYSTEM", "set theme for ${rs.name}: $it")
            })
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        tag = ACTIVITY_TAG
        prefs = PrefsHelper(this)
        dbHelper = getInstance(this)!!
        fillFragmentOrderList()
        super.onCreate(savedInstanceState)

        autoBackupDatabase()


        installSplashScreen()
        setThemeAccordingToRuleSystem()
        setContentView(R.layout.activity_main)

        try {
            Class.forName("dalvik.system.CloseGuard")
                .getMethod("setEnabled", Boolean::class.javaPrimitiveType)
                .invoke(null, true)
        } catch (e: ReflectiveOperationException) {
            throw RuntimeException(e)
        }

        //deactivate logging except for debug-mode
        if (!BuildConfig.DEBUG) {
            Global.logging = false
        }
        //needs to be before load initial Data from Db and Update Global Displays because this one uses progressbar
        initInterfaceItems()
        progressDialog = MyProgressDialog.newInstance(this)
        mDropBoxResultReceiver = DropBoxResultReceiver(Handler(Looper.getMainLooper()))
        LoadSkillGroupsFromResources().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        loadInitialDataFromDbAndUpdateGlobalDisplays()
        implementOpenSessionManager()
        crawlOgresClubOrLoadDataFromDb()
        log("BBB", "initial #progressBars: ${progressBars.childCount}")
    }

    private fun setEdgeToEdgeDisplay() {
        if (prefs.edgeToEdge) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            );


            header.layoutParams = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                setMargins(0, getStatusBarHeight(), 0, 0)
            }
        }

        sessionDisplay.layoutParams = LinearLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            getSessionDisplayHeight()
        )
        getMainScreen()?.setViewPagerPadding()
        getCharacterView()?.setViewPagerPadding()
        getSessionDataDisplay()?.setViewPagerPadding()
        getLogView()?.setViewPagerPadding()
    }

    private fun getStatusBarHeight(): Int = Helper.getStatusBarHeight(this) +
            prefs.statusBarOffsetMainActivity

    private fun getSessionDisplayHeight(): Int = (resources.getDimension(R.dimen.session_display_height) +
            prefs.sessionDisplayHeightCorrection).toInt()

    /**
     * this determines how much space is left at the top for the statusbar + the session display
     */
    override fun getViewPagerPadding(): Int {

        return super.getViewPagerPadding() + prefs.statusBarOffsetMainActivity + getSessionDisplayHeight() + resources.getDimension(R.dimen.small_space).toInt()
    }


    private fun crawlOgresClubOrLoadDataFromDb() {

        lifecycleScope.launch(context = Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                val rSOgresClub = Helper.activeRules(
                    allowedRules = listOf(RuleSystem.rule_system_ogres_club),
                    activeRuleSystems = dbHelper.activeSessions.map { it.ruleSystem }
                )
                val doCrawl =  rSOgresClub && (prefs.crawlOgresClub == CRAWL_OGRES_CLUB_ON_EACH_START ||
                        !dbHelper.featsExist)
                if (doCrawl) {

                    crawlOgresClubForData()


                } else {

                    //load data from db
                    dbHelper.let {
                        Global.featsOnOgresClub = it.allFeats
                        Global.classesFromOgresClub = it.allClasses
                        Global.archetypesFromOgresClub = it.allArchetypes
                        Global.racesFromOgresClub = it.allRaces
                        Global.subracesFromOgresClub = it.allSubraces
                    }

                }
            }
        }
    }

    override fun onResume() {
        log("onResume called")
        super.onResume()
        log("settings: filter chars: " + prefs.filterChars + " filter sessions: " + prefs.filterSessions)
        makeSureAtLeastOneSessionIsActive()
        updateSessionDisplay()
        setEdgeToEdgeDisplay()
        updateGlobalLogDisplays(filterSessions = true)
        loadRuleSystemRelatedData()

    }

    override fun onPause() {
        super.onPause()
        /*
        to prevent those from leaking
         */

        progressDialog.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        mLoadCharsTask?.cancel(true)
        mLoadCharsTask = null
        mLoadLogsTask?.cancel(true)
        mLoadLogsTask = null
        mLoadSessionsTask?.cancel(true)
        mLoadSessionsTask = null
        mLoadFeatsFromResourcesTask?.cancel(true)
        mLoadFeatsFromResourcesTask = null
    }

    /**
     * recreate charProgressItems with given header in Global.charProgressItems
     */
    fun recreateCharProgressItems(
        itemsData:List<ItemHeaderAndCreateFunction>,
        character: Character? = null) {
        for (itemData in itemsData) {
            if (PrefsHelper(this).activeRuleSystems
                    .any { itemData.doUseWithRules[it] == true }
            ) {

                charProgressItemsMap[itemData.filterConstraintId] =
                    itemData.createNew(this, character, mutableListOf())
                charProgressItemsMap.updateStringData(this)
            }
        }
    }

    fun autoBackupDatabase() {
        //export DB, so data isn't lost when user is too lazy to export
        prefs.localCharTrackerFolder?.let {
            if (prefs.backupDatabaseOnAppStart) {

                dbHelper?.exportDbAndNotifySuccess(ctDocumentFile = it, DEBUG_TAG = "BACKUP_DB")

            }
        }
    }

    fun createCharProgressItems(character: Character? = null) {
        charProgressItemsMap = CharProgressItem.createCharProgressItems(activity = this, character = character)
    }

    fun importDatabase(originUri: Uri) {
        if (dbHelper?.importDatabase(originUri) == true) {
            onDataImported()
            Toast.makeText(this,
                "Database successfully imported from '${FileUtils.getLastPathSegmentNoType(originUri)}'",
                Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Database import failed.", Toast.LENGTH_SHORT).show()
        }
        loadInitialDataFromDbAndUpdateGlobalDisplays()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val DEBUG_TAG = "ON_ACTIVITY_RESULT"
        log(DEBUG_TAG,"Started onActivityResult. " +
                "requestCode = " + requestCode +
                " ResultCode = " + resultCode +
                " data = $data")
        data?.let {
            when (requestCode) {
                DialogDataOptions.FILE_SELECT_CODE_DB_IMPORT -> if (resultCode == Activity.RESULT_OK) {
                    data.data?.let { uri ->
                        importDatabase(uri)
                    }
                }
                DialogDataOptions.FILE_SELECT_CODE_XML_IMPORT_LOCAL -> if (resultCode == Activity.RESULT_OK) {
                    data.data?.let { uri ->
                        /*
                         * import xml and toast feedback
                         */
                        confirmDeleteDbAndStartImportXmlTask(
                            ImportXmlParam(
                                xmlLocalUris = listOf(uri),
                                import_option = prefs.xmlImport,
                                local = true
                            )
                        )
                    }
                }
                DialogDataOptions.FILE_SELECT_CODE_XML_EXPORT_LOCAL -> {
                    Log.d(DEBUG_TAG, "result for XML Export")
                    if (resultCode == Activity.RESULT_OK) {
                        data.data?.let { folderUri ->
                            takePersistentUriPermission(folderUri)
                            prefs.localCharTrackerFolder?.let { folder ->
                                runExportToXMLTask(
                                    folder = folder,
                                    includeIcons = true
                                )
                            }
                        }
                    }
                }
                DialogDataOptions.FILE_SELECT_CODE_DB_EXPORT -> if (resultCode == Activity.RESULT_OK) {
                    data.data?.let { chosenFolderUri ->
                        takePersistentUriPermission(uri = chosenFolderUri)
                        prefs.localCharTrackerFolder?.let {
                                dbHelper?.exportDbAndNotifySuccess(ctDocumentFile = it, DEBUG_TAG = DEBUG_TAG)
                        }
                    }
                }
                DialogDataOptions.FILE_SELECT_CODE_ICONS_IMPORT_LOCAL -> if (resultCode == Activity.RESULT_OK) {
                    data.data?.let { folderUri ->
                        takePersistentUriPermission(folderUri)
                        CoroutineScope(Dispatchers.Main).launch {
                            showProgressBarLoading()
                            withContext(Dispatchers.IO){
                                dbHelper?.allCharactersInclDeleted?.map { it.iconFileName }
                                    ?.filter { it.isNotBlank() }?.let { iconNames ->
                                        FileUtils.importIconsFromLocalStorage(
                                            charTrackerFolder = prefs.localCharTrackerFolder,
                                            context = this@ActivityMain,
                                            charIconFileNamesToImport = iconNames
                                        )
                                    }
                            }
                            hideProgressBarLoading()
                            updateGlobalLogDisplays(filterSessions = false,
                                filterChars = false, loadSessionDetails = false)
                        }
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun takePersistentUriPermission(uri: Uri) {
        FileUtils.takePersistentUriPermission(
            uri = uri,
            applicationContext = applicationContext)
    }
    fun runExportToXMLTask(folder: DocumentFile, includeIcons: Boolean) {
        mExportToXmlTask?.cancel(true)
        mExportToXmlTask = ExportToXmlTask(includeIcons).apply {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, folder) }
    }

    private fun startImportXmlTaskUsingUri(params: ImportXmlParam) {
        ImportFromXmlTask(
            progressDialog = progressDialog,
            local = params.local).
        executeOnExecutor(
            AsyncTask.THREAD_POOL_EXECUTOR,
            params
        )
    }

    fun confirmDeleteDbAndStartImportXmlTask(params: ImportXmlParam) {
        if (params.local) {
            params.folder?.findFile(DialogDataOptions.FILE_NAME_XML_EXPORT)?.uri?.let { uri ->
                params.xmlLocalUris = listOf(uri)
            } ?: run {
                Toast.makeText(
                    this@ActivityMain,
                    "No file named ${DialogDataOptions.FILE_NAME_XML_EXPORT} found. Abort Xml Import. Consider renaming file",
                    Toast.LENGTH_SHORT).show()
            }
            Log.e("IMPORT_FROM_XML", "No file named '${DialogDataOptions.FILE_NAME_XML_EXPORT}' found in folder. Abort Xml Import.\n" +
                    "available file: ${params.folder?.listFiles()?.joinToString()}")
        }
        /*
         *Confirm Delete Db before importing
         */
        if (prefs.xmlImportDeleteDb) {
            DialogCustom.showConfirmDialog(
                fragmentManager = this@ActivityMain.supportFragmentManager,
                msg = "Confirm deleting Database:",
                onConfirm = {
                    startImportXmlTaskUsingUri(params)
                },
                onCancel = {
                    prefs.xmlImportDeleteDb = false
                    log("CCC", "delete db = ${prefs.xmlImportDeleteDb}")
                    startImportXmlTaskUsingUri(params)
                })
        } else {
            startImportXmlTaskUsingUri(params)
        }
    }

    private fun confirmDeleteDbAndStartImportXmlTask(paths: ArrayList<String>?, import_option: Int, local: Boolean) {
        confirmDeleteDbAndStartImportXmlTask(
            ImportXmlParam(
                xmlLocalUris = paths?.map { Uri.fromFile(File(it)) },
                import_option = import_option,
                local = local)
        )
    }

//    private void exportToXmlAndToastResults(String path) {
//
//    }

    //    private void exportToXmlAndToastResults(String path) {
//
//    }


    ///////////////////////////////////// INTERFACES //////////////////////////////////////////////

    ///////////////////////////////////// INTERFACES //////////////////////////////////////////////
    interface ProgressPublisher {
        fun doProgress(values: Array<Int>)
        fun display(msg: String)
    }

    //----------------------------------- PRIVATE METHODS --------------------------------------------


    //----------------------------------- PRIVATE METHODS --------------------------------------------
    fun initInterfaceItems() {
        settings = findViewById(R.id.settings)
        help = findViewById(R.id.help)
        button_data_options = findViewById(R.id.button_data_options)
        progressBarFrameLoading = findViewById(R.id.progress_bar_frame_loading)
        progressBarLoadingTextView = progressBarFrameLoading.findViewById(R.id.progress_bar_text_view)
        progressBarFrameSpecial = findViewById(R.id.progress_bar_frame_special)
        progressBarSpecialTextView = progressBarFrameSpecial.findViewById(R.id.progress_bar_text_view)
        progressBarSpecial = progressBarFrameSpecial.findViewById(R.id.progress_bar)
        progressBarSpecial.progressDrawable = ContextCompat.getDrawable(applicationContext,
            R.drawable.my_progress_bar
        )
        progressBarLoading = progressBarFrameLoading.findViewById(R.id.progress_bar)
        progressBarLoading.progressDrawable = ContextCompat.getDrawable(applicationContext,
            R.drawable.my_progress_bar
        )

        progressBars = findViewById(R.id.progress_bars)
        sessionDisplay = findViewById(R.id.session_display)
        header = findViewById(R.id.header)
        pager = findViewById(R.id.viewPager)
        pager.adapter = pageAdapter
        pager.offscreenPageLimit = 3
        pager.currentItem = fragmentOrderList.indexOf(FragmentMainScreen.REF)
        pager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                updateSessionDisplay()
            }

            override fun onPageSelected(position: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
        })
        settings.setOnClickListener {
            val dialog_settings = DialogSettings.newInstance()
            dialog_settings.mOnDismissListener = DialogInterface.OnDismissListener {
                val ms: FragmentMainScreen? = getMainScreen()
                ms?.setNumColsForChars(this@ActivityMain)
                val cv: FragmentCharacterView? = getCharacterView()
                cv?.setNumColsForChars(this@ActivityMain)
                onResume()
                getLogView()?.fragmentLogView?.apply {
                    updateSearchContextSize()
                }
            }
            dialog_settings.mOnRecreateActivityNecessaryListener ={recreate()}

            dialog_settings.show(supportFragmentManager, FRAGMENT_TAG_SHOW_SETTINGS)
        }
        help.setOnClickListener {
            HttpUtils.openWebPage(url = resources.getString(R.string.user_manual_link), activity = this)
        }

        button_data_options.setOnClickListener {
            killRefreshDisplayTimer()
            supportFragmentManager.let { fragmentManager ->
                DialogDataOptions.newInstance().show(fragmentManager, DialogDataOptions.FRAGMENT_TAG)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(KEY_PLEASE_CREATE_SESSION_PROMPT_ACTIVE, PLEASE_CREATE_SESSION_PROMPT_ACTIVE)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        PLEASE_CREATE_SESSION_PROMPT_ACTIVE = savedInstanceState.getBoolean(
            KEY_PLEASE_CREATE_SESSION_PROMPT_ACTIVE)
    }




    fun implementOpenSessionManager() {
        sessionDisplay.setOnClickListener {
            killRefreshDisplayTimer()
            startActivity(Intent(this@ActivityMain, ActivitySessionManager::class.java))
        }
    }

    fun killRefreshDisplayTimer() {
        refreshAfterCharToggeledHandler.removeCallbacks(refreshGlobalDisplaysRunnable)
    }

    open fun makeSureAtLeastOneSessionIsActive(): Boolean {
        if (dbHelper?.getAllSessionNames()?.size == 0) {
            if (!PLEASE_CREATE_SESSION_PROMPT_ACTIVE) {
                log("SESSIONNN", "start DialogNewSession Activity because no Session available and not importing")
                PLEASE_CREATE_SESSION_PROMPT_ACTIVE = true
                DialogNewSession.newInstance(true).show(
                    supportFragmentManager, "newSessionCauseNonExistsMotherfucker!")
            }
            return true
        } else if (dbHelper?.getActiveSessionNames()?.size == 0) {
            log("SESSIONNN", "no session was active")
            val allsessions = dbHelper?.allSessions
            if (allsessions?.size == 1) {
                log("SESSIONNN", "exactely one session exists, so we will choose it for you!")
                /*
                no session is chosen, bu since there is only one session anyways, we can just use this one
                 */
                allsessions.first().let { s ->
                    s.isActive = true
                    CoroutineScope(Dispatchers.Default).launch {
                        withContext(Dispatchers.IO) {
                            dbHelper.updateSession(s)
                        }
                        ActivitySessionManager.setActivityOfCharsAccordingToActiveSessionsTask(this@ActivityMain)
                    }
                    loadInitialDataFromDbAndUpdateGlobalDisplays()
                    adjustUIAccordingToRuleSystem()
                }
                return true

            } else {
                if (!ActivitySessionManager.IS_SHOWING) {
                    Toast.makeText(this, "Please choose at least one session", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, ActivitySessionManager::class.java)
                    startActivity(intent)
                }
                return false
            }
        }
        return false
    }

    private fun setBackgroundsAccordingToRuleSystem() {
        getMainScreen()?.adjustUIAccordingToRuleSystem()
        getLogView()?.setBackgroundAccordingToRuleSystem()
        getCharacterView()?.setBackgroundAccordingToRuleSystem()
        getSessionDataDisplay()?.setBackgroundAccordingToRuleSystem()
    }

    fun adjustUIAccordingToRuleSystem() {
        setBackgroundsAccordingToRuleSystem()
    }

    open fun updateSessionDisplay() {
        dbHelper?.activeSessions?.let { activeSessions ->
//            log("update session display")
            if (activeSessions.isEmpty()) {
                sessionDisplay.setText(R.string.no_session)
            } else {
                /*
                only display this in LogView to not confuse user when creating an entry
                and it shows all sessions, but the entry will only be booked to the current session
                which he can't see in the display
                */

                var sessionsDisplayed = SpannableStringBuilder()
                if (!prefs.filterSessions) {
                    sessionsDisplayed = if (pager.currentItem == 2) {
                        SpannableStringBuilder()
                    } else {
                        Helper.addSessionsDisplaySpannable(
                            context = this,
                            lineHeight = sessionDisplay.lineHeight,
                            sessions = activeSessions,
                            sb = sessionsDisplayed)
                        sessionsDisplayed.append(" - ")
                    }
                    when (prefs.filterChars) {
                        PrefsHelper.FILTER_CHARS_NO_CHARS -> {
                            sessionDisplay.text = sessionsDisplayed.append("All Sessions - Notes")
                        }
                        PrefsHelper.FILTER_CHARS_SHOW_ALL_CHARS -> {
                            sessionDisplay.text = sessionsDisplayed.append("All Sessions - All Chars")
                        }
                        else -> {
                            sessionDisplay.text = sessionsDisplayed.append("All Sessions")
                        }
                    }
                } else {
                    Helper.addSessionsDisplaySpannable(
                        context = this,
                        lineHeight = sessionDisplay.lineHeight,
                        sessions = activeSessions,
                        sb = sessionsDisplayed)

                    when (prefs.filterChars) {
                        PrefsHelper.FILTER_CHARS_NO_CHARS -> {
                            sessionsDisplayed.append(" - Notes")
                            sessionDisplay.text = sessionsDisplayed
                        }
                        PrefsHelper.FILTER_CHARS_SHOW_ALL_CHARS -> {
                            sessionsDisplayed.append(" - All Chars")
                            sessionDisplay.text = sessionsDisplayed
                        }
                        else -> {
                            sessionDisplay.text = sessionsDisplayed
                        }
                    }
                }
            }
        }

    }

    fun updateGlobalLogDisplays(
        filterSessions: Boolean,
        filterChars: Boolean = true,
        loadSessionDetails: Boolean = true) {
        log("CHAR_DETAILS", "update global logs: LOAD_SESSIONS_DETAILS_UPDATE_GLOBAL_SESSIONS= $loadSessionDetails" )

        filterLogsAndUpdateLogDisplaysTask?.cancel(true)
        if (filterLogsAndUpdateLogDisplaysTask?.isCancelled == true) {
//                log("LOAD_INIT_DATA", "CANCEL: update display task num of loadings = $numOfLoadings")
            numOfLoadings--
        }

        filterLogsAndUpdateLogDisplaysTask = FilterLogsAndUpdateGlobalLogDisplaysTask()
//        log("starting asyncTask")
        filterLogsAndUpdateLogDisplaysTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, filterSessions, filterChars, loadSessionDetails)
    }

    suspend fun crawlOgresClubForData() {
        withContext(Dispatchers.Main) {
            Log.d("CRAWL_OGRES_CLUB", "crawling ogres club")

            if (mOgresClubCrawler == null) {
                mOgresClubCrawler = OgresClubCrawler(
                    this@ActivityMain,
                    progressBarFrameSpecial,
                    progressBarSpecial,
                    progressBarSpecialTextView
                )
                mOgresClubCrawler?.setOnFinishedCrawlingOgresClubListener(object :
                    OnFinishedCrawlingOgresClubListener {
                    override fun onComplete() {
                        getCharacterView()?.fillAdapter()
                    }
                })
            }
            Toast.makeText(
                this@ActivityMain,
                R.string.msg_crawling_data_ogres_club,
                Toast.LENGTH_SHORT
            ).show()
            mOgresClubCrawler?.execute()
        }

    }


    fun loadInitialDataFromDbAndUpdateGlobalDisplays() {

        showProgressDialogLoadingData(progressDialog)

        showProgressBarLoading()
//        log("LOAD_INIT_DATA", "load init date: showing progress bar")
        numOfTasksInitLoadingData = 3 //only used for logs chars and Session

        mLoadLogsTask?.cancel(true)
        mLoadLogsTask = LoadLogsTask()
        mLoadLogsTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)


        mLoadCharsTask?.cancel(true)
        mLoadCharsTask = LoadCharsTask()
        mLoadCharsTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

        mLoadSessionsTask?.cancel(true)
        mLoadSessionsTask = LoadSessionsTask()
        mLoadSessionsTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    fun loadRuleSystemRelatedData() {
        ////////////////////////////////////////////////////////////////////////

        prefs.activeRuleSystems.let { ruleSystem ->
            val loadOgresClubData = RuleSystem.rule_system_ogres_club in ruleSystem
            val loadE5Data = Helper.activeRules(
                listOf(RuleSystem.rule_system_Dnd_E5,
                    RuleSystem.rule_system_skillfull_dnd_5E),
                ruleSystem)
            mLoadFeatsFromResourcesTask?.cancel(true)
            mLoadFeatsFromResourcesTask = LoadFeatsFromResourcesTask(
                load35Pf = loadOgresClubData,
                load5e = loadE5Data)
                .apply { executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR) }
            /*
            load feats from db. If there are some do nothing, else crawl them from the internet
            */

            if (loadE5Data) {
                loadDndE5ClassesFromResources()
                loadDndE5RacesFromResources()
            }



        }



    }


    open fun getMainScreen(): FragmentMainScreen? {
        return getViewPagerFragment(FragmentMainScreen.REF) as? FragmentMainScreen?
    }

    open fun getSessionDataDisplay(): FragmentSessionData? {
        return getViewPagerFragment(FragmentSessionData.REF) as? FragmentSessionData?
    }

    open fun getLogView(): FragmentLogViewMainActivity? {
        return getViewPagerFragment(FragmentLogViewMainActivity.REF) as? FragmentLogViewMainActivity?
    }

    open fun getCharacterView(): FragmentCharacterView? {
        return getViewPagerFragment(FragmentCharacterView.REF) as? FragmentCharacterView?
    }

    val dialogMainScreen: FragmentMainScreen?
        get() = (supportFragmentManager
            .findFragmentByTag(
                FragmentMainScreen.REF + FragmentDialogWrapper.REF_SUFFIX
            ) as? FragmentDialogWrapper)?.fragment
                as? FragmentMainScreen?

    //////////////////////////////////////// PRIVATE CLASSES ///////////////////////////////////////

    //////////////////////////////////////// PRIVATE CLASSES ///////////////////////////////////////

    fun inflateNewProgressBar(tag: String) : ProgressBarData {

        val inflater = LayoutInflater.from(this)
        val progressBarFrame: FrameLayout = inflater.inflate(R.layout.progress_bar, null) as FrameLayout
        progressBarFrame.visibility = View.VISIBLE
        progressBars.addView(progressBarFrame)
        log("BBB", "INFLATE new progressBar tag: $tag #progressBars: ${progressBars.childCount}")
        val progressBarTextView: TextView = progressBarFrame.findViewById(R.id.progress_bar_text_view)
        progressBarFrame.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            sessionDisplay.lineHeight + 10)
        val progressBar: ProgressBar = progressBarFrame.findViewById(R.id.progress_bar)
        progressBar.progressDrawable = resources.getDrawable(R.drawable.my_progress_bar)
        val result = ProgressBarData(tag = tag, frame = progressBarFrame, textView = progressBarTextView, progressBar = progressBar)
        log("BBB", "progressBarData created: $result")
        listOfProgressBars.add(result)
        return result
    }

    fun getProgressBarData(tag: String?): ProgressBarData? {
        var progressBarData : ProgressBarData? = null

        tag?.let { tg ->
            val progressBarDataWithMatchingTag = listOfProgressBars.mapNotNull { if (it.tag == tg) it else null }
//            if (progressBarDataWithMatchingTag.isNotEmpty()) {
//                log("BBB", "progressDataWithMatchingTag size = ${progressBarDataWithMatchingTag.size}   -->   ${progressBarDataWithMatchingTag[0]}")
//            }
            if (progressBarDataWithMatchingTag.size == 1) {
                progressBarData = progressBarDataWithMatchingTag[0]
            }
        }
        return progressBarData

    }

    fun endProgressBar(progressBarData: ProgressBarData) {

        progressBarData.progressBar.visibility = View.GONE
        progressBarData.progressBar.progress = 0
        progressBarData.frame.visibility = View.GONE
        val iterator = listOfProgressBars.iterator()
        while (iterator.hasNext()){
            val data = iterator.next()
            if(data.tag == tag) {
                iterator.remove()
                progressBars.removeView(data.frame)
                log("BBB", "removed progress Bar: ${data.tag} #progressBars: ${progressBars.childCount}")

            }
        }
    }

    /**
     * Handle the Results after DropBox upload or download is completed by DropBoxIntent.
     */

    inner class DropBoxResultReceiver(handler: Handler?) : ResultReceiver(handler) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
            super.onReceiveResult(resultCode, resultData)
            /*
            get tag and only do something if the tag is not null, because we use a tag to identify
            our progressbar which we use to do progress and stuff
             */
            val tag = resultData.getString(DropBoxIntent.REF_TAG)

            when (resultCode) {

                DropBoxIntent.UPLOAD_SUCCESSFUL -> {
                    val msg = resultData.getString(DropBoxIntent.REF_MSG) ?: ""
                    Toast.makeText(applicationContext, "${getString(R.string.msg_upload_successful)}\n$msg", Toast.LENGTH_SHORT).show()
                    getProgressBarData(tag)?.frame?.visibility = View.GONE
                }
                DropBoxIntent.UPLOAD_FAILED -> {
                    getProgressBarData(tag)?.frame?.visibility = View.GONE
                    Toast.makeText(applicationContext, R.string.msg_upload_failed, Toast.LENGTH_SHORT).show()
                }
                DropBoxIntent.DO_PROGRESS -> {
                    /*
                    value1 < 0 -> value2 als max progress setzen
                    value1 >= 0 -> value1 als progress
                     */
                    if (resultData.getInt("value1") < 0) {
                        getProgressBarData(tag)?.progressBar?.max = resultData.getInt("value2")
                    } else {
                        getProgressBarData(tag)?.progressBar?.progress = resultData.getInt("value1")
                    }
                }
                DropBoxIntent.IMPORT_FROM_XML -> {
                    val xmlLocalPaths = resultData.getStringArrayList("path")
                    xmlLocalPaths?.let { paths ->
                        confirmDeleteDbAndStartImportXmlTask(
                            paths = paths, import_option = prefs.xmlImport, local = false)
                    }
                }
                DropBoxIntent.MAKE_TOAST -> {
                    Toast.makeText(this@ActivityMain,
                        resultData.getString("msg"),
                        resultData.getInt("length")).show()
                }
                DropBoxIntent.LOAD_INITIAL_DATA_AND_REFRESH_DISPLAYS -> {
                    updateBaseContextLocaleAndMaybeRecreate()
                    loadInitialDataFromDbAndUpdateGlobalDisplays()
                }
                DropBoxIntent.SHOW_PROGRESS_BAR -> {
                    log("BBB", "SHOW progress bar tag: $tag")
                    /*
                    get ProgressBarData related to tag and make visible
                    or inflate new progressBar
                     */
                    getProgressBarData(tag)?.let {
                        log("BBB", "Found progressBarData: ${it.tag}.")
                        it.progressBar.visibility = View.VISIBLE
                        it.frame.visibility = View.VISIBLE
                    }
                        ?: run { tag?.let { inflateNewProgressBar(tag = it) } }

                }
                DropBoxIntent.HIDE_PROGRESS_BAR -> {

                    log("BBB", "HIDE progress bar tag: $tag")
                    getProgressBarData(tag)?.let { endProgressBar(it) }

                }
                DropBoxIntent.SET_PROGRESSBAR_TEXT -> {
                    getProgressBarData(tag)?.textView?.text = resultData.getString("msg")
                }
                DropBoxIntent.INCREASE_OVERALL_PROGRESS -> {
                    getProgressBarData(DropBoxIntent.OVERALL_PROGRESS_INDICATOR_TAG)?.let { progBarData ->
                        val progBar = progBarData.progressBar
                        progBar.incrementProgressBy(1)
                        if (progBar.progress >= progBar.max) {
                            endProgressBar(progBarData)
                        }
                    }
                }
                DropBoxIntent.RESULT_CODE_DISMISS_DATA_OPTIONS -> {
                    PLEASE_CREATE_SESSION_PROMPT_ACTIVE = false
                    log(DROPBOX_DEBUG_TAG, "onReceiveResult: closing data options.")
                    (supportFragmentManager.findFragmentByTag(DialogDataOptions.FRAGMENT_TAG)
                            as? DialogDataOptions?)?.dismiss()
                }
            }
        }
    }


//-------------------------------- ASYNC-TASKS ---------------------------------------------------

    inner class ExportToXmlTask(val includeIcons: Boolean = false): AsyncTask<DocumentFile, Int, Pair<Boolean, String>?>(),
        ProgressPublisher {

        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            progressBarSpecial.progress = 0
            progressBarSpecialTextView.setText(R.string.msg_creating_xml_file)
            progressBarFrameSpecial.visibility = View.VISIBLE
        }


        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: DocumentFile?): Pair<Boolean, String>? {
            val folder: DocumentFile? = params[0]
            folder?.let { return dbHelper?.writeToXML(
                context = this@ActivityMain,
                folder = folder,
                progressPublisher = this,
                includeIcons = includeIcons) }
            return null
        }


        override fun doProgress(values: Array<Int>) {
            publishProgress(*values)
        }


        override fun display(msg: String) {
            runOnUiThread{ progressBarSpecialTextView.text = msg }
        }

        @Deprecated("Deprecated in Java")
        override fun onProgressUpdate(vararg values: Int?) {
            values[0]?.let { progress ->
                if (progress < 0) {
                    values[1]?.let { max -> progressBarSpecial.setMax(max) }
                } else {
                    progressBarSpecial.setProgress(progress)
                }
            }
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: Pair<Boolean, String>?) {
            progressBarSpecial.progress = 0
            val successfullyCreatedXml: Boolean = result?.first ?: false
            val path: String = result?.second ?: "invalid Path"

            if (successfullyCreatedXml) {
                Toast.makeText(applicationContext, getString(R.string.msg_successfully_created_file, path),
                    Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(applicationContext, R.string.msg_creating_xml_failed, Toast.LENGTH_SHORT).show()
            }

            progressBarFrameSpecial.visibility = View.GONE

        }


    }

    /**
     * @param xmlLocalUris is used for import from xml, where the files are downlaoded from dropbox
     * @param folder is used for local xml import, where only the local chartrackerfolder is given
     * and the file name is always the same
     */
    class ImportXmlParam(
        var xmlLocalUris: List<Uri>?,
        val import_option: Int,
        val local: Boolean,
        val folder: DocumentFile? = null)

    inner class ImportFromXmlTask(val progressDialog: MyProgressDialog, val local: Boolean): AsyncTask<ImportXmlParam, Int, XmlImportResult>(),
        ProgressPublisher {

        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {

            progressDialog.setTitle(R.string.label_importing)
            val msg: String = resources.getString(R.string.importingDataFromXmlProgressDialogMessagePleaseWait)
            progressDialog.setMessage(msg)
            progressDialog.setProgress(0)
            progressDialog.show(MyProgressDialog.PROGRESS_STYLE_BAR)
        }

        override fun doProgress(values: Array<Int>) {
            runOnUiThread {
                if (values[0] < 0) {
                    progressDialog.progressBar?.max = values[1]
                } else {
                    progressDialog.setProgress(values[0])
                    log("ImportXml", "successfully done progress")
                }
            }
        }

        override fun display(msg: String) {
            runOnUiThread { progressDialog.setMessage(msg)}
        }


        fun toast(msg: String?) {
            runOnUiThread {
                Toast.makeText(this@ActivityMain, msg, Toast.LENGTH_SHORT).show() }
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: ImportXmlParam): XmlImportResult? {
            return dbHelper.importFromXml(
                params[0],
                this
            )

        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(output: XmlImportResult?) {
            progressDialog.setProgress(0)
            progressDialog.dismiss()
            output?.let { xmlImportResult ->
                toastXmlImportSummery(xmlImportResult, this@ActivityMain)
                /*
                download icon from Dropbox if auto upload is active
                or copy from storage to internal dir
                 */
                CoroutineScope(Dispatchers.Main).launch {
                    if (prefs.autoImportIcons) {
                        if (local) {
                            importIconsFromLocalStorage(output)
                        } else {
                            downloadIconsFromDropbox(output)
                        }
                    }
                    log("IMPORT_XML", "importing set to false")
                    /*
                    after import, no imported sessions could be active.
                    if this is the case, makeSureAtLeastOneSessionIsActive fun will deal with it.
                    Otherwise load initial data
                     */
                    if (makeSureAtLeastOneSessionIsActive()) {
                        loadInitialDataFromDbAndUpdateGlobalDisplays()
                    }
                }
            }

        }

        suspend fun importIconsFromLocalStorage(xmlImportResult: XmlImportResult) {
                FileUtils.importIconsFromLocalStorage(
                    charTrackerFolder = xmlImportResult.folder,
                    context = this@ActivityMain,
                    charIconFileNamesToImport = xmlImportResult.charIconFileNamesToImport
                )
        }

        fun downloadIconsFromDropbox(xmlImportResult: XmlImportResult) {
            //todo make this parallel
            Log.d("DROPBOX", "Downloading char Items because autodownload is true")
            dbXHelper.doDropBoxStuff(doStuff =  {
                log(DROPBOX_DEBUG_TAG, "getting meta data from dropbox: available character icons")

                CoroutineScope(Dispatchers.Main).launch {
                    val asyncResult = async(context = Dispatchers.IO) {
                        DropboxClientFactory.client?.files()?.listFolder("/icons")?.entries
                    }
                    log(DROPBOX_DEBUG_TAG, "finished loading meta data")
                    asyncResult.await()?.map { it.name }?.let {
                            charIconsFileNamesInDropBox ->
                        val charIconsFileNamesToDownload = xmlImportResult.charIconFileNamesToImport.
                        intersect(charIconsFileNamesInDropBox.toSet())

                        dbXHelper.startDropboxIntentWthEnding(
                            task = DropBoxIntent.TASK_DOWNLOAD,
                            mode = DropBoxIntent.MODE_CHAR_ICON,
                            fileNameWithEnding = charIconsFileNamesToDownload.toTypedArray())
                    }
                }
            })
        }
    }

    open fun showProgressDialogLoadingData(progressDialog: MyProgressDialog?) {
        progressDialog?.setTitle(R.string.msg_loading_data)
        progressDialog?.setMessage(getString(R.string.msg_please_wait_while_loading_data_from_db))
        progressDialog?.show(MyProgressDialog.PROGRESS_STYLE_SPINNER)
    }


    inner class LoadFeatsFromResourcesTask(
        val load35Pf: Boolean,
        val load5e: Boolean) : AsyncTask<Void?, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void?): Void? {
            Global.featsFromResources = ArrayList()
            if (load35Pf) load35PfFeats()
            if (load5e) load5eFeats()
            return null
        }

        private fun load35PfFeats() {
            val res: Resources = resources
            val ta = res.obtainTypedArray(R.array.feats_35pf_links)
            val n = ta.length()
            for (i in 0 until n) {
                val id = ta.getResourceId(i, 0)
                if (id > 0) {
                    val featLink = FeatLink(res.getStringArray(id))
                    Global.featsFromResources.add(featLink)
                    //                    Helper.log("PARSING_FEATS", "created new FeatLink from Res; name = " + featLink.name + ", links = " + featLink.links.toString());
                } else {
                    log("PARSING_FEATS", "something wrong with featsandLinks.xml")
                }
            }
            ta.recycle() // Important!
        }

        private fun load5eFeats() {
            val res: Resources = resources
            val ta = res.obtainTypedArray(R.array.feats_5e_links)
            val numfeats = ta.length()
            for (i in 0 until numfeats) {
                val id = ta.getResourceId(i, 0)
                if (id > 0) {
                    val a = res.getStringArray(id)
                    val featLink = FeatLink(name = a[0], link = a[4])
                    Global.featsFromResources.add(featLink)
                    //                    Helper.log("PARSING_FEATS", "created new FeatLink from Res; name = " + featLink.name + ", links = " + featLink.links.toString());
                } else {
                    log("PARSING_FEATS", "something wrong with featsandLinks.xml")
                }
            }
            ta.recycle() // Important!
        }
    }

    fun loadDndE5ClassesFromResources() {
        Global.classesFromResources = ArrayList()
        Global.archetypesFromResources = ArrayList()
        val res: Resources = resources
        val ta = res.obtainTypedArray(R.array.class_links_5e)
        val n = ta.length()
        for (i in 0 until n) {
            val id = ta.getResourceId(i, 0)
            if (id > 0) {
                val a = res.getStringArray(id)
                val cls = DndClass(name = a[0], link = a[1], archetype_name = a[2])
                log("PARSING CLASSES", "class: '${cls.name}', archetype name: '${cls.archetype_name}'")
                for (n in 3 until a.size step 2) {
                    Global.archetypesFromResources.add(
                        DndArchetype(name = a[n], link = a[n+1])
                            .apply{
                                classes.add(cls.name)
                                cls.archetype_names.add(name)
                            }
                    )
                }
                Global.classesFromResources.add(cls)
            } else {
                log("PARSING_CLASSES", "something wrong with class_links_5e.xml")
            }
        }
        ta.recycle() // Important!
//        log("PARSING_CLASSES", "parsed classes: ${Global.classesFromResources}")
//        log("PARSING_ARCHETYPES", "parsed archetypes: ${Global.archetypesFromResources}")


    }

    fun loadDndE5RacesFromResources() {
        Global.racesFromResources = ArrayList()
        val res: Resources = resources
        val ta = res.obtainTypedArray(R.array.race_links_5e)
        val n = ta.length()
        for (i in 0 until n) {
            val id = ta.getResourceId(i, 0)
            if (id > 0) {
                val a = res.getStringArray(id)
                val race = Race(name = a[0], link = a[1])
                Global.racesFromResources.add(race)
            } else {
                log("PARSING_RACES", "something wrong with race_links_5e.xml")
            }
        }
        ta.recycle() // Important!
    }

    inner class LoadSkillGroupsFromResources :
        AsyncTask<Void?, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void?): Void? {

            val sr5Skills = Skill.getSkillMap(
                arrayId = R.array.shadowrun5_skill_list,
                context = this@ActivityMain
            )
            Global.skill_groups = ArrayList(
                sr5Skills.values
                    .map { it.type }
                    .filterNot { it == getString(R.string.shadowrun5_skill_group_name_none) }
                    .map {sgName ->
                        SkillGroup(
                            name = sgName,
                            skills = sr5Skills.values
                                .filter { it.type == sgName }
                                .map {it.name}
                        )
                    }
            )
            return null
        }
    }





    /**
     * load Data from Database and store in Global Variables
     */
    inner class LoadSessionsTask : AsyncTask<Void?, Void?, Void?>() {


        @Deprecated("Deprecated in Java")
        override fun onPreExecute() { //            addTaskLoadInitData();
//            log(LOAD_INIT_DATA_DEBUG_TAG, "START: Load Sessions task")
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Void?): Void? {
            Global.charProgressionWords = createCharProgressWords(
                CharProgressItem.createCharProgressItems(this@ActivityMain))
            dbHelper?.getActiveSessionsIds()?.let { Global.activeSessionIds = it }
//            log(LOAD_INIT_DATA_DEBUG_TAG, "Done Loading active Sessions")
            //            Global.allSessionIds = dbHelper.getActiveSessionsIds();
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            updateSessionDisplay()
            loadInitialDataPostExecute()
            loadRuleSystemRelatedData()
        }
    }

//    private void addTaskLoadInitData(){
//        numOfTasksInitLoadingData++;
//        Helper.log("LOAD_INIT_DATA", "started Task loading initial data, count = " + numOfTasksInitLoadingData);
//    }

//    private void addTaskLoadInitData(){
//        numOfTasksInitLoadingData++;
//        Helper.log("LOAD_INIT_DATA", "started Task loading initial data, count = " + numOfTasksInitLoadingData);
//    }

    open fun removeTaskLoadInitData() {
        numOfTasksInitLoadingData--
//        log("LOAD_INIT_DATA", "completed task loading initial data, count = $numOfTasksInitLoadingData")
    }

    open fun onLoadInitialDataComplete() {
        progressDialog.dismiss()
//        log("LOAD_INIT_DATA", "load init data complete: hiding progress bar")
        hideProgressBarLoading()
        makeSureAtLeastOneSessionIsActive()
        updateGlobalLogDisplays(filterSessions = true)
    }

    fun loadInitialDataPostExecute() {
        removeTaskLoadInitData()
//            log(LOAD_INIT_DATA_DEBUG_TAG, "DONE: Load Logs task")
        if (numOfTasksInitLoadingData == 0) {
            onLoadInitialDataComplete()
        }
    }

    /**
     * load Data from Database and store in Global Variables
     */
    inner class LoadLogsTask : AsyncTask<Void?, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() { //            addTaskLoadInitData();
//            log(LOAD_INIT_DATA_DEBUG_TAG, "START: Load Logs task")
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Void?): Void? {
            dbHelper?.let {
                Global.allLogs = it.getAllLogs()
            }

//            log(LOAD_INIT_DATA_DEBUG_TAG, "Done loading all logs")
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            loadInitialDataPostExecute()
        }
    }

    /**
     * load Data from Database and store in Global Variables
     */
    inner class LoadCharsTask : AsyncTask<Void?, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() { //            addTaskLoadInitData();
//            log(LOAD_INIT_DATA_DEBUG_TAG, "START: Load Chars task")
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Void?): Void? {
            dbHelper?.let {
                Global.chars = it.allCharacters
//                log(LOAD_INIT_DATA_DEBUG_TAG, "Done Loading all chars")
            }

            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            loadInitialDataPostExecute()
        }
    }


    /**
     * first parameter declares if logsFilteredBySession should be rebuilt from allLogs
     *
     *
     * filtering by chars is always necessary, because we never want to see log with no char related at the same time as logs
     * with some char related
     */
    inner class FilterLogsAndUpdateGlobalLogDisplaysTask : AsyncTask<Boolean, Void?, List<Boolean>>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
//            log("LOAD_INIT_DATA", "update dispalys: showing progress bar")
            showProgressBarLoading()
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Boolean?): List<Boolean> { /*
            if (android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();
            */
            var loadSessionDetails = true
            val filterSessions = params[0] ?: false
            val filterChars = params[1] ?: false
            if (params.size > 2) {
                loadSessionDetails = params[2] ?: true
            }
            if (filterSessions) {
                Global.logsFilteredBySession = filterByActiveSessions(
                    Global.allLogs,
                    Global.activeSessionIds
                )
//                log("AsyncTask", "session have been filtered: TRUE")
            } else {
//                log("AsyncTask", "session have been filtered: FALSE")
            }
            if (filterChars) {
                val helpList: ArrayList<LogEntry> = if (prefs.filterSessions) {
                    Global.logsFilteredBySession
                } else {
                    Global.allLogs
                }
                when (prefs.filterChars) {
                    PrefsHelper.FILTER_CHARS_NO_CHARS -> {
                        Global.logsToShow = filterByChars(helpList, ArrayList())
                    }
                    PrefsHelper.FILTER_CHARS_SHOW_ALL_CHARS -> {
                        Global.logsToShow = filterByChars(helpList, Global.chars)
                    }
                    else -> {
                        Global.logsToShow = filterByChars(helpList, getActiveChars(Global.chars))
                    }
                }
                log("AsyncTask", "chars have been filtered: TRUE")
            } else {
                log("AsyncTask", "chars have been filtered: FALSE")
            }
            // I think we don't really need it and it crashes the app regularly
            Collections.sort(Global.logsToShow, LogEntry.CREATION_DATE_COMPARATOR)
            Global.tagsToAutoComplete = getSortedTagNamesWithHashTagFromLogEntries(Global.logsToShow, dbHelper)
            Global.tagsToAutoComplete.sort()
            //debugging ----------------------------------------------------------------------
/*
            if (Global.logging) {
                Helper.log("+++++++++++++++++++", "AllLogs: ");
                for (LogEntry e : allLogs) {
                    Helper.log(e.toString_SessionCharsExpText(ActivityMain.this));
                }
                Helper.log("+++++++++++++++++++", "LogsFilteredBySession: ");
                for (LogEntry e : logsFilteredBySession) {
                    Helper.log(e.toString_SessionCharsExpText(ActivityMain.this));
                }
                Helper.log("+++++++++++++++++++", "LogsToShow: ");
                for (LogEntry e : logsToShow) {
                    Helper.log(e.toString_SessionCharsExpText(ActivityMain.this));
                }
                Helper.log("+++++++++++++++++++", "TagsToAutoComplete: ");
                for (String t : tagsToAutoComplete) {
                    Helper.log(t);
                }
                Helper.log("+++++++++++++++++++", "AllChars And Activity: ");
                for (Character c : chars) {
                    Helper.log(c.id + " " + c.name + ": ACTIVE " + c.isActive + ", DELETED " + c.isDeleted);
                }
                Helper.log("+++++++++++++++++++", "ActiveSessions: ");
                for (long id : activeSessionIds) {
                    Helper.log(id + " " + dbHelper.getSessionName(id));
                }
            }
            */
//debugging ----------------------------------------------------------------------
            return listOf(filterSessions, loadSessionDetails)
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: List<Boolean>) {
            val filterSessions = result[0]
            val loadSessionDetails = result[1]
            val ms = getMainScreen()
            if (ms != null) {
                ms.refreshDisplays(loadSessionDetails = loadSessionDetails)
                log("FragmentMainScreen displays updated")
            } else {
                log("FragmentMainScreen was null, so couldn't update views")
            }
            val lv = getLogView()
            if (lv != null) {
                lv.refreshDisplays()
                log("FragmentLogView displays updated")
            } else {
                log("FragmentLogView was null, so couldn't update views")
            }
            val cv = getCharacterView()
            if (cv != null) {
                cv.fillAdapter()
                log("CharView displays updated")
            } else {
                log("CharView was null, so couldn't update views")
            }
            log("LOAD_INIT_DATA", "update displays called: hiding progress bar")
            dialogMainScreen?.refreshDisplays(loadSessionDetails = false)
            hideProgressBarLoading()
        }
    }

    /*
    Fixes something I cant remember but makes edit texts in viewpager unable to enter numbers...
    This is far worse!! Beware!!!

    class CustomViewPager : ViewPager {
        constructor(context: Context?) : super(context!!)
        constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs)

        override fun requestChildFocus(child: View, focused: View) {
            //Do nothing, disables automatic focus behaviour
        }
    }
    */


    data class ProgressBarData(val tag: String,
                               val frame: FrameLayout,
                               val textView: TextView,
                               val progressBar: ProgressBar)
}

