package com.mtths.chartracker.activities

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.preferences.PrefsHelper
import java.util.Locale

open class BaseFragmentActivity: FragmentActivity() {

    override fun onResume() {
        updateBaseContextLocaleAndMaybeRecreate()
        super.onResume()
    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(updateBaseContextLocale(base))
    }

    fun updateBaseContextLocaleAndMaybeRecreate() {
        val oldLoc = Locale.getDefault()
        updateBaseContextLocale(this)
        if (oldLoc != Locale.getDefault()) {
            Log.d("BASE_FRAGMENT_ON_RESUME", "!!!!!!!!!!!!!!!!!!!!!!!!!!!! Language changed from $oldLoc to ${Locale.getDefault()}. recreating activity !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            recreate()
        }
    }

    open fun updateBaseContextLocale(context: Context): Context? {
        val prefs = PrefsHelper(context)
        val language = DatabaseHelper.getInstance(context)?.activeSessions?.also {
            Log.d("LANGUAGE", "updateBaseContextLocale: active sessions: ${it.joinToString {"${it.name} // ${it.language}"}}")
        }?.firstOrNull()?.language
             ?: prefs.language
        Log.d("LANGUAGE", "updateBaseContextLocale: set language to $language")
        val locale = Locale(language)
        Locale.setDefault(locale)

        return if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            updateResourcesLocale(context, locale)
        } else updateResourcesLocaleLegacy(context, locale)
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    open fun updateResourcesLocale(context: Context, locale: Locale): Context? {
        val configuration = Configuration(context.resources.configuration)
        configuration.setLocale(locale)
        return context.createConfigurationContext(configuration)
    }

    @Suppress("deprecation")
    open fun updateResourcesLocaleLegacy(context: Context, locale: Locale): Context? {
        val resources = context.resources
        val configuration: Configuration = resources.configuration
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)
        return context
    }

}