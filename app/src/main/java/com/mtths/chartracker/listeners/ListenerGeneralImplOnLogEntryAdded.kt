package com.mtths.chartracker.listeners

import android.content.Context
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.Global
import com.mtths.chartracker.utils.Helper.arrayContainsElementIgnoringCases
import com.mtths.chartracker.utils.Helper.log
import java.util.*

/**
 * Created by mtths on 22.04.17.
 */
//I don't really know, why this is implemented as a listener and not just simply in the onLogEntryAdded method
class ListenerGeneralImplOnLogEntryAdded internal constructor(private val context: Context) {
    fun generalOnLogEntryAdded(newLogEntry_id: Long) {
        DatabaseHelper.getInstance(context)?.let { dbHelper ->
            dbHelper.getLogEntry(newLogEntry_id)?.let { e ->
                log("newLogEntryLoaded", e.toString_SessionCharsExpText(context))
                Global.allLogs.add(0, e)
                Global.logsFilteredBySession.add(0, e)
                Global.logsToShow.add(0, e)
                e.tag_ids?.forEach { id ->
                    val tagName = dbHelper.getTagName(id)
                    if (!arrayContainsElementIgnoringCases(Global.tagsToAutoComplete, tagName)) {
                        Global.tagsToAutoComplete.add(tagName)
                    }
                }
                Collections.sort(Global.tagsToAutoComplete)

            }
        }
    }

}