package com.mtths.chartracker.listeners

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemLongClickListener
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.dataclasses.Character

/**
 * Created by mtths on 09.04.17.
 */
class ListenerImplShowCharDetails internal constructor(private val mContext: Context) : OnItemLongClickListener {
    override fun onItemLongClick(parent: AdapterView<*>, view: View, position: Int, id: Long): Boolean {
        val showCharView = Intent(mContext, ActivityCharDetails::class.java)
        //hand over character id to new Activity
        val (id1) = parent.getItemAtPosition(position) as Character
        showCharView.putExtra(ActivityCharDetails.PARAM_CHAR_ID, id1)
        log("transmitted id to ActivityCharDetails: $id1")
        mContext.startActivity(showCharView)
        //longClick has been handled, so don't loadedCharDeatails onClickListener
        return true
    }

}