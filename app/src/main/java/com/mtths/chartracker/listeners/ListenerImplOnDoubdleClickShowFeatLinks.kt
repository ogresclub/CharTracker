package com.mtths.chartracker.listeners

import android.os.AsyncTask
import android.view.View
import com.mtths.chartracker.utils.GetFeatLinksTask
import com.mtths.chartracker.utils.Helper.getTextAfterStringAndTrim
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.pedromassango.doubleclick.DoubleClickListener

class ListenerImplOnDoubdleClickShowFeatLinks internal constructor(private val mActivity: ProgressBarFrameDropBoxActivity, var logEntryText: String) : DoubleClickListener {
    private var mGetFeatLinksTask: GetFeatLinksTask? = null
    override fun onDoubleClick(view: View) {
        if (logEntryText.toLowerCase().contains("@Feat".toLowerCase())) {
            val textAfterAtFeat = getTextAfterStringAndTrim(logEntryText, "@Feat")

            mGetFeatLinksTask?.cancel(true)

            mGetFeatLinksTask = GetFeatLinksTask(mActivity)
            mGetFeatLinksTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, textAfterAtFeat)
        }
    }

    override fun onSingleClick(view: View) {}

}