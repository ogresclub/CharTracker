package com.mtths.chartracker.listeners

import androidx.fragment.app.FragmentManager
import android.view.View
import android.view.View.OnLongClickListener
import android.widget.AdapterView
import android.widget.AdapterView.OnItemLongClickListener
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogEditLogEntry

/**
 * Created by mtths on 10.04.17.
 */
class ListenerImplOpenEditLogsOnLongClick : OnItemLongClickListener, OnLongClickListener {
    private var fm: FragmentManager
    private var e: LogEntry? = null

    /**
     * use this for creating a AdapterView OnItemLongClickListener
     *
     * @param fm
     */
    internal constructor(fm: FragmentManager) {
        this.fm = fm
    }

    /**
     * use for creating an View OnLongClickListener
     *
     * @param e
     */
    internal constructor(fm: FragmentManager, e: LogEntry?) {
        this.e = e
        this.fm = fm
    }

    override fun onItemLongClick(parent: AdapterView<*>, view: View, position: Int, id: Long): Boolean { //get the log Entry
        val (id1) = parent.getItemAtPosition(position) as LogEntry
        DialogEditLogEntry.newInstance(id1).show(
                fm, "dialogEditLogEntry"
        )
        return true
    }

    override fun onLongClick(v: View): Boolean { //get the log Entry
        DialogEditLogEntry.newInstance(e!!.id)
            .show(
                fm, "dialogEditLogEntry"
        )
        return true
    }
}