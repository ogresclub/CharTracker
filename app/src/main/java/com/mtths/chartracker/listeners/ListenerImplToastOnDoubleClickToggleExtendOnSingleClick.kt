package com.mtths.chartracker.listeners

import android.app.Activity
import android.view.View
import android.widget.Toast
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.dataclasses.Character
import com.pedromassango.doubleclick.DoubleClickListener
import kotlin.contracts.contract

class ListenerImplToastOnDoubleClickToggleExtendOnSingleClick(var activity: Activity, var msg: String, var filterConstraintId: Int?, var character: Character?) : DoubleClickListener {
    override fun onSingleClick(view: View) {
        filterConstraintId?.let { filterConstraintId ->
            character?.let { character ->
                character.toggleExtend(
                    charProgItemfilterConstraintId = filterConstraintId,
                    context = activity)
                DatabaseHelper.getInstance(activity)
                    ?.updateCharacter(character, keep_last_update_date = false)
//                Toast.makeText(
//                    activity,
//                    "expand $filterConstraint = ${character.getExtend(filterConstraint, orientation)}\n" +
//                            "${character.extendCharProgItemsMap}",
//                    Toast.LENGTH_SHORT).show()

                try {
                    (activity as ActivityCharDetails).updateDisplays(generateCPI = false)
                } catch (e: Exception) {
                    Helper.log("can't update displays after changing expand option for charProgressItem because activity is not activityCharDetails")
                    e.printStackTrace()
                }
            }

        }

    }
    override fun onDoubleClick(view: View) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

}