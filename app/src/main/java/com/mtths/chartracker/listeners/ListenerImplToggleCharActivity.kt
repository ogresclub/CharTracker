package com.mtths.chartracker.listeners

import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.RelativeLayout
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.fragments.FragmentMainScreen.OnCharActivityChangedListener
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.R
import com.mtths.chartracker.dataclasses.Character

/**
 * Created by mtths on 09.04.17.
 */
class ListenerImplToggleCharActivity(
    var context: Context,
    var mOnCharActivityChangedListener: OnCharActivityChangedListener) : OnItemClickListener {
    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) { //get character associated to button clicked
        val character = parent.getItemAtPosition(position) as Character
        //change activity of char
        if (!(character.isDeleted || character.isRetired || character.isDead)) {
            character.toggleActivity()
            getInstance(context)?.updateCharacter(character, keep_last_update_date = true)
            mOnCharActivityChangedListener.onCharStatsChanged(character, loadSessionDetails = false)
            log("Char activity switched: " + character.name + " = " + character.isActive)
            val background = view.findViewById<View>(R.id.background) as RelativeLayout
            //change color of Button
            if (character.isActive) {
                background.setBackgroundResource(R.drawable.button_holo_green_light_for_character)
            } else {
                background.setBackgroundResource(R.drawable.button_light_grey_for_character)
            }
        }
    }

}