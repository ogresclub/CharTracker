package com.mtths.chartracker

import com.mtths.chartracker.utils.Helper.getCharIconsMap
import com.mtths.chartracker.adapters.LogAdapter
import com.mtths.chartracker.dataclasses.*
import com.mtths.chartracker.utils.CharProgressItemMap
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * This class contains globally used constants
 */
object Global {
    /*
     * DATA
     */
    var allLogs = ArrayList<LogEntry>()
    var logsFilteredBySession = ArrayList<LogEntry>()
    var logsToShow = ArrayList<LogEntry>()
    var tagsToAutoComplete = ArrayList<String>()
    var characterNamesToAutocomplete = ArrayList<LogAdapter.ColoredString>()
    var placesNamesToAutocomplete = ArrayList<LogAdapter.ColoredString>()

    var charIconsMap = getCharIconsMap(inclErrorIcon = false)
    var charIconsMapWErrorIcon = getCharIconsMap(inclErrorIcon = true)

    /**
     * first entry is the name, second entry is the link to the feat on ogres club.
     * Probably one hast to add https://ogres.club before the link.
     */
    var featsOnOgresClub = ArrayList<FeatLink>()
    var featsFromResources = ArrayList<FeatLink>()
    var classesFromResources = ArrayList<DndClass>()
    var classesFromOgresClub = ArrayList<DndClass>()
    var archetypesFromResources = ArrayList<DndArchetype>()
    var archetypesFromOgresClub = ArrayList<DndArchetype>()
    var racesFromOgresClub = ArrayList<Race>()
    var racesFromResources = ArrayList<Race>()
    var subracesFromOgresClub = ArrayList<Subrace>()
    var activeSessionIds = ArrayList<Long>()
    var charsRelatedToSession = ArrayList<Character>()
    var chars = ArrayList<Character>()
    val cachedCharProgressItems = HashMap<Long, CharProgressItemMap>()
    var skill_groups: ArrayList<SkillGroup> = ArrayList<SkillGroup>()

    /**
     * the headers of items whose data is relevant to all characters in session
     * and also a function to create new char progress item
     */
    val ITEMS_SESSION_DATA: List<ItemHeaderAndCreateFunction> = listOf(
        ItemHeaderAndCreateFunction(
            filterConstraintId = R.string.filter_constraint_quest,
            createNew = CharProgressItem::createCharProgressItemQuests
        ),
        ItemHeaderAndCreateFunction(
            filterConstraintId = R.string.filter_constraint_character,
            createNew = CharProgressItem::createCharProgressItemCharacters
        ),
        ItemHeaderAndCreateFunction(
            filterConstraintId = R.string.filter_constraint_place,
            createNew = CharProgressItem::createCharProgressItemPlaces
        )
    )

    /**
     * the headers of valuebased items
     * and also a function to create new char progress item
     */
    val ITEMS_VALUE_BASED_DATA: List<ItemHeaderAndCreateFunction> = listOf(
        ItemHeaderAndCreateFunction(
            filterConstraintId = R.string.filter_constraint_attribute,
            createNew = CharProgressItem::createCharProgressItemAttributes
        ),
        ItemHeaderAndCreateFunction(
            filterConstraintId = R.string.filter_constraint_skill,
            createNew = CharProgressItem::createCharProgressItemSkills
        ),
        ItemHeaderAndCreateFunction(
            filterConstraintId = R.string.filter_constraint_skill_group,
            createNew = CharProgressItem::createCharProgressItemSkillGroups,
            doUseWithRules = HashMap<RuleSystem, Boolean>().apply {
                RuleSystem.allRuleSystems().forEach {
                    set(it, false)
                }
                set(RuleSystem.rule_system_sr5, true)
            }
        ),
        ItemHeaderAndCreateFunction(R.string.filter_constraint_language, CharProgressItem::createCharProgressItemLanguages),
        ItemHeaderAndCreateFunction(
            filterConstraintId = R.string.filter_constraint_knowledge,
            CharProgressItem::createCharProgressItemKnowledge,
            doUseWithRules = HashMap<RuleSystem, Boolean>().apply {
                RuleSystem.allRuleSystems().forEach {
                    set(it, false)
                }
                set(RuleSystem.rule_system_sr6, true)
            }
        ),
        ItemHeaderAndCreateFunction(
            R.string.filter_constraint_class,
            CharProgressItem::createCharProgressItemClasses,
            doUseWithRules = HashMap<RuleSystem, Boolean>().apply {
                RuleSystem.allRuleSystems().forEach {
                    set(it, true)
                }
                set(RuleSystem.rule_system_sr5, false)
                set(RuleSystem.rule_system_sr6, false)
            }),
        ItemHeaderAndCreateFunction(
            filterConstraintId = R.string.filter_constraint_initiation,
            createNew = CharProgressItem::createCharProgressItemInitiation,
            doUseWithRules = HashMap<RuleSystem, Boolean>().apply {
                RuleSystem.allRuleSystems().forEach {
                    set(it, false)
                }
                set(RuleSystem.rule_system_sr5, true)
                set(RuleSystem.rule_system_sr6, true)
            }
        )
    )




    fun clearCache() {
        chars.clear()
        charsRelatedToSession.clear()
        logsToShow.clear()
        cachedCharProgressItems.clear()
        logsFilteredBySession.clear()
        allLogs.clear()
    }


    /**
     * Those are the @tags which are used only to track char progression, not any story relevant information
     * log entries containing those @tags will be colored and can be filtered
     */
    var charProgressionWords = ArrayList<String>()
    var ATTRIBUTE_POINT_EXP_COST = 500
    const val SKILL_POINT_EXP_COST = 200

    const val ICONS_FOLDER = "icons"
    /*
     * those references are used for Helper.getTagNames() method to determine which Object you want
     * to get the names of
     */
    const val TAG_REF = 0
    const val CHARACTER_REF = 1
    const val SESSION_REF = 2
    var logging = true
    var IGNORE_EXP_CHECK_INDICATOR = ".ignore"
    const val FEATURED_MARKER = ".featured"
    const val ORIGIN_LANGUAGE_MARKER = ".origin"
    const val GRAVITY_CENTER_INDICATOR = ".center"
    const val MOTHER_TONGUE_LANGUAGE_MARKER = ".motherTongue"


    val PATTERN_AFTER_AT: Pattern = Pattern.compile("@.*")
    val PATTERN_FEATURE_MARKERS_AT_VALUE_AND_NUMBERS: Pattern = Pattern.compile("(?i)(\\s|^)+[\\-\\.]\\w*?(\\s|$)+|-?\\d+|@Value")
    val PATTERN_AT_VALUE: Pattern = Pattern.compile("(?i)@VALUE")
    val PATTERN_FILTER_CONSTRAINT: Pattern = Pattern.compile("(^|\\s)@\\w+")
    val PATTERN_SKILL: Pattern = Pattern.compile("(^[^\\[@\\d.+-]+)\\s*\\d*\\s*((\\[+)([^\\[\\]]+)(]+))?")
    val PATTERN_NAME_NO_DIGITS_REALLY_NO_BRACKETS: Pattern = Pattern.compile("(^[^\\[@\\d.(]+)\\s*(\\[([^\\[\\]]*)])?(\\[\\[([^\\[\\]]*)]])?")
    val PATTERN_DOMAIN: Pattern = Pattern.compile("https?://([^/]+).*")
    val PATTERN_SPECIALIZATION: Pattern = Pattern.compile("([^\\d/]*)(-?\\d*)\\s*((//)(.*))?")
    val REGEX_MARKER = "\\.[a-zA-Z]+"
    val PATTERN_MARKER:Pattern = Pattern.compile(REGEX_MARKER)
    val MY_DATE_FORMAT= SimpleDateFormat(
            "dd-MM-yyyy HH:mm:ss", Locale.GERMANY)
    val DATE_FORMAT_NO_TIME = SimpleDateFormat("dd-MM-yyyy", Locale.GERMANY)


}