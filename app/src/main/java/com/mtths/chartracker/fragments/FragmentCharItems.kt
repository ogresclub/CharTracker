package com.mtths.chartracker.fragments

import android.content.res.Configuration
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.CharItemAdapter
import com.mtths.chartracker.dataclasses.Character.Companion.getTotalWealthString
import com.mtths.chartracker.dataclasses.ItemCategory.Companion.walletCategories
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dialogs.DialogShowItems
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.HttpUtils
import kotlinx.coroutines.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

class FragmentCharItems: ViewPagerFragment() {

    private lateinit var backPackContainer: LinearLayout

    private  var slotItemsContainers = LinkedHashMap<Int, LinearLayout>()
    private val c: Character?
        get() = (activity as ActivityCharDetails?)?.character
    private lateinit var downloadButton: Button
    private var mDownloadItemsTask: DownloadItemsTask? = null
    private lateinit var mCharItemAdapter: CharItemAdapter
    private lateinit var buttonStash: ImageView
    private lateinit var buttonWallet: ImageView
    private lateinit var managerIcon: ImageView
    private lateinit var tvCurrencyTotal: TextView
    private lateinit var tvCurrencyTotalStash: TextView
    private lateinit var lm: LootManager
    private lateinit var prefs: PrefsHelper
    private lateinit  var background: LinearLayout
    private lateinit var root: LinearLayout
    override val defaultViewPagerIndex = 0
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val layout = inflater.inflate(R.layout.fragment_char_items, container, false)
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            layout.setBackgroundResource(R.drawable.wallpaper_dnd)
        }
        return layout
    }

    fun showProgressBar(msg: String) {
        try {
            (activity as ProgressBarFrameDropBoxActivity).showProgressBarLoading(msg)
        } catch (e: Exception) {
            Log.e("LOOTMANAGER", "ERROR showing progress bar!")
            e.printStackTrace()
        }
    }

    fun hideProgressBar() {
        try {
            (activity as ProgressBarFrameDropBoxActivity).hideProgressBarLoading()
        } catch (e: Exception) {
            Log.e("LOOTMANAGER", "ERROR hiding progress bar!")
            e.printStackTrace()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        prefs = PrefsHelper(context)
        lm = LootManager(context)
        activity?.let {
            mCharItemAdapter = CharItemAdapter(
                slotItemsContainer = slotItemsContainers,
                backPackItemsContainer = backPackContainer,
                fActivity = it,
                doLootManagerStuff = { doNetwork ->
                    lm.doLootManagerStuff(doNetwork = doNetwork) {
                        downloadCharItems()
                    }
                }
            ).apply {
                onItemEquipped = {downloadCharItems() }
                character = c
            }
        }

        if (prefs.autoDownloadCharItems) {
            downloadCharItems()
        }
        updateDisplay()
    }

    override fun onResume() {
        super.onResume()
        lm.onResume()
        setViewPagerPadding()
    }

    fun updateDisplay() {
        mCharItemAdapter.character = c
        setBackgroundAccordingToRuleSystem()
        updateDownloadButton()
        mCharItemAdapter.inflate()
        updateStashButton()
        updateTotalWealthDisplay()
        updateBackgroundColor()

    }

    fun updateBackgroundColor() {
        val charDetails = (activity as? ActivityCharDetails?)
        charDetails?.setBackgroundColorAccordingToCharStatus(background)
    }

    override fun onDetach() {
        super.onDetach()
        mDownloadItemsTask?.cancel(true)
        mDownloadItemsTask = null
    }

    fun downloadCharItems() {
        mDownloadItemsTask?.cancel(true)
        mDownloadItemsTask = DownloadItemsTask()
        mDownloadItemsTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    private fun updateStashButton() {
        c?.let {
            if (it.loot.map { it.stashes }.flatten().map { it.name }.contains("Trupp")) {
                buttonStash.setImageResource(R.drawable.ic_berzstash)
            } else {
                buttonStash.setImageResource(R.drawable.ic_stash)
            }
            if (it.useManagerAccess) {
                managerIcon.visibility = View.VISIBLE
            } else {
                managerIcon.visibility = View.INVISIBLE
            }
        }
    }

    private fun updateTotalWealthDisplay() {
        c?.let { c ->
            if (c.isManager) {
                CoroutineScope(Dispatchers.Main).launch {
                    var tCurrency = 0f
                    withContext(Dispatchers.IO) {
                        HttpUtils.getCharacterStashItems(c.lootManagerDownloadName)?.let {
                            tCurrency = Item.calcTotalCurrency(
                                items = it,
                                onlyUnclaimed = true,
                                extendedWallet = c.displayTreasureInWallet)
                        }
                    }
                    tvCurrencyTotalStash.text = getTotalWealthString(tCurrency)
                }
            } else {
                tvCurrencyTotalStash.text = ""
            }
            tvCurrencyTotal.text = getTotalWealthString(
                c.totalCurrency(extendedWallet = c.displayTreasureInWallet)
            )

        }
    }



    private fun initInterfaceItems(view: View) {
        root = view.findViewById(R.id.root)
        downloadButton = view.findViewById(R.id.download_button)
        buttonStash = view.findViewById(R.id.search_button)
        buttonWallet = view.findViewById(R.id.wallet_button)
        managerIcon = view.findViewById(R.id.iv_manager_icon)
        tvCurrencyTotal = view.findViewById(R.id.currency_total)
        tvCurrencyTotalStash = view.findViewById(R.id.currency_total_stash)
        backPackContainer= view.findViewById(R.id.items_container)
        slotItemsContainers[CharItemAdapter.RIGHT] = view.findViewById(R.id.char_slots_right)
        slotItemsContainers[CharItemAdapter.LEFT] = view.findViewById(R.id.char_slots_left)
        background = view.findViewById(R.id.background)

        downloadButton.setOnClickListener {
            downloadCharItems()
        }

        buttonStash.setOnClickListener {

            val managerAccess = checkManagerAccessAndRevokeForNotManager()
            c?.let { c ->
                Helper.log("SHOWSTASH", "${c.name} isManager: ${c.isManager}")
                DialogShowItems.newInstance(
                    char_id = c.id,
                    showCharItems = false,
                    manager = managerAccess).apply {
                    onItemEquipped = {downloadCharItems()}
                }
                    .show(childFragmentManager, "showStash")
            }

        }

        buttonWallet.setOnClickListener {

            c?.let { c ->
                DialogShowItems.newInstance(
                    char_id = c.id,
                    showCharItems = true,
                    categories = ArrayList(walletCategories)
                ).apply {
                    onItemEquipped = { downloadCharItems() }
                }
                    .show(childFragmentManager, "showWallet")

            }

        }
        buttonWallet.setOnLongClickListener{
            c?.let {  c ->

                if (checkManagerAccessAndRevokeForNotManager()) {
                    DialogShowItems.newInstance(
                        c.id,
                        showCharItems = false,
                        categories = ArrayList(walletCategories),
                        manager = true)
                        .apply {
                            onItemEquipped = {downloadCharItems()}
                        }
                        .show(childFragmentManager, "showStashWallet")
                } else {
                    Toast.makeText(context, "No Manager Access. Use short click to show wallet or stash button to access the stash.", Toast.LENGTH_SHORT)
                        .show()                    }

            }
            return@setOnLongClickListener true
        }
    }

    fun setViewPagerPadding() {
        background.setPadding(
            0,(activity as ActivityCharDetails).getViewPagerPadding(),0,0
        )
    }

    fun checkManagerAccessAndRevokeForNotManager(): Boolean {
        var managerAccess = false
        c?.let { c ->
            managerAccess = c.useManagerAccess
            // revoke managerAccess after one use if you are not the manager.
            // U have to activate it again
            if (!c.isManager && managerAccess) {
                c.useManagerAccess = false
                DatabaseHelper.getInstance(context)?.updateCharacter(
                    character = c, keep_last_update_date = true)
                Toast.makeText(context, "You have one time manager access.", Toast.LENGTH_SHORT).show()
            }
        }
        return managerAccess
    }

    fun updateDownloadButton() {
        val displayDownloadName = if (c?.lootManagerDownloadName != c?.name) " (${c?.lootManagerDownloadName})" else ""
        downloadButton.text = context?.getString(R.string.download_items_button_lable, displayDownloadName)
    }

    inner class DownloadItemsTask: AsyncTask<Unit, Unit, ArrayList<Item>?>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            downloadButton.setText(R.string.downloading)
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg p0: Unit?): ArrayList<Item>? {
            return c?.let {
                HttpUtils.getCharacterItems(it.lootManagerDownloadName)
            }
        }

        //todo use compose for item list

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(items: ArrayList<Item>?) {
            items?.also {
                backPackContainer.removeAllViews()
                c?.let {   c ->
                    //some items have changed
                    Log.d("LOOTMANAGER", "items have changed. updating loot:")
                    c.loot = it
                    mCharItemAdapter.inflate()
                    DatabaseHelper.getInstance(context)?.updateCharacter(
                        character = c, keep_last_update_date = false)
                    try {
                        (activity as? ActivityCharDetails?)?.apply {
                            updateDisplays(generateCPI = false)
                        }
                    } catch (e: ClassCastException) {
                        Helper.log("ITEMS__", "cant cast activity to ActivityCharDetails")
                        e.printStackTrace()
                    }
                }
            } ?: context?.let {
                Toast.makeText(
                    it,
                    resources.getString(R.string.download_items_error_msg),
                    Toast.LENGTH_LONG)
                    .show()
            }

            updateDownloadButton()
        }


    }

    companion object {
        const val REF = "fragment_char_items"

        fun newInstance(viewPagerIndex: Int): FragmentCharItems {
            val args = Bundle()
            args.putInt(ActivityMain.PARAM_VIEW_PAGER_INDEX, viewPagerIndex)
            val fragment = FragmentCharItems()
            fragment.arguments = args
            return fragment
        }
    }
}
