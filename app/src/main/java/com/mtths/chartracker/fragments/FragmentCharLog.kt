package com.mtths.chartracker.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.mtths.chartracker.*
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.preferences.PrefsHelper.Companion.PREF_CHAR_LOG_MODE_ALL
import com.mtths.chartracker.preferences.PrefsHelper.Companion.PREF_CHAR_LOG_MODE_CHAR_PROGRESS_DATA
import com.mtths.chartracker.preferences.PrefsHelper.Companion.PREF_CHAR_LOG_MODE_STORY
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.LogAdapter
import com.mtths.chartracker.adapters.LogAdapter.Companion.CLOSE_LOGIC_BRACKET
import com.mtths.chartracker.adapters.LogAdapter.Companion.OPEN_LOGIC_BRACKET
import com.mtths.chartracker.dialogs.DialogExportCharLog
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper

class FragmentCharLog : ViewPagerFragment() {
    private lateinit var exportButton: Button
    private lateinit var dataButton: Button
    private lateinit var storyButton: Button
    private lateinit var background: LinearLayout
    private lateinit var root: FrameLayout
    lateinit var fragmentLogView: FragmentLogView

    var dbHelper: DatabaseHelper? = null
    lateinit var prefs: PrefsHelper
    override val defaultViewPagerIndex = 3

    class FragmentCharLogView: FragmentLogView() {
        override var initLogAdapter = {
            LogAdapter(
                (activity as ProgressBarFrameDropBoxActivity),
                R.layout.list_view_item_log,
                LogAdapter.EXP_TEXT
            )
        }
        override var getLogs = {
            (activity as? ActivityCharDetails?)?.charLogs ?: arrayListOf()
        }

        override fun getSearchQuery(): String {
            var query = super.getSearchQuery()

            when (prefs.charLogMode) {
                PREF_CHAR_LOG_MODE_STORY -> {
                    logAdapter.setExcludedWords(Global.charProgressionWords)

                }

                PREF_CHAR_LOG_MODE_CHAR_PROGRESS_DATA -> {
                    logAdapter.setExcludedWords(null)
                    query = "$OPEN_LOGIC_BRACKET$query$CLOSE_LOGIC_BRACKET" +
                            Global.charProgressionWords.joinToString(
                        separator = LogAdapter.OR_SEPARATOR,
                        prefix = " $OPEN_LOGIC_BRACKET",
                        postfix = CLOSE_LOGIC_BRACKET
                    ) { w ->
                        "\"$w\""
                    }.also {
                        Helper.log(
                            "CHAR_PROGRESS_DATA",
                            "filter char logs to show only char prog data: " +
                                    "filer condition: $it"
                        )
                    }
                }

                else -> logAdapter.setExcludedWords(null)

            }
            return query
        }
    }

    /////////////////////////////////// METHODS ////////////////////////////////////////////////////
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dbHelper = getInstance(context)
        prefs = PrefsHelper(context)
        initInterfaceItems(view)
        initFragmentLogView()
        updateInitialDisplay()
        setButtonsOnClick()
        setButtonsColor()
        //        implementClickListenersForLogs();
//they are implemented in LogAdapter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_char_log, container, false)
    }

    override fun onResume() {
        super.onResume()
        setViewPagerPadding()
    }

    fun setViewPagerPadding() {
        background.setPadding(
            2,(activity as ActivityCharDetails).getViewPagerPadding(),2,0
        )
    }

    fun updateBackgroundColor() {
        (activity as ActivityCharDetails?)!!.setBackgroundColorAccordingToCharStatus(background)
    }

    /**
     * update only the displays not needing child fragments to be updated
     * child fragments will not yet be fully initialized when calling onViewCreated
     * they should update their initial display on their own
     *
     */
    private fun updateInitialDisplay() {
        setBackgroundAccordingToRuleSystem()
        updateBackgroundColor()
    }

    /**
     * update ALL the displays including child fragments
     */
    fun updateDisplay() {
        updateInitialDisplay()
        fragmentLogView.refreshDisplays()
    }

    //////////////////////////////////// PRIVATE METHODS /////////////////////////////////////////
    private fun initInterfaceItems(v: View) {
        root = v.findViewById(R.id.root)
        exportButton = v.findViewById(R.id.export_char_log)
        dataButton = v.findViewById(R.id.data_button)
        storyButton = v.findViewById(R.id.story_button)
        background = v.findViewById(R.id.background)
    }

    private fun initFragmentLogView() {
        childFragmentManager.beginTransaction().apply {
            fragmentLogView = FragmentCharLogView()
            replace(R.id.char_log_display, fragmentLogView)
            commit()
        }
    }

    fun setButtonsOnClick() {
        exportButton.setOnClickListener {
            DialogExportCharLog.newInstance().show(childFragmentManager, "DialogExportCharLog")
        }
        dataButton.setOnClickListener {
            prefs.charLogMode = if (prefs.charLogMode == PREF_CHAR_LOG_MODE_CHAR_PROGRESS_DATA) {
                PREF_CHAR_LOG_MODE_ALL
            } else {
                PREF_CHAR_LOG_MODE_CHAR_PROGRESS_DATA
            }
            setButtonsColor()
            fragmentLogView.refreshDisplays()
        }
        storyButton.setOnClickListener {
            prefs.charLogMode = if (prefs.charLogMode == PREF_CHAR_LOG_MODE_STORY) {
                PREF_CHAR_LOG_MODE_ALL
            } else {
                PREF_CHAR_LOG_MODE_STORY
            }
            setButtonsColor()
            fragmentLogView.refreshDisplays()
        }
    }

    fun setActive(b: Button, active: Boolean) {
        b.setBackgroundResource(if (active) {
            R.drawable.button_holo_green_light
        } else {
            R.drawable.button_light_grey
        }
        )

    }

    fun setButtonsColor() {
        when (prefs.charLogMode) {
            PREF_CHAR_LOG_MODE_ALL -> {
                setActive(storyButton, false)
                setActive(dataButton, false)
            }
            PREF_CHAR_LOG_MODE_STORY -> {
                setActive(storyButton, true)
                setActive(dataButton, false)
            }
            PREF_CHAR_LOG_MODE_CHAR_PROGRESS_DATA -> {
                setActive(storyButton, false)
                setActive(dataButton, true)
            }

        }
    }


    companion object {
        const val REF = "fragment_char_log"
        private const val PARAM_CHAR_ID = "char_id"
        fun newInstance(viewPagerIndex: Int): FragmentCharLog {
            val fragment = FragmentCharLog()
            val args = Bundle()
            args.putInt(ActivityMain.PARAM_VIEW_PAGER_INDEX, viewPagerIndex)
            fragment.arguments = args
            return fragment
        }
    }
}