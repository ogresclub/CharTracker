package com.mtths.chartracker.fragments

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mtths.chartracker.*
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.adapters.CharacterAdapter
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.Global.logsFilteredBySession
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.AutoCompleteTextToAddAdapter
import com.mtths.chartracker.utils.Helper.generateCharProgressItems
import com.mtths.chartracker.utils.Helper.isDigit
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.adapters.LogAdapter
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.listeners.ListenerImplShowCharDetails
import com.mtths.chartracker.listeners.ListenerImplToggleCharActivity
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.AutoCompleteUtils
import com.mtths.chartracker.utils.AutoCompleteUtils.Companion.getNumberOfActiveCharsAndSetAttributePointExpCost
import com.mtths.chartracker.utils.AutoCompleteUtils.Companion.implementReplaceTextWithAutoCompleteProposal
import com.mtths.chartracker.utils.AutoCompleteUtils.Companion.proposeExp
import com.mtths.chartracker.utils.CharProgressItemMap
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.SpaceTokenizer
import com.mtths.chartracker.widgets.MyMultiAutoCompleteTextView
import com.mtths.chartracker.widgets.MyMultiAutoCompleteTextViewNonfiltering
import kotlinx.coroutines.launch
import kotlin.math.sqrt


class FragmentMainScreen: ShakeFragment() {

    //////// VARIABLE DECLARATION /////////////////////////////////////////////////////////////////

    //////// VARIABLE DECLARATION /////////////////////////////////////////////////////////////////
    lateinit var experienceToAdd: MyMultiAutoCompleteTextView
    lateinit var textToAdd: MyMultiAutoCompleteTextViewNonfiltering
    lateinit var charDisplay: GridView
    lateinit var historyView: RecyclerView
    lateinit var addButton: Button
    lateinit var root: LinearLayout
    lateinit var prefs: PrefsHelper
    var showAsDialog = false


    private var charDisplayAdapter: CharacterAdapter? = null

    lateinit var logAdapter: LogAdapter
    lateinit var autoCompleteTextAdapter: AutoCompleteTextToAddAdapter

    var dbHelper: DatabaseHelper? = null

    //Autocompletition

    //Autocompletition
    var loadedCharDetailsToAutoComplete = false
    var removeThresholdForAutoCompletition = false

    var containsCharProgressItemValueKeyWord = false
    override val defaultViewPagerIndex = 2

    val DEBUG_TAG_AUTO_COMPLETITION = "AUTO_COMPLETE"

    var mLoadCharsRelatedToSessions: LoadCharsRelatedToSessionsTask? = null
    var mLoadCharProgressItemsForSessionAndValueBasedOnesTask: LoadCharProgressItemsForSessionAndValueBasedOnesTask? = null


    //Listeners
    var mOnCharActivityChangedListener: OnCharActivityChangedListener? = null
    var mOnLogEntryAddedListener: OnLogEntryAddedListener? = null
    var mOnSessionDataLoadedListener: OnSessionStatsLoadedListener? = null


    //////////////////////////////// INTERFACES ///////////////////////////////////////////////////
    interface OnCharActivityChangedListener {
        fun onCharStatsChanged(changedChar: Character, loadSessionDetails: Boolean)
        fun onCharFilterOptionsToggled()
    }

    interface OnLogEntryAddedListener {
        fun onLogEntryAdded(newLogEntry_id: Long)
    }

    interface OnDemandAllDataListener {
        fun onDemandAllData()
    }

    interface OnSessionStatsLoadedListener {
        fun fillSessionStats()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        mOnCharActivityChangedListener = try {
            context as OnCharActivityChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnCharActivityChangedListener")
        }
        mOnLogEntryAddedListener = try {
            context as OnLogEntryAddedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnLogEntryAddedListener")
        }
        mOnSessionDataLoadedListener = try {
            context as OnSessionStatsLoadedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnSessionDataLoadedListener")
        }
    }

    fun setNumColsForChars(context: Context) {
        val numCols: Int = autoSetNumCols(Global.charsRelatedToSession.size, context)
        charDisplay.numColumns = numCols
        charDisplayAdapter?.textSizeFactor = (sqrt(3.toDouble() / numCols.toDouble()).toFloat())
        charDisplayAdapter?.numColsGridView = numCols
    }

    fun getCharProgressItemsMap(): CharProgressItemMap {
        return (activity as ProgressBarFrameDropBoxActivity).charProgressItemsMap
    }

    private fun killListenersAndTasks() {
        mOnCharActivityChangedListener = null
        mOnLogEntryAddedListener = null
        mOnSessionDataLoadedListener = null
        mLoadCharProgressItemsForSessionAndValueBasedOnesTask?.cancel(true)
        mLoadCharProgressItemsForSessionAndValueBasedOnesTask = null
        mLoadCharsRelatedToSessions?.cancel(true)
        mLoadCharsRelatedToSessions = null
    }

    override fun onDetach() {
        super.onDetach()
        killListenersAndTasks()

    }

    override fun onShake(count: Int) {
        if (Helper.activeRules(
            allowedRules = listOf(RuleSystem.rule_system_ogres_club),
            activeRuleSystems = dbHelper?.activeSessions?.map { it.ruleSystem } ?: emptyList()
        )) {
            lifecycleScope.launch {
                (activity as? ActivityMain?)?.crawlOgresClubForData()
            }

        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //////////// ON CREATE VIEW/ INSTANTIATE   ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * load interfaceItems,
     * set adapters for grid and listView
     * initiate DatabaseHelper
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showAsDialog = arguments?.getBoolean(PARAM_AS_DIALOG) ?: false
        prefs = PrefsHelper(context)
        dbHelper = getInstance(context)
        initInterfaceItems(view)
        initAdapters()
        implementToggleCharActivity()
        implementShowCharDetails()
        //        implementLogsClickListeners();
        //they are implemented in Log Adapter
        implementTapAddButton()
        implementDynamicAutocompletition()
        implementTypeCheckingForExp()
        implementReplaceTextWithAutoCompleteProposal(textToAdd, ::getCharProgressItemsMap)
        if (showAsDialog) {
            refreshDisplays(loadSessionDetails = false)
        }
    }

    fun setViewPagerPadding() {
        if (!showAsDialog) {
            if (prefs.edgeToEdge) {
                // remove padding
                view?.findViewById<LinearLayout>(R.id.container)?.apply {
                    setPadding(2, 0, 2, 0)
                }
            }
            root.setPadding(
                0, (activity as ActivityMain).getViewPagerPadding(), 0, 0
            )
        }
    }

    fun adjustUIAccordingToRuleSystem() {
        setBackgroundAccordingToRuleSystem()
        when {
            prefs.rulesSR -> {
                experienceToAdd.setHint(R.string.label_exp_to_add_sr)
            }
            else -> {
                experienceToAdd.setHint(R.string.label_exp_to_add)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        adjustUIAccordingToRuleSystem()
        setViewPagerPadding()
    }


    //////// INIT METHODS //////////////////////////////////////////////////////////////////////////
    fun initInterfaceItems(v: View) { //define UI items
        experienceToAdd = v.findViewById<MyMultiAutoCompleteTextView?>(R.id.editExp).apply {
            if (prefs.rulesSR) {
                setHint(R.string.edit_exp_hint_sr)
            }
        }
        textToAdd = v.findViewById(R.id.editText)
        charDisplay = v.findViewById(R.id.charDisplay)
        context?.let {
            setNumColsForChars(it) }
        historyView = v.findViewById(R.id.historyView)
        addButton = v.findViewById(R.id.add_button)
        root = v.findViewById(R.id.main_screen)
    }

    private fun initAdapters() { //charAdapter
        context?.let {  charDisplayAdapter = CharacterAdapter(
                context = it,
                layoutResource = R.layout.character_small,
                thumbNailQuality = 300,
                deactivateDeadAndRetiredChars = true,
                numColsGridView = charDisplay.numColumns)
        }
        charDisplayAdapter?.initialTextSizeCharName = 14f
        charDisplay.adapter = charDisplayAdapter
        context?.let {
            setNumColsForChars(it) }
        //logAdapter
        logAdapter = LogAdapter((activity as? ProgressBarFrameDropBoxActivity?)!!,
                R.layout.list_view_item_history,
                Global.logsToShow,
                LogAdapter.CHARS_EXP_TEXT
        )
        historyView.apply {
            adapter = logAdapter
            layoutManager = LinearLayoutManager(context)
        }
        //textAdapter
        autoCompleteTextAdapter = AutoCompleteTextToAddAdapter(
                context, R.layout.auto_complete_entry
        )
        textToAdd.setAdapter(autoCompleteTextAdapter)
        experienceToAdd.setTokenizer(SpaceTokenizer())
        experienceToAdd.always_show = true
    }

    ///////// ONCLICK EVENTS DEFINED //////////////////////////////////////////////////////////////
    //// tap add button below! ////////////////////////////////////////////////////////////////////
    fun implementToggleCharActivity() {

        context?.let {cntxt ->
            mOnCharActivityChangedListener?.let {listener ->
                charDisplay.onItemClickListener = ListenerImplToggleCharActivity(
                        cntxt, listener)
            }
        }
    }

    fun implementShowCharDetails() {
        context?.let { charDisplay.onItemLongClickListener = ListenerImplShowCharDetails(it) }
    }


    fun implementDynamicAutocompletition() {
        textToAdd.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                fillAutoCompleteAdapter()
                proposeExp(s = s, maTv = experienceToAdd, cpiMap = getCharProgressItemsMap())
            }
        })
    }

    fun implementTypeCheckingForExp() {
        val msg = "Invalid Input: Only signed numbers allowed"
        val filter =
            InputFilter { source, start, end, dest, dstart, dend ->
                log("expfilter","start: $start, end: $end, source: '$source' dstart: $dstart, dend: $dend")
                if (dstart == 0) {
                    if (source.isNotEmpty() && source[0] !in "-123456789".toCharArray()) {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                        return@InputFilter ""
                    }
                } else {
                    if (source.isNotEmpty() && !isDigit(source[0])) {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                        return@InputFilter ""
                    }
                    for (i in (start + 1) until end) {
                        if (!isDigit(source[i])) { // Accept only letter & digits ; otherwise just return
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                            return@InputFilter ""
                        }
                    }
                }

                null
            }
        experienceToAdd.filters = arrayOf(filter)
    }


    fun autoSetNumCols(numberOfCharsInThisSession: Int, context: Context): Int {
        val MAX_CHAR_PER_ROW_MS: Int = PrefsHelper(context).maxNumColsMainScreen
        var numberOfRows = numberOfCharsInThisSession / MAX_CHAR_PER_ROW_MS + 1
        val numberOfCharNotFillingACompleteRow = numberOfCharsInThisSession % MAX_CHAR_PER_ROW_MS
        if (numberOfCharNotFillingACompleteRow == 0) {
            numberOfRows--
        }
        //        Helper.log("AUTO_COL_MS", "number of char not filling a complete row = " + numberOfCharNotFillingACompleteRow);
        numberOfRows = Math.max(numberOfRows, 1)
        //        Helper.log("AUTO_COL_MS", "number of rows = " + numberOfRows);
        var fillUpTerm = 0
        if (numberOfCharsInThisSession % numberOfRows > 0) {
            fillUpTerm = numberOfRows - numberOfCharsInThisSession % numberOfRows
        }
        //        Helper.log("AUTO_COL_MS", "fill up term = " + fillUpTerm);
        return (numberOfCharsInThisSession + fillUpTerm) / numberOfRows
    }

    fun implementTapAddButton() {
        addButton.setOnClickListener {
            dbHelper?.createLogEntry(
                    context,
                    experienceToAdd,
                    textToAdd,
                Helper.getActiveChars(Global.chars),
                    dbHelper?.getActiveSessionsIds(),
                    mOnLogEntryAddedListener,
                    false);

            charDisplayAdapter?.notifyDataSetChanged();
        };
    }

/////////////////// FILL ADAPTERS   ////////////////////////////////////////////////////////////


    /////////////////// FILL ADAPTERS   ////////////////////////////////////////////////////////////
    inner class LoadCharsRelatedToSessionsTask : AsyncTask<Void?, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void?): Void? {
            if(prefs.showCharsRelatedToSession) {
                dbHelper?.let {
                    if (!prefs.filterSessions) {
                        Global.charsRelatedToSession = it.getAllCharactersMainScreen()

                    } else {
                        Global.charsRelatedToSession = it.getCharsRelatedToSessions(Global.activeSessionIds)
                    }
                }
            } else {
                Global.charsRelatedToSession = ArrayList()
            }
            //            String s = "";
//            for (Character c : Global.charsRelatedToSession) {
//                s += c.name;
//            }
//                Helper.log("MAIN_SCREEN_CHARS", "got chars related to session from db: " + s);
//            }
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) { //            Helper.log("MAIN_SCREEN_CHARS", "setting chars and notifying adapter");
            charDisplayAdapter?.setChars(Global.charsRelatedToSession)
            context?.let { setNumColsForChars(it) }

            charDisplayAdapter?.notifyDataSetChanged()
        }
    }

    /**
     * Load all value based charProgressItems and adds the active one names to the autocompleteadapter
     */
    inner class LoadCharProgressItemsForSessionAndValueBasedOnesTask() : AsyncTask<Boolean?, Void?, ArrayList<String>>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            loadedCharDetailsToAutoComplete = false
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg args: Boolean?): ArrayList<String> {
            var loadSessionItems = true
            if (args.isNotEmpty()) {
                loadSessionItems = args[0] ?: true
            }
            val activeChar = Global.chars.find { it.isActive }
            log("CHAR_DETAILS", "active character = $activeChar")
            log("CHAR_DETAILS", "LOAD_SESSIONS_DETAILS_TASK= $loadSessionItems")

            if (getCharProgressItemsMap().isEmpty() || loadSessionItems) {
                (activity as ActivityMain).createCharProgressItems(
                    character = activeChar)
            }
            /*
            first load characters, places and quests char ProgressItems in general.
            When this is done, load value based charProgressItems if only one character is selected.
             */
            if (loadSessionItems) {
                log("CHAR_DETAILS", "GENERATE SESSION DATA")
                (activity as ActivityMain).recreateCharProgressItems(
                    itemsData = Global.ITEMS_SESSION_DATA,
                    character = activeChar
                )
                Global.characterNamesToAutocomplete.clear()
                Global.characterNamesToAutocomplete.addAll(
                    Global.chars.map {
                        LogAdapter.ColoredString(
                            names = arrayListOf(it.name),
                            color = R.color.charachter_name_color
                        )
                    }
                )
                Global.placesNamesToAutocomplete.clear()


                generateCharProgressItems(
                    logs = logsFilteredBySession,
                    cpiMap = getCharProgressItemsMap(),
                    itemData = Global.ITEMS_SESSION_DATA,
                    activeRuleSystems = prefs.activeRuleSystems,
                    context = requireContext(),
                    generateConnectionChars = true)
            }

            /*
            Now load ValueBased charProgressItems to autocomplete if only one character is selected.
             */
            (activity as ActivityMain).recreateCharProgressItems(
                itemsData = Global.ITEMS_VALUE_BASED_DATA,
                character = activeChar
            )
            if (getNumberOfActiveCharsAndSetAttributePointExpCost() == 1) {
                val logs =
                    Helper.filterByChars(logsFilteredBySession, Helper.getActiveChars(Global.chars))
                generateCharProgressItems(
                    logs = logs,
                    cpiMap = getCharProgressItemsMap(),
                    itemData = Global.ITEMS_VALUE_BASED_DATA,
                    activeRuleSystems = prefs.activeRuleSystems,
                    context = requireContext())
            }

            return ArrayList()
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result_list: ArrayList<String>) {
            fillAutoCompleteAdapter()
            loadedCharDetailsToAutoComplete = true
            mOnSessionDataLoadedListener?.fillSessionStats()
            fillHistoryAdapter()
        }
    }




    fun fillHistoryAdapter() {
        logAdapter.setAndShowAllLogs(Global.logsToShow)
    }

    fun fillAutoCompleteAdapter() {
        AutoCompleteUtils.Companion.GenerateAutoCompleteDataArgs(
            et = textToAdd,
            cpiMap = getCharProgressItemsMap(),
            loadedCharDetailsToAutoComplete = loadedCharDetailsToAutoComplete,
            activeRuleSystems = prefs.activeRuleSystems,
            autoCompleteTextToAddAdapter = autoCompleteTextAdapter
        ).let {
            activity?.lifecycleScope?.launch {
                AutoCompleteUtils.fillAutocompleteData(it)
            }
        }
    }

    private fun logAutoComplete(msg: String) {
//        Helper.log(DEBUG_TAG_AUTO_COMPLETITION, msg)
    }


    fun loadCharDetailsToAutoComplete(loadSessionDetails: Boolean = true) {
        mLoadCharProgressItemsForSessionAndValueBasedOnesTask?.cancel(true)
        mLoadCharProgressItemsForSessionAndValueBasedOnesTask = LoadCharProgressItemsForSessionAndValueBasedOnesTask()
        mLoadCharProgressItemsForSessionAndValueBasedOnesTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, loadSessionDetails)
    }

    fun loadCharsRelatedToSession() {
        mLoadCharsRelatedToSessions?.cancel(true)
        mLoadCharsRelatedToSessions = LoadCharsRelatedToSessionsTask()
        mLoadCharsRelatedToSessions?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }


    fun refreshDisplays(loadSessionDetails: Boolean = true) {
        loadedCharDetailsToAutoComplete = false
        loadCharsRelatedToSession()
        fillHistoryAdapter()
        //        Helper.log(DEBUG_TAG_AUTO_COMPLETITION, "Fill Autocomplete Adapter");
        fillAutoCompleteAdapter()
        loadCharDetailsToAutoComplete(loadSessionDetails)
    }

    companion object {

        const val REF = "fragment_main_screen"
        const val PARAM_AS_DIALOG = "show_as_dialog"
        fun newInstance(viewPagerIndex: Int, asDialog: Boolean = false): FragmentMainScreen {
            val args = Bundle()
            args.putInt(ActivityMain.PARAM_VIEW_PAGER_INDEX, viewPagerIndex)
            args.putBoolean(PARAM_AS_DIALOG, asDialog)
            val fragment = FragmentMainScreen()
            fragment.arguments = args
            return fragment
        }
    }


}
