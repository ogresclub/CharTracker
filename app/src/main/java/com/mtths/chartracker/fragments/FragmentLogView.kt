package com.mtths.chartracker.fragments

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.widget.AppCompatCheckedTextView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.adapters.LogAdapter
import com.mtths.chartracker.databinding.FragmentLogViewBinding
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dialogs.DialogCreateLogEntry
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.dialogs.FragmentDialogWrapper
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.SpaceTokenizer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

open class FragmentLogView : Fragment() {
    private var _binding: FragmentLogViewBinding? = null
    private val binding: FragmentLogViewBinding
        get() = _binding!!
    lateinit var logAdapter: LogAdapter
    private var autocompleteTagsAdapter: ArrayAdapter<String>? = null
    lateinit var prefs: PrefsHelper

    private lateinit var expHintToast: Toast
    private lateinit var dateHintToast: Toast

    /**
     * increase bottom margin of button
     */
    var floatingButtonOffset: Int = 0
        set(value) {
            field = value
            binding.actionButton.updateLayoutParams<RelativeLayout.LayoutParams> {
                bottomMargin += floatingButtonOffset
            }
        }

    /////////////////////////////// INTERFACES //////////////////////////////////////////////////
    interface OnSessionsChangedListener {
        fun onActiveSessionsChanged()
        fun onShowAllSessionsToggled()
    }

    //////////////////////////////////////// METHODS ///////////////////////////////////////////////
// -------------------------------------------------------------------------------------------//
/////////////////////////////// ON CREATE VIEW / NEW INSTANCE /////////////////////////////////////////
// -------------------------------------------------------------------------------------------//
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = FragmentLogViewBinding.inflate(inflater, container, false)
        val root = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefs = PrefsHelper(context)
        initInterfaceItems(view)
        initAdapters()
        updateButtonDisplays()
        initToasts()
        setShowCharProgressionSettingsInEffect()
        updateSearchContextSizeIndicator()
        implementOnChangeOfSearchQuery()
        implementExpandCollapseSearchArea()
        implementDateFunctionality()
        implementSubMatches()
        implementToggleSearchContextSize()
        implementCreateLogEntry()
        implementHideActionButtonOnScroll()
    }

    /////////////////////////////////////// PRIVATE METHODS ////////////////////////////////////////
    private fun initInterfaceItems(v: View) {

        binding.editTextSearchContextSize.setText("${prefs.searchContextSize}")
        binding.searchContextSizeIndicator.text = "${prefs.searchContextSize}"


        binding.buttonClear.setOnClickListener {
            binding.searchQuery.setText("")
        }
        binding.buttonClear.setOnLongClickListener {
            DialogCustom.getAlterDialogBuilder(context)
                .setTitle(R.string.title_confirm_delete)
                .setMessage(context?.getString(
                    R.string.msg_confirm_delete_num_log_entries, logAdapter.itemCount)
                )
                .setPositiveButton(R.string.yes) { _,_ ->
                    CoroutineScope(Dispatchers.Main).launch {
                        val logs = logAdapter.deleteFilteredLogs()

                        (activity as? ActivityMain?)?.updateGlobalLogDisplays(
                            filterSessions = true,
                            filterChars = true,
                            loadSessionDetails = true
                        )
                        (activity as? ActivityCharDetails?)?.onLogEntriesDeleted(logs)

                    }
                }
                .setNegativeButton(R.string.no) { d, _ -> d.dismiss() }
                .show()
            return@setOnLongClickListener true
        }

        updateUI()

    }

    private fun implementHideActionButtonOnScroll() {
        binding.logsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    // Scrolling down
                    binding.actionButton.hide()
                } else {
                    // Scrolling up
                    binding.actionButton.show()
                }
            }
        })
    }

    private fun implementCreateLogEntry() {

        binding.actionButton.apply {

            setOnClickListener {
                when (activity) {
                    is ActivityMain -> FragmentDialogWrapper.newInstance(
                        fragmentClass = FragmentMainScreen::class.java,
                        args = Bundle().apply {
                            putBoolean(FragmentMainScreen.PARAM_AS_DIALOG, true)
                        }
                    ).show(
                        requireActivity().supportFragmentManager,
                        FragmentMainScreen.REF + FragmentDialogWrapper.REF_SUFFIX
                    )
                    else -> DialogCreateLogEntry().show(
                        requireActivity().supportFragmentManager,
                        DialogCreateLogEntry.REF
                    )
                }
            }
        }


    }

    private fun initToasts() {
        expHintToast = toast(msgId = R.string.msg_exp_filter_allowed_digits)
        dateHintToast = toast(msgId = R.string.msg_date_filter_format)
    }

    private fun updateUI() {
        if (!prefs.searchLogIsExpanded) {
            binding.dateFrom.visibility = View.GONE
            binding.dateUntil.visibility = View.GONE
            binding.containerExpSearchContextSize.visibility = View.GONE
            binding.arrowUp.visibility = View.GONE
            binding.arrowDown.visibility = View.VISIBLE
        } else {
            binding.dateFrom.visibility = View.VISIBLE
            binding.dateUntil.visibility = View.VISIBLE
            binding.containerExpSearchContextSize.visibility = View.VISIBLE
            binding.arrowUp.visibility = View.VISIBLE
            binding.arrowDown.visibility = View.GONE
            
            if (prefs.filterSearchResults)  {
                binding.containerSearchContextSize.visibility = View.VISIBLE
                binding.editTextSearchContextSize.visibility = View.VISIBLE
            } else  {
                binding.containerSearchContextSize.visibility = View.GONE
                binding.editTextSearchContextSize.visibility = View.GONE
            }
        }

    }

    fun setShowCharProgressionSettingsInEffect() {
        if (prefs.showCharProgression) {
            logAdapter.setExcludedWords(null)
        } else {
            logAdapter.setExcludedWords(Global.charProgressionWords)
        }
    }

    fun updateButtonDisplays() {
        if (prefs.searchSubMatches) {
            activateCheckBox(binding.submatchesCheckBox, requireContext())
        } else {
            deactivateCheckBox(binding.submatchesCheckBox, requireContext())
        }
    }


    private fun implementDateFunctionality() {
        binding.dateFrom.setOnLongClickListener {
            val oldestLog = Global.logsToShow.mapNotNull { it.dateOfCreation }.minOf {it}
//            log("DATESS", "olderst log date = $oldestLog, " +
//                    "year = ${oldestLog.year + 1900}," +
//                    "month = ${oldestLog.month}," +
//                    "day = ${oldestLog.day +1}",)
            val datePickerDialog = DatePickerDialog(
                requireContext(),
                { view, year, month, dayOfMonth ->
                    val day = if (dayOfMonth>9) "$dayOfMonth" else "0$dayOfMonth"
                    val month = if (month+1>9) "${month+1}" else "0${month+1}"
                    binding.dateFrom.setText("$day-$month-$year")
                },
                oldestLog.year + 1900, oldestLog.month, oldestLog.day +1
            )
            datePickerDialog.show()
            true
        }
        binding.dateUntil.setOnLongClickListener {
            val today = Date()
            log(
                "DATESS",
                "now = $today, " +
                        "year = ${today.year + 1900}," +
                        "month = ${today.month}," +
                        "day = ${today.day + 1}",
            )
            val datePickerDialog = DatePickerDialog(
                requireContext(),
                { view, year, month, dayOfMonth ->
                    val day = if (dayOfMonth>9) "$dayOfMonth" else "0$dayOfMonth"
                    val month = if (month+1>9) "${month+1}" else "0${month+1}"
                    binding.dateFrom.setText("$day-$month-$year")
                },
                today.year + 1900, today.month,
                SimpleDateFormat("dd").format(today).toInt()
            )
            datePickerDialog.show()
            true
        }
    }

    private fun implementSubMatches() {
        binding.submatchesCheckBox.setOnClickListener {
            prefs.searchSubMatches = !prefs.searchSubMatches
            updateSearchContextSizeIndicator()
            completeFillLogAdapter()
            updateButtonDisplays()
        }
    }

    private fun implementToggleSearchContextSize() {
        binding.containerSearchContextSize.setOnClickListener {
            prefs.searchContextSize = 0
            updateSearchContextSize()
        }
    }

    private fun implementExpandCollapseSearchArea() {
        binding.arrowDown.setOnClickListener {
            prefs.searchLogIsExpanded = true
            updateUI()
        }
        binding.arrowUp.setOnClickListener {
            prefs.searchLogIsExpanded = false
            updateUI()
        }
    }

    private fun implementOnChangeOfSearchQuery() {
        val textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                completeFillLogAdapter()
            }
        }
        binding.searchQuery.addTextChangedListener(textWatcher)
        binding.dateUntil.addTextChangedListener(textWatcher)
        binding.dateFrom.addTextChangedListener(textWatcher)
        binding.expFilter.addTextChangedListener(textWatcher)

        binding.editTextSearchContextSize.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val str = s.toString()
                if (str.matches(Regex("\\d+"))) {
                    prefs.searchContextSize = s.toString().toInt()
                    completeFillLogAdapter()
                    updateSearchContextSizeIndicator()
                } else {
                    prefs.searchContextSize = 0
                }

            }
        })
    }

    /**
     * update the textview indicating the search context size
     * as well as in sub matches are active or not
     */
    private fun updateSearchContextSizeIndicator() {
        binding.searchContextSizeIndicator.text = resources.getString(
            R.string.log_view_indicator_template,
            prefs.searchContextSize,
            if (prefs.searchSubMatches) "s" else ""
        )
    }

    fun updateSearchContextSize() {
        updateSearchContextSizeIndicator()
        binding.editTextSearchContextSize.setText(prefs.searchContextSize.toString())
    }

    open var initLogAdapter: () -> LogAdapter = {
        LogAdapter(
            (activity as ProgressBarFrameDropBoxActivity),
            R.layout.list_view_item_log,
            LogAdapter.CHARS_EXP_TEXT
        )
    }

    private fun initAdapters() {
        logAdapter = initLogAdapter()
        binding.logsRecyclerView.apply {
            adapter = logAdapter
            layoutManager = LinearLayoutManager(context)

        }
        context?.let {
            autocompleteTagsAdapter = ArrayAdapter(
                    it, R.layout.auto_complete_entry
            )
        }
        binding.searchQuery.apply {
            setTokenizer(SpaceTokenizer())
            setAdapter<ArrayAdapter<String>>(autocompleteTagsAdapter)
            threshold = 1
        }
    }

    private fun toast(msgId: Int): Toast {
        return Toast.makeText(requireContext(), msgId, Toast.LENGTH_LONG)
    }

    open fun getSearchQuery(): String = binding.searchQuery.text.toString()

    fun getCompleteSearchQuery(): String {
        var completeSearchQuery = getSearchQuery()
        val expFilterText = binding.expFilter.text.toString()
        val dateUntilText = binding.dateUntil.text.toString()
        val dateFromText = binding.dateFrom.text.toString()
        val exp = "exp=$expFilterText"
        val until = "${LogAdapter.UNTIL_INDICATOR}$dateUntilText"
        val from = "${LogAdapter.FROM_INDICATOR}$dateFromText"
        if (LogAdapter.PATTERN_EXP_CONSTRAINT.matcher(exp).find()) {
            completeSearchQuery += " $exp"
            expHintToast.cancel()
        } else if (expFilterText.isNotBlank()) {
            expHintToast.show()
        }
        if(LogAdapter.PATTERN_DATE_CONSTRAINT_FROM.matcher(from).find()) {
            completeSearchQuery += " $from"
            dateHintToast.cancel()
        } else if (dateFromText.isNotBlank()) {
            dateHintToast.show()
        }
        if(LogAdapter.PATTERN_DATE_CONSTRAINT_UNTIL.matcher(until).find()) {
            completeSearchQuery += " $until"
            dateHintToast.cancel()
        } else if (dateUntilText.isNotBlank()) {
            dateHintToast.show()
        }
        if (!prefs.searchSubMatches) {
            completeSearchQuery += "${LogAdapter.SHOW_ONLY_FULL_MATCHES_INDICATOR}true"
        }
        log("LOG_VIEW", "complete search query: $completeSearchQuery")
        return completeSearchQuery
    }

// -------------------------------------------------------------------------------------------//
/////////////////////////////// FILL ADAPTERS /////////////////////////////////////////
// -------------------------------------------------------------------------------------------//

    open var getLogs: () -> ArrayList<LogEntry> = {Global.logsToShow}

    fun completeFillLogAdapter() {
        logAdapter.allowFiltering = prefs.filterSearchResults
        logAdapter.searchContextSize = prefs.searchContextSize
        logAdapter.setLogs(getLogs())
        logAdapter.filter.filter(getCompleteSearchQuery())
    }

    private fun fillAutoCompleteAdapter() {
        autocompleteTagsAdapter?.clear()
        autocompleteTagsAdapter?.addAll(Global.tagsToAutoComplete)
        autocompleteTagsAdapter?.notifyDataSetChanged()
    }

    fun refreshDisplays() { //        initialFillLogAdapter();
        completeFillLogAdapter()
        fillAutoCompleteAdapter()
    }

    companion object {

        const val REF = "fragment_log_view"


        fun newInstance(): FragmentLogView {
            val args = Bundle()
            val fragment = FragmentLogView()
            fragment.arguments = args
            return fragment
        }

        fun activateCheckBox(checkBox: AppCompatCheckedTextView, context: Context) {
            checkBox.setTextColor(context.resources.getColor(android.R.color.holo_green_light))
            checkBox.isChecked = true
        }

        fun deactivateCheckBox(checkBox: AppCompatCheckedTextView, context: Context) {
            checkBox.setTextColor(context.resources.getColor(android.R.color.holo_red_dark))
            checkBox.isChecked = false
        }
    }
}