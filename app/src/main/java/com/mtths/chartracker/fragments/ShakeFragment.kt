package com.mtths.chartracker.fragments

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.mtths.chartracker.utils.ShakeDetector
import com.mtths.chartracker.utils.ShakeDetector.OnShakeListener

open class ShakeFragment : ViewPagerFragment(), OnShakeListener {
    private var mSensorManager: SensorManager? = null
    private var mAccelerometer: Sensor? = null
    private var mShakeDetector: ShakeDetector? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        implementShakeDetection()
    }

    override fun onResume() {
        super.onResume()
        mSensorManager!!.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onPause() {
        super.onPause()
        mSensorManager!!.unregisterListener(mShakeDetector)
    }

    private fun implementShakeDetection() { // ShakeDetector initialization
        mSensorManager = requireActivity().getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mAccelerometer = mSensorManager!!
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mShakeDetector = ShakeDetector()
        mShakeDetector!!.setOnShakeListener(this)
    }

    override fun onShake(count: Int) {}
}