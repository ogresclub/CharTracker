package com.mtths.chartracker.fragments

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.GridView
import android.widget.LinearLayout
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.adapters.CharacterAdapter
import com.mtths.chartracker.dialogs.DialogNewChar.Companion.newInstance
import com.mtths.chartracker.fragments.FragmentMainScreen.OnCharActivityChangedListener
import com.mtths.chartracker.utils.Helper.sortChars
import com.mtths.chartracker.listeners.ListenerImplShowCharDetails
import com.mtths.chartracker.listeners.ListenerImplToggleCharActivity
import com.mtths.chartracker.dialogs.DialogDataOptions
import com.mtths.chartracker.preferences.PrefsHelper

class FragmentCharacterView : ViewPagerFragment() {
    //////////////////////////////////////// VARIABLES /////////////////////////////////////////////
    private lateinit var charDisplay: GridView
    private lateinit var dataButton: Button
    private lateinit var newCharButton: Button
    private lateinit var root: LinearLayout
    private var charDisplayAdapter: CharacterAdapter? = null
    private var mOnCharActivityChangedListener: OnCharActivityChangedListener? = null
    private var mLoadAllCharsTask: LoadAllCharsTask? = null
    private lateinit var prefs: PrefsHelper

    ////////////////////////////////////////// METHODS /////////////////////////////////////////////

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_character_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefs = PrefsHelper(context)
        mLoadAllCharsTask = LoadAllCharsTask()
        initInterfaceItems(view)
        implementOpenCreateNewCharActivity()
        implementShowDataOption()
        implementToggleCharActivity()
        implementShowCharDetails()
        initAdapter()
    }


    override fun onResume() {
        super.onResume()
        setBackgroundAccordingToRuleSystem()
        setViewPagerPadding()
    }

    fun setViewPagerPadding() {
        root.setPadding(
            2,(activity as ActivityMain).getViewPagerPadding(),2,0
        )

    }

    //////////////////////////////////////// INTERFACE STUFF ///////////////////////////////////////
    override fun onAttach(context: Context) {
        super.onAttach(context)
        // This makes sure that the container activity has implemented
// the callback interface. If not, it throws an exception
        mOnCharActivityChangedListener = try {
            context as OnCharActivityChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnCharActivityChanged")
        }
    }

    fun setNumColsForChars(context: Context?) {
        val prefs = PrefsHelper(context)
        val numCols = prefs.numColsCharView
        charDisplay.numColumns = numCols
        charDisplayAdapter?.textSizeFactor = 3f / numCols.toFloat()
        charDisplayAdapter?.numColsGridView = numCols
    }

    override fun onDetach() {
        super.onDetach()
        mOnCharActivityChangedListener = null
    }

    override fun onDestroy() {
        super.onDestroy()
        mLoadAllCharsTask?.cancel(true)
    }

    ////////////////////////////////////// PRIVATE METHODS /////////////////////////////////////////
    private fun initInterfaceItems(v: View) {
        root = v.findViewById(R.id.root)
        charDisplay = v.findViewById(R.id.charDisplay)
        setNumColsForChars(context)
        dataButton = v.findViewById(R.id.data_button)
        newCharButton = v.findViewById(R.id.newCharButton)
    }

    private fun initAdapter() {
        context?.let { charDisplayAdapter = CharacterAdapter(
            context = it,
            layoutResource = R.layout.character,
            thumbNailQuality = 500,
            deactivateDeadAndRetiredChars = true,
            numColsGridView = charDisplay.numColumns)
        }
        charDisplayAdapter?.initialTextSizeCharName = 18f
        setNumColsForChars(context)
        charDisplay.adapter = charDisplayAdapter
    }

    fun fillAdapter() {

        mLoadAllCharsTask?.cancel(true)
        mLoadAllCharsTask = LoadAllCharsTask()
        mLoadAllCharsTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    private fun implementOpenCreateNewCharActivity() {
        newCharButton.setOnClickListener {
            Log.e("IMPORT_GENESIS", "stats: ${DatabaseHelper.getInstance(context)?.getCharsRelatedToSessions(Global.activeSessionIds)?.joinToString { "${it.name} stats: ${it.stats}" }}")
            (activity as ActivityMain?)!!.killRefreshDisplayTimer()
            fragmentManager?.let { fragmentManager ->
                newInstance().show(fragmentManager, "newChar") }

        }
    }

    private fun implementShowDataOption() {
        dataButton.setOnClickListener {
            (activity as? ActivityMain?)?.killRefreshDisplayTimer()

            DialogDataOptions.newInstance().show(childFragmentManager, DialogDataOptions.FRAGMENT_TAG)


        }
    }

    private fun implementToggleCharActivity() {
        context?.let {cntxt ->
            mOnCharActivityChangedListener?.let {listener ->
                charDisplay.onItemClickListener = ListenerImplToggleCharActivity(
                    cntxt, listener)
            }
        }
    }

    private fun implementShowCharDetails() {
        context?.let {  charDisplay.onItemLongClickListener = ListenerImplShowCharDetails(it) }
    }

    private inner class LoadAllCharsTask : AsyncTask<Void, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void): Void? {
            DatabaseHelper.getInstance(context)?.let { dbhelper ->
                if (prefs?.let { !it.showOnlyActiveSessionsCharacters || !it.filterSessions } != false) {
                    Global.chars = dbhelper.allCharacters

                } else {
                    Global.chars = dbhelper.getCharsRelatedToSessions(
                        Global.activeSessionIds, show_all_chars = true)

                }
                sortChars(Global.chars)
            }
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            charDisplayAdapter?.setChars(Global.chars)
            charDisplayAdapter?.notifyDataSetChanged()
        }
    }

    companion object {
        const val REF = "fragment_character_view"
        fun newInstance(viewPagerIndex: Int): FragmentCharacterView {
            val args = Bundle()
            args.putInt(ActivityMain.PARAM_VIEW_PAGER_INDEX, viewPagerIndex)
            val fragment = FragmentCharacterView()
            fragment.arguments = args
            return fragment
        }
    }
}