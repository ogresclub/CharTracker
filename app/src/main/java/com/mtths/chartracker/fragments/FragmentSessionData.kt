package com.mtths.chartracker.fragments

import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.preferences.PrefsHelper.Companion.PREF_TAGS_SORT_ORDER_ALPHABETICAL
import com.mtths.chartracker.preferences.PrefsHelper.Companion.PREF_TAGS_SORT_ORDER_CHRONOLOGICAL
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.widgets.SquareImageView

class FragmentSessionData: ViewPagerFragment() {
    lateinit var root: LinearLayout
    lateinit var background: LinearLayout
    var dbHelper: DatabaseHelper? = null
    lateinit var prefs: PrefsHelper
    lateinit var sessionTagsDisplay: TextView
    var mLoadTagsRelatedToSessionTask: LoadTagsRelatedToSessionTask? = null
    lateinit var sessionStatsContainerMap: HashMap<Int, LinearLayout>
    lateinit var iconSortAlpha: SquareImageView
    lateinit var iconSortChrono: SquareImageView
    override val defaultViewPagerIndex = 1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_session_data, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        dbHelper = DatabaseHelper.getInstance(requireContext())
        prefs = PrefsHelper(requireContext())
        setOnClicklisteners()

    }

    override fun onResume() {
        super.onResume()
        setBackgroundAccordingToRuleSystem()
        setViewPagerPadding()
    }

    override fun onDestroy() {
        super.onDestroy()
        mLoadTagsRelatedToSessionTask = null
    }

    fun initInterfaceItems(view: View) {
        root = view.findViewById(R.id.root)
        background = view.findViewById(R.id.background)
        sessionTagsDisplay = view.findViewById(R.id.session_tags_display)
        iconSortAlpha = view.findViewById(R.id.icon_sort_alphabetically)
        iconSortChrono = view.findViewById(R.id.icon_sort_history)


        sessionStatsContainerMap = HashMap<Int, LinearLayout>().also {
            it[CharProgressItem.POSITION_LEFT] = view.findViewById(R.id.session_stats_container_left)
            it[CharProgressItem.POSITION_MIDDLE] = view.findViewById(R.id.session_stats_container_middle)
            it[CharProgressItem.POSITION_RIGHT] = view.findViewById(R.id.session_stats_container_right)
        }
    }

    fun setOnClicklisteners() {
        iconSortAlpha.setOnClickListener {
            prefs.tagsSortOrder = PREF_TAGS_SORT_ORDER_ALPHABETICAL
            loadTags()
        }
        iconSortChrono.setOnClickListener {
            prefs.tagsSortOrder = PREF_TAGS_SORT_ORDER_CHRONOLOGICAL
            loadTags()
        }
    }

    fun setViewPagerPadding() {
            background.setPadding(
                2,
                (activity as ProgressBarFrameDropBoxActivity).getViewPagerPadding(),
                2,
                0
            )
    }

    fun updateBackgroundColor() {
        val charDetails = (activity as? ActivityCharDetails?)
        charDetails?.setBackgroundColorAccordingToCharStatus(background)
    }

    fun loadTags() {
        mLoadTagsRelatedToSessionTask?.cancel(true)
        mLoadTagsRelatedToSessionTask = LoadTagsRelatedToSessionTask()
        mLoadTagsRelatedToSessionTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    fun fill(){
        loadTags()
//        log("SESSION_DATA: filling data")
        for (container in sessionStatsContainerMap.values) {
            container.removeAllViews()
        }
        val landscapeMode = Helper.useLandscapeMode(resources)
        val orientation = resources.configuration.orientation

        for (itemData in Global.ITEMS_SESSION_DATA) {
            (activity as ProgressBarFrameDropBoxActivity).
            charProgressItemsMap[itemData.filterConstraintId]?.mainItem?.let { item ->

                val expand = prefs.getExtend(
                    charProgItemFilterConstraintId = item.filterConstraintId,
                    orientation = orientation)


                if (landscapeMode) {
                    sessionStatsContainerMap[item.positionSessionDataLandscape]?.addView(
                        item.adapter?.inflate(expand = expand)
                    )
                } else {
                    sessionStatsContainerMap[CharProgressItem.POSITION_LEFT]?.addView(
                        item.adapter?.inflate(expand = expand)
                    )
                }
            }

        }
    }

    inner class LoadTagsRelatedToSessionTask: AsyncTask<Unit, Unit, ArrayList<String>>() {

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg p0: Unit?): ArrayList<String> {
            return dbHelper?.getTagNamesRelatedToSessions(Global.activeSessionIds) ?: ArrayList()
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: ArrayList<String>?) {
            result?.let { tags ->
                if (tags.isNotEmpty()) {
                    if (prefs.tagsSortOrder == PREF_TAGS_SORT_ORDER_ALPHABETICAL) {
                        tags.sort()
                    }
                    val tags_list = tags.joinToString()
                    val spanString = SpannableString(tags_list)
                    for (tag in tags) {
                        val clickableSpan: ClickableSpan = object : ClickableSpan() {
                            override fun onClick(widget: View) {
                                fragmentManager?.let { fragmentManager ->
                                    DialogShowFilteredLogs.newInstance(
                                        searchString = tag,
                                        activity_char_details = false,
                                        use_session_logs = true)
                                        .show(fragmentManager, "showTags")}

                            }
                        }
                        val start = tags_list.indexOf(tag)
                        spanString.setSpan(clickableSpan, start, start + tag.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                        spanString.setSpan(ForegroundColorSpan(Color.BLACK), start, start + tag.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                    }
                    sessionTagsDisplay.text = spanString
                    sessionTagsDisplay.movementMethod = LinkMovementMethod.getInstance()
                } else {
                    sessionTagsDisplay.setText(R.string.none)
                }
            }
        }
    }


    companion object {

        const val REF = "fragment_session_data"
        fun newInstance(viewPagerIndex: Int): FragmentSessionData {
            val args = Bundle()
            args.putInt(ActivityMain.PARAM_VIEW_PAGER_INDEX, viewPagerIndex)
            val fragment = FragmentSessionData()
            fragment.arguments = args
            return fragment
        }
    }


}