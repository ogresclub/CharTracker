package com.mtths.chartracker.fragments

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.InputType
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.GridView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.preferences.PrefsHelper.Companion.DEFAULT_PREF_SELECTED_DAMAGE_TYPE
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.adapters.DamageMonitorAdapter
import com.mtths.chartracker.adapters.ImagedTextAdapter
import com.mtths.chartracker.databinding.FragmentDamageMonitorBinding
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.Character.Companion.REF_DAMAGE_REF_MAGIC
import com.mtths.chartracker.dataclasses.Character.Companion.REF_DAMAGE_REF_MATRIX
import com.mtths.chartracker.dataclasses.Character.Companion.REF_DAMAGE_REF_PHYSICAL
import com.mtths.chartracker.dataclasses.Character.Companion.REF_DAMAGE_REF_STUN
import com.mtths.chartracker.dataclasses.DamageMonitorData
import com.mtths.chartracker.dataclasses.ImagedText
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dialogs.DialogChangeCharacterStatus
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.utils.Helper
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.min

class FragmentDamageMonitor : Fragment() {
    private var _binding: FragmentDamageMonitorBinding? = null
    private val binding get() = _binding!!
    private lateinit var damageAdapter: DamageMonitorAdapter

    private lateinit var prefs: PrefsHelper
    private var ruleSystem = RuleSystem.rule_system_sr6
    private var itemSize = 70f
    var columns = 3

    private var data: DamageMonitorData? = null
    private var isDialog = false
    private var inverseIndex = 0

    val mOnStatusMonitorChangedHandler = Handler(Looper.getMainLooper())
    val mOnStatusMonitorChangedRunnable = Runnable {
        mOnStatusMonitorChangedListener?.onStatusMonitorChanged()
    }

    val character: Character?
        get() = (activity as? ActivityCharDetails?)?.character

    private var mOnStatusMonitorChangedListener: FragmentStatusMonitor.OnStatusMonitorChangedListener? = null


    companion object {

        private const val ARG_DATA = "arg_data"
        private const val ARG_DIALOG = "arg_dialog"
        private const val ARG_INVERSE_INDEX = "inverse_index"

        fun newInstance(
            data: DamageMonitorData,
            isDialog: Boolean =  false,
            /**
             * this is the inverse index of this instance if multiple damage monitors are used below each other
             * this is only need because in dialogs the set height needs some weird correction
             * I don't understand
             */
            inverseIndex: Int = 0
        ): FragmentDamageMonitor {
            val fragment = FragmentDamageMonitor()
            val args = Bundle()
            args.putParcelable(ARG_DATA, data)
            args.putBoolean(ARG_DIALOG, isDialog)
            args.putInt(ARG_INVERSE_INDEX, inverseIndex)
            fragment.arguments = args
            return fragment
        }

        fun modifyEditTextValue(eT: EditText, value: Int) {
            eT.apply {
                val cVal = text.toString().toIntOrNull() ?: 0
                setText((cVal + value).toString())
            }
        }

        fun modifyDamageEditTextValues(
            damageEditTextMap: HashMap<DamageMonitorAdapter.Damage, EditText>,
            damageValueMap: IntArray,
            factor: Int = 1) {
            damageEditTextMap.forEach {
                modifyEditTextValue(
                    it.value,
                    (damageValueMap.getOrNull(it.key.index) ?: 0) * factor)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = FragmentDamageMonitorBinding.inflate(inflater, container, false)
        val view = binding.root
        prefs = PrefsHelper(context)
        ruleSystem = character?.ruleSystem ?: prefs.ruleSystem
        setParams()
        fillNameLabels()
        initAdapter()
        implementShowDialogAddDamage()
        implementClearDamage()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        character?.let { c ->
            implementAddDamageOnClick()
            implementToggleAddReplace()
        }
        data?.hp?.let { hp ->
            if (hp > 0) {
                setData()
            }
        }
    }

    fun implementClearDamage() {
        binding.tvClear.setOnLongClickListener {
            damageAdapter.clearAllDamage()
            onDamageChanged(delayed = 0)
            return@setOnLongClickListener true
        }
    }

    private fun getAddDamageDialogTitle(suffix: String = ""): String {
        /*
         * another option would be to put the add damage dialog header in the damage monitor data class
         */
        val ctx = requireContext()
        data?.let { data ->
            val title =  when(data.damageRef) {
                REF_DAMAGE_REF_MAGIC ->  ctx.getString(
                    R.string.label_spend_something,
                    data.nameId?.let { ctx.getString(it) } ?: data.name )
                else -> ctx.getString(
                    R.string.label_add_damage,
                    data.nameId?.let { ctx.getString(it) } ?: data.name)
            }
            return "$title$suffix"
        }

        return "Add Damage"

    }

    /**
     * inflate a toggle to apply damage reduction, but only for physical damage
     * and only if damage reduction is not 0
     */
    fun inflateSwitchApplyDamageReduction(container: ViewGroup) {

        if (data?.damageRef == REF_DAMAGE_REF_PHYSICAL &&
            (character?.damageReduction ?: 0) > 0) {

            (LayoutInflater.from(context).inflate(
                R.layout.settings_switch_nobg, null, false
            ) as SwitchCompat).apply {
                isChecked = character?.getPrefs(context)?.applyDamageReductionInDialogAddDamage ?: false
                setText(R.string.switch_label_apply_damage_reduction)
                val margin = resources.getDimension(R.dimen.big_text).toInt()
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                ).apply {
                    setMargins(margin, 10, margin, 0)
                }

                textSize = Helper.pxToDp(resources.getDimension(R.dimen.big_text), context)
                setOnClickListener {
                    character?.getPrefs(context)?.applyDamageReductionInDialogAddDamage = isChecked
                }
                container.addView(this)
            }
        }
    }

    /**
     * apply Damage Reduction, but only, if damage is positiv
     * healing (negative damage) is never modified by Dr
     */
    private fun applyDr(damage: Int, dr: Int): Int  = if (damage > 0) {
            max(damage - dr, 0)
        } else {
            damage
        }


    fun showDialogAddDamage(
        initValues: IntArray? = null,
        msg: String = "",
        headerSuffix: String = "",
        createCustomStartView:
            (damageEditTextMap: HashMap<DamageMonitorAdapter.Damage, EditText>) -> View? = {null})
    {
        DialogCustom.getAlterDialogBuilder(context).apply {
            setTitle(getAddDamageDialogTitle(suffix = headerSuffix))
            if (msg.isNotBlank()) {
                setMessage(msg)
            }
            setNegativeButton(R.string.cancel) { d, _ -> d.dismiss() }
            val container = LinearLayout(context).apply {
                orientation = LinearLayout.VERTICAL
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                )
                val padding = context.resources.getDimension(R.dimen.char_progress_list_item_padding_big).toInt()
                setPadding(padding, padding, padding, padding)
            }

            val scrollView = ScrollView(context).apply {
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                )
            }
            scrollView.addView(container)
            // create label and edit text for each damage type
            val damageEditTextMap = HashMap<DamageMonitorAdapter.Damage, EditText>()
            damageAdapter.damages.forEach {  damage ->

                container.addView(
                    layoutInflater.inflate(
                        R.layout.list_view_item_imaged_labeled_edit_view_signed_number,
                        null,
                        false
                    ).apply {
                        findViewById<ImageView>(R.id.icon).setImageResource(damage.imageResource)
                        findViewById<TextView>(R.id.text).apply {
                            text = damage.getName(context)
                            if (damageAdapter.damages.size == 1) visibility = View.GONE
                        }
                        damageEditTextMap[damage] = findViewById<EditText?>(R.id.edit_text).apply {
                            inputType = InputType.TYPE_NUMBER_FLAG_SIGNED or InputType.TYPE_CLASS_NUMBER
                            initValues?.getOrNull(damage.index)?.toString().let { setText(it)}
                            if (damage.index == 0) {requestFocus()}
                        }
                    }
                )
            }
            inflateSwitchApplyDamageReduction(container)
            createCustomStartView(damageEditTextMap)?.let {
                container.addView(it, 0)
            }
            setView(scrollView)
            setPositiveButton(R.string.ok) { d, _ ->
                val DamageValuesMap = damageEditTextMap.mapValues { it.value.text.toString().toIntOrNull() ?: 0 }
                val dr = character?.let {
                    if (it.getPrefs(context).applyDamageReductionInDialogAddDamage) {
                        it.damageReduction
                    } else 0
                } ?: 0
                val totalExpectedDamage = DamageValuesMap.keys.sumOf { it.amount } + //existing damage
                        applyDr(damage = DamageValuesMap.values.sum(), dr ) //damage added
                if (totalExpectedDamage > damageAdapter.size) {
                    Toast.makeText(
                        context,
                        context.getString(R.string.you_are_dead),
                        Toast.LENGTH_SHORT).show()
                    (activity as? ActivityCharDetails?).let { a ->
                        DialogChangeCharacterStatus
                            .killCharacterAndCreateLogEntryForDeath(
                                character=character,
                                context = context,
                                fragmentManager = childFragmentManager,
                                onCharStatusChangedListener = a as? DialogChangeCharacterStatus.OnCharStatusChangedListener,
                                onLogEntryAddedListener = a as? FragmentMainScreen.OnLogEntryAddedListener
                            )
                    }

                }
                /*
                !!BEWARE!!
                !!!!!!!!!! Damage reduction is applied to all damage types individually !!!!!!!!!
                this means is might be applied multiple times
                 */
                DamageValuesMap.keys.forEach { damage ->
                    val rawDamageToAdd = (DamageValuesMap[damage] ?: 0)
                    //when damage is positive, (maybe) apply dr, if negative (healing) ignore dr
                    val damageToAdd = applyDr(damage = rawDamageToAdd, dr = dr)
                    damage.amount = max(0, damage.amount + damageToAdd)
                    damageAdapter.damages[damage.index] = damage
                }
                onDamageChanged()

            }
            create()
        }.show()
    }

    /**
     * display a dialog to add certain number a damages of each type at one
     */
    fun implementShowDialogAddDamage() {
        // todo store history or store presets
        binding.plusButton.setOnClickListener {
            showDialogAddDamage()
        }
    }

    fun setParams() {
        arguments?.let { args ->
            data = when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU ->
                    args.getParcelable(ARG_DATA, DamageMonitorData::class.java)
                else -> args.getParcelable(ARG_DATA)
            }
            isDialog =args.getBoolean(ARG_DIALOG)
            inverseIndex = args.getInt(ARG_INVERSE_INDEX)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mOnStatusMonitorChangedListener = try {
            context as FragmentStatusMonitor.OnStatusMonitorChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(
                context.toString()
                        + " must implement OnStatusMonitorChangedListener"
            )
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnStatusMonitorChangedListener = null
    }

    fun updateDisplays() {
        damageAdapter.notifyDataSetChanged()
    }

    fun applyDamageFromCharacter() {
        character?.let { c ->
            data?.damageRef?.also { damageRef ->
                c.damages[damageRef]?.let { damages ->
                    damageAdapter.damages.apply {
                        damages.forEachIndexed { index, d ->
                            this[index].amount = d
                        }
                    }
                    damageAdapter.notifyDataSetChanged()
                }
            } ?: run {
                Log.e("DAMAGE_MONITOR", "cant set damage. no damage ref was given")
            }

        }
    }

    fun setData() {
        setHp()
        applyDamageFromCharacter()
    }

    fun setHp() {

        var hp = data?.hp ?: 0

        /*
        give those predefined damageRefs, we'll use character stats for hp
        and ignore data hp value (which should not be given anyway in this case)
         */

        character?.let { c ->
            hp = when (data?.damageRef) {
                REF_DAMAGE_REF_PHYSICAL -> c.hp
                REF_DAMAGE_REF_STUN -> c.stun_hp
                REF_DAMAGE_REF_MATRIX -> {

                    //hide matrix monitor, if matrix device rating = 0 (no device is used)
                    binding.container.visibility = if (c.matrix_device_rating == 0) {
                        View.GONE
                    } else {
                        View.VISIBLE
                    }

                    c.matrix_hp
                }

                REF_DAMAGE_REF_MAGIC -> {
                    when (ruleSystem) {
                        RuleSystem.rule_system_splittermond -> c.fokusPunkte
                        RuleSystem.rule_system_skillfull_dnd_5E -> c.magic_points
                        else -> 0
                    }
                }

                else -> hp
            }
        }

        val size = when (data?.damageRef) {
            REF_DAMAGE_REF_PHYSICAL -> {
                hp + (character?.maxOverFlow?: 0)
            }
            else -> hp
        }




        damageAdapter.apply {
            totalHp = hp
            this.size = size

        }

        /*
        -------------------  Set Number of columns depending of parameters  -------------------
         */
        data?.colCount?.also {
            columns = it
        } ?: run {
            //row count is not fixed, so will be maxed to fill container
            columns = min(calcMaxNumCols(FragmentStatusMonitor.DEFAULT_ITEM_SIZE), size)
        }

        if (data?.splittermondHp == true) {
            var cols = hp / (character?.splittermondHpMultiplier ?: 1)
            itemSize = min(
                maxColSize(numcols = cols),
                FragmentStatusMonitor.DEFAULT_ITEM_SIZE
            )
            while (itemSize < FragmentStatusMonitor.DEFAULT_ITEM_SIZE / 2) {
                cols /= 2
                itemSize = min(
                    maxColSize(numcols = cols),
                    FragmentStatusMonitor.DEFAULT_ITEM_SIZE
                )
            }
            columns = min(calcMaxNumCols(itemSize), cols)
        }

        val numRows = ceil(size.toFloat() / columns).toInt()
        val height = numRows * itemSize
        val dialogCorrection = if (isDialog && inverseIndex == 1) 0.15f * numRows * itemSize else 0f

        setHeight(
            gridView = binding.damageMonitorGrid,
            height = height + dialogCorrection,
            columns = columns,
            itemSize = itemSize
        )
    }


    private fun setHeight(gridView: GridView, height: Float, columns: Int, itemSize: Float) {
        val layoutParams = LinearLayout.LayoutParams(
            Helper.dpToPx(getWidth(columns, itemSize), requireContext()),
            Helper.dpToPx(height, requireContext())
        ).apply {
            gravity = Gravity.CENTER
            bottomMargin = Helper.dpToPx(10f, requireContext())
        }
        gridView.layoutParams = layoutParams
        gridView.numColumns = columns
    }

    fun getWidth(columns: Int, itemSize: Float): Float {
        return min(getParentContainerWidth(), itemSize*columns)
    }

    /**
     * in dp
     */
    fun getParentContainerWidth(): Float {
        return if (isDialog){
            DisplayMetrics().apply {
                (context as Activity).windowManager.defaultDisplay.getMetrics(this)
            }.widthPixels.toFloat().let { width ->
                Helper.pxToDp(width, requireContext())
            }

        } else {
            Helper.pxToDp(binding.container.width.toFloat(), requireContext())
        }
    }

    private fun calcMaxNumCols(itemSize: Float): Int {
        return ((getParentContainerWidth()- 30) / itemSize).toInt()
    }

    private fun maxColSize(numcols: Int): Float  {
        return getParentContainerWidth() / numcols - 5
    }

    fun onDamageChanged(delayed: Long = 1000) {
        damageAdapter.damages
            .map { it.amount }
            .toIntArray()
            .let { damages ->

                data?.damageRef?.let {
                    character?.damages?.put(it, damages)
                    Log.d("EXPORT_DAMAGES", "exporting damages $it [${damages.joinToString(",")}], character damages = ${Gson().toJson(character?.damages)}")
                }
                damageAdapter.notifyDataSetChanged()
                mOnStatusMonitorChangedHandler.removeCallbacks(mOnStatusMonitorChangedRunnable)
                mOnStatusMonitorChangedHandler.postDelayed(mOnStatusMonitorChangedRunnable, delayed)
            }
    }

    private fun implementAddDamageOnClick() {


        binding.damageMonitorGrid.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                damageAdapter.apply {
                    val type = selectedDamageType
                    val damage: Int = damages.getOrNull(type)?.amount ?: 0
                    if (position + 1 - getDamageUntil(type) > damage) {
                        addDamagePoint()
                    } else {
                        healDamagePoint()
                    }
                }
                onDamageChanged()

            }
        binding.damageMonitorGrid.onItemLongClickListener =
            AdapterView.OnItemLongClickListener { parent, view, position, id ->
                damageAdapter.apply {
                    putDamage(position + 1 - getDamageUntil(selectedDamageType))
                }
                onDamageChanged()
                return@OnItemLongClickListener true
            }
    }

    private fun initAdapter() {
        context?.let {
            val size = when (data?.damageRef) {
                REF_DAMAGE_REF_STUN -> 15
                REF_DAMAGE_REF_PHYSICAL -> 21
                else -> 15
            }
            damageAdapter = DamageMonitorAdapter(it, size) // Adjust the size and parameters as needed
            binding.damageMonitorGrid.adapter = damageAdapter
        }

        data?.damages?.let {
            binding.damageTypeContainer.visibility = View.VISIBLE
            damageAdapter.damages = it
        } ?: run {
            binding.damageTypeContainer.visibility = View.GONE
        }


        damageAdapter.selectedDamageType =
            prefs.selectedDamageTypes[data?.damageRef] ?: DEFAULT_PREF_SELECTED_DAMAGE_TYPE


        binding.damageTypeSpinner.adapter = ImagedTextAdapter(
            requireContext(),
            R.layout.list_view_item_imaged_text,
            R.layout.support_simple_spinner_dropdown_item,
            damageAdapter.damages.map {
                ImagedText(text = it.getName(requireContext()), icon = it.imageResource) }
        )
        binding.damageTypeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                damageAdapter.selectedDamageType = position
                data?.damageRef?.let {
                    prefs.selectedDamageTypes[it] = position
                }
                Log.d("STATUS_MONITOR", "selected ${data?.damageRef} damage type: $position, " +
                        "name = ${context?.getString(damageAdapter.damages[position].nameResourceId)}")
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    private fun implementToggleAddReplace() {
        binding.addReplaceToggle.apply {

            val doReplace = {
                data?.damageRef?.let {
                    prefs.damageMonitorModi[it]
                } ?: false
            }


            fun setText(){
                text = context?.getString(
                    if (doReplace()) R.string.replace_label else R.string.label_add
                )
            }

            setText()
            damageAdapter.doReplace = doReplace

            setOnClickListener {
                data?.damageRef?.let { prefs.toggleDamageMonitorModus(it) } ?:
                Log.e("FRAGMENT_DAMAGE_MONITOR",
                    "no damage type was defined. Did you forget the param in newInstance " +
                            "or did not create fragment with newInstance method?")
                setText()
            }
        }
    }

    private fun fillNameLabels() {

        binding.nameLabel.apply {
            val mName = data?.nameId?.let { nameId ->
                context.getString(nameId)
            } ?: run {
                data?.name
            }
            text = context.resources.getString(
                R.string.monitor_label,
               mName
            )
        }

    }
}