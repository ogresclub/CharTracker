package com.mtths.chartracker.fragments

import androidx.fragment.app.Fragment
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.activities.ActivityMain

/**
 * a fragment that is used in a viewpager with one landscape background
 * split into multiple parts for span all viewpager pages
 */
open class ViewPagerFragment: Fragment() {

    private val viewPagerIndex: Int
        get() = arguments?.getInt(ActivityMain.PARAM_VIEW_PAGER_INDEX) ?: defaultViewPagerIndex

    open val defaultViewPagerIndex = 0

    fun setBackgroundAccordingToRuleSystem() {
        PrefsHelper(context).ruleSystem.getBackgroundResource(requireContext(), viewPagerIndex).let {
            view?.setBackgroundResource(it)
        }
    }
}