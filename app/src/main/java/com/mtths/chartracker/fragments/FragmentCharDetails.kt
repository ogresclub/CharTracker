package com.mtths.chartracker.fragments

import android.content.*
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.*
import com.mtths.chartracker.dataclasses.Character.Companion.renameCustomCharIcon
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.StatsAdapter
import com.mtths.chartracker.utils.Helper.getIconPath
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.utils.Helper.update
import com.mtths.chartracker.dataclasses.*
import com.mtths.chartracker.dataclasses.CharProgressItem.Companion.createCharProgressItems
import com.mtths.chartracker.dialogs.*
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.sortLogsByTranslatedFilterConstraint
import com.mtths.chartracker.utils.HttpUtils
import com.mtths.chartracker.utils.SnackBarUtils
import com.mtths.chartracker.widgets.SquareImageView
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.collections.ArrayList


class FragmentCharDetails : ShakeFragment() {
    private val character: Character?
        get() = (activity as? ActivityCharDetails?)?.character
    private lateinit var changeStatusButton: Button
    private lateinit var applyButton: Button
    lateinit  var background: LinearLayout
    lateinit var dateOfCreation: TextView
    lateinit var charIcon: ImageView
    lateinit var help: ImageView
    lateinit var simpleCharIcon: SquareImageView
    lateinit var textViewCharName: TextView
    lateinit var sessions_box: LinearLayout
    lateinit var sessions_display: TextView
    lateinit var stat_container: LinearLayout
    lateinit var base_stat_container: LinearLayout
    lateinit var stats_adapter: StatsAdapter
    lateinit var settings: ImageView
    lateinit var buttonWallet: ImageView
    lateinit var walletContainer: RelativeLayout
    lateinit var tvCurrencyTotal: TextView
    lateinit var coordinatorLayout: CoordinatorLayout
    lateinit  var charProgressItemsListRight: LinearLayout
    lateinit  var charProgressItemsListLeft: LinearLayout
    var loadCharDetailsJob: Job? = null
    private var loadSessionNamesRelatedToCharacterJob: Job? = null
    private var mOnCharIconChangedListener: DialogCharIcon.OnCharIconChangedListener? = null
    var sessionNames: ArrayList<String>? = null
    var dbHelper: DatabaseHelper? = null
    lateinit var prefs: PrefsHelper
    override val defaultViewPagerIndex: Int = 1



    val pickVisualMedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
        // Process URIs
        val TAG = "ICON_STUFF"

        /*
        set new icon and store uri of icon
         */
        log(TAG, "onResult PickIcon")

        character?.let { c ->
            log(TAG, "content Uri = $uri")
            context?.let { context ->
                uri?.let { uri ->

                    charIcon.setImageURI(uri)
                    val iconFileName = c.copyIconToInternalAppFolder(context, uri)
                    mOnCharIconChangedListener?.onCharIconChanged(newIconName = iconFileName, isCustomIcon = true)
                }
            }
        }
        Toast.makeText(context, context?.getString(R.string.icon_updated_notification), Toast.LENGTH_SHORT).show()
    }

    ////////////////////////////////// METHODS /////////////////////////////////////////////////////
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dbHelper = getInstance(context)
        prefs = PrefsHelper(context)
        initInterfaceItems(view)
        initAdapters()
        updateDisplays(generateCPI = true, updateUI = true)
        loadCachedCPIIfAvailable()
        implementApplyButton()
        implementChangeStatusButton()
        implementChooseCharIcon()
        implementChangeCharName()
        setOnClickListenersSessionAndTags()
        implementWallet()
    }

    override fun onResume() {
        super.onResume()
        setViewPagerPadding()
    }

    private fun initAdapters() {
        stats_adapter =
            StatsAdapter(
                mActivity = activity,
                statContainer = stat_container,
                baseStatContainer = base_stat_container,
                coordinatorLayout = coordinatorLayout
            )
    }

    fun updateDisplays(updateUI: Boolean, generateCPI: Boolean) {
        lifecycleScope.launch { _updateDisplays(updateUI, generateCPI) }
    }


    suspend fun _updateDisplays(updateUI: Boolean, generateCPI: Boolean) {
        withContext(Dispatchers.Main) {
            updateBackgroundColor()
            updateTotalWealthDisplay()
            setBackgroundAccordingToRuleSystem()
            loadBasicCharFacts()
            loadSessionNamesRelatedToCharacter()
            loadCharDetails(updateUI = updateUI, generateCPI = generateCPI)
            fillSessions()
        }

    }

    fun setViewPagerPadding() {
        background.setPadding(
            0,(activity as ActivityCharDetails).getViewPagerPadding(),0,0
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        /*
        This makes sure that the container activity has implemented
        the callback interface. If not, it throws an exception
        */
        mOnCharIconChangedListener = try {
            context as DialogCharIcon.OnCharIconChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnCharIconChangedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnCharIconChangedListener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_character_details, container, false)
    }

    fun inflateSessionSpecificStats() {
        stats_adapter.inflate(character)
    }

    fun updateCharIcon() {
        character?.let { c ->
            context?.let {context ->
                charIcon.setImageDrawable(c.decodeCharIcon(
                    context = context, thumbNailQuality = 512))
                if (c.iconFileName.isBlank()) {
                    simpleCharIcon.setImageResource(R.color.transparent)
                } else {
                    simpleCharIcon.setImageResource(c.getSimpleIconResource())

                }
            }
        }
    }

    fun updateBackgroundColor() {
        val charDetails = (activity as? ActivityCharDetails?)
        charDetails?.setBackgroundColorAccordingToCharStatus(background)
    }

    fun loadBasicCharFacts() {
        // ------ get data and set to displays
        textViewCharName.text = character?.name

        updateCharIcon()
        //only display date, not time!
        var dates: String? = character?.getDateOfCreationAsString()
        if (character?.isRetired == true && character?.getDateOfRetirementAsString()?.length != 0) {
            dates += " - " + character?.getDateOfRetirementAsString()
        }
        dateOfCreation.text = dates
    }

    private fun loadCachedCPIIfAvailable() {
        //load cached char ProgressItems if available
        character?.let { c ->
            Global.cachedCharProgressItems[c.id]?.let {
                c.charProgressItemsMap = it
                log("LoadCharDetails", "cached CPIM loaded for ${c.name} ----------------- ")
            }
            inflateCharDetails()
            // without handler some lateinit variable is not yet initialized and the app crashes
            Handler(Looper.getMainLooper()).postDelayed(
                {(activity as? ActivityCharDetails?)?.fragmentSessionData?.fill()},
                100
            )

        }
    }

    override fun onShake(count: Int) {
        toggleCheckMistakes()
    }

    fun toggleCheckMistakes() {
        val check_mistakes = !prefs.checkExpCalcMistakes
        prefs.checkExpCalcMistakes = check_mistakes

        val msg = if (check_mistakes) {
            "showing mistakes"
        } else {
            "not showing mistakes"
        }
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        inflateCharDetails()
        val showFilteredLogs = childFragmentManager.findFragmentByTag(ActivityCharDetails.FRAGMENT_TAG_SHOW_FILTERED_LOGS) as? DialogShowFilteredLogs
        showFilteredLogs?.setMarkMistakes(prefs.checkExpCalcMistakes)
        showFilteredLogs?.fillLogAdapter()

    }

    ///////////////////////////////////// PRIVATE METHODS /////////////////////////////////////////
    private fun initInterfaceItems(v: View) {
        background = v.findViewById(R.id.background)
        changeStatusButton = v.findViewById(R.id.status_button)
        applyButton = v.findViewById(R.id.apply_button)
        dateOfCreation = v.findViewById(R.id.date_of_creation)
        charIcon = v.findViewById(R.id.char_icon)
        help = v.findViewById(R.id.help)
        help.setOnClickListener {
            activity?.let {
                HttpUtils.openWebPage(
                    url = resources.getString(R.string.user_manual_link),
                    activity = it
                )
            }
        }
        simpleCharIcon = v.findViewById(R.id.simple_char_icon)
        textViewCharName = v.findViewById(R.id.text_view_char_name)
        sessions_display = v.findViewById(R.id.sessions)
        sessions_box = v.findViewById(R.id.session_box)
        charProgressItemsListRight = v.findViewById(R.id.charProgressItemsListRight)
        charProgressItemsListLeft = charProgressItemsListRight
        if (Helper.useLandscapeMode(resources))  {
            charProgressItemsListLeft = v.findViewById(R.id.charProgressItemsListLeft)
        }
        stat_container = v.findViewById(R.id.stat_container)
        base_stat_container = v.findViewById(R.id.base_stat_container)
        settings = v.findViewById(R.id.settings)
        buttonWallet = v.findViewById(R.id.wallet_button)
        walletContainer = v.findViewById(R.id.wallet_container)
        tvCurrencyTotal = v.findViewById(R.id.currency_total)
        coordinatorLayout = v.findViewById(R.id.coordiantor_layout)
        settings.setOnClickListener {
            childFragmentManager.let { fragmentManager ->
                DialogSettingsChar.newInstance(character!!.id).show(
                    fragmentManager, "char_settings")
            }
        }
    }

//    private fun setClassOnClickListeners() {
//        class_display.setOnLongClickListener {
//            val dialog = DialogCustom<DndClass>()
//            dialog.setInput(false, "", true, "enter class name")
//            dialog.setMessage("    Change Class   ")
//            var classes_to_auto_complete = ArrayList<DndClass>()
//            //show only classes matching to a given archetype if there is a valid archetype.
//            if (character?.archetype != null && character?.archetype?.classes != null && character?.archetype?.classes?.size != 0) {
//                classes_to_auto_complete = ArrayList()
//                for (cl_name in character!!.archetype.classes) {
//                    classes_to_auto_complete.add(DndClass(cl_name))
//                }
//            } else {
//                classes_to_auto_complete = Global.classesFromOgresClub.clone() as ArrayList<DndClass>
//            }
//            dialog.setEditText2Adapter(
//                ArrayAdapter<DndClass>(
//                    requireContext(),
//                    R.layout.auto_complete_entry,
//                    classes_to_auto_complete
//                ),
//                true)
//            dialog.setEditText2_OnItemClickListener(OnItemClickListener { parent, view, position, id ->
//                val new_class_name = parent.getItemAtPosition(position).toString().trim { it <= ' ' }
//                if (new_class_name.isEmpty()) {
//                    Toast.makeText(context, "This is no name for a class!", Toast.LENGTH_SHORT).show()
//                } else {
//                    dialog.dismiss()
//                    ChangeClassTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new_class_name)
//                }
//            })
//            dialog.setPositiveButton("Ok", View.OnClickListener {
//                val new_class_name = dialog.text2.text.toString().trim { it <= ' ' }
//                if (new_class_name.isEmpty()) {
//                    Toast.makeText(context, "This is no name for a class!", Toast.LENGTH_SHORT).show()
//                } else {
//                    dialog.dismiss()
//                    ChangeClassTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new_class_name)
//                }
//            })
//            dialog.setNegativeButton("Cancel", View.OnClickListener { dialog.dismiss() })
//            fragmentManager?.let { fragmentManager -> dialog.show(fragmentManager, "DialogChangeClass") }
//            true
//        }
//        class_display.setOnClickListener {
//            if (character?.dndClass != null && character?.dndClass?.link != null && character?.dndClass?.link?.isNotEmpty() == true) {
//                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(character!!.dndClass.link))
//                activity?.let {
//                    startActivity(browserIntent)
//                }
//            }
//        }
//    }

    private fun openLink(link: String, what: String, hint: String = "") {
        if (link.isBlank()) {
            var msg = "no link found for $what."
            if (hint.isNotBlank()) msg += " $hint."
            toast(msg =  msg)
        } else {
            log(
                "URL",
                "start opening link for $what" +
                        "in browser with url = '$link'"
            )
            activity?.let {
                HttpUtils.openWebPage(link, it)
            }
        }

    }

    private fun toast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    private fun implementChangeStatusButton() {
        changeStatusButton.setOnClickListener {
            DialogChangeCharacterStatus.newInstance().show(childFragmentManager, "changeStatus")

        }
    }

    //todo this is only called when toggling expand for the session box and can be simplfied. Not the whole UI needs to be updated, only the session box
    private fun toggleExpand(filterConstraintId: Int) {
        context?.let {context ->

            character?.let {
                character?.toggleExtend(
                    charProgItemfilterConstraintId = filterConstraintId,
                    context = context
                )
                DatabaseHelper.getInstance(context)?.updateCharacter(it, keep_last_update_date = false)
                updateDisplays(updateUI = true, generateCPI = false)

//                Toast.makeText(
//                    context,
//                    "expand ${filterConstraint} = ${character?.getExtend(filterConstraint, orientation)}}\n" +
//                            "${character?.extendCharProgItemsMap}",
//                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setOnClickListenersSessionAndTags() {
        sessions_box.setOnClickListener {
            toggleExpand(R.string.session_display_key)
        }
    }

    private fun updateTotalWealthDisplay() {
        character?.let { c ->
            tvCurrencyTotal.text = Character.getTotalWealthString(
                c.totalCurrency(extendedWallet = c.displayTreasureInWallet)
            )
        }
    }

    private fun implementWallet() {

        character?.let { c ->
            buttonWallet.apply {

                val rS = c.ruleSystem ?: prefs.ruleSystem

                if (rS.showWalletInCharDeatils) {

                    //use local currency to calculate wealth
                    c.displaySchwarzgeldWealth = true

                    /*
                    add all default items to schwarzloot
                    we use schwarzloot, because this is simply used for loot that is stored
                    locally in the character and not in lootmanager
                     */
                    c.schwarzloot.addAll(
                        rS.defaultItems.filter {
                            itm -> c.schwarzloot.find { it.name == itm.name } == null
                        }
                    )
                    Helper.updateCharacter(
                        character = c, keep_last_update_date = false, context = context)

                    walletContainer.visibility = View.VISIBLE
                    setOnClickListener {

                        childFragmentManager.let { fm ->
                            DialogShowItems.newInstance(
                                char_id = c.id,
                                showCharItems = true,
                                categories = ArrayList(ItemCategory.walletCategories),
                                localOnly = true
                            ).apply {
                                onApply = {
                                    (activity as? ActivityCharDetails?)?.updateDisplays(generateCPI = false)
                                }
                            }
                                .show(fm, "showWallet")

                        }
                    }
                } else {
                    walletContainer.visibility = View.GONE
                }
            }
        }
    }

    private fun updateIconPath(character: Character, newCharName: String) {
            context?.let{ context ->
                val oldType = character.getIconType()
                if (oldType.isNotBlank()) {
                    // this means, that custom char icon is used
                    val newIconPath = getIconPath(newCharName, context, oldType)
                    renameCustomCharIcon(
                        oldIconPath = character.getCompleteIconPath(context),
                        newIconPath = newIconPath
                    )
                    character.iconFileName = newIconPath.substringAfterLast("/")
                }
                dbHelper?.updateCharacter(character = character, keep_last_update_date = false)
                update(Global.chars, character)
            }
    }

    private fun implementChangeCharName() {
        textViewCharName.setOnLongClickListener {


            DialogCustom.newInstance().apply {
                setTitle(R.string.label_change_character_name)
                setInput(
                    editText2Visible = true,
                    hint2StringResId = R.string.msg_enter_new_character_name
                )
                setInputText(
                    text2Text = character?.name
                )
                setPositiveButton(R.string.ok) {
                    //get charName for EditText
                    val newCharName = text2.text.toString().trim()
                    if (newCharName.isEmpty()) {
                        Toast.makeText(context, R.string.msg_please_enter_a_new_character_name, Toast.LENGTH_SHORT).show()
                    } else {
                        character?.let { character ->
                            //remember old Values to compare later to give the correct toast
                            val oldCharName = character.name
                            val charNameChanged = oldCharName != newCharName

                            //update character in database and chars
                            if (charNameChanged) {
                                character.name = newCharName
                                textViewCharName.text = character.name
                                updateIconPath(character = character, newCharName = newCharName)
                                //make Toast for User
                                context?.let { context ->
                                    Helper.toastChangesInParameters(
                                        booleanArrayOf(charNameChanged),
                                        arrayOf(context.getString(R.string.label_character_name)),
                                        arrayOf(newCharName),
                                        context
                                    )
                                }

                            }
                        }
                        dismiss()
                    }
                }
                show(
                    this@FragmentCharDetails.childFragmentManager,
                    "changeName"
                )
            }
            return@setOnLongClickListener true
        }
    }

    private fun implementApplyButton() {
        applyButton.setOnClickListener {
            activity?.finish()
        }
    }

    private fun implementChooseCharIcon() {

        charIcon.setOnClickListener {
            log("ICON_STUFF", "char icon clicked")
            DialogCharIcon.newInstance().show(
                childFragmentManager, "dialogCharIcon"
            )
        }

        charIcon.setOnLongClickListener {
            pickVisualMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
            return@setOnLongClickListener true
        }
    }

    private fun fillSessions() {
        context?.let { context ->
            val orientation = context.resources.configuration.orientation
            val filterConstraint = R.string.session_display_key
            val expand = character?.getExtend(
                charProgItemfilterConstraintId = filterConstraint,
                context = context
            )
            if (expand == true) {
                sessions_display.visibility = View.VISIBLE
                sessionNames?.also {
                    sessions_display.text = it.joinToString()
                } ?: run {
                    sessions_display.setText(R.string.none)
                }
            } else {
                sessions_display.visibility = View.GONE
            }
        }
    }

    fun loadSessionNamesRelatedToCharacter() {
        loadSessionNamesRelatedToCharacterJob?.cancel()
        loadSessionNamesRelatedToCharacterJob = (activity as? ActivityCharDetails?)?.lifecycleScope?.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                sessionNames = character?.let { dbHelper?.getSessionNamesRelatedToCharacter(it.id) }
                sessionNames?.sort()
            }
            fillSessions()
        }
    }

    suspend fun loadCharDetails(updateUI: Boolean, generateCPI: Boolean) {
        loadCharDetailsJob?.cancel()
        withContext(Dispatchers.Main) {
            loadCharDetailsJob = launch {
                if (generateCPI && isActive) {
                    generateCPI()
                }
                if (updateUI && isActive) {
                    inflateCharDetails()
                    inflateSessionSpecificStats()
                    (activity as? ActivityCharDetails?)?.fragmentStatusMonitor?.setData()
                }
            }
        }
    }

    private suspend fun addAllLogsToCPI(logs: List<LogEntry>, cPMetaData: CharProgMetaData)  {
        withContext(Dispatchers.Default) {
            /*
            use reverse ordering to start with oldest entry in order to get increasing order
            but not changing the order of the charLogs
             */
            var e: LogEntry
            for (i in logs.indices) {
                if (isActive) {
                    e = logs[logs.size - 1 - i]
                    e.hasError = false //general value

                    cPMetaData.mainItem.adapter?.add(e)
                    cPMetaData.relevantItems.forEach {
                        it.adapter?.getExtraData(
                            e.text
                        )
                    }

                }
            }
        }
    }

    suspend fun checkErrorsInFilterConstraints(
        legitFcs: List<String>,
        foundFcs: List<String>
    ) {
        if (prefs.checkFilterConstraintErrors) {

            // check if we have some wrong filter constraints and inform user:

            val errors = foundFcs.map { it.lowercase() }
                .minus(legitFcs.map { it.lowercase() }.toSet())

            if (errors.isNotEmpty()) {
                withContext(Dispatchers.Main) {
                    SnackBarUtils.showSnackBar(
                        view = coordinatorLayout,
                        msg = requireContext().getString(
                            R.string.msg_invalid_filter_constraints,
                            errors.joinToString(separator = "\n")
                        ),
                        duration = Snackbar.LENGTH_LONG,
                        context = context
                    )
                }
            }
        }
    }

    suspend fun generateCPI() {
        withContext(Dispatchers.Default) {
            character?.let { character ->
                try {
                    val charDetails = (activity as ActivityCharDetails)

                    charDetails.charProgressItemsMap =
                        createCharProgressItems(
                            activity = this@FragmentCharDetails.activity as? ProgressBarFrameDropBoxActivity?,
                            character = character
                        )

                    charDetails.charLogs?.let { charLogs ->
                        Global.ATTRIBUTE_POINT_EXP_COST =
                            charDetails.character?.ATTRIBUTE_POINT_EXP_COST
                                ?: Global.ATTRIBUTE_POINT_EXP_COST

                        /*
                        sort char logs and (if more than 1 char in session) session logs
                        by filter constraints
                         */
                        val defCfcMap = async { sortLogsByTranslatedFilterConstraint(
                            logs = charLogs,
                            cpiMap = charDetails.charProgressItemsMap,
                            context = requireContext()
                        ) }
                        val defSfcMap =
                                async {
                                    if (Global.charsRelatedToSession.size > 1) {
                                        // this means the charLogs and the session logs might actually be different

                                        sortLogsByTranslatedFilterConstraint(
                                        logs = Global.logsFilteredBySession,
                                        cpiMap = charDetails.charProgressItemsMap,
                                        context = requireContext()
                                    )
                                } else {
                                    HashMap()
                                    }
                                }
                        val (cfcMap, sfcMap) = awaitAll(defCfcMap, defSfcMap)

                        //overwrite session stats logs from logs filtered by session
                        if (sfcMap.isNotEmpty()) {
                            context?.let { context ->
                                Global.ITEMS_SESSION_DATA.forEach {
                                    val fc = context.getString(it.filterConstraintId)
//                                    Log.d("SESSION_STATSS", "$fc: size = ${sfcMap[fc]?.size}")
                                    sfcMap[fc]?.let {
                                        cfcMap[fc] = it
                                    }
                                }
                            }

                        }

                        checkErrorsInFilterConstraints(
                            legitFcs = charDetails.charProgressItemsMap.keys.mapNotNull {
                                context?.getString(it)
                            },
                            foundFcs = cfcMap.map { it.key }
                        )


                        if (isActive) {
                            // add logs to adapter for each cpi
                            val jobs = mutableListOf<Deferred<CharProgMetaData>>()
                            charDetails.charProgressItemsMap.values.forEach { cPMetaData ->
                                jobs.add(
                                    async() {
                                        val fc = cPMetaData.mainItem.filterConstraint
                                        val logs = cfcMap[fc] ?: listOf()
                                        addAllLogsToCPI(
                                            logs = logs, cPMetaData = cPMetaData
                                        )
                                        cPMetaData
                                    }
                                )
                            }

                            jobs.awaitAll()
                        }

                        if (isActive) {
                            character.charProgressItemsMap = charDetails.charProgressItemsMap
                            Global.cachedCharProgressItems[character.id] =
                                charDetails.charProgressItemsMap
                        }


                    }


                } catch (e: java.lang.ClassCastException) {
                    log("LoadCharDetails", "cant cast activity to ActivityCharDetails")
                }
                character.apply {
                    Log.d("TEMP_BUFFS","temp buffs generated by cpi: ${tempBuffs.joinToString { "%s; %s".format(it.name, it.type) }}")
                    updateCPIBuffs()
                    loadBuffBoni()
                }
                dbHelper?.updateCharacter(character, keep_last_update_date = true)
            }
        }
    }

    fun inflateCPI(item: CharProgressItem, character: Character) {
        val landscapeMode = Helper.useLandscapeMode(resources)

        val expand = character.getExtend(
            charProgItemfilterConstraintId = item.filterConstraintId,
            context = requireContext()
        )

        item.check_mistakes = prefs.checkExpCalcMistakes
        item.adapter?.inflate(expand = expand)?.let { view ->

            if (landscapeMode && item.positionCharDetailsLandscape == CharProgressItem.POSITION_LEFT) {
                charProgressItemsListLeft.addView(view)
            } else {
                charProgressItemsListRight.addView(view)
            }
        }
    }

    fun inflateCharDetails() { //remove old views
        log("LoadCharDetails", "start inflating char Details")
        charProgressItemsListRight.removeAllViews()
        charProgressItemsListLeft.removeAllViews()


        character?.let { character ->

            for (item in character.charProgressItemsMap.values.map { it.mainItem }) {
                if (item.isSessionData) {
                    inflateCPI(item = item, character = character)
                }
            }
        }
    }

    companion object {
        const val REF = "fragment_char_details"

        fun newInstance(viewPagerIndex: Int): FragmentCharDetails {
            val args = Bundle()
            args.putInt(ActivityMain.PARAM_VIEW_PAGER_INDEX, viewPagerIndex)
            val fragment = FragmentCharDetails()
            fragment.arguments = args
            return fragment
        }
    }
}