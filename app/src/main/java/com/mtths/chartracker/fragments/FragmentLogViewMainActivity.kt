package com.mtths.chartracker.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.widget.AppCompatCheckedTextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.fragments.FragmentLogView.Companion.activateCheckBox
import com.mtths.chartracker.fragments.FragmentLogView.Companion.deactivateCheckBox
import com.mtths.chartracker.fragments.FragmentLogView.OnSessionsChangedListener
import com.mtths.chartracker.fragments.FragmentMainScreen.OnCharActivityChangedListener
import com.mtths.chartracker.preferences.PrefsHelper

class FragmentLogViewMainActivity : ViewPagerFragment() {
    private lateinit var showAllCharsLogsCheckBox: AppCompatCheckedTextView
    private lateinit var showNotesCheckBox: AppCompatCheckedTextView
    private lateinit var showAllSessionsCheckBox: AppCompatCheckedTextView
    private lateinit var showCharProgression: AppCompatCheckedTextView
    lateinit var fragmentLogView: FragmentLogView
    private lateinit var root: RelativeLayout
    lateinit var prefs: PrefsHelper
    override val defaultViewPagerIndex = 3

    /**
     * tag names of all tag from the logs which will be finally displays after all filtering
     */
    var mOnSessionsChangedListener: OnSessionsChangedListener? = null
    var mOnCharActivityChangedListener: OnCharActivityChangedListener? = null


    //////////////////////////////////////// METHODS ///////////////////////////////////////////////
// -------------------------------------------------------------------------------------------//
/////////////////////////////// ON CREATE VIEW / NEW INSTANCE /////////////////////////////////////////
// -------------------------------------------------------------------------------------------//
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val layout = inflater.inflate(R.layout.fragment_log_view_main_activity, container, false)
        return layout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefs = PrefsHelper(context)
        initInterfaceItems(view)
        updateButtonDisplays()
        initFragmentLogView()
        implementShowCharProgression()
        implementShowAllCharsLogs()
        implementShowNotes()
        implementShowAllSessions()

    }

    fun setViewPagerPadding() {
        root.setPadding(
            2,(activity as ActivityMain).getViewPagerPadding(),2,0
        )
    }

    override fun onResume() {
        super.onResume()
        setBackgroundAccordingToRuleSystem()
        setViewPagerPadding()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        /*
        This makes sure that the container activity has implemented
        the callback interface. If not, it throws an exception
        */
        mOnSessionsChangedListener = try {
            context as OnSessionsChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnActiveSessionChangedListener")
        }
        mOnCharActivityChangedListener = try {
            context as OnCharActivityChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnCharActivityChangedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnSessionsChangedListener = null
        mOnCharActivityChangedListener = null
    }

    /////////////////////////////////////// PRIVATE METHODS ////////////////////////////////////////
    private fun initInterfaceItems(v: View) {
        root = v.findViewById(R.id.root)

        showAllCharsLogsCheckBox = v.findViewById(R.id.showAllCharsCheckBox)
        showNotesCheckBox = v.findViewById(R.id.showNotesCheckBox)
        showAllSessionsCheckBox = v.findViewById(R.id.showAllSessionsCheckBox)
        showCharProgression = v.findViewById(R.id.showCharProgression)
    }

    private fun initFragmentLogView() {
        childFragmentManager.beginTransaction().apply {
            fragmentLogView = FragmentLogView.newInstance()
            replace(R.id.log_view_frame, fragmentLogView)
            commit()
        }
        Handler(Looper.getMainLooper()).postDelayed(
            { fragmentLogView.floatingButtonOffset = showAllCharsLogsCheckBox.height }, 100)
    }

    private fun updateButtonDisplays() {
        if (prefs.filterSessions) {
            deactivateCheckBox(showAllSessionsCheckBox, requireContext())
        } else {
            activateCheckBox(showAllSessionsCheckBox, requireContext())
        }
        when (prefs.filterChars) {
            PrefsHelper.FILTER_CHARS_NO_CHARS -> {
                activateCheckBox(showNotesCheckBox, requireContext())
                deactivateCheckBox(showAllCharsLogsCheckBox, requireContext())
            }
            PrefsHelper.FILTER_CHARS_SHOW_ALL_CHARS -> {
                deactivateCheckBox(showNotesCheckBox, requireContext())
                activateCheckBox(showAllCharsLogsCheckBox, requireContext())
            }
            else -> {
                deactivateCheckBox(showNotesCheckBox, requireContext())
                deactivateCheckBox(showAllCharsLogsCheckBox, requireContext())
            }
        }
        if (prefs.showCharProgression) {
            activateCheckBox(showCharProgression, requireContext())
        } else {
            deactivateCheckBox(showCharProgression, requireContext())
        }
    }

    private fun updateAllButtonDisplays() {
        updateButtonDisplays()
        fragmentLogView.updateButtonDisplays()
    }

    private fun implementShowCharProgression() {
        showCharProgression.setOnClickListener {
            prefs.showCharProgression = !prefs.showCharProgression

            fragmentLogView.setShowCharProgressionSettingsInEffect()
            updateAllButtonDisplays()

            log("ToggleLogView", "showCharProgression set to " + prefs.showCharProgression)
            fragmentLogView.completeFillLogAdapter()
        }
    }

    private fun implementShowAllSessions() {
        showAllSessionsCheckBox.setOnClickListener {
            prefs.filterSessions = !prefs.filterSessions
            updateAllButtonDisplays()
            log("filter session option changed to ${prefs.filterSessions}")
            mOnSessionsChangedListener?.onShowAllSessionsToggled()
        }
    }

    private fun implementShowAllCharsLogs() {
        showAllCharsLogsCheckBox.setOnClickListener { v ->



            val checkBox = v as AppCompatCheckedTextView
            if (checkBox.isChecked) {
                prefs.filterChars = PrefsHelper.FILTER_CHARS_YES
            } else {
                prefs.filterChars = PrefsHelper.FILTER_CHARS_SHOW_ALL_CHARS
            }
            updateAllButtonDisplays()
            log("filter chars option changed to ${prefs.filterChars}")
            mOnCharActivityChangedListener?.onCharFilterOptionsToggled()
        }
    }

    private fun implementShowNotes() {
        showNotesCheckBox.setOnClickListener { v ->
            val checkBox = v as AppCompatCheckedTextView
            if (checkBox.isChecked) {
                prefs.filterChars = PrefsHelper.FILTER_CHARS_YES
            } else {
                prefs.filterChars = PrefsHelper.FILTER_CHARS_NO_CHARS
            }
            updateAllButtonDisplays()
            log("Filter chars option changed to ${prefs.filterChars}")
            mOnCharActivityChangedListener?.onCharFilterOptionsToggled()
        }
    }

    private fun toast(msg: String): Toast {
        return Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG)
    }

    fun refreshDisplays() { //        initialFillLogAdapter();
        fragmentLogView.refreshDisplays()
    }

    companion object {

        const val REF = "fragment_log_view_main_activity"
        fun newInstance(viewPagerIndex: Int): FragmentLogViewMainActivity {
            val args = Bundle()
            args.putInt(ActivityMain.PARAM_VIEW_PAGER_INDEX, viewPagerIndex)
            val fragment = FragmentLogViewMainActivity()
            fragment.arguments = args
            return fragment
        }
    }
}