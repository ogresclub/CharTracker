package com.mtths.chartracker.fragments

import android.content.Context
import android.content.res.Configuration
import android.widget.GridView
import android.widget.LinearLayout
import android.widget.TextView
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView.OnItemLongClickListener
import android.widget.FrameLayout
import android.widget.ScrollView
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.adapters.EdgeAdapter
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.RuleSystem
import java.lang.Exception
import java.util.*
import kotlin.ClassCastException
import kotlin.collections.LinkedHashMap

class FragmentStatusMonitor : ViewPagerFragment() {

    val damageMonitors: LinkedHashMap<String, FragmentDamageMonitor> = LinkedHashMap()

    private lateinit var edge_monitor: GridView
    private lateinit var edge_adapter: EdgeAdapter
    private lateinit var wounds_container: LinearLayout
    private lateinit var wounds_display: TextView
    private lateinit var wounds_label: TextView
    private lateinit var container: LinearLayout
    private lateinit var root: View
    lateinit  var background: LinearLayout


    private lateinit var prefs: PrefsHelper
    private var ruleSystem = RuleSystem.rule_system_sr6
    override val defaultViewPagerIndex = 0


    val character: Character?
        get() = (activity as? ActivityCharDetails?)?.character

    private var mOnStatusMonitorChangedListener: OnStatusMonitorChangedListener? = null

    internal interface OnStatusMonitorChangedListener {
        fun onStatusMonitorChanged()
    }

    /////////////////////////////////// METHODS ////////////////////////////////////////////////////
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mOnStatusMonitorChangedListener = try {
            context as OnStatusMonitorChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(
                context.toString()
                        + " must implement OnStatusMonitorChangedListener"
            )
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnStatusMonitorChangedListener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefs = PrefsHelper(context)

        character?.let { c ->
            ruleSystem = c.ruleSystem ?: ruleSystem
            initInterfaceItems(view)
            setupDamageFragments()
            initAdapters()
            updateBackgroundColor()
            implementEdgeOnClick()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout = inflater.inflate(R.layout.fragment_status_monitor, container, false)
        return layout
    }

    override fun onResume() {
        super.onResume()
        setViewPagerPadding()
    }

    private fun setupDamageFragments() {

        val t = childFragmentManager.beginTransaction()

        /*
        inflate FrameLayouts, where we can later put in our damageMonitors
         */
        var id = 1

        ruleSystem.damageTypes.forEach {

             if (Helper.getOrientation(context) ==
                Configuration.ORIENTATION_LANDSCAPE) {

                 /*
                 for landscape orientation, we need to put the Framelayouts in Scrollviews,
                 because the damage monitors are now stacked horizontally,
                 but their content is still vertically
                 and we want to be able to scroll each monitor individually
                  */
                 container.addView(
                     ScrollView(context).apply {

                         layoutParams = LinearLayout.LayoutParams(
                                 0,
                                 LinearLayout.LayoutParams.MATCH_PARENT,
                                 1f
                         )

                         addView(
                             FrameLayout(requireContext()).apply {

                                 layoutParams = FrameLayout.LayoutParams(
                                     FrameLayout.LayoutParams.MATCH_PARENT,
                                     FrameLayout.LayoutParams.MATCH_PARENT,
                                     Gravity.CENTER
                                 )
                                 this.id = id
                             }
                         )
                     }

                 )
             } else {

                 //orientation Portrait, we only need the FrameLayout to replace it

                 container.addView(

                     FrameLayout(requireContext()).apply {
                         layoutParams = FrameLayout.LayoutParams(
                             FrameLayout.LayoutParams.MATCH_PARENT,
                             FrameLayout.LayoutParams.WRAP_CONTENT,
                         )
                         this.id = id
                     }
                 )
             }
            val f = FragmentDamageMonitor.newInstance(
                data = it.value
            )
            damageMonitors[it.key] = f
            t.replace(id, f)

            id++
        }

        t.commit()
    }



    fun updateBackgroundColor() {
        val charDetails = (activity as? ActivityCharDetails?)
        charDetails?.setBackgroundColorAccordingToCharStatus(background)
    }

    fun log(msg: String) = Log.d("STATUS_MONITOR", "#### $msg")


    fun setEdge(totalEdge: Int, currentEdge: Int, max_edge: Int? = null, edgeImageResource: Int? = null) {
        edge_adapter.totalEdge = totalEdge
        edge_adapter.currentEdge = currentEdge
        max_edge?.let { edge_adapter.MAX_EDGE = it }
        edgeImageResource?.let { edge_adapter.edgeImageResource = it }
        edge_adapter.notifyDataSetChanged()
        log("set Edge: current edge: $currentEdge, total edge: $totalEdge: ${Log.getStackTraceString(Exception())}")
    }

    fun setData() {

        damageMonitors.values.forEach {
            it.setData()
        }
        updateDisplays()
    }

    fun updateDisplays() {
        damageMonitors.values.forEach {
            it.updateDisplays()
        }
        setBackgroundAccordingToRuleSystem()

        character?.damageMalus?.let { dmgMalus ->
            Log.e("HPPPPPPPP", "damageMalus = $dmgMalus" )
            if (ruleSystem.hasWounds) {
                wounds_display.text = String.format(Locale.GERMAN, "-%d", dmgMalus)
                if (dmgMalus <= 0) {
                    wounds_container.visibility = View.GONE
                } else {
                    wounds_container.visibility = View.VISIBLE
                }
            }
        }

        //dont really know why I put this here and not in setData
        character?.let { c ->
            if (Helper.rulesSR(c, requireContext())) {
                setEdge(totalEdge = c.edge, currentEdge = c.currentEdge)
            } else {
                val edge = c.splitterpunkte
                setEdge(
                    totalEdge = edge,
                    currentEdge = c.currentEdge,
                    max_edge = edge,
                    edgeImageResource = R.drawable.ic_shard)
            }
        }


    }

    //////////////////////////////////// PRIVATE METHODS /////////////////////////////////////////
    private fun initInterfaceItems(v: View) {
        root = v.findViewById(R.id.root)
        container = v.findViewById(R.id.container)
        background = v.findViewById(R.id.background)
        edge_monitor = v.findViewById<GridView?>(R.id.edge_monitor).apply {
            if (!ruleSystem.hasEdge) {
                visibility = View.GONE
            }
        }
        wounds_container = v.findViewById<LinearLayout?>(R.id.overflow_container).apply {
            if (!ruleSystem.hasWounds) {
                visibility = View.GONE
            }
        }
        wounds_display = v.findViewById(R.id.overflow_display)
        wounds_label = v.findViewById<TextView?>(R.id.wounds_label).apply {
            text = context.getString(ruleSystem.woundsNameId)
        }

    }

    private fun initAdapters() {
        edge_adapter = EdgeAdapter(requireContext())
        edge_monitor.adapter = edge_adapter
    }

    fun implementEdgeOnClick() {

        fun onEdgeChange() {

            character?.currentEdge = edge_adapter.currentEdge
            mOnStatusMonitorChangedListener?.onStatusMonitorChanged()
        }
        edge_monitor.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            Log.d("EDGE", "clicked position $position, edge before changing due to click action =${edge_adapter.currentEdge}")
            if (position + 1 > edge_adapter.currentEdge) {
                edge_adapter.addEdge()
            } else {
                edge_adapter.spendEdge()
            }
            onEdgeChange()
        }
        edge_monitor.onItemLongClickListener = OnItemLongClickListener { parent, view, position, id ->
            edge_adapter.currentEdge = position + 1
            edge_adapter.notifyDataSetChanged()
            onEdgeChange()
            return@OnItemLongClickListener true
        }
    }

    fun setViewPagerPadding() {
        background.setPadding(
            0,(activity as ActivityCharDetails).getViewPagerPadding(),0,0
        )
    }

    companion object {
        const val REF = "fragment_status_monitor"

        private const val PARAM_CHAR_ID = "char_id"
        fun newInstance(viewPagerIndex: Int): FragmentStatusMonitor {
            val fragment = FragmentStatusMonitor()
            val args = Bundle()
            args.putInt(ActivityMain.PARAM_VIEW_PAGER_INDEX, viewPagerIndex)
            fragment.arguments = args
            return fragment
        }

        const val DEFAULT_ITEM_SIZE = 70f
    }
}