package com.mtths.chartracker.utils

import android.os.AsyncTask
import android.text.Editable
import android.util.Log
import android.util.Pair
import android.widget.AdapterView
import android.widget.EditText
import android.widget.MultiAutoCompleteTextView
import com.mtths.chartracker.Global
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.adapters.AutoCompleteTextToAddAdapter
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterBuffs
import com.mtths.chartracker.dataclasses.BuffEffect
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.widgets.MyMultiAutoCompleteTextView
import com.mtths.chartracker.widgets.MyMultiAutoCompleteTextViewNonfiltering
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AutoCompleteUtils {

    companion object {

        val DEBUG_TAG_AUTO_COMPLETITION = "AUTO_COMPLETE"


        data class AutocompleteData(
            val firstWordBeginningWithAtBeforeSelection: String,
            val text: String,
            val textUntilSelection: String,
            val indexOfNextWhiteSpaceAfterAtBeforeSelection: Int,
            val lastIndexOfAtBeforeSelection: Int,
            val startSelection: Int,
            val showBuffs: Boolean
        )

        fun getFirstWordBeforeSelectionStartingWithAt(et: EditText): AutocompleteData {
            val text = et.text.toString()
            var firstWordBeginningWithAtBeforeSelection = ""
            val startSelection = et.selectionStart
            val textUntilSelection = text.substring(0, startSelection)
            val lastIndexOfAtBeforeSelection = textUntilSelection.lastIndexOf("@")
            var indexOfNextWhiteSpaceAfterAtBeforeSelection = -1
            if (lastIndexOfAtBeforeSelection >= 0) {
                indexOfNextWhiteSpaceAfterAtBeforeSelection = textUntilSelection.indexOf(" ", lastIndexOfAtBeforeSelection)
                if (indexOfNextWhiteSpaceAfterAtBeforeSelection < 0) {
                    indexOfNextWhiteSpaceAfterAtBeforeSelection = startSelection
                }
                firstWordBeginningWithAtBeforeSelection = textUntilSelection.substring(lastIndexOfAtBeforeSelection, indexOfNextWhiteSpaceAfterAtBeforeSelection).trim()
            }

            /*
            //Debugging
            logAutoComplete("selection start = " + startSelection);
            logAutoComplete("text until selection = '" + textUntilSelection + "'");
            logAutoComplete("last index of @ before selection = " + lastIndexOfAtBeforeSelection);
            logAutoComplete("index of next white space after last @ before selection = " + indexOfNextWhiteSpaceAfterAtBeforeSelection);
            logAutoComplete("first word beginning with @ before selection = '" + firstWordBeginningWithAtBeforeSelection + "'");
            */

            /*
            decide weather to show buffs
             */
            var showBuffs = false
            val missingDelimiterValue = "-"
            val textUntilSelectionNoWhiteSpace = textUntilSelection.replace(Regex("\\s"), "")

            //detect buffs = []
            val textAfterLastBuffUntilSelectionNoWhiteSpace = textUntilSelectionNoWhiteSpace.substringAfterLast("${CharProgressItemAdapterBuffs.BUFF_REF}=[", missingDelimiterValue)
            if (textAfterLastBuffUntilSelectionNoWhiteSpace != missingDelimiterValue) {
                if (!textAfterLastBuffUntilSelectionNoWhiteSpace.contains("]")) {
                    showBuffs = true
                }
            }



            return AutocompleteData(
                firstWordBeginningWithAtBeforeSelection = firstWordBeginningWithAtBeforeSelection,
                text = text,
                textUntilSelection = textUntilSelection,
                indexOfNextWhiteSpaceAfterAtBeforeSelection = indexOfNextWhiteSpaceAfterAtBeforeSelection,
                lastIndexOfAtBeforeSelection = lastIndexOfAtBeforeSelection,
                startSelection = startSelection,
                showBuffs = showBuffs
            )
        }

        fun proposeExp(
            s: Editable,
            maTv: MultiAutoCompleteTextView,
            cpiMap: CharProgressItemMap,
            character: Character? = null) {
            val text = s.toString()
            var exp_proposal = 0
                    logAutoComplete("++++++++++++++++++++++ generating exp proposal +++++++++++++++++");
            val nb_of_active_chars: Int = getNumberOfActiveCharsAndSetAttributePointExpCost(character)

            if (!(text.isEmpty() || text.contains(Global.FEATURED_MARKER)
                        || text.contains(Global.IGNORE_EXP_CHECK_INDICATOR))) {
                for (item in cpiMap.values.map { it.mainItem }) {
                    item.adapter?.let {
                            adapter ->
                        exp_proposal +=
                            adapter.getExpProposal(text,
                                nb_of_active_chars,
                                adapter.getAutoCompleteDataOrDataIfNoAutoCompleteDataAvailable())}
//                                    logAutoComplete("exp proposal " + item.header.uppercase() + " = " + exp_proposal);
                }
            } else {
                maTv.setText("")
            }
//                    logAutoComplete("exp_proposal = " + exp_proposal);
            if (exp_proposal != 0) {
                maTv.setText(exp_proposal.toString())
            }
        }

        /**
         * get the number of active chars and set Global.ATTRIBUTE_POINT_EXP_COST to chars value
         * so if there is only one char the correct attribute cost for this character will be used.
         * When there aremote than one active chars no exp will be proposed anyway.
         *
         * if a character if handed as parameter the result will be 1
         * and attribute point cost ill be loaded from this character
         * @return
         */
        fun getNumberOfActiveCharsAndSetAttributePointExpCost(character: Character? = null): Int {
            if (character != null) {
                Global.ATTRIBUTE_POINT_EXP_COST = character.ATTRIBUTE_POINT_EXP_COST
                return 1
            } else {
                var nb_of_active_chars = 0
                for (c in Global.chars) {
                    if (c.isActive) {
//                log("ZZZ", "${c.name} is Active !!!!!!!!!!!")
                        nb_of_active_chars++
                        Global.ATTRIBUTE_POINT_EXP_COST = c.ATTRIBUTE_POINT_EXP_COST
                    }
                }
            logAutoComplete("Number of active Chars: $nb_of_active_chars ########################################################################")
                return nb_of_active_chars
            }
        }

        class GenerateAutoCompleteDataArgs(
            val et: MyMultiAutoCompleteTextViewNonfiltering,
            val cpiMap: CharProgressItemMap,
            val loadedCharDetailsToAutoComplete: Boolean,
            val activeRuleSystems: List<RuleSystem>,
            val autoCompleteTextToAddAdapter: AutoCompleteTextToAddAdapter
        )

        fun generateAutoCompleteData(
            args: GenerateAutoCompleteDataArgs
        ): GenerateAutoCompleteDataResult? {
            var result: GenerateAutoCompleteDataResult? = null
            var containsCharProgressItemValueKeyWord = false
            var removeThresholdForAutoCompletition = false
            logAutoComplete("Filling Autocomplete Adapter --------------------------------");
            var list = ArrayList<String>()
            //this will later be added to autoCompleteAdapter
            val attributes_list = ArrayList<String>()
            val rulesSR = Helper.isShadowrun(args.activeRuleSystems)
            val rulesE5 = RuleSystem.rule_system_Dnd_E5 in args.activeRuleSystems
            val rulesOgresClub = RuleSystem.rule_system_ogres_club in args.activeRuleSystems

            fun fillWithStdListToAutoComplete(list: ArrayList<String>): ArrayList<String> {
                return Helper.fillWithStdListToAutoComplete(
                    sr = rulesSR,
                    list = list,
                    charProgressItemsMap = args.cpiMap
                )
            }

            try {

                /*
                get first word beginning with @ before selection to decide which char progress item is
                currently active
                 */
                val tmp = getFirstWordBeforeSelectionStartingWithAt(args.et)
                val firstWordBeginningWithAtBeforeSelection = tmp.firstWordBeginningWithAtBeforeSelection
                val text = tmp.text
                val textUntilSelection = tmp.textUntilSelection
                val indexOfNextWhiteSpaceAfterAtBeforeSelection = tmp.indexOfNextWhiteSpaceAfterAtBeforeSelection
                val lastIndexOfAtBeforeSelection = tmp.lastIndexOfAtBeforeSelection
                val startSelection = tmp.startSelection
                //--------------------------------------------
                for (item in args.cpiMap.values.map { it.mainItem }) {
                    if (firstWordBeginningWithAtBeforeSelection.equals(item.filterConstraint, ignoreCase = true).also { item.isActive = it }) {
                        containsCharProgressItemValueKeyWord = true
                    }
                }
                val wordAtCurrentCursorPosition: String = getWordAtCurrentCursorPosition(text, startSelection)
                val temp = getTextAfterFilterConstraintUntilNextAt(text, indexOfNextWhiteSpaceAfterAtBeforeSelection)
                val textAfterFilterConstraint = temp.first[0]
                val textAfterFilterConstraintUntilNextAt = temp.first[1]
                /*
                remove Threshold if text is empty or looks like
                fdsjköfskö @filterConstraint whitespace selectionBeginsHere
                */
                for (item in args.cpiMap.values.map { it.mainItem }) {
                    if (item.isActive && item.usesAutoComplete) {
                        removeThresholdForAutoCompletition = true
                    }
                }
                args.et.always_show = removeThresholdForAutoCompletition

                //DeBugging
                /*logAutoComplete("containsKeyWord = $containsCharProgressItemValueKeyWord");
                logAutoComplete("loadedCharDetailsToAutoComplete = $loadedCharDetailsToAutoComplete");
                logAutoComplete("text = '$text'");
                logAutoComplete("text after filter constraint = '$textAfterFilterConstraint'");
                logAutoComplete("text after filter constraint until next @ = '$textAfterFilterConstraintUntilNextAt'");
                logAutoComplete("removeThreshold = $removeThresholdForAutoCompletition");
                logAutoComplete("--------------------------------------------------------------");*/

                if (text.trim().isEmpty()) {
                    list.add(Global.IGNORE_EXP_CHECK_INDICATOR)
                    list.add(Global.ORIGIN_LANGUAGE_MARKER)
                    list.add(Global.FEATURED_MARKER)
                } else {
                    if (!args.loadedCharDetailsToAutoComplete) {
                        list = fillWithStdListToAutoComplete(list)
                        logAutoComplete("not loaded charDetails: list = std: $list");
                    } else {
                        if (!containsCharProgressItemValueKeyWord) {
                            list = fillWithStdListToAutoComplete(list)
                            logAutoComplete("loaded charDetails but non active: list = std: $list");
                        } else {
                            /*
                            at least one charProgressItem filter constraint is met and the threshold is
                            removed which means that there is only at least one word after the
                            active filter constraint
                             */
                            logAutoComplete("loaded charDetails and some are active: list: $list");
                            for (item in args.cpiMap.values.map { it.mainItem }) {
                                if (item.isActive) {
                                    if (wordAtCurrentCursorPosition.startsWith("-")) {
                                        item.adapter?.createAutoCompleteFeatureMarkers()?.forEach{marker -> list.add(marker)}
                                    } else {
                                        if (!textAfterFilterConstraintUntilNextAt.contains("@")) {
                                            /*
                                            if attributes are active add all attributes and also name of attributes
                                            that might not be included in this standard list (other rpgs, of Luck, Appearance...)
                                             */
                                            when {
                                                item.header.equals("Attributes", ignoreCase = true) && (rulesOgresClub || rulesE5) -> {
                                                    attributes_list.add("Strength")
                                                    attributes_list.add("Dexterity")
                                                    attributes_list.add("Constitution")
                                                    attributes_list.add("Intelligence")
                                                    attributes_list.add("Wisdom")
                                                    attributes_list.add("Charisma")
                                                    item.adapter?.data?.forEach { pair ->
                                                        pair as ValueNamePair
                                                        if (!Helper.arrayContainsElementIgnoringCases(
                                                                attributes_list,
                                                                pair.name
                                                            )
                                                        ) {
                                                            list.add(pair.name)
                                                        }
                                                    }
                                                    logAutoComplete("attributes list filled and added missing attributes: list :  " + list.toString());
                                                }
                                                item.usesAutoComplete -> {

                                                    if (!item.multipleAutoComplete) {
                                                        /*
                                                    for non Attributes att related skill/Languages... to auto complete list.
                                                     */
                                                        item.adapter?.createAutoCompleteStrings()
                                                            ?.let { autocomplete ->
                                                                logAutoComplete("names for item " + item.header + ": " + autocomplete.toString());
                                                                list.addAll(autocomplete)
                                                                logAutoComplete("added names for item " + item.header + " : list = " + list.toString());
                                                            }
                                                    } else {
                                                        if (!textAfterFilterConstraint.trim().equals("", ignoreCase = true)) {
                                                            item.adapter?.createAutoCompleteStrings()?.
                                                            filter { !textAfterFilterConstraint.contains(it) }
                                                                ?.let { list.addAll(it) }
                                                        }
                                                    }

                                                }
                                                else -> { //item doesn't use autocomplete
                                                    list = fillWithStdListToAutoComplete(list)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                logAutoComplete("sorted list: $list");
                list.addAll(attributes_list) //empty if attributes not active
//            Helper.log("COMPLETE_NAMES", "names to autocomplete: $characterNamesToAutocomplete");
                Helper.removeDuplicate(list) //should not be necessary but those autocomplete adapter ware weird sometimes...
                if (tmp.showBuffs) {
                    list.addAll(
                        BuffEffect.getBuffableStats(
                            ruleSystems = PrefsHelper(context = null).activeRuleSystems,
                            cpiMap = args.cpiMap
                        ))
                }
                list.sortWith(Helper.TAGS_AT_THE_END_COMPARATOR)
//            logAutoComplete("finish creating autocomplete list: " + list.toString());
                result = GenerateAutoCompleteDataResult(
                    list,
                    text,
                    textUntilSelection,
                    firstWordBeginningWithAtBeforeSelection,
                    textAfterFilterConstraint,
                    textAfterFilterConstraintUntilNextAt,
                    wordAtCurrentCursorPosition,
                    startSelection,
                    lastIndexOfAtBeforeSelection,
                    indexOfNextWhiteSpaceAfterAtBeforeSelection,
                    containsCharProgressItemValueKeyWord,
                    removeThresholdForAutoCompletition,
                    args)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return result
        }

        data class GenerateAutoCompleteDataResult(
            val list: ArrayList<String>,
            val text: String,
            val textUntilSelection: String,
            val firstWordBeginningWithAtBeforeSelection: String,
            val textAfterFilterConstraint: String,
            val textAfterFilterConstraintUntilNextAt: String,
            val wordAtCurrentCursorPosition: String,
            val startSelection: Int,
            val lastIndexOfAtBeforeSelection: Int,
            val indexOfNextWhiteSpaceAfterAtBeforeSelection: Int,
            val containsCharProgressItemValueKeyWord: Boolean,
            val removeThresholdForAutoCompletition: Boolean,
            val generateAutoCompleteDataArgs: GenerateAutoCompleteDataArgs
        )

        fun lastCharacterBeforeCursorIsComma(editText: EditText): Boolean {
            val startSelection = editText.selectionStart
            var text = editText.text.toString()
            text = text.substring(0, startSelection)
            if (text.lastIndexOf(" ") > 0) {
                text = text.substring(0, text.lastIndexOf(" ")).trim { it <= ' ' }
            }
            logAutoComplete("text before selection = $text")
            return text.endsWith(",")
        }

        private fun logAutoComplete(msg: String) {
            Log.d("AUTOCOMPLETE", "####$msg")
        }

        fun getTextAfterFilterConstraintUntilNextAt(text: String, indexOfNextWhiteSpaceAfterAtBeforeSelection: Int): Pair<Array<String>, Int> {
            var textAfterFilterConstraint = text
            var textAfterFilterConstraintUntilNextAt = textAfterFilterConstraint
            var indexOfFirstAtAfterActiveFilterConstraint = -1
            if (indexOfNextWhiteSpaceAfterAtBeforeSelection >= 0) {
                textAfterFilterConstraint = textAfterFilterConstraint.substring(indexOfNextWhiteSpaceAfterAtBeforeSelection).trim()
                textAfterFilterConstraintUntilNextAt = textAfterFilterConstraint
                indexOfFirstAtAfterActiveFilterConstraint = textAfterFilterConstraint.indexOf("@")
                if (indexOfFirstAtAfterActiveFilterConstraint >= 0) {
                    textAfterFilterConstraintUntilNextAt = textAfterFilterConstraint.substring(0, indexOfFirstAtAfterActiveFilterConstraint).trim()
                }
            }
            return Pair(arrayOf(textAfterFilterConstraint, textAfterFilterConstraintUntilNextAt), indexOfFirstAtAfterActiveFilterConstraint)
        }

        fun getWordAtCurrentCursorPosition(text: String, startSelection: Int): String { //        int startSelectionBackup = startSelection;
//        Helper.log("START_SELECTION", "start selection before loop = " + startSelection + " text.length = " + text.length());
//
//        if (!(text.length() == 0 || startSelection == 0 )) {
//            while (text.substring(startSelection - 1, startSelection).equalsIgnoreCase(" ")) {
//                Helper.log("START_SELECTION", "text at selection = " + text.substring(startSelection - 1, startSelection));
//                startSelection--;
//                if (startSelection == 0) {
//                    break;
//                }
//            }
//        }
//
//        Helper.log("START_SELECTION", "start selection after loop = " + startSelection);
//        if(startSelection > 1) {
//            Helper.log("START_SELECTION", "text to check if it's whitespace = " + text.substring(startSelection - 2, startSelection - 1));
//
//            if(text.substring(startSelection - 2, startSelection - 1).matches("\\s")) {
//                editText.setSelection(startSelection);
//            }
//
//        } else if (startSelection == 1) {
//
//            editText.setSelection(startSelection);
//
//        } else {
//
//            startSelection = startSelectionBackup;
//        }
//        Helper.log("START_SELECTION", "start selection after changing selection = " + startSelection);
            var selectedWord: String = ""
            var length = 0
            for (currentWord in text.split(Regex("\\s")).toTypedArray()) {
                length += currentWord.length + 1
                if (length > startSelection) {
                    selectedWord = currentWord
                    break
                }
            }
//                Helper.log("START_SELECTION", "selected word = " + selectedWord);
            return selectedWord
        }

        /**
         * @param character add a character here, if checking for number of active char == 1
         * should be ignored and attribute point cost from this character will be used
         */
        fun implementReplaceTextWithAutoCompleteProposal(
            multiAutoCompleteTextView: MyMultiAutoCompleteTextView,
            cpiMap: () -> CharProgressItemMap,
            character: Character? = null
        ) {
            multiAutoCompleteTextView.onItemClickListener =
                AdapterView.OnItemClickListener { parent, view, position, id ->
                    val auto_complete_proposal = parent.getItemAtPosition(position) as String
                    logAutoComplete("proposal clicked: $auto_complete_proposal")
                    /*
                    get first word beginning with @ before selection to decide which char progress item is
                    currently active
                    */
                    multiAutoCompleteTextView.let { textToAdd ->
                        val tmp = getFirstWordBeforeSelectionStartingWithAt(textToAdd)
                        val text = tmp.text

                        val indexOfNextWhiteSpaceAfterAtBeforeSelection =
                            tmp.indexOfNextWhiteSpaceAfterAtBeforeSelection
                        //                int lastIndexOfAtBeforeSelection = tmp.second[1];
                        val temp = getTextAfterFilterConstraintUntilNextAt(
                            text,
                            indexOfNextWhiteSpaceAfterAtBeforeSelection
                        )
                        //                String textAfterFilterConstraint= temp.first[0];
                        val textAfterFilterConstraintUntilNextAt = temp.first[1]
                        /*
                        the autocomplete from the editText itself is faster than calling this method
                        so the text we will get from the edit text will be the completed one
                         */
                        for (item in cpiMap().values.map { it.mainItem }) {
                            /*
                            THE FIRST PART IS FOR AUTO COMPLETING THE TEXT AFTER THE FILTER CONSTRAINT
                            */
                            if (tmp.firstWordBeginningWithAtBeforeSelection.equals(
                                    item.filterConstraint,
                                    ignoreCase = true
                                )
                                && item.usesAutoComplete &&
                                !textAfterFilterConstraintUntilNextAt.contains("@") &&
                                /*
                                the above is necessary because the standard autocomplete from autocompleteTextView
                                is faster so the filter constraint will be there even though I might have
                                entered only @ and then clicked
                                 */
                                !auto_complete_proposal.equals(
                                    item.filterConstraint,
                                    ignoreCase = true
                                ) &&
                                /*
                                this means we don't use "multiple auto complete" (I hope that it means that at least)
                                 */
                                !text.substring(0, tmp.startSelection).substringAfterLast(item.filterConstraint).contains(",")
                            ) {
                                                      logAutoComplete("CONDITION IS TRUE -----------------------------");

                                //Debugging
//                                logAutoComplete("selection start = " + tmp.startSelection);
//                                logAutoComplete("text until selection = '" + tmp.textUntilSelection + "'");
//                                logAutoComplete("last index of @ before selection = " + tmp.lastIndexOfAtBeforeSelection);
//                                logAutoComplete("index of next white space after last @ before selection = " + indexOfNextWhiteSpaceAfterAtBeforeSelection);
//                                logAutoComplete("first word beginning with @ before selection = '" + tmp.firstWordBeginningWithAtBeforeSelection + "'");



                                //DeBugging
//                                logAutoComplete("containsKeyWord = " + containsCharProgressItemValueKeyWord);
//                                logAutoComplete("loadedCharDetailsToAutoComplete = " + loadedCharDetailsToAutoComplete);
//                                logAutoComplete("text = '" + text + "'");
//                                logAutoComplete("text after filter constraint until next @ = '" + textAfterFilterConstraintUntilNextAt + "'");

                                var valueAutoCompleted = false
                                var value = ""
                                if (item.isValueBased) {
                                    if (getNumberOfActiveCharsAndSetAttributePointExpCost(
                                            character = character) == 1
                                        ) { //
                                        item.adapter?.getAutoCompleteValue(auto_complete_proposal)
                                            ?.let {
                                                value = " $it"
//                                                                                    logAutoComplete("-------------- value = " + value);
                                                valueAutoCompleted = true
                                            }
                                    }
                                }
                                logAutoComplete("value auto completed = $valueAutoCompleted, character = $character")
                                val replacement = item.adapter?.getAutoCompleteReplacement(auto_complete_proposal) ?: auto_complete_proposal
                                val indexOfAutoCmpltPrpAftrActvFltrCnstrEnd = (text.indexOf(
                                    auto_complete_proposal,
                                    indexOfNextWhiteSpaceAfterAtBeforeSelection
                                )
                                        + auto_complete_proposal.length)
                                var result_text = tmp.textUntilSelection.substring(
                                    0,
                                    indexOfNextWhiteSpaceAfterAtBeforeSelection
                                ) + " " + replacement + value
                                val end_of_completed_text = result_text.length
                                /*
                                logAutoComplete("index of auto complete proposal after active filter constraint = " + indexOfAutoCmpltPrpAftrActvFltrCnstrEnd);
                                logAutoComplete("index of next whitespace before selection = " + indexOfNextWhiteSpaceAfterAtBeforeSelection);
                                logAutoComplete("result text = " + result_text);
                                logAutoComplete("--------------------------------------------------------------");
                                */
                                if (indexOfAutoCmpltPrpAftrActvFltrCnstrEnd >= auto_complete_proposal.length &&
                                    indexOfAutoCmpltPrpAftrActvFltrCnstrEnd <= text.length - 1
                                ) {
                                    result_text += text.substring(
                                        indexOfAutoCmpltPrpAftrActvFltrCnstrEnd
                                    )
                                }
                                textToAdd.setText(result_text)
                                if (valueAutoCompleted) {
                                    textToAdd.setSelection(
                                        end_of_completed_text - value.length + 1,
                                        end_of_completed_text + 1
                                    )
                                } else {
                                    textToAdd.setSelection(end_of_completed_text + 1)
                                }
                            }
                        }
                        return@OnItemClickListener
                    }
                }
        }

        suspend fun fillAutocompleteData(args: GenerateAutoCompleteDataArgs) {

            withContext(Dispatchers.Default) {
                generateAutoCompleteData(args)
            }?.let { acDataRes ->
                withContext(Dispatchers.Main) {

                    acDataRes.generateAutoCompleteDataArgs.let { args ->
                        args.autoCompleteTextToAddAdapter.setData(acDataRes.list)
                        //                autoCompleteTextAdapter.notifyDataSetChanged();
                        var filter_string =
                            acDataRes.wordAtCurrentCursorPosition // = word at current cursor position
                        /*
                    for items where we want auto complete we take as
                    filter string all that is right after the filter constraint.
                    Like this also whole phrases can be auto completed (e.g. for feats)
                    */
                        val textAfterFilterConstraintUntilNextAt =
                            acDataRes.textAfterFilterConstraintUntilNextAt
                        if (!(textAfterFilterConstraintUntilNextAt.contains(Regex("[,/]")))) {


                            args.cpiMap.values
                                .map { it.mainItem }.forEach { item ->
                                    if (item.isActive && item.usesAutoComplete && !filter_string.startsWith(
                                            "-"
                                        )
                                    ) {
                                        if (!textAfterFilterConstraintUntilNextAt.contains("@")) {
                                            if (!item.multipleAutoComplete ||
                                                !lastCharacterBeforeCursorIsComma(args.et)
                                            ) {
                                                filter_string = textAfterFilterConstraintUntilNextAt
                                            }
                                        }
                                    }
                                }
                        }
//                        logAutoComplete( "filter string  = '$filter_string'");
                        filter_string.let {
                            args.autoCompleteTextToAddAdapter.filter.filter(it)
                        }
                    }
                    /*
                //this makes text2Add lose focus sometimes and I don't know the reason for it, so I removed it
                if (removeThresholdForAutoCompletition)  {
                    textToAdd.showDropDownIfFocused();
                }
                */
//

                }
            }
        }



        //todo remove this class and use above function
        class FillAutoCompleteDataTask:
            AsyncTask<GenerateAutoCompleteDataArgs, Void, GenerateAutoCompleteDataResult?>() {

            @Deprecated("Deprecated in Java")
            override fun doInBackground(vararg params: GenerateAutoCompleteDataArgs): GenerateAutoCompleteDataResult? {
                return generateAutoCompleteData(params[0])
            }


            @Deprecated("Deprecated in Java")
            override fun onPostExecute(result: GenerateAutoCompleteDataResult?) {
                result?.let { acDataRes ->
                    acDataRes.generateAutoCompleteDataArgs.let { args ->
                        args.autoCompleteTextToAddAdapter.setData(acDataRes.list)
                        //                autoCompleteTextAdapter.notifyDataSetChanged();
                        var filter_string =
                            acDataRes.wordAtCurrentCursorPosition // = word at current cursor position
                        /*
                        for items where we want auto complete we take as
                        filter string all that is right after the filter constraint.
                        Like this also whole phrases can be auto completed (e.g. for feats)
                        */
                        val textAfterFilterConstraintUntilNextAt =
                            acDataRes.textAfterFilterConstraintUntilNextAt
                        if (!(textAfterFilterConstraintUntilNextAt.contains(Regex("[,/]")))) {


                            args.cpiMap.values
                                .map { it.mainItem }.forEach { item ->
                                    if (item.isActive && item.usesAutoComplete && !filter_string.startsWith(
                                            "-"
                                        )
                                    ) {
                                        if (!textAfterFilterConstraintUntilNextAt.contains("@")) {
                                            if (!item.multipleAutoComplete ||
                                                !lastCharacterBeforeCursorIsComma(args.et)
                                            ) {
                                                filter_string = textAfterFilterConstraintUntilNextAt
                                            }
                                        }
                                    }
                                }
                        }
//                        logAutoComplete( "filter string  = '$filter_string'");
                        filter_string.let {
                            args.autoCompleteTextToAddAdapter.filter.filter(it)
                        }
                    }
                    /*
                    //this makes text2Add lose focus sometimes and I don't know the reason for it, so I removed it
                    if (removeThresholdForAutoCompletition)  {
                        textToAdd.showDropDownIfFocused();
                    }
                    */
//
                }
            }
        }










    }
}