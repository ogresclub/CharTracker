package com.mtths.chartracker.utils

import android.content.Context
import com.mtths.chartracker.activities.ActivityLmAuth

class LmAuth {

    companion object {
        fun startAuth(context: Context) {
            context.startActivity(ActivityLmAuth.makeIntent(context))
        }
    }

}