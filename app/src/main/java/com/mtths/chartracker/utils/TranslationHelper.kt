package com.mtths.chartracker.utils

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import com.mtths.chartracker.dataclasses.CharProgressItem
import java.util.Locale

class TranslationHelper(val context: Context) {

    fun getAllTranslations(stringResourceId: Int): List<String> {
        return getAllTranslations(listOf(stringResourceId)).also {
//            Log.d("ALL_TRANSLATIONS","all translations for resource $stringResourceId: ${it.joinToString()}")
        }
    }

    fun getAllTranslations(stringIdsList: List<Int>): List<String> {
        return ALL_LOCALE_LANGUAGE_CODES.map { langCode ->
            getResource(langCode)
        }.let {locRes ->
            stringIdsList.map {
                id -> locRes.map {
                    res ->  res.getString(id)
                }
            }
        }.flatten()
    }

    fun getEnglishTranslation(stringResourceId: Int) =
        getResource("en").getString(stringResourceId)

    /**
     * A string to be used in a regex representing all translations of the stringResource
     */
    fun getAllTranslationsRegex(stringResourceId: Int): String {
        return getAllTranslations(stringResourceId)
            .joinToString(separator = "|", prefix = "(?:", postfix = ")")
    }

    fun equalsAnyIgnoreCase(s: String, stringResourceId: Int): Boolean {
        return getAllTranslations(stringResourceId).any {it.equals(s, ignoreCase = true)}
    }

    private fun getResource(localeLanguageCode: String): Resources =
        context.createConfigurationContext(
            getConfiguration(localeLanguageCode)
        ).resources

    private fun getConfiguration(localeLanguageCode: String): Configuration {
        return Configuration(context.resources.configuration).apply {
            setLocale(Locale(localeLanguageCode))
        }
    }

    companion object {
        val ALL_LOCALE_LANGUAGE_CODES = listOf("de", "en")

        /**
         * if the filterConstraint in the (log entry) text is not using the current locale language
         * replace it with the current
         */
        fun updateFilterConstraintToCurrentLanguage(text: String, item: CharProgressItem): String {
            return TranslationHelper(item.context).getAllTranslations(item.filterConstraintId).let {
                    text.replace(Regex("(${it.joinToString("|")})"), item.filterConstraint)
                }

        }
    }
}