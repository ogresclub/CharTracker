package com.mtths.chartracker.utils

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.os.Build
import android.text.Layout
import android.text.Spanned
import android.text.style.BulletSpan
import android.text.style.LeadingMarginSpan

/**
 * Custom Bullet Span implementation (based on [BulletSpan])
 * Default implementation doesn't allow for radius modification
 */
class BulletSpanWithRadius : LeadingMarginSpan {
    private val mGapWidth: Int
    private val mBulletRadius: Int
    private val mWantColor: Boolean
    private val mColor: Int

    constructor() {
        mGapWidth = STANDARD_GAP_WIDTH
        mBulletRadius = STANDARD_BULLET_RADIUS
        mWantColor = false
        mColor = 0
    }

    constructor(gapWidth: Int) {
        mGapWidth = gapWidth
        mBulletRadius = STANDARD_BULLET_RADIUS
        mWantColor = false
        mColor = 0
    }

    constructor(bulletRadius: Int, gapWidth: Int) {
        mGapWidth = gapWidth
        mBulletRadius = bulletRadius
        mWantColor = false
        mColor = 0
    }

    constructor(bulletRadius: Int, gapWidth: Int, color: Int) {
        mGapWidth = gapWidth
        mBulletRadius = bulletRadius
        mWantColor = true
        mColor = color
    }

    override fun getLeadingMargin(first: Boolean): Int {
        return 2 * mBulletRadius + mGapWidth
    }

    @SuppressLint("NewApi")
    override fun drawLeadingMargin(c: Canvas, p: Paint, x: Int, dir: Int, top: Int, baseline: Int, bottom: Int,
                                   text: CharSequence, start: Int, end: Int, first: Boolean, l: Layout) {
        if ((text as Spanned).getSpanStart(this) == start) {
            val style = p.style
            var oldcolor = 0
            if (mWantColor) {
                oldcolor = p.color
                p.color = mColor
            }
            p.style = Paint.Style.FILL
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && c.isHardwareAccelerated) {
                if (sBulletPath == null) {
                    sBulletPath = Path()
                    // Bullet is slightly better to avoid aliasing artifacts on mdpi devices.
                    sBulletPath!!.addCircle(0.0f, 0.0f, 1.2f * mBulletRadius, Path.Direction.CW)
                }
                c.save()
                c.translate(x + dir * (mBulletRadius * 1.2f + 1), (top + bottom) / 2.0f)
                c.drawPath(sBulletPath!!, p)
                c.restore()
            } else {
                c.drawCircle(x + dir * (mBulletRadius + 1).toFloat(), (top + bottom) / 2.0f, mBulletRadius.toFloat(), p)
            }
            if (mWantColor) {
                p.color = oldcolor
            }
            p.style = style
        }
    }

    companion object {
        private var sBulletPath: Path? = null
        const val STANDARD_GAP_WIDTH = 2
        const val STANDARD_BULLET_RADIUS = 4
    }
}