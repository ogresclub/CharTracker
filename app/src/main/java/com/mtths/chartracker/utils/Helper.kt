package com.mtths.chartracker.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.*
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.LogAdapter
import com.mtths.chartracker.charprogressitemadapters.*
import com.mtths.chartracker.dataclasses.*
import com.mtths.chartracker.dialogs.DialogShowFilteredLogs
import com.mtths.chartracker.preferences.PrefsHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.*
import java.lang.reflect.InvocationTargetException
import java.nio.channels.FileChannel
import java.text.Normalizer
import java.util.*
import java.util.regex.Pattern
import kotlin.Comparator
import kotlin.collections.HashMap
import kotlin.math.min


/**
 * Some Help methods
 */
object Helper {

    /**
     * @param array          an array
     * @param elementToCheck check if this element is in the array
     * @return true is elemetTocheck ist contained in array (IGNORING CASES!), false else
     * @contract (null OR empty, _) -> false
     */
    fun arrayContainsElementIgnoringCases(array: ArrayList<String>?, elementToCheck: String?): Boolean {
        return array?.find { it.equals(elementToCheck, ignoreCase = true) } != null
    }


    /**
     * return the index of the String or -1 if non is found
     */
    fun getIndexOfString(array: ArrayList<String>?, elementToCheck: String?, ignoreCase: Boolean = true): Int {
        if (array != null && elementToCheck != null) {
            for ((i, s) in array.withIndex()) {
                if (s.equals(elementToCheck, ignoreCase = ignoreCase)) {
                    return i
                }
            }
        }
        return -1
    }

    fun replaceItem(list: ArrayList<Item>?, item: Item, addIfNotFound: Boolean = false) {
        list?.find { it.id == item.id }?.let { oItem ->
            list.indexOf(oItem).let { idx ->
                list[idx] = item
            }
        } ?: run {
            if (addIfNotFound) {
                list?.add(item)
            }
        }
    }

    /**
     * in pixel
     */
    fun getStatusBarHeight(activity: Activity): Int {
        var statusBarHeight = activity.resources.getDimensionPixelSize(R.dimen.margin_24dp)
        val resourceId = activity.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId != 0) {
            statusBarHeight = activity.resources.getDimensionPixelSize(resourceId)
        }
        return statusBarHeight
    }

    suspend fun deleteLogs(context: Context, logs: List<LogEntry>?) {
        logs?.let {
            Global.allLogs.removeAll(it)
            Global.logsFilteredBySession.removeAll(it)
            Global.logsToShow.removeAll(it)
        }
        withContext(Dispatchers.IO) {
            logs?.forEach { log ->
                DatabaseHelper.getInstance(context)?.deleteLogEntry(log)
            }
        }
    }

    /**
     * @param array          an array
     * @param elementToCheck check if this element is in the array
     * @return (null OR empty, _) ---> false
     */
    fun arrayContainsElement(array: ArrayList<Long>?, elementToCheck: Long): Boolean {
        return array?.contains(elementToCheck)?: false
    }

    /**
     * check if two array have a common element. Array may be null.
     *
     * @param a first array
     * @param b second array
     * @return true if they have a common element
     */
    fun haveCommonElements(a: ArrayList<Long>?, b: ArrayList<Long>?): Boolean {
        if (a != null && b != null) {
            for (eA in a) {
                for (eB in b) {
                    if (eA == eB) {
                        return true
                    }
                }
            }
        }
        return false
    }

    fun getDateTime(): String {
        val date = Date()
        return Global.MY_DATE_FORMAT.format(date)
    }



    fun toStringNice(a: ArrayList<String>?): String {
        return a?.let{it.toString().replace("[", "").replace("]", "")}?: ""
    }

    fun getNames(dbHelper: DatabaseHelper, ids: ArrayList<Long>, CLASS_CODE: Int): ArrayList<String> {
        val names = ArrayList<String>()
        for (id in ids) {
            when (CLASS_CODE) {
                Global.CHARACTER_REF -> names.add(dbHelper.getCharacterName(id))
                Global.TAG_REF -> names.add(dbHelper.getTagName(id))
                Global.SESSION_REF -> names.add(dbHelper.getSessionName(id))
            }
        }
        return names
    }

    fun copyToArray(list: ArrayList<Long>): LongArray { //copy to array because wome probs with Koltin and Java
        val array = LongArray(list.size)
        for (i in list.indices) {
            array[i] = list[i]
        }
        return array
    }

    /**
     * make sure that text really contains "@Value"
     * @param text
     * @return
     */
    fun getTextAfterAtValueAndTrim(text: String): String {
        return getTextAfterStringAndTrim(text, "@Value")
    }

    fun getTextAfterAtValueAndRemoveWhiteSpace(text: String): String {
        val after_at_value = cutAfterString(text, "@Value")
        //        Helper.log("CHAR_DETAILS", "text after @Value before removing whitespace = " + after_at_value);
        //        Helper.log("CHAR_DETAILS", "after removing whitespace = " + result);
        return after_at_value.replace("\\s".toRegex(), "")
    }

    /**
     * return th text between both strings or original text when
     */
    fun getTextBetween(text: String, first: String, second: String): String {
        var result = text
        result = result.substringAfter(first)
        result = result.substringBefore(second)
        return result
    }

    fun getTextBetweenStringAndAtValueAndTrim(text: String, string: String): String {
        return getTextBetween(text =  text, first = string, second = "@Value")
    }

    fun <T> removeDuplicate(list: MutableList<T>) {
        val h = HashSet(list)
        list.clear()
        list.addAll(h)
    }

    fun getTagIdsFromLogEntries(logs: ArrayList<LogEntry>): ArrayList<Long> {
        val activeSessionsTagIds = ArrayList<Long>()
        for ((_, _, _, _, tag_ids) in logs) {
            for (tag_id in tag_ids!!) {
                if (!arrayContainsElement(activeSessionsTagIds, tag_id)) {
                    activeSessionsTagIds.add(tag_id)
                }
            }
        }
        return activeSessionsTagIds
    }

    fun log(msg: String, logging: Boolean = true) {
        log("chartracker", msg, logging)
    }

    fun log(tag: String?, msg: String, logging: Boolean = true) {
        if (Global.logging && logging) {
            Log.d(tag, msg)
        }
    }

    fun getSortedTagNamesWithHashTagFromLogEntries(logs: ArrayList<LogEntry>, dbHelper: DatabaseHelper): ArrayList<String> {
        val tagNames = ArrayList<String>()
        for (id in getTagIdsFromLogEntries(logs)) {
            tagNames.add("#" + dbHelper.getTagName(id))
        }
        tagNames.sort()
        return tagNames
    }

    fun toastChangesInParameters(changesIndication: BooleanArray, whatChanged: Array<String>, newValues: Array<*>, context: Context?) {
        var s = ""
        changesIndication.forEachIndexed { i, changed ->
            if (changed) {
                s += whatChanged[i] + " ${context?.getString(R.string.msg_changed_to)} " + newValues[i] + "\n"
            }
        }
        if (s.isNotEmpty()) { //remove last linebreak "\n" from s
            s = s.dropLast(1)
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show()
        }
    }

    fun getIds(chars: ArrayList<Character>): ArrayList<Long> {
        val charIds = ArrayList<Long>()
        for (c in chars) {
            charIds.add(c.id)
        }
        return charIds
    }

    private
    fun addTag(t: Tag, names: ArrayList<String>, hashTag: Boolean) {
        if (hashTag) {
            names.add("#" + t.name)
        } else {
            names.add(t.name)
        }
    }

    fun fillWithStdListToAutoComplete(
        sr: Boolean = false,
        list: ArrayList<String>,
        charProgressItemsMap: CharProgressItemMap):
            ArrayList<String> {
        list.addAll(Global.tagsToAutoComplete)
        Global.characterNamesToAutocomplete.forEach {
            list.addAll(it.names)
        }
        Global.placesNamesToAutocomplete.forEach {
            list.addAll(it.names)
        }
        for (item in charProgressItemsMap.values.map { it.mainItem }) {
            list.add(item.filterConstraint)
        }
        list.add(Global.IGNORE_EXP_CHECK_INDICATOR)
        list.add(Global.FEATURED_MARKER)
        if (sr) {
            list.add(Global.ORIGIN_LANGUAGE_MARKER)
            list.add(Global.MOTHER_TONGUE_LANGUAGE_MARKER)
        }
        list.addAll(Global.chars.map { it.name })
        list.sort()
        return ArrayList(list.distinct())

    }
    fun removeUnnecassaryZeros(floatString: String): String {
        return floatString.replace("\\.?,?0+$".toRegex(), "")
    }

    fun getTagsFromString(text: String): ArrayList<String> {
        val tag_names = ArrayList<String>()
        //getTags written not in front of the text
        val MY_PATTERN = Pattern.compile("#(\\S+)")
        val mat = MY_PATTERN.matcher(text)
        while (mat.find()) {
            var edited_string = mat.group(1)
            while (edited_string.startsWith("##")) { //cut unnecessary # in beginning
                edited_string = edited_string.substring(1)
            }
            if (edited_string.endsWith(".") || edited_string.endsWith(",") || edited_string.endsWith("!") || edited_string.endsWith("?")) { //remove , or . in the end
                edited_string = edited_string.substring(0, edited_string.length - 1)
            }
            //remove all ' from strings because the make trouble with sqlite syntax
            edited_string.replace("'", "")
            addIfNotContainedIgnoringCases(tag_names, edited_string)
        }
//        log("Tags", "$text \n $tag_names")
        return tag_names
    }

    fun addIfNotContainedIgnoringCases(list: ArrayList<String>, elementToAdd: String): ArrayList<String> {
        if (!arrayContainsElementIgnoringCases(list, elementToAdd)) {
            list.add(elementToAdd)
        }
        return list
    }

    @JvmStatic
    fun addIfNotContained(list: ArrayList<Long>, elementToAdd: Long): ArrayList<Long> {
        if (!arrayContainsElement(list, elementToAdd)) {
            list.add(elementToAdd)
        }
        return list
    }

    @JvmStatic
    fun getIntFromEditText(eT: EditText): Int {
        val valueAsString = eT.text.toString().trim { it <= ' ' }
        val value: Int
        value = if (valueAsString.length == 0) {
            0
        } else {
            valueAsString.toInt()
        }
        return value
    }

    @JvmStatic
    fun toastCheckPermissionToWriteExternalStorage(context: Context) {
        Toast.makeText(context.applicationContext,
            "Not able to write to external storage. Please check if permission to write to " +
                    "storage is granted in settings/apps/CharTracker/permissions",
            Toast.LENGTH_LONG).show()
    }

    @JvmStatic
    fun toastXmlImportSummery(data: DatabaseHelper.XmlImportResult, context: Context) {

        /*
                             * Build strings to put together the toasts depending on the situation.
                             */
        var displayText = ""
        if (data.logsCreatedCounter > 0) {
            displayText += ("\n" + data.logsCreatedCounter
                    + " new " + logEntries(data.logsCreatedCounter) + " imported")
        }
        if (data.logsUpdatedCounter > 0) {
            displayText += "\n" + data.logsUpdatedCounter + " " + logEntries(data.logsUpdatedCounter) + " updated"
        }
        if (data.newCreatedChars.size > 0) {
            displayText += ("\nCharacters created for reference purpose: "
                    + data.newCreatedChars.toString().replace("[", "").replace("]", ""))
        }
        if (data.newCreatedTags.size > 0) {
            displayText += ("\nTags created: "
                    + data.newCreatedTags.toString().replace("[", "").replace("]", ""))
        }
        /*
                             * check Conditions
                             */displayText = if (data.newCreatedChars.size == 0 && data.newCreatedTags.size == 0 &&
            data.logsCreatedCounter == 0L && data.logsUpdatedCounter == 0L) {
            "Everything up to date"
        } else { //remove new Line in the beginning.
            displayText.substring(1)
        }
        //Toast
        Toast.makeText(context, displayText, Toast.LENGTH_LONG).show()
    }

    private fun logEntries(i: Long): String {
        return if (i == 1L || i == -1L) {
            "log entry"
        } else {
            "log entries"
        }
    }

    /**
     * a map mapping all known filter constraints (lowercase) from all known char progress items
     * @param cpiMap to know all relevant CharProgressItems and their filter constraint
     *
     */
    fun getCurrentFilterConstraintMap(cpiMap: CharProgressItemMap, context: Context): HashMap<String, String> {
        return HashMap<String, String>().apply {
            cpiMap.keys.forEach{
                val fc = context.getString(it) // current language filter constraint
                TranslationHelper(context).getAllTranslations(it).forEach {
                    put(it.lowercase(),fc)
                }

            }
        }
    }

    /**
     * each filter constraint is mapped to a list of all logs using the filter constraint
     * the filter constraint will be translated into the current app laguage
     * @param cpiMap to know all relevant CharProgressItems and their filter constraint
     *
     *
     */
    fun sortLogsByTranslatedFilterConstraint(
        logs: List<LogEntry>,
        cpiMap: CharProgressItemMap,
        context: Context
    ): HashMap<String, MutableList<LogEntry>> {

        return HashMap<String, MutableList<LogEntry>>().apply {
            val translator =  getCurrentFilterConstraintMap(cpiMap = cpiMap, context = context)
            logs.forEach { e ->
                getFilterConstraints(e.text).forEach { fc ->
                    getOrPut(
                        key = translator[fc.lowercase()] ?: fc,
                        defaultValue = { mutableListOf() }
                    ).add(e)
                }
            }
        }
    }


    /**
     * @return true if able to write to external storage, false if not
     */


    fun getAppDirInternalStorage(context: Context): String {
        val m = context.packageManager
        var s = context.packageName
        try {
            val p = m.getPackageInfo(s, 0)
            s = p.applicationInfo?.dataDir
        } catch (e: PackageManager.NameNotFoundException) {
            log("LOADING_APP_INTERNAL_DIR", "Error Package name not found ")
        }
        return s
    }

    fun getMarkers(s: String): String {
        var result = ""
        val matcher = Global.PATTERN_MARKER.matcher(s)
        while (matcher.find()) {
            matcher.group()
            result += " ${matcher.group()}"
        }
        return result
    }

    fun getCharIconsFolderPath(context: Context): String {
        return getCharIconsFolderPath(getAppDirInternalStorage(context))
    }

    fun getCharIconsFolderPath(appInternalDir: String): String {
        return "$appInternalDir/${Global.ICONS_FOLDER}"
    }

    fun getIconPath(charName: String, context: Context, type: String): String {
        return "${getCharIconsFolderPath(context)}/${getIconFileName(charName, type)}"
    }

    fun getIconFileName(charName: String, type: String): String {
        return "${normalizeString(charName)}.$type"
    }

    fun normalizeString(s: String): String {
        return normalize(s).replace(" ", "_")
    }

    fun getDatabasePath(context: Context): String {
        return "${getAppDirInternalStorage(context)}/databases/database"
    }

    /**
     * method to write String to a file with option to overwrite.
     * no overwrite means that it will be written to the end of the file.
     */

    fun writeToFile(file: File, data: ArrayList<String>, FileWillBeOverwritten: Boolean): kotlin.Pair<Boolean, String> {
        var filePrinter: PrintStream? = null
        filePrinter = try {
            if (FileWillBeOverwritten) {
                PrintStream(FileOutputStream(file))
            } else {
                PrintStream(FileOutputStream(file, true))
            }
        } catch (e: IOException) {
            val msg = "Writing to file failed!: " + e.message
            print(msg)
            return kotlin.Pair(false, msg)
        }
        if (filePrinter != null) {
            for (line in data) {
                filePrinter.println(line)
            }
            filePrinter.close()
            return kotlin.Pair(true, "printed to file successfully")
        }
        return kotlin.Pair(false, "Unable to print to file. FilePrinter initiation failed.")
    }

    //    static ArrayList<String> readCharLogFromTextFile(String fileName, String charName, Context context) {
//        String path =  APP_EXTENAL_FILE_DIRECTORY + "/" + fileName;
//
//        BufferedReader br = null;
//        try {
//            br = new BufferedReader(
//                    new InputStreamReader(new FileInputStream(path)));
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        if (br != null) {
//            String line = null;
//            try {
//                line = br.readLine();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            while (line != null) {
//                //do something to the line
//            }
//        }
//        Toast.makeText(context, "Successfully read from file", Toast.LENGTH_SHORT).loadedCharDeatails();
//    }
    @JvmStatic
    fun filterByChars(logs: ArrayList<LogEntry>, chars: ArrayList<Character>): ArrayList<LogEntry> { //get loadedCharDeatails char listOfViews
        val charIds = ArrayList<Long>()
        for (c in chars) {
            charIds.add(c.id)
        }
        val filteredLogs = ArrayList<LogEntry>()
        for (e in logs) { //if at least on char is loadedCharDeatails, get logs with at least one of those chars related
            if (charIds.size > 0) {
                if (haveCommonElements(e.character_ids, charIds)) {
                    filteredLogs.add(e)
                }
            } else { //if no char is active, get logs with no char related.
                if (e.character_ids!!.size == 0) {
                    filteredLogs.add(e)
                }
            }
        }
        return filteredLogs
    }

    @JvmStatic
    fun filterByActiveSessions(logs: ArrayList<LogEntry>, activeSessionIds: ArrayList<Long>): ArrayList<LogEntry> {
        val filteredLogs = ArrayList<LogEntry>()
        for (e in logs) { //if at least on char is loadedCharDeatails, get logs with at least one of those chars related
            if (activeSessionIds.size > 0) {
                if (haveCommonElements(e.session_ids, activeSessionIds)) {
                    filteredLogs.add(e)
                }
            } else { //if no char is loadedCharDeatails, get logs with no char related.
                if (e.session_ids!!.size == 0) {
                    filteredLogs.add(e)
                }
            }
        }
        return filteredLogs
    }

    /**
     * returns the first index, where an element of logs hab the same id as e.
     *
     * @param logs
     * @param e
     * @return
     */
    @JvmStatic
    fun indexOf(logs: ArrayList<LogEntry>, e: LogEntry): Int {
        val size = logs.size
        for (i in 0 until size) {
            if (e.id == logs[i].id) {
                return i
            }
        }
        return -1
    }

    /**
     * get index by comparing ids
     *
     * @return -1 of no char with same id found
     */
    @JvmStatic
    fun indexOf(chars: ArrayList<Character>, c: Character): Int {
        return indexOf(chars, c.id)
    }

    /**
     * get index by comparing ids
     *
     * @return -1 of no char with same id found
     */
    @JvmStatic
    fun indexOf(chars: ArrayList<Character>, char_id: Long): Int {
        val size = chars.size
        for (i in 0 until size) {
            if (char_id == chars[i].id) {
                return i
            }
        }
        return -1
    }

    fun remove(chars: ArrayList<Character>, c: Character): Boolean {
        val i = indexOf(chars, c)
        return if (i == -1) {
            false
        } else {
            chars.removeAt(i)
            true
        }
    }

    fun update(chars: ArrayList<Character>, context: Context?) {
        val dbHelper = DatabaseHelper.getInstance(context)
        for (i in chars.indices) {
            chars[i] = dbHelper!!.getCharacter(chars[i].id)
        }
    }

    fun update(chars: ArrayList<Character>, c: Character): Boolean {
        val index = indexOf(chars, c)
        return if (index >= 0) {
            chars.removeAt(index)
            chars.add(index, c)
            true
        } else {
            false
        }
    }

    /**
     * update character in db as well as in Global.chars
     * @return if the character was in Global.chars
     */
    fun updateCharacter(character: Character, context: Context, keep_last_update_date: Boolean): Boolean {
        DatabaseHelper.getInstance(context)?.updateCharacter(
            character = character,
            keep_last_update_date = keep_last_update_date)
        return update(Global.chars, character)
    }

    fun deleteStoredRuleSystemsForCharsRelatedToLog(e: LogEntry, dbHelper: DatabaseHelper?) {
        // Delete Rule Systems for chraacters
        e.character_ids?.forEach { id ->
            dbHelper?.getCharacter(id)?.let {
                it.ruleSystems = null
                dbHelper.updateCharacter(character =  it, keep_last_update_date = true)
            }
        }
    }

    fun deleteStoredRuleSystemsForCharsRelatedToSession(sessionId: Long, dbHelper: DatabaseHelper?) {
        dbHelper?.getCharsRelatedToSessions(
            sessionIds = arrayListOf(sessionId),
            show_all_chars = true)?.forEach {
            it.ruleSystems = null
            dbHelper.updateCharacter(it, keep_last_update_date = true)
        }
    }

    fun useLandscapeMode(resources: Resources): Boolean {
        return resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
    }

    fun copyStreamToFile(inputStream: InputStream, outputFile: File) {
        inputStream.use { input ->
            val outputStream = FileOutputStream(outputFile)
            outputStream.use { output ->
                val buffer = ByteArray(4 * 1024) // buffer size
                while (true) {
                    val byteCount = input.read(buffer)
                    if (byteCount < 0) break
                    output.write(buffer, 0, byteCount)
                }
                output.flush()
            }
        }
    }

    fun isDigit(cs: Char): Boolean {
        return cs in "0123456789".toCharArray()
    }

    fun isShadowrun(rSList: List<RuleSystem>): Boolean {
        return activeRules(listOf(RuleSystem.rule_system_sr5, RuleSystem.rule_system_sr6), rSList)
    }

    fun isDnd(rSList: List<RuleSystem>): Boolean {
        return activeRules(
            listOf(
                RuleSystem.rule_system_Dnd_E5,
                RuleSystem.rule_system_ogres_club,
                RuleSystem.rule_system_skillfull_dnd_5E
            ), rSList)
    }

    fun activeRules(allowedRules: List<RuleSystem>, activeRuleSystems: List<RuleSystem>): Boolean {
        return allowedRules.intersect(activeRuleSystems.toSet())
            .isNotEmpty()
    }

    /**
     * return all words starting with @ in string.
     */
    fun getFilterConstraints(s: String): ArrayList<String> {
        val result = ArrayList<String>()
        val m = Global.PATTERN_FILTER_CONSTRAINT.matcher(s)
        while (m.find()) {
            m.group().let {
                result.add(it.trim())
            }
        }
        removeDuplicate(result)
        return result
    }

    fun sign(i: Int) = if (i < 0) "-" else "+"

    fun capitalizeNewsLetterStyle(input: String): String {
        val components = input.split("\\s+".toRegex())

        return buildString {
            components.forEachIndexed { index, word ->
                when (index) {
                    in 1..components.size - 2 -> word.capitalizeMiddleWord() // Some short auxiliary words aren't capitalized
                    else -> word.replaceFirstChar(Char::uppercaseChar) // The first and the last words are always capitalized
                }.let { append(it).append(' ') }
            }
            deleteCharAt(length - 1) // Drop the last whitespace
        }
    }

    private fun String.capitalizeMiddleWord(): String =
        if (length > 3 || this !in NON_CAPITALIZED_WORDS) replaceFirstChar(Char::uppercaseChar) else this

    val NON_CAPITALIZED_WORDS = setOf(
        "as", "at", "but", "by", "for", "of", "the" // and so on
    )

    fun updateLogDbAndGlobal(e: LogEntry, context: Context) {
        DatabaseHelper.getInstance(context)?.updateLogEntry(e)
        update(Global.allLogs, e)
        update(Global.logsFilteredBySession, e)
        update(Global.logsToShow, e)
    }

    fun update(logs: ArrayList<LogEntry>, e: LogEntry): Boolean {
        val index = indexOf(logs, e)
        return if (index >= 0) {
            logs[index] = e
            true
        } else {
            false
        }
    }

    fun remove(logs: ArrayList<LogEntry>, e: LogEntry): Boolean {
        val i = indexOf(logs, e)
        return if (i == -1) {
            false
        } else {
            logs.removeAt(i)
            true
        }
    }

    fun add(list: ArrayList<Character>, c: Character) {
        list.add(c)
        Collections.sort(list, Character.PRIORITY_COMPARATOR)
    }

    fun getActiveChars(chars: ArrayList<Character>): ArrayList<Character> {
        val activeChars = ArrayList<Character>()
        for (c in chars) {
            if (c.isActive) {
                activeChars.add(c)
            }
        }
        return activeChars
    }

    fun getResId(resName: String?, c: Class<*>): Int {
        return try {
            val idField = c.getDeclaredField(resName!!)
            idField.getInt(idField)
        } catch (e: Exception) {
            e.printStackTrace()
            -1
        }
    }

    fun sortChars(chars: ArrayList<Character>) {
        chars.sortWith(Character.PRIORITY_COMPARATOR)
        chars.sortWith(Character.DEAD_COMPARATOR)
        chars.sortWith(Character.RETIRED_COMPARATOR)
        chars.sortWith(Character.RETIRED_AND_DEAD_COMPARATOR)
    }

    /**
     * may return null if char is not in chars.
     * @param char_id
     * @return
     */
    fun getChar(char_id: Long): Character? {
        val index = indexOf(Global.chars, char_id)
        return if (index >= 0) {
            Global.chars[index]
        } else {
            null
        }
    }

    fun getStatAttributeModification(attribute: String, text: String?): Int? {
        var result: Int? = null
        text?.let {
            val m = Pattern.compile("(?i)$attribute\\s*(-?\\+?\\d+)", Pattern.CASE_INSENSITIVE)
                .matcher(text)
            if (m.find()) {
                result = m.group(1)?.toInt()
            }
        }
        return result
    }



    /**
     * Simply add new Icons to the listOfViews to make them available in the app
     *
     * @return A listOfViews associating names of icons to the actual drawable.
     */

    fun getCharIconsMap(inclErrorIcon: Boolean = true): HashMap<String, Int> {
        val map = HashMap<String, Int>()
        //// ARMOR ////////
        map["shoulder_armor"] = R.drawable.ic_char_shoulder_armor
        map["gauntlet"] = R.drawable.ic_char_gauntlet
        //masks & helmets
        map["dread_skull"] = R.drawable.ic_char_dread_skull
        //shield
        map["bordered_shield"] = R.drawable.ic_char_bordered_shield
        map["rosa_shield"] = R.drawable.ic_char_rosa_shield
        map["winged_shield"] = R.drawable.ic_char_winged_shield
        map["swords_emblem"] = R.drawable.ic_char_swords_emblem
        ////// WEAPONS /////
        //fist
        map["fist"] = R.drawable.ic_char_fist
        //hammer
        map["thor_hammer"] = R.drawable.ic_char_thor_hammer
        //sword
        map["sword_hilt"] = R.drawable.ic_char_sword_hilt
        map["crossed_swords"] = R.drawable.ic_char_crossed_swords
        //axe
        map["meat_cleaver"] = R.drawable.ic_char_meat_cleaver
        map["crossed_axes"] = R.drawable.ic_char_crossed_axes
        //rouge style
        map["curvy_knife"] = R.drawable.ic_char_curvy_knife
        map["rogue"] = R.drawable.ic_char_rogue
        //bow
        map["high_shot"] = R.drawable.ic_char_high_shot
        map["bowman"] = R.drawable.ic_char_bowman
        //arrow
        map["interleaved_arrows"] = R.drawable.ic_char_interleaved_arrows
        //staff
        map["bo"] = R.drawable.ic_char_bo
        //improvised
        map["rock"] = R.drawable.ic_char_rock
        //natural
        map["ivory_tusks"] = R.drawable.ic_char_ivory_tusks
        map["fangs"] = R.drawable.ic_char_fangs
        map["fluffy_wing"] = R.drawable.ic_char_fluffy_wing

        ////// MAGIC /////////
        //scrolls & books
        map["tied_scroll"] = R.drawable.ic_char_tied_scroll
        map["book_cover"] = R.drawable.ic_char_book_cover
        map["kindle"] = R.drawable.ic_char_kindle
        //wizard stereotype
        map["wizard_staff"] = R.drawable.ic_char_wizard_staff
        map["pointy_hat"] = R.drawable.ic_char_pointy_hat
        //spells
        map["lightning_branches"] = R.drawable.ic_char_lightning_branches
        //Animals And Creatures
        map["pawprint"] = R.drawable.ic_char_paw_print
        map["ratling"] = R.drawable.ic_char_ratling
        map["troll"] = R.drawable.ic_char_troll
        map["bull"] = R.drawable.ic_char_bull
        map["dwarf_face"] = R.drawable.ic_char_dwarf_face
        ///////// OTHER /////////
        map["yin_yang"] = R.drawable.ic_char_yin_yang
        map["voodoo_doll"] = R.drawable.ic_char_voodoo_doll
        map["imprisoned"] = R.drawable.ic_char_imprisoned
        map["beard"] = R.drawable.ic_char_beard
        map["drum"] = R.drawable.ic_char_drum
        map["violin"] = R.drawable.ic_char_violin
        map["starlight"] = R.drawable.ic_char_starlight
        map["pope_crown"] = R.drawable.ic_char_pope_crown
        map["take_my_money"] = R.drawable.ic_take_my_money

        //healing
        map["first_aid_kit"] = R.drawable.ic_char_first_aid_kit
        map["heart_bottle"] = R.drawable.ic_char_heart_bottle



        if (inclErrorIcon) { //error when getting character
            map["error"] = Character.ERROR_ICON
        }
        return map
    }

    fun normalize(s: String?): String {
        return Normalizer.normalize(s, Normalizer.Form.NFD).replace("\\p{InCombiningDiacriticalMarks}+".toRegex(), "").lowercase()
    }

    /**
     * All Strings in toHighlight will be highlighted, ignoring cases and accents
     *
     * @param toHighlight         all the parts that should be highlighted
     * @param originalText        the text where parts should be highlighted. This is just used as a reference.
     * @param what                something like StyleSpan or ForegroundColorSpan
     * EXAMPLE:
     * *what = new ForegroundColorSpan(Color.BLUE)
     * *what = new StyleSpan(android.graphics.Typeface.BOLD)
     * @param constructorArgument eg android.graphics.Typeface.BOLD / ITALIC ... for StyleSpan or color.BLUE for ForegroundColorSpan
     * @param cArgs               this is needed to get the Constructor of the class "what".
     * Those classes are the classes which are arguments of the constructor of what.
     * @param spannable           this is the text which might be styled before
     *
     * @return highlighted spannableText as charSequence
     */
    fun highlightText(toHighlight: List<String>?,
                      originalText: String,
                      what: Class<*>,
                      constructorArgument: Any?,
                      cArgs: Array<Class<*>?>,
                      spannable: SpannableStringBuilder,
                      noSubstringMatches: Boolean = false): SpannableStringBuilder {
        val otl = originalText.lowercase(Locale.getDefault())
        val DEBUG_TAG = "HIGHLIGHTING"
//        log(DEBUG_TAG, "Start Highlighting:$what")
        if (toHighlight != null && toHighlight.isNotEmpty()) {
            val normalizedText = normalize(originalText)
//            log(DEBUG_TAG, "normalized Text: '$normalizedText'")
            for (h in toHighlight) {
                if (h != "") {
                    val h_norm = normalize(h)
//                    log(DEBUG_TAG, "normalized String to highlight: '$h_norm'")
                    var start = normalizedText.indexOf(h_norm)
//                    log(DEBUG_TAG, "Start Index = $start")
                    while (start >= 0) {
                        val spanStart = min(start, originalText.length)
                        val spanEnd = min(start + h_norm.length, originalText.length)
//                        log(DEBUG_TAG, "using span on idices $spanStart to $spanEnd")
                        if (!(noSubstringMatches &&
                                    !hasNoPreOrSuffix(textLowerCase =  otl, name = h, start = spanStart))) {
                            try {
                                spannable.setSpan(
                                    what.getConstructor(*cArgs).newInstance(constructorArgument),
                                    spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                                )
                            } catch (e: InstantiationException) {
                                e.printStackTrace()
                            } catch (e: IllegalAccessException) {
                                e.printStackTrace()
                            } catch (e: InvocationTargetException) {
                                e.printStackTrace()
                            } catch (e: NoSuchMethodException) {
                                e.printStackTrace()
                            }
                        }
                        start = normalizedText.indexOf(h_norm, spanEnd)
                    }
                }
            }
        }
        return spannable
    }

    fun getKey(map: HashMap<String, Int>, value: Int): String? {
        for (key in map.keys) {
            if (map[key] == value) {
                return key //return the first found
            }
        }
        return null
    }

    fun setTypeface(textToChange: ArrayList<String>?,
                    originalText: String,
                    typeFaceSpanConstructorArg: String?,
                    spannable: SpannableStringBuilder,
                    noSubstringMatches: Boolean = false): SpannableStringBuilder {
        return highlightText(
            toHighlight = textToChange,
            originalText = originalText,
            what = TypefaceSpan::class.java,
            constructorArgument = typeFaceSpanConstructorArg,
            cArgs = arrayOf(String::class.java),
            spannable = spannable,
            noSubstringMatches = noSubstringMatches)
    }

    fun setStyle(textToChange: List<String>?,
                 originalText: String,
                 styleSpanConstructorArg: Int,
                 spannable: SpannableStringBuilder,
                 noSubstringMatches: Boolean = false): SpannableStringBuilder {
        return highlightText(
            toHighlight = textToChange,
            originalText = originalText,
            what = StyleSpan::class.java,
            constructorArgument = styleSpanConstructorArg,
            cArgs = arrayOf(Int::class.javaPrimitiveType),
            spannable = spannable,
            noSubstringMatches = noSubstringMatches)
    }

    fun setColor(textToChange: ArrayList<String>?,
                 originalText: String,
                 ForegroundColorSpanConstructorArg: Int,
                 spannable: SpannableStringBuilder,
                 noSubstringMatches: Boolean = false): SpannableStringBuilder {
        return highlightText(
            toHighlight = textToChange,
            originalText = originalText,
            what =  ForegroundColorSpan::class.java,
            constructorArgument = ForegroundColorSpanConstructorArg,
            cArgs = arrayOf(Int::class.javaPrimitiveType),
            spannable = spannable,
            noSubstringMatches = noSubstringMatches)
    }

    fun decodeSampledBitmapFromResource(res: Resources?, resId: Int,
                                        reqWidth: Int, reqHeight: Int): Bitmap {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeResource(res, resId, options)
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeResource(res, resId, options)
    }

    fun decodeSampledBitmapFromInputStream(inputStream: InputStream,
                                           reqWidth: Int, reqHeight: Int): Bitmap? {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeStream(inputStream, null, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeStream(inputStream, null, options)
    }

    fun decodeSampledBitmapFromFile(path: String,
                                    reqWidth: Int, reqHeight: Int): Bitmap? {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeFile(path, options)
    }

    fun calculateInSampleSize(
        options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int { // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
//        log("ICON_STUFF", "width = $width, height = $height")
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            inSampleSize *= 2
            /*
            Calculate the largest inSampleSize value that is a power of 2 and keeps both
            height and width larger than the requested height and width.
            */
            while (height / inSampleSize >= reqHeight
                && width / inSampleSize >= reqWidth) {
                inSampleSize *= 2
//                log("ICON_STUFF", "inSampleSize = $inSampleSize")
            }
        }
//        log("ICON_STUFF", "inSampleSize RESULT = $inSampleSize")

        return inSampleSize
    }

    fun countMatches(str: String, findStr: String): Int {
        var lastIndex = 0
        var count = 0
        while (lastIndex != -1) {
            lastIndex = str.indexOf(findStr, lastIndex)
            if (lastIndex != -1) {
                count++
                lastIndex += findStr.length
            }
        }
        return count
    }

    /**
     * method to get exp from User Input (editText), deleting all 0 in the beginning
     */
    fun getExpToAdd(e: EditText): Int {
        val s = e.text.toString().trim { it <= ' ' }
        //when there was no imput, return ""
//we don't want to parse "" to int, because we might get error.
        return if (s.length == 0) {
            0
        } else { //no things like 007 should be 7 or 00000 should be 0
            e.text.toString().trim { it <= ' ' }.toInt()
        }
    }

    fun isAtLeastOneCharActive(chars: ArrayList<Character>): Boolean {
        var b = false
        for (character in chars) {
            b = b || character.isActive
        }
        return b
    }

    fun getRegexContainsFilterConstraint(filterConstraint: String): Regex {
        return Regex("(?i)(?s).*$filterConstraint\\s+.*")
    }

    // todo remove and don't use external storage permission
    fun askPermission(PERMISSION_CODES: Array<String?>, REQUEST_CODE: Int, activity: Activity?): Boolean {
        var allPermissionsGranted = true
        for (permission_code in PERMISSION_CODES) {
            allPermissionsGranted = allPermissionsGranted &&
                    (ContextCompat.checkSelfPermission(activity!!, permission_code!!)
                            == PackageManager.PERMISSION_GRANTED)
        }
        return if (!allPermissionsGranted) {
            ActivityCompat.requestPermissions(activity!!,
                PERMISSION_CODES,
                REQUEST_CODE)
            //here the result is not actually false, but we don't want to execute write to xml then
            //because this is done in method onRequestPermissionResult
            false
        } else {
            true
        }
    }

    fun capitalize(s: String): String {
        return s.replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString()
        }
    }

    /**
     * check that text contains string to avoid exception
     * @param text
     * @param string
     * @return
     */
    fun getTextAfterStringAndTrim(text: String, string: String?): String {
        return cutAfterString(text, string).trim()
    }

    /**
     * this result includes the string in the end and doesn't cut it
     * @param text
     * @param string
     * @return
     */
    fun getStuffBeforeString(text: String, string: String): String {
        val indexOfString = text.indexOf(string)
        return if (indexOfString >= 0) {
            text.substring(0, indexOfString + string.length)
        } else {
            text
        }
    }
    /**
     *
     * @param text
     * @param string
     * @return the part of the text that comes BEFORE the string,
     * or the original text if string is null or not found.
     * string is cut of
     */
    fun getTextBeforeString(text: String, string: String?): String {
        string?.let {
            val idx = text.indexOf(it)
            if (idx > -1) {
                return text.substring(0, idx)
            }
        }
        return text
    }

    open fun parseName(logEntryText: String, item: CharProgressItem?): String {
        var name = item?.let { item ->
            cutAfterString(
                text = TranslationHelper.updateFilterConstraintToCurrentLanguage(
                    text = logEntryText, item = item
                ),
                string = item.filterConstraint)
        } ?: logEntryText
        return name.substringBefore(",").trim()

    }

    /**
     *
     * @param text
     * @param string
     * @return the part of the text that come AFTER the string, or text if string is null, or ""
     */
    fun cutAfterString(text: String, string: String?): String {
        return string?.let { text.substringAfter(it) } ?: text
    }

    fun getTextBeforeFirstAt(text: String): String {
        return getStuffBeforeString(text, "@")
    }

    fun addIfNotAlreadyContainedIgnoringCases(list: ArrayList<String>, s: String) {
        if (!arrayContainsElementIgnoringCases(list, s)) {
            list.add(s)
        }
    }

    fun updateAllLogs(e: LogEntry) {
        update(Global.allLogs, e)
        update(Global.logsFilteredBySession, e)
        update(Global.logsToShow, e)
    }

    /**
     *
     * @param editText
     * @return 0 if string can not be converted to int
     */
    fun getInt(editText: EditText): Int {
        val int_as_string = editText.text.toString()
        return if (int_as_string.matches(Regex("[+-]\\d*"))) { //string consists only of digits
            int_as_string.toInt()
        } else {
            0
        }
    }

    fun createCharProgressWords(charProgressItemsMap: CharProgressItemMap): ArrayList<String> { /*
        Create list for AutoComplete
         */
        val charProgressionWords = ArrayList<String>()
        for (item in charProgressItemsMap.values.map { it.mainItem }) {
            if (!item.isStoryRelevant) {
                charProgressionWords.add(item.filterConstraint)
            }
        }
        return charProgressionWords
    }

    fun spToPx(sp: Float, context: Context): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.resources.displayMetrics).toInt();
    }

    fun dpToPx(dp: Float, context: Context): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.resources.displayMetrics).toInt();
    }


    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into dp
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    fun pxToDp(px: Float, context: Context): Float {
        return px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    /**
     * This method converts device specific pixels to scalable pixel.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into sp
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent sp equivalent to px value
     */
    fun pxToSp(px: Float, context: Context): Float {
        return px / context.resources.displayMetrics.density.also {
            Log.d("DENSITY__", "desity = $it")
        }
    }

    fun getUnCommonValues(list1: ArrayList<Long>, list2: ArrayList<Long>): List<Long> {
        return list1.minus(list2).plus(list2.minus(list1))
    }

    /**
     * Creates the specified `toFile` as a byte for byte copy of the
     * `fromFile`. If `toFile` already exists, then it
     * will be replaced with a copy of `fromFile`. The name and path
     * of `toFile` will be that of `toFile`.<br></br>
     * <br></br>
     * * Note: `fromFile` and `toFile` will be closed by
     * this function.*
     *
     * @param fromFile - FileInputStream for the file to copy from.
     * @param toFile   - FileInputStream for the file to copy to.
     */
    @Throws(IOException::class)
    fun copyFile(fromFile: FileInputStream, toFile: FileOutputStream) {
        var fromChannel: FileChannel? = null
        var toChannel: FileChannel? = null
        try {
            fromChannel = fromFile.channel
            toChannel = toFile.channel
            fromChannel.transferTo(0, fromChannel.size(), toChannel)
        } finally {
            try {
                fromChannel?.close()
            } finally {
                toChannel?.close()
            }
        }
    }

    fun addSessionsDisplaySpannable(context: Context, lineHeight: Int, sessions: List<Session>, sb: SpannableStringBuilder) {
        sessions.forEachIndexed { index, session ->
            sb.append(
                session.getDisplaySpannable(context, lineHeight)
            )
            if (index < sessions.size - 1) {
                if (!PrefsHelper(context).showRuleSystemIcons) sb.append(",")
                sb.append(" ")
            }
        }
    }

    /**
     * add data to list depending on rule system and return list
     */
    fun <T> addAllToList(context: Context, l: MutableList<T>, addForOgreClub: List<T>?, addForE5: List<T>?): List<T> {
        return addAllAccordingToRuleSystem(context, l, HashMap<RuleSystem, List<T>>().apply {
            if (addForOgreClub != null) {
                put(RuleSystem.rule_system_ogres_club, addForOgreClub)
            }
            if (addForE5 != null) {
                put(RuleSystem.rule_system_Dnd_E5, addForE5)
            }
        })
    }

    fun <T> addAllAccordingToRuleSystem(
        context: Context,
        l: MutableList<T>,
        data: HashMap<RuleSystem, List<T>>): List<T> {
        addAllAccordingToRuleSystem(
            PrefsHelper(context).activeRuleSystems, l ,data)
        return l.distinct()
    }
    fun <T> addAllAccordingToRuleSystem(
        ruleSystems: List<RuleSystem>,
        l: MutableList<T>,
        data: HashMap<RuleSystem, List<T>>): List<T> {
        ruleSystems.forEach { rS ->
            data[rS]?.let { l.addAll(it) }
        }
        return l.distinct()
    }

    fun getSkillGroup(skill_name: String?): SkillGroup? {
        for (sg in Global.skill_groups) {
            if (arrayContainsElementIgnoringCases(
                    ArrayList(sg.skills), skill_name
                )
            ) {
                log("SKILLS", "get skillgroup result = " + sg.name)
                return sg
            }
        }
        return null
    }

    /**
     * Might return Null if no valueNamePair with fitting name exists for this item
     * @param name
     * @return
     */
    fun getValueNamePairSkill(
        name: String,
        data: ArrayList<ValueNamePairSkillSR>
    ): ValueNamePairSkillSR? {
        return data.find { it.name.equals(name, ignoreCase = true) }
    }

    fun getSplittermondRef(s: String): String {
            return s.lowercase()
                .replace(Regex("\\s"), "_")
                .replace("’", "")
                .replace("'", "")
    }

    fun getSkillGroupName(skill_name: String): String {
        val sg  = getSkillGroup(skill_name)
        return if (sg == null) {
            ""
        } else {
            sg.name
        }
    }

    fun getSortedSkills(
        skill_list: List<String>,
        data: ArrayList<ValueNamePairSkillSR>,
        mCalculator: CharProgressItemAdapterSkillsSR.Calculator
    ): ArrayList<ValueNamePairSkillSR> {
        val result: ArrayList<ValueNamePairSkillSR> = ArrayList()
        for (skill in skill_list) {
            var pair: ValueNamePairSkillSR? =
                getValueNamePairSkill(skill, data)
            if (pair == null) {
                pair = ValueNamePairSkillSR(name = skill, mCalculator = mCalculator)
            }
            result.add(pair)
        }
        Collections.sort(result, ValueNamePairSkillSR.PAIR_INTEGER_STRING__VALUE_COMPARATOR__INVERS)
        for (i in result.indices) {
//            Helper.log("SKILL_GROUP_SKILLS_SORTED", "index = " + i + ", name =" + result.get(i).name + ", value = " + result.get(i).value);
        }
        return result
    }




    fun generateCharProgressItems(
        context: Context,
        logs: List<LogEntry>,
        itemData: List<ItemHeaderAndCreateFunction>,
        cpiMap: CharProgressItemMap,
        activeRuleSystems: List<RuleSystem>,
        /**
         * generate all connections and then create a CPICharacter for each connection
         * connections will be created in addition to the cpi in itemData
         */
        generateConnectionChars: Boolean = false) {
        /*
       use reverse ordering to start with oldest entry in order to get increasing order
        */

        //clear adapters
        for (itDa in itemData) {
            if (true in activeRuleSystems.map { itDa.doUseWithRules[it] }) {
                cpiMap[itDa.filterConstraintId]?.mainItem?.let { item ->
//                    log("TUTUTUTUTUTUTUT", item.filterConstraint + " $activeRuleSystems")
                    //clear data
                    item.adapter?.clear()
                }
            }
        }

        /**
         * check if the filterConstraint matches any of the ones given in itemData
         * If generateConnectionChars == true then connection will be allowed as well
         */
        fun useItemIfConditionsAllow(
            filterConstraint: String,
            task: () -> Unit = { }) {
            var useItem = false

            itemData.find { it.matches(filterConstraint, context) }?.let { iData ->
                useItem = activeRuleSystems.any { iData.doUseWithRules[it] ?: false }
            }
            //set useItem = true if filterConstraint = @connection and generateConnectionChars


            val th = TranslationHelper(context)
            if (generateConnectionChars &&
                th.equalsAnyIgnoreCase(
                    s = filterConstraint,
                    stringResourceId = R.string.filter_constraint_connection
                )
            ) useItem = true

            if (useItem) {
                task()
            }
        }


        // generate new data
        var e: LogEntry
        for (i in logs.indices) {
            e = logs[logs.size - 1 - i]
            val filterConstraints = getFilterConstraints(e.text)
            filterConstraints.forEach { filterConstraint ->
                cpiMap.get(filterConstraint)?.mainItem?.let { item ->
                    useItemIfConditionsAllow(
                        filterConstraint = filterConstraint
                    ) {
                        /*
                        add to data but ignore mismatches like when @Value missing
                        because for those we don't want to have any autocompletition.
                         */
                        item.adapter?.generate_autocomplete_data(e)
                    }

                }
            }
        }
        /*
         create character for each connection
         */
        if (generateConnectionChars) {
            log("CONNECTION_CHARS", "creating cpi character for each connection")

            cpiMap[R.string.filter_constraint_connection]?.mainItem?.adapter?.auto_complete_data?.forEach {
                cpiMap[R.string.filter_constraint_character]?.mainItem?.adapter?.let { characterAdapter ->
                    (it as? CharProgressItemAdapterConnection.Connection?)?.let { conn ->
                        useItemIfConditionsAllow(
                            filterConstraint = context.getString(R.string.filter_constraint_character)
                        ) {

                            characterAdapter.generate_autocomplete_data(
                                LogEntry(
                                    text = "@Character ${conn.name} // ${conn.description} //"
                                ).apply {
                                    conn.relatedLogEntry?.created_at?.let { created_at = it }
                                }
                            )
                        }
                    }

                }

            }
        }
    }

    fun rulesSr5(character: Character?, context: Context): Boolean {
        return character?.ruleSystems?.let {
//            log("CHARACTER_RULE_SYSTEMS", "using CHARACTER rule system: rules = $it")
            RuleSystem.rule_system_sr5 in it
        } ?: PrefsHelper(context).rulesSR5.also {
//            log("CHARACTER_RULE_SYSTEMS", "using SESSION rule system: rule=Sr5: $it \n" +
//                    Log.getStackTraceString(Exception())
//            )
        }
    }
    fun rulesSr6(character: Character?, context: Context?): Boolean {
        return character?.ruleSystems?.let {
            RuleSystem.rule_system_sr6 in it
        } ?: PrefsHelper(context).rulesSR6
    }
    fun rulesSkillfullE5(character: Character?, context: Context?): Boolean {
        return character?.ruleSystems?.let {
            RuleSystem.rule_system_skillfull_dnd_5E in it
        } ?: PrefsHelper(context).rulesSkillfull5E
    }
    fun rulesSplittermond(character: Character?, context: Context?): Boolean {
        return character?.ruleSystems?.let {
            RuleSystem.rule_system_splittermond in it
        } ?: PrefsHelper(context).rulesSplittermond
    }

    fun rulesDnD(character: Character?, context: Context?): Boolean {
        return character?.ruleSystems?.let {
            isDnd(it)
        } ?: PrefsHelper(context).rulesDnD
    }

    fun rulesSR(character: Character?, context: Context?): Boolean {
        return character?.ruleSystems?.let {
            isShadowrun(it)
        } ?: PrefsHelper(context).rulesSR
    }




    class FormatTextResult(val spannableStringBuilder: SpannableStringBuilder, val originalTextWithoutStyleMarkers: String)

    fun formatText(activity: ProgressBarFrameDropBoxActivity,
                   sb: SpannableStringBuilder,
                   originalText: String,
                   bold_matches: ArrayList<String>? = getBoldWords(originalText),
                   emph_matches: ArrayList<String>? = getEmphWords(originalText),
                   italic_matches: List<String>) : FormatTextResult {
        var sb = sb
        var originalText = originalText
        /*
        first make words bold, because after that we want to get rid of all the bold marker *dsa*
         */
        sb = setStyle(bold_matches, originalText, Typeface.BOLD, sb)

        /*
         clear displayed text and original text of all * because they are used as indicators for bold text
         */
        removeAllStrings(sb, "*")
        originalText = originalText.replace(Regex("\\*"), "")

        /*
        now color emphasised words
         */
        sb = setColor(
            emph_matches,
            originalText,
            ContextCompat.getColor(activity, R.color.emph_color),
            sb)

        /*
         clear displayed text and original text of all * because they are used as indicators for bold text
         */
        removeAllStrings(sb, "~")
        originalText = originalText.replace(Regex("~"), "")

        /*
       make tags italic
        */

        sb = setStyle(italic_matches, originalText, Typeface.ITALIC, sb)


        /*
        set Links to Names of Characters and Places which are being auto completed
         */
        val coloredStrings = ArrayList<LogAdapter.ColoredString>()
        coloredStrings.addAll(Global.characterNamesToAutocomplete)
        coloredStrings.addAll(Global.placesNamesToAutocomplete)
        sb = setLinkColors(
            activity = activity,
            cNamesList = coloredStrings,
            originalText = originalText,
            sb = sb)
        return FormatTextResult(spannableStringBuilder = sb, originalTextWithoutStyleMarkers = originalText)
    }

    fun hasNoPreOrSuffix(textLowerCase: String, name: String, start: Int): Boolean {
        val pre: String = if (start == 0) " " else textLowerCase[start-1].toString()
        val suf: String = if (start + name.length == textLowerCase.length) {
            " "
        } else  {
            textLowerCase[start + name.length].toString()
        }
        val result = !pre.matches("[a-z]".toRegex()) && !suf.matches("[a-rt-z]".toRegex())
//        Helper.log("SET_LINK_COLORS", "text: '$textLowerCase', // name: '${name}' pre: '$pre', suf: '$suf', mark: $result")
        return result
    }

    @Synchronized
    fun setLinkColors(activity: ProgressBarFrameDropBoxActivity,
                      cNamesList: ArrayList<LogAdapter.ColoredString>,
                      originalText: String,
                      sb: SpannableStringBuilder): SpannableStringBuilder {

        val cNamesListIterator = cNamesList.iterator()
        val otl = originalText.lowercase(Locale.getDefault())
        while (cNamesListIterator.hasNext()) {
            val names = cNamesListIterator.next()
            names.names.forEach { name ->

                var start = otl.indexOf(name.lowercase(Locale.getDefault()))
                while (start >= 0) {

                    if (hasNoPreOrSuffix(textLowerCase = otl, name = name, start = start)) {
                        val clickableSpan: ClickableSpan = object : ClickableSpan() {
                            override fun onClick(widget: View) {
                                val activity_tag = activity.tag
                                val isCharDetails =
                                    activity_tag.equals("char_details", ignoreCase = true)

                                log("CHARPROGCHAR", "full=true ${names.names.joinToString(separator = " OR ")}")

                                DialogShowFilteredLogs.newInstance(
                                    searchString = "full=true ${names.names.joinToString(separator = " OR ")}",
                                    activity_char_details = isCharDetails
                                ).show(activity.supportFragmentManager, "showTags")
                            }
                        }

                        sb.setSpan(
                            clickableSpan,
                            start,
                            start + name.length,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                        sb.setSpan(
                            ForegroundColorSpan(ContextCompat.getColor(activity, names.color)),
                            start, start + name.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                    }
                    start = originalText.indexOf(name, start + 1)
                }
            }
        }

        return sb
    }

    fun generateImageSpannable(context: Context, lineHeight: Int, icon: Int): SpannableStringBuilder {
        val sb = SpannableStringBuilder()
        //we need to create placeholders for the charicons.
        val iconSpannable: Spannable = SpannableString("i ")
        ContextCompat.getDrawable(context, icon)?.let { d ->
            val span = ImageSpan(d, ImageSpan.ALIGN_BASELINE)
            val factor= 0.8
            d.setBounds(0, 0, (lineHeight *factor).toInt(), (lineHeight *factor).toInt())
            iconSpannable.setSpan(span, 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        }
        sb.append(iconSpannable)
        return sb
    }

    /**
     * append the text to the stringbuilder and set span (what with flags) to text
     * word marked with *sad* will be displayed bold
     */
    fun append(
        sb: SpannableStringBuilder,
        text: CharSequence?,
        what: List<Any?>,
        flags: Int): SpannableStringBuilder {
        val start = sb.length
        sb.append(text)
        for (w in what) {
            sb.setSpan(w, start, sb.length, flags)
        }

        return sb
    }

    fun append(sb: SpannableStringBuilder, text: CharSequence?, what: Any?, flags: Int): SpannableStringBuilder {
        return append(sb, text, listOf(what), flags)
    }

    fun getOrientation(context: Context?): Int{
        return context?.resources?.configuration?.orientation ?: Configuration.ORIENTATION_PORTRAIT
    }

    fun getBoldWords(text: String): ArrayList<String> {
        val matcher = BOLD_PATTERN.matcher(text)
        val bold_matches = ArrayList<String>()
        while (matcher.find()) bold_matches.add(matcher.group(2))
        return bold_matches
    }

    fun getEmphWords(text: String): ArrayList<String> {
        val matcher = EMPH_PATTERN.matcher(text)
        val bold_matches = ArrayList<String>()
        while (matcher.find()) bold_matches.add(matcher.group(2))
        return bold_matches
    }

    fun removeAllStrings(sb: SpannableStringBuilder, s: String): SpannableStringBuilder {
        val length = s.length
        while (true) {
            val index = sb.indexOf(s)
            if (index != -1) {
                sb.delete(index, index + length)
            } else {
                break
            }
        }
        return sb
    }

    fun implementExpand(
        expandButton: ImageView,
        character: Character,
        key: String,
        context: Context,
        onExpandChanged: () -> Unit = {},
    ) {
        implementExpand(
            expandButton = expandButton,
            isExpanded = {
                character.getExtend(
                    key = key,
                    context = context)
            },
            toggleExpand = {
                character.toggleExtend(
                    key = key,
                    context = context
                )
                updateCharacter(
                    character = character,
                    context = context,
                    keep_last_update_date = true
                )

            },
            onExpandChanged = onExpandChanged
        )
    }

    fun implementExpand(
        expandButton: ImageView,
        isExpanded: () -> Boolean,
        /**
         * toggle the variable storing the expand state
         */
        toggleExpand: () -> Unit,
        onExpandChanged: () -> Unit = {},
    ) {
        fun setImage() {
            expandButton.setImageResource(

                    if (isExpanded()) {
                        Log.d("EXPAND", "img expanded")
                        R.drawable.expanded_marker
                    } else {
                        Log.d("EXPAND", "img NOT expanded")

                        R.drawable.expand_marker
                    }
                )

        }

        setImage()

        expandButton.setOnClickListener {
            toggleExpand()
            Log.d("EXPAND", "setting image: getExpand = ${isExpanded.invoke()}")
            setImage()
            onExpandChanged()

        }



    }

    fun checkIgnoreExp(s: String): Boolean {
        val textLowerCase = s.lowercase()
        return textLowerCase.contains(Global.IGNORE_EXP_CHECK_INDICATOR) ||
                textLowerCase.contains(Global.FEATURED_MARKER
                )
    }

    fun checkFeatured(s: String): Boolean {
        val textLowerCase = s.lowercase()
        return textLowerCase.contains(Global.FEATURED_MARKER)
    }

    fun ignoreBracketsAndDigits(s: String): String {
        val m = Global.PATTERN_NAME_NO_DIGITS_REALLY_NO_BRACKETS
            .matcher(s)
        if (m.find()) {
            m.group(1)?.let { return it.trim() }
        }
        return ""
    }

    fun getMyDate(dateAsString: String): Date? {
        var result: Date? = null
        try {
            result = Global.MY_DATE_FORMAT.parse(dateAsString)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return result
    }

    var TAGS_AT_THE_END_COMPARATOR: Comparator<String?> = Comparator{ o1, o2 ->
        val o1StartWithHashTag = o1?.startsWith("#") ?: false
        val o2StartWithHashTag = o2?.startsWith("#") ?: false
        var result = 0
        result = if (!o1StartWithHashTag && !o2StartWithHashTag || o1StartWithHashTag && o2StartWithHashTag) {
            o2?.let {o1?.compareTo(it, ignoreCase = true) ?: -1} ?: 1
        } else if (o1StartWithHashTag) {
            -1
        } else {
            1
        }
        result
    }

    val DATE_IN_STRING_FORMAT_COMPARATOR: Comparator<String> =
        Comparator.nullsFirst(compareBy<String> { getMyDate(it) })


    val BOLD_PATTERN = Pattern.compile("(^| |\\n)(\\*([^*]*)\\*)")
    val EMPH_PATTERN = Pattern.compile("(^| |\\n)(~([^~]*)~)")
}
