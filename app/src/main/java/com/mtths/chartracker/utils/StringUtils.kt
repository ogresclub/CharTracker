package com.mtths.chartracker.utils

import android.content.Context
import com.mtths.chartracker.adapters.LogAdapter
import java.util.regex.Pattern

object StringUtils {

    /**
     * parse a char progress items stat which is given as a text
     * (especially potentially including commata) which must be surrounded by []
     * for example description
     * Do not forget to escape the prefix and postfix chars if necessary
     */
    fun parseCPITextStat(
        text: String,
        refId: Int,
        context: Context
    ): String {
        return getCPIData(
            dataNameId = refId,
            text = text,
            prefix = "\\[",
            postfix = "\\]",
            context = context,
            allowComma = true
        ) ?: ""
    }

    fun decorateWithTranslation(
        context: Context,
        f: (String, String) -> String
    ): (Int, String) -> String = { refId, text ->
        val ref = TranslationHelper(context).getAllTranslationsRegex(refId)
        f(ref, text)
    }


    fun getStatUntilEndByCommaFromText(stat: String, text: String): String {
        val m = Pattern.compile("$stat\\s*=\\s?([^,=]+)", Pattern.CASE_INSENSITIVE).matcher(text)
        var result = "-"
        if (m.find()) {
            m.group(1)?.trim()?.let { result = it }
        }
        return result
    }

    fun getStatUntilEndByCommaFromText(statId: Int, text: String, context: Context): String {
        return getStatUntilEndByCommaFromText(
            stat = TranslationHelper(context).getAllTranslationsRegex(statId),
            text = text)
    }

    fun getIntegerStatFromText(stat: String, text: String): String {
        val m =
            Pattern.compile("$stat\\s*=?\\s*(-?\\s*\\d+)", Pattern.CASE_INSENSITIVE).matcher(text)
        var result = "-"
        if (m.find()) {
            m.group(1)?.trim()?.let {  result = it }
        }
        return result
    }
    fun getIntegerStatFromText(statId: Int, text: String, context: Context): String {
        return getIntegerStatFromText(
            stat = TranslationHelper(context).getAllTranslationsRegex(statId),
            text = text
        )
    }

    fun getStatFromText(stat: String, text: String): String? {
        val m =
            Pattern.compile("$stat\\s*=?\\s*([^,\\s]+)", Pattern.CASE_INSENSITIVE).matcher(text)
        return if (m.find()) {
            m.group(1)?.trim()
        } else null
    }

    fun getStatFromText(statId: Int, text: String, context: Context): String? {
        return getStatFromText(
            stat = TranslationHelper(context).getAllTranslationsRegex(statId),
            text = text,
        )
    }

    fun getFloatOrIntegerStatFromText(stat: String, text: String): String {
        val m = Pattern.compile("$stat\\s*=?\\s*(-?\\+?\\d+([,.]\\d+)*)", Pattern.CASE_INSENSITIVE)
            .matcher(text)
        var result = "-"
        if (m.find()) {
            m.group(1)?.trim()?.let { result = it }
        }
        return result
    }

    fun getIntegerStatWithModificationFromText(stat: String, text: String): String {
        val m = Pattern.compile(
            "$stat\\s*=?\\s*(-?\\s*\\d+\\s*(\\(\\d+\\))?)",
            Pattern.CASE_INSENSITIVE
        ).matcher(text)
        var result = "-"
        if (m.find()) {
            m.group(1)?.trim()?.let { result = it }
        }
        return result
    }

    fun getIntegerStatWithModificationFromText(context: Context): (Int, String) -> String =
        decorateWithTranslation(context, ::getIntegerStatWithModificationFromText)


    fun getWareType(text: String): String {
        val m = Pattern.compile("type\\s*=\\s?(bio|cyber)", Pattern.CASE_INSENSITIVE).matcher(text)
        var result = "-"
        if (m.find()) {
            m.group(1)?.trim()?.let { result = it }
        }
        return result
    }

    fun getAmmoFromText(stat: String, text: String): String {
        val m =
            Pattern.compile("$stat\\s*=?\\s*(-?\\d+(\\s*\\([a-z]\\))?)", Pattern.CASE_INSENSITIVE)
                .matcher(text)
        var result = "-"
        if (m.find()) {
            m.group(1)?.trim()?.let { result = it }
        }

        return result
    }

    fun getAmmoFromText(context: Context): (Int, String) -> String =
        decorateWithTranslation(context, ::getAmmoFromText)

    fun getWeaponDamageFromText(stat: String, text: String): String {
        val m =
            Pattern.compile("$stat\\s*=?\\s*(-?\\d+[A-Z]?)", Pattern.CASE_INSENSITIVE).matcher(text)
        var result = "-"
        if (m.find()) {
            m.group(1)?.trim()?.let { result = it }
        }
        return result
    }

    fun getWeaponDamageFromText(context: Context): (Int, String) -> String =
        decorateWithTranslation(context, ::getWeaponDamageFromText)



    /**
     * Do not forget to escape the begin and end chars if necessary
     */
    fun getCPIData(
        dataName: String,
        text: String,
        prefix: String = "",
        postfix: String = "",
        allowComma: Boolean = false
    )
    : String? {
        val mPrefix = prefix
        val mPostfix = postfix
        val mDataName = dataName

        val allowedContent = if (allowComma) "[\\S\\s]" else "[^,]"
        // . matches every character, except line break, thats why I use [\S\s]

        val m =
            Pattern.compile(
                "$mDataName\\s*=\\s*$mPrefix($allowedContent*)$mPostfix",
                Pattern.CASE_INSENSITIVE).matcher(text)
        var result: String? = null
        if (m.find()) {
            m.group(1)?.trim()?.let {
                /*
                when there are more closing bracket characters after the end of the list
                the greedy regex return too much,
                but then we take only text until the correct closing bracket
                 */
                result = getSurroundedContent("[$it]")
            }
        }

        return result
    }

    fun getCPIData(
        dataNameId: Int,
        text: String,
        prefix: String = "",
        postfix: String = "",
        context: Context,
        /**
         * don't really know why I would use it, now that I think about it...
         */
        allowComma: Boolean = false
    )
            : String? {
        return getCPIData(
            dataName = TranslationHelper(context).getAllTranslationsRegex(dataNameId),
            text = text,
            prefix = prefix,
            postfix = postfix,
            allowComma = allowComma
        )
    }


        fun getValue(s: String, begin: Regex = Regex(""), end: Regex = Regex("")): Int? {
        val m = Pattern.compile("(\\s+?|^)$begin=?\\+?(-?\\d+)$end(\\s+?|$)").matcher(s)
        if (m.find()) {
            return m.group(2)?.toIntOrNull()
        }
        return null
    }

    fun getIntegerStatWithExtraCharsFromText(stat: String, text: String, extra_chars: String = ""): String {
        val m =
            Pattern.compile("$stat\\s*=?\\s*([\\d$extra_chars]*)", Pattern.CASE_INSENSITIVE).matcher(text)
        var result = "-"
        if (m.find()) {
            m.group(1)?.trim()?.let { result = it }
        }
        return result
    }

    fun getIntegerStatWithExtraCharsFromText(
        statId: Int,
        text: String,
        extra_chars: String = "",
        context: Context): String {
        return getIntegerStatWithExtraCharsFromText(
            stat = TranslationHelper(context).getAllTranslationsRegex(statId),
            text = text,
            extra_chars = extra_chars
        )
    }

    /**
     * Do not forget to escape the begin and end chars if necessary
     */
    fun getListFromText(
        stat: String,
        text: String,
        begin: String = "\\[",
        end: String = "\\]",
        sep: Char = ','
    ): List<String> {
        return getNamedListFromText(
            stat = stat,
            text = text,
            begin = begin,
            end = end,
            sep = sep,
        )
            .list
    }

    /**
     * Do not forget to escape the begin and end chars if necessary
     */
    fun getListFromText(
        statId: Int,
        text: String,
        begin: String = "\\[",
        end: String = "\\]",
        sep: Char = ',',
        context: Context,
        allowBeginAndEndInList: Boolean = false
    ): List<String> {
        return getListFromText(
            stat = TranslationHelper(context).getAllTranslationsRegex(statId),
            text = text,
            begin = begin,
            end = end,
            sep = sep
        )
    }

    fun getSurroundedContent(s: String, open: Char = '[', close: Char = ']'): String {
        val start = s.indexOf(open)
        if (start > -1) {
            findClosingBracketIndex(
                text = s,
                openIndex = start,
                open = open,
                close = close
            )?.let { end ->
                return s.substring(start + 1, end)
            }
        }
        return ""
    }

    fun findClosingBracketIndex(text: String, openIndex: Int, open: Char = '[', close: Char = ']'): Int? {
        var depth = 0
        for (i in openIndex until text.length) {
            when (text[i]) {
                open -> depth++
                close -> {
                    depth--
                    if (depth == 0) return i
                }
            }
        }
        return null // No matching closing bracket found
    }

    /**
     * closing bracket length has to be the same as opening bracket length!
     */
    fun findClosingBracketIndex(text: String, openIndex: Int, open: String = "\\[", close: String = "\\]"): Int? {
        var depth = 0
//        Log.d("CHECK_W_BRACK", "find closing brackets: open = '$open', close = '$close', length = ${text.length}")
        val length = close.length
        for (i in openIndex until text.length + 1 - length) {
//            Log.d("CHECK_W_BRACK", "index = $i, bracket? = '${text.substring(i,i+length)}'")
            when (text.substring(i,i+length)) {
                open -> depth++
                close -> {
                    depth--
                    if (depth == 0) return i.also {
//                        Log.d("CHECK_W_BRACK", "FOUND CLOSING BRACKET AT index = $i")

                    }
                }
            }
        }
        return null // No matching closing bracket found
    }

    /**
     * check a string against given query with a given function respecting brackets (in the query)
     * e.g. check(mString,"a AND (b OR c)") = check(mString, "a") && check(mString, "b OR c")
     * tough brackets have to be escaped as \( \)
     */
    fun checkRespectingBrackets(
        check: (s:String, query: String) -> Boolean,
        s: String,
        q: String,
        open: String = LogAdapter.OPEN_LOGIC_BRACKET,
        close: String = LogAdapter.CLOSE_LOGIC_BRACKET,
        or: String = LogAdapter.OR_SEPARATOR
    ): Boolean {

        val oidx = q.indexOf(open)
        if (oidx == -1) {
            return check(s, q).also {
//                Log.d("CHECK_W_BRACK", "no brackets: q = '$q', check = $it")
            }
        } else {
            // we have a opening bracket
            val pre = if (oidx > 0) q.substring(0, oidx) else ""
            var mid = q.substring(oidx + open.length)
//            Log.d("CHECK_W_BRACK", "pre = '$pre', rest = '$mid'")
            val preOr = pre.trim().endsWith(or)
            /*
            preOr is false, if pre is empty and thus we will not end up getting true from the or
            in that case
             */
            var result = (pre.isEmpty() || checkRespectingBrackets(check, s, pre))





            val eidx = findClosingBracketIndex(
                text = mid,
                openIndex = oidx,
                open = open,
                close = close
            )

            if (eidx == null) {
                //bracket did not close
//                Log.w("CHECK_W_BRACK", "rest '$mid' did not close bracket")
                val midBool = checkRespectingBrackets(check, s, mid)
                return if (preOr) result || midBool else result && midBool
            } else {
                // we also have closing brackets
                val post = q.substring(eidx + close.length)
//                Log.d("CHECK_W_BRACK", "found closing brackets: mid (before cutting end) = '$mid'")
                mid = mid.substring(0, eidx - pre.length)
//                Log.d("CHECK_W_BRACK", "found closing brackets: pre = '$pre', mid = '$mid', post = '$post'")
                val postOr = post.trim().startsWith(or)

                val midBool = checkRespectingBrackets(check, s, mid)
                result = if (preOr) result || midBool else result && midBool
                val postBool = checkRespectingBrackets(check, s, post)
                result = if (postOr) result || postBool else result && postBool
                return result.also {
//                    Log.d("CHECK_W_BRACK","check brack for '$s' with q='$q': result = $it")
                }
            }

        }
    }

    /**
     * Do not forget to escape the begin and end chars if necessary
     * list has to be of form name[item1, item2, item3]
     */
    private fun getNamedListFromText(
        stat: String,
        text: String,
        begin: String = "\\[",
        end: String = "\\]",
        sep: Char = ',',
    ): NamedList<String> {
        val mBegin = begin
        val mEnd = end
        val mStat = stat
        val content = "[\\s\\S]"
        val m = Pattern.compile(
            "$mStat\\s*=?\\s*(\\S*)\\s*$mBegin($content*)$mEnd",
            Pattern.CASE_INSENSITIVE
        ).matcher(text)
        var result = ""
        var name = ""
        if (m.find()) {
            m.group(2)?.trim()?.let {
                result = getSurroundedContent("[$it]")
            }
            m.group(1)?.let { name = it }
        }

        return if (result.isEmpty()) {
            return NamedList(emptyList(), name)
        } else {
            NamedList(result.split(sep).map { it.trim() }, name)
        }
    }

    /**
     * Do not forget to escape the begin and end chars if necessary
     * list has to be of form name[item1, item2, item3]
     */
    fun getNamedListFromText(
        statId: Int,
        text: String,
        begin: String = "\\[",
        end: String = "\\]",
        sep: Char = ',',
        context: Context
    ): NamedList<String> {
        return getNamedListFromText(
            stat = TranslationHelper(context).getAllTranslationsRegex(statId),
            text = text,
            begin = begin,
            end = end,
            sep = sep
        )
    }
    data class NamedList<T>(val list: List<T>, val name: T)
}