package com.mtths.chartracker.utils

import android.content.Context
import android.util.Log
import com.mtths.chartracker.dataclasses.CharProgMetaData
import com.mtths.chartracker.dataclasses.CharProgressItemData

/**
 * use filter constraint as key e.g. (@Feat)
 * this is needed to access the same cpi for a filter constraint in any given language
 * when don't want to use the string resource ids as key, so we take the strings,
 * but they depend on the locale
 */
class CharProgressItemMap: LinkedHashMap<Int, CharProgMetaData>() {

    /**
     *  mapping all filter constraints in all different languages to the corresponding cpi
     */
    var stringData: HashMap<List<String>, CharProgMetaData> = HashMap()

    fun updateStringData(context: Context) {
        stringData = mapKeysTo(HashMap()) {
            TranslationHelper(context).getAllTranslations(it.key).map {
                it.lowercase()
            }
        }
    }

    fun get(filterConstraint: String): CharProgMetaData? {
        return stringData.filterKeys { it.contains(filterConstraint.lowercase()) }.values.firstOrNull()
    }

    fun get(filterConstraintId: Int, context: Context): CharProgMetaData? {
        return get(context.getString(filterConstraintId))
    }

    fun clearViewHolders() {
        stringData.values.forEach { meta ->
            meta.mainItem.adapter?.data?.forEach {
                if (it is CharProgressItemData) {
                    it.viewHolder = CharProgressItemData.ViewHolder()
                }
            }
        }
    }
}