package com.mtths.chartracker.utils

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.R

object SnackBarUtils {

    fun showSnackBar(
        view: View,
        msg: String,
        duration: Int,
        context: Context? = null
    ) {
        Snackbar.make(
            view,
            msg,
            duration
        ).apply {
            this.view.findViewById<TextView>(R.id.snackbar_text).apply {
                maxLines = 100
                context?.let { setTextColor(ContextCompat.getColor(it,android.R.color.holo_green_light) )}
            }
        }.show()
    }
}