package com.mtths.chartracker.utils

import android.content.Context
import android.content.Intent
import android.content.UriPermission
import android.net.Uri
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.core.net.toUri
import androidx.documentfile.provider.DocumentFile
import com.mtths.chartracker.Global
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dialogs.DialogDataOptions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

/**
 * Utilities for files
 */
object FileUtils {

    /**
     * remove the part before : from last path segment
     */
    fun getLastPathSegmentNoType(uri: Uri): String? {
        return uri.lastPathSegment?.substringAfter(":")
    }
    /**
     * remove the part before last / from last path segment
     */
    fun getName(uri: Uri): String? {
        return uri.lastPathSegment?.substringAfterLast("/")
    }

    /**
     * @param mimeType is only needed if new object has to be created
     */
    fun findOrCreateFile(documentFile: DocumentFile, displayName: String, mimeType: String): DocumentFile? {
        return documentFile.findFile(displayName) ?: documentFile.createFile(mimeType, displayName)
    }

    fun findOrCreateDir(documentFile: DocumentFile, displayName: String): DocumentFile? {
        return documentFile.findFile(displayName) ?: documentFile.createDirectory(displayName)
    }

    fun getCharTrackerFolder(uriP: UriPermission, context: Context): DocumentFile? {
            return uriP.uri?.let { uri ->
                DocumentFile.fromTreeUri(context,uri)?.let { docFile ->
                    findOrCreateDir(docFile, DialogDataOptions.EXPORT_DIRECTORY_FOLDER_NAME)}
            }
    }

    fun copyFile(pathFrom: String, pathTo: String): Boolean {
        val target = File(pathTo)
        val origin = File(pathFrom)
        if (origin.exists()) {
            Helper.log("Found file to copy. Copying...")
            try {
                Helper.copyFile(FileInputStream(origin), FileOutputStream(target))
            } catch (e: IOException) {
                e.printStackTrace()
                return false
            }
            Helper.log("Done copying")
            return true
        }
        Helper.log("File to copy not Found.")
        return false
    }

    fun copyUris(originUri: Uri, targetUri: Uri, context: Context): Boolean {
        val DEBUG_TAG = "COPY_URIS"
        val logging = true

        Helper.log(DEBUG_TAG, "Found file to copy. Copying...", logging)
        try {
            context.contentResolver?.let { contentResolver ->
                contentResolver.openOutputStream(targetUri)
                    ?.let {ops ->
                        contentResolver.openInputStream(originUri)?.apply {
                            copyTo(ops)
                            close()
                        }
                        ops.close()
                    }
            }

        } catch (e: Exception) {
            Helper.log(DEBUG_TAG, "error copying $originUri to $targetUri; ${Log.getStackTraceString(e)}", logging)

            return false
        }
        Helper.log(DEBUG_TAG, "Done copying", logging)
        return true
    }

    fun createFileAndAllParentDirs(f: File) {
        if (!f.exists()) {
            f.parentFile?.mkdirs()
            f.createNewFile()
        }
    }

    fun getType(contentUri: Uri, context: Context): String? {
        return MimeTypeMap.getSingleton().getExtensionFromMimeType(
            context.contentResolver.getType(contentUri)
        )
    }
    suspend fun importIconsFromLocalStorage(
        charTrackerFolder: DocumentFile?,
        context: Context,
        charIconFileNamesToImport: List<String>) {
        withContext(Dispatchers.IO) {
            charTrackerFolder?.findFile(Global.ICONS_FOLDER)?.let { iconFolder ->
                charIconFileNamesToImport.forEach { iconName ->
                    val appIconPath = Character.getCompleteIconPath(context, iconName)
                    val target = File(appIconPath).apply {
                        createFileAndAllParentDirs(this)
                    }
                    Log.d("IMPORT_XML_LOCAL", "copying $iconName : appiconpath = $appIconPath")
                    iconFolder.findFile(iconName)?.uri?.let { iconUri ->
                        Log.d("IMPORT_XML_LOCAL", "copying $iconName : iconuri = $iconUri")

                        copyUris(
                            targetUri = target.toUri(),
                            originUri = iconUri,
                            context = context
                        )
                    } ?: Log.d("IMPORT_XML_LOCAL", "copying $iconName : no icon found to copy")
                }
            } ?: run {
                Log.d("IMPORT_XML_LOCAL", "no icons folder found.")
            }
        }

    }

    fun getCharTrackerFolder(folderUri: Uri, context: Context): DocumentFile? {
        val dF = DocumentFile.fromTreeUri(
            context,
            folderUri
        )
        if (getName(folderUri) == DialogDataOptions.EXPORT_DIRECTORY_FOLDER_NAME) {
            return dF
        } else {
            return dF?.let {
                findOrCreateDir(
                    documentFile = it,
                    displayName = DialogDataOptions.EXPORT_DIRECTORY_FOLDER_NAME
                )
            }
        }
    }

    fun takePersistentUriPermission(uri: Uri, applicationContext: Context) {
        if (getName(uri) != Global.ICONS_FOLDER) {
            Log.d("PERSIST_PERMISSION", "taking persistent Permission for folder $uri")
            val contentResolver = applicationContext.contentResolver
            val flags: Int = Intent.FLAG_GRANT_READ_URI_PERMISSION or
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            // Revoke permissions for all URIs
            contentResolver.persistedUriPermissions.forEach {
                applicationContext.revokeUriPermission(it.uri, flags)
            }
            contentResolver.takePersistableUriPermission(uri, flags)
        } else {
            Log.d("PERSIST_PERMISSION", "NOT taking persistent Permission for folder, " +
                    "because it is only icon folder: $uri")
        }
    }
}