package com.mtths.chartracker.utils

import android.content.Context
import android.util.Log
import com.mtths.chartracker.R
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterValueBased
import com.mtths.chartracker.dataclasses.BuffEffect
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.RuleSystem.Companion.sr6SkillIsMatrixDamageMalusAffected
import com.mtths.chartracker.dataclasses.ValueNamePair
import kotlin.math.abs

open class SkillDetailsBuilder(
    val c: Character,
    val valueNamePair: ValueNamePair,
    val context: Context?,
) {

    open val value: Int
        get() = c.getStatValue(valueNamePair.name) ?: 0

    open val buff_value: Int?
        get() = c.getStatBuffValue(valueNamePair.name)

    open val all_checks_value: Int?
        get() = c.getStatBuffValueOrZero(BuffEffect.BUFF_REF_ALL_CHECKS)
    open val replacement: Int?
        get() = c.getStatReplacementValue(valueNamePair.name)

    open val total: Int
        get() = CharProgressItemAdapterValueBased.calcTotalSkillValue(
        vnp = valueNamePair,
        character = c,
        context = context
    ).totalValue

    open val malus: Int
        get() = c.damageMalus

    open val sizeStealthV: Int
        get() = CharProgressItemAdapterValueBased.getStealthMod(
        valueNamePair = valueNamePair,
        context = context,
        character = c
    )

    open val armorPenatlyV: Int
        get() = CharProgressItemAdapterValueBased.getAmorPenalty(
        vnp = valueNamePair,
        context = context,
        character = c
    )

    open val attrs: String
        get() = valueNamePair.skill?.attributesList
        ?.joinToString(separator = " + ") { attr ->
            val attrValue = CharProgressItemAdapterValueBased.getRelatedAttributesBuffedValueSum(
                attributes = listOf(attr), character = c
            ) ?: 0
            val attrChecksBuff = CharProgressItemAdapterValueBased.getRelatedAttributesCheckBuffSum(
                attributes = listOf(attr), character = c
            )
            var s = "%d (%s)".format(
                attrValue,
                attr
            )
            attrChecksBuff?.let { attrChecksVal ->
                if (attrChecksVal != 0) {
                    s += " + %d (%s)".format(
                        attrChecksVal,
                        "$attr ${context?.getString(R.string.label_checks)?.lowercase()}"
                    )
                }
            }
            s
        } ?: "No related Attribute Found"


    open val startFormatValue: String
        get() = "%s: %s = ".format(
        valueNamePair.name,
        "%d"
    )

    open val ranks: String
        get() = "%d (%s)".format(
            value,
            context?.getString(R.string.label_ranks)?.lowercase()
        )


    val buff: String
        get() = "%d (${context?.getString(R.string.label_buffs)?.
        dropLast(1)?.
        lowercase()})".
        format(buff_value)

    val all_checks: String
        get() = all_checks_value?.let { value ->
            if (value != 0) {
                val sign = if (value > 0) "+" else "-"
                " $sign %d (%s)".format(
                    abs(value),
                    context?.getString(R.string.all_checks)
                )
            } else ""
        } ?: ""

    open val wounds: String
        get() = if (malus > 0) {
        " - %d (%s)".format(
            malus,
            c.ruleSystem?.woundsNameId?.let { context?.getString(it)?.lowercase() }
        )
    } else {
        ""
    }

    open val sizeStealth: String
        get() = if (sizeStealthV != 0) {
        " %s%d (%s)".format(
            Helper.sign(sizeStealthV),
            sizeStealthV,
            context?.resources?.getString(R.string.size)
        )
    } else ""

    open val armorPenalty: String
        get() = if (armorPenatlyV > 0) {
        " - %d (%s)".format(
            armorPenatlyV,
            context?.resources?.getString(R.string.label_armor)
        )
    } else ""

    open val repl: String
        get() = "%d (${context?.getString(R.string.replace_label)?.
        lowercase()})".
        format(replacement)

    open val rksNbuff: String
        get() = buff_value?.let { "%s + %s".format(ranks, buff) } ?: ranks

    open val calculation: String
        get() = replacement?.let { repl } ?: rksNbuff

    open val explanationIfRepl: String
        get() = replacement?.let {
            ";\t${context?.getString(R.string.label_replaced)?.lowercase()} [%s]"
                .format(rksNbuff)
        } ?: ""

    open val totalCalculation: String
        get() = calculation + " + %s".format(attrs) + wounds + all_checks +
                sizeStealth + armorPenalty + explanationIfRepl
    open val msg: String
        get() = startFormatValue.format(total) + totalCalculation

    open val msgFormatTotal: String
        get() = startFormatValue + totalCalculation





}

open class PathfinderSkillDetailsBuilder(
    c: Character, vnp: ValueNamePair, ctx: Context?
): SkillDetailsBuilder(
    c = c, valueNamePair = vnp, context = ctx
) {
    override val ranks: String
    get() {
        var ranks = "%d (%s)".format(
            value,
            context?.getString(R.string.label_ranks)?.lowercase())
        if (value > 0 ) {
            var bonus = 1
            if (c.isClassSkill(valueNamePair.name.lowercase())) {
                bonus = 3
            }
            ranks += " + $bonus (${context?.getString(R.string.class_label)?.lowercase()})"
        }
        return ranks
    }
}

open class SR5SkillDetailsBuilder(
    c: Character, vnp: ValueNamePair, ctx: Context?
): SkillDetailsBuilder(
    c = c, valueNamePair = vnp, context = ctx
) {

    val concentration = if (c.concentrationMalus > 0) {
        " - %d (%s)".format(
            c.concentrationMalus,
            context?.getString(R.string.label_concentration)?.lowercase()
        )
    } else {
        ""
    }

    override val ranks: String
        get() {
            var ranks = "%d (%s)".format(
                value,
                context?.getString(R.string.label_ranks)?.lowercase())
            if (value == 0 ) {
                ranks += " - 1 (${context?.getString(R.string.label_unproficient)})"
            }
            return ranks
        }

    override val wounds: String
        get() = super.wounds +  concentration

}

class SR6SkillDetailsBuilder(
    c: Character, vnp: ValueNamePair, ctx: Context?
): SR5SkillDetailsBuilder(
    c = c, vnp = vnp, ctx = ctx
) {
    val matrixDamageMalus: String
        get() = context?.let { ctx ->
            Log.d("SKILL_DETAILS_BUILDER", "getting matrix damage malus for ${valueNamePair.name}")
            if (sr6SkillIsMatrixDamageMalusAffected(ctx, valueNamePair) && c.matrixDamageMalus > 0) {
                " - %d (%s)".format(
                    c.matrixDamageMalus,
                    ctx.getString(R.string.label_damage_monitor_matrix).lowercase()
                )
            } else ""
        } ?: ""

    override val wounds: String
        get() = super.wounds + matrixDamageMalus
}