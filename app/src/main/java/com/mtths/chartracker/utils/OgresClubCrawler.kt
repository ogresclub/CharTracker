package com.mtths.chartracker.utils

import android.content.Context
import android.os.AsyncTask
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.Global
import com.mtths.chartracker.R
import com.mtths.chartracker.utils.Helper.arrayContainsElementIgnoringCases
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.dataclasses.*
import org.jsoup.Jsoup
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap

class OgresClubCrawler (val context: Context,
                        val progressBarFrame: FrameLayout,
                        val progressBar: ProgressBar,
                        val progressBarTextView: TextView) {
    // todo use coroutines

    interface OnFinishedCrawlingOgresClubListener {
        fun onComplete()
    }

    var numOfTasks = 0
    var dbHelper: DatabaseHelper? = null
    var mOnAllTasksCompleteListener: OnFinishedCrawlingOgresClubListener? = null
    private var mLoadFeatsAndStoreTask: LoadFeatsAndStoreTask
    private var mLoadRacesAndSubracesAndStoreTask: LoadRacesAndSubracesAndStoreTask
    private var mLoadClassesAndArchetypesAndStoreTask: LoadClassesAndArchetypesAndStoreTask
    fun execute() {
        mLoadFeatsAndStoreTask.cancel(true)
        mLoadFeatsAndStoreTask = LoadFeatsAndStoreTask()
        mLoadFeatsAndStoreTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        mLoadRacesAndSubracesAndStoreTask.cancel(true)
        mLoadRacesAndSubracesAndStoreTask = LoadRacesAndSubracesAndStoreTask()
        mLoadRacesAndSubracesAndStoreTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        mLoadClassesAndArchetypesAndStoreTask.cancel(true)
        mLoadClassesAndArchetypesAndStoreTask = LoadClassesAndArchetypesAndStoreTask()
        mLoadClassesAndArchetypesAndStoreTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    fun setOnFinishedCrawlingOgresClubListener(mOnFinishedCrawlingOgresClubListener: OnFinishedCrawlingOgresClubListener?) {
        mOnAllTasksCompleteListener = mOnFinishedCrawlingOgresClubListener
    }

    fun addTask() {
        numOfTasks++
        log(DEBUG_TAG, "started Task, count = $numOfTasks")
    }

    fun removeTask() {
        numOfTasks--
        log(DEBUG_TAG, "completed task, count = $numOfTasks")
    }

    private fun postExecuteTasks() {
        removeTask()
        if (numOfTasks == 0) {
            log(DEBUG_TAG, "postExecuteTasks:: progressbar: ${progressBar.progress}/${progressBar.max}")
            progressBarFrame.visibility = View.GONE
            progressBar.max = 0
            var msg = "crawled\n" + Global.featsOnOgresClub.size + " feats"
            msg += "\n" + Global.classesFromOgresClub.size + " classes"
            msg += "\n" + Global.archetypesFromOgresClub.size + " archetypes"
            msg += "\n" + Global.racesFromOgresClub.size + " races"
            msg += "\n" + Global.subracesFromOgresClub.size + " subraces"
            msg += "\nfrom ogres.club"
            SnackBarUtils.showSnackBar(
                view = progressBarTextView,
                msg = msg,
                duration = Snackbar.LENGTH_LONG,
                context = context
            )
            if (mOnAllTasksCompleteListener != null) {
                mOnAllTasksCompleteListener!!.onComplete()
            }
        }
    }

    private fun preExecuteTasks() {
        if (numOfTasks == 0) {
            progressBarFrame.visibility = View.VISIBLE
            progressBar.progress = 0
            progressBar.max = 0
            progressBarTextView.setText(R.string.crawling_ogres_club)
        }
        addTask()
    }
    ////////////////////////////// ASYNC TASKS TO CRAWL /////////////////////////////////
    /**
     * Load data (feats and classnames as well as their base skills/level)
     */
    internal inner class LoadRacesAndSubracesAndStoreTask : AsyncTask<Void, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            preExecuteTasks()
            dbHelper?.resetRacesAndSubRaces()
            Global.racesFromOgresClub = ArrayList()
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void): Void? { // it's important to read the subraces before creating the races,
// because during race creation related subrace will be stored as well and
//during subrace creation the relations will not be established
            readRacesFromOgresClub(progressBar)
            readSubRacesFromOgresClub(progressBar)
            val max = Global.racesFromOgresClub.size + Global.subracesFromOgresClub.size
            progressBar.max += max
            for (race in Global.racesFromOgresClub) {
                race.id = dbHelper!!.createRace(race)
                progressBar.progress += 1
            }
            for (subrace in Global.subracesFromOgresClub) {
                subrace.id = dbHelper!!.createSubrace(subrace)
                progressBar.progress += 1
            }
            progressBar.max -= max
            log(DEBUG_TAG, "done with races")
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            postExecuteTasks()
        }
    }

    /**
     * Load data (feats and classnames as well as their base skills/level)
     */
    internal inner class LoadFeatsAndStoreTask : AsyncTask<Void, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            preExecuteTasks()
            dbHelper?.deleteAllFeats()
            log(DEBUG_TAG, "deleted all feats")
            Global.featsOnOgresClub = ArrayList()
        }

        @Deprecated("Deprecated in Java")
        protected override fun doInBackground(vararg voids: Void): Void? {
            log(DEBUG_TAG, "reading feats from ogres club")
            readFeatsFromOgresClub(progressBar)
            val max = Global.featsOnOgresClub.size
            progressBar.max += max
            for (featLink in Global.featsOnOgresClub) {
                dbHelper!!.createFeat(featLink!!)
                progressBar.progress += 1
            }
            progressBar.max -= max
            log(DEBUG_TAG, "done with feats")
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            postExecuteTasks()
        }
    }

    /**
     * Load data (feats and classnames as well as their base skills/level)
     */
    internal inner class LoadClassesAndArchetypesAndStoreTask : AsyncTask<Void, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            preExecuteTasks()
        }

        @Deprecated("Deprecated in Java")
        protected override fun doInBackground(vararg voids: Void): Void? {
            dbHelper?.resetClassesAndArchetypes()
            Global.classesFromOgresClub = ArrayList()
            Global.archetypesFromOgresClub = ArrayList()
            readClassesFromOgresClub(progressBar)
            readArchetypesFromOgresClub(progressBar)
            val max = Global.classesFromOgresClub.size + Global.archetypesFromOgresClub.size
            progressBar.max += max
            for (cl in Global.classesFromOgresClub) {
                cl.id = dbHelper!!.createClass(cl)
                progressBar.progress += 1
            }
            for (archetype in Global.archetypesFromOgresClub) {
                archetype.id = dbHelper!!.createArchetype(archetype)
                progressBar.progress += 1

            }
            progressBar.max -= max
            log(DEBUG_TAG, "done with classes")
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            postExecuteTasks()
        }
    }

    companion object {
        const val DEBUG_TAG = "OGRES_CLUB_CRAWLER"
        /////////////////////////// STATIC METHODS FOR CRAWLING /////////////////////////////
        private fun normalize_for_db(s: String): String {
            return s.replace("'", "`")
        }

        fun readFeatsFromOgresClub(progressBar: ProgressBar) {
            val DEBUG_TAG_LOADING_FEATS_OGRES_CLUB = "LOADING_FEATS_OGRES_CLUB"
            var max = 0
            try {
                val doc = Jsoup.connect("https://ogres.club/feats")
                        .get()
                log(DEBUG_TAG_LOADING_FEATS_OGRES_CLUB, "successfully connected to https://ogres.club/feats")
                val feat_links = doc.select("td:eq(0) > a[href]")
                if (feat_links.size == 0) {
                //                log(DEBUG_TAG_LOADING_FEATS_OGRES_CLUB, "no feats selected");
                }
                max = feat_links.size
                progressBar.max += max
                for (feat_link in feat_links) {
                    var alreadyContained = false
                    val feat_link_id = FeatLink.normalize(feat_link.text())
                    for (i in Global.featsOnOgresClub.indices) {
                        val existing_feat_link = Global.featsOnOgresClub[i]
                        if (existing_feat_link.id_string.equals(feat_link_id, ignoreCase = true)) {
                            alreadyContained = true
                            val new_link = feat_link.attr("abs:href")
                            /*
                            make sure the same link doesN#t get read twice because the feat appear often
                            on ogres.club
                             */
                            if (!arrayContainsElementIgnoringCases(existing_feat_link.links, new_link)) {
                                existing_feat_link.addLink(new_link)
                            }
                        }
                    }
                    if (!alreadyContained || feat_link.text().replace("\\s".toRegex(), "").length == 0) {
                        val new_feat_link = FeatLink(feat_link.text(), feat_link.attr("abs:href"))
                        Global.featsOnOgresClub.add(new_feat_link)
                        //System.out.println(feat_link.text());
                    }
                    progressBar.progress += 1
                }
            } catch (e: IOException) {
                e.printStackTrace()
                //            log(DEBUG_TAG_LOADING_FEATS_OGRES_CLUB, e.getMessage());
            }
            progressBar.max -= max
            progressBar.progress -= max
        }

        fun readClassesFromOgresClub(progressBar: ProgressBar) {

            fun getProgressionBonus(s: String): Int {
                return when (s.lowercase()) {
                    "fair", "average" -> -1
                    "low", "poor" -> -3
                    "good", "high" -> +1
                    else -> -1
                }
            }

            /**
             * replace some skills with the name used in the app
             */
             val replaceSkillsDict = HashMap<String, String>().apply {
                 put("investigation", "investigate")
            }

            fun replaceSkill(s: String): String {
                return replaceSkillsDict[s.lowercase()] ?: s
            }



            val DEBUG_TAG_LOADING_CLASSES_OGRES_CLUB = DEBUG_TAG + "_CLASSES"
            var max = 0
            try {
                val doc = Jsoup.connect("https://ogres.club/classes")
                        .get()
//                log(DEBUG_TAG_LOADING_CLASSES_OGRES_CLUB, "successfully connected to https://ogres.club/classes")
                val table = doc.select("tbody")[0]
//                            log(DEBUG_TAG_LOADING_CLASSES_OGRES_CLUB, "tbody.toString = " + table.toString());
                val rows = table.select("tr")
//                            log(DEBUG_TAG_LOADING_CLASSES_OGRES_CLUB, "rows.toString = " + rows.toString());
                max = rows.size
                progressBar.max += max
                for (i in 1 until rows.size) { // first row is unimportant
                    val row = rows[i]
                    val cols = row.select("td")
//                                    log(DEBUG_TAG_LOADING_CLASSES_OGRES_CLUB, "cols = " + cols.toString());
                    val class_link = cols[0].select("a")[0]
//                                    log(DEBUG_TAG_LOADING_CLASSES_OGRES_CLUB, "classlink = " + class_link.toString());
                    val skills_pre_lvl_as_string = cols[6].text().replace("[^\\d]".toRegex(), "")
//                                    log(DEBUG_TAG_LOADING_CLASSES_OGRES_CLUB, "skills/lvl = " + skills_pre_lvl_as_string);
                    val hpr = cols[1].text().trim().toIntOrNull() ?: 0
                    val babBonus = getProgressionBonus(cols[2].text().trim())
                    val fortBonus = getProgressionBonus(cols[3].text().trim())
                    val refBonus = getProgressionBonus(cols[4].text().trim())
                    val willBonus = getProgressionBonus(cols[5].text().trim())
                    val classSkills = cols[7].text().split(",").map { replaceSkill(it.trim()) }.map {
                        it.split(" or ").map { it.trim() }
                        }.flatten()
                    var skills_per_lvl = 2 //standard value, but should be overwritten anyways
                    if (skills_pre_lvl_as_string.matches(Regex("\\d+"))) {
                        skills_per_lvl = skills_pre_lvl_as_string.toInt()
                    }
                    if (class_link.text().replace("\\s".toRegex(), "").isNotEmpty()) {
                        val new_dnd_class = DndClass(
                            name = normalize_for_db(class_link.text()),
                            link = class_link.attr("abs:href"),
                            skills_per_lvl = skills_per_lvl,
                            hpr = hpr,
                            classSkills = classSkills,
                            progressions = HashMap<String, Int>().apply {
                             put(Character.PROGRESSION_REF_BAB, babBonus)
                             put(Character.PROGRESSION_REF_FORT_SAVES, fortBonus)
                             put(Character.PROGRESSION_REF_REFLEX_SAVES, refBonus)
                             put(Character.PROGRESSION_REF_WILL_SAVES, willBonus)
                         }
                        )
                        Global.classesFromOgresClub.add(new_dnd_class)
                                            log(DEBUG_TAG_LOADING_CLASSES_OGRES_CLUB, new_dnd_class.toString());
                    }
                    progressBar.progress += 1
                }
            } catch (e: IOException) {
                e.printStackTrace()
//                            log(DEBUG_TAG_LOADING_CLASSES_OGRES_CLUB, e.message?:"no msg");
            }
            progressBar.max -= max
            progressBar.progress -= max
        }

        fun readArchetypesFromOgresClub(progressBar: ProgressBar) {
            val DEBUG_TAG_LOADING_ARCHETYPES_OGRES_CLUB = DEBUG_TAG + "_ARCHETYPES"
            val max = Global.classesFromOgresClub.size
            progressBar.max += max
            for (dndClass in Global.classesFromOgresClub) {
                try {
                    val doc = Jsoup.connect(dndClass.link).get()
                    log(DEBUG_TAG_LOADING_ARCHETYPES_OGRES_CLUB, "successfully connected to" + dndClass.link)
                    val archetypesHeader = doc.select("h3")

                    for (archetypeHeader in archetypesHeader) {
                        val archetype_name = normalize_for_db(archetypeHeader.text())
                        var archetype = DndArchetype(archetype_name, dndClass.name)
                        //check if archetype already exists and take already existent one
                        for (a in Global.archetypesFromOgresClub) {
                            if (archetype_name.equals(a.name, ignoreCase = true)) {
                                archetype = a
                            }
                        }
                        Global.archetypesFromOgresClub.add(archetype)
                        dndClass.archetype_names.add(archetype.name)
                    }
                    //
                } catch (e: IOException) {
                    e.printStackTrace()
                    //            log(DEBUG_TAG_LOADING_ARCHETYPES_OGRES_CLUB, e.getMessage());
                }
                progressBar.progress += 1
            }
            progressBar.max -= max
            progressBar.progress -= max
        }

        fun readRacesFromOgresClub(progressBar: ProgressBar) {
            val DEBUG_TAG_LOADING_RACES_OGRES_CLUB = DEBUG_TAG + "_RACES"
            var max = 0
            try {
                val doc = Jsoup.connect("https://ogres.club/races")
                        .get()
                log(DEBUG_TAG_LOADING_RACES_OGRES_CLUB, "successfully connected to https://ogres.club/races")
                val race_links = doc.select("td:eq(0) > a[href]")
                //            log(DEBUG_TAG_LOADING_RACES_OGRES_CLUB, "race_link.toString = " + table.toString());
                max = race_links.size
                progressBar.max += max
                for (race_link in race_links) {
                    val race = Race(name = normalize_for_db(race_link.text()), link = race_link.attr("abs:href"))
                    Global.racesFromOgresClub.add(race)
                    progressBar.progress += 1
                }
            } catch (e: IOException) {
                e.printStackTrace()
                //            log(DEBUG_TAG_LOADING_RACES_OGRES_CLUB, e.getMessage());
            }
            progressBar.max -= max
            progressBar.progress -= max
        }

        fun readSubRacesFromOgresClub(progressBar: ProgressBar) {
            val DEBUG_TAG_LOADING_SUB_RACES_OGRES_CLUB = DEBUG_TAG + "_SUB_RACES"
            val max = Global.racesFromOgresClub.size
            progressBar.max += max
            for (race in Global.racesFromOgresClub) {
                try {
                    val doc = Jsoup.connect(race.link).get()
                    log(DEBUG_TAG_LOADING_SUB_RACES_OGRES_CLUB, "successfully connected to" + race.link)
                    val raceHeaders = doc.select("h3")
                    for (raceHeader in raceHeaders) {

                        val subrace_name = normalize_for_db(raceHeader.text())
                        var subrace = Subrace(subrace_name, race.name)
                        //check if subrace already exists and take already existent one
                        for (sr in Global.subracesFromOgresClub) {
                            if (subrace_name.equals(sr.name, ignoreCase = true)) {
                                subrace = sr
                            }
                        }
                        Global.subracesFromOgresClub.add(subrace)
                        race.subraces.add(subrace.name)
                    }
                    //
                } catch (e: IOException) {
                    e.printStackTrace()
                    //            log(DEBUG_TAG_LOADING_SUBRACES_OGRES_CLUB, e.getMessage());
                }
                progressBar.progress += 1
            }
            progressBar.max -= max
            progressBar.progress -= max
        }
    }

    init {
        dbHelper = getInstance(context)
        mLoadFeatsAndStoreTask = LoadFeatsAndStoreTask()
        mLoadRacesAndSubracesAndStoreTask = LoadRacesAndSubracesAndStoreTask()
        mLoadClassesAndArchetypesAndStoreTask = LoadClassesAndArchetypesAndStoreTask()
    }
}