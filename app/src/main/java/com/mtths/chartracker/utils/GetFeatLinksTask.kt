package com.mtths.chartracker.utils

import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.widget.AdapterView
import android.widget.Toast
import com.mtths.chartracker.Global
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.dataclasses.FeatLink
import com.mtths.chartracker.dialogs.DialogShowFeatLinks
import com.mtths.chartracker.dialogs.DialogShowListView
import java.util.*
import kotlin.collections.ArrayList

class GetFeatLinksTask (var mActivity: ProgressBarFrameDropBoxActivity) : AsyncTask<String, Void?, ArrayList<FeatLink>>() {
    private var featID: String? = null
    @Deprecated("Deprecated in Java")
    override fun onPreExecute() {
        mActivity.showProgressBarLoading()
    }

    @Deprecated("Deprecated in Java")
    override fun doInBackground(vararg strings: String): ArrayList<FeatLink> { //remove everything in paranthesis
        val featName = strings[0].replace("\\(.*\\)".toRegex(), "").trim()
                .replace("\\[.*\\]".toRegex(), "").trim()
        log("-----------------------", "Searching for feat: '$featName'")
        featID = FeatLink.normalize(featName)
        log("-----------------------", "normalization: '$featID'")
        val result = checkForMatchingFeatAndGetLinks(Global.featsOnOgresClub, featID)
        result.addAll(checkForMatchingFeatAndGetLinks(Global.featsFromResources, featID))
        return result
    }

    @Deprecated("Deprecated in Java")
    override fun onPostExecute(featLinks: ArrayList<FeatLink>) {
        mActivity.hideProgressBarLoading()
        if (featLinks.size == 0) {
            Toast.makeText(mActivity, "no matching feat found", Toast.LENGTH_SHORT).show()
        } else if (featLinks.size == 1) {

            showLinks(featLinks[0])
        } else {
            /*
             * more than one feat matches
             */

            //check if one or more feats matches the featID exactly
            val featlinkExactMatches = ArrayList<FeatLink>()
            for (featLink in featLinks) {
                if (featLink.id_string.equals(featID, ignoreCase = true)) {
                    featlinkExactMatches.add(featLink)
                }
            }
            if (featlinkExactMatches.isNotEmpty()) {
                //get first element in list
                val mergedExactMatches = featlinkExactMatches[0]
                if (featlinkExactMatches.size > 1) {
                        /*
                        we have more than one exact match
                        merge all other exact matches to first element
                         */
                    featlinkExactMatches.removeAt(0)
                    mergedExactMatches.addAllLinks(featlinkExactMatches)
                }
                showLinks(mergedExactMatches)
            } else {
                /*
                no feat is an exact match so user has to choose
                 */

                //get list of feat names
                val feat_names = ArrayList<String>()
                for (featLink in featLinks) {
                    feat_names.add(featLink.name)
                }
                /*
                show dialog to choose from found feat and then show links
                 */
                val dialog = DialogShowListView.newInstance(feat_names)
                dialog.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->
                    val feat_name = parent.getItemAtPosition(position) as String
                    val featLink = getFeatLink(featLinks, feat_name)
                    if (featLink != null) {
                        showLinks(featLink)
                    } else {
                        Toast.makeText(mActivity, "An error occurred", Toast.LENGTH_SHORT).show()
                    }
                    dialog.dismiss()
                })
                dialog.show(mActivity.supportFragmentManager, TAG_CHOOSE_FEAT_LINK)
            }
        }
    }

    private fun getFeatLink(feat_links: ArrayList<FeatLink>, feat_name: String): FeatLink? {
        for (featLink in feat_links) {
            if (featLink.id_string.equals(FeatLink.normalize(feat_name), ignoreCase = true)) {
                return featLink
            }
        }
        return null
    }

    private fun showLinks(featLink: FeatLink) {
        if (featLink.links.size == 0) {
            Toast.makeText(mActivity, "no matching link found", Toast.LENGTH_SHORT).show()
        } else if (featLink.links.size == 1) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(featLink.links[0]))
            mActivity.startActivity(browserIntent)
        } else { /*
             * more than one link found
             * show list of found links to choose from
             */
            val dialog = DialogShowFeatLinks(
                featLink = featLink,
                onLinkClicked = { link, dialog ->
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
                    mActivity.startActivity(browserIntent)
                    dialog.dismiss()
                }
            )

            dialog.show(mActivity.supportFragmentManager, TAG_CHOOSE_FEAT_LINK)
        }
    }

    private fun checkForMatchingFeatAndGetLinks(featLinks: ArrayList<FeatLink>, featID: String?): ArrayList<FeatLink> {
        return featID?.let { fId -> ArrayList(featLinks.filter{ it.id_string.contains(fId) })} ?: ArrayList()
    }

    companion object {
        var TAG_CHOOSE_FEAT_LINK = "choose_feat_link"
    }

}