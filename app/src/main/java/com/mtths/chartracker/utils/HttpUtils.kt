package com.mtths.chartracker.utils

import android.app.Activity
import android.content.Intent
import com.mtths.chartracker.Item
import com.mtths.chartracker.activities.ActivityWebView
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.json.JSONObject
import java.util.concurrent.TimeUnit
import okhttp3.RequestBody.Companion.toRequestBody


class HttpUtils {

    data class CommitWithLink(val hash: String) {

        val hash_short: String = if (hash.length < 9) hash else hash.substring(0, 7)
        val link_data = "https://bitbucket.org/ihopeidontforgetthisaccname/pydnd/src/$hash/dndpage/data/"
        val link_html = "https://bitbucket.org/ihopeidontforgetthisaccname/pydnd/commits/$hash"
    }

    companion object {

        const val NO_CURRENT_COMMIT_AVAILABLE_MSG: String = "None"
        const val LOOT_MANAGER_URL = "https://ogres.club/lootmanager"

        /**
         * return hash of latest commit or null
         */
        fun getHashOflatestCommit(): CommitWithLink? {
            val url =
                "https://api.bitbucket.org/2.0/repositories/ihopeidontforgetthisaccname/pydnd/commits/master?limit=1"
            val client = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build()
            val request = Request.Builder()
                .url(url)
                .build()
            try {
                client.newCall(request).execute().use { response ->
                    val json = response.body.toString()
                    val obj = JSONObject(json)
                    val values = obj.getJSONArray("values")
                    val firstCommit = values.getJSONObject(1)
                    return CommitWithLink(
                        firstCommit.getString("hash")
                    )
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

        fun getCharacterItems(characterName: String): ArrayList<Item>? {
            val url = "$LOOT_MANAGER_URL/get-char-items?char=${characterName.lowercase()}"

            val client = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build()
            val request = Request.Builder()
                .url(url)
                .build()
            try {
                client.newCall(request).execute().use { response ->
                    return response.body?.let {
                        Item.parseItems(
                            json = it.string().also { Helper.log("CHARITEMS", it) },
                            local = false)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

        fun getCharacterStashItems(characterName: String): ArrayList<Item>? {
            val url = "$LOOT_MANAGER_URL/get-stash-items-for-character?char=${characterName.lowercase()}"

            val client = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build()
            val request = Request.Builder()
                .url(url)
                .build()
            try {
                client.newCall(request).execute().use { response ->
                    return response.body?.let {
                        Item.parseItems(
                            json = it.string().also { Helper.log("CHARSTASHITEMS", it) },
                            local = false)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

        fun removeItemFromCharacter(
            characterName: String,
            item_id: Int,
            amount: Int = 1,
            token: String,
            deviceId: String): String {
            val url = "$LOOT_MANAGER_URL/remove-item-from-character"
            val client = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build()
            val jdata = JSONObject().apply {
                put("char", characterName.lowercase())
                put("item_id", item_id.toString())
                put("amount", amount.toString())
            }
            val body = jdata.toString().toRequestBody("application/json".toMediaTypeOrNull())
            val request = Request.Builder()
                .url(url)
                .header("Auth-Token", token)
                .header("Device-Id", deviceId)
                .header("Content-Type", "application/json")
                .post(body)
                .build()
            try {
                client.newCall(request).execute().use { response ->
                    return response.body?.string() ?: "no response"
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return "error"
        }

        fun equipItem(
            characterName: String,
            item_id: Int,
            amount: Int,
            token: String,
            deviceId: String): String {
            val url = "$LOOT_MANAGER_URL/equip-item"
            val client = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build()
            val jdata = JSONObject().apply {
                put("char", characterName.lowercase())
                put("item_id", item_id.toString())
                put("amount", amount.toString())
            }
            val body = jdata.toString().toRequestBody("application/json".toMediaTypeOrNull())
            val request = Request.Builder()
                .url(url)
                .header("Auth-Token", token)
                .header("Device-Id", deviceId)
                .header("Content-Type", "application/json")
                .post(body)
                .build()
            try {
                client.newCall(request).execute().use { response ->
                    return response.body?.string() ?: "no response"
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return "error"
        }

        /**
         * you should use item_id, not item name.
         * Item name can be used instead of id in special cases
         *
         * StashName can be used, when no characterName is given
         */
        fun changeBalance(
            characterName: String,
            stashName: String,
            item_id: Int,
            itemName: String,
            amount: Int,
            token: String,
            deviceId: String): String {
            val url = "$LOOT_MANAGER_URL/change-balance"
            val client = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build()
            val jdata = JSONObject().apply {
                put("char", characterName.lowercase())
                put("item_id", item_id.toString())
                put("amount", amount.toString())
                put("item_name", itemName)
                put("stash_name", stashName)
            }
            val body = jdata.toString().toRequestBody("application/json".toMediaTypeOrNull())
            val request = Request.Builder()
                .url(url)
                .header("Auth-Token", token)
                .header("Device-Id", deviceId)
                .header("Content-Type", "application/json")
                .post(body)
                .build()
            try {
                client.newCall(request).execute().use { response ->
                    return response.body?.string() ?: "no response"
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return "error"
        }

        fun useCharge(item_id: Int, token: String, deviceId: String): String {
            val url = "$LOOT_MANAGER_URL/use-charge"
            val client = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).build()
            val jdata = JSONObject().apply {
                put("item_id", item_id.toString())
            }
            val body = jdata.toString().toRequestBody("application/json".toMediaTypeOrNull())
            val request = Request.Builder()
                .url(url)
                .header("Auth-Token", token)
                .header("Device-Id", deviceId)
                .header("Content-Type", "application/json")
                .post(body)
                .build()
            try {
                client.newCall(request).execute().use { response ->
                    return response.body?.string() ?: "no response"
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return "error"
        }

        /**
         * Open a web page of a specified URL
         *
         * @param url URL to open
         */
        fun openWebPage(url: String?, activity: Activity) {
            val intnt = Intent(activity, ActivityWebView::class.java)
            intnt.putExtra(ActivityWebView.EXTRA_URL, url)
            Helper.log("transmitted url to ActivityWebView: $url")
            activity.startActivity(intnt)

//            val webpage = Uri.parse(url)
//            val intent = Intent(Intent.ACTION_VIEW, webpage)
//            if (intent.resolveActivity(activity.packageManager) != null) {
//                activity.startActivity(intent)
//            } else {
//                Toast.makeText(activity, "unable to open url: '$url'", Toast.LENGTH_LONG).show()
//            }
        }
    }




}
