package com.mtths.chartracker.parsers

import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.utils.Helper
import java.util.regex.Pattern

object Parsers {

    /**
     * book the exp to all related chars in list
     * nothing will be stored in db
     *
     * @param e
     */
    fun bookExpAndMoney(e: LogEntry, subtract: Boolean = false, chars: List<Character>?) {
        /*
         * book exp to related chars
         */
        Helper.log(
            "Exp and money Booking for log entry with id: ${e.id}: ${e.text}\n characters: ${e.character_ids}, all chars: ${
                chars?.map {
                    Pair(
                        it.name,
                        it.id
                    )
                }
            }"
        )
       chars?.forEach {eC ->
            e.character_ids?.forEach {
                if (eC.id == it) {
                    Helper.log(eC.name + " booking exp and money ")

                    eC.currentExp += if (subtract) -e.exp else e.exp
                    if (e.exp > 0) {
                        eC.totalExp += if (subtract) -e.exp else e.exp
                    }
                    val money = calcMoneyFromText(e.text)
                    eC.money += if (subtract) -money else money
                    Helper.log("${eC.name} current exp: ${eC.currentExp}, ${eC.totalExp}")
                }

            }

        }
    }

    fun calcMoneyFromText(text: String?): Int {
        //get all money references and calculate the sum
        var moneyToAdd = 0
        text?.let { txt ->
            val m = Pattern.compile("[¥€$£]-?\\d+").matcher(txt)
            while (m.find()) {
//            Helper.log("MATCHER", "found nuyen: " + m.group(0));
                moneyToAdd += m.group(0)?.let { it.substring(1).toInt() } ?: 0
            }

        }
        return moneyToAdd
    }

}