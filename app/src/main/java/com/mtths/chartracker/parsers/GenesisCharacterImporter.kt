package com.mtths.chartracker.parsers

import android.content.DialogInterface
import android.net.Uri
import android.util.Log
import android.webkit.MimeTypeMap
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.Global
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.SettingsAdapter
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterArmorSplittermond
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterBuffs
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterLanguages
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSkills
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSpellsSplittermond
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterSpilttermondPowers
import com.mtths.chartracker.charprogressitemadapters.CharProgressItemAdapterWeaponSplittermond
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.SpellSplittermond
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.dialogs.DialogCustom
import com.mtths.chartracker.dialogs.DialogNewChar.Companion.createInitialLogs
import com.mtths.chartracker.dialogs.DialogNewChar.Companion.createLogEntry
import com.mtths.chartracker.dialogs.DialogNewChar.Companion.generateInitialLogValues
import com.mtths.chartracker.dialogs.DialogNewChar.Companion.generateStatsLog
import com.mtths.chartracker.dialogs.MyProgressDialog
import com.mtths.chartracker.interfaces.Moddable
import com.mtths.chartracker.utils.Helper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Calendar
import java.util.Date
import kotlin.math.max

class GenesisCharacterImporter(val activity: FragmentActivity) {

    val context = activity
    val dbHelper = DatabaseHelper(context)
    val prefs = PrefsHelper(context)
    var progressDialog: MyProgressDialog? = null


    fun log(msg: String) {
        Log.d("IMPORT_GENESIS", "gggggggggg $msg")
    }

    fun showGenesisImportSettingsDialog(onConfirmListener: DialogInterface.OnClickListener, fileType: String) {
        when(fileType) {
            "xml" -> prefs.deleteGenesisCharOnUpdate = true
            "json" -> prefs.deleteGenesisCharOnUpdate = false
            else -> {}
        }
        val d = DialogCustom.getAlterDialogBuilder(context)
        val dialogView = LinearLayout(context).apply {
            layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
            ).apply {
                setPadding(
                    Helper.dpToPx(30f, context),
                    0,
                    Helper.dpToPx(30f, context),
                    0)
            }
        }
        d.setTitle(R.string.dialog_title_genesis_import_settings)
        d.setMessage(R.string.dialog_msg_genesis_import_settings)
        d.setView(dialogView)
        val adapter = SettingsAdapter(
            activity = activity,
            switchContainer = dialogView,
            layoutResourceIdSwitch = R.layout.settings_switch_nobg)
        adapter.inflateGenesisImportSettings()
        d.setPositiveButton(R.string.ok, onConfirmListener)
        d.setNegativeButton(R.string.cancel) {dialog, _ -> dialog.dismiss()}
        d.create().show()
    }

    suspend fun getCharacterIfAlreadyExists(character: Character?): Character? {
        fun log(msg: String) = Log.d("IMPORT_GENESIS", "getCharIfAlreadyExists: $msg")
        return character?.let { c ->
            DatabaseHelper.getInstance(context)?.allCharactersInclDeleted
                ?.find { c.name.equals(it.name, ignoreCase = true) }}
    }

    class GenerateSplitterImportLogsResult(val logs: List<LogEntry>, val new: Boolean, val character: Character)


    suspend fun generateSplitterImportLogs(
        res: SplittermondParser.SplittermondGenesisParserResult,
        fileType: String
    ): GenerateSplitterImportLogsResult? {


        return withContext(Dispatchers.IO) {


            /**
             * vnp with same name and date  will be merged into one and only the highest value kept
             */
            /**
             * vnp with same name and date  will be merged into one and only the highest value kept
             */
            fun mergeByDateAndName(l: List<SplittermondParser.ValueNamePair>): List<SplittermondParser.ValueNamePair> {
                val dMap = HashMap<Pair<String, Date?>, SplittermondParser.ValueNamePair>()
                l.sortedBy { it.date }.forEach {
                    val p = Pair(it.name, it.date)
                    dMap[p]?.also { ex ->
                        ex.exp += it.exp
                        ex.value = max(ex.value, it.value)
                    } ?: run {
                        dMap[p] = it
                    }
                }
                return dMap.values.toList()
            }

            fun generateCPIs(character: Character, charLogs: List<LogEntry>) {

                character.charProgressItemsMap = CharProgressItem.createCharProgressItems(
                    activity = activity as? ProgressBarFrameDropBoxActivity?,
                    character = character
                )

                /*
                use reverse ordering to start with oldest entry in order to get increasing order
                but not changing the order of the charLogs
                 */
                var e: LogEntry
                for (i in charLogs.indices) {
                    e = charLogs[charLogs.size - 1 - i]
                    val filterConstraints = Helper.getFilterConstraints(e.text)

                    filterConstraints.forEach { filterConstraint ->
                        if (Global.ITEMS_SESSION_DATA.none { it.matches(
                                filterConstraint = filterConstraint, context = context)
                            })

                            character.charProgressItemsMap.get(filterConstraint)?.let { cPMetaData ->
                                cPMetaData.mainItem.adapter?.add(e)
                            }
                    }
                }
            }

            fun log(msg: String) {
                Log.e("IMPORT_GENESIS", "[[]][[]] $msg")
            }

            fun removeImportTags(text: String) =
                text.replace(Regex("#[\\w-:_]+"), "").trim()




            res.character?.let { parsedCharacter ->

                var c = getCharacterIfAlreadyExists(parsedCharacter) ?: parsedCharacter
                val new = c.id == -1L
                if (!new) {
                    //add new parsed buffs to existing character
                    parsedCharacter.buffs.forEach { newBuff ->
                        c.buffs.find { it.name.equals(newBuff.name, ignoreCase = true) }?.let {
                            if (it.effects.isEmpty()) {
                                it.effects = newBuff.effects
                            }
                        } ?: run {
                            c.buffs.add(newBuff)
                        }
                    }
                }
                if (fileType == "json"){
                    c.mondzeichen = parsedCharacter.mondzeichen.also { log("mondzeichen: parsed: $it") }
                    c.race = parsedCharacter.race
                }
                dbHelper?.updateCharacter(character = c, keep_last_update_date = false)

                val now = Calendar.getInstance().time
                val dateOfCreation =
                    if (c.created_at.isNotBlank()) {
                        Global.MY_DATE_FORMAT.parse(c.created_at) ?: now
                    } else {
                        now
                    }
                log("dateOfCreation: $dateOfCreation")

                var creationCounter = 1L
                log("character already existed: ${!new}, buffs = ${c.buffs}, size = ${c.size}")
                log("stats: ${c.stats}")

                val charLogs: MutableList<LogEntry> = dbHelper?.getCharLogs(c.id) ?: mutableListOf()

                if (prefs.deleteGenesisCharOnUpdate && !new)  {

                    val genesisTagId = dbHelper?.getTagId("genesis") ?: -1
                    val genesisLogs = charLogs.filter {
                        genesisTagId in (it.tag_ids ?: listOf())
                    }

                    Helper.deleteLogs(
                        context = context,
                        logs = genesisLogs)
                    charLogs.removeAll(genesisLogs)
                    c = dbHelper?.getCharacter(c.id) ?: Character()
                    withContext(Dispatchers.Main) {
                        Toast.makeText(context, R.string.msg_genesis_data_deleted, Toast.LENGTH_SHORT).show()
                    }
                    // we need to reload character, because in deleteLogs stats and exp is changed
                    // but not in the currently loaded character Instance

                }
                /*
                In this copy we want to remove the import tags to focus on the data in the log entry
                 */
                charLogs.forEach { it.text = removeImportTags(it.text)}
                log( "charLogs = ${charLogs.joinToString(separator = "\n"){"${it.created_at} ${it.exp} ${it.text}"}}")

                /*
               create unmodified (import tags still there) copy of charLogs
                */
                val completeCharLogs = dbHelper?.getCharLogs(c.id) ?: mutableListOf()
                generateCPIs(character = c, charLogs = completeCharLogs)

                val logs = mutableListOf<LogEntry>()

                /*
                 * only import exp, if it is zero, because that means you only use the exp from
                 * json and no other source.
                 */
                if (fileType == "json" && (c.totalExp == 0 || prefs.deleteGenesisCharOnUpdate)) {
                    log("creating exp logs for character because total exp was null " +
                            "(total exp = ${c.totalExp} or all genesis data was deleted " +
                            "before import")
                    if (parsedCharacter.totalExp > 0) {
                        logs.add(
                            createLogEntry(
                                text = "gesamt exp aus genesis json",
                                exp = parsedCharacter.totalExp,
                                date = dateOfCreation,
                                timeOffsetInMillis = creationCounter * 1000
                            )
                        )
                        creationCounter++
                    }
                    if (parsedCharacter.currentExp > 0) {
                        logs.add(
                            createLogEntry(
                                text = "investierte exp aus genesis json",
                                exp = -(parsedCharacter.totalExp - parsedCharacter.currentExp),
                                date = dateOfCreation,
                                timeOffsetInMillis = creationCounter * 1000
                            )
                        )
                        creationCounter++
                    }
                }



                res.rewards.map { it.apply { logEntry.session_ids = Global.activeSessionIds } }
                    .forEach { r ->
                        log("-----------....reward log: ${r.logEntry.created_at} " +
                                "${r.logEntry.exp} ${r.text} checking existence")
                        val exLogs = charLogs.filter { r.matchesText(it) }
                        if (exLogs.isNotEmpty()) {
                            log("reward exLogs matching text = " +
                                    exLogs.joinToString("\n") {
                                        "${it.created_at} ${it.exp} ${it.text}" })
                        }
                        if (exLogs.find {
                                Global.DATE_FORMAT_NO_TIME.format(it.dateOfCreation!!) ==
                                        Global.DATE_FORMAT_NO_TIME.format(r.logEntry.dateOfCreation!!)
                            } == null) {
                            log("found no log with same text and date " +
                                    "-> adding reward log ...---------------")
                            //replace log text with log text appended with resources gained
                            logs.add(r.logEntry.copy(text = r.text).apply {
                                created_at = r.logEntry.created_at
                                log("adding: $created_at $exp $text $character_ids")
                            })
                        } else {
                            log("one exLog had even same creation day " +
                                    "--> Assuming this reward log entry already exists.")
                        }
                    }




                res.attributesInitial.filter {
                    val v = c.getStatValue(it.name)
                    (v != null && it.value > v) || v == null
                }.let {
                    if (it.isNotEmpty()) {
                        logs.add(
                            generateInitialLogValues(
                                attributes = it,
                                note = "",
                                level = null,
                                className = "",
                                date = dateOfCreation,
                                timeOffsetInMillis = creationCounter*1000,
                                context = context
                            )
                        )
                        creationCounter++
                    }
                }

                val skAdapter = c.charProgressItemsMap[R.string.filter_constraint_skill]?.mainItem?.adapter
                        as? CharProgressItemAdapterSkills?


                res.skillsInitial.filter {
                    it.value > c.getStatValueOrZero(it.name)
                }.let{
                    // skills without masteries
                    if (it.isNotEmpty()) {
                        logs.add(
                            generateStatsLog(
                                preText = ".ignore",
                                statCPIfilterConstraintId = R.string.filter_constraint_skill,
                                data = it,
                                date = dateOfCreation,
                                timeOffsetInMillis = creationCounter * 1000,
                                context = context
                            )
                        )
                        creationCounter++
                    }
                }

                //masteries
                res.skillsInitial.forEach { sk ->
                    //remove already existing masteries
                    val masteriesToRemove = mutableListOf<SplittermondParser.Mastery>()

                    sk.masteries.forEach { m ->
                        skAdapter?.data?.find { sk.name.equals(it.name, ignoreCase = true)}?.let { exSk ->
                            exSk.specializations.find{
                                it.name.equals(m.name, ignoreCase = true) &&
                                        it.level == m.level }?.let { exSpec ->
                                masteriesToRemove.add(m)
                                Log.e("MASTERYY", "remove mastery from initial skills $m")
                            }
                        }
                    }
                    masteriesToRemove.forEach {
                        sk.masteries.remove(it)
                    }
                }
                val skillsWithMasteriesToAdd = res.skillsInitial.filter {
                    it.masteries.isNotEmpty()
                }
                val skillMasteriesToAdd = skillsWithMasteriesToAdd.map { it.masteries }.flatten()
                if (skillMasteriesToAdd.isNotEmpty()) {
                    logs.add(createLogEntry(
                        text = skillMasteriesToAdd.joinToString(
                            separator = "\n", prefix = ".ignore\n") { m ->
                            "%s %s [%s]".format(
                                context?.getString(R.string.filter_constraint_skill),
                                m.skillName,
                                m.name + if (m.level > 1) " ${m.level}" else "")
                        },
                        date = dateOfCreation,
                        timeOffsetInMillis = creationCounter * 1000
                    )
                    )
                    creationCounter++
                }


                res.resourcesInitial.also { log("resources initial BEFORE filter: ${it.joinToString { "${it.name} ${it.value}" }}") }.filter {
                    it.value > c.getStatValueOrZero(it.name)
                }.also { log("resources initial AFTER filter: ${it.joinToString { "${it.name} ${it.value}" }}") }.let { resInit ->
                    if (resInit.isNotEmpty()) {
                        resInit.forEach {
                            logs.add(
                                generateStatsLog(
                                    statCPIfilterConstraintId = R.string.filter_constraint_resource,
                                    preText = ".ignore",
                                    data = listOf(it),
                                    date = dateOfCreation,
                                    timeOffsetInMillis = creationCounter * 1000,
                                    context = context
                                )
                            )
                            creationCounter++
                        }
                    }
                }
                val lAdapter = c.charProgressItemsMap[R.string.filter_constraint_language]?.mainItem?.adapter as? CharProgressItemAdapterLanguages?
                res.languagesInitial.also {
                    log("languages initial BEFORE filter: ${it.joinToString()}")
                }.filter { language ->
                    lAdapter?.data?.find { it.name.equals(language, ignoreCase = true) } == null
                }.also {
                    log("languages initial AFTER filter: ${it.joinToString()}")
                }.let { langs ->
                    if (langs.isNotEmpty()) {
                        langs.joinToString("\n", prefix = ".featured\n") {
                            "%s $it".format(context?.getString(R.string.filter_constraint_language))
                        }.let {
                            logs.add(
                                createLogEntry(
                                    text = it,
                                    date = dateOfCreation,
                                    timeOffsetInMillis = creationCounter * 1000
                                )
                            )
                            creationCounter++
                        }
                    }
                }



                /*
                 spells without date (should be mostly those imported from json)
                 those with date are dealt with later
                 */
                val sAdapter = c.charProgressItemsMap[R.string.filter_constraint_spell]?.mainItem?.adapter
                        as? CharProgressItemAdapterSpellsSplittermond?

                res.spells.filter { it.date == null }.forEach { dSpell ->
                    val exSpell = sAdapter?.data?.find {
                        setOf(it.name.lowercase(), it.genesisId.lowercase()).intersect(
                            setOf(dSpell.spell.genesisId, dSpell.spell.name)).isNotEmpty()
                    }

                    dSpell.spell.let{ s ->

                        val spelldata = listOf(
                            "%s ${s.name}".format(context?.getString(R.string.filter_constraint_spell)),
                            "${context.getString(SpellSplittermond.REF_ID_SCHOOL)} = ${s.school}",
                            "${context.getString(SpellSplittermond.REF_ID_LEVEL)} = ${s.level}",
                            "${context.getString(SpellSplittermond.REF_ID_DIFFICULTY)} = ${s.difficulty}",
                            "${context.getString(SpellSplittermond.REF_ID_FOCUS_COST)} = ${s.focus}",
                            "${context.getString(SpellSplittermond.REF_ID_CAST_DURATION)} = ${s.castDuration}",
                            "${context.getString(SpellSplittermond.REF_ID_CAST_RANGE)} = ${s.castRange}",
                            "${context.getString(SpellSplittermond.REF_ID_SPELL_DURATION)} = ${s.spellDuration}",
                            "${context.getString(SpellSplittermond.REF_ID_ENHANCEMENT)} = ${s.enhancement}",
                            "${context.getString(SpellSplittermond.REF_ID_ENHANCEMENT_DESCRIPTION)} " +
                                    "= [${s.enhancementDescription}]",
                            "${context.getString(SpellSplittermond.REF_ID_ENHANCEMENT_OPTIONS)} = ${s.enhancementOptions}",
                            "${context.getString(SpellSplittermond.REF_ID_PAGE)} = ${s.page}",
                            "${context.getString(SpellSplittermond.REF_ID_DESCRIPTION)} = [${s.description}]",
                            "${context.getString(SpellSplittermond.REF_ID_GENESIS_ID)} = ${s.genesisId}"
                        )


                        if ( exSpell == null) {
                            // those are spells that did not cost exp, so they don't have a date

                            val ignore = if (dSpell.exp == 0) {
                                "${Global.IGNORE_EXP_CHECK_INDICATOR} "
                            } else {
                                ""
                            }

                            logs.add(
                                createLogEntry(
                                    text = "${ignore}${spelldata.joinToString()}",
                                    date = dateOfCreation,
                                    timeOffsetInMillis = creationCounter * 1000
                                ).apply { exp = -dSpell.exp }
                            )
                            creationCounter++
                        } else {
                            /*
                            spell already exists
                            overwrite text, because we want the spell data from json,
                            but the date from xml
                            */
                            exSpell.relatedLogEntry?.let { e ->
                                e.text = e.text.substringBefore(context.getString(
                                    R.string.filter_constraint_spell)) + spelldata.joinToString()
                                Helper.updateLogDbAndGlobal(e = e, context = context)
                            }
                        }

                    }
                }



                res.attributesIncreases.filter {
                    it.value > c.getStatValueOrZero(it.name)
                }.forEach {
                    logs.add(
                        createLogEntry(
                            text = "%s %s %d".format(
                                context?.getString(R.string.filter_constraint_attribute),
                                Helper.capitalizeNewsLetterStyle(it.name),
                                it.value
                            ),
                            date = it.date ?: dateOfCreation,
                            timeOffsetInMillis = creationCounter * 1000
                        ).apply { exp = -it.exp }
                    )
                    creationCounter++
                }

                mergeByDateAndName(res.skillsIncreases).filter {
                    it.value > c.getStatValueOrZero(it.name)
                }.forEach {
                    logs.add(
                        createLogEntry(
                            text = "%s %s %d".format(
                                context.getString(R.string.filter_constraint_skill),
                                Helper.capitalizeNewsLetterStyle(it.name),
                                it.value
                            ),
                            date = it.date ?: dateOfCreation,
                            timeOffsetInMillis = creationCounter * 1000
                        ).apply { exp = -it.exp }
                    )
                    creationCounter++
                }

                mergeByDateAndName(res.languagesIncreases).forEach {
                    logs.add(
                        createLogEntry(
                            text = "%s %s".format(
                                context.getString(R.string.filter_constraint_language),
                                Helper.capitalizeNewsLetterStyle(it.name)
                            ),
                            date = it.date ?: dateOfCreation,
                            timeOffsetInMillis = creationCounter * 1000
                        ).apply { exp = -it.exp }
                    )
                    creationCounter++
                }

                val exMasteries = mutableListOf<SplittermondParser.Mastery>()
                res.masteryIncreases.forEach { m ->
                    log("####### check ex mastery: $m")
                    skAdapter?.data?.find {
                        it.name.equals(
                            m.skillName,
                            ignoreCase = true
                        )
                    }?.also {
                        log("####### check ex mastery: $m;; found skill: ${it.name}: ${it.specializations.joinToString { "${it.name}: ${it.level}" }}")
                    }?.specializations?.find {
                        (it.name.equals(m.name, ignoreCase = true) ||
                                m.genesisId.matches(Regex("(?i)${it.name}(${it.level})?")) &&
                                it.level == m.level)
                    }?.also { log("####### check ex mastery: $m;; found skill mastery: $it") }?.let {
                        exMasteries.add(m)
                    }
                }
                val newMasteries = res.masteryIncreases.toMutableList()
                exMasteries.forEach{
                    newMasteries.remove(it)
                    log("removing mastery: '$it' from masteryIncreases because it already exists")
                }

                fun buildMasteryText(
                    m: SplittermondParser.Mastery,
                    useGenesisId: Boolean,
                    includeDescription: Boolean): String {
                    return "%s %s [%s%s%s]".format(
                        context?.getString(R.string.filter_constraint_skill),
                        m.skillName,
                        if (useGenesisId) m.genesisId else m.name,
                        if (m.level > 1) " ${m.level}" else "",
                        if (includeDescription && m.description.isNotBlank()) " // ${m.description}" else "")
                }

                newMasteries.forEach { m ->
                    logs.add(createLogEntry(
                        text = buildMasteryText(m = m, useGenesisId = false, includeDescription = true),
                        date = m.date ?: dateOfCreation,
                        timeOffsetInMillis = creationCounter * 1000
                    ).apply { exp = -m.exp })
                    creationCounter++
                }

                /*
                replace logs using genesisId for mastery names with their real names
                when doing import via xml name will always be genesisId
                 */
                exMasteries.forEach { exM ->
                    if (exM.genesisId.isNotBlank()) {

                        val oldText = buildMasteryText(
                            m = exM, useGenesisId = true, includeDescription = false)
                        log("replacing mastery genesisId with real name: replace: '$oldText'")
                        completeCharLogs.find { it.text.lowercase().contains(oldText.lowercase()) }?.let { e ->
                            log("replacing mastery genesisId with real name: found log to replace: ${e.text}")

                            val newText = buildMasteryText(
                                m = exM, useGenesisId = false, includeDescription = true)
                            log("replacing mastery genesisId with real name: replace with: ${newText}")

                            e.text = e.text.replace(oldText, newText, ignoreCase = true)
                            log("replacing mastery genesisId with real name: log after replacing: ${e.text}")

                            Helper.updateLogDbAndGlobal(e = e, context = context)
                        }
                    }
                }



                mergeByDateAndName(res.resourceIncreases).also {
                    log ("resource increases BEFORE filtering: ${it.joinToString{"${it.name}, +${it.value}"}}")
                }.filter {
                    it.value > c.getStatValueOrZero(it.name)
                }.also {
                    log ("resource increases AFTER filtering: ${it.joinToString{"${it.name}, +${it.value}"}}")
                }
                    .forEach {
                        logs.add(
                            createLogEntry(
                                text = "%s ${it.name} +${it.value}".format(
                                    context?.getString(R.string.filter_constraint_resource)),
                                date = it.date ?: dateOfCreation,
                                timeOffsetInMillis = creationCounter * 1000
                            ).apply { exp = -it.exp }
                        )
                        creationCounter++
                    }




                val pAdapter =  c.charProgressItemsMap[R.string.filter_constraint_splittermond_power]?.mainItem?.adapter
                        as? CharProgressItemAdapterSpilttermondPowers?

                //todo add powermod, masterref specs (check if exists is not working)

                res.powers.forEach { power ->

                    val exPower = pAdapter?.data?.
                    find { it.name.equals(power.name, ignoreCase = true) ||
                            it.name.matches(Regex("(?i)${power.genesisId}(${power.count})?"))}
                    //dont overwrite buff visibility
                    c.buffStates[exPower?.name]?.hide?.let { power.buff?.hide = it }

                    val nameString = power.name + if (power.count > 1) " ${power.count}" else ""
                    val powerData = mutableListOf(
                        "%s $nameString".format(
                            context?.getString(R.string.filter_constraint_splittermond_power)
                        )
                    ).apply {
                        power.buff?.let { b ->
                            if (b.effects.isNotEmpty()) {
                                add("%s = %s".format(
                                    CharProgressItemAdapterBuffs.BUFF_REF,
                                    b.effects.joinToString(prefix = "[", postfix = "]")
                                ))
                            }
                        }
                        if (power.description.isNotBlank()) {
                            add("${ValueNamePair.REF_DESCRIPTION} = [${power.description}]")
                        }
                    }

                    if ( exPower == null) {

                        val ignore = if (power.exp == 0) {
                            "${Global.IGNORE_EXP_CHECK_INDICATOR} "
                        } else {
                            ""
                        }

                        logs.add(
                            createLogEntry(
                                text = "${ignore}${powerData.joinToString()}",
                                date = dateOfCreation,
                                timeOffsetInMillis = creationCounter * 1000
                            ).apply { exp = -power.exp }
                        )
                        creationCounter++

                    } else {
                        /*
                        power already exists
                        overwrite text, because we want the power data from json,
                        but the date from xml
                        */
                        exPower.relatedLogEntry?.let { e ->
                            e.text = e.text.substringBefore(context.getString(
                                R.string.filter_constraint_splittermond_power)) + powerData.joinToString()
                            Helper.updateLogDbAndGlobal(e = e, context = context)
                        }
                    }




                }


                //those are spells imported from xml
                res.spells.filter { it.date != null }.forEach { dSpell ->

                    if (sAdapter?.data?.
                        find { it.name.equals(dSpell.spell.name, ignoreCase = true) ||
                                it.genesisId.equals(dSpell.spell.name, ignoreCase = true)} == null) {

                        val ignore = if (dSpell.exp == 0) {
                            "${Global.FEATURED_MARKER} "
                        } else {
                            ""
                        }
                        logs.add(
                            createLogEntry(
                                text = "$ignore%s ${dSpell.spell.name}, ".format(
                                    context?.getString(R.string.filter_constraint_spell)
                                ) +
                                        "${context.getString(SpellSplittermond.REF_ID_SCHOOL)} = ${dSpell.spell.school}, " +
                                        "${context.getString(SpellSplittermond.REF_ID_LEVEL)} = ${dSpell.free}",
                                date = dSpell.date!!,
                                timeOffsetInMillis = creationCounter * 1000
                            ).apply { exp = -dSpell.exp }
                        )
                        creationCounter++
                    }
                }

                val wAdapter = c.charProgressItemsMap[R.string.filter_constraint_weapon]?.mainItem?.adapter
                        as? CharProgressItemAdapterWeaponSplittermond?

                res.weapons.forEach { weapon ->

                    if (wAdapter?.data?.
                        find { it.name.equals(weapon.name, ignoreCase = true) } == null) {

                        val wData = mutableListOf(
                            "%s ${weapon.name}".format(
                                context?.getString(R.string.filter_constraint_weapon)
                            ),
                            "damage = ${weapon.damage}",
                            "wgs = ${weapon.wgs}",
                            "skill = ${weapon.skill}",
                            "attributes = ${weapon.attributes.joinToString(prefix = "[", postfix = "]")}").apply {

                            if (weapon.range > 0) {
                                add("range = ${weapon.range}")
                            }

                            if (weapon.features.isNotEmpty()) {
                                add(
                                    "mods = ${
                                        weapon.features.joinToString(
                                            prefix = "[",
                                            postfix = "]"
                                        ) { f ->
                                            f.name + if (f.level > 0) " ${f.level}" else ""
                                        }
                                    }"
                                )
                            }
                        }

                        logs.add(
                            createLogEntry(
                                text = wData.joinToString(),
                                date = dateOfCreation,
                                timeOffsetInMillis = creationCounter * 1000
                            )
                        )
                        creationCounter++
                    }
                }

                val aAdapter = c.charProgressItemsMap[R.string.filter_constraint_armor]?.mainItem?.adapter
                        as? CharProgressItemAdapterArmorSplittermond?

                res.armors.forEach { armor ->

                    if (aAdapter?.data?.
                        find { it.name.equals(armor.name, ignoreCase = true) } == null) {

                        val aData = listOf(
                            "%s ${armor.name}".format(context?.getString(R.string.filter_constraint_armor)),
                            "${CharProgressItemAdapterArmorSplittermond.REF_SR} = ${armor.damageReduction}",
                            "${CharProgressItemAdapterArmorSplittermond.REF_TIK_MALUS} = ${armor.tik}",
                            "${CharProgressItemAdapterArmorSplittermond.REF_BEHINDERUNG} = ${armor.behinderung}",
                            "${CharProgressItemAdapterArmorSplittermond.REF_VERTEIDIGUNG} = ${armor.verteidigung}",
                            "${Moddable.DEFAULT_REF_MOD} = ${armor.features.joinToString(prefix = "[", postfix = "]"){ f ->
                                "${f.name} ${f.level}"
                            }}",
                        )

                        logs.add(
                            createLogEntry(
                                text = aData.joinToString(),
                                date = dateOfCreation,
                                timeOffsetInMillis = creationCounter * 1000
                            )
                        )
                        creationCounter++

                    }
                }




//                logs.addAll(res.logs)
                log("all logs to add: \n" + logs.joinToString(separator = "\n------") { "${it.created_at} ${it.exp} '${it.text}'" })

                return@withContext GenerateSplitterImportLogsResult(
                    logs = logs,
                    new = new,
                    character = c
                )

            }
        }
    }

    fun getfiletype(uri: Uri): String {
        val ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(
            context.contentResolver.getType(uri)
        )
        log("extension of import file: '$ext'")

        return when (ext?.lowercase()) {
            "xml" -> {
                "xml"
            }

            "bin", "json" -> {
                //sometimes a xml file is classified as bin by android
                "json"
            }

            else -> ""
        }
    }

    fun importCharacter(uri: Uri, onFinish: () -> Unit) {
        var message = "no character found in parse result"

        var fileType = getfiletype(uri)

        showGenesisImportSettingsDialog(
            fileType = fileType,
            onConfirmListener = { _, _ ->

            CoroutineScope(Dispatchers.Main).launch {

                progressDialog = MyProgressDialog.newInstance(context).apply {
                    setTitle(R.string.msg_importing_character)
                    show(MyProgressDialog.PROGRESS_STYLE_SPINNER)
                }

                val logsCreated = withContext(context = Dispatchers.IO) {

                    val ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(
                        context.contentResolver.getType(uri))
                    log("extension of import file: '$ext'")

                    //Parse character according to file type
                    val parserResult = when (fileType) {
                        "xml" -> {
                            SplittermondParser.parseSplittermondCharFromGenesisXml(
                                uri,
                                context
                            )
                        }
                        "json" -> {
                            fileType = "json"
                            SplittermondParser.parseSplittermondCharFromGenesisJson(
                                uri,
                                context
                            )
                        }
                        else -> {
                            message = context.getString(R.string.msg_invalid_filetype_import_splittermond)
                            null
                        }
                    }

                    parserResult?.let { parserResult ->
                        log("parsed Splittermond character: ${parserResult.character}")

                        generateSplitterImportLogs(
                            res = parserResult,
                            fileType = fileType)?.let { r ->
                            val c = r.character
                            val dateString = Global.MY_DATE_FORMAT.format(
                                Calendar.getInstance().time).replace(" ", "_")
                            val logs = r.logs.map { it.apply { text =
                                "#genesis #%s %s".format(dateString, text)
                            } }

                            if (c.id < 0) {
                                message = context.getString(R.string.msg_new_character_created)
                                dbHelper.createCharacter(c)?.also {
                                    c.id = it
                                }
                            } else {
                                message = context.getString(R.string.msg_character_updated)
                                c.id
                            }?.let {
                                createInitialLogs(
                                    initialLogs = logs,
                                    newCharId = it,
                                    context = context
                                )
                                if (r.new) {
                                    Helper.add(Global.chars, c)
                                } else {
                                    Helper.update(Global.chars, c)
                                }
                                listOf(
                                    Global.allLogs,
                                    Global.logsFilteredBySession,
                                    Global.logsToShow
                                ).forEach {
                                    it.addAll(logs)
                                    it.sortWith(LogEntry.CREATION_DATE_COMPARATOR)
                                }
                                logs.forEach {
                                    Parsers.bookExpAndMoney(e = it, chars = listOf(c))
                                }
                                // vorteile buff names (ref) might have been replaced be real names
                                c.clearNonModifiableBuffs()
                                dbHelper.updateCharacter(
                                    character = c, keep_last_update_date = false)

                                logs
                            }
                        }
                    }

                }
                log("logs created = ${logsCreated?.joinToString(separator = "\n")}")

                var msg = message
//                                if (prefs.deleteGenesisCharOnUpdate) {
//                                    msg = "$msg\nold genesis logs have been deleted."
//                                }
                msg = "$msg\n" + context.getString(R.string.msg_display_num_log_created, logsCreated?.size)

                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                progressDialog?.dismiss()

                onFinish()
            }
        })
    }


}