package com.mtths.chartracker.parsers

import android.content.Context
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.dataclasses.Buff

object Translators {

    val splittermondTranslatorDE_EN = HashMap<String, String>().apply {

        put("ausstrahlung", "charisma")
        put("beweglichkeit", "agility")
        put("intuition", "intuition")
        put("konstitution", "constitution")
        put("mystik", "mystic")
        put("stärke", "strength")
        put("verstand", "mind")
        put("willenskraft", "willpower")

        put("handgemenge", "melee")
        put("hiebwaffen", "slashing")
        put("kettenwaffen", "chains")
        put("klingenwaffen", "blades")
        put("schusswaffen", "longrange")
        put("stangenwaffen", "staffs")

        put("wurfwaffen", "throwing")
        put("akrobatik", "acrobatics")
        put("alchemie", "alchemy")
        put("anführen", "leadership")
        put("arkane kunde", "arcane lore")
        put("athletik", "athletics")
        put("darbietung", "performance")
        put("diplomatie", "diplomacy")
        put("edelhandwerk", "clscraft")
        put("empathie", "empathy")
        put("entschlossenheit", "determination")
        put("fingerfertigkeit", "dexterity")
        put("geschichte und mythen", "history")
        put("handwerk", "craftmanship")
        put("heilkunde", "heal")
        put("heimlichkeit", "stealth")
        put("jagdkunst", "hunting")
        put("länderkunde", "country lore")
        put("naturkunde", "nature")
        put("redegewandtheit", "eloquence")
        put("schlösser und,fallen", "locks n traps")
        put("schwimmen", "swim")
        put("seefahrt", "seafaring")
        put("straßenkunde", "street lore")
        put("tierführung", "animals")
        put("überleben", "survival")
        put("wahrnehmung", "perception")
        put("zähigkeit", "endurance")

        put("bannmagie", "antimagic")
        put("beherrschungsmagie", "control magic")
        put("bewegungsmagie", "motion magic")
        put("erkenntnismagie", "insight magic")
        put("felsmagie", "stone magic")
        put("feuermagie", "fire magic")
        put("heilungsmagie", "heal magic")
        put("illusionsmagie", "illusion magic")
        put("kampfmagie", "combat magic")
        put("lichtmagie", "light magic")
        put("naturmagie", "nature magic")
        put("schattenmagie", "shadow magic")
        put("schicksalsmagie", "fate magic")
        put("schutzmagie", "protection magic")
        put("stärkungsmagie", "enhance magic")
        put("todesmagie", "death magic")
        put("verwandlungsmagie", "transformation magic")
        put("wassermagie", "water magic")
        put("windmagie", "wind magic")
        put("allgemein", "general")
        put("magie", "magic")
        put("kampf", "combat")

        put("ansehen", "reputation")
        put("stand", "status")
        put("kontakte", "contacts")
        put("vermögen", "wealth")
        put("zuflucht", "refuge")
        put("golem", "golem")
        put("gefolgt", "entourage")
        put("glaube", "faith")
        put("kreatur", "creature")
        put("mentor", "mentor")
        put("rang", "rank")
        put("relikt", "relic")
        put("ruhm", "fame")
    }

    val splittermondTranslatorEN_DE = HashMap<String, String>().apply {
        splittermondTranslatorDE_EN.keys.forEach { k ->
            splittermondTranslatorDE_EN[k]?.let { put(it, k) }
        }
    }

    fun translateSplittermondDeEn(s: String): String {
        return splittermondTranslatorDE_EN[s] ?: s
    }
    fun translateSplittermondEnDe(s: String): String {
        return splittermondTranslatorEN_DE[s.lowercase()] ?: s
    }

    val splittermondBuffsDict = HashMap<String, (count: Int, context: Context) -> String>().apply {
        put("sturdy", {c, _ -> "hp +$c"})
        put("swift", {c, _ -> "speed +$c"})
        put("griffintail", {c, _ -> "beweglichkeit checks +1"})
        put("focuspool", {c, cxtx -> "fokus +${5*c* PrefsHelper(cxtx).fokusPercentage / 100}"})
        put("resistmind", {c, _ -> "geistiger widerstand +${2*c}"})
        put("resistbody", {c, _ -> "körperlicher widerstand +${2*c}"})
        put("addsplinter", {c, _ -> "splitterpunkte +2"})
    }

    fun getSplittermondPower(powerref: String, count: Int?, context: Context) : Buff? {
        val b = Buff().apply {
            name = powerref
            splittermondBuffsDict[powerref]?.let {
                effects = it(count ?: 1, context)
                    .split(",")
                    .filterNot { it.isBlank() }
                    .map {Buff.parseBuffEffect(it) }
                    .toMutableList()
            } ?: return null
            type = "power"
            modifiable = true
            active = true
            expand = true
        }
        return b
    }

    val splittermondSizeDict = HashMap<String, Int>().apply {
        put("rattles", 3)
        put("alb", 5)
        put("gnome", 3)
        put("kappa", 4)
        put("naga", 5)
        put("nixe", 5)
        put("oreade", 5)
        put("tengu", 5)
        put("varg", 6)
        put("dwarf", 4)
        put("human", 5)
    }

    fun getSplittermondSize(raceName: String): Int {
        var race = raceName.lowercase()
        race = if (raceName.startsWith("rattles")) "rattles" else race
        return splittermondSizeDict[race] ?: 0
    }

}