package com.mtths.chartracker.parsers

import android.content.Context
import android.net.Uri
import android.util.Log
import com.mtths.chartracker.Global
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.dataclasses.Buff
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.DndArchetype
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.Race
import com.mtths.chartracker.dataclasses.SpellSplittermond
import org.json.JSONObject
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.Date
import java.util.regex.Pattern
import java.util.stream.Collectors
import kotlin.math.min

object SplittermondParser {

    class SplittermondGenesisParserResult(
        val character: Character?,
        val attributesInitial: List<ValueNamePair>,
        val attributesIncreases: List<ValueNamePair>,
        val skillsInitial: List<ValueNamePair>,
        val skillsIncreases: List<ValueNamePair>,
        val masteryIncreases: List<Mastery>,
        val resourcesInitial: List<ValueNamePair>,
        val resourceIncreases: List<ValueNamePair>,
        val rewards: List<Reward>,
        val languagesInitial: List<String>,
        val languagesIncreases: List<ValueNamePair>,
        val spells: List<SpellDated>,
        val powers: List<Power>,
        val weapons: List<Weapon>,
        val armors: List<Armor>
    )
    data class Mastery(
        var name: String = "",
        var genesisId: String = "",
        var level: Int = 0,
        val free: Int = -1,
        val skillName: String,
        val date: Date? = null,
        var description: String = "",
        var page: String = "",
        val exp: Int = 0)

    data class ValueNamePair(
        var name: String = "",
        var value: Int = 0,
        var description: String = "",
        var exp: Int = 0,
        val date: Date? = null,
        var genesisId: String = "",
        val masteries: MutableList<Mastery> = mutableListOf()
    )

    data class Reward(
        var logEntry: LogEntry,
        val resourcesGained: MutableList<ValueNamePair> = mutableListOf()){

        val text: String
            get() = (logEntry.text + "\n" + getModificationText()).trim()

        /**
         * @Resource is added, when there are Resource rewards attached to the log entry
         */
        fun matchesLogText(e: LogEntry): Boolean = e.text.substringBefore("@Resource") ==
                logEntry.text.substringBefore("@Resource")

        fun matchesText(e: LogEntry) = e.text.trim() == text.trim()

        fun getModificationText() =
            resourcesGained.joinToString(separator = "\n") {
                "@Resource %s +%d".format(it.name, it.value) }
        /**
         * check if the resources gained match those of log entry
         */
        fun matchesModification(e:LogEntry): Boolean =
            e.text.substringAfter("@Resource") == getModificationText()

        fun equalsLog(e: LogEntry): Boolean {
            val matchesDate = e.dateOfCreation?.let { Global.DATE_FORMAT_NO_TIME.format(it) } ==
                    logEntry.dateOfCreation?.let { Global.DATE_FORMAT_NO_TIME.format(it) }
            return matchesLogText(e) && matchesModification(e) && matchesDate && e.exp == logEntry.exp
        }
    }

    data class SpellDated(
        var date: Date? = null,
        val spell: SpellSplittermond = SpellSplittermond(),
        var exp: Int = 0,
        var free: Int = 0)

    data class Power(
        var name: String = "",
        var genesisId: String = "",
        var count: Int = 0,
        var description: String = "",
        var page: String = "",
        var buff: Buff? = null,
        var exp: Int = 0
    )

    data class Weapon(
        var name: String = "",
        var skill: String = "",
        var attributes: MutableList<String> = mutableListOf(),
        var damage: String = "",
        var wgs: Int = 0,
        var range: Int = 0,
        var features: MutableList<Mastery> = mutableListOf()
    )
    data class Armor(
        var name: String = "",
        var verteidigung: Int = 0,
        var damageReduction: Int = 0,
        var tik: Int = 0,
        var behinderung: Int = 0,
        var features: MutableList<Mastery> = mutableListOf()
    )

    private fun log(msg: String) = Log.d("PARSE_GENESIS", "GGGGGGGGG: $msg")
    private fun loge(msg: String) = Log.e("PARSE_GENESIS", "GGGGGGGGG: $msg")

    fun getSkillName(s: String): String {
        return when {
            s == "clscraft" -> "fine craft"
            s == "locksntraps" -> "locks n traps"
            s != "antimagic" && s.endsWith("magic") -> s.substringBefore("magic") + " magic"
            s.endsWith("lore") -> s.substringBefore("lore") + " lore"
            else -> s
        }
    }

    fun translate(s: String, language_code: String = PrefsHelper.LANGUAGE_CODE_DE): String {
        return if (language_code == PrefsHelper.LANGUAGE_CODE_DE) {
            Translators.translateSplittermondEnDe(s)
        } else {
            s
        }
    }

    fun parseSplittermondCharFromGenesisJson(
        fileUri: Uri, context: Context,
        language_code: String = PrefsHelper.LANGUAGE_CODE_DE
    ): SplittermondGenesisParserResult {





        log("##################################### start parseSplittermondCharFromGenesis")

        var c: Character? = Character()
        var e: LogEntry? = null
        val attributesInit = mutableListOf<ValueNamePair>()
        val attrIncreases = mutableListOf<ValueNamePair>()
        val skillsInit = mutableListOf<ValueNamePair>()
        val masteryIncreases = mutableListOf<Mastery>()
        val languages = mutableListOf<String>()
        val resourcesInitial = HashMap<String, ValueNamePair>()
        val resourcesIncrease = mutableListOf<ValueNamePair>()
        val spells = mutableListOf<SpellDated>()
        val powers = mutableListOf<Power>()
        val weapons = mutableListOf<Weapon>()
        val armors = mutableListOf<Armor>()
        val rewards = mutableListOf<Reward>()


        var inputStreamReader: InputStreamReader? = null
        try {
            inputStreamReader = InputStreamReader(context.contentResolver.openInputStream(fileUri))
            log("successfully initiated InputStreamReader to file path")
        } catch (e: FileNotFoundException) {
            loge("couldn't open inputstream from uri to parse splittermond character")
            c = null
            e.printStackTrace()
        }
        inputStreamReader?.let { isr ->
            val json = BufferedReader(inputStreamReader).lines().parallel().collect(Collectors.joining("\n"))
            try {
                var ref = ""
                val jCharacter = JSONObject(json)
                var jitem: JSONObject?
                ref = "name"
                if (jCharacter.has(ref)) {
                    c?.name = jCharacter.getString(ref)
                }
                ref = "race"
                if (jCharacter.has(ref)) {
                    c?.race = Race(jCharacter.getString(ref))
                }
                ref = "freeExp"
                if (jCharacter.has(ref)) {
                    jCharacter.getInt(ref).let {
                        c?.currentExp = it
                        c?.totalExp = it
                    }
                }
                ref = "investedExp"
                if (jCharacter.has(ref)) {
                    c?.let {
                        it.totalExp += jCharacter.getInt(ref)
                    }
                }

                ref = "moonSign"
                if (jCharacter.has(ref)) {
                    val jMondZeichen = jCharacter.getJSONObject(ref)
                    c?.mondzeichen = Character.Mondzeichen().apply {
                        var rref = "name"
                        if (jMondZeichen.has(rref)) {
                            name = jMondZeichen.getString(rref)
                        }
                        rref = "description"
                        if (jMondZeichen.has(rref)) {
                            description = jMondZeichen.getString(rref)
                        }
                    }
                }

                ref = "skills"
                if (jCharacter.has(ref)) {
                    val jskills = jCharacter.getJSONArray(ref)
                    for (i in 0 until jskills.length()) {
                        val jskill = jskills.getJSONObject(i)
                        ValueNamePair().apply {
                            var rref = "name"
                            if (jskill.has(rref)) {
                                name = jskill.getString(rref)
                            }
                            rref = "id"
                            if (jskill.has(rref)) {
                                genesisId = jskill.getString(rref)
                            }
                            rref = "points"
                            if (jskill.has(rref)) {
                                value = jskill.getInt(rref)
                            }
                        }.let { sk ->
                            if (sk.value > 0) {
                                skillsInit.add(sk)
                                log("add skill increase: $sk")
                            }
                            var rref = "masterships"
                            if (jskill.has(rref)) {
                                val jmasteries = jskill.getJSONArray(rref)
                                for (i in 0 until jmasteries.length()) {
                                    val jmastery = jmasteries.getJSONObject(i)

                                    Mastery(skillName = sk.name).apply {
                                        var mref = "name"
                                        if (jmastery.has(mref)) {
                                            name = jmastery.getString(mref)
                                        }
                                        mref = "id"
                                        if (jmastery.has(mref)) {
                                            genesisId = jmastery.getString(mref)
                                        }
                                        mref = "level"
                                        if (jmastery.has(mref)) {
                                            level = jmastery.getInt(mref)
                                        }
                                        mref = "longDescription"
                                        if (jmastery.has(mref)) {
                                            description = jmastery.getString(mref)
                                                .let { if (it == "null") "" else it }
                                        }
                                        mref = "page"
                                        if (jmastery.has(mref)) {
                                            page = jmastery.getString(mref)
                                        }
                                    }.let {
                                        masteryIncreases.add(it)
                                        log("add mastery increase: $it")
                                    }
                                }
                            }
                        }

                    }
                }


                ref = "spells"
                if (jCharacter.has(ref)) {
                    val jspells = jCharacter.getJSONArray(ref)
                    for (i in 0 until jspells.length()) {
                        val jitem = jspells.getJSONObject(i)

                        spells.add(
                            SpellDated(
                                spell = SpellSplittermond().apply {
                                    var rref = "name"
                                    if (jitem.has(rref)) {
                                        name = jitem.getString(rref)
                                    }
                                    rref = "school"
                                    if (jitem.has(rref)) {
                                        school = jitem.getString(rref)
                                    }
                                    rref = "schoolGrade"
                                    if (jitem.has(rref)) {
                                        level = jitem.getInt(rref)
                                    }
                                    rref = "difficulty"
                                    if (jitem.has(rref)) {
                                        difficulty = jitem.getString(rref)
                                    }
                                    rref = "focus"
                                    if (jitem.has(rref)) {
                                        focus = jitem.getString(rref)
                                    }
                                    rref = "castDuration"
                                    if (jitem.has(rref)) {
                                        castDuration = jitem.getString(rref)
                                    }
                                    rref = "spellDuration"
                                    if (jitem.has(rref)) {
                                        spellDuration = jitem.getString(rref)
                                    }
                                    rref = "castRange"
                                    if (jitem.has(rref)) {
                                        castRange = jitem.getString(rref)
                                    }
                                    rref = "enhancement"
                                    if (jitem.has(rref)) {
                                        enhancement = jitem.getString(rref)
                                    }
                                    rref = "page"
                                    if (jitem.has(rref)) {
                                        page = jitem.getString(rref)
                                    }
                                    rref = "longDescription"
                                    if (jitem.has(rref)) {
                                        description = jitem.getString(rref)
                                    }
                                    rref = "enhancementDescription"
                                    if (jitem.has(rref)) {
                                        enhancementDescription = jitem.getString(rref)
                                    }
                                    rref = "enhancementOptions"
                                    if (jitem.has(rref)) {
                                        enhancementOptions = jitem.getString(rref).split(", ")
                                    }
                                    ref = "id"
                                    if (jitem.has(ref)) {
                                        this.genesisId = jitem.getString(ref)
                                    }
                                }.also {
                                    log("parsed json spell: ${it.name}")

                                }
                            )

                        )
                    }
                }

                ref = "powers"
                if (jCharacter.has(ref)) {
                    val jpowers = jCharacter.getJSONArray(ref)
                    for (i in 0 until jpowers.length()) {
                        val jitem = jpowers.getJSONObject(i)

                        powers.add(
                            Power().apply {

                                var rref = "name"
                                if (jitem.has(rref)) {
                                    name = jitem.getString(rref)
                                }
                                ref = "id"
                                if (jitem.has(ref)) {
                                    this.genesisId = jitem.getString(ref)
                                }
                                ref = "count"
                                if (jitem.has(ref)) {
                                    this.count = jitem.getInt(ref)
                                }
                                ref = "longDescription"
                                if (jitem.has(ref)) {
                                    this.description = jitem.getString(ref)
                                }
                                buff = Translators.getSplittermondPower(
                                    powerref = genesisId, count = count, context = context
                                )
                            }.also {
                                log("parsed json power: ${it.name}")
                            }
                        )
                    }
                }

                ref = "attributes"
                if (jCharacter.has(ref)) {
                    val jattributes = jCharacter.getJSONArray(ref)
                    for (i in 0 until jattributes.length()) {
                        val jattr = jattributes.getJSONObject(i)

                        var add: Boolean

                        val vnp = ValueNamePair().apply {

                            var rref = "name"
                            if (jattr.has(rref)) {
                                name = jattr.getString(rref)
                            }
                            add = name in listOf(
                                "Ausstrahlung",
                                "Beweglichkeit",
                                "Intuition",
                                "Konstitution",
                                "Mystik",
                                "Stärke",
                                "Verstand",
                                "Willenskraft"
                            )

                            ref = "id"
                            if (jattr.has(ref)) {
                                this.genesisId = jattr.getString(ref)
                            }
                            ref = "startValue"
                            if (jattr.has(ref)) {
                                value = jattr.getInt(ref)
                            }
                            ref = "value"
                            if (jattr.has(ref)) {
                                val current = jattr.getInt(ref)
                                if (add && current > value) {
                                    attrIncreases.add(
                                        this.copy(value = current).also {
                                            log("parsed json increased attribute: ${it.name}, ${it.value}")
                                        }
                                    )
                                }
                            }
                        }

                        if (add) {
                            attributesInit.add(vnp
                                .also {
                                    log("parsed json attribute initial: ${it.name}, ${it.value}")
                                }
                            )
                        }
                    }
                }



                listOf("meleeWeapons", "longRangeWeapons").forEach { ref ->

                    if (jCharacter.has(ref)) {
                        val jmweapons = jCharacter.getJSONArray(ref)
                        for (i in 0 until jmweapons.length()) {
                            val jmweapon = jmweapons.getJSONObject(i)

                            weapons.add(
                                Weapon().apply {

                                    var rref = "name"
                                    if (jmweapon.has(rref)) {
                                        name = jmweapon.getString(rref).replace(",", "")
                                    }
                                    rref = "skill"
                                    if (jmweapon.has(rref)) {
                                        skill = jmweapon.getString(rref)
                                    }
                                    rref = "attribute1Id"
                                    if (jmweapon.has(rref)) {
                                        attributes.add(translate(jmweapon.getString(rref)))
                                    }
                                    rref = "attribute2Id"
                                    if (jmweapon.has(rref)) {
                                        attributes.add(translate(jmweapon.getString(rref)))
                                    }
                                    rref = "damage"
                                    if (jmweapon.has(rref)) {
                                        damage = jmweapon.getString(rref)
                                    }
                                    rref = "weaponSpeed"
                                    if (jmweapon.has(rref)) {
                                        wgs = jmweapon.getInt(rref)
                                    }
                                    rref = "range"
                                    if (jmweapon.has(rref)) {
                                        range = jmweapon.getInt(rref)
                                    }
                                    rref = "features"
                                    if (jmweapon.has(rref)) {
                                        val jfeatures = jmweapon.getJSONArray(rref)
                                        for (i in 0 until jfeatures.length()) {
                                            val jmastery = jfeatures.getJSONObject(i)

                                            Mastery(skillName = "").apply {
                                                var mref = "name"
                                                if (jmastery.has(mref)) {
                                                    name = jmastery.getString(mref).replace(Regex("\\d"), "")

                                                }
                                                mref = "level"
                                                if (jmastery.has(mref)) {
                                                    level = jmastery.getInt(mref)
                                                }
                                                mref = "description"
                                                if (jmastery.has(mref)) {
                                                    description = jmastery.getString(mref)
                                                        .let { if (it == "null") "" else it }
                                                }
                                                mref = "page"
                                                if (jmastery.has(mref)) {
                                                    page = jmastery.getString(mref)
                                                }
                                            }.let {
                                                features.add(it)
                                                log("add feature: '$it' to weapon $name")
                                            }
                                        }
                                    }

                                }.also {
                                    log("parsed json weapon: ${it.name}")
                                }
                            )
                        }
                    }
                }
                listOf("armors", "shields").forEach { ref ->

                    if (jCharacter.has(ref)) {
                        val jarmors = jCharacter.getJSONArray(ref)
                        for (i in 0 until jarmors.length()) {
                            val jarmor = jarmors.getJSONObject(i)

                            armors.add(
                                Armor().apply {

                                    var rref = "name"
                                    if (jarmor.has(rref)) {
                                        name = jarmor.getString(rref).replace(",", "")
                                    }
                                    rref = "defense"
                                    if (jarmor.has(rref)) {
                                        verteidigung = jarmor.getInt(rref)
                                    }
                                    rref = "damageReduction"
                                    if (jarmor.has(rref)) {
                                        damageReduction = jarmor.getInt(rref)
                                    }
                                    rref = "handicap"
                                    if (jarmor.has(rref)) {
                                        behinderung = jarmor.getInt(rref)
                                    }
                                    rref = "tickMalus"
                                    if (jarmor.has(rref)) {
                                        tik = jarmor.getInt(rref)
                                    }
                                    rref = "features"
                                    if (jarmor.has(rref)) {
                                        val jfeatures = jarmor.getJSONArray(rref)
                                        for (i in 0 until jfeatures.length()) {
                                            val jmastery = jfeatures.getJSONObject(i)

                                            Mastery(skillName = "").apply {
                                                var mref = "name"
                                                if (jmastery.has(mref)) {
                                                    name = jmastery.getString(mref).replace(Regex("\\d"), "")

                                                }
                                                mref = "level"
                                                if (jmastery.has(mref)) {
                                                    level = jmastery.getInt(mref)
                                                }
                                                mref = "description"
                                                if (jmastery.has(mref)) {
                                                    description = jmastery.getString(mref)
                                                        .let { if (it == "null") "" else it }
                                                }
                                                mref = "page"
                                                if (jmastery.has(mref)) {
                                                    page = jmastery.getString(mref)
                                                }
                                            }.let {
                                                features.add(it)
                                                log("add feature: '$it' to armor $name")
                                            }
                                        }
                                    }

                                }.also {
                                    log("parsed json armor: ${it.name}")
                                }
                            )
                        }
                    }
                }


            } catch (e: Exception) {
                loge("error parsing json during import splittermond character")
                e.printStackTrace()
                c = null
            }
        }

        return SplittermondGenesisParserResult(
            character = c,
            attributesInitial = attributesInit,
            attributesIncreases = attrIncreases,
            skillsIncreases = listOf(),
            skillsInitial = skillsInit,
            masteryIncreases = masteryIncreases,
            languagesInitial = languages,
            languagesIncreases = listOf(),
            resourcesInitial = resourcesInitial.values.toList().filter { it.value > 0 },
            resourceIncreases = resourcesIncrease.filter { it.value > 0 },
            spells = spells,
            rewards = rewards,
            powers = powers,
            weapons = weapons,
            armors = armors
        ).also { log(
            "result character = ${it.character}, \n" +
                    "initAttributes = ${it.attributesInitial.joinToString()}, \n" +
                    "increaseAttributes = ${it.attributesIncreases.joinToString()}, \n" +
                    "skills = ${it.skillsInitial.joinToString()}, \n" +
                    "masteryIncreases = ${it.masteryIncreases.joinToString()}, \n" +
                    "languages: ${it.languagesInitial.joinToString()}, \n" +
                    "buffs: ${c?.buffs?.joinToString()}, \n" +
                    "resources ${it.resourcesInitial.joinToString()}, \n" +
                    "resourcesIncreased ${it.resourceIncreases.joinToString()}, \n" +
                    "spells: ${it.spells.joinToString()}, \n" +
                    "rewards: ${it.rewards.joinToString()}",
        ) }
    }
    fun parseSplittermondCharFromGenesisXml(
        fileUri: Uri, context: Context,
        language_code: String = PrefsHelper.LANGUAGE_CODE_DE
    ): SplittermondGenesisParserResult {



        log("##################################### start parseSplittermondCharFromGenesis")

        var c: Character? = Character()
        var r: Reward? = null

        var dspell: SpellDated? = null
        var skill: ValueNamePair? = null
        val attributesInit = mutableListOf<ValueNamePair>()
        val attrIncreases = mutableListOf<ValueNamePair>()
        val skillsInit = HashMap<String, ValueNamePair>()
        val skillIncreases = mutableListOf<ValueNamePair>()
        val masteryIncreases = mutableListOf<Mastery>()
        val languages = mutableListOf<String>()
        val languagesIncreases = mutableListOf<ValueNamePair>()
        val resourcesInitial = HashMap<String, ValueNamePair>()
        val resourcesIncrease = mutableListOf<ValueNamePair>()
        val spells = HashMap<String, SpellDated>()
        val powers = mutableListOf<Power>()
        val rewards = mutableListOf<Reward>()

        val GenesisDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val PATTERN_SKILL_NAME = Pattern.compile("([^\\s\\d]+)(\\d*)")


        var isr: InputStreamReader? = null
        try {
            isr = InputStreamReader(context.contentResolver.openInputStream(fileUri))
            log("successfully initiated InputStreamReader to file path")
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        //--------------- initiate parsing xml ----------------//

        val factory: XmlPullParserFactory
        var xpp: XmlPullParser
        try {
            factory = XmlPullParserFactory.newInstance()
            xpp = factory.newPullParser()
            log("XML Parser initiated to $fileUri")
            xpp.setInput(isr)
            xpp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)

            /*
            Start Import
             */
            try {
                isr = InputStreamReader(context.contentResolver.openInputStream(fileUri))
                log("successfully initiated InputStreamReader to file path")
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            xpp = factory.newPullParser()
            log("XML Parser initiated second time")
            xpp.setInput(isr)

            xpp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)


            var text: String? = null

            var eventType = xpp.eventType

            while (eventType != XmlPullParser.END_DOCUMENT) {

                log("############################## NEUE RUNDE #####################################")
                val tagName: String? = xpp.name
                log("eventType = $eventType, tagName = $tagName")

                when (eventType) {

                    /////////////////////////////////////////
                    /////////////////////////////////////////
                    //////// EVENT TYPE : START TAG /////////
                    /////////////////////////////////////////
                    /////////////////////////////////////////

                    XmlPullParser.START_TAG -> {

                        log("EVENT TYPE START TAG ----------- tagName =$tagName")
                        try {


                            /*
                         * Log Entry (create new LogEntry e) and get attribute created at
                         * also create empty lists for tags and characters
                         */
                            when {
                                tagName.equals("splimochar", ignoreCase = true) -> {
                                    c = Character().apply {
                                        isActive = true
                                    }
                                    xpp.getAttributeValue("", "race")?.let {
                                        c.race = Race(it)
                                        c.size = Translators.getSplittermondSize(it)
                                    }
                                    xpp.getAttributeValue("", "edu")
                                        ?.let { c.archetype = DndArchetype(it, "") }
                                    log("character: $c")

                                }

                                tagName.equals("attr", ignoreCase = true) -> {
                                    val stat =
                                        translate(xpp.getAttributeValue("", "id").lowercase())

                                    val initValue =
                                        xpp.getAttributeValue("", "start").toIntOrNull() ?: 0
                                    val cValue =
                                        xpp.getAttributeValue("", "value").toIntOrNull() ?: 0
                                    attributesInit.add(
                                        ValueNamePair(
                                            name = stat,
                                            value = initValue
                                        )
                                    )
                                    log("attribute: $stat, $initValue, $cValue")
                                }

                                tagName.equals("skillval", ignoreCase = true) -> {
                                    val name = translate(
                                        getSkillName(
                                            xpp.getAttributeValue("", "skill").lowercase()
                                        )
                                    )
                                    log("skill $name")
                                    var value = 0
                                    try {
                                        value = xpp.getAttributeValue("", "val").toIntOrNull() ?: 0
                                    } catch (e: Exception) {
                                        log("skill $name has no value")
                                    }
                                    if (value > 0) {
                                        skillsInit[name] =
                                            ValueNamePair(name = name, value = value).
                                            also { skill = it }
                                        log("skill $name, $value")
                                    }
                                }

                                tagName.equals("languageref", ignoreCase = true) -> {
                                    val name = xpp.getAttributeValue("", "ref").lowercase()
                                    languages.add(name)
                                    log("language: $name")
                                }

                                tagName.equals("powerref", ignoreCase = true) -> {
                                    val ref = xpp.getAttributeValue("", "ref").lowercase()
                                    val count =
                                        xpp.getAttributeValue("", "count").toIntOrNull() ?: 0

                                    powers.add(
                                        Power(
                                            name = ref,
                                            count = count,
                                            buff = Translators.getSplittermondPower(
                                                powerref = ref, count = count, context = context
                                            )
                                        )

                                            .also { log("Power: $it") }
                                    )
                                }

                                tagName.equals("resourceref", ignoreCase = true) -> {
                                    val ref = translate(xpp.getAttributeValue("", "ref").lowercase())
                                    var value = 0
                                    try {
                                        value = xpp.getAttributeValue("", "val").toIntOrNull() ?: 0
                                    } catch (e: Exception) {
                                        log("$tagName: resource $ref has no value")
                                    }

                                    var description = ""
                                    try {
                                        description = xpp.getAttributeValue("", "description")
                                    } catch (e: Exception) {
                                        log("resource $ref has no description")
                                    }

                                    if (value > 0) {
                                        resourcesInitial[ref] =
                                            ValueNamePair(
                                                name = ref, value = value, description = description
                                            )

                                    }
                                    log("!!!!!!!!!!!!!!! $tagName: resource: $ref, $value, descr: '$description'")
                                }

                                tagName.equals("spellval", ignoreCase = true) -> {
                                    val name = translate(xpp.getAttributeValue("", "spell").lowercase())
                                    log("spell: $name ")

                                    val school = translate(
                                        getSkillName(
                                            xpp.getAttributeValue("", "school").lowercase()
                                        )
                                    )

                                    var free = 0
                                    try {
                                        free = xpp.getAttributeValue("", "free").toIntOrNull() ?: 0
                                    } catch (e: Exception) {
                                        log("$tagName: no attribute free found")
                                    }
                                    log("spell: $name [$school], free = $free ")

                                    spells[name] =
                                        SpellDated(
                                            spell = SpellSplittermond(
                                                name = name,
                                                school = school
                                            ),
                                            free = free
                                        )



                                }

                                tagName.equals("reward", ignoreCase = true) -> {
                                    val sDate =
                                        xpp.getAttributeValue("", "date").substringBefore(".")
                                    log("reward:: parsed date: $sDate")
                                    val date =
                                        GenesisDateFormat.parse(sDate)
                                    val exp = xpp.getAttributeValue("", "exp").toIntOrNull() ?: 0
                                    log("reward:: exp: $exp")
                                    val createdAt = date?.let { Global.MY_DATE_FORMAT.format(it) } ?: ""
                                    log("reward:: createdAt: $createdAt")

                                    r = Reward(logEntry = LogEntry(exp = exp).apply { created_at = createdAt })

                                }

                                tagName.equals("attrmod", ignoreCase = true) -> {
                                    val stat =
                                        translate(xpp.getAttributeValue("", "attr").lowercase())
                                    val sDate =
                                        xpp.getAttributeValue("", "date").substringBefore(".")
                                    val date =
                                        GenesisDateFormat.parse(sDate)
                                    val exp = xpp.getAttributeValue("", "expCost").toIntOrNull() ?: 0
                                    val value = xpp.getAttributeValue("", "val").toIntOrNull() ?: 0

                                    log("increase attr: $stat $value, exp = $exp, date = $date")

                                    attrIncreases.add(
                                        ValueNamePair(
                                            name = stat,
                                            value = value,
                                            description = "@Attribute",
                                            exp = exp,
                                            date = date
                                        )
                                    )
                                }

                                tagName.equals("languagemod", ignoreCase = true) -> {
                                    val sDate =
                                        xpp.getAttributeValue("", "date").substringBefore(".")
                                    val date =
                                        GenesisDateFormat.parse(sDate)
                                    val exp = xpp.getAttributeValue("", "expCost").toIntOrNull() ?: 0
                                    val ref =
                                        xpp.getAttributeValue("", "ref")
                                    languagesIncreases.add(
                                        ValueNamePair(
                                            name = ref,
                                            exp = exp,
                                            date = date
                                        )
                                    )
                                }

                                tagName.equals("skillmod", ignoreCase = true) -> {
                                    val stat = translate(getSkillName(
                                        xpp.getAttributeValue("", "ref").lowercase()))
                                    val sDate =
                                        xpp.getAttributeValue("", "date").substringBefore(".")
                                    val date =
                                        GenesisDateFormat.parse(sDate)
                                    val exp = xpp.getAttributeValue("", "expCost").toIntOrNull() ?: 0
                                    val value = xpp.getAttributeValue("", "value").toIntOrNull() ?: 0

                                    skillIncreases.add(
                                        ValueNamePair(
                                            name = stat,
                                            value = value,
                                            exp = exp,
                                            date = date,
                                            description = "@Skill"
                                        )
                                    )
                                }

                                tagName.equals("spellmod", ignoreCase = true) -> {
                                    val sDate =
                                        xpp.getAttributeValue("", "date").substringBefore(".")
                                    val date =
                                        GenesisDateFormat.parse(sDate)
                                    var exp = 0
                                    try {
                                        exp = xpp.getAttributeValue("", "expCost").toIntOrNull() ?: 0
                                    } catch (e: Exception) {
                                        log("a spell was added on $sDate, but it cost no exp")
                                    }

                                    dspell = date?.let { SpellDated(date = it, exp = exp) }
                                }

                                tagName.equals("spell", ignoreCase = true) -> {
                                    dspell?.let { sd ->
                                        sd.spell.apply {
                                            school = translate(
                                                getSkillName(xpp.getAttributeValue("", "school")))
                                            name = translate(xpp.getAttributeValue("", "spell"))
                                            try {
                                                level = xpp.getAttributeValue("", "free").toIntOrNull() ?: -1
                                            } catch (e: Exception) {
                                                log("spell $name has no level (attribute free)")
                                            }
                                        }
                                        try {
                                            sd.free = xpp.getAttributeValue("", "free").toIntOrNull() ?: 0
                                        } catch (e: Exception) {
                                            log ("a spell was added, but was free")
                                        }
                                        spells[sd.spell.name]?.apply {
                                            date = sd.date
                                            exp = sd.exp
                                        }
                                    }
                                }

                                tagName.equals("resourcemod", ignoreCase = true) -> {
                                    /*
                                    when resourcemod has no date, it is part of a reward,
                                    where the date is already stored in the reward
                                    this resourcemod is store twice in xml
                                    once in history and once in reward.
                                    We can distinguish those two by depth (4 for rewards, 3 for increases)
                                     */
                                    var date: Date? = null
                                    try {
                                        val sDate = xpp.getAttributeValue("", "date")
                                                .substringBefore(".")
                                        date = GenesisDateFormat.parse(sDate)
                                    } catch (e: Exception) {
                                        log("resourcemod: has no date -> it comes from reward. depth = ${xpp.depth}")
                                    }




                                        val ref =
                                            translate(xpp.getAttributeValue("", "ref").lowercase())
                                        // assume type is always relative
                                        var type = ""
                                        try {
                                            type = xpp.getAttributeValue("", "type").lowercase()
                                        } catch (e: Exception) {
                                            log("$tagName has no type. so assume default value: relative")
                                        }
                                        val value =
                                            xpp.getAttributeValue("", "value").toIntOrNull() ?: 0
                                        var exp = 0
                                        try {
                                            exp = xpp.getAttributeValue("", "expCost").toIntOrNull() ?: 0
                                        } catch (e: Exception) {
                                            log("$tagName has no exp. dont worry if it is a resourcemod from a reward")
                                        }

                                    if (value > 0) {
                                        if (date != null) {

                                            resourcesIncrease.find { it.name == ref && it.date == date }
                                                ?.also {
                                                    it.value += value
                                                    it.exp += exp
                                                } ?: run {
                                                resourcesIncrease.add(
                                                    ValueNamePair(
                                                        name = ref,
                                                        value = value,
                                                        date = date,
                                                        exp = exp
                                                    )
                                                )
                                                log("resourcemod: resource increased: $ref, $value, date = $date, type = $type, depth = ${xpp.depth}")
                                            }

                                        } else {
                                            //date is null
                                            if (xpp.depth == 5) {
                                                //is is part of reward so we add data to reward
                                                r?.resourcesGained?.add(
                                                    ValueNamePair(
                                                        name = ref,
                                                        value = value,
                                                        date = date,
                                                        exp = exp
                                                    )
                                                )?.also {
                                                    log("$tagName: resources gained for reward ${r?.logEntry?.text}: ${r?.resourcesGained?.size} total resource size:: resources: ${r?.getModificationText()}")
                                                }
                                            }
                                        }
                                    }
                                }

                                tagName.equals("masterref", ignoreCase = true) -> {
                                    var free = 0
                                    var spec: String? = null
                                    var ref: String? = null
                                    try {
                                        free = xpp.getAttributeValue("", "free").toIntOrNull() ?: 0
                                    } catch (e: Exception) {
                                        log ("masterref: no free found")
                                    }
                                    try {
                                        spec = xpp.getAttributeValue("", "spec").lowercase()
                                    } catch (e: Exception) {
                                        log ("masterref: no spec found")
                                    }
                                    try {
                                        ref = xpp.getAttributeValue("", "ref").lowercase()
                                    } catch (e: Exception) {
                                        log ("masterref: no ref found")
                                    }
                                    log("masterref: free = $free")
                                    log("masterref: spec = $spec")
                                    log("masterref: ref = $ref")
                                    val x = ref ?: spec ?: ""
                                    var level = free
                                    val skName = translate(getSkillName(x.substringBefore("/")))

                                    log("masterref: skillname = $skName")
                                    var specName = translate(x.substringAfter("/"))

                                    PATTERN_SKILL_NAME.matcher(specName).let { m ->
                                        if (m.find()) {
                                            m.group(2)?.toIntOrNull()?.let { level = it }
                                        }
                                    }
                                    log("masterref: specname = $specName")
                                    log("masterref: level = $level")


                                    skill?.let { sk ->
                                        if (sk.name.equals(skName, ignoreCase = true) && free > 0) {
                                            /*
                                             dont add masteries that are not free,
                                             because the are added via mastermod (history)
                                             */
                                            sk.masteries.add(
                                                Mastery(
                                                    name = specName,
                                                    level = level,
                                                    free = free,
                                                    skillName = skName
                                                )
                                            )
                                        } else {
                                            loge("masterref: Skill mastery $spec doesn't match " +
                                                    "skill name $sk.name! " +
                                                    "Mastery will be dropped")
                                        }
                                    }

                                    log("masterref: skill mastery: $spec, free = $free")
                                }

                                tagName.equals("mastermod", ignoreCase = true) -> {

                                    log("$tagName: start parsing")

                                    var ref = ""
                                    try {
                                        ref = xpp.getAttributeValue("", "ref")
                                        log("$tagName: ref from 'ref' attribute $ref")
                                    } catch (e: Exception) {
                                        log("$tagName: no attribute ref found -> maybe is spec?")
                                    }
                                   var spec = ""
                                    try {
                                        spec = xpp.getAttributeValue("", "spec")
                                        log("$tagName: spec from 'spec' attribute $spec")
                                    } catch (e: Exception) {
                                        loge("$tagName: no attribute 'spec' found either")
                                    }


                                    var level = 0
                                    try {
                                        level =
                                            xpp.getAttributeValue("", "level").toIntOrNull() ?: 0
                                    } catch (e: Exception) {
                                        log("mastermod: no level found")
                                    }
                                    log("$tagName: level from 'level' attribute: $level")
                                    if (spec.isNotBlank()) {
                                        spec.substringAfterLast("/").toIntOrNull()?.let { level = it}
                                        log("$tagName:got level from spec : $level")

                                    }


                                    val sDate =
                                        xpp.getAttributeValue("", "date").substringBefore(".")
                                    log("$tagName: date as string $sDate")
                                    val date =
                                        GenesisDateFormat.parse(sDate)

                                    var exp = 0
                                    try {
                                        exp =
                                            xpp.getAttributeValue("", "expCost").toIntOrNull() ?: 0
                                    } catch (e: Exception) {
                                        log("mastermod: no exp found")
                                    }
                                    log("$tagName: exp $exp")
                                    if (level == 0 && exp > 0) {
                                        level = exp / 5
                                    }
                                    log("$tagName: level after maybe setting via exp: $level")



                                    val skillName = translate(getSkillName(xpp.getAttributeValue("", "skill")))

                                    log("$tagName: skillname $skillName")
                                    var name = ""
                                    if (ref.isNotBlank()) {
                                        name = translate(ref.substringAfter("/"))
                                    }
                                    if (spec.isNotBlank()) {
                                        name = translate(spec.substringAfter("/").substringBefore("/").lowercase())
                                    }

                                    PATTERN_SKILL_NAME.matcher(name).let { m ->
                                        if (m.find()) {
                                            m.group(2)?.toIntOrNull()?.let { level = it }
                                        }
                                    }
                                    log("$tagName: name $name")
                                    log("$tagName: level after extracting from masteryname: $level")
                                    if (level == 0) level = 1
                                    log("$tagName: level after setting to 1 as default : $level")




                                    log("mastermod: skill mastery: ref = '$ref', spec = '$spec', skill = $skillName, exp = $exp, " +
                                            "level = $level, date = $date")

                                    masteryIncreases.add(
                                        Mastery(
                                            name = name,
                                            level = level,
                                            date = date,
                                            skillName = skillName,
                                            exp = exp,
                                        )
                                    )
                                }




                                else -> {}
                                /*
                                 * Session (create new Session s) and get attributes created at & name
                                 */
                            }
                        } catch (e: Exception) {
                            loge("Error at ${xpp.name}")
                            e.printStackTrace()
                        }
                    }

                    /////////////////////////////////////////
                    /////////////////////////////////////////
                    ///////// EVENT TYPE : TEXT /////////////
                    /////////////////////////////////////////
                    /////////////////////////////////////////

                    XmlPullParser.TEXT -> {
                        log("EVENT TYPE TEXT ----------- BORING: tagName =$tagName text = ${xpp.text}")
                        text = xpp.text

                        try {

                        } catch (e: Exception) {
                            loge("error at $tagName")
                        }

                    }

                    /////////////////////////////////////////
                    /////////////////////////////////////////
                    ////////// EVENT TYPE : END TAG /////////
                    /////////////////////////////////////////
                    /////////////////////////////////////////

                    XmlPullParser.END_TAG -> {

                        log("EVENT TYPE END TAG ----------- tagName =$tagName")

                        when {
                            tagName.equals("name", ignoreCase = true) -> {
                                text?.let { c?.name = it }
                                log("name: $text")
                            }
                            tagName.equals("title", ignoreCase = true) -> {
                                text?.let { r?.logEntry?.text = it }
                                log("reward title: $text")
                                if (text == "Erschaffung") {
                                    r?.logEntry?.dateOfCreation?.let {
                                        c?.created_at = Global.MY_DATE_FORMAT.format(it)
                                    }
                                }
                            }
                            tagName.equals("reward", ignoreCase = true) -> {
                                if (r != null) {
                                    rewards.add(r.copy())
                                    log("added reward $r")
                                    r = null
                                } else {
                                    loge("reward end tag: no reward found to add.")
                                }
                            }
                        }

                    }


                }
                try {
                    log("next Event plz")
                    eventType = xpp.next()
                } catch (e1: IOException) {
                    loge("cant get next event")
                    e1.printStackTrace()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
            loge("Error parsing Splittermond Character")
        }
        log("##################### DONE PARSING #########################################")

        //get the initial values of skills and resources.
        //They are not in the xml...

        //for skills increase values are absolute
        skillIncreases.forEach { sInc ->
            skillsInit[sInc.name]?.apply {
                value = min(value, sInc.value - 1)
            }
        }
        //here increase values are relative!
        resourcesIncrease.forEach { rInc ->
            resourcesInitial[rInc.name]?.apply {
                value -= rInc.value
                log("resourceref: reduced resource ${rInc.name} by ${rInc.value} because initial value is not stored in xml. new value: $value. reason: resource mod")
            }
        }
        //here increase values are relative! (resources gained by rewards)
        rewards.map { it.resourcesGained }.flatten().forEach { r ->
            resourcesInitial[r.name]?.apply {
                value -= r.value
                log("resourceref: reduced resource ${r.name} by ${r.value} because initial value is not stored in xml. new value: $value. reason: resource mod")

            }
        }

        /*
         remove all languages that have been gained in language mod,
         so they don't appear twice in the languages list
         */
        languages.removeAll { languagesIncreases.map { it.name }.contains(it) }

        skillsInit.values.forEach{ s ->
            val remove = mutableListOf<Mastery>()
            s.masteries.forEach { m ->
                masteryIncreases.find { mInc ->
                    mInc.skillName.equals(s.name, ignoreCase = true) &&
                            mInc.name.equals(m.name, ignoreCase = true) &&
                            mInc.level == m.level
                }?.let {
                    remove.add(m)
                    log("masterref: remove mastery ${it.skillName} / ${it.name} (${it.level}) from initial skills")
                }
            }
            remove.forEach { s.masteries.remove(it) }
        }

        return SplittermondGenesisParserResult(
            character = c,
            attributesInitial = attributesInit,
            attributesIncreases = attrIncreases,
            skillsIncreases = skillIncreases.filter { it.value > 0 },
            skillsInitial = skillsInit.values.toList().filter { it.value > 0 },
            masteryIncreases = masteryIncreases,
            languagesInitial = languages,
            languagesIncreases = languagesIncreases,
            resourcesInitial = resourcesInitial.values.toList().filter { it.value > 0 },
            resourceIncreases = resourcesIncrease.filter { it.value > 0 },
            spells = spells.values.toList(),
            rewards = rewards,
            powers = powers,
            weapons = listOf(),
            armors = listOf()
        ).also { log(
            "result character = ${it.character}, \n" +
                    "initAttributes = ${it.attributesInitial.joinToString()}, \n" +
                    "increaseAttributes = ${it.attributesIncreases.joinToString()}, \n" +
                    "skills = ${it.skillsInitial.joinToString()}, \n" +
                    "masteryIncreases = ${it.masteryIncreases.joinToString()}, \n" +
                    "languages: ${it.languagesInitial.joinToString()}, \n" +
                    "increaseLanguages = ${it.languagesIncreases.joinToString()}, \n" +
                    "buffs: ${c?.buffs?.joinToString()}, \n" +
                    "resources: ${it.resourcesInitial.joinToString()}, \n" +
                    "resourcesIncreased: ${it.resourceIncreases.joinToString()}, \n" +
                    "spells: ${it.spells.joinToString()}, \n" +
                    "logsWithDate: ${it.rewards.joinToString()}",
        ) }

    }
}