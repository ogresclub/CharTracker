package com.mtths.chartracker.dialogs

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import com.mtths.chartracker.Global
import com.mtths.chartracker.utils.Helper.getKey
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.R
import com.mtths.chartracker.widgets.SquareImageView
import java.util.*

/**
 * Created by mtths on 15.04.17.
 */
class DialogCharIcon : DialogFragment() {
    private lateinit var gridView: GridView
    private var imageIDs = ArrayList<Int>()
    private var iconMap = Global.charIconsMap
    var mOnCharIconChangedListener: OnCharIconChangedListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_char_icon, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        gridView = root as GridView
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageIDs.addAll(iconMap.values)
        context?.let { contxt ->
            gridView.adapter = ImageAdapter(contxt)
        }
        implementSetCharIcon()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        /*
        This makes sure that the container activity has implemented
        the callback interface. If not, it throws an exception
        */
        mOnCharIconChangedListener = try {
            context as OnCharIconChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnCharIconChangedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnCharIconChangedListener = null
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindDrawables(gridView)
    }

    ////////////////////////////////////// PRIVATE METHODS /////////////////////////////////////////
    private fun implementSetCharIcon() {
        gridView.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val image_id = imageIDs[position]
            val imageName = getKey(iconMap, image_id)
            imageName?.let {  mOnCharIconChangedListener?.onCharIconChanged(it)}
            dismiss()
        }
    }

    //////////////////////////////////// CLASSES //////////////////////////////////////////////////
    inner class ImageAdapter(private val context: Context) : BaseAdapter() {
        //---returns the number of images---
        override fun getCount(): Int {
            return imageIDs.size
        }

        //---returns the ID of an item---
        override fun getItem(position: Int): Any {
            return imageIDs[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        //---returns an ImageView view---
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val convertView = convertView
            val imageView: SquareImageView
            if (convertView == null) {
                imageView = SquareImageView(context)
                imageView.layoutParams = AbsListView.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT)
                imageView.setPadding(2, 2, 2, 2)
                imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            } else {
                imageView = convertView as SquareImageView
            }

            imageView.setImageResource(imageIDs[position])
            return imageView
        }

    }

    /////////////////////////////////// PRIVATE METHODS ////////////////////////////////////////////
    //To free up memory taken by adapterViews and others
    private fun unbindDrawables(view: View?) {
        if (view?.background != null) view.background.callback = null
        if (view is ImageView) {
            view.setImageBitmap(null)
        } else if (view is ViewGroup) {
            for (i in 0 until view.childCount) unbindDrawables(view.getChildAt(i))
            if (view !is AdapterView<*>) view.removeAllViews()
        }
    }

    //////////////////////////// INTERFACE /////////////////////////////////////////////////////////
    interface OnCharIconChangedListener {
        fun onCharIconChanged(newIconName: String, isCustomIcon: Boolean = false)
    }

    companion object {
        fun newInstance(): DialogCharIcon {
            log("Dialog new Char Icon new Instance called")
            val args = Bundle()
            val fragment = DialogCharIcon()
            fragment.arguments = args
            return fragment
        }
    }
}