package com.mtths.chartracker.dialogs

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.DialogFragment
import com.mtths.chartracker.databinding.DialogDamageMonitorBinding
import com.mtths.chartracker.dataclasses.DamageMonitorData
import com.mtths.chartracker.fragments.FragmentDamageMonitor

class DialogDamageMonitor: DialogFragment() {

    private var _binding: DialogDamageMonitorBinding? = null
    private val binding get() = _binding!!


    companion object {

        private const val ARG_ID_DATA = "id_data"
        private const val ARG_ID_HEADER = "id_header"

        const val TAG = "dialog_damage_monitor"
        fun newInstance(
            data: ArrayList<DamageMonitorData>,
            header: String
        ): DialogDamageMonitor {
            val fragment = DialogDamageMonitor()
            val args = Bundle()
            args.putParcelableArrayList(ARG_ID_DATA, data)
            args.putString(ARG_ID_HEADER, header)
            fragment.arguments = args
            return fragment
        }
    }

    var data: ArrayList<DamageMonitorData>? = null
    var header: String? = null

    private fun setParams() {

        arguments?.let {args ->
            data = when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU ->
                    args.getParcelableArrayList(ARG_ID_DATA, DamageMonitorData::class.java)
                else -> args.getParcelableArrayList(ARG_ID_DATA)
            }
            header = args.getString(ARG_ID_HEADER)


        }
    }

    private fun setHeader() {
        binding.header.text = header
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        _binding = DialogDamageMonitorBinding.inflate(inflater, container, false)
        val view = binding.root
        setParams()
        setHeader()
        childFragmentManager.beginTransaction().apply {

        data?.forEachIndexed { index, data ->

            val invIndex = (this@DialogDamageMonitor.data?.size ?: 0) - index
            /*
            for each DamageMonitorData create FrameLayout and then replace it with fragment
             */
            binding.fragmentContainer.addView(
                FrameLayout(requireContext()).apply {
                    layoutParams = FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                    )
                    this.id = invIndex
                }
            )
            replace(
                invIndex,
                FragmentDamageMonitor.newInstance(
                    data = data,
                    isDialog = true,
                    inverseIndex = invIndex
                )
            )

            }

            commit()

        }
        // You can add your fragment here

        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}