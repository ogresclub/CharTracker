package com.mtths.chartracker.dialogs

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.mtths.chartracker.*
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.adapters.SettingsAdapter
import com.mtths.chartracker.dialogs.DialogChangeCharacterStatus.OnCharStatusChangedListener
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper

class DialogSettingsChar : DialogFragment() {
    private lateinit var exp_point_cost_header: TextView
    private lateinit var exp_point_cost_button_container: LinearLayout
    private lateinit var button_500: Button
    private lateinit var button_300: Button
    private lateinit var button_ok: TextView
    private lateinit var button_cancel: TextView
    private lateinit var switchesContainer: LinearLayout
    private lateinit var namedEditTextsContainer: LinearLayout
    private lateinit var adapter: SettingsAdapter
    private lateinit var tasks: HashMap<Int, () -> Unit>


    var mOnCharStatusChangedListener: OnCharStatusChangedListener? = null
    lateinit var prefs: PrefsHelper
    var c: Character? = null
    var dbHelper: DatabaseHelper? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_char_settings, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        prefs = PrefsHelper(context)
        dbHelper = getInstance(context)
        arguments?.let { arguments ->
            c = dbHelper?.getCharacter(
                arguments.getLong(PARAM_CHAR_ID, 1)
            )
        }
        initInterfaceItems(view)
        tasks = SettingsAdapter.getTasks {
            //otherwise recreating activity causes crash,
            // because the listener calls activityMain.onResume
            activity?.let { activity ->
                val intent = activity.intent;
                activity.finish();
                startActivity(intent)
            }
        }
        adapter = SettingsAdapter(
            activity = requireActivity(),
            switchContainer = switchesContainer,
            namedEditTextContainer = namedEditTextsContainer,
            character = c
        ).apply {
            inflateCharSettings()
            inflateSettings(
                tasks = tasks)
        }
        implementOnClickListeners()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        /*
        This makes sure that the container activity has implemented
        the callback interface. If not, it throws an exception
        */
        mOnCharStatusChangedListener = try {
            context as OnCharStatusChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnCharStatusChangedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnCharStatusChangedListener = null
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        c?.let { mOnCharStatusChangedListener?.onCharStatusChanged(it) }
    }

    private fun initInterfaceItems(v: View) {
        button_ok = v.findViewById(R.id.button_ok)
        button_cancel = v.findViewById(R.id.button_cancel)
        exp_point_cost_header = v.findViewById(R.id.exp_point_cost_header_text_view)
        exp_point_cost_button_container = v.findViewById(R.id.exp_point_cost_buttons_container)
        button_500 = v.findViewById(R.id.button_500)
        button_300 = v.findViewById(R.id.button_300)
        active_exp_point_cost_according_to_settings()
        switchesContainer = v.findViewById(R.id.switch_container)
        namedEditTextsContainer = v.findViewById(R.id.named_edit_texts_container)
        if (!Helper.activeRules(
                allowedRules = listOf(RuleSystem.rule_system_ogres_club),
                activeRuleSystems = c?.ruleSystems ?: prefs.activeRuleSystems)
        ) {
            exp_point_cost_header.visibility = View.GONE
            exp_point_cost_button_container.visibility = View.GONE
        }

    }

    private fun active_exp_point_cost_according_to_settings() {
        if (300 == c?.ATTRIBUTE_POINT_EXP_COST) {
            activate(button_300)
        }
        if (500 == c?.ATTRIBUTE_POINT_EXP_COST) {
            activate(button_500)
        }
    }


    private fun implementOnClickListeners() {
        button_500.setOnClickListener {
            activate(button_500)
            deactivate(button_300)
            c!!.ATTRIBUTE_POINT_EXP_COST = 500
            mOnCharStatusChangedListener!!.onCharStatusChanged(c!!)
        }
        button_300.setOnClickListener {
            activate(button_300)
            deactivate(button_500)
            c!!.ATTRIBUTE_POINT_EXP_COST = 300
            mOnCharStatusChangedListener!!.onCharStatusChanged(c!!)
        }
        button_cancel.setOnClickListener {
            dismiss()
        }

        button_ok.setOnClickListener {
            c?.let { c ->
                adapter.applySettingsChanges(tasks = tasks)
                mOnCharStatusChangedListener?.onCharStatusChanged(c)
            }
            dismiss()
        }
    }

    private fun activate(b: Button?) {
        b!!.setBackgroundResource(R.drawable.button_holo_green_light)
    }

    private fun deactivate(b: Button?) {
        b!!.setBackgroundResource(R.drawable.button_light_grey)
    }

    companion object {
        const val PARAM_CHAR_ID = "param_char_id"
        fun newInstance(char_id: Long): DialogSettingsChar {
            val args = Bundle()
            args.putLong(PARAM_CHAR_ID, char_id)
            val fragment = DialogSettingsChar()
            fragment.arguments = args
            return fragment
        }
    }
}