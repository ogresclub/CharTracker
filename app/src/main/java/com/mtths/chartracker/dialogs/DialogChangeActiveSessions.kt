package com.mtths.chartracker.dialogs

import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import android.widget.TextView
import com.mtths.chartracker.*
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.adapters.SessionAdapter
import com.mtths.chartracker.dataclasses.Session
import java.util.*

class DialogChangeActiveSessions : DialogFragment() {
    //////////////////////////////////////////// VARIABLES /////////////////////////////////////////
    interface OnActiveSessionsChangedListener {
        fun onActiveSessionsChanged(active_sessions: ArrayList<Session>)
    }

    lateinit var sessionList: ListView
    lateinit var cancel_button: TextView
    lateinit var okButton: TextView
    lateinit var adapter: SessionAdapter
    var sessions = ArrayList<Session>()
    var dbHelper: DatabaseHelper? = null
    var loadAllSessinsTask: LoadAllSessionsTask? = null
    var onActiveSessionsChangedListener: OnActiveSessionsChangedListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_change_active_sessions, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dbHelper = getInstance(context)
        initInterfaceItems(view)
        implementSessionActivityToggled()
        implementOpenCreateNewSessionDialog()
        implementOkButton()
        initAdapter()
        fillAdapter()
    }

    //////////////////////////////////// PRIVATE METHODS //////////////////////////////////////////


    private fun initInterfaceItems(v: View) {
        sessionList = v.findViewById<View>(R.id.session_listView) as ListView
        cancel_button = v.findViewById<View>(R.id.cancel_button) as TextView
        okButton = v.findViewById<View>(R.id.ok_button) as TextView
    }

    private fun initAdapter() {
        adapter = SessionAdapter(requireContext(), R.layout.list_view_item_session)
        sessionList.adapter = adapter
    }

    private fun implementOkButton() {
        okButton.setOnClickListener {
            val active_sessions = ArrayList<Session>()
            for (s in sessions) {
                if (s.isActive) active_sessions.add(s)
            }
            onActiveSessionsChangedListener?.onActiveSessionsChanged(active_sessions)
            dismiss()
        }
    }

    private fun implementOpenCreateNewSessionDialog() {
        cancel_button.setOnClickListener { dismiss() }
    }

    private fun implementSessionActivityToggled() {
        sessionList.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val s = parent.getItemAtPosition(position) as Session
            s.toggleActivity()
            if (s.isActive) {
                view.setBackgroundResource(R.color.holo_green_light_half_transparent)
            } else {
                view.setBackgroundResource(R.color.light_grey_half_transparent)
            }
            log("changed active session ids: " + Global.activeSessionIds)
        }
    }

    private fun fillAdapter() {
        if (loadAllSessinsTask != null) {
            loadAllSessinsTask?.cancel(true)
        }
        loadAllSessinsTask = LoadAllSessionsTask()
        loadAllSessinsTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    inner class LoadAllSessionsTask : AsyncTask<Void, Void?, Void?>() {
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            sessions.clear()
            adapter.clear()
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void): Void? {

            val active_sessions = arguments?.getLongArray("session_ids")
            dbHelper?.allSessions?.let { allLoadedSessions ->
                allLoadedSessions.forEach {
                it.isActive = active_sessions?.contains(it.id) ?: false
                }
                sessions = allLoadedSessions
            }


            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            adapter.sessions = sessions
            adapter.notifyDataSetChanged()
        }
    }

    companion object {
        fun newInstance(session_ids: LongArray?): DialogChangeActiveSessions {
            val args = Bundle()
            args.putLongArray("session_ids", session_ids)
            val fragment = DialogChangeActiveSessions()
            fragment.arguments = args
            return fragment
        }
    }
}