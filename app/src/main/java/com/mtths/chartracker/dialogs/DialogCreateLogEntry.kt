package com.mtths.chartracker.dialogs

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.Global
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.AutoCompleteTextToAddAdapter
import com.mtths.chartracker.databinding.DialogCreateLogEntryBinding
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.AutoCompleteUtils
import com.mtths.chartracker.utils.AutoCompleteUtils.Companion.proposeExp
import com.mtths.chartracker.utils.CharProgressItemMap
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.SpaceTokenizer
import kotlinx.coroutines.launch


class DialogCreateLogEntry: DialogFragment() {

    var _binding: DialogCreateLogEntryBinding? = null
    val binding: DialogCreateLogEntryBinding
        get() = _binding!!

    lateinit var mAutoCompleteAdapter: AutoCompleteTextToAddAdapter
    var fullScreen = true

    companion object {
        const val REF = "dialog_create_log_entry"
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogCreateLogEntryBinding.inflate(inflater, container,  false)
        val root = binding.root
        return root
    }

    override fun onStart() {
        super.onStart()
        if (fullScreen) {
            dialog?.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapters()
        implementDynamicAutocompletition()
        implementAddButton()
        binding.root.apply {
            setBackgroundColor(
                ContextCompat.getColor(requireContext(),R.color.dark_greenish)
            )
            val padding = context.resources.getDimensionPixelSize(R.dimen.small_space)
            setPadding(padding, padding, padding, padding)
        }
    }

    fun getCharProgressItemsMap(): CharProgressItemMap {
        return (activity as ProgressBarFrameDropBoxActivity).charProgressItemsMap
    }

    fun fillAutoCompleteAdapter() {
        AutoCompleteUtils.Companion.GenerateAutoCompleteDataArgs(
            et = binding.editText,
            cpiMap = getCharProgressItemsMap(),
            loadedCharDetailsToAutoComplete = true,
            activeRuleSystems = PrefsHelper(context).activeRuleSystems,
            autoCompleteTextToAddAdapter = mAutoCompleteAdapter
        ).let {
            activity?.lifecycleScope?.launch {
                AutoCompleteUtils.fillAutocompleteData(it)
            }
        }
    }

    private fun initAdapters() {
        mAutoCompleteAdapter = AutoCompleteTextToAddAdapter(
            context, R.layout.auto_complete_entry
        )
        binding.editText.apply {
            setAdapter(mAutoCompleteAdapter)
            setTokenizer(SpaceTokenizer())
        }

        mAutoCompleteAdapter.clear()
        val list = Helper.fillWithStdListToAutoComplete(
            sr = PrefsHelper(context).rulesSR,
            list = ArrayList(),
            charProgressItemsMap = (activity as ProgressBarFrameDropBoxActivity).charProgressItemsMap
        )

        mAutoCompleteAdapter.addAll(list)
        mAutoCompleteAdapter.notifyDataSetChanged()
    }

    fun implementDynamicAutocompletition() {
        AutoCompleteUtils.implementReplaceTextWithAutoCompleteProposal(
            multiAutoCompleteTextView = binding.editText,
            cpiMap = ::getCharProgressItemsMap,
            character = (activity as? ActivityCharDetails)?.character
        )
        binding.editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                fillAutoCompleteAdapter()
                proposeExp(
                    s = s,
                    maTv = binding.editExp,
                    cpiMap = getCharProgressItemsMap(),
                    character = (activity as? ActivityCharDetails)?.character
                )
            }
        })
    }

    fun implementAddButton() {
        binding.addButton.setOnClickListener {
            getInstance(context)?.apply {

                val activeChars = when (activity) {
                    is ActivityCharDetails -> listOfNotNull((activity as ActivityCharDetails).character)
                    else -> Global.chars.filter { it.isActive }
                }
                val mOnLogEntryAddedListener = when (activity) {
                    is ActivityCharDetails -> activity as ActivityCharDetails
                    is ActivityMain -> activity as ActivityMain
                    else -> null
                }

                createLogEntry(
                    context,
                    binding.editExp,
                    binding.editText,
                    ArrayList(activeChars),
                    ArrayList(Global.activeSessionIds),
                    mOnLogEntryAddedListener,
                    true
                )
            }
            dismiss()
        }
    }

}