package com.mtths.chartracker.dialogs

import android.content.res.Resources
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mtths.chartracker.*
import com.mtths.chartracker.preferences.PrefsHelper.Companion.PREF_DIALOG_SHOW_STASH_UI_STATE_HIDDEN
import com.mtths.chartracker.preferences.PrefsHelper.Companion.PREF_DIALOG_SHOW_STASH_UI_STATE_SHOW_CATEGORIES
import com.mtths.chartracker.preferences.PrefsHelper.Companion.PREF_DIALOG_SHOW_STASH_UI_STATE_SHOW_TOGGLES
import com.mtths.chartracker.adapters.CategoriesAdapter
import com.mtths.chartracker.adapters.CharItemAdapter
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.ItemCategory
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.HttpUtils
import kotlinx.coroutines.*


open class DialogShowItems : DialogFragment() {

    private lateinit var rv_items : RecyclerView
    private lateinit var rv_categories : RecyclerView
    private lateinit var prefs: PrefsHelper
    private lateinit var lm: LootManager
    private lateinit var itemAdapter : CharItemAdapter
    private lateinit var categoriesAdapter: CategoriesAdapter
    private lateinit var btn_expand : TextView
    private lateinit var btn_apply: TextView
    private lateinit var eTSearch: EditText
    private lateinit var switchShowAll: SwitchCompat
    private lateinit var switchShowCursed: SwitchCompat
    private lateinit var switchShowMarked: SwitchCompat
    private lateinit var switchShowGone: SwitchCompat
    private lateinit var arrowUp: ImageView
    private lateinit var arrowDown: ImageView
    private lateinit var togglesContainer: LinearLayout
    private var itemsHaveBeenEquipped = false
    var onItemEquipped: () -> Unit = {}

    var onApply: () -> Unit = {}

    val showCharItems: Boolean
        get() = arguments?.getBoolean(PARAM_SHOW_CHAR_ITEMS) == true

    val managerAccess: Boolean
        get() = arguments?.getBoolean(PARAM_MANAGER) == true

    val localOnly: Boolean
        get() = arguments?.getBoolean(PARAM_LOCAL_ONLY) == true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_show_stash, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefs = PrefsHelper(context)
        initInterfaceItems(view)
        implementSearch()
        arguments?.let { arguments ->
            eTSearch.setText(arguments.getString(PARAM_QUERY, ""))
            itemAdapter.categoryFilter = arguments.getStringArrayList(PARAM_SLOTS)?.
            also { prefs.activeItemCategories = it }
        }
        lm = LootManager(context)
    }

    override fun onResume() {
        super.onResume()
        lm.onResume()
        loadCharacter()
        populateListView(keepCategories = true)
    }

    private fun loadCharacter() {
        arguments?.let { arguments ->
            itemAdapter.character = DatabaseHelper.getInstance(context)?.getCharacter(
                arguments.getLong(DialogSettingsChar.PARAM_CHAR_ID, 1)
            )
        }
    }

    fun getCharacterItems(c: Character, localOnly: Boolean) : ArrayList<Item>? {
        return (if (localOnly) arrayListOf() else HttpUtils.getCharacterItems(c.lootManagerDownloadName))?.apply {
             addAll(c.schwarzloot.also {
                Log.d("SCHWARZGELD","found schwarzloot on Character: $it")
            })

            if (c.schwarzgeld == null && prefs.enableSchwarzgeld) add(Item.SCHWARZGELD)
            Log.d("SCHWARZGELD", "Schwarzgeld has been added")
        }
    }

    fun populateListView(keepCategories: Boolean = false) {
        itemAdapter.character?.let { c ->
            CoroutineScope(Dispatchers.Main).launch {
                val job = async(Dispatchers.IO) {
                    if (showCharItems) {
                        itemAdapter.displayCharacter = true
                        itemAdapter.completeStash = getCharacterItems(c = c, localOnly = localOnly)

                    } else {
                        if (managerAccess) {
                            itemAdapter.managerAccess = true
                        }
                        itemAdapter.completeStash =
                            HttpUtils.getCharacterStashItems(c.lootManagerDownloadName)
                    }
                    itemAdapter.completeStash?.sortBy { it.category }
                    categoriesAdapter.data = ItemCategory.getCategories(itemAdapter.completeStash)
                    setInitialCategories(keepCategories)
                }
                job.await()
                updateList()
            }
        }
    }

    private fun setInitialCategories(keepCategories: Boolean) {
        if (!keepCategories && arguments?.getStringArrayList(PARAM_SLOTS) == null) {
            prefs.activeItemCategories = categoriesAdapter.data.filterNot { it == "all" }
        }
    }

    private fun initInterfaceItems(view : View) {
        rv_items = view.findViewById(R.id.recycler_view_items)
        activity?.let {
            itemAdapter = CharItemAdapter(fActivity = it,
                doLootManagerStuff = { doNetwork: () -> String ->
                    lm.doLootManagerStuff(doNetwork) {
                        populateListView(keepCategories = true)
                        itemsHaveBeenEquipped = true
                    }
                })
        }
        itemAdapter.completeStash = arrayListOf(Item().apply { name = getString(R.string.loading_all_caps) })
        rv_items.adapter = itemAdapter
        rv_items.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        rv_categories = view.findViewById(R.id.recycler_view_categories)
        categoriesAdapter = CategoriesAdapter(requireContext())
        categoriesAdapter.filterItemsByCategory = {
            itemAdapter.categoryFilter = prefs.activeItemCategories
            updateList()
        }
        rv_categories.adapter = categoriesAdapter
        rv_categories.layoutManager = GridLayoutManager(
            context, resources.getInteger(R.integer.categories_rows))
        btn_expand = view.findViewById(R.id.expand_button)
        btn_expand.setOnClickListener {
            val expand = itemAdapter.toggleExpand()
            itemAdapter.notifyDataSetChanged()
            if (expand) {
                btn_expand.setTextColor(resources.getColor(android.R.color.holo_green_light))
            } else {
                btn_expand.setTextColor(resources.getColor(R.color.light_grey_half_transparent))
            }
        }
        btn_apply = view.findViewById(R.id.apply_button)
        btn_apply.setOnClickListener {
            onApply()
            dismiss()
        }
        eTSearch = view.findViewById<EditText?>(R.id.edit_text_search).also {
            if (showCharItems) {
                it.hint = context?.getString(R.string.label_search_my_items)
            }
        }
        arrowUp = view.findViewById(R.id.arrow_up_settings_toggles)
        arrowDown = view.findViewById(R.id.arrow_down_settings_toggles)
        togglesContainer = view.findViewById(R.id.toggles_container)
        updateUIAccordingToState()
        switchShowAll = view.findViewById(R.id.show_all_switch)
        switchShowCursed = view.findViewById(R.id.show_cursed_switch)
        setCursedVisibilityAccordingToScreenWidth()
        switchShowMarked = view.findViewById(R.id.show_marked_switch)
        switchShowGone = view.findViewById(R.id.show_gone_switch)
        setOnClickListeners()
    }

    fun setCursedVisibilityAccordingToScreenWidth() {
        val displayMetrics = Resources.getSystem().displayMetrics
        val width = displayMetrics.widthPixels * 1.6f / displayMetrics.density
//        Log.d("SCREENMETRICS", "width = $width")
        switchShowCursed.visibility = if (width >= 820) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun setOnClickListeners() {
        switchShowAll.setOnClickListener {
            itemAdapter.showOnlyAvailable = !switchShowAll.isChecked
            itemAdapter.clear()
            itemAdapter.notifyDataSetChanged()
            updateList()
        }
        switchShowCursed.setOnClickListener {
            itemAdapter.showOnlyCursed = switchShowCursed.isChecked
            itemAdapter.clear()
            itemAdapter.notifyDataSetChanged()
            updateList()
        }
        switchShowMarked.setOnClickListener {
            itemAdapter.showOnlyMarked = switchShowMarked.isChecked
            itemAdapter.clear()
            itemAdapter.notifyDataSetChanged()
            updateList()
        }
        switchShowGone.setOnClickListener {
            itemAdapter.showOnlyGone = switchShowGone.isChecked
            itemAdapter.clear()
            itemAdapter.notifyDataSetChanged()
            updateList()
        }
        arrowUp.setOnClickListener {
            var state = prefs.dialogShowStashUiState
            if (state < PREF_DIALOG_SHOW_STASH_UI_STATE_SHOW_CATEGORIES) {
                state++
                prefs.dialogShowStashUiState = state
                updateUIAccordingToState()
            }
        }
        arrowDown.setOnClickListener {
            var state = prefs.dialogShowStashUiState
            if (state > PREF_DIALOG_SHOW_STASH_UI_STATE_HIDDEN) {
                state--
                prefs.dialogShowStashUiState = state
                updateUIAccordingToState()
            }
        }
    }

    private fun updateUIAccordingToState() {
        if (prefs.dialogShowStashUiState == PREF_DIALOG_SHOW_STASH_UI_STATE_HIDDEN) {
            arrowDown.visibility = View.INVISIBLE
        } else {
            arrowDown.visibility = View.VISIBLE
        }
        if (prefs.dialogShowStashUiState >= PREF_DIALOG_SHOW_STASH_UI_STATE_SHOW_TOGGLES) {
            togglesContainer.visibility = View.VISIBLE
        } else {
            togglesContainer.visibility = View.GONE
        }
        if (prefs.dialogShowStashUiState >= PREF_DIALOG_SHOW_STASH_UI_STATE_SHOW_CATEGORIES) {
            rv_categories.visibility = View.VISIBLE
            arrowUp.visibility = View.INVISIBLE
        } else {
            rv_categories.visibility = View.GONE
            arrowUp.visibility = View.VISIBLE
        }

    }

    override fun onDetach() {
        super.onDetach()
        if (itemsHaveBeenEquipped) {
            onItemEquipped()
        }
    }

    fun updateList() {
        itemAdapter.fill(eTSearch.text)
        categoriesAdapter.notifyDataSetChanged()

    }

    fun implementSearch() {
        eTSearch.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                itemAdapter.highlightText = eTSearch.text.toString()
                updateList()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })
    }

    companion object {
        const val PARAM_CHAR_ID = "param_char_id"
        const val PARAM_QUERY = "param_query"
        const val PARAM_SLOTS = "param_slots"
        const val PARAM_SHOW_CHAR_ITEMS = "param_show_char_items"
        const val PARAM_MANAGER = "param_manager"
        const val PARAM_LOCAL_ONLY = "local_only"
        fun newInstance(char_id: Long,
                        searchQuery: String = "",
                        categories: ArrayList<String>? = null,
                        showCharItems: Boolean,
                        manager: Boolean = false,
                        localOnly: Boolean = false): DialogShowItems {
            val args = Bundle()
            args.putLong(PARAM_CHAR_ID, char_id)
            args.putString(PARAM_QUERY, searchQuery)
            args.putBoolean(PARAM_SHOW_CHAR_ITEMS, showCharItems)
            args.putBoolean(PARAM_MANAGER, manager)
            args.putBoolean(PARAM_LOCAL_ONLY, localOnly)
            categories?.let {
                args.putStringArrayList(PARAM_SLOTS, it)
            }
            val fragment = DialogShowItems()
            fragment.arguments = args
            return fragment
        }
    }

}