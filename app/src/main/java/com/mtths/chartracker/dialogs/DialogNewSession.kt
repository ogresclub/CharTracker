package com.mtths.chartracker.dialogs

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.utils.Helper.arrayContainsElementIgnoringCases
import com.mtths.chartracker.R
import com.mtths.chartracker.adapters.RuleSystemAdapter
import com.mtths.chartracker.dataclasses.ImagedText
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.Session
import java.util.*

class DialogNewSession : DialogFragment() {
    private lateinit var newSessionName: EditText
    private lateinit var okButton: TextView
    private lateinit var note: TextView
    private lateinit var cancelButton: TextView
    private lateinit var rulesSpinner: Spinner
    var nameExists = false
    var existingSessionNames: ArrayList<String>? = null
    var dbHelper: DatabaseHelper? = null
    var mOnNewSessionCreatedListener: OnNewSessionCreatedListener? = null

    //////////////////////////////// INTERFACES //////////////////////////////////////////////////
    interface OnNewSessionCreatedListener {
        /**
         * add session_id to Global.activeSessionIds
         *
         * @param session_id
         */
        fun onNewSessionCreated(session_id: Long)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // This makes sure that the container activity has implemented
// the callback interface. If not, it throws an exception
        mOnNewSessionCreatedListener = try {
            context as OnNewSessionCreatedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnSessionCreated")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnNewSessionCreatedListener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_new_session, container, false)
        if (arguments?.getBoolean(PARAM_NO_SESSION_EXISTS) == true) {
            val dialog = dialog
            dialog?.setCanceledOnTouchOutside(false)
            dialog?.setCancelable(false)
        }
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        dbHelper = getInstance(context)
        newSessionName.addTextChangedListener(validNewSessionNameController)
        okButton.setOnClickListener {
            val name = newSessionName.text.toString().trim()
            var message = ""
            //todo choose language
            if (name.isNotBlank()) {
                //get rulesystem with selected name
                val rS = (rulesSpinner.selectedItem as ImagedText).let {
                    selectedRS -> RuleSystem.allRuleSystems().find {
                        it.name.equals(selectedRS.text, ignoreCase = true) } ?: RuleSystem.rule_system_default }

                if (!nameExists) {
                    val s = Session(
                        name = name,
                        isActive =  true,
                        ruleSystem = rS,
                        language = rS.defaultLanguage
                    )
                    dbHelper?.createSession(s)?.let {id ->
                        message = "New session created: \n$s "
                        mOnNewSessionCreatedListener?.onNewSessionCreated(id)
                        dismiss()
                    }

                } else {
                    message = "Session already exists"
                }
            } else {
                message = "Please enter a name"
            }
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun initInterfaceItems(v: View) {
        newSessionName = v.findViewById(R.id.new_session_name)
        okButton = v.findViewById(R.id.ok)
        note = v.findViewById(R.id.note)
        cancelButton = v.findViewById(R.id.cancel)
        rulesSpinner = v.findViewById(R.id.rules_system_spinner)
        rulesSpinner.adapter = RuleSystemAdapter(
            requireContext(),
            R.layout.list_view_item_imaged_text,
            R.id.text,
            RuleSystem.allRuleSystems()
        )
        rulesSpinner.setPopupBackgroundResource(R.color.light_grey)
        val noSessionExists = requireArguments().getBoolean(PARAM_NO_SESSION_EXISTS, false)
        if (noSessionExists) {
            note.setText(R.string.new_session_prompt)
            cancelButton.setText(R.string.import_data)
            cancelButton.setOnClickListener {
                fragmentManager?.let { fragmentManager ->
                    DialogDataOptions.newInstance(true).show(
                        fragmentManager, "dialogDataOption_onlyImport")
                }
                dismiss()
            }
        } else {
            cancelButton.setOnClickListener { dismiss() }
        }
    }

    override fun onResume() {
        super.onResume()
        dbHelper?.let {  existingSessionNames = it.getAllSessionNames() }
    }

    var validNewSessionNameController: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(e: Editable) {
            val s = e.toString().trim { it <= ' ' }
            if (arrayContainsElementIgnoringCases(existingSessionNames, s) ||
                s.isEmpty()
            ) {
                nameExists = true
                newSessionName.setBackgroundResource(android.R.color.holo_red_light)
            } else {
                nameExists = false
                newSessionName.setBackgroundResource(android.R.color.transparent)
            }
        }
    }

    companion object {
        const val PARAM_NO_SESSION_EXISTS = "no_session_exists"
        fun newInstance(noSessionExists: Boolean): DialogNewSession {
            val args = Bundle()
            args.putBoolean(PARAM_NO_SESSION_EXISTS, noSessionExists)
            val fragment = DialogNewSession()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(): DialogNewSession {
            val args = Bundle()
            val fragment = DialogNewSession()
            fragment.arguments = args
            return fragment
        }
    }
}