package com.mtths.chartracker.dialogs

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import com.mtths.chartracker.R
import com.mtths.chartracker.utils.Helper.log

class MyProgressDialog(context: Context): AlertDialog(context) {

    var progressBar: ProgressBar? = null
    var spinner: ProgressBar? = null
    var progressBarContainer: LinearLayout? = null
    var progressNumbers: TextView? = null
    val padding = 50
    var progressStyle: Int = PROGRESS_STYLE_SPINNER
        set(value) {
            field = value
            setVisibilitiesAccordingToProgressStyle()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        log("LOAD_INIT_DATA", "custom views have been set!")
    }

    fun inflateProgressViews() {
        val dialogView = LinearLayout(context).apply {
            orientation = LinearLayout.VERTICAL
            setPadding(padding, padding, padding, padding)
            progressBar = ProgressBar(
                ContextThemeWrapper(context, R.style.myDialog),
                null,
                android.R.attr.progressBarStyleHorizontal
            ).apply {
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
            }
            spinner = ProgressBar(ContextThemeWrapper(context, R.style.myDialog)).apply {
                setPadding(padding, padding, padding, padding)
            }
            progressBarContainer = LinearLayout(context).apply {
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                progressBar?.max = 100
                addView(progressBar)
                orientation = LinearLayout.VERTICAL
                progressNumbers = TextView(ContextThemeWrapper(context, R.style.myDialog)).apply {
                    gravity = Gravity.END
                }
                addView(progressNumbers)
            }
            setVisibilitiesAccordingToProgressStyle()
            addView(spinner)
            addView(progressBarContainer)
            log("LOAD_INIT_DATA", "created custom dialogView")

        }
        setView(dialogView)
    }

    fun show(progressStyle: Int) {
        this.progressStyle = progressStyle
        super.show()
    }

    fun setVisibilitiesAccordingToProgressStyle() {
        progressBarContainer?.visibility = if (progressStyle == PROGRESS_STYLE_SPINNER) {
            View.GONE
        } else {
            View.VISIBLE
        }
        spinner?.visibility = if (progressStyle == PROGRESS_STYLE_BAR) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

    fun setProgress(progress: Int) {
        progressBar?.progress = progress
        progressNumbers?.text = "%d/%d".format(progress, progressBar?.max ?: 0)
    }

    companion object {
        const val PROGRESS_STYLE_SPINNER = 0
        const val PROGRESS_STYLE_BAR = 1

        fun newInstance(context: Context): MyProgressDialog {

            return MyProgressDialog(ContextThemeWrapper(context, R.style.myDialog)).apply {
                /**
                 * I dont know where to call this method in one of the progress dialogs methods
                 * for it to work, so I call it here.
                 * it doesn't work in onCreate 4 sure
                 */
                inflateProgressViews()
            }
        }
    }


}


