package com.mtths.chartracker.dialogs

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.DialogFragment
import android.text.InputType
import android.text.SpannableString
import android.text.Spanned
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnLongClickListener
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.lifecycle.lifecycleScope
import com.mtths.chartracker.*
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.AutoCompleteTextToAddAdapter
import com.mtths.chartracker.databinding.DialogEditLogEntryBinding
import com.mtths.chartracker.dataclasses.CharProgressItem
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.dataclasses.Session
import com.mtths.chartracker.dataclasses.ValueNamePair
import com.mtths.chartracker.dialogs.DialogChangeActiveChars.Companion.newInstance
import com.mtths.chartracker.dialogs.DialogChangeActiveChars.OnActiveCharsChangedListener
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.AutoCompleteUtils
import com.mtths.chartracker.utils.CharProgressItemMap
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.SpaceTokenizer
import com.mtths.chartracker.widgets.MyMultiAutoCompleteTextViewNonfiltering
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.ParseException
import java.util.*
import kotlin.collections.ArrayList

/**
 * This class opens the Dialog to show the details and edit a log Entry.
 */
class DialogEditLogEntry : DialogFragment() {
    var _binding: DialogEditLogEntryBinding? = null
    val binding: DialogEditLogEntryBinding
        get() = _binding!!
    //INSTANCE VARIABLES
    private var dbHelper: DatabaseHelper? = null
    private var e: LogEntry = LogEntry()
    private var characters: MutableList<Character> = mutableListOf()
    //INTERFACE ITEMS
    lateinit var labelCharacters: TextView
    lateinit var labelTags: TextView
    lateinit var expViews: LinearLayout
    lateinit var dateOfCreationDisplay: TextView
    lateinit var sessionsDisplay: TextView
    lateinit var charactersDisplay: TextView
    lateinit var tagsDisplay: TextView
    lateinit var expDisplay: TextView
    lateinit var textField: MyMultiAutoCompleteTextViewNonfiltering
    lateinit var applyButton: Button
    lateinit var deleteButton: Button
    lateinit var cancelButton: Button
    lateinit var mAutoCompleteAdapter: AutoCompleteTextToAddAdapter
    var mOnLogEntryEditedListener: OnLogEntryEditedListener? = null

    var predictedExpCost = 0
    var cpiMap: CharProgressItemMap? = null
    var scope: CoroutineScope? = null


    //////////////////////////////////// INTERFACES ////////////////////////////////////////////////
    interface OnLogEntryEditedListener {
        /**
         * update log entry's TextView in listView
         */
        fun onLogEntryUpdated(eUpdated: LogEntry)

        /**
         * remove logEntry from allLogs and also logsFilteredBySession
         */
        fun onLogEntryDeleted(e: LogEntry)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // This makes sure that the container activity has implemented
// the callback interface. If not, it throws an exception
        mOnLogEntryEditedListener = try {
            context as OnLogEntryEditedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnLogEntryEditedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnLogEntryEditedListener = null
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
        scope?.cancel()
    }

    //////////////////////////////////// METHODS ////////////////////////////////////////////////////
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = DialogEditLogEntryBinding.inflate(inflater, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems()
        initAdapters()
        fillViewsWithLogData()
        setApplyButtonOnClickAction()
        setDeleteButtonOnClickAction()
        setCancelButtonOnClickAction()
        implementSetDateAndTime()
        implementChangeExp()
        implementChangeChars()
        implementChangeSessions()
        implementDynamicAutocompletition()
        implementRecalcExp()
        AutoCompleteUtils.implementReplaceTextWithAutoCompleteProposal(
            multiAutoCompleteTextView = textField,
            cpiMap = ::getCharProgressItemsMap
        )
    }

    //////////////////////////////// PRIVATE METHODS ///////////////////////////////////////////////

    fun fillAutoCompleteAdapter() {

        AutoCompleteUtils.Companion.GenerateAutoCompleteDataArgs(
            et = textField,
            cpiMap = getCharProgressItemsMap(),
            loadedCharDetailsToAutoComplete = true,
            activeRuleSystems = PrefsHelper(context).activeRuleSystems,
            autoCompleteTextToAddAdapter = mAutoCompleteAdapter
        ).let {
            activity?.lifecycleScope?.launch {
                AutoCompleteUtils.fillAutocompleteData(it)
            }

        }
    }

    private fun implementRecalcExp() {
        binding.recalcExpButton.apply {
            setOnClickListener {
                expDisplay.text = predictedExpCost.toString()
                e.exp = predictedExpCost
                updateExpCalc()
            }
        }
        updateExpCalc()
    }

    private fun updateExpCalc() {

        predictedExpCost = 0

        if (e.character_ids?.size == 1 && e.exp < 0) {
            val text = textField.text.toString()
            characters.singleOrNull()?.let { c ->
                /*
                 fill cpiMap with logs, but only until this one
                 no newer logs, because we want the state of this character
                 at time of this log entry
                  */
                suspend fun generateCPIs(): CharProgressItemMap {
                    return withContext(Dispatchers.IO) {
                        var charLogs: List<LogEntry> = mutableListOf()
                        if (isActive) {
                            charLogs = dbHelper?.getCharLogs(c.id) ?: mutableListOf()
                        }
                        var cpiMap = CharProgressItemMap()
                        if (isActive) {
                            cpiMap = CharProgressItem.createCharProgressItems(
                                activity = activity as? ProgressBarFrameDropBoxActivity?,
                                character = c
                            )
                        }

                        if (isActive) {
                            // use only older logs than this one
                             charLogs = charLogs.filter { log ->
                                e.dateOfCreation?.let { log.dateOfCreation?.before(it) } ?: false
                            }
                            Collections.sort(charLogs, LogEntry.CREATION_DATE_COMPARATOR)
                        }
                        /*
                        use reverse ordering to start with oldest entry in order to get increasing order
                        but not changing the order of the charLogs
                         */
                        var e: LogEntry
                        for (i in charLogs.indices) {
                            if (isActive) {
                                e = charLogs[charLogs.size - 1 - i]
                                val filterConstraints = Helper.getFilterConstraints(e.text)

                                filterConstraints.forEach { filterConstraint ->

                                        cpiMap.get(filterConstraint)?.let { cPMetaData ->
                                            if (cPMetaData.mainItem.isValueBased) {
                                                cPMetaData.mainItem.adapter?.add(e)
                                            }
                                        }
                                }
                            }
                        }

                        cpiMap.also {
                            Log.d(
                                "REFRESH_EXP",
                                "done generating cpi, " +
                                        "size = ${cpiMap.size}, " +
                                        "skills: ${cpiMap.get("@Skill")?.mainItem?.adapter?.data?.joinToString {
                                            (it as? ValueNamePair?)?.let {
                                                "${it.name}: ${it.value}"
                                            } ?: "not vnp"
                                        }}")
                        }
                    }


                }

                scope = CoroutineScope(Dispatchers.Main)
                scope?.launch {
                    if (cpiMap == null) {
                        cpiMap = generateCPIs()
                    }


                    Helper.getFilterConstraints(text).forEach {fc ->
                        cpiMap?.get(fc)?.mainItem?.adapter?.let { a ->
                            predictedExpCost += a.getExpProposal(
                                text = text,
                                nb_of_active_chars = 1,
                                data = a.data
                            )
                        }
                    }

                    if (predictedExpCost != 0 && predictedExpCost.toString() != expDisplay.text.toString()) {
                        binding.recalcExpButton.visibility = View.VISIBLE
                    } else {
                        binding.recalcExpButton.visibility = View.GONE
                    }

                }

            }
        }

    }

    private fun implementDynamicAutocompletition() {
        textField.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                fillAutoCompleteAdapter()
                updateExpCalc()
            }
        })
    }

    private fun getCharProgressItemsMap(): CharProgressItemMap {
        return (activity as ProgressBarFrameDropBoxActivity).charProgressItemsMap
    }

    private fun implementChangeExp() {
        expViews.setOnLongClickListener {
            val changeExpDialog = DialogCustom.newInstance()
            changeExpDialog.setInput(true, requireContext().getString(R.string.msg_enter_exp), false, "")
            changeExpDialog.setTitle(Helper.capitalize(requireContext().getString(R.string.msg_enter_exp)))
            changeExpDialog.setTextInputType(
                InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED,
                InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED
            )
            changeExpDialog.setInputText(e.exp.toString(), "")
            changeExpDialog.setButtonVisibility(View.GONE, View.VISIBLE)
            changeExpDialog.setPositiveButton(resources.getString(R.string.ok)) {
                val new_exp_as_string = changeExpDialog.text1.text.toString()
                var new_exp = 0
                if (new_exp_as_string.matches(Regex("-?\\d+"))) {
                    new_exp = new_exp_as_string.toInt()
                    e.exp = new_exp
                    expDisplay.text = new_exp.toString()
                    changeExpDialog.dismiss()
                } else {
                    Toast.makeText(
                        context,
                        requireContext().getString(R.string.msg_please_enter_a_signed_number_as_exp),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            changeExpDialog.show(childFragmentManager, "DialogChangeExp")

            true
        }
    }

    private fun implementChangeSessions() {
        sessionsDisplay.setOnLongClickListener {
            val changeSessionsDialog =
                DialogChangeActiveSessions.newInstance(Helper.copyToArray(e!!.session_ids!!))
            changeSessionsDialog.onActiveSessionsChangedListener = object :
                DialogChangeActiveSessions.OnActiveSessionsChangedListener {
                override fun onActiveSessionsChanged(active_sessions: ArrayList<Session>) {
                    e.session_ids = ArrayList()
                    for (s in active_sessions) {
                        e.session_ids?.add(s.id)
                    }
                    showSessions()
                }
            }
            fragmentManager?.let { fragmentManager ->  changeSessionsDialog.show(fragmentManager, "DialogChangeActiveSessions")}
            true

        }
    }

    private fun implementChangeChars() {
        charactersDisplay.setOnLongClickListener(OnLongClickListenerImplChangeActiveChars())
        labelCharacters.setOnLongClickListener(OnLongClickListenerImplChangeActiveChars())
    }

    private inner class OnLongClickListenerImplChangeActiveChars : OnLongClickListener {
        override fun onLongClick(v: View): Boolean {
            val changeCharsDialog = newInstance(
                Helper.copyToArray(e.character_ids!!)
            )
            changeCharsDialog.onActiveCharsChangedListener = object : OnActiveCharsChangedListener {
                override fun onActiveCharsChanged(active_chars: ArrayList<Character>) {
                    e.character_ids = ArrayList()
                    for (c in active_chars) {
                        e.character_ids!!.add(c.id)
                    }
                    showCharacters()
                }
            }
            fragmentManager?.let { fragmentManager ->
                changeCharsDialog.show(fragmentManager, "DialogChangeActiveChars")
            }
            return true
        }
    }

    private fun implementSetDateAndTime() {
        dateOfCreationDisplay.setOnClickListener {
            val timePickerDialog = TimePickerDialog(
                context,
                { view, hourOfDay, minute ->
                    try {
                        val date = e.dateOfCreation
                        date!!.hours = hourOfDay
                        date.minutes = minute
                        e.dateOfCreation = date
                    } catch (e1: ParseException) {
                        e1.printStackTrace()
                    } finally {
                        dateOfCreationDisplay.text = e.created_at
                    }
                },
                e.hour, e.minute, true)
            timePickerDialog.show()
        }
        dateOfCreationDisplay.setOnLongClickListener {
            //                Helper.log("DateChanger", "Year = " + e.getYear());
            val datePickerDialog = DatePickerDialog(
                requireContext(),
                { view, year, month, dayOfMonth ->
                    try {
                        val date = e.dateOfCreation
                        date?.year = year - 1900
                        date?.month = month
                        date?.date = dayOfMonth
                        e.dateOfCreation = date
                    } catch (e1: ParseException) {
                        e1.printStackTrace()
                    } finally {
                        dateOfCreationDisplay.text = e.created_at
                    }
                },
                e.year, e.month - 1, e.day
            )
            datePickerDialog.show()
            true
        }
    }

    private fun setDeleteButtonOnClickAction() {
        deleteButton.setOnClickListener {
            val dialog = DialogCustom.newInstance()
            dialog.setTitle(requireContext().getString(R.string.msg_confirm_delete_log_entry))
            dialog.setInput(false, "", false, "")
            dialog.setPositiveButton(requireContext().getString(R.string.label_delete)) {
                dbHelper?.deleteLogEntry(e)
                Toast.makeText(
                    context,
                    requireContext().getString(R.string.msg_log_entry_deleted),
                    Toast.LENGTH_SHORT
                )
                    .show()
                mOnLogEntryEditedListener?.onLogEntryDeleted(e)
                dialog.dismiss()
                dismiss()
            }
            dialog.setNegativeButton(requireContext().getString(R.string.cancel)) { dialog.dismiss() }
            fragmentManager?.let { fragmentManager ->
                dialog.show(fragmentManager, "deleteLogEntryConfirmation")
            }
        }
    }

    private fun setApplyButtonOnClickAction() {
        applyButton.setOnClickListener {
            val txt = textField.text.toString().trim()
            if (e.text != txt) {
                /*
                 * get new tags and set new text to logEntry
                 */
                val newTagNames = Helper.getTagsFromString(txt)
                e.text = txt
                /*
                 * get Tag ids and listOfViews of added and removed tagNames
                 */
                updateTagsAndNotifyUser(e, newTagNames)
            }
            dbHelper?.updateLogEntry(e)
            mOnLogEntryEditedListener?.onLogEntryUpdated(e)
            dismiss()
        }
    }

    private fun setCancelButtonOnClickAction() {
        cancelButton.setOnClickListener { dismiss() }
    }

    private fun fillViewsWithLogData() { //get LogEntry clicked
        dbHelper = DatabaseHelper.getInstance(context)
        arguments?.getLong(PARAM_ID, -1)?.let { id -> dbHelper?.getLogEntry(id)?.also { logEntry ->  e = logEntry } }
        showSessions()
        showCharacters()
        //tags -----------------------------
        val tag_names = e.getTagNames(dbHelper, false)
        Collections.sort(tag_names)
        //set tag names related to character:
        if (tag_names.size > 0) {
            tagsDisplay.visibility = View.VISIBLE
            labelTags.visibility = View.VISIBLE
            val tags_list = Helper.toStringNice(tag_names)
            val spanString = SpannableString(tags_list)
            for (tag in tag_names) {
                val clickableSpan: ClickableSpan = object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        fragmentManager?.let {fragmentManager ->
                            DialogShowFilteredLogs.newInstance("#$tag", false)
                                .show(fragmentManager, "showTags")
                        }
                    }
                }
                val start = tags_list.indexOf(tag)
                spanString.setSpan(clickableSpan, start, start + tag.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                spanString.setSpan(ForegroundColorSpan(resources.getColor(android.R.color.holo_green_light))
                        , start, start + tag.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
            tagsDisplay.text = spanString
            tagsDisplay.movementMethod = LinkMovementMethod.getInstance()
        } else {
            tagsDisplay.visibility = View.GONE
            labelTags.visibility = View.GONE
        }
        //exp ------------------------------------------
        expDisplay.text = e.exp.toString()
        //text -------------------------------------------
        textField.setText(e.text)
        //--------------------------------------------------
        dateOfCreationDisplay.text = e.created_at
    }

    private fun showSessions() { //Sessions -------------------------
        val session_names = e.getSessionNames(dbHelper)
        Helper.removeDuplicate(session_names)
        if (session_names.size > 0) {
            sessionsDisplay.text = Helper.toStringNice(session_names)
        } else {
            sessionsDisplay.text = requireContext().getString(R.string.none)
        }
    }

    private fun showCharacters() {
        dbHelper?.let { dbh ->
            //characters -----------------------
            characters = e.getCharacters(dbh)
            characters.sortBy { it.displayName }
            if (characters.isNotEmpty()) {
                this.charactersDisplay.text = characters.joinToString { it.displayName }
            } else {
                this.charactersDisplay.text = context?.getText(R.string.none)
            }
        }
    }

    private fun initInterfaceItems() {
        labelCharacters = binding.labelCharacters
        labelTags = binding.labelTags
        expViews = binding.expViews
        dateOfCreationDisplay = binding.dateOfCreation
        sessionsDisplay = binding.sessions
        charactersDisplay = binding.characters
        tagsDisplay = binding.tagNames
        expDisplay = binding.number
        textField = binding.text
        applyButton = binding.applyBtn
        deleteButton = binding.statusButton
        cancelButton = binding.cancelButton
    }

    private fun initAdapters() {
        mAutoCompleteAdapter = AutoCompleteTextToAddAdapter(
            context, R.layout.auto_complete_entry
        )
        textField.setAdapter(mAutoCompleteAdapter)
        textField.setTokenizer(SpaceTokenizer())
        mAutoCompleteAdapter.clear()
        val list = Helper.fillWithStdListToAutoComplete(
            sr = PrefsHelper(context).rulesSR,
            list = ArrayList(),
            charProgressItemsMap = (activity as ProgressBarFrameDropBoxActivity).charProgressItemsMap
        )

        mAutoCompleteAdapter.addAll(list)
        mAutoCompleteAdapter.notifyDataSetChanged()
    }

    /**
     * Ids if tags in newTagNames will be read from db or generated and then e.tag_ids will
     * be overwritten
     *
     * @param e               affected logEntry
     * @param newTagNames the list of new tagsNames
     */
    fun updateTagsAndNotifyUser(e: LogEntry, newTagNames: ArrayList<String>) { /*
        get tag names related to log Entry
         */
        val eOldTagNames = ArrayList<String>()
        for (id in e.tag_ids!!) {
            eOldTagNames.add(dbHelper!!.getTag(id)!!.name)
        }
        val removedTags = ArrayList<String>()
        val addedTags = ArrayList<String>()
        /*
         get new tag ids and get removed tags and added tags
         */e.tag_ids!!.clear()
        for (tagName in newTagNames) {
            val id = dbHelper!!.getTagId(tagName)
            //add or remove changed tagNames
            if (id < 0) { //tag doesn't exists
                e.tag_ids!!.add(dbHelper!!.createTag(tagName))
            } else {
                e.tag_ids!!.add(id)
            }
            //check if tag has been added addedTags
            if (!Helper.arrayContainsElementIgnoringCases(eOldTagNames, tagName)) {
                addedTags.add(tagName)
            }
        }
        //get removed tag
        for (tagName in eOldTagNames) {
            if (!Helper.arrayContainsElementIgnoringCases(newTagNames, tagName)) {
                removedTags.add(tagName)
            }
        }
        /*
        notify user
         */
        val removeTagsMessage = requireContext().getString(R.string.msg_removed_tags, Helper.toStringNice(removedTags)) + "\n"
        val addedTagsMessage = requireContext().getString(R.string.msg_added_tags, Helper.toStringNice(addedTags)) + "\n"
        val tagsAdded = addedTags.size != 0
        val tagsRemoved = removedTags.size != 0
        var message = ""
        if (tagsAdded || tagsRemoved) {
            if (tagsAdded) {
                message += addedTagsMessage
            }
            if (tagsRemoved) {
                message += removeTagsMessage
            }
            //remove last line break
            message = message.substring(0, message.length - 1)
        } else {
            message = requireContext().getString(R.string.msg_log_entry_updated)
        }
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        //PARAMETERS
        const val PARAM_ID = "log_id"
        const val TAG = "dialog_edit_log_entry"

        @JvmStatic fun newInstance(log_id: Long): DialogEditLogEntry {
            val args = Bundle()
            args.putLong(PARAM_ID, log_id)
            val fragment = DialogEditLogEntry()
            fragment.arguments = args
            return fragment
        }
    }
}