package com.mtths.chartracker.dialogs

import android.app.Activity
import android.app.AlertDialog
import android.app.ListActivity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.R
import java.io.File
import java.util.*

/**
 * Activity to open a openFile/createFile dialog
 *
 *
 * I didn't write this stuff myself, so hope it works.
 *
 * @author android
 */
class FileDialog : ListActivity() {
    private var path: MutableList<String> = ArrayList()
    private lateinit var myPath: TextView
    private lateinit var mFileName: EditText
    private var mList: ArrayList<HashMap<String, Any?>>? = null
    private lateinit var selectButton: Button
    private lateinit var layoutSelect: LinearLayout
    private lateinit var layoutCreate: LinearLayout
    private var inputManager: InputMethodManager? = null
    private var parentPath: String? = null
    private var currentPath: String = ROOT
    private var selectionMode = MODE_CREATE
    private var formatFilter: Array<String>? = null
    private var canSelectDir = false
    private var selectedFile: File? = null
    private val lastPositions = HashMap<String?, Int>()
    /**
     * Called when the activity is first created. Configura todos os parametros
     * de entrada e das VIEWS..
     */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setResult(Activity.RESULT_CANCELED, intent)
        setContentView(R.layout.file_dialog_main)
        myPath = findViewById<View>(R.id.path) as TextView
        mFileName = findViewById<View>(R.id.fdEditTextFile) as EditText
        inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        selectButton = findViewById<View>(R.id.fdButtonSelect) as Button
        selectButton.isEnabled = false
        selectButton.setOnClickListener {
            if (selectedFile != null) {
                intent.putExtra(RESULT_PATH, selectedFile!!.path.trim { it <= ' ' })
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
                log("Selected file was null")
            }
        }
        val newButton = findViewById<View>(R.id.fdButtonNew) as Button
        newButton.setOnClickListener { v ->
            setCreateVisible(v)
            mFileName.setText("")
            mFileName.requestFocus()
        }
        selectionMode = intent.getIntExtra(SELECTION_MODE, MODE_CREATE)
        formatFilter = intent.getStringArrayExtra(FORMAT_FILTER)
        canSelectDir = intent.getBooleanExtra(CAN_SELECT_DIR, false)
        if (selectionMode == MODE_OPEN) {
            newButton.isEnabled = false
            newButton.visibility = View.GONE
        }
        layoutSelect = findViewById<View>(R.id.fdLinearLayoutSelect) as LinearLayout
        layoutCreate = findViewById<View>(R.id.fdLinearLayoutCreate) as LinearLayout
        layoutCreate.visibility = View.GONE
        val cancelButton = findViewById<View>(R.id.fdButtonCancel) as Button
        cancelButton.setOnClickListener { v -> setSelectVisible(v) }
        val createButton = findViewById<View>(R.id.fdButtonCreate) as Button
        createButton.setOnClickListener {
            if (mFileName.text.isNotEmpty()) {
                intent.putExtra(RESULT_PATH, currentPath + "/" + mFileName.text)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
        var startPath = intent.getStringExtra(START_PATH)
        startPath = startPath ?: ROOT
        if (canSelectDir) {
            val file = File(startPath)
            selectedFile = file
            selectButton.isEnabled = true
        }
        getDir(startPath)
    }

    private fun getDir(dirPath: String?) {
        val useAutoSelection = dirPath!!.length < currentPath.length
        val position = lastPositions[parentPath]
        getDirImpl(dirPath)
        if (position != null && useAutoSelection) {
            listView.setSelection(position)
        }
    }

    /**
     * Monta a estrutura de arquivos e diretorios filhos do diretorio fornecido.
     *
     * @param dirPath Diretorio pai.
     */
    private fun getDirImpl(dirPath: String) {
        currentPath = dirPath
        val item: MutableList<String> = ArrayList()
        path = ArrayList()
        mList = ArrayList()
        var f = File(currentPath)
        var files = f.listFiles()
        if (files == null) {
            currentPath = ROOT
            f = File(currentPath)
            files = f.listFiles()
        }
        myPath.text = getText(R.string.location).toString() + ": " + currentPath
        if (currentPath != ROOT) {
            item.add(ROOT)
            addItem(ROOT, R.drawable.folder)
            path.add(ROOT)
            item.add(MNT)
            addItem(MNT, R.drawable.folder)
            path.add(MNT)
            item.add(STORAGE)
            addItem(STORAGE, R.drawable.folder)
            path.add(STORAGE)
            item.add("../")
            addItem("../", R.drawable.folder)
            path.add(f.parent)
            parentPath = f.parent
        }
        val dirsMap = TreeMap<String, String>()
        val dirsPathMap = TreeMap<String, String>()
        val filesMap = TreeMap<String, String>()
        val filesPathMap = TreeMap<String, String>()
        if (files != null) {
            for (file in files) {
                if (file.isDirectory) {
                    val dirName = file.name
                    dirsMap[dirName] = dirName
                    dirsPathMap[dirName] = file.path
                } else {
                    val fileName = file.name
                    val fileNameLwr = fileName.toLowerCase()
                    // se ha um filtro de formatos, utiliza-o
                    if (formatFilter != null) {
                        var contains = false
                        for (aFormatFilter in formatFilter!!) {
                            val formatLwr = aFormatFilter.toLowerCase()
                            if (fileNameLwr.endsWith(formatLwr)) {
                                contains = true
                                break
                            }
                        }
                        if (contains) {
                            filesMap[fileName] = fileName
                            filesPathMap[fileName] = file.path
                        }
                        // senao, adiciona todos os arquivos
                    } else {
                        filesMap[fileName] = fileName
                        filesPathMap[fileName] = file.path
                    }
                }
            }
        }
        item.addAll(dirsMap.tailMap("").values)
        item.addAll(filesMap.tailMap("").values)
        path.addAll(dirsPathMap.tailMap("").values)
        path.addAll(filesPathMap.tailMap("").values)
        val fileList = SimpleAdapter(this, mList, R.layout.file_dialog_row, arrayOf(
                ITEM_KEY, ITEM_IMAGE
        ), intArrayOf(R.id.fdrowtext, R.id.fdrowimage))
        for (dir in dirsMap.tailMap("").values) {
            addItem(dir, R.drawable.folder)
        }
        for (file in filesMap.tailMap("").values) {
            addItem(file, R.drawable.file)
        }
        fileList.notifyDataSetChanged()
        listAdapter = fileList
    }

    private fun addItem(fileName: String, imageId: Int) {
        val item = HashMap<String, Any?>()
        item[ITEM_KEY] = fileName
        item[ITEM_IMAGE] = imageId
        mList!!.add(item)
    }

    /**
     * Quando clica no item da lista, deve-se: 1) Se for diretorio, abre seus
     * arquivos filhos; 2) Se puder escolher diretorio, define-o como sendo o
     * path escolhido. 3) Se for arquivo, define-o como path escolhido. 4) Ativa
     * botao de selecao.
     */
    @Deprecated("Deprecated in Java")
    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        val file = File(path[position])
        setSelectVisible(v)
        if (file.isDirectory) {
            selectButton.isEnabled = false
            if (file.canRead()) {
                lastPositions[currentPath] = position
                getDir(path[position])
                if (canSelectDir) {
                    selectedFile = file
                    v.isSelected = true
                    selectButton.isEnabled = true
                }
            } else {
                AlertDialog.Builder(this).setIcon(R.drawable.icon)
                        .setTitle("[" + file.name + "] " + getText(R.string.cant_read_folder))
                        .setPositiveButton("OK") { dialog, which -> }.show()
            }
        } else {
            selectedFile = file
            v.isSelected = true
            selectButton.isEnabled = true
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            selectButton.isEnabled = false
            if (layoutCreate.visibility == View.VISIBLE) {
                layoutCreate.visibility = View.GONE
                layoutSelect.visibility = View.VISIBLE
            } else {
                if (currentPath != ROOT) {
                    getDir(parentPath)
                } else {
                    return super.onKeyDown(keyCode, event)
                }
            }
            true
        } else {
            super.onKeyDown(keyCode, event)
        }
    }

    /**
     * Define se o botao de CREATE e visivel.
     *
     * @param v
     */
    private fun setCreateVisible(v: View) {
        layoutCreate.visibility = View.VISIBLE
        layoutSelect.visibility = View.GONE
        inputManager!!.hideSoftInputFromWindow(v.windowToken, 0)
        selectButton.isEnabled = false
    }

    /**
     * Define se o botao de SELECT e visivel.
     *
     * @param v
     */
    private fun setSelectVisible(v: View) {
        layoutCreate.visibility = View.GONE
        layoutSelect.visibility = View.VISIBLE
        inputManager!!.hideSoftInputFromWindow(v.windowToken, 0)
        selectButton.isEnabled = false
    }

    companion object {
        /**
         * selectionMode options
         */
        const val MODE_CREATE = 0
        const val MODE_OPEN = 1
        /**
         * Chave de um item da lista de paths.
         */
        private const val ITEM_KEY = "key"
        /**
         * Imagem de um item da lista de paths (diretorio ou arquivo).
         */
        private const val ITEM_IMAGE = "image"
        /**
         * root directory
         */
        private const val ROOT = "/"
        /**
         * directories for easier access to external storage and external sd-card
         */
        private const val MNT = "/mnt"
        private const val STORAGE = "/storage"
        /**
         * Parametro de entrada da Activity: path inicial. Padrao: ROOT.
         */
        const val START_PATH = "START_PATH"
        /**
         * Parametro de entrada da Activity: filtro de formatos de arquivos. Padrao:
         * null.
         */
        const val FORMAT_FILTER = "FORMAT_FILTER"
        /**
         * Parametro de saida da Activity: path escolhido. Padrao: null.
         */
        const val RESULT_PATH = "RESULT_PATH"
        /**
         * Parametro de entrada da Activity: tipo de selecao: pode criar novos paths
         * ou nao. Padrao: nao permite.
         */
        const val SELECTION_MODE = "SELECTION_MODE"
        /**
         * Parametro de entrada da Activity: se e permitido escolher diretorios.
         * Padrao: falso.
         */
        const val CAN_SELECT_DIR = "CAN_SELECT_DIR"

        /**
         * opens a FileChooser (implementation downloaded from
         * https://code.google.com/archive/p/android-file-dialog/)
         * to choose a file with option to create a new one
         *
         * @param formatFilter  just write the file extensions you want to have in a String[].
         * @param requestCode   the requestCode will be used later in MainActivities
         * onActivityResult method to determine what will happen
         * with the chosen file.
         * @param selectionMode 0 = create, 1 = load (or use SelectionMode class for reference)
         */
        fun openFileDialog(requestCode: Int, formatFilter: Array<String?>?, selectionMode: Int, context: Context) { //open Activity to choose a file to export to
            val intent = Intent(context, FileDialog::class.java)
            intent.putExtra(START_PATH, Environment.getExternalStorageDirectory().absolutePath)
            intent.putExtra(SELECTION_MODE, selectionMode)
            if (formatFilter != null) {
                intent.putExtra(FORMAT_FILTER, formatFilter)
            }
            (context as Activity).startActivityForResult(intent, requestCode)
        }
    }
}