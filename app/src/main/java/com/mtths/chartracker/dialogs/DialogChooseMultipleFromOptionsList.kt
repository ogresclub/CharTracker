package com.mtths.chartracker.dialogs

import android.content.Context
import android.os.Bundle
import android.os.ResultReceiver
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.mtths.chartracker.*
import com.mtths.chartracker.dropbox.DbxHelper.Companion.DROPBOX_DEBUG_TAG
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.dropbox.DropBoxIntent
import com.mtths.chartracker.dropbox.DropBoxIntent.Companion.RESULT_CODE_DISMISS_DATA_OPTIONS
import com.mtths.chartracker.dropbox.DropboxClientFactory
import com.mtths.chartracker.dropbox.DropboxDialogFragment
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper
import kotlinx.coroutines.*


open class DialogChooseMultipleFromOptionsList : DropboxDialogFragment() {

    lateinit var list_view : ListView
    lateinit var prefs: PrefsHelper
    var adapter : ToggableStringAdapter? = null
    lateinit var btn_cancel : TextView
    lateinit var btn_apply: TextView

    open var mOnItemsChosenListener: OnItemsChosenListener? = null
    open var mListViewFiller: ListViewFiller? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_choose_items, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        dbXHelper.mDropBoxResultReceiver = (activity as? ActivityMain?)?.mDropBoxResultReceiver
        prefs = PrefsHelper(context)
        adapter?.add(ToggableString("loading ..."))
        adapter?.let { mListViewFiller?.populateListView(it)}
        implementOnClickListenerForListView()

    }

    open fun implementOnClickListenerForListView() {
        list_view.setOnItemClickListener {
                parent, view, position, id ->
            val toggableString = (parent as ListView).getItemAtPosition(position) as ToggableString
            toggableString.toggleActive()
            adapter?.notifyDataSetChanged()
        }

    }

    private fun initInterfaceItems(view : View) {
        list_view = view.findViewById(R.id.list_view_languages)

        val layout_res = arguments?.getInt(REF_LAYOUT_RES, DEFAULT_LAYOUT_RESOURCE) ?: DEFAULT_LAYOUT_RESOURCE
        context?.let { adapter = ToggableStringAdapter(it, layout_res) }
        list_view.adapter = adapter
        btn_cancel = view.findViewById(R.id.cancel_button)
        btn_cancel.setOnClickListener { dismiss() }
        btn_apply = view.findViewById(R.id.apply_button)
        btn_apply.setOnClickListener {
            adapter?.getActiveStrings()?.let { adapter ->
                if (adapter.isNotEmpty()) {
                    mOnItemsChosenListener?.onItemsChosen(*adapter.toTypedArray())
                    dismiss()
                } else {
                    Toast.makeText(context, "Please choose one or more files.", Toast.LENGTH_LONG).show()
                }
            }
        }

    }


    interface ListViewFiller  {
        fun populateListView(adapter: ToggableStringAdapter)
    }


    interface OnItemsChosenListener {
        fun onItemsChosen(vararg items: String)
    }

    companion object {

        fun newInstance(layoutResource: Int = DEFAULT_LAYOUT_RESOURCE
        ): DialogChooseMultipleFromOptionsList {

            val d = DialogChooseMultipleFromOptionsList()
            val args = Bundle()
            args.putInt(REF_LAYOUT_RES, layoutResource)
            d.arguments = args

            return d
        }

        const val REF_LAYOUT_RES : String = "layout_res"

        //DEFAULT VALUES
        const val DEFAULT_LAYOUT_RESOURCE = R.layout.list_view_item_choose_multiple_dialog

    }

    class ToggableString(val text: String, var isActive: Boolean = false) {
        fun toggleActive() {
            isActive = !isActive
        }
    }

    class  ToggableStringAdapter(val c : Context, var resourceId : Int) : ArrayAdapter<ToggableString>(c, resourceId) {

        fun getActiveStrings(): ArrayList<String> {
            val activeItems = ArrayList<String>()
            for (i in (0 until count)) {
                val item = getItem(i) as ToggableString
                if (item.isActive) activeItems.add(item.text)
            }
            return activeItems
        }


        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {


            var holder : TextView
                    = if (convertView == null) {
                (LayoutInflater.from(c)).inflate(resourceId, null) as TextView
            } else {
                convertView as TextView
            }

            val toggableString = getItem(position)
            holder.text = toggableString?.text

            if (toggableString?.isActive == true) {
                holder.setBackgroundColor(ContextCompat.getColor(context,
                    R.color.holo_green_light_half_transparent
                ))
            } else {
                holder.setBackgroundColor(ContextCompat.getColor(context,
                    R.color.light_grey_half_transparent
                ))

            }

            return holder
        }
    }



}

open class DialogChooseFilesToDownloadFromDropBox : DialogChooseMultipleFromOptionsList() {

    var resultReceiver: ResultReceiver? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        resultReceiver = arguments?.getParcelable(REF_RESULT_RECEIVER)
    }

    override var mOnItemsChosenListener: OnItemsChosenListener? = object : OnItemsChosenListener {
        override fun onItemsChosen(vararg items: String) {
            Helper.log(DROPBOX_DEBUG_TAG, "start dropbox intent: downloading files")
            dbXHelper.startDropboxIntent(
                task = DropBoxIntent.TASK_DOWNLOAD,
                mode = DropBoxIntent.MODE_XML,
                fileNameNoEnding = *items)
            resultReceiver?.send(RESULT_CODE_DISMISS_DATA_OPTIONS, Bundle())
        }
    }

    override var mListViewFiller: ListViewFiller? = object : ListViewFiller {

        override fun populateListView(adapter: ToggableStringAdapter) {
            doDropBoxStuff(dismissAfterJob = false) {
                Helper.log(DROPBOX_DEBUG_TAG, "loading meta data to populate list view")
                CoroutineScope(Dispatchers.Main).launch {
                    adapter.clear()
                    val asyncResult = async(context = Dispatchers.IO) {
                        DropboxClientFactory.client?.files()?.listFolder("")?.entries
                    }
                    Helper.log(DROPBOX_DEBUG_TAG, "finished loading meta data")
                    asyncResult.await()?.
                        sortedBy { it.name.lowercase() }?.
                        forEach {
                            if (it.name.endsWith(".xml")) adapter.add(ToggableString(it.name))
                        }
                    Helper.log(DROPBOX_DEBUG_TAG, "finished filling adapter")


                }
            }
        }
    }

    companion object {

        const val REF_RESULT_RECEIVER = "dropbox_result_receiver"

        fun newInstance(dropBoxReceiver: ResultReceiver?): DialogChooseFilesToDownloadFromDropBox {
            val args = Bundle()
            args.putParcelable(REF_RESULT_RECEIVER, dropBoxReceiver)
            val fragment = DialogChooseFilesToDownloadFromDropBox()
            fragment.arguments = args
            return fragment
        }
    }
}