package com.mtths.chartracker.dialogs

import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.FragmentManager
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.fragments.FragmentMainScreen.OnLogEntryAddedListener
import com.mtths.chartracker.Global
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.utils.Helper.remove
import com.mtths.chartracker.R
import java.util.*

/**
 * Created by mtths on 22.04.17.
 */
class DialogChangeCharacterStatus : DialogFragment() {
    lateinit var restore: Button
    lateinit var delete: Button
    lateinit var retire: Button
    lateinit var dead: Button
    var mOnCharStatusChangedListener: OnCharStatusChangedListener? = null
    var mOnLogEntryAddedListener: OnLogEntryAddedListener? = null
    private var mDeleteCharacterTask: DeleteCharacterTask? = null
    ////////////////////////////////////// METHODS ////////////////////////////////////////////////
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_change_char_status, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        implementDeleteButton()
        implementRetireButton()
        implementRestoreButton()
        implementDeadButton()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // This makes sure that the container activity has implemented
// the callback interface. If not, it throws an exception
        mOnCharStatusChangedListener = try {
            context as OnCharStatusChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnCharStatusChangedListener")
        }
        mOnLogEntryAddedListener = try {
            context as OnLogEntryAddedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement FragmentMainScreen.OnLogEntryAddedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnCharStatusChangedListener = null
        mOnLogEntryAddedListener = null
        mDeleteCharacterTask?.cancel(true)
        mDeleteCharacterTask = null
    }

    override fun onDestroy() {
        super.onDestroy()
        mDeleteCharacterTask?.cancel(true)
        mDeleteCharacterTask = null
    }

    //////////////////////////////// INTERFACES ////////////////////////////////////////////////////
    interface OnCharStatusChangedListener {
        fun onCharStatusChanged(
            character: Character,
            keepLastUpdateDate: Boolean = false,
            updateDisplays: Boolean = true)
    }

    ///////////////////////////////////// PRIVATE METHODS //////////////////////////////////////////
    private fun initInterfaceItems(v: View) {
        restore = v.findViewById(R.id.restore)
        retire = v.findViewById(R.id.retire)
        dead = v.findViewById(R.id.dead)
        delete = v.findViewById(R.id.delete)
        val c = character
        if (c!!.isRetired) {
            retire.visibility = View.GONE
        }
        if (c.isDead) {
            dead.visibility = View.GONE
        }
        if (!(c.isDead || c.isRetired)) {
            restore?.visibility = View.GONE
        }
    }

    private fun implementRetireButton() {
        retire.setOnClickListener {
            character?.let { c ->
                c.isRetired = true
                //only ad new retirement date, when none has been stored so far
                if (c.retired_at.isEmpty()) {
                    c.setDateOfRetirement(Calendar.getInstance().time)
                }
                c.isActive = false
                mOnCharStatusChangedListener?.onCharStatusChanged(c)
                Toast.makeText(
                    context,
                    context?.getString(R.string.msg_character_has_retired, c.name),
                    Toast.LENGTH_SHORT)
                    .show()
                dismiss()
            }

        }
    }

    private fun implementRestoreButton() {
        restore.setOnClickListener {
            character?.let { c ->
                var message = ""
                when {
                    c.isRetired -> {
                        val dialog = DialogCustom.newInstance()
                        dialog.setTitle(context?.getString(R.string.msg_question_delete_retirement_date))
                        dialog.setPositiveButton(
                            requireContext().resources.getString(R.string.yes),
                            View.OnClickListener {
                                c.retired_at = ""
                                val message = requireContext().getString(R.string.msg_character_has_been_restored)
                                c.isRetired = false
                                restoreCharCallback(c, message)
                                dialog.dismiss()
                            })
                        dialog.setNegativeButton(
                            requireContext().resources.getString(R.string.no),
                            View.OnClickListener {
                                val message = requireContext().getString(R.string.msg_character_has_been_restored)
                                c.isRetired = false
                                restoreCharCallback(c, message)
                                dialog.dismiss()
                            })
                        dialog.show(childFragmentManager, "DeleteRetirementDateOnRestoration?")

                    }

                    c.isDead -> {

                        ////////// create new Log Entry for Resurrection ///////////////////////////////

                        val dialog = DialogCustom.newInstance()
                        dialog.noTitle = false
                        dialog.fullScreen = true
                        dialog.setTitle(R.string.msg_create_a_log_entry)
                        dialog.setInput(
                            true,
                            requireContext().getString(R.string.msg_enter_exp_cost),
                            true,
                            requireContext().getString(R.string.msg_describe_resurrection, c.name)
                        )
                        dialog.setTextInputType(InputType.TYPE_NUMBER_FLAG_SIGNED, -1)
                        dialog.setInputText("-500", "")
                        dialog.setNegativeButton(
                            R.string.cancel,
                            { dialog.dismiss() })
                        dialog.setPositiveButton("Log", {
                            if (dialog.text1.text.toString().matches(Regex("[+-]?\\d+"))) {
                                val dbHelper = getInstance(context)
                                val activeChars = ArrayList<Character>()
                                activeChars.add(c)
                                val sessionIds = dbHelper!!.getSessionIdsRelatedToCharacter(c.id)
                                log("newLogEntry", "sessions: '$sessionIds")
                                val text = dialog.text2.text.toString()
                                dialog.text2.setText("${requireContext().getString(R.string.filter_constraint_resurrection)} $text")
                                dbHelper.createLogEntry(
                                    context,
                                    dialog.text1,
                                    dialog.text2,
                                    activeChars,
                                    sessionIds,
                                    mOnLogEntryAddedListener,
                                    true
                                )
                                val message = requireContext().getString(R.string.msg_character_has_been_resurrected)
                                c.isDead = false
                                restoreCharCallback(c, message)
                                dialog.dismiss()
                            } else { //exp entered was no number. Somehow inputType is not working correctly.
                                Toast.makeText(
                                    context,
                                    R.string.msg_please_enter_a_signed_number_as_exp,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        })

                        dialog.show(childFragmentManager, "createLogEntryForResurrection")

                        /////////////////////////////////////////////////////////////////////////////////
                    }

                    else -> { /*
                        Character is not dead or retired so it has to be deleted, but actually I don't
                        think this case ever happens because deleted chars are not accessible
                         */
                        c.isDeleted = false
                        message = requireContext().getString(R.string.msg_character_has_been_restored)
                        restoreCharCallback(c, message)
                    }
                }
            }
        }
    }

    private fun restoreCharCallback(c: Character, message: String) {
        mOnCharStatusChangedListener?.onCharStatusChanged(c)
        Toast.makeText(context, message.format(c.name), Toast.LENGTH_SHORT).show()
        dismiss()
    }

    private fun implementDeadButton() {
        dead.setOnClickListener {
            killCharacterAndCreateLogEntryForDeath(
                character = character,
                context = requireContext(),
                fragmentManager = childFragmentManager,
                onCreateDeathLog = {dismiss()},
                onCharStatusChangedListener = mOnCharStatusChangedListener,
                onLogEntryAddedListener = mOnLogEntryAddedListener
            )
        }
    }

    private fun implementDeleteButton() {
        delete.setOnClickListener {
            val dbHelper = getInstance(context)
            val dialog = DialogCustom.newInstance()
            dialog.setTitle(requireContext().getString(R.string.msg_question_confirm_delete_character))
            dialog.setPositiveButton(requireContext().getString(R.string.yes)) {
                val d = DialogCustom.newInstance()
                d.setTitle(requireContext().getString(R.string.msg_question_confirm_hard_delete_character))
                d.setPositiveButton(requireContext().getString(R.string.yes)) {
                    d.dismiss()
                    mDeleteCharacterTask = DeleteCharacterTask()
                    mDeleteCharacterTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                }
                d.setNegativeButton(requireContext().getString(R.string.no)) {
                    dbHelper?.deleteCharacter(character!!, false)
                    //need to change this instance of character also to update in chars
                    remove(Global.chars, character!!)
                    Toast.makeText(
                        context,
                        requireContext().getString(R.string.msg_character_soft_deleted),
                        Toast.LENGTH_SHORT
                    ).show()
                    requireActivity().finish()
                }
                childFragmentManager.let { d.show(it, "chooseDeleteOption") }

            }
            dialog.setNegativeButton(requireContext().getString(R.string.cancel), View.OnClickListener { dialog.dismiss() })
            childFragmentManager.let { dialog.show(it, "ConfirmDeletion") }

        }
    }

    private val character: Character?
        get() = (activity as? ActivityCharDetails?)?.character

    private inner class DeleteCharacterTask : AsyncTask<Void, Void?, Void?>() {
        var progressDialog = ProgressDialog(ContextThemeWrapper(context, R.style.myDialog))
        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            progressDialog.setTitle(requireContext().getString(R.string.msg_deleting_character))
            progressDialog.setMessage(requireContext().getString(R.string.msg_please_wait_while_character_is_being_deleted))
            progressDialog.show()
        }

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void): Void? {
            val dbHelper = getInstance(this@DialogChangeCharacterStatus.context)
            dbHelper!!.deleteCharacter(character!!, true)
            val c = character
            remove(Global.chars, c!!)
            /*
            load all logs new from database, because all the references to the char have been deleted
            remove char from allCharIds and if he/she was active, also from activeCharIds
            */
            Global.allLogs = dbHelper.getAllLogs()
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            progressDialog.dismiss()
            Toast.makeText(
                context,
                requireContext().getString(R.string.msg_character_hard_deleted),
                Toast.LENGTH_SHORT)
                .show()
            activity?.finish()
        }
    }

    companion object {
        fun newInstance(): DialogChangeCharacterStatus {
            val args = Bundle()
            val fragment = DialogChangeCharacterStatus()
            fragment.arguments = args
            return fragment
        }

        fun killCharacterAndCreateLogEntryForDeath(
            character: Character?,
            context: Context,
            fragmentManager: FragmentManager,
            onCharStatusChangedListener: OnCharStatusChangedListener?,
            onLogEntryAddedListener: OnLogEntryAddedListener?,
            onCreateDeathLog: () -> Unit = {}
        ) {
            character?.let { c ->
                ////////// create new Log Entry for Resurrection ///////////////////////////////
                val dialog = DialogCustom.newInstance()
                dialog.setTitle(context.getString(R.string.msg_question_how_did_character_die, c.name))
                dialog.setInput(
                    false,
                    "",
                    true,
                    context.getString(R.string.msg_question_how_did_character_die, c.name)
                )
                dialog.setTextInputType(InputType.TYPE_NUMBER_FLAG_SIGNED, -1)
                dialog.setInputText("", "")
                dialog.setNegativeButton(context.getString(R.string.cancel)) { dialog.dismiss() }
                dialog.setPositiveButton(context.getString(R.string.label_log)) {
                    val dbHelper = getInstance(context)
                    val sessionIds = dbHelper!!.getSessionIdsRelatedToCharacter(c.id)
                    log("newLogEntry", "sessions: '$sessionIds")
                    val text = dialog.text2.text.toString()
                    dialog.text2.setText("${context.getString(R.string.filter_constraint_death)} $text")
                    dbHelper.createLogEntry(
                        context,
                        dialog.text1,
                        dialog.text2,
                        arrayListOf(character),
                        sessionIds,
                        onLogEntryAddedListener,
                        true
                    )
                    val message = context.getString(R.string.msg_character_has_died)
                    killCharacter(
                        c = c,
                        msg = message,
                        context = context,
                        onCharStatusChangedListener = onCharStatusChangedListener)
                    dialog.dismiss()
                    onCreateDeathLog()
                }
                fragmentManager.let { dialog.show(it, "createLogEntryForDeath") }

                /////////////////////////////////////////////////////////////////////////////////
            }
        }

        private fun killCharacter(
            c: Character?,
            msg: String,
            context: Context,
            onCharStatusChangedListener: OnCharStatusChangedListener?) {

            c?.let { c ->
                c.isDead = true
                c.isActive = false
                onCharStatusChangedListener?.onCharStatusChanged(c)
                Toast.makeText(context, msg.format(c.name), Toast.LENGTH_SHORT).show()
            }

        }
    }
}