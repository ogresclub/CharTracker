package com.mtths.chartracker.dialogs

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import com.mtths.chartracker.R
import java.util.*

class DialogShowListView : DialogFragment() {
    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var listView: ListView
    private lateinit var cancel_button: Button
    var mOnItemClickListener: OnItemClickListener? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_show_filtered_logs, container, false)
        dialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        adapter = ArrayAdapter(requireContext(), R.layout.list_view_item_log)
        listView.adapter = adapter
        fillAdapter()
        cancel_button.setOnClickListener { dismiss() }
        if (mOnItemClickListener != null) {
            listView.onItemClickListener = mOnItemClickListener
        }
    }

    fun setOnItemClickListener(mOnItemClickListener: OnItemClickListener?) {
        this.mOnItemClickListener = mOnItemClickListener
    }

    override fun onResume() {
        super.onResume()
        fillAdapter()
    }

    private fun initInterfaceItems(view: View) {
        cancel_button = view.findViewById(R.id.cancel_button)
        listView = view.findViewById(R.id.filteredLogsRecyclerView)
    }

    fun fillAdapter() {
        adapter.clear()
        val list = arguments?.getStringArrayList(PARAM_DATA) ?: ArrayList()
        adapter.addAll(list)
        adapter.notifyDataSetChanged()
    }

    companion object {
        var PARAM_DATA = "data"
        fun newInstance(data: ArrayList<String>): DialogShowListView {
            val args = Bundle()
            args.putStringArrayList(PARAM_DATA, data)
            val fragment = DialogShowListView()
            fragment.arguments = args
            return fragment
        }
    }
}