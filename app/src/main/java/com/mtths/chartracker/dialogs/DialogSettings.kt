package com.mtths.chartracker.dialogs

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.R
import com.mtths.chartracker.adapters.SettingsAdapter
import kotlinx.coroutines.launch


class DialogSettings : DialogFragment() {
    private lateinit var update_ogres_club_header_text_view: TextView
    private lateinit var button_ogres_club_each_start: Button
    private lateinit var button_ogres_club_now: Button
    private lateinit var button_ogres_club_never: Button
    private lateinit var crawlOgresClubButtonsContainer: LinearLayout
    private lateinit var maxColsMainScreenIncrease: TextView
    private lateinit var maxColsMainScreenDecrease: TextView
    private lateinit var maxColsMainScreenLabel: TextView
    private lateinit var maxColsMainScreenIndicator: TextView
    private lateinit var numColsCharViewIncrease: TextView
    private lateinit var numColsCharViewDecrease: TextView
    private lateinit var numColsCharViewLabel: TextView
    private lateinit var numColsCharViewIndicator: TextView
    private lateinit var searchContextSizeIncrease: TextView
    private lateinit var searchContextSizeDecrease: TextView
    private lateinit var searchContextSizeLabel: TextView
    private lateinit var searchContextSizeIndicator: TextView
    private lateinit var ok_button: Button
    private lateinit var viewPagerParamsCharViewWidthDisplay: TextView
    private lateinit var viewPagerParamsMainScreenWidthDisplay: TextView
    private lateinit var viewPagerParamsLogViewWidthDisplay: TextView
    private lateinit var switchContainer: LinearLayout
    private lateinit var namedEditTextContainer: LinearLayout
    private lateinit var adapter: SettingsAdapter


    var mOnDismissListener: DialogInterface.OnDismissListener? = null
    var mOnRecreateActivityNecessaryListener: (() -> Unit)? = null
    private var recreateActivityMain = false
    private lateinit var prefs: PrefsHelper

    lateinit var tasks: HashMap<Int, () -> Unit>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_settings, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mOnDismissListener?.onDismiss(dialog)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        prefs = PrefsHelper(context)
        initInterfaceItems(view)

        setMaxColsMainScreenLabelAccordingToSettings()
        setNumColsCharViewLabelAccordingToSettings()
        setsearchContextSizeLabelAccordingToSettings()

        activeUpdateOgresClubButtonAccordingToSettings()

        val recreateActivity: () -> Unit = {
            //otherwise recreating activity causes crash,
            // because the listener calls activityMain.onResume
            mOnDismissListener = null
            activity?.let { activity ->
                val intent = activity.intent;
                activity.finish();
                startActivity(intent)
            }
        }

        tasks = SettingsAdapter.getTasks(recreateActivity)

        setClickListeners()
        implementOnClickListeners()
        adapter = SettingsAdapter(
            activity = requireActivity(),
            switchContainer = switchContainer,
            namedEditTextContainer = namedEditTextContainer).apply {
            inflateSettings(
                tasks = tasks
            )
        }

    }

    private fun initInterfaceItems(v: View) {
        button_ogres_club_each_start = v.findViewById(R.id.crawl_ogres_club_on_each_start_button)
        update_ogres_club_header_text_view = v.findViewById(R.id.crawl_ogres_club_text_view)
        button_ogres_club_now = v.findViewById(R.id.update_ogres_club_now_button)
        button_ogres_club_never = v.findViewById(R.id.update_ogres_club_never_button)
        crawlOgresClubButtonsContainer = v.findViewById(R.id.crawl_ogres_club_buttons_container)
        if (!prefs.rulesOgresClub) {
            crawlOgresClubButtonsContainer.visibility = View.GONE
            update_ogres_club_header_text_view.visibility = View.GONE
        }
        ok_button = v.findViewById(R.id.ok_button)
        maxColsMainScreenLabel = v.findViewById(R.id.max_cols_main_screen_label)
        maxColsMainScreenLabel.text = getString(R.string.max_cols_in_main_screen_label)
        maxColsMainScreenIndicator = v.findViewById(R.id.max_cols_main_screen_indicator)
        maxColsMainScreenIncrease = v.findViewById(R.id.max_cols_main_screen_increase)
        maxColsMainScreenDecrease = v.findViewById(R.id.max_cols_main_screen_decrease)
        numColsCharViewLabel = v.findViewById(R.id.cols_char_view_label)
        numColsCharViewLabel.text = getString(R.string.cols_in_char_view_label)
        numColsCharViewIndicator = v.findViewById(R.id.num_cols_char_view_indicator)
        numColsCharViewIncrease = v.findViewById(R.id.cols_char_view_increase)
        numColsCharViewDecrease = v.findViewById(R.id.cols_char_view_decrease)
        searchContextSizeLabel = v.findViewById(R.id.search_context_size_label)
        searchContextSizeLabel.text = getString(R.string.search_context_size_label)
        searchContextSizeIndicator = v.findViewById(R.id.search_context_size_indicator)
        searchContextSizeIncrease = v.findViewById(R.id.search_context_size_increase)
        searchContextSizeDecrease = v.findViewById(R.id.search_context_size_decrease)
        switchContainer = v.findViewById(R.id.switch_container)
        namedEditTextContainer = v.findViewById(R.id.named_edit_texts_container)
    }

    private fun setClickListeners() {
        ok_button.setOnClickListener {
            adapter.applySettingsChanges(tasks = tasks)
            dismiss()
            if (recreateActivityMain) {
                mOnRecreateActivityNecessaryListener?.invoke()
            }
        }
        maxColsMainScreenIncrease.setOnClickListener {
            prefs.maxNumColsMainScreen++
            setMaxColsMainScreenLabelAccordingToSettings()
        }
        maxColsMainScreenDecrease.setOnClickListener {
            if (prefs.maxNumColsMainScreen > 1) {
                prefs.maxNumColsMainScreen--
                setMaxColsMainScreenLabelAccordingToSettings()
            }
        }
        numColsCharViewIncrease.setOnClickListener {
            prefs.numColsCharView++
            setNumColsCharViewLabelAccordingToSettings()
        }
        numColsCharViewDecrease.setOnClickListener {
            if (prefs.numColsCharView > 1) {
                prefs.numColsCharView--
                setNumColsCharViewLabelAccordingToSettings()
            }
        }
        searchContextSizeIncrease.setOnClickListener {
            prefs.searchContextSize++
            setsearchContextSizeLabelAccordingToSettings()
        }
        searchContextSizeDecrease.setOnClickListener {
            if (prefs.searchContextSize > 0) {
                prefs.searchContextSize--
                setsearchContextSizeLabelAccordingToSettings()
            }
        }




    }
    private fun setMaxColsMainScreenLabelAccordingToSettings() {
        maxColsMainScreenIndicator.text = prefs.maxNumColsMainScreen.toString()
    }
    private fun setNumColsCharViewLabelAccordingToSettings() {
        numColsCharViewIndicator.text = prefs.numColsCharView.toString()
    }

    private fun setsearchContextSizeLabelAccordingToSettings() {
        searchContextSizeIndicator.text = prefs.searchContextSize.toString()
    }

    private fun activeUpdateOgresClubButtonAccordingToSettings() {
        var active_button = button_ogres_club_each_start
        if (prefs.crawlOgresClub == PrefsHelper.CRAWL_OGRES_CLUB_NEVER) {
            active_button = button_ogres_club_never
        }
        active_button.setBackgroundResource(R.drawable.button_holo_green_light)
    }

    private fun implementOnClickListeners() {
        button_ogres_club_each_start.setOnClickListener {
            button_ogres_club_each_start.setBackgroundResource(R.drawable.button_holo_green_light)
            button_ogres_club_never.setBackgroundResource(R.drawable.button_light_grey)
            button_ogres_club_now.setBackgroundResource(R.drawable.button_light_grey)
            prefs.crawlOgresClub = PrefsHelper.CRAWL_OGRES_CLUB_ON_EACH_START
        }
        button_ogres_club_never.setOnClickListener {
            button_ogres_club_each_start.setBackgroundResource(R.drawable.button_light_grey)
            button_ogres_club_never.setBackgroundResource(R.drawable.button_holo_green_light)
            button_ogres_club_now.setBackgroundResource(R.drawable.button_light_grey)
            prefs.crawlOgresClub = PrefsHelper.CRAWL_OGRES_CLUB_NEVER
        }
        button_ogres_club_now.setOnClickListener {
            val activityMain = (activity as ActivityMain?)
            lifecycleScope.launch {
                activityMain?.crawlOgresClubForData()
            }
        }
    }

    private fun activate(b: Button) {
        b.setBackgroundResource(R.drawable.button_holo_green_light)
    }

    private fun deactivate(b: Button) {
        b.setBackgroundResource(R.drawable.button_light_grey)
    }

    companion object {
        fun newInstance(): DialogSettings {
            val args = Bundle()
            val fragment = DialogSettings()
            fragment.arguments = args
            return fragment
        }
    }
}