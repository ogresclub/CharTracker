package com.mtths.chartracker.dialogs

import android.content.Context
import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.DialogFragment

import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import com.mtths.chartracker.DatabaseHelper
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.Helper.arrayContainsElementIgnoringCases
import com.mtths.chartracker.utils.Helper.deleteStoredRuleSystemsForCharsRelatedToSession
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.utils.Helper.toStringNice
import com.mtths.chartracker.R
import com.mtths.chartracker.adapters.RuleSystemAdapter
import com.mtths.chartracker.dataclasses.ImagedText
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.dataclasses.Session
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

/**
 * you can get
 */
class DialogEditSession : DialogFragment() {
    var dbHelper: DatabaseHelper? = null
    private lateinit var created_at: TextView
    private lateinit var characters: TextView
    private lateinit var tags: TextView
    private lateinit var label_characters: TextView
    private lateinit var label_tags: TextView
    private lateinit var delete: Button
    private lateinit var cancel: Button
    private lateinit var apply: Button
    private lateinit var name: EditText
    private lateinit var rulesSpinner: Spinner
    private lateinit var languagesSpinner: Spinner
    private var s: Session? = null
    var nameExists = false
    var existingSessionNames: ArrayList<String>? = null
    private var mOnSessionEditedListener: OnSessionEditedListener? = null
    private var mLoadTagsTask: LoadTagsTask? = null
    private var mLoadCharactersTask: LoadCharactersTask? = null

    ////////////////////////// INFERFACES /////////////////////////////////////////////////////////
    interface OnSessionEditedListener {
        fun onSessionEdited(editedSession: Session)
        fun onSessionDeleted(s: Session)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // This makes sure that the container activity has implemented
// the callback interface. If not, it throws an exception
        mOnSessionEditedListener = try {
            context as OnSessionEditedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnSessionEditedListener = null
    }

    ////////////////////////////////// METHODS ////////////////////////////////////////////////////
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_edit_session, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dbHelper = getInstance(context)
        dbHelper?.let {
            s = it.getSession(session_id = requireArguments().getLong(PARAM_ID))
            existingSessionNames = it.getAllSessionNames()
        }
        existingSessionNames?.remove(s?.name)
        initInterfaceItems(view)
        initRulesSpinner(view)
        initLanguagesSpinner(view)

        /*
         * populate Text from data
         */
        created_at.text = s?.created_at
        name.setText(s?.name)
        loadSessionInfo()
    }

    fun loadSessionInfo() {
        mLoadCharactersTask?.cancel(true)
        mLoadCharactersTask = LoadCharactersTask()
        mLoadCharactersTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

        mLoadTagsTask?.cancel(true)
        mLoadTagsTask = LoadTagsTask()
        mLoadTagsTask?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mLoadCharactersTask?.cancel(true)
        mLoadCharactersTask = null
        mLoadTagsTask?.cancel(true)
        mLoadTagsTask = null
    }

    private inner class LoadTagsTask : AsyncTask<Void, Void?, Void?>() {
        var tagNames: ArrayList<String>? = null
        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void): Void? {
            tagNames = dbHelper?.getTagNamesRelatedToSessions(arrayListOf(s!!.id))
            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            if (tagNames!!.size > 0) {
                Collections.sort(tagNames)
                tags.text = toStringNice(tagNames!!)
            } else {
                tags.visibility = View.GONE
                label_tags.visibility = View.GONE
            }
        }
    }

    private inner class LoadCharactersTask : AsyncTask<Void, Void?, Void?>() {
        var charNames: ArrayList<String>? = null
        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg voids: Void): Void? {
            dbHelper?.let {dbh ->
                s?.let { s ->
                    charNames =
                        ArrayList(
                            dbh.getCharsRelatedToSessions(
                                arrayListOf(s.id), show_all_chars = true
                            ).map { it.name })

                }

            }

            return null
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(aVoid: Void?) {
            if (charNames!!.size > 0) {
                Collections.sort(charNames)
                characters.text = toStringNice(charNames!!)
            } else {
                characters.visibility = View.GONE
                label_characters.visibility = View.GONE
            }
        }
    }
    private fun initLanguagesSpinner(root: View) {
        languagesSpinner = root.findViewById(R.id.languages_spinner)
        languagesSpinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.support_simple_spinner_dropdown_item,
            PrefsHelper.ALL_LANGUAGES)
        languagesSpinner.setPopupBackgroundResource(R.color.light_grey)
        // display current rule system
        val idx = PrefsHelper.ALL_LANGUAGES.indexOfFirst { it == s?.language }
        if (idx > -1) {
            languagesSpinner.setSelection(idx)
        }
    }
    private fun initRulesSpinner(root: View) {
        rulesSpinner = root.findViewById(R.id.rules_system_spinner)
        rulesSpinner.adapter = RuleSystemAdapter(
            requireContext(),
            R.layout.list_view_item_imaged_text,
            R.id.text,
            RuleSystem.allRuleSystems()
        )
        rulesSpinner.setPopupBackgroundResource(R.color.light_grey)
        // display current rule system
        val idx = RuleSystem.allRuleSystems().indexOfFirst { it == s?.ruleSystem }
        if (idx > -1) {
            rulesSpinner.setSelection(idx)
        }
    }

    private fun initInterfaceItems(root: View) {
        log("Interface Items will be initialized")
        created_at = root.findViewById<View>(R.id.date_of_creation) as TextView
        characters = root.findViewById<View>(R.id.characters) as TextView
        label_characters = root.findViewById<View>(R.id.label_characters) as TextView
        tags = root.findViewById<View>(R.id.tag_names) as TextView
        label_tags = root.findViewById<View>(R.id.label_tags) as TextView
        delete = root.findViewById<View>(R.id.status_button) as Button
        cancel = root.findViewById<View>(R.id.cancel_button) as Button
        apply = root.findViewById<View>(R.id.apply_btn) as Button
        name = root.findViewById<View>(R.id.session_name) as EditText
        cancel.setOnClickListener { dismiss() }
        name.addTextChangedListener(validNewSessionNameController)
        delete.setOnClickListener {
            DialogCustom.getAlterDialogBuilder(context)
                    .setTitle("Confirm Deletion")
                    .setMessage("Are you sure that you want to delete this session and all references to it?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes) { dialog, whichButton ->
                        s?.let {
                            //todo improve delete session option

                            //Delete Cached Rule Systems from all Characters Related to Session and tghen Delete Session
                            CoroutineScope(Dispatchers.Main).launch {
                                val jDelete = async(Dispatchers.IO) {
                                    deleteStoredRuleSystemsForCharsRelatedToSession(
                                        sessionId = it.id,
                                        dbHelper =  dbHelper)
                                    dbHelper?.deleteSession(it.id)
                                }
                                jDelete.await()
                                dismiss()
                                mOnSessionEditedListener?.onSessionDeleted(it)

                            }

                        }

                    }
                    .setNegativeButton(android.R.string.no, null).show()
        }
        apply.setOnClickListener {
            //get session name from EditText
            val newName = name.text.toString().trim()
            if (name.text.isNotBlank()) {
                if (!nameExists) {
                    s?.let { s ->
                        s.name = newName
                        (rulesSpinner.selectedItem as ImagedText).let { imagedText ->
                            RuleSystem.allRuleSystems().
                            find { rS ->  rS.name.equals(imagedText.text, ignoreCase = true) }?.
                                let { s.ruleSystem = it }
                        }
                        s.language = languagesSpinner.selectedItem as String
                        dbHelper?.updateSession(s)
                        mOnSessionEditedListener?.onSessionEdited(s)
                        dismiss()
                    }
                } else {
                    Toast.makeText(context, "Session already exists", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(context, "Please enter a name", Toast.LENGTH_SHORT).show()
            }
            s?.let { s ->
                CoroutineScope(Dispatchers.Main).launch {
                    launch(Dispatchers.IO) { dbHelper?.updateSession(s) }
                    launch(Dispatchers.IO) {
                        deleteStoredRuleSystemsForCharsRelatedToSession(
                            s.id,
                            dbHelper
                        )
                    }
                }
            }
        }
    }

    var validNewSessionNameController: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(e: Editable) {
            val s = e.toString().trim { it <= ' ' }
            if (arrayContainsElementIgnoringCases(existingSessionNames, s) ||
                    s.length == 0) {
                nameExists = true
                name.setBackgroundResource(android.R.color.holo_red_light)
            } else {
                nameExists = false
                name.setBackgroundResource(R.color.light_grey_half_transparent)
            }
        }
    }

    companion object {
        var PARAM_ID = "id"
        fun newInstance(session_id: Long): DialogEditSession {
            val args = Bundle()
            args.putLong(PARAM_ID, session_id)
            val fragment = DialogEditSession()
            fragment.arguments = args
            return fragment
        }
    }
}