package com.mtths.chartracker.dialogs

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Button
import android.widget.GridView
import androidx.fragment.app.DialogFragment
import com.mtths.chartracker.*
import com.mtths.chartracker.adapters.CharacterAdapter
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.utils.Helper
import kotlin.collections.ArrayList

class DialogChangeActiveChars : DialogFragment() {

    interface OnActiveCharsChangedListener {
        fun onActiveCharsChanged(active_chars: ArrayList<Character>)
    }

    //////////////////////////////////////// VARIABLES /////////////////////////////////////////////

    lateinit var charDisplay: GridView
    lateinit var ok_button: Button

    lateinit var dbHelper: DatabaseHelper
    lateinit var chars: ArrayList<Character>


    internal lateinit var charDisplayAdapter: CharacterAdapter
    private lateinit var loadAllCharsTask: LoadAllCharsTask
    var onActiveCharsChangedListener: OnActiveCharsChangedListener? = null

    ////////////////////////////////////////// METHODS /////////////////////////////////////////////

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_change_active_chars, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initInterfaceItems(view)

        dbHelper = DatabaseHelper(requireContext())
        loadAllCharsTask = LoadAllCharsTask()


        implementToggleCharActivity()

        initAdapter()

        fillAdapter()

        ok_button.setOnClickListener(OkOnClickListener())

    }

    inner class OkOnClickListener: View.OnClickListener {
        override fun onClick(v: View?) {

            var active_chars: ArrayList<Character> = ArrayList()
            for (char in chars){
                if (char.isActive) active_chars.add(char)
            }
            onActiveCharsChangedListener?.onActiveCharsChanged(active_chars)
            dismiss()
        }
    }

    companion object {
        fun newInstance(char_ids: LongArray): DialogChangeActiveChars {

            val args = Bundle()
            args.putLongArray("char_ids", char_ids)

            val fragment = DialogChangeActiveChars()
            fragment.arguments = args
            return fragment
    }

    }




    ////////////////////////////////////// PRIVATE METHODS /////////////////////////////////////////

    private fun initInterfaceItems(v: View) {
        charDisplay = v.findViewById(R.id.charDisplay) as GridView
        ok_button = v.findViewById(R.id.ok_button) as Button
    }

    private fun initAdapter() {
        this.context?.let {  charDisplayAdapter = CharacterAdapter(
                context = it,
                layoutResource = R.layout.character_no_exp,
                thumbNailQuality = 500,
                deactivateDeadAndRetiredChars = false,
                numColsGridView = charDisplay.numColumns) }
        charDisplayAdapter.noExp()
        charDisplay.adapter = charDisplayAdapter
    }

    internal fun fillAdapter() {
        loadAllCharsTask.cancel(true)
        loadAllCharsTask = LoadAllCharsTask()
        loadAllCharsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    inner class LoadAllCharsTask : AsyncTask<Unit, Unit, Unit>() {

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Unit?): Unit {
            chars = dbHelper.allCharactersInclDeleted
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: Unit?) {
            val active_char_ids = arguments?.getLongArray("char_ids")
            for (char in chars) {
                char.isActive = active_char_ids?.contains(char.id) ?: false
            }
            Helper.sortChars(chars);


            charDisplayAdapter.setChars(chars)
            charDisplayAdapter.notifyDataSetChanged()
        }


    }


    private fun implementToggleCharActivity() {
        charDisplay.onItemClickListener =
                AdapterView.OnItemClickListener { parent, _, position, _ ->
                    //get character associated to button clicked
                    val character = parent.getItemAtPosition(position) as Character
                    character.toggleActivity()
                    charDisplayAdapter.notifyDataSetChanged()
                }
    }



}