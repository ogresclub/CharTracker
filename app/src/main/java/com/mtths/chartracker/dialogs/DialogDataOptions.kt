package com.mtths.chartracker.dialogs

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import com.mtths.chartracker.*
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.dropbox.DbxHelper.Companion.DROPBOX_DEBUG_TAG
import com.mtths.chartracker.Global.chars
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.adapters.SettingsAdapter
import com.mtths.chartracker.databinding.DialogDataOptionsBinding
import com.mtths.chartracker.dataclasses.RuleSystem
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.dropbox.DropBoxIntent
import com.mtths.chartracker.dropbox.DropboxDialogFragment
import com.mtths.chartracker.parsers.GenesisCharacterImporter
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.FileUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class DialogDataOptions : DropboxDialogFragment() {
    private var only_import = false
    
    private var _binding: DialogDataOptionsBinding? = null

    private var importExportButtons: Array<Button> = arrayOf()
    private var xmlGenesisIconsDbButtons: Array<Button> = arrayOf()
    private var localDropboxButtons: Array<Button> = arrayOf()
    private lateinit var settingsAdapter: SettingsAdapter

    val binding: DialogDataOptionsBinding
        get() = _binding!!

    /**
     * means this dialog is only opened to export the database to a public folder
     * usually this is used when the app crashes on loading data from db to fix the database
     */
    private var dataRecovery = false
    var dbHelper: DatabaseHelper? = null
    var mOnDataImportedListener: OnDataImportedListener? = null

    val buttonLevelMap: HashMap<Button, Int> = HashMap()

    lateinit var prefs: PrefsHelper

    //////////////////////////////// ACTIVITY CONTRACTS /////////////////////////////////////


    private val importGenesisCharacter =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->

            // Handle the returned Uri
            if (uri != null) {

                GenesisCharacterImporter(requireActivity())
                    .importCharacter(uri = uri) {

                        dismiss()
                        (activity as? ActivityMain)?.updateGlobalLogDisplays(
                            filterSessions = false,
                            filterChars = false,
                            loadSessionDetails = true
                        )

                    }
            }
        }

    ///////////////////////////////////// INTERFACES //////////////////////////////////////////////
    interface OnDataImportedListener {
        /**
         * implement to update displays
         */
        fun onDataImported()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // This makes sure that the container activity has implemented
// the callback interface. If not, it throws an exception
        mOnDataImportedListener = try {
            context as OnDataImportedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnDataImportedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnDataImportedListener = null
    }

    ///////////////////////////////////// METHODS //////////////////////////////////////////////////
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = DialogDataOptionsBinding.inflate(inflater, container, false)
        val view = binding.root
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        only_import = arguments?.getBoolean(PARAM_ONLY_IMPORT, false) ?: false
        dataRecovery = arguments?.getBoolean(PARAM_DATA_RECOVERY, false) ?: false
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        if (only_import || dataRecovery) {
            dialog?.setCanceledOnTouchOutside(false)
            dialog?.setCancelable(false)
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (dataRecovery) {
            exportDbLocal()
            dismiss()

        } else {

            initVariables()
            //generically exportXmlToDropbox will be chosen as option
            dbHelper = getInstance(context)
            prefs = PrefsHelper(context)
            dbXHelper.mDropBoxResultReceiver = (activity as? ActivityMain?)?.mDropBoxResultReceiver
            /*
         * export Buttons disappear if import only option is active
         */

            setExportButtonsAccordingToSetting()


            when (prefs.xmlImport) {
                DatabaseHelper.IMPORT_KEEP_LOCAL -> updateImportButtons(binding.keepLocalButton)
                DatabaseHelper.IMPORT_OVERWRITE -> updateImportButtons(binding.overwriteButton)
            }

            if (prefs.xmlImportDeleteDb) binding.deleteDbButton.setBackgroundResource(R.drawable.button_holo_red_light)

            updateView(if (only_import) 2 else 1)
        }
    }

    private fun setExportButtonsAccordingToSetting() {
        when (prefs.xmlExport) {
            DatabaseHelper.EXPORT_ALL_LOGS -> updateExportButtons(binding.exportAllLogsButton)
            DatabaseHelper.EXPORT_CURRENT_LOGS -> updateExportButtons(binding.exportCurrentLogsButton)
            DatabaseHelper.EXPORT_CURRENT_SESSIONS -> updateExportButtons(binding.exportCurrentSessionsButton)
        }
    }

    ////////////////////////////// PRIVATE METHODS /////////////////////////////////////////////////



    private fun setExportOnClickListeners() {
        binding.exportAllLogsButton.setOnClickListener {
            prefs.xmlExport = DatabaseHelper.EXPORT_ALL_LOGS
            updateExportButtons(binding.exportAllLogsButton)
        }
        binding.exportCurrentLogsButton.setOnClickListener {
            prefs.xmlExport = DatabaseHelper.EXPORT_CURRENT_LOGS
            updateExportButtons(binding.exportCurrentLogsButton)
        }
        binding.exportCurrentSessionsButton.setOnClickListener {
            prefs.xmlExport = DatabaseHelper.EXPORT_CURRENT_SESSIONS
            updateExportButtons(binding.exportCurrentSessionsButton)
        }

    }

    private fun setImportOnClickListeners() {

        binding.deleteDbButton.setOnClickListener {
            prefs.xmlImportDeleteDb = !prefs.xmlImportDeleteDb
            setButtonActivity(binding.deleteDbButton, prefs.xmlImportDeleteDb,
                R.drawable.button_holo_red_light
            )
        }
        binding.overwriteButton.setOnClickListener {
            prefs.xmlImport = DatabaseHelper.IMPORT_OVERWRITE
            updateImportButtons(binding.overwriteButton)
        }
        binding.keepLocalButton.setOnClickListener {
            prefs.xmlImport = DatabaseHelper.IMPORT_KEEP_LOCAL
            updateImportButtons(binding.keepLocalButton)
        }

    }

    private fun setGeneralVisibilityLevel(level: Int) {
        val visibilities = arrayOf(View.VISIBLE, View.GONE, View.GONE, View.GONE)
        for (i in 0 until 4) {
            if (i + 1 <= level)
                visibilities[i] = View.VISIBLE
        }
        //level 2 buttons
        localDropboxButtons.forEach {
            it.visibility = visibilities[1]
            if (level < 3) {
                deactivateButton(it)
            }
        }

        //level 3 buttons
        xmlGenesisIconsDbButtons.forEach {
            it.visibility = visibilities[2]
            if (level < 4) {
                deactivateButton(it)
            }
        }

        binding.importOptionsLinearLayout.visibility = visibilities[3]
        binding.exportOptionsLinearLayout.visibility = visibilities[3]
    }




    private fun initVariables() {

        settingsAdapter = SettingsAdapter(
            activity = requireActivity(),
            switchContainer = binding.settingsContainer)
        settingsAdapter.inflateDataOptionsSettings()



        importExportButtons = arrayOf(
            binding.importButton,
            binding.exportButton)
        xmlGenesisIconsDbButtons = arrayOf(
            binding.xmlButton,
            binding.genesisButton,
            binding.iconsButton,
            binding.dbButton)
        localDropboxButtons = arrayOf(
            binding.localButton,
            binding.dropboxButton)

        buttonLevelMap.apply {
            importExportButtons.forEach {
                put(it, 2)
            }
            localDropboxButtons.forEach {
                put(it, 3)
            }
            xmlGenesisIconsDbButtons.forEach {
                put(it, 4)
            }
        }


        /*
        generically export xml from dropbox will be chosen
        but if only_import is selected it will be import
        */
        if (only_import) {
            toggleSelected(buttonArray = importExportButtons, selectThisButton = binding.importButton)
        }

        val buttonGroups = arrayOf(importExportButtons, xmlGenesisIconsDbButtons, localDropboxButtons)
        for (bGroup in buttonGroups) {
            bGroup.forEach { b ->
                b.setOnClickListener {
                    toggleSelected(buttonArray = bGroup, selectThisButton = b)
                    buttonLevelMap[b]?.let { level -> updateView(level) }
                }
            }
        }

        setExportOnClickListeners()
        setImportOnClickListeners()

        binding.cancelButton.setOnClickListener {
            log("Cancel Button pressed")
            dismiss()
        }
        binding.okButton.setOnClickListener {
            // ###################### importExport XML Dropbox ################################# //
            if (binding.importButton.isSelected
                && binding.xmlButton.isSelected
                && binding.dropboxButton.isSelected) {
                log(DROPBOX_DEBUG_TAG,"importing XML from Dropbox. open dialog to choose xml files")
                //make sure to prompt is not opening while dialog to choose xml is visible
                (activity as ActivityMain).PLEASE_CREATE_SESSION_PROMPT_ACTIVE = true

                doDropBoxStuff {
                    activity?.supportFragmentManager?.let {
                        DialogChooseFilesToDownloadFromDropBox.newInstance(
                            dropBoxReceiver = dbXHelper.mDropBoxResultReceiver)
                            .show(it, "choose_file_from_dropbox")
                    } ?: log(
                        DROPBOX_DEBUG_TAG,
                        "%%%%%%%%%%%%%% unable to start download because fragment manager is null")
                }
            }
            if (binding.exportButton.isSelected
                && binding.xmlButton.isSelected
                && binding.dropboxButton.isSelected) {
                log("exporting XML to Dropbox")

                when (PrefsHelper(context).xmlExport) {
                    DatabaseHelper.EXPORT_CURRENT_LOGS ->
                        DialogCustom.promptFile(this) { fileName ->
                            dbXHelper.startDropboxIntent(
                                task = DropBoxIntent.TASK_UPLOAD,
                                mode = DropBoxIntent.MODE_XML,
                                fileNameNoEnding = arrayOf(fileName)
                            )
                            dismiss()
                        }
                    DatabaseHelper.EXPORT_ALL_LOGS -> {
                        dbXHelper.startDropboxIntent(
                            task = DropBoxIntent.TASK_UPLOAD,
                            mode = DropBoxIntent.MODE_XML,
                            fileNameNoEnding = arrayOf("db")
                        )
                        dismiss()
                    }
                    DatabaseHelper.EXPORT_CURRENT_SESSIONS -> {

                        val activeSessions = if (PrefsHelper(context).filterSessions) {
                            dbHelper?.activeSessions
                        } else {
                            dbHelper?.allSessions
                        }

                        var totalCount = 0
                        var progress = 0

                        val b = Bundle()
                        b.putString(
                            DropBoxIntent.REF_TAG,
                            DropBoxIntent.OVERALL_PROGRESS_INDICATOR_TAG
                        )
                        activeSessions?.let {
                            val activeSessionsNotNull = it
                            totalCount = activeSessionsNotNull.size

                            /*
                             * initiate a progressBar showing the overall progress of
                             * the export
                             */
                            b.putString(DropBoxIntent.REF_MSG, "Exporting XML Files")
                            dbXHelper.mDropBoxResultReceiver?.send(DropBoxIntent.SHOW_PROGRESS_BAR, b)
                            dbXHelper.mDropBoxResultReceiver?.send(DropBoxIntent.SET_PROGRESSBAR_TEXT, b)

                            b.putInt(DropBoxIntent.REF_PROGRESS, -1)
                            b.putInt(DropBoxIntent.REF_MAX_PROGRESS, totalCount)
                            log("AAA", "#sesion to upload = $totalCount")
                            dbXHelper.mDropBoxResultReceiver?.send(DropBoxIntent.DO_PROGRESS, b)
                            b.putInt(DropBoxIntent.REF_PROGRESS, -1)

                            return@let activeSessionsNotNull
                        }?.forEach { session ->
                            log("AAA", "session name upload = ${session.name}")
                            progress++
                            dbXHelper.startDropboxIntent(
                                task = DropBoxIntent.TASK_UPLOAD,
                                mode = DropBoxIntent.MODE_XML,
                                overallProgressIndicator = 1,
                                fileNameNoEnding = arrayOf(session.name)
                            )
                        }
                        dismiss()


                    }

                }
            }
            //importExport Db Dropbox
            if (binding.importButton.isSelected
                && binding.dbButton.isSelected
                && binding.dropboxButton.isSelected) {
                log("importing Db from Dropbox")
                dbXHelper.startDropboxIntent(task = DropBoxIntent.TASK_DOWNLOAD,
                    mode = DropBoxIntent.MODE_DB,
                    fileNameNoEnding = arrayOf("db"))
                dismiss()
            }
            if (binding.exportButton.isSelected
                && binding.dbButton.isSelected
                && binding.dropboxButton.isSelected) {
                log("exporting Db to Dropbox")
                dbXHelper.startDropboxIntent(DropBoxIntent.TASK_UPLOAD, DropBoxIntent.MODE_DB)
                dismiss()
            }
            //importExport XML Local
            if (binding.importButton.isSelected
                && binding.xmlButton.isSelected
                && binding.localButton.isSelected) {
                log("importing from local XML")
                importFromLocalXML()
                dismiss()
            }
            if (binding.exportButton.isSelected
                && binding.xmlButton.isSelected
                && binding.localButton.isSelected) {
                log("Exporting to local XML")
                exportToLocalXML()
                dismiss()
            }
            //importExport Db Local
            if (binding.importButton.isSelected
                && binding.dbButton.isSelected
                && binding.localButton.isSelected) {
                log("importing local Db")
                importLocalDb()
                dismiss()
            }
            if (binding.exportButton.isSelected
                && binding.dbButton.isSelected
                && binding.localButton.isSelected) {
                log("Exporting local DB")
                exportDbLocal()
                dismiss()
            }
            //import export icons
            if (binding.exportButton.isSelected
                && binding.iconsButton.isSelected
                && binding.dropboxButton.isSelected) {
                log("exporting icons to dropbox")
                exportIconsToDropbox()
            }
            if (binding.importButton.isSelected
                && binding.iconsButton.isSelected
                && binding.dropboxButton.isSelected) {
                log("importing icons from dropbox")
                importIconsFromDropbox()
            }
            if (binding.importButton.isSelected
                && binding.iconsButton.isSelected
                && binding.localButton.isSelected) {
                log("importing icons locally")
                importIconsFromLocalStorage()
            }
            if (binding.importButton.isSelected
                && binding.localButton.isSelected
                && binding.genesisButton.isSelected) {
                log("import genesis character")
                importGenesisCharacter.launch("*/*")
            }
        }
    }

    private fun importLocalDb() {
        var chooseFile = Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "*/*"
            addCategory(Intent.CATEGORY_OPENABLE)
        }
        chooseFile = Intent.createChooser(chooseFile, getString(R.string.select_database_file_prompt))
        activity?.startActivityForResult(chooseFile, FILE_SELECT_CODE_DB_IMPORT)
    }

    private fun exportDbLocal() {
        var chooseFile = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
        }
        chooseFile = Intent.createChooser(chooseFile, getString(R.string.select_folder_prompt))
        activity?.startActivityForResult(chooseFile, FILE_SELECT_CODE_DB_EXPORT)
    }

    private fun importFromLocalXML() {

        fun chooseFolder() {
            var chooseFile = Intent(Intent.ACTION_GET_CONTENT).apply {
                type = "text/xml"
                addCategory(Intent.CATEGORY_OPENABLE)
            }
            chooseFile = Intent.createChooser(chooseFile, getString(R.string.select_xml_file_prompt))
            activity?.startActivityForResult(chooseFile, FILE_SELECT_CODE_XML_IMPORT_LOCAL)
        }

        prefs.localCharTrackerFolder?.let { folder ->
            if (prefs.useStoredCharTrackerFolder) {
                try {
                    (activity as ActivityMain).confirmDeleteDbAndStartImportXmlTask(
                        ActivityMain.ImportXmlParam(
                            xmlLocalUris = listOf(),
                            import_option = prefs.xmlImport,
                            local = true,
                            folder = folder
                        )
                    )
                } catch (e: Exception) {
                    Log.e(
                        "EXPORT_TO_XML",
                        "unable cast activity to ActivityMain in order to run ImportFromXML Task"
                    )
                    e.printStackTrace()
                }
            } else {
                chooseFolder()
            }
        } ?: run {
           chooseFolder()
        }
    }

    private fun exportToLocalXML() {

        fun chooseFolder() {

            var chooseFile = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
            }
            chooseFile = Intent.createChooser(chooseFile, getString(R.string.select_folder_prompt))
            activity?.startActivityForResult(chooseFile, FILE_SELECT_CODE_XML_EXPORT_LOCAL)
        }

        prefs.localCharTrackerFolder?.let { folder ->
            if (prefs.useStoredCharTrackerFolder) {
                try {
                    (activity as ActivityMain).runExportToXMLTask(
                        folder = folder,
                        includeIcons = true
                    )
                } catch (e: Exception) {
                    Log.e(
                        "EXPORT_TO_XML",
                        "unable cast activity to ActivityMain in order to run ExportToXML Task"
                    )
                    e.printStackTrace()
                }
            } else {
                chooseFolder()
            }
        } ?: run {
            chooseFolder()
        }
    }

    private fun importIconsFromLocalStorage() {

        fun chooseFolder() {
            var chooseFile = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
            }
            chooseFile = Intent.createChooser(chooseFile, getString(R.string.select_folder_prompt))
            activity?.startActivityForResult(chooseFile, FILE_SELECT_CODE_ICONS_IMPORT_LOCAL)
            dismiss()
        }

        prefs.localCharTrackerFolder?.let { folder ->
            if (prefs.useStoredCharTrackerFolder) {
                CoroutineScope(Dispatchers.Main).launch {
                    try {
                        val a = (activity as ActivityMain)
                        a.showProgressBarLoading()
                        dbHelper?.allCharactersInclDeleted?.map { it.iconFileName }
                            ?.filter { it.isNotBlank() }?.let { iconNames ->
                            FileUtils.importIconsFromLocalStorage(
                                charTrackerFolder = folder,
                                context = requireContext(),
                                charIconFileNamesToImport = iconNames

                            )
                        }
                        a.hideProgressBarLoading()
                        dismiss()
                        a.updateGlobalLogDisplays(
                            filterSessions = false, filterChars = false, loadSessionDetails = false
                        )
                    } catch (e: java.lang.Exception) {
                        Log.e(
                            "IMPORT_ICONS_LOCALLY",
                            "unable cast activity to ActivityMain in order to run ExportToXML Task"
                        )
                    }
                }
            } else {
                chooseFolder()
            }
        } ?: run {
            chooseFolder()
        }
    }

    private fun exportIconsToDropbox() {
        dbXHelper.startDropboxIntentWthEnding(
            task = DropBoxIntent.TASK_UPLOAD,
            mode = DropBoxIntent.MODE_CHAR_ICON,
            fileNameWithEnding = chars.filter { it.iconFileName.isNotEmpty() }.map { it.iconFileName }.toTypedArray())

    }

    private fun importIconsFromDropbox() {
        Log.d(DROPBOX_DEBUG_TAG, "staring intent to download icons from Dropbox")
        dbXHelper.startDropboxIntentWthEnding(
            task = DropBoxIntent.TASK_DOWNLOAD,
            mode = DropBoxIntent.MODE_CHAR_ICON,
            fileNameWithEnding = chars.filter { it.iconFileName.isNotEmpty() }.map { it.iconFileName }.toTypedArray())
    }


    ///////////////////////////     INTERNAL METHODS    //////////////////////////////////

    private fun updateExportButtons(pressedButton: Button) {
        markButton(pressedButton, arrayOf(
            binding.exportAllLogsButton,
            binding.exportCurrentLogsButton,
            binding.exportCurrentSessionsButton),
            R.drawable.button_violet_half_transparent
        )
    }

    private fun setButtonActivity(button: Button, active: Boolean, backgroundResId: Int) {
        if (active) activateButton(
            button = button,
            backgroundResId = backgroundResId)
        else deactivateButton(button)
    }


    private fun updateImportButtons(pressedButton: Button) {
        markButton(pressedButton, arrayOf(binding.overwriteButton, binding.keepLocalButton),
            R.drawable.button_violet_half_transparent
        )
    }

    /**
     * mark one button out of a given array of buttons
     */
    private fun markButton(
        pressedButton: Button,
        buttons: Array<Button>, backgroundResId: Int) {

        for (button: Button in buttons) {
            if (button.id != pressedButton.id) {
                deactivateButton(button)
            }
        }
        pressedButton.setBackgroundResource(backgroundResId)

    }

    private fun deactivateButton(button: Button, gone: Boolean = false) {
        button.setBackgroundResource(R.drawable.button_light_grey)
        button.isSelected = false
        if (gone) {
            button.visibility = View.GONE
        }
    }
    private fun activateButton(button: Button, backgroundResId: Int) {
        button.setBackgroundResource(backgroundResId)
        button.isSelected = true
        button.visibility = View.VISIBLE
    }



    private fun updateView(visibilityLevel: Int) {

        fun manageVisibilityAccordingToRuleSystem() {
            //manage buttons visibility depending of rule system
            if (prefs.ruleSystem != RuleSystem.rule_system_splittermond) {
                binding.genesisButton.visibility = View.GONE
            }
        }

        fun manageButtonVisibilities() {
            // mange xml options visibility
            if (!binding.xmlButton.isSelected)  {
                binding.exportOptionsLinearLayout.visibility = View.GONE
                binding.importOptionsLinearLayout.visibility = View.GONE
            } else {
                if (binding.exportButton.isSelected) {

                    binding.importOptionsLinearLayout.visibility = View.GONE
                    if (binding.localButton.isSelected) {
                        binding.exportCurrentLogsButton.visibility = View.GONE
                        binding.exportCurrentSessionsButton.visibility = View.GONE
                        updateExportButtons(binding.exportAllLogsButton)
                    } else {
                        binding.exportCurrentLogsButton.visibility = View.VISIBLE
                        binding.exportCurrentSessionsButton.visibility = View.VISIBLE
                        setExportButtonsAccordingToSetting()
                    }
                }
                if (binding.importButton.isSelected) {
                    binding.exportOptionsLinearLayout.visibility = View.GONE
                }

            }

            //manage local options
            if (binding.localButton.isSelected && binding.exportButton.isSelected) {
                binding.iconsButton.visibility = View.GONE
                binding.genesisButton.visibility = View.GONE
            }
            //manage dropbox options
            if (binding.dropboxButton.isSelected) {
                binding.genesisButton.visibility = View.GONE
            }
            //manage export options
            if (binding.exportButton.isSelected)  {
                binding.genesisButton.visibility = View.GONE
            }

            if (only_import) {
                binding.exportButton.visibility = View.GONE
                binding.importButton.visibility = View.GONE
                binding.cancelButton.visibility = View.GONE
                binding.deleteDbButton.visibility = View.GONE
                binding.iconsButton.visibility = View.GONE
            }
        }

        fun manageSettingsVisibility() {
            binding.settingsContainer.visibility = if (
                binding.localButton.isSelected && binding.xmlButton.isSelected
                ) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        setGeneralVisibilityLevel(visibilityLevel)
        manageVisibilityAccordingToRuleSystem()
        manageButtonVisibilities()
        manageSettingsVisibility()







    }



    companion object {

        /////////////////////////////////////////// VARIABLES ///////////////////////////////////////////
        //PARAMETERS
        /**
         * this is used, when no session exists and from new Session one chooses to import
         * data and come here. Then all export Buttons shall be GONE.
         */
        const val PARAM_ONLY_IMPORT = "only_import"
        const val PARAM_DATA_RECOVERY = "data_recovery"

        //Request Permission requst codes
        const val PERMISSIONS_REQUEST_EXTERNAL_STORAGE_EXPORT_XML = 0



        //activity result codes
        const val FILE_SELECT_CODE_DB_IMPORT = 0
        const val FILE_SELECT_CODE_DB_EXPORT = 1
        const val FILE_SELECT_CODE_XML_IMPORT_LOCAL = 2
        const val FILE_SELECT_CODE_XML_EXPORT_LOCAL = 3
        const val FILE_SELECT_CODE_ICONS_IMPORT_LOCAL = 4

        const val FILE_NAME_XML_EXPORT = "chartracker_db.xml"
        const val FILE_NAME_XML_MIME_TYPE = "application/xml"
        const val FILE_NAME_DB_EXPORT = "database.db"
        const val EXPORT_DIRECTORY_FOLDER_NAME = "chartracker"

        const val FRAGMENT_TAG = "fragment_tag_data_options"

        /**
         * standard Instance opens window to choose if you want to import or export.
         * @return
         */
        fun newInstance(onlyImport: Boolean = false, dataRecovery: Boolean = false): DialogDataOptions {
            val args = Bundle()
            args.putBoolean(PARAM_ONLY_IMPORT, onlyImport)
            args.putBoolean(PARAM_DATA_RECOVERY, dataRecovery)
            val fragment = DialogDataOptions()
            fragment.arguments = args
            return fragment
        }


        /**
         * select the given Button if contained in buttonArray and deselect all the others.
         * then color button according to selection
         */
        fun toggleSelected(buttonArray: Array<Button>, selectThisButton: Button? = null) {
            selectThisButton?.let { selectedButton ->
                buttonArray.forEach {
                    it.isSelected = (it.id == selectedButton.id)
                }
            }
            buttonArray.forEach {
                if (it.isSelected) {
                    it.setBackgroundResource(R.drawable.button_holo_green_light)
                } else {
                    it.setBackgroundResource(R.drawable.button_light_grey)
                }
            }
        }

    }
}