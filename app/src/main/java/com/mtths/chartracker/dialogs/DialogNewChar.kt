package com.mtths.chartracker.dialogs

import android.content.Context
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.MultiAutoCompleteTextView
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts

import androidx.fragment.app.DialogFragment
import com.mtths.chartracker.*
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.databinding.DialogNewCharBinding
import com.mtths.chartracker.utils.Helper.arrayContainsElementIgnoringCases
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.dataclasses.*
import com.mtths.chartracker.utils.Helper
import com.mtths.chartracker.utils.Helper.capitalize
import com.mtths.chartracker.utils.HttpUtils
import com.mtths.chartracker.parsers.SplittermondParser
import com.mtths.chartracker.preferences.PrefsHelper
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class DialogNewChar : DialogFragment() {

    private var _binding: DialogNewCharBinding? = null
    val binding: DialogNewCharBinding
        get() = _binding!!

    val attributeEditTextMap = HashMap<String, EditText>()
    private lateinit var classAdapter: ArrayAdapter<DndClass>
    private lateinit var archetypeAdapter: ArrayAdapter<DndArchetype>
    private lateinit var subraceAdapter: ArrayAdapter<Subrace>
    private lateinit var raceAdapter: ArrayAdapter<Race>
    private lateinit var prefs: PrefsHelper
    lateinit var newCharacter: Character
    private var arrayOfKnownChars: ArrayList<String>? = null
    private var deletedCharNames: ArrayList<String>? = null
    private var dbHelper: DatabaseHelper? = null
    private var okToCreate = true
    private var customIconUri: Uri? = null
    private var mHashLoader: HashLoader? = null
    private var finishloadingCallCounter: Int = 0
    //Listeners
    var mOnNewCharCreatedListener: OnNewCharCreatedListener? = null
    var mOnLoadingListener: OnLoadingListener? = null

    val pickVisualMedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
        // Process URIs
        Helper.log("ICON_STUFF", "onResult PickIcon")
        uri?.let {
            context?.also{
                Helper.log("ICON_STUFF", "uri = $uri")
                binding.icon.setImageURI(uri)
                customIconUri = uri
            }
        }

    }

    ///////////////////////////////////// INTERFACES ///////////////////////////////////////////////
    interface OnNewCharCreatedListener {
        fun onNewCharCreated(newChar: Character, initialLogs: ArrayList<LogEntry>)
    }
    interface OnLoadingListener {
        fun onStartLoading()
        fun onEndLoading()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // This makes sure that the container activity has implemented
// the callback interface. If not, it throws an exception
        mOnNewCharCreatedListener = try {
            context as OnNewCharCreatedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnNewCharCreatedListener")
        }
        mOnLoadingListener = try {
            context as OnLoadingListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnLoadingListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnNewCharCreatedListener = null
        mOnLoadingListener = null
        mHashLoader?.cancel(true)
        mHashLoader = null
        while (finishloadingCallCounter > 0) {
            finishloadingCallCounter--
            mOnLoadingListener?.onEndLoading()
        }
    }

    ////////////////////////////////// METHODS ////////////////////////////////////////////////////
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = DialogNewCharBinding.inflate(inflater, container, false)
        val view = binding.root
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dbHelper = getInstance(context)
        prefs = PrefsHelper(context)
        newCharacter = Character().apply { isActive = true }
        initInterfaceItems()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.icon.setImageResource(newCharacter.getSimpleIconResource())

        if (prefs.rulesOgresClub) {
            mHashLoader = HashLoader()
            mHashLoader?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        }

        arrayOfKnownChars = dbHelper?.charNames
        deletedCharNames = dbHelper?.deletedCharacterNames
        //dynamically make total exp and current exp editTexts in/visible depending on if character
//does already exist.
//Also change text of button to create
        setOnChangeOfCharName()
        setTextWatchersForClassArchetypeAndRace()
        setOnItemClickListenersForClassArchetypeAndSubrace()
        initAdapters()
        implementOpenCharIconMenu()
        implementCreateCharacterButton()
        binding.cancelButton.setOnClickListener {
            dismiss()
            finishloadingCallCounter--
            mOnLoadingListener?.onEndLoading()
        }
        if (prefs.rulesSR) {
            binding.editTextArchetype.setTokenizer(MultiAutoCompleteTextView.CommaTokenizer())
            binding.editTextArchetype.hint = "Archetypes"
        }
    }

    ///////////////////////////////////// PRIVATE METHODS //////////////////////////////////////////
    private fun initInterfaceItems() {
        binding.editTextCharName.requestFocus()
        buildAttributeViews()
        setVisibilityAccordingToRuleSystemData()
        val imgheight = binding.icon.height
        val clsHeight = binding.editTextClass.height
        Log.d("HJFLHUI", "height")
        if (binding.iconLevelContainer.height < clsHeight + imgheight) {

            binding.leftUpperContainer.layoutParams = FrameLayout.LayoutParams(
                0,
                imgheight + clsHeight
            )
        }
    }

    private fun setVisibilityAccordingToRuleSystemData() {
        prefs.ruleSystem.newCharData.apply {
            if (!showArchetypes) binding.editTextArchetype.visibility = View.GONE
            if (!showClass) binding.editTextClass.visibility = View.GONE
            if (!showLevel) binding.editTextLevel.visibility = View.GONE
            if (!showNotes) binding.editTextStatNotes.visibility = View.GONE
            if (!showRace) binding.editTextRace.visibility = View.GONE
            if (!showSubRace) binding.editTextSubrace.visibility = View.GONE
        }
    }

    private fun buildAttributeViews() {


        fun getEditText(attr: String): EditText {
            return EditText(context).apply {
                attributeEditTextMap[attr] = this
                hint = attr.uppercase()
//                textSize = context.resources.getDimension(R.dimen.dialog_new_char_attribute_hint_text_size)
                inputType = InputType.TYPE_CLASS_NUMBER
                setEms(attr.length)
                gravity = Gravity.CENTER
            }
        }

        val attrList = context?.resources?.getStringArray(prefs.ruleSystem.attributesListId) ?: arrayOf()

        attrList.forEachIndexed { index, attr ->

            if (index < attrList.size - 1 && index % 2 == 0) {
                //there is still one attribute coming after this one
                binding.attributesContainer.addView(
                    LinearLayout(context).apply {
                        layoutParams = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                        listOf(
                            attr,
                            attrList[index+1]
                        ).forEach { pAttr ->
                            addView(
                                getEditText(pAttr).apply {
                                    layoutParams = LinearLayout.LayoutParams(
                                        0,
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        1f
                                    )
                                }
                            )
                        }

                    }
                )

            }  else if (index == attrList.size - 1 && index % 2 == 0) {
                // last one, but has no follow up and was not already dealt with by the one before
                binding.attributesContainer.addView(
                    getEditText(attr).apply {
                        layoutParams = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    }
                )

            }

        }


    }

    private fun initAdapters() {
        raceAdapter = ArrayAdapter(requireContext(), R.layout.auto_complete_entry)
        subraceAdapter = ArrayAdapter(requireContext(), R.layout.auto_complete_entry)
        classAdapter = ArrayAdapter(requireContext(), R.layout.auto_complete_entry)
        archetypeAdapter = ArrayAdapter(requireContext(), R.layout.auto_complete_entry)
        binding.editTextRace.setAdapter(raceAdapter)
        binding.editTextRace.always_show = true
        binding.editTextSubrace.setAdapter(subraceAdapter)
        binding.editTextSubrace.always_show = true
        binding.editTextClass.setAdapter(classAdapter)
        classAdapter.addAll(Global.classesFromOgresClub)
        binding.editTextClass.always_show = true
        binding.editTextArchetype.setAdapter(archetypeAdapter)
        archetypeAdapter.addAll(Global.archetypesFromOgresClub)
        binding.editTextArchetype.always_show = true

        fillArchetypeAdapter("")
        fillClassAdapter("")
        fillSubraceAdapter("")
        fillRaceAdapter("")
    }

    private fun fillSubraceAdapter(current_race_name: String) {
        subraceAdapter.clear()
        if (current_race_name.isNotBlank()) {
            Helper.addAllToList(
                context = requireContext(),
                l = ArrayList(),
                addForOgreClub = dbHelper?.getSubracesRelatedToRace(current_race_name),
                addForE5 = null
            ).forEach {
                subraceAdapter.add(Subrace(it))
            }

        } else {
            Helper.addAllToList(
                context = requireContext(),
                l = ArrayList(),
                addForOgreClub = Global.subracesFromOgresClub,
                addForE5 = null
            ).forEach {
                subraceAdapter.add(it)
            }
        }
        archetypeAdapter.notifyDataSetChanged()
    }

    private fun fillRaceAdapter(current_subrace_name: String) {
        raceAdapter.clear()

        if (current_subrace_name.isNotBlank()) {
            Helper.addAllToList(
                context = requireContext(),
                l = ArrayList(),
                addForOgreClub = Global.racesFromOgresClub.filter { r ->
                    r.subraces.find { sr ->
                        sr.equals(
                            current_subrace_name,
                            ignoreCase = true
                        )
                    } != null
                },
                addForE5 = null
            ).forEach {
                raceAdapter.add(it)
            }
        } else {
            Helper.addAllAccordingToRuleSystem(
                context = requireContext(),
                l = ArrayList(),
                data = HashMap<RuleSystem, List<Race>>().apply {
                    put(RuleSystem.rule_system_ogres_club, Global.racesFromOgresClub)
                    put(RuleSystem.rule_system_Dnd_E5, Global.racesFromResources)
                    put(
                        RuleSystem.rule_system_sr5,
                        resources.getStringArray(R.array.races_shadworun)
                            .map { Race(name = it) }
                    )
                    put(
                        RuleSystem.rule_system_sr6,
                        resources.getStringArray(R.array.races_shadworun)
                            .map { Race(name = it) }
                    )
                }
            ).forEach {
                raceAdapter.add(it)
            }
        }
        raceAdapter.notifyDataSetChanged()
    }

    /**
     * add archetypes related to class to adapter or us all archetypes, depending on rulesystem
     */
    private fun fillArchetypeAdapter(current_cl_name: String) {
        archetypeAdapter.clear()
        var gotArchetypesFromClass = false
        if (current_cl_name.isNotBlank()) {
            val cls: DndClass? = when {
                prefs.rulesOgresClub -> dbHelper?.getClass(current_cl_name)
                prefs.rulesE5 ->
                    Global.classesFromResources.find {
                        it.name.equals(current_cl_name, ignoreCase = true)
                    }
                else -> null
            }
            cls?.archetype_names?.
            also { gotArchetypesFromClass = true }?.
            forEach { archetype_name ->
                archetypeAdapter.add(DndArchetype(name = archetype_name))

            }
        }
        if (!gotArchetypesFromClass) {
            Helper.addAllAccordingToRuleSystem(
                context = requireContext(),
                l = ArrayList(),
                data = HashMap<RuleSystem, List<DndArchetype>>().apply {
                    put(RuleSystem.rule_system_ogres_club, Global.archetypesFromOgresClub)
                    put(RuleSystem.rule_system_Dnd_E5, Global.archetypesFromResources)
                    put(
                        RuleSystem.rule_system_sr5,
                        resources.getStringArray(R.array.archetypes_shadworun)
                            .map { DndArchetype(name = it) }
                    )
                    put(
                        RuleSystem.rule_system_sr6,
                        resources.getStringArray(R.array.archetypes_shadworun)
                            .map { DndArchetype(name = it) }
                    )
                }
            ).forEach {
                archetypeAdapter.add(it)
            }
        }
        //            Helper.log("DIALOG_NEW_CHAR", "archetypes from ogres club = " + Global.archetypesFromOgresClub.toString());

        archetypeAdapter.notifyDataSetChanged()
    }

    fun fillClassAdapter(current_archetype_name: String) {
        classAdapter.clear()
        var gotClassesFromArchetype = false
        if (current_archetype_name.isNotBlank()) {
            val archetype = when {
                prefs.rulesOgresClub -> dbHelper?.getArchetype(current_archetype_name)
                prefs.rulesE5 ->
                    Global.archetypesFromResources.find {
                        it.name.equals(current_archetype_name, ignoreCase = true)
                    }
                else -> null
            }
            archetype?.classes?.
            also { gotClassesFromArchetype = true }?.
            forEach {a_name ->
                classAdapter.add(DndClass(a_name))
            }
        }
        log("CLASSES", "gotClassesFromArchetype: $gotClassesFromArchetype")
        if (!gotClassesFromArchetype) {

            Helper.addAllToList(
                context = requireContext(),
                l = ArrayList(),
                addForOgreClub = Global.classesFromOgresClub,
                addForE5 = Global.classesFromResources
            ).forEach {
                classAdapter.add(it)
            }
        }
        //            Helper.log("DIALOG_NEW_CHAR", "classes on ogres club = " + Global.classesFromOgresClub.toString());

        classAdapter.notifyDataSetChanged()
    }

    private fun setTextWatchersForClassArchetypeAndRace() {
        binding.editTextRace.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val current_race_name = s.toString().trim()
                fillSubraceAdapter(current_race_name)
            }
        })
        binding.editTextSubrace.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val current_subrace_name = s.toString().trim()
                fillRaceAdapter(current_subrace_name)
            }
        })
        binding.editTextClass.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val current_cl_name = s.toString().trim()
                fillArchetypeAdapter(current_cl_name)
            }
        })
        binding.editTextArchetype.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val current_arch_name = s.toString().trim()
                fillClassAdapter(current_arch_name)
            }
        })

    }

    /**
     * this is basically the same as the text watchers, but the don't get triggered on auto
     * complete so we have to trigger by hand using this
     */
    private fun setOnItemClickListenersForClassArchetypeAndSubrace() {
        binding.editTextRace.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val current_race_name = parent.getItemAtPosition(position).toString().trim()
            fillSubraceAdapter(current_race_name)
            binding.editTextRace.setText(current_race_name)
            binding.editTextRace.setSelection(binding.editTextRace.length())
        }
        binding.editTextSubrace.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val current_subrace_name = parent.getItemAtPosition(position).toString().trim()
            binding.editTextSubrace.setText(current_subrace_name)
            binding.editTextSubrace.setSelection(binding.editTextSubrace.length())
        }
        binding.editTextClass.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val current_cl_name = parent.getItemAtPosition(position).toString().trim()
            fillArchetypeAdapter(current_cl_name)
            binding.editTextClass.setText(current_cl_name)
            binding.editTextClass.setSelection(binding.editTextClass.length())
        }
        if (prefs.rulesDnD) {
            binding.editTextArchetype.onItemClickListener = OnItemClickListener { parent, view, position, id ->
                val current_arch_name = parent.getItemAtPosition(position).toString().trim()
                fillClassAdapter(current_arch_name)
                binding.editTextArchetype.setText(current_arch_name)
                binding.editTextArchetype.setSelection(binding.editTextArchetype.length())
            }
        }
    }

    private fun implementCreateCharacterButton() {
        binding.applyButton.setOnClickListener {
            newCharacter.name = binding.editTextCharName.text.toString().trim()
            var message = ""
            //check if a CharName was entered, else ask to do so
            if (newCharacter.name.isEmpty()) {
                message = "Please enter a name"
            } else if (arrayContainsElementIgnoringCases(arrayOfKnownChars, newCharacter.name)) { //char already exists
                message = "Character with this name already exists"
            } else if (arrayContainsElementIgnoringCases(deletedCharNames, newCharacter.name)) {
                DialogCustom.getAlterDialogBuilder(context)
                    .setTitle("Restore character")
                    .setMessage("Do you want to restore character " + newCharacter.name + "?")
                    .setPositiveButton(R.string.yes) { dialog, whichButton ->

                        dbHelper?.getCharacter(newCharacter.name)?.let { c ->
                            c.isDeleted = false
                            dbHelper?.updateCharacter(character = c, keep_last_update_date = false)
                            mOnNewCharCreatedListener?.onNewCharCreated(c, ArrayList())
                            Toast.makeText(context, "Character restored", Toast.LENGTH_SHORT).show()
                        }
                            ?: Toast.makeText(context, "No Character with this name in db", Toast.LENGTH_SHORT).show()

                        dialog.dismiss()
                        dismiss()
                    }
                    .setNegativeButton(R.string.no) { dialog, which -> dialog.dismiss() }.show()
            } else {
                //char doesn't exist
                okToCreate = true
                var race_name = binding.editTextRace.text.toString().trim()
                if (race_name.isBlank()) race_name = Character.UNDEFINED
                var race = dbHelper?.getRace(race_name)
                if (race == null) {
                    race = Race(name = race_name)
                }
                newCharacter.race = race

                var subrace_name = binding.editTextSubrace.text.toString().trim()
                if (subrace_name.isBlank()) subrace_name = Character.UNDEFINED
                var subrace = dbHelper?.getSubrace(subrace_name)
                if (subrace == null) {
                    subrace = Subrace(name = subrace_name)
                }
                newCharacter.subrace = subrace

                var class_name = binding.editTextClass.text.toString().trim()
                if (class_name.isBlank()) class_name = Character.UNDEFINED
                var cl = dbHelper?.getClass(class_name)
                if (cl == null) {
                    cl = DndClass(name = class_name)
                }
                newCharacter.dndClass = cl

                var archetype_name = binding.editTextArchetype.text.toString().trim().removeSuffix(",")
                if (archetype_name.isBlank()) archetype_name = Character.UNDEFINED
                var archetype = dbHelper?.getArchetype(archetype_name)
                if (archetype == null) {
                    archetype = DndArchetype(name = archetype_name)
                }
                newCharacter.archetype = archetype

                customIconUri?.let { customIconUri ->
                    context?.let { context ->
                        val iconFileName = newCharacter.copyIconToInternalAppFolder(context, customIconUri)
                        newCharacter.iconFileName = iconFileName
                    }
                }

                newCharacter.ATTRIBUTE_POINT_EXP_COST = 500
                val initialAttributesLog = generateInitialLog()
                if (okToCreate) { //                        Helper.log("CREATE NEW CHARACTER", "ok to create");
                    dbHelper?.createCharacter(newCharacter)?.let { id ->
                        newCharacter.id = id
                        message = "New Character created."
                        createInitialLogs(
                            initialLogs = listOf(initialAttributesLog),
                            newCharId = id,
                            context = requireContext())
                        mOnNewCharCreatedListener?.onNewCharCreated(
                            newChar = newCharacter, initialLogs = arrayListOf(initialAttributesLog))
                        dismiss()
                    }
                }
            }
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun generateInitialLog(): LogEntry {
        val attributes = mutableListOf<SplittermondParser.ValueNamePair>()

        context?.resources?.getStringArray(prefs.ruleSystem.attributesListId)?.forEach { attr ->
            attributes.add(
                SplittermondParser.ValueNamePair(
                    name = capitalize(attr),
                    value = attributeEditTextMap[attr]?.text.toString().toIntOrNull() ?: 0
                )
            )
        }


        return generateInitialLogValues(
            attributes = attributes,
            note = binding.editTextStatNotes.text.toString().trim(),
            level = binding.editTextLevel.text.toString().trim().toIntOrNull(),
            className = newCharacter.dndClass.name.replace(Character.UNDEFINED, ""),
            context = context)
    }

    private fun setOnChangeOfCharName() {
        binding.editTextCharName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(e: Editable) {
                val s = e.toString().trim { it <= ' ' }
                if (s.isEmpty() ||
                    arrayContainsElementIgnoringCases(arrayOfKnownChars, s)) {
                    setDisplaysInvalidName()
                    binding.applyButton.visibility = View.GONE
                } else {
                    binding.editTextCharName.setBackgroundResource(android.R.color.transparent)
                    binding.applyButton.visibility = View.VISIBLE
                    binding.applyButton.setText(R.string.create)
                }
                if (arrayContainsElementIgnoringCases(deletedCharNames, s)) {
                    setDisplaysInvalidName()
                    binding.applyButton.visibility = View.VISIBLE
                    binding.applyButton.setText(R.string.restore)
                    Toast.makeText(context, "Deleted character with this name found. Press button to restore character.", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }



    private fun implementOpenCharIconMenu() {
        binding.icon.setOnClickListener {
            fragmentManager?.let { fragmentManager ->
                DialogCharIcon.newInstance().show(
                    fragmentManager, "newCharChooseIcon"
                ) }

        }
        binding.icon.setOnLongClickListener {
            pickVisualMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
            return@setOnLongClickListener true

        }
    }

    private fun setDisplaysInvalidName() {
        binding.editTextCharName.setBackgroundResource(android.R.color.holo_red_light)
    }

    fun setIcon(iconName: String) {
        binding.icon.setImageResource(Global.charIconsMap[iconName]?: Character.ERROR_ICON)
        newCharacter.iconName = iconName
    }

    inner class HashLoader(): AsyncTask<Unit, Unit, String?>() {

        @Deprecated("Deprecated in Java")
        override fun onPreExecute() {
            mOnLoadingListener?.onStartLoading()
            finishloadingCallCounter++
        }
        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Unit?): String? {
            return HttpUtils.getHashOflatestCommit()?.hash
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: String?) {
            if (result == null) {
                Toast.makeText(context, "hash download failed", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(context, "current hash = $result", Toast.LENGTH_LONG).show()
                newCharacter.currentCommit = HttpUtils.CommitWithLink(hash = result)
                mOnLoadingListener?.onEndLoading()
                finishloadingCallCounter--
            }
        }

        @Deprecated("Deprecated in Java")
        override fun onCancelled() {
            super.onCancelled()
            mOnLoadingListener?.onEndLoading()
            finishloadingCallCounter--
        }
    }

    companion object {
        fun newInstance(): DialogNewChar {
            val args = Bundle()
            val fragment = DialogNewChar()
            fragment.arguments = args
            return fragment
        }


        fun generateInitialLogValues(
            attributes: List<SplittermondParser.ValueNamePair>,
            note: String,
            level: Int?,
            className: String,
            timeOffsetInMillis: Long = 0,
            date: Date = Calendar.getInstance().time,
            context: Context?): LogEntry {

            fun log(msg: String) {
//            Log.e("INITIAL_ATTRIBUTES", "~~~~~~~ $msg")
            }

            var postText  = ""
            //because of input type it can't be anything else than a number so we can parse.
            if (PrefsHelper(context).ruleSystem.hasLevel) {
                level?.let {
                    postText += "%s $className $it".format(context?.getString(R.string.filter_constraint_class))
                }
            }

            log("level: $postText")

            return generateStatsLog(
                statCPIfilterConstraintId = R.string.filter_constraint_attribute,
                data = attributes,
                preText = ".ignore Start Values: $note",
                postText = postText,
                timeOffsetInMillis = timeOffsetInMillis,
                date = date,
                context = context
            )
        }
        fun generateStatsLog(
            statCPIfilterConstraintId: Int,
            data: List<SplittermondParser.ValueNamePair>,
            preText: String = "",
            postText: String = "",
            timeOffsetInMillis: Long = 0,
            date: Date,
            context: Context?): LogEntry {

            fun log(msg: String) {
                Log.e("GENERATE_LOGS", "~~~~~~~ $msg")
            }



            var text = preText
            if (text.isNotEmpty()) text += "\n"
            text += data.joinToString(separator = "\n") {
                val extraText =  if (it.description.isNotBlank()) {
                    ", ${ValueNamePair.REF_DESCRIPTION} = [${it.description}]"
                } else ""
                String.format(
                    Locale.GERMAN,
                    "%s %s %d%s",
                    context?.getString(statCPIfilterConstraintId),
                    capitalize(it.name),
                    it.value,
                    extraText)

            }
            text += ("\n" + postText)
            log(text)

            return createLogEntry(text = text, timeOffsetInMillis = timeOffsetInMillis, date = date)
        }

        fun createLogEntry(text: String, timeOffsetInMillis: Long = 0, date: Date = Calendar.getInstance().time, exp: Int = 0): LogEntry {
            val e = LogEntry(
                exp = exp,
                text = text,
                session_ids = Global.activeSessionIds
            )
            e.dateOfCreation = Date(date.time + timeOffsetInMillis)
            log( "generated log = ${e.text}")
            return e
        }

        fun createInitialLogs(
            newCharId: Long,
            initialLogs: List<LogEntry>,
            context: Context) {

            fun log(msg: String) {
                Log.d("DO_CREATE_NEW_CHAR", msg)
            }
            DatabaseHelper.getInstance(context)?.let { dbh ->
                val initialLogIds = ArrayList<Long>()
                initialLogs.forEach { initialLog ->
                    initialLog.character_ids?.add(newCharId)
                    dbh.createLogEntry(initialLog).also {
                        initialLog.id = it
                        initialLogIds.add(it)
                    }
                }
                log("initial log created. calling on new character created")
            }

        }
    }
}