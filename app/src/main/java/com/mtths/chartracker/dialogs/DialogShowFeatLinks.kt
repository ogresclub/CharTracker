package com.mtths.chartracker.dialogs

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView.OnItemClickListener
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mtths.chartracker.adapters.FeatAdapter
import com.mtths.chartracker.dataclasses.FeatLink
import com.mtths.chartracker.R

class DialogShowFeatLinks(
    val featLink: FeatLink,
    var onLinkClicked: ((String, DialogShowFeatLinks) -> Unit)? = null
) : DialogFragment() {
    private lateinit var adapter: FeatAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var cancel_button: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_show_filtered_logs, container, false)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        adapter = FeatAdapter(
            requireContext(),
            R.layout.list_view_item_log,
            featLink,
            onLinkClicked = {link ->
                onLinkClicked?.invoke(link, this)
            }
        )
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        fillAdapter()
        cancel_button.setOnClickListener { dismiss() }

    }



    override fun onResume() {
        super.onResume()
        fillAdapter()
    }

    private fun initInterfaceItems(view: View) {
        cancel_button = view.findViewById(R.id.cancel_button)
        recyclerView = view.findViewById(R.id.filteredLogsRecyclerView)
    }

    fun fillAdapter() {
        adapter.notifyDataSetChanged()
    }
}