package com.mtths.chartracker.dialogs

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.Toast
import com.mtths.chartracker.*
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.dialogs.DialogDataOptions.Companion.toggleSelected
import com.mtths.chartracker.utils.Helper.askPermission
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.dropbox.DropBoxIntent
import com.mtths.chartracker.dropbox.DropboxDialogFragment

class DialogExportCharLog : DropboxDialogFragment() {
    lateinit var xmlButton: Button
    lateinit var textButton: Button
    lateinit var cancelButton: Button
    lateinit var localButton: Button
    lateinit var dropboxButton: Button
    lateinit var okButton: Button
    var dbHelper: DatabaseHelper? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_export_char_log, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        dbHelper = getInstance(context)
        dbXHelper.mDropBoxResultReceiver = (activity as ActivityCharDetails?)!!.dropBoxResultReceiver
    }

    private fun initInterfaceItems(view: View) {
        xmlButton = view.findViewById<View>(R.id.xml_button) as Button
        textButton = view.findViewById<View>(R.id.text_button) as Button
        localButton = view.findViewById<View>(R.id.localButton) as Button
        dropboxButton = view.findViewById<View>(R.id.dropboxButton) as Button
        cancelButton = view.findViewById<View>(R.id.cancelButton) as Button
        okButton = view.findViewById<View>(R.id.ok_button) as Button

        // -----------------set OnclickListeners for PopUp Window ------------------------//
        val xmlDbButtons = arrayOf(xmlButton, textButton)
        val localDropboxButtons = arrayOf(localButton, dropboxButton)
        toggleSelected(buttonArray = xmlDbButtons, selectThisButton = xmlButton)
        toggleSelected(buttonArray =  localDropboxButtons, selectThisButton = dropboxButton)

        val buttonGroups = arrayOf(xmlDbButtons, localDropboxButtons)
        for (bGroup in buttonGroups) {
            bGroup.forEach { b ->
                b.setOnClickListener {
                    toggleSelected(buttonArray = bGroup, selectThisButton = b)
                }
            }
        }
        cancelButton.setOnClickListener {
            log("Cancel Button pressed")
            dismiss()
        }
        okButton.setOnClickListener {
            //Export XML Dropbox
            if (xmlButton.isSelected && dropboxButton.isSelected) {
                log("exporting XML charLog to Dropbox")
                exportCharLogToDropBoxAsXml()
            }
            //Export Text Dropbox
            if (textButton.isSelected && dropboxButton.isSelected) {
                log("exporting text charLog to Dropbox")
                exportCharLogToDropBoxAsText()
            }
            //Export XML Local
            if (xmlButton.isSelected && localButton.isSelected) {
                log("Exporting charLog to local XML")
                exportCharLogAsXmlLocal()
            }
            //Export Text Local
            if (textButton.isSelected && localButton.isSelected) {
                log("Exporting charLog local as text")
                exportCharLogAsTextLocal()
            }
        }
    }

    fun exportCharLogAsTextLocal() {
        val result = askPermission(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            DialogDataOptions.PERMISSIONS_REQUEST_EXTERNAL_STORAGE_EXPORT_XML, activity)
        if (result) {
            activity?.let {
                FileDialog.openFileDialog(
                    ActivityCharDetails.FILE_SELECT_CODE_EXPORT_CHARLOG_LOCAL_TEXT,
                    arrayOf("txt"),
                    FileDialog.MODE_CREATE,
                    it
                )
            }
        } else {
            Toast.makeText(context, "No Rights to write to storage", Toast.LENGTH_SHORT).show()
        }
        dismiss()
    }

    fun exportCharLogAsXmlLocal() {
        val result = askPermission(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            DialogDataOptions.PERMISSIONS_REQUEST_EXTERNAL_STORAGE_EXPORT_XML, activity)
        if (result) {
            activity?.let {
                FileDialog.openFileDialog(
                    ActivityCharDetails.FILE_SELECT_CODE_EXPORT_CHARLOG_LOCAL_XML,
                    arrayOf("xml"),
                    FileDialog.MODE_CREATE,
                    it
                )
            }
        } else {
            Toast.makeText(context, "No Rights to write to storage", Toast.LENGTH_SHORT).show()
        }
        dismiss()
    }

    fun exportCharLogToDropBoxAsText() {
        log("exporting charLog as text to Dropbox")
        (activity as? ActivityCharDetails?)?.character?.let { c ->
            dbXHelper.startDropboxIntent(
                DropBoxIntent.TASK_UPLOAD,
                DropBoxIntent.MODE_CHARLOG_AS_TEXT,
                    c.name)
        }
    }



    fun exportCharLogToDropBoxAsXml() {
        log("exporting charLog as xml to Dropbox")
        (activity as? ActivityCharDetails?)?.character?.let { c ->
            dbXHelper.startDropboxIntent(
                DropBoxIntent.TASK_UPLOAD,
                DropBoxIntent.MODE_CHARLOG_AS_XML,
                    c.name)

        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): DialogExportCharLog {
            val args = Bundle()
            val fragment = DialogExportCharLog()
            fragment.arguments = args
            return fragment
        }
    }
}