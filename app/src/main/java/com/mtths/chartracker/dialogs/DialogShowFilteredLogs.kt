package com.mtths.chartracker.dialogs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mtths.chartracker.Global
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.adapters.LogAdapter
import com.mtths.chartracker.utils.Helper

/**
 * similar to LogView but in a dialogFragment
 * ONLY USE IN ACTIVITY CHAR DETAILS BECAUSE IT USES CHARLOG AS BASE TO FILTER
 */
class DialogShowFilteredLogs : DialogFragment() {
    private lateinit var cancelButton: Button
    private lateinit var logsRecyclerView: RecyclerView
    private lateinit var logAdapter: LogAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_show_filtered_logs, container, false)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        dialog?.setCancelable(false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        logAdapter = LogAdapter(
            (activity as ProgressBarFrameDropBoxActivity),
            R.layout.list_view_item_show_filtered_logs,
            LogAdapter.EXP_TEXT
        )

        logAdapter.setMarkErrors(arguments?.getBoolean(PARAM_MARK_ERRORS, false) ?: false)

        logsRecyclerView.apply {
            adapter = logAdapter
            layoutManager = LinearLayoutManager(context)
        }
        fillLogAdapter()
        cancelButton.setOnClickListener { dismiss() }
    }

    override fun onResume() {
        super.onResume()
        fillLogAdapter()
    }

    fun setMarkMistakes(markMistakes: Boolean) {
        logAdapter.setMarkErrors(markMistakes)
    }

    ////////////////////// PRIVATE METHODS ///////////////////
    private fun initInterfaceItems(view: View) {
        cancelButton = view.findViewById(R.id.cancel_button)
        logsRecyclerView = view.findViewById(R.id.filteredLogsRecyclerView)
    }

    fun fillLogAdapter() {
        if (arguments?.getBoolean(PARAM_CALLED_FROM_ACTIVITY_CHAR_DETAILS) == true) {
            try {
                Helper.log("FILTERED_LOGS", "called from activity char details")
                (activity as? ActivityCharDetails?)?.charLogs?.let {
                    logAdapter.setLogs(it)
                }
            } catch (e: ClassCastException) {
                e.printStackTrace()
            }
        } else if (requireArguments().getBoolean(PARAM_USE_SESSION_LOGS)) {
            Helper.log("FILTERED_LOGS", "use session logs")
            logAdapter.setLogs(Global.logsFilteredBySession)
        } else {
            Helper.log("FILTERED_LOGS", "use logs to show")
            logAdapter.setLogs(Global.logsToShow)
        }
        logAdapter.filter.filter(requireArguments().getString(PARAM_SEARCH_STRING))
    }

    companion object {
        const val PARAM_SEARCH_STRING = "search_string"
        const val PARAM_MARK_ERRORS = "mark_errors"
        const val PARAM_CALLED_FROM_ACTIVITY_CHAR_DETAILS = "activity_char_details"
        const val PARAM_USE_SESSION_LOGS = "use_session_logs"
        const val TAG= "dialog_show_filtered_logs"
        /**
         *
         * @param searchString
         * @param markErrors
         * @param activity_char_details true if you call this method from activity char details
         * @return
         */

        fun newInstance(searchString: String,
                        markErrors: Boolean = false,
                        activity_char_details: Boolean = true,
                        use_session_logs: Boolean = false): DialogShowFilteredLogs {
            Log.e("SRSKLL", Log.getStackTraceString(Exception()))
            val args = Bundle()
            args.putString(PARAM_SEARCH_STRING, searchString)
            args.putBoolean(PARAM_MARK_ERRORS, markErrors)
            args.putBoolean(PARAM_CALLED_FROM_ACTIVITY_CHAR_DETAILS, activity_char_details)
            args.putBoolean(PARAM_USE_SESSION_LOGS, use_session_logs)
            val fragment = DialogShowFilteredLogs()
            fragment.arguments = args
            return fragment
        }
    }
}