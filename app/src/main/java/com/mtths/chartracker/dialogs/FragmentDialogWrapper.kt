package com.mtths.chartracker.dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.mtths.chartracker.R

class FragmentDialogWrapper : DialogFragment() {

    lateinit var fragment: Fragment

    companion object {
        const val REF_SUFFIX = "_dialog"
        private const val ARG_FRAGMENT_CLASS = "fragment_class"
        private const val ARG_FRAGMENT_ARGS = "fragment_args"

        fun newInstance(fragmentClass: Class<out Fragment>, args: Bundle? = null): FragmentDialogWrapper {
            val fragment = FragmentDialogWrapper()
            val bundle = Bundle()
            bundle.putString(ARG_FRAGMENT_CLASS, fragmentClass.name)
            bundle.putBundle(ARG_FRAGMENT_ARGS, args)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return requireActivity().layoutInflater.inflate(
            R.layout.dialog_fragment_wrapper,
            null, false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fragmentClassName = arguments?.getString(ARG_FRAGMENT_CLASS)
        val fragmentArgs = arguments?.getBundle(ARG_FRAGMENT_ARGS)

        if (fragmentClassName != null) {
            fragment = childFragmentManager.fragmentFactory.instantiate(
                requireActivity().classLoader,
                fragmentClassName
            )
            fragment.arguments = fragmentArgs


            childFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
        }
    }
}