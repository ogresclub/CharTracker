package com.mtths.chartracker.dialogs

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.mtths.chartracker.*
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.dialogs.DialogChangeCharacterStatus.OnCharStatusChangedListener
import com.mtths.chartracker.utils.Helper.log
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.HttpUtils

class DialogCurrentCommit : DialogFragment() {

    lateinit var button_update: Button
    lateinit var button_set: Button
    lateinit var button_ok: TextView
    lateinit var button_cancel: TextView
    lateinit var text_view_hash_display: TextView
    var mOnCharStatusChangedListener: OnCharStatusChangedListener? = null
    var mHashLoader: HashLoader? = null
    lateinit var prefs: PrefsHelper
    var c: Character? = null
    var dbHelper: DatabaseHelper? = null
    val LOADING_INDICATOR = "..loading"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.dialog_current_commit, container, false)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        assert(dialog?.window != null)
        dialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        prefs = PrefsHelper(context)
        dbHelper = getInstance(context)
        arguments?.let { arguments ->
            c = dbHelper?.getCharacter(
                arguments.getLong(DialogSettingsChar.PARAM_CHAR_ID, 1)
            )
        }
        initInterfaceItems(view)
        implementOnClickListeners()
        text_view_hash_display.text = c?.currentCommit?.hash?: HttpUtils.NO_CURRENT_COMMIT_AVAILABLE_MSG


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        /*
        This makes sure that the container activity has implemented
        the callback interface. If not, it throws an exception
        */
        mOnCharStatusChangedListener = try {
            context as OnCharStatusChangedListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString()
                    + " must implement OnCharStatusChangedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mOnCharStatusChangedListener = null
        mHashLoader?.cancel(true)
    }

    private fun initInterfaceItems(v: View) {
        button_ok = v.findViewById(R.id.button_ok)
        button_cancel = v.findViewById(R.id.button_cancel)
        button_update = v.findViewById(R.id.button_update)
        button_set = v.findViewById(R.id.button_set)
        text_view_hash_display = v.findViewById(R.id.hash_display)
    }

    private fun implementOnClickListeners() {
        button_update.setOnClickListener {
            text_view_hash_display.text = LOADING_INDICATOR
            if (mHashLoader == null) {
                mHashLoader = HashLoader()
                mHashLoader?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            }

        }
        button_set.setOnClickListener {
            val dialog: DialogCustom<String> = DialogCustom()
            dialog.setInput(false, "", true, "enter hash")
            dialog.setTitle("    Set Commit    ")
            dialog.setPositiveButton("Ok") {
                val new_hash = dialog.text2.text.toString().trim()
                if (new_hash.isEmpty()) {
                    Toast.makeText(context, "Please enter a string", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    dialog.dismiss()
                    text_view_hash_display.text = new_hash
                }
            }
            dialog.setNegativeButton("Cancel") { dialog.dismiss() }
            fragmentManager?.let { fm ->
                dialog.show(fm, "setcurrentCommit")
            }

        }
        button_cancel.setOnClickListener {
            dismiss()
        }
        button_ok.setOnClickListener {
            c?.let { c ->
                val oldHash = c.currentCommit.hash
                //get priority value from EditTextPriority
                val newHash = text_view_hash_display.text.toString()
                val hashchanged = oldHash != newHash
                log("new hash: $newHash, old hash: $oldHash")
                if (hashchanged && !newHash.contentEquals(LOADING_INDICATOR)) {
                    c.currentCommit = HttpUtils.CommitWithLink(newHash)
                    mOnCharStatusChangedListener?.onCharStatusChanged(c)
                    Toast.makeText(context, "Hash set to $newHash", Toast.LENGTH_LONG,).show()
                }

            }
            dismiss()
        }

    }

    inner class HashLoader(): AsyncTask<Unit, Unit, String?>() {
        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Unit?): String? {
            return HttpUtils.getHashOflatestCommit()?.hash
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: String?) {
            if (result == null) {
                Toast.makeText(context, "hash download failed", Toast.LENGTH_LONG).show()
            } else {
                text_view_hash_display.text = result
            }
            mHashLoader = null
        }
    }

    companion object {
        const val PARAM_CHAR_ID = "param_char_id"
        fun newInstance(char_id: Long): DialogCurrentCommit {
            val args = Bundle()
            args.putLong(PARAM_CHAR_ID, char_id)
            val fragment = DialogCurrentCommit()
            fragment.arguments = args
            return fragment
        }
    }


}