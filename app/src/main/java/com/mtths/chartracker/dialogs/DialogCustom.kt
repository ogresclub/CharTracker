package com.mtths.chartracker.dialogs

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.*
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import android.widget.MultiAutoCompleteTextView.Tokenizer
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.mtths.chartracker.DatabaseHelper.Companion.getInstance
import com.mtths.chartracker.Global
import com.mtths.chartracker.widgets.MyMultiAutoCompleteTextView
import com.mtths.chartracker.R
import com.mtths.chartracker.activities.ActivityCharDetails
import com.mtths.chartracker.activities.ActivityMain
import com.mtths.chartracker.activities.ProgressBarFrameDropBoxActivity
import com.mtths.chartracker.databinding.DialogCustomBinding
import com.mtths.chartracker.dataclasses.Character
import com.mtths.chartracker.dataclasses.LogEntry
import com.mtths.chartracker.fragments.FragmentMainScreen
import com.mtths.chartracker.preferences.PrefsHelper
import com.mtths.chartracker.utils.AutoCompleteUtils
import com.mtths.chartracker.utils.AutoCompleteUtils.Companion.proposeExp
import com.mtths.chartracker.utils.CharProgressItemMap
import com.mtths.chartracker.widgets.MyMultiAutoCompleteTextViewNonfiltering
import kotlin.collections.ArrayList

/**
 * Created by mtths on 22.04.17.
 */
class DialogCustom<T> : DialogFragment() {
    var _binding: DialogCustomBinding? = null
    val binding: DialogCustomBinding
        get() = _binding!!

    var fullScreen = false
    var noTitle = true
    private val DEBUG_TAG = "DIALOG_CUSTOM"
    private var title: CharSequence? = null
    private var titleResId: Int? = null
    private lateinit var titleView: TextView
    private var yesButton: TextView? = null
    private var yesText: CharSequence? = null
    private var yesTextId: Int? = null
    private var yesOnClickListener: View.OnClickListener? = null
    private var noButton: TextView? = null
    private var noText: CharSequence? = null
    private var noTextId: Int? = null
    private var noOnClickListener: View.OnClickListener? = View.OnClickListener { dismiss() }
    private var noTextColor: Int? = null
    private var message_prompt: TextView? = null
    private var message: CharSequence? = null
    private var messageId: Int? = null
    private var view: View? = null
    private var messageOnClickListener: View.OnClickListener? = null
    private lateinit var editText_1: MyMultiAutoCompleteTextView
    private var editText_1_hint: CharSequence? = null
    private var editText_1_hint_res_Id: Int? = null
    private var editText_1_text: CharSequence? = null
    private var editText_1_text_res_Id: Int? = null

    private var editText_1_inputType = -1
    private var editText_1_visibility = View.GONE
    private var editText_1_adapter: ArrayAdapter<T>? = null
    private var alwaysShowAutoCompleteEditText1 = false
    private var editText_1_onItemClickListener: OnItemClickListener? = null
    private var editText1Tokenizer: Tokenizer? = null
    private lateinit var editText_2: MyMultiAutoCompleteTextView
    private var editText_2_hint: CharSequence? = null
    private var editText_2_hint_res_Id: Int? = null
    private var editText_2_text: CharSequence? = null
    private var editText_2_text_res_Id: Int? = null
    private var editText_2_inputType = -1
    private var editText_2_visibility = View.GONE
    private var noButtonVisibility = View.VISIBLE
    private var yesButtonVisibility = View.VISIBLE
    private var editText_2_adapter: ArrayAdapter<T>? = null
    private var alwaysShowAutoCompleteEditText2 = false
    private var editText_2_onItemClickListener: OnItemClickListener? = null
    private var editText2Tokenizer: Tokenizer? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = DialogCustomBinding.inflate(inflater, container, false)
        val root = binding.root
        dialog?.window?.apply {
            setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
        if (noTitle) {
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
            setStyle(STYLE_NO_TITLE, R.style.AppTheme_NoTitleDialog)
        }
        return root
    }

    override fun onStart() {
        super.onStart()
        if (fullScreen) {
            dialog?.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInterfaceItems(view)
        messageId?.also {msgId ->
            message_prompt?.visibility = View.VISIBLE
            message_prompt?.setText(msgId)
            messageOnClickListener?.let {  message_prompt?.setOnClickListener(it)}
        } ?: run {
            message?.also {
                message_prompt?.visibility = View.VISIBLE
                message_prompt?.text = it
                messageOnClickListener?.let {  message_prompt?.setOnClickListener(it)}
            } ?: kotlin.run {
                message_prompt?.visibility = View.GONE
            }
        }

        title?.let {
            titleView.text = title
        }
        titleResId?.let {
            titleView.text = context?.getString(it)
        }
        if (title != null || titleResId != null) {
            titleView.visibility = View.VISIBLE
        } else {
            titleView.visibility = View.GONE
        }
        editText_1.visibility = editText_1_visibility
        if (editText_1_hint != null) {
            editText_1.hint = editText_1_hint
        }
        editText_1.hint = editText_1_hint_res_Id?.let { context?.getString(it) }
        if (editText_1_text != null) {
            editText_1.setText(editText_1_text)
            editText_1.setSelection(0, editText_1_text!!.length)
            setFocus(editText_1, context)
        }
        editText_1_text_res_Id?.let {editText_1.setText( context?.getString(it)) }

        if (editText_1_inputType >= 0) {
            editText_1.inputType = editText_1_inputType
            editText_1.setRawInputType(editText_1_inputType)
        }
        if (editText_1_adapter != null) {
            editText_1.setAdapter(editText_1_adapter)
            editText_1.always_show = alwaysShowAutoCompleteEditText1
        }
        if (editText1Tokenizer != null) {
            editText_1.setTokenizer(editText1Tokenizer)
        }
        if (editText_1_onItemClickListener != null) {
            editText_1.onItemClickListener = editText_1_onItemClickListener
        }
        editText_2.visibility = editText_2_visibility
        if (editText_2_hint != null) {
            editText_2.hint = editText_2_hint
            setFocus(editText_2, context)
        }
        editText_2.hint = editText_2_hint_res_Id?.let { context?.getString(it) }

        if (editText_2_text != null) {
            editText_2.setText(editText_2_text)
            editText_2.setSelection(0, editText_2_text!!.length)
        }
        editText_2_text_res_Id?.let { editText_2.setText(context?.getString(it)) }

        if (editText_2_inputType >= 0) {
            editText_2.inputType = editText_2_inputType
            editText_2.setRawInputType(editText_2_inputType)
        }
        if (editText_2_adapter != null) {
            editText_2.setAdapter(editText_2_adapter)
            editText_2.always_show = alwaysShowAutoCompleteEditText2
        }
        if (editText2Tokenizer != null) {
            editText_2.setTokenizer(editText2Tokenizer)
        }
        if (editText_2_onItemClickListener != null) {
            editText_2.onItemClickListener = editText_2_onItemClickListener
        }
        if (yesOnClickListener != null) {
            yesButton!!.setOnClickListener(yesOnClickListener)
        }
        if (noOnClickListener != null) {
            noButton!!.setOnClickListener(noOnClickListener)
        }
        yesButton!!.visibility = yesButtonVisibility
        noButton!!.visibility = noButtonVisibility
        if (yesText != null) {
            yesButton!!.text = yesText
        }
        if (noText != null) {
            noButton!!.text = noText
        }
        yesTextId?.let {
            yesButton?.setText(it)
        }
        noTextId?.let {
            noButton?.setText(it)
        }
        noTextColor?.let {
            noButton?.setTextColor(it)
        }
        this.view?.let {
            binding.customViewContainer.addView(it)
        }
    }

    override fun onResume() {
        super.onResume()
        setFocus(editText_1, context)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    fun setButtonVisibility(negativeButtonVisibility: Int, positiveButtonVisibility: Int) {
        yesButtonVisibility = positiveButtonVisibility
        noButtonVisibility = negativeButtonVisibility
    }

    fun setPositiveButton(text: CharSequence?, onClickListener: View.OnClickListener? = null) {
        yesText = text
        yesOnClickListener = onClickListener
    }
    fun setPositiveButton(textId: Int?, onClickListener: View.OnClickListener? = null) {
        yesTextId = textId
        yesOnClickListener = onClickListener
    }

    fun setNegativeButton(text: CharSequence, onClickListener: View.OnClickListener? = null) {
        noText = text
        noOnClickListener = onClickListener
    }
    fun setNegativeButton(textId: Int?, onClickListener: View.OnClickListener? = null) {
        noTextId = textId
        noOnClickListener = onClickListener
    }

    fun setNegativeButtonTextColor(color: Int) {
        noTextColor = color
    }

    fun setDefaultInput() {
        // I need to give one parameter so the compiler knows which of the overloaded functions to use
        setInput(hint1StringResId = null)
    }


    fun setInput(
        editText1Visible: Boolean = false,
        hint1: CharSequence = "",
        editText2Visible: Boolean = true,
        hint2: CharSequence = "",
        hint1StringResId: Int? = null,
        hint2StringResId: Int? = null) {
        if (editText1Visible) {
            editText_1_visibility = View.VISIBLE
            editText_1_hint = hint1
            editText_1_hint_res_Id = hint1StringResId
        }
        if (editText2Visible) {
            editText_2_visibility = View.VISIBLE
            editText_2_hint = hint2
            editText_2_hint_res_Id = hint2StringResId
        }
    }

    fun setEditText1Adapter(adapter: ArrayAdapter<T>?, alwaysShow: Boolean) {
        editText_1_adapter = adapter
        alwaysShowAutoCompleteEditText1 = alwaysShow
    }

    fun setEditText2Adapter(adapter: ArrayAdapter<T>?, alwaysShow: Boolean) {
        editText_2_adapter = adapter
        alwaysShowAutoCompleteEditText2 = alwaysShow
    }

    fun setEditText1_OnItemClickListener(listener: OnItemClickListener?) {
        editText_1_onItemClickListener = listener
    }

    fun setEditText2_OnItemClickListener(listener: OnItemClickListener?) {
        editText_2_onItemClickListener = listener
    }

    fun setEditText1Tokenizer(tokenizer: MultiAutoCompleteTextView.Tokenizer?) {
        editText1Tokenizer = tokenizer
    }

    fun setEditText2Tokenizer(tokenizer: Tokenizer?) {
        editText2Tokenizer = tokenizer
    }

    fun setInputText(
        text1Text: CharSequence? = null,
        text2Text: CharSequence? = null,
        text1TextResId: Int? = null,
        text2TextResId: Int? = null,
    ) {
        editText_1_text = text1Text
        editText_1_text_res_Id = text1TextResId
        editText_2_text = text2Text
        editText_2_text_res_Id = text2TextResId
    }

    //todo remove this method and only use stringResources!
    fun setMessage(message: CharSequence?) {
        this.message = message
    }

    fun setMessage(messageId: Int) {
        this.messageId = messageId
    }

    fun setTitle(title: CharSequence?) {
        this.title = title
    }

    fun setTitle(titleStringResourceId: Int) {
        this.titleResId = titleStringResourceId
    }


    fun setMessageOnClickListener(listener: View.OnClickListener) {
        messageOnClickListener = listener
    }

    fun setView(view: View) {
        this.view = view
    }

    /**
     * if you are not interested in one of the edit texts just write a negative number.
     * @param text_1_inputType
     * @param text_2_inputType
     */
    fun setTextInputType(
        text_1_inputType: Int,
        text_2_inputType: Int) {
        editText_1_inputType = text_1_inputType
        editText_2_inputType = text_2_inputType
    }

    val text1: EditText
        get() = editText_1

    val text2: EditText
        get() = editText_2

    /////////////////////////////// PRIVATE METHODS //////////////////////////////////////////////
    private fun initInterfaceItems(v: View) {
        yesButton = binding.yes
        noButton = binding.no
        editText_1 = binding.text1
        editText_2 = binding.text2
        message_prompt = binding.promptMessage
        titleView = binding.title
    }

    private fun setFocus(editText: EditText, context: Context?) {
        editText.isFocusableInTouchMode = true
        editText.requestFocus()
    }


    companion object {

        fun newInstance(): DialogCustom<*> {
            val args = Bundle()
            val fragment: DialogCustom<*> = DialogCustom<Any?>()
            fragment.arguments = args
            return fragment
        }


        fun promptFile(fragment: Fragment, onFinish: (fileName: String) -> Unit) {
            val dialog = newInstance()
            dialog.setDefaultInput()
            dialog.setTitle(R.string.enter_file_name_prompt)
            dialog.setPositiveButton(fragment.context?.getString(R.string.ok)) {
                var fileName = dialog.text2.text.toString().trim()
                if (fileName.isBlank() ||
                    (fileName.contains(".") && !fileName.endsWith(".xml"))
                ) {
                    Toast.makeText(fragment.context, R.string.msg_invalid_file_name, Toast.LENGTH_LONG)
                        .show()
                } else {
                    onFinish(fileName)
                    dialog.dismiss()
                }
            }
            fragment.childFragmentManager.let { fm ->
                dialog.show(fm, "custom_dialog_prompt_filename")
            }

        }

        fun showAlterDialog(fragmentManager: FragmentManager,
                            title: String? = null,
                            msg: String? = null) {
            val dialog = newInstance()
            dialog.setTitle(title)
            dialog.setMessage(msg)
            dialog.setPositiveButton("Ok") {
                dialog.dismiss()
            }
            dialog.setButtonVisibility(View.INVISIBLE, View.VISIBLE)
            dialog.setInput(false, "", false, "")
            dialog.show(fragmentManager, "promptString")
        }

        fun promptString(fragmentManager: FragmentManager,
                         title: String? = null,
                         msg: String? = null,
                         onConfirm: (s: String) -> Unit,
                         onCancel: () -> Unit = {}) {
            val dialog = newInstance()
            dialog.setTitle(title)
            dialog.setMessage(msg)
            dialog.setPositiveButton(R.string.ok) {
                dialog.dismiss()
                onConfirm(dialog.editText_2.text.toString())
            }
            dialog.setNegativeButton(R.string.cancel) {
                dialog.dismiss()
                onCancel()
            }
            dialog.setInput(false, "", true, "")
            dialog.show(fragmentManager, "promptString")
        }


        fun showConfirmDialog(
            fragmentManager: FragmentManager,
            msg: String,
            onConfirm: () -> Unit,
            onCancel: () -> Unit
        ) {
            val dialog = newInstance()
            dialog.setTitle(msg)
            dialog.setPositiveButton(R.string.yes) {
                dialog.dismiss()
                onConfirm()
            }
            dialog.setNegativeButton(R.string.cancel) {
                dialog.dismiss()
                onCancel()
            }
            dialog.show(fragmentManager, "ConfirmDeletion")
        }

        fun showNumberPicker(
            context: Context,
            onAmountChanged: (newValue: Int) -> Unit,
            onApplyBeforeChangeAmount: (newValue: Int) -> Unit = {},
            setNewAmount: (amount: Int) -> Unit,
            getOldAmount: () -> Int,
        ) {
            val d = getAlterDialogBuilder(context)
            val inflater = LayoutInflater.from(context)
            val dialogView = inflater.inflate(R.layout.number_picker_dialog, null)
            d.setTitle(R.string.label_change_amount)
            d.setMessage(R.string.label_choose_new_amount)
            d.setView(dialogView)
            val numberPicker = dialogView.findViewById<NumberPicker>(R.id.dialog_number_picker)
            numberPicker.minValue = 0
            numberPicker.maxValue = 999
            numberPicker.value = getOldAmount()
            numberPicker.wrapSelectorWheel = false
            d.setPositiveButton(R.string.ok) { dialogInterface, i ->
                onApplyBeforeChangeAmount(numberPicker.value)
                setNewAmount(numberPicker.value)
                onAmountChanged(numberPicker.value)
            }
            d.setNegativeButton(R.string.cancel) { dialogInterface, i -> }
            val alertDialog = d.create()
            alertDialog.show()
        }
        fun showNumberPickerToChangeAmount(
            context: Context,
            onAmountChanged: (newValue: Int) -> Unit,
            setNewAmount: (amount: Int) -> Unit,
            getOldAmount: () -> Int,
            logEntryWithAmountString: LogEntry?,
            amountRef: String,
        ) {
            showNumberPicker(
                context = context,
                onApplyBeforeChangeAmount = { newValue ->
                    val oldAmount = getOldAmount()
                    LogEntry.updateAmountInLogEntry(
                        oldAmount = oldAmount,
                        newAmount = newValue,
                        context = context,
                        logEntryWithAmountString = logEntryWithAmountString,
                        amountRef = amountRef
                    )
                },
                setNewAmount = setNewAmount,
                onAmountChanged = onAmountChanged,
                getOldAmount = getOldAmount
            )
        }

        fun showNumberPrompt(
            fragmentManager: FragmentManager,
            message: String,
            hint: String = "",
            initialValue: Int? = 1,
            minValue: Int? = null,
            maxValue: Int? = null,
            proposals: List<Int>? = null,
            context: Context,
            inputType: Int = InputType.TYPE_CLASS_NUMBER,
            customView: View? = null,
            onConfirm: (n: Int) -> Unit
        ) {
            val d = DialogCustom<String>()
            d.setInput(true, "", false, "")
            d.setTitle(message)
            d.setTextInputType(inputType, -1)
            customView?.let { d.setView(it) }
            proposals?.let { proposals ->
                d.setEditText1Adapter(ArrayAdapter<String>(
                    context,
                    R.layout.auto_complete_entry,
                    proposals.filter {
                        (maxValue == null || it <= maxValue) &&
                                (minValue == null || it >= minValue)
                    }.map { it.toString() }.toTypedArray()
                ),
                    alwaysShow = true
                )
            }
            initialValue?.let {
                d.editText_1_text = it.toString()
            }
            d.editText_1_hint = hint
            d.setPositiveButton(R.string.ok) {
                val value = d.editText_1.text.toString().toInt()
                var validValue = true
                minValue?.let { min ->
                    if (value < min) {
                        Toast.makeText(
                            context,
                            context.getString(R.string.msg_notify_placeholder_value_is, "min", min),
                            Toast.LENGTH_SHORT
                        ).show()
                        validValue = false
                    }
                }
                maxValue?.let { max ->
                    if (value > max) {
                        Toast.makeText(
                            context,
                            context.getString(R.string.msg_notify_placeholder_value_is, "max", max),
                            Toast.LENGTH_SHORT
                        ).show()
                        validValue = false
                    }
                }
                if (validValue) {
                    onConfirm(value)
                    d.dismiss()
                }

            }
            d.show(fragmentManager, "NumberPrompt")
        }

//        fun showCreateLogEntryDialogCustom(
//            activity: Activity,
//            initialExp: Int = 0,
//            initialLogText: String = "",
//            sessionIds: List<Long> = Global.activeSessionIds,
//        ) {
//            ////////// create new Log Entry ///////////////////////////////
//
//
//            val activeChars = when (activity) {
//                is ActivityCharDetails -> listOfNotNull(activity.character)
//                else -> Global.chars.filter { it.isActive }
//            }
//            val mOnLogEntryAddedListener = when (activity) {
//                is ActivityCharDetails -> activity
//                is ActivityMain -> activity
//                else -> null
//            }
//
//
//
//            getAlterDialogBuilder(activity).apply {
//
//                fun getCharProgressItemsMap(): CharProgressItemMap {
//                    return (activity as ProgressBarFrameDropBoxActivity).charProgressItemsMap
//                }
//
//                setTitle(R.string.msg_create_a_log_entry)
//
//
//
//                val root = LayoutInflater.from(context).inflate(
//                    R.layout.dialog_create_log_entry, null, false
//                ) as RelativeLayout
//                val editTextExp = root.findViewById<MyMultiAutoCompleteTextView>(R.id.editExp).apply {
//                    setText(if (initialExp != 0) initialExp.toString() else "")
//                }
//                val editTextLog = root.findViewById<MyMultiAutoCompleteTextViewNonfiltering>(R.id.editText).apply {
//                    setText(initialLogText)
//                }
//                val addButton = root.findViewById<Button>(R.id.add_button).apply {
//                    visibility = View.GONE
//                }
//
//                AutoCompleteUtils.Companion.GenerateAutoCompleteDataArgs(
//                    et = editTextLog,
//                    cpiMap = getCharProgressItemsMap(),
//                    loadedCharDetailsToAutoComplete = true,
//                    activeRuleSystems = PrefsHelper(context).activeRuleSystems,
//                    autoCompleteTextToAddAdapter = mAutoCompleteAdapter
//                ).let {
//                    AutoCompleteUtils.fillAutocompleteData(it)
//                }
//
//
//
//                AutoCompleteUtils.implementReplaceTextWithAutoCompleteProposal(
//                    multiAutoCompleteTextView = editTextLog,
//                    cpiMap = ::getCharProgressItemsMap
//                )
//                editTextLog.addTextChangedListener(object : TextWatcher {
//                    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
//                    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
//                    override fun afterTextChanged(s: Editable) {
//                        fillAutoCompleteAdapter()
//                        proposeExp(s = s, maTv = editTextExp, cpiMap = getCharProgressItemsMap())
//                    }
//                })
//
//                setView(root)
//                create()
//                setNegativeButton(
//                    R.string.cancel
//                ) { d, _ -> d.dismiss() }
//                setPositiveButton(R.string.create) { d, _ ->
//                    getInstance(context)?.apply {
//                        createLogEntry(
//                            context,
//                            editTextExp,
//                            editTextLog,
//                            ArrayList(activeChars),
//                            ArrayList(sessionIds),
//                            mOnLogEntryAddedListener,
//                            true
//                        )
//                        Toast.makeText(
//                            context,
//                            R.string.msg_log_entry_created,
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    }
//                    d.dismiss()
//                }
//
//                show()
//            }
//        }

        fun showCreateLogEntryDialog(
            context: Context,
            initialExp: Int = 0,
            initialLogText: String = "",
            activeChars: List<Character> = Global.chars.filter { it.isActive },
            sessionIds: List<Long> = Global.activeSessionIds,
            mOnLogEntryAddedListener: FragmentMainScreen.OnLogEntryAddedListener?,
            fragmentManager: FragmentManager,
            modifyDialog: (d: DialogCustom<*>) -> Unit = {},
            ) {
            ////////// create new Log Entry ///////////////////////////////

            newInstance().apply {
                fullScreen = true
                noTitle = false
                setTitle(R.string.msg_create_a_log_entry)
                setInput(
                    editText1Visible = true,
                    hint1StringResId = R.string.msg_enter_exp,
                    editText2Visible = true,
                    hint2StringResId = R.string.msg_what_happened
                )
                setTextInputType(
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED,
                    -1
                )
                setInputText(
                    text1Text = if (initialExp != 0) initialExp.toString() else "",
                    text2Text = initialLogText)
                setNegativeButton(
                    R.string.cancel
                ) { dismiss() }
                setPositiveButton(R.string.create) {
                    getInstance(context)?.apply {
                        createLogEntry(
                            context,
                            text1,
                            text2,
                            ArrayList(activeChars),
                            ArrayList(sessionIds),
                            mOnLogEntryAddedListener,
                            true
                        )
                        Toast.makeText(
                            context,
                            R.string.msg_log_entry_created,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    dismiss()
                }
                modifyDialog(this)

                show(fragmentManager, "createLogEntryDialog")
            }
        }




        //todo use string resources and translate used of alert dialog
        fun getAlterDialogBuilder(context: Context?): AlertDialog.Builder =
            AlertDialog.Builder(ContextThemeWrapper(context, R.style.myDialog))

        fun getMyDialogContext(context: Context) = ContextThemeWrapper(context, R.style.myDialog)
    }

}